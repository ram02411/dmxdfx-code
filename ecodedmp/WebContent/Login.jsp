<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>Evolve| Login</title>


    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    
    <script src="logine.js"></script>
    <style>
        .form-control:focus {
            border-color: none !important;
            box-shadow: none !important;
        }
    </style>
    <script src="assets/js/jquery-1.11.0.min.js"></script>

   
</head>
<body class="page-body login-page">


    <div class="container" style="background-color: white; width: 100%">
        <div class="col-md-12" style="height: 100px;"></div>
        <div class="col-md-12">

            <div class="col-md-12 col-lg-12" style="text-align: center">
                <a href="extra-login.html" class="logo">
                    <img src="assets/images/Ecode_Logo.png" width="150" alt="" />
                </a>

                <p class="description" style="margin-top: 12px">Dear user, log in to access the admin area!!!</p>
            </div>

        </div>
        <div class="col-md-12 col-lg-12" style="text-align: center; height: 100px">
		
		</div>
    </div>

    <div class="container-fluid">

        <div style="position: relative; width: 320px; margin: 45px auto; text-align: center; padding: 20px 0;">
		
            <form>

                <div class="form-group">

                    <div class="input-group" style="background: #373e4a; border: 1px solid #373e4a; padding-top: 6px; padding-bottom: 6px;">
                        <div class="input-group-addon" style="background: #373e4a; border: 1px solid #373e4a; padding-top: 6px; padding-bottom: 6px;">
                            <i class="entypo-user"></i>
                        </div>

                        <input type="text" class="form-control" name="username" id="username1" placeholder="Username" autocomplete="off" style="background: #373e4a; color: white; border: #373e4a; padding-top: 6px; padding-bottom: 6px;" />
                    </div>


                </div>

                <div class="form-group">

                    <div class="input-group" style="background: #373e4a; border: 1px solid #373e4a; padding-top: 6px; padding-bottom: 6px;">
                        <div class="input-group-addon" style="background: #373e4a; border: 1px solid #373e4a; padding-top: 6px; padding-bottom: 6px;">
                            <i class="entypo-key"></i>
                        </div>

                        <input type="password" class="form-control" name="password" id="password1" placeholder="Password" autocomplete="off" style="background: #373e4a; color: white; border: #373e4a; padding-top: 6px; padding-bottom: 6px;" />
                    </div>

                </div>

                <div class="form-group" style="padding-top: 12px; padding-bottom: 12px" onclick="">


                    <input class="btn btn-primary btn-block btn-blue" type="button" onclick="validate()" style="padding: 13px;" value="Log In"/>

                 </div>

                <div class="login-bottom-links" style="font-size: 12px; line-height: 1.42857143; color: #949494; padding-top: 20px; padding-bottom: 30px;">
                    <a href="#" class="link" style="font-size: 12px">Ecode Networks</a>  - <a href="#" class="link" style="font-size: 12px">Privacy Policy</a>
                </div>
                
                    

                
            </form>

        </div>
    </div>



    <!-- Bottom Scripts -->
    
    <script src="assets/js/gsap/main-gsap.js"></script>
    <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/neon-api.js"></script>
    <script src="assets/js/neon-chat.js"></script>
    <script src="assets/js/neon-custom.js"></script>
    <script src="assets/js/neon-demo.js"></script>

</body>
</html>
