<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="com.validate.*"%>
<%@ page language="java" import="com.ecode.keygenerate.Lockdownkey"%>

<%


String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	String tl=Lockdownusr.getLockdown();
	System.out.println(tl);
	if(tl.equalsIgnoreCase("open")){
	}
	else{
		String redirectURL = "lock.jsp";
		 response.sendRedirect(redirectURL);
	}
}
else{
 String redirectURL = "Login.jsp";
 response.sendRedirect(redirectURL);
}

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>Evolve</title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	
    <script src="zoomcharts-zoomcharts/lib/zoomcharts-dev.js"></script>
    <script src="zoomcharts-zoomcharts/lib/zoomcharts.js"></script>
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="info.js"></script>
	
	<style>
	    .spn1 {
            text-align:center;
            font-size:33px;
	    }
      
	</style>
	
</head>
<body class="page-body" data-url="http://neon.dev">
   
    <div class="page-container">
        
        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width: 60px; height: 50px;" />

                        <span>Welcome<strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>

            <!--  <div class="sidebar-user-info">

               <div class="sui-normal">
                    <a href="#" class="user-link">
                        <img src="assets/images/729ebe0c689023040b7eb03fe48e7792.png" alt="" class="img-circle" width="60" height="10"/>

                        <span>Welcome,</span>
                        <strong>Admin</strong>
                    </a>
                </div>
                   <div class="sui-hover inline-links animate-in">
                    <!-- You can remove "inline-links" class to make links appear vertically, class "animate-in" will make A elements animateable when click on user profile 
                    <a href="#">
                        <i class="entypo-pencil"></i>
                        New Page
                    </a>

                    <a href="mailbox.html">
                        <i class="entypo-mail"></i>
                        Inbox
                    </a>

                    <a href="extra-lockscreen.html">
                        <i class="entypo-lock"></i>
                        Log Off
                    </a>

                    <span class="close-sui-popup">&times;</span><!-- this is mandatory 
                </div>
            </div>-->




            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>

                <li class="opened">
                    <a href="Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="Inventory.jsp">
                                <span>Inventory</span>
                            </a>
                        </li>
                         
                    </ul>
                </li>
                
                <li class="opened">
                    <a href="L2Topology.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        
                        <li>
                            <a href="L2Topology.jsp">
                                <span>Viewer</span>
                            </a>
                            <ul>
                        <li>
                            <a href="L2Topology.jsp">
                                <span>L2 Topology</span>
                            </a>
                        </li>
                       
                        <li>
                            <a href="L3Topology.jsp">
                                <span>L3 Topology</span>
                            </a>
                        </li>
                        
                          <li>
                            <a href="${pageContext.request.contextPath}/ecode-data/findL2Path">
                                <span>Path Detection</span>
                            </a>
                        </li>
                        
                       
                    </ul>
                        </li>
  
                    </ul>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/ecode-data/services">
                        <i class="entypo-cog"></i>
                        <span>Service Chaining</span>
                    </a>

                </li>
                
                <li>
                    <a href="License.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>License</span>
                    </a>


                </li>
            </ul>



        </div>
        <div class="main-content">

            <br />
            <div class="row">

                <!-- Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
		
		<ul class="user-info pull-left pull-right-xs pull-none-xsm">
			
			<!-- Information Notifications -->
			<li class="notifications dropdown">
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-info-circled"></i>
					<span class="badge badge-info">6</span>
				</a>
				<script>

				    var jsonData = JSON.parse(data);
				    for (var i = 0; i < jsonData.counters.length; i++) {
				        var counter = jsonData.counters[i];
				        //alert(counter.info);
				        //alert(counter.time);
				    }
                    </script>
                <ul class="dropdown-menu">
					<li class="top">
	<p class="small">
		
		You have <strong>3</strong> new notifications.
	</p>
</li>

<li>
	<ul class="dropdown-menu-list scroller"> <li>
        <script>


            for (var i = 0; i < jsonData.counters.length; i++) {
                var counter = jsonData.counters[i];
                document.write("<li class=\"unread notification-success\">");
                document.write("			<a href=\"#\">");
                document.write("				<i class=\"entypo-user-add pull-right\"><\/i>");
                document.write("				");
                document.write("				<span class=\"line\">");
                document.write(counter.info);
                document.write("				<\/span>");
                document.write("				");
                document.write("				<span class=\"line small\">");
                document.write(counter.time);
                document.write("				<\/span>");
                document.write("			<\/a>");
                document.write("		<\/li>");
            }
        </script></li>
	</ul>
</li>
</ul>
</li>

<!-- Warning Notifications -->
				<li class="notifications dropdown">
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-attention"></i>
					<span class="badge badge-warning"><script>
					                                      var jsonData = JSON.parse(data);
					                                      document.write(jsonData.counters.length);
			    </script></span> 
				</a>
				<script>

				    var jsonData = JSON.parse(data);
				    for (var i = 0; i < jsonData.counters.length; i++) {
				        var counter = jsonData.counters[i];
				        //alert(counter.info);
				        //alert(counter.time);
				    }
                    </script>
                <ul class="dropdown-menu">
					<li class="top">
	<p class="small">
		You have <strong>3</strong> new notifications.
	</p>
</li>

<li>
	<ul class="dropdown-menu-list scroller"> <li>
        <script>


            for (var i = 0; i < jsonData.counters.length; i++) {
                var counter = jsonData.counters[i];
                document.write("<li class=\"unread notification-success\">");
                document.write("			<a href=\"#\">");
                document.write("				<i class=\"entypo-user-add pull-right\"><\/i>");
                document.write("				");
                document.write("				<span class=\"line\">");
                document.write(counter.info);
                document.write("				<\/span>");
                document.write("				");
                document.write("				<span class=\"line small\">");
                document.write(counter.time);
                document.write("				<\/span>");
                document.write("			<\/a>");
                document.write("		<\/li>");
            }
        </script></li>
	</ul>
</li>
</ul>
</li>
			
			<!-- Error Notifications -->
			<li class="notifications dropdown">
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<i class="entypo-alert"></i>
					<span class="badge badge-danger">3</span>
				</a>
				
				<ul class="dropdown-menu">
					<li class="top">
	<p>You have 6 pending tasks</p>
</li>

<li>
	<ul class="dropdown-menu-list scroller">
		<li>
			<a href="#">
				<span class="task">
					<span class="desc">Procurement</span>
					<span class="percent">27%</span>
				</span>
			
				<span class="progress">
					<span style="width: 27%;" class="progress-bar progress-bar-success">
						<span class="sr-only">27% Complete</span>
					</span>
				</span>
			</a>
		</li>
		<li>
			<a href="#">
				<span class="task">
					<span class="desc">App Development</span>
					<span class="percent">83%</span>
				</span>
				
				<span class="progress progress-striped">
					<span style="width: 83%;" class="progress-bar progress-bar-danger">
						<span class="sr-only">83% Complete</span>
					</span>
				</span>
			</a>
		</li>
		<li>
			<a href="#">
				<span class="task">
					<span class="desc">HTML Slicing</span>
					<span class="percent">91%</span>
				</span>
				
				<span class="progress">
					<span style="width: 91%;" class="progress-bar progress-bar-success">
						<span class="sr-only">91% Complete</span>
					</span>
				</span>
			</a>
		</li>
		<li>
			<a href="#">
				<span class="task">
					<span class="desc">Database Repair</span>
					<span class="percent">12%</span>
				</span>
				
				<span class="progress progress-striped">
					<span style="width: 12%;" class="progress-bar progress-bar-warning">
						<span class="sr-only">12% Complete</span>
					</span>
				</span>
			</a>
		</li>
		<li>
			<a href="#">
				<span class="task">
					<span class="desc">Backup Create Progress</span>
					<span class="percent">54%</span>
				</span>
				
				<span class="progress progress-striped">
					<span style="width: 54%;" class="progress-bar progress-bar-info">
						<span class="sr-only">54% Complete</span>
					</span>
				</span>
			</a>
		</li>
		<li>
			<a href="#">
				<span class="task">
					<span class="desc">Upgrade Progress</span>
					<span class="percent">17%</span>
				</span>
				
				<span class="progress progress-striped">
					<span style="width: 17%;" class="progress-bar progress-bar-important">
						<span class="sr-only">17% Complete</span>
					</span>
				</span>
			</a>
		</li>
	</ul>
</li>

<li class="external">
	<a href="#">See all tasks</a>
</li>				</ul>
				
			</li>
		
		</ul>
	
	</div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                         <li>
                          <a href="Dashboard.html"> <strong>Ecode Networks</strong><i class="entypo-users"></i></a>  
                        </li>
                         <li class="sep"></li>
                         <li>
                        <a href="#" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"><strong>IP Settings</strong><i class="entypo-cog"></i></a>
                            
                        </li>
                  
                        <li class="sep"></li>

                        <li>
                            <a href="setout.jsp"><strong>Log Out</strong><i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>
                   

                   
                 
            </div>
                </div>
                <script type="text/javascript">
                    jQuery(document).ready(function ($) {
                        $('.pie').sparkline('html', {
                            type: 'pie',
                            borderWidth: 0,
                            sliceColors: ['#3d4554', '#ee4749', '#00b19d']
                        });


                        $(".chart").sparkline([1, 2, 3, 1], {
                            type: 'pie',
                            barColor: '#485671',
                            height: '110px',
                            barWidth: 10,
                            barSpacing: 2
                        });

                        // Rickshaw
                        var seriesData = [[], []];

                        var random = new Rickshaw.Fixtures.RandomData(90);

                        for (var i = 0; i < 150; i++) {
                            random.addData(seriesData);
                        }

                        var graph = new Rickshaw.Graph({
                            element: document.getElementById("rickshaw-chart-demo-2"),
                            height: 217,
                            renderer: 'area',
                            stroke: false,
                            preserve: true,
                            series: [{
                                color: '#359ade',
                                data: seriesData[0],
                                name: 'ICMP'
                            }, {
                                color: '#73c8ff',
                                data: seriesData[1],
                                name: 'TCP'
                            }, {
                                color: '#e0f2ff',
                                data: seriesData[1],
                                name: 'UDP'
                            }
                            ]
                        });

                        graph.render();

                        var hoverDetail = new Rickshaw.Graph.HoverDetail({
                            graph: graph,
                            xFormatter: function (x) {
                                return new Date(x * 1000).toString();
                            }
                        });

                        var legend = new Rickshaw.Graph.Legend({
                            graph: graph,
                            element: document.getElementById('rickshaw-legend')
                        });

                        var highlighter = new Rickshaw.Graph.Behavior.Series.Highlight({
                            graph: graph,
                            legend: legend
                        });

                        setInterval(function () {
                            random.removeData(seriesData);
                            random.addData(seriesData);
                            graph.update();

                        }, 500);

                    });
</script>
<br />
         <div class="row">
       
        <div class="col-sm-3">
	
		<div class="tile-title tile-cyan">
			
			<div class="icon">
				<p id="demo1"><i class="glyphicon"><span class="spn1">10</span></i></p>
               
			</div>
			
			<div class="title">
				<h3>Active Nodes</h3>
				<br />
			</div>
		</div>
		</div>
       <div class="col-sm-3">
	
		<div class="tile-title tile-blue">
			
			<div class="icon">
			<p id="demo2"><i class="glyphicon"><span class="spn1">12</span></i></p>
                 
			</div>
			
			<div class="title">
				<h3>Total Nodes</h3>
				<br />
			</div>
		</div>
		
	</div>
	<div class="col-sm-3">
	
		<div class="tile-title tile-red">
			
			<div class="icon">
			<p id="demo3">	<i class="glyphicon"><span class="spn1">10</span></i></p>
                 
			</div>
			
			<div class="title">
				<h3>Total Errors</h3>
				<br />
			</div>
		</div>
		
	</div>
        <div class="col-sm-3">
	
		<div class="tile-orange tile-title">
			
			<div class="icon">
				<p id="demo4"><i class="glyphicon"><span class="spn1">18</span></i></p>
                 
			</div>
			
			<div class="title">
				<h3>Nodes to be provisioned</h3>
				<br />
			</div>
		</div>
		
	</div>
</div>


           
 <div class="row">
           <div class="col-md-6">
                    <div class="map">
                        <div class="panel panel-success" data-collapsed="0">
                            <!-- to apply shadow add class "panel-shadow" -->
                            <!-- panel head -->
                            <div class="panel-heading">
                                <div class="panel-title"><h4>Device Locator<br /><small>Shows VPOP-UPS</small></h4></div>

                                <div class="panel-options">
                                    <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-3" class="bg"><i class="entypo-cog"></i></a>
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                    <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                                </div>
                            </div>

                            <!-- panel body -->
                            <div class="panel-body no-padding">

                                <div id="map" style="height:320px; width: 100%;" class="map"></div>
                                    
    
<script>

    // change radius

    var options = {
        height: 320,
        background: {
            url: "https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
            params: {
                subdomains: ["a", "b", "c", "d"],
                //attribution: "© OpenStreetMap contributors, © CartoDB"
            }
         },
        container: $("#map")[0],
        data: {
            preloaded: {
                nodes: [
                    {
                        id: "California",
                        // long, lat
                        loaded: true,
                        coordinates: [-120.0000, 37.0000],
                        title: "VPOP-P-CAL-001",
                        description: "54.164.108.211"
                    },
                    {
                        id: "Oregon",
                        loaded: true,
                        coordinates: [-120.5000, 44.0000],
                        title: "VPOP-P-ORE-001",
                        description: "54.68.200.61"
                    },
                    {
                        id: "Virginia",
                        loaded: true,
                        coordinates: [-79.0000, 37.5000],
                        title: " VPOP -P-VRG-001",
                        description: "54.68.51.106"
                    },
                    {
                        id: "Ireland",
                        loaded: true,
                        coordinates: [-6.2675, 53.3442],
                        title: "VPOP-P-IRE-001",
                        description: "54.72.125.101"
                    },
        {
            id: "Germany",
            loaded: true,
            coordinates: [9.9167, 51.5167],
            title: "VPOP-P-FRK-001",
            description: "54.77.198.14"
        }
                ],
                links: [
                      { from: "California", to: "Oregon", Coordinates: [-120.0000, 37.0000, -120.5000, 44.0000] },
                       { from: "California", to: "Virginia", Coordinates: [-120.0000, 37.0000, -79.0000, 37.5000] },
                       { from: "Oregon", to: "Ireland", Coordinates: [-120.5000, 44.0000, -6.2675, 53.3442] },
                        { from: "Ireland", to: "Germany", Coordinates: [-6.2675, 53.3442, -13.3833, 52.5167] },
                         { from: "Germany", to: "Virginia", Coordinates: [-13.3833, 52.5167, -79.0000, 37.5000] }
                ]
            }
        },

        layers: [
            {
                name: "Points",
                type: "items",
                style: {
                    nodeRules: { "rule0": nodeStyle },

                    linkRules: {
                        "0": function (link) {
                            //  link.label = link.data.drivingTime
                        }
                    },
                    nodeLabel: {
                        backgroundStyle: {
                            fillColor: "black"
                        },
                        textStyle: {
                            fillColor: "white"
                        }
                    },
                    linkLabel: {
                        backgroundStyle: {
                            fillColor: "rgba(0, 0, 0, 0.6)"
                        },
                        textStyle: {
                            fillColor: "white"
                        }
                    }
                }
            }],
        info: {
            enabled: true,
            nodeContentsFunction: function (itemData, item) {
                return "<div style='margin:auto; width:180px; height:100%; padding': 10px;>" +
                              "<h4 style='font-weight: 250; font-size: 17px; color:blue; padding-bottom: 3px; margin:0px; padding:10px'>" + itemData.title + "</h4>" +
                              "<h5 style='font-size: 13px;font-family: Arial, Helvetica, sans-serif;font-weight: 300;padding:5px'>" + itemData.description + "</h5>" +
                         "</div>";
            }
        },
        navigation: {
            initialLat: 35.04409,
            initialLng: -90.246213,
            initialZoom: 3,
            minZoom: 2
        },
        events: {
            onHoverChange: function (event) {
                if (event.hoverNode) nodeStyle(node)
            }
        }
    }
    function nodeStyle(node) {

        node.label = node.data.id,
        node.fillColor = "#0099cc",
                           node.lineColor = "#0077aa",
                           node.lineWidth = 2,
                          node.image = "assets/images/Router-icon.png";
    }


    function linkStyle(link) {
        link.label = "Link";
        link.labelStyle = { textStyle: { fillColor: "red", font: "18px arial" } }
    }

    $(document).ready(function () {
        chart = new GeoChart(options);
    });

</script>



                            </div>

                        </div>

                    </div>


               </div>

          

           <div class="col-md-6">
                    <div class="real">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="panel-title">
                                    <h4>Real Time Stats
						<br />
                                        <small>Current Server Uptime</small>
                                    </h4>
                                </div>

                                <div class="panel-options">
                                    <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                    <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                    <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                                </div>
                            </div>

                            <div class="panel-body no-padding">
                                <div id="rickshaw-chart-demo-2">
                                    <div id="rickshaw-legend"></div>
                                </div>
                            </div>
                        </div>
                   </div>
                 </div>
                 </div>
	
                <div class="row">
                    <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="panel-title panel-heading">Priority Logs</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <table class="table table-bordered table-responsive">
                            <thead>

                                <tr>

                                    <th>Date</th>

                                    <th>Origin</th>

                                    <th>Message</th>

                                    <th>Priority</th>

                                    <th>Action</th>
                                    
                                </tr>

                            </thead>

                            <tbody>

                                <tr class="active">

                                    <td>29/May/2015 08:29:45</td>

                                    <td>115.112.82.196</td>

                                    <td>request for connection parameters from user id 00a58f19</td>
                                    <td>1</td>
                                    <td>None</td>
                                    
                                </tr>

                                <tr class="success">

                                    <td>29/May/2015 08:29:45</td>

                                    <td>115.112.82.196</td>

                                    <td>VPOP allocated for the user 00a58f19 is 54_77_198_142</td>
                                    <td>2</td>
                                    <td>None</td>
                                    
                                </tr>

                                <tr class="danger">

                                    <td>29/May/2015 08:29:45</td>

                                    <td>49.205.133.173</td>

                                    <td>User 00b59r20 failed connect to net storage</td>
                                    <td>3</td>
                                    <td>Pending</td>
                                    
                                </tr>

                                <tr class="warning">

                                    <td>29/May/2015 08:29:45</td>

                                    <td>49.205.133.173</td>

                                    <td>User 00b59r20 is not authorized to access net storage</td>
                                    <td>4</td>
                                    <td>Pending</td>
                                    
                                </tr>

                                <tr class="info">

                                    <td>29/May/2015 08:29:45</td>

                                    <td>49.205.133.173</td>

                                    <td>User 00b59r20 connected to VPOP 54_77_198_142</td>
                                    <td>5</td>
                                    <td>None</td>
                                    
                                </tr>

                            </tbody>
                        </table>
                    </div>
</div>
                </div>

                <footer class="main" style="text-align:center;">
                    Copyright &copy; 2015 <strong>Ecode Networks, </strong>All Rights Reservered

	
                </footer>
            </div>
            </div>

    <div class="modal fade" id="modal-6">
	<div class="modal-dialog" style="width:400px !important;margin:2% 45%">
		<div class="modal-content">
			
			<div class="modal-header"  style="text-align:center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title"><strong>Set IP</strong></h3>
			</div>
			
			<div class="modal-body">
			
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Controller IP</label>
							
							<input type="text" class="form-control" id="CIP" placeholder="Enter Controller IP">
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Evolve IP</label>
							
							<input type="text" class="form-control" id="EIP" placeholder="Enter Evolve IP">
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			
                <div class="row" style="text-align:center">
				
				<button type="button" class="btn btn-info"> Change IP</button>&nbsp &nbsp &nbsp
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
			</div>
			
			</div>
		</div>
	</div>

  
    <link rel="stylesheet" href="assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
	<link rel="stylesheet" href="assets/js/rickshaw/rickshaw.min.css">

	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="assets/js/jquery.sparkline.min.js"></script>
	<script src="assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="assets/js/neon-chat.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>
</body>
</html>

   


