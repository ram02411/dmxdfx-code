<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="com.validate.*"%>


<%


  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	
}
else{
 String redirectURL = "Login.jsp";
 response.sendRedirect(redirectURL);
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>Evolve</title>

    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <link href="jquery.wizard.css" rel="stylesheet" />
    <script src="jquery.wizard.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <style>
        
      @import "//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css";
      @import "http://fonts.googleapis.com/css?family=Roboto:400,500";

.box > .icon { text-align: center; position: relative; }
.box > .icon > .image { position: relative; z-index: 2; margin: auto; width: 88px; height: 88px; border: 8px solid white; line-height: 88px; border-radius: 50%; background: #63B76C; vertical-align: middle; }
.box > .icon:hover > .image { background: #333; }
.box > .icon > .image > i { font-size: 36px !important; color: #fff !important; }
.box > .icon:hover > .image > i { color: white !important; }
.box > .icon > .info { margin-top: -24px; background: rgba(0, 0, 0, 0.04); border: 1px solid #e0e0e0; padding: 15px 0 10px 0; }
.box > .icon:hover > .info { background: rgba(0, 0, 0, 0.04); border-color: #e0e0e0; color: white; }
.box > .icon > .info > h3.title { font-family: "Roboto",sans-serif !important; font-size: 16px; color: #222; font-weight: 500; }
.box > .icon > .info > p { font-family: "Roboto",sans-serif !important; font-size: 13px; color: #666; line-height: 1.5em; margin: 20px;}
.box > .icon:hover > .info > h3.title, .box > .icon:hover > .info > p, .box > .icon:hover > .info > .more > a { color: #222; }
.box > .icon > .info > .more a { font-family: "Roboto",sans-serif !important; font-size: 12px; color: #222; line-height: 12px; text-transform: uppercase; text-decoration: none; }
.box > .icon:hover > .info > .more > a { color: #fff; padding: 6px 8px; background-color: #63B76C; }
.box .space { height: 30px; }

       .sidebar-nav {
        padding: 9px 0;
      }

	  [data-wizard-init] {
		margin: auto;
		width: 600px;
	  }
    </style>
    
</head>

<body class="page-body">

    <div class="page-container">

        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width: 57px; height: 53px;" />

                        <span style="margin-left: 8px">Welcome<strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>



            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                       <li class="opened">
                    <a href="Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="Inventory.jsp">
                                <span>Inventory</span>
                            </a>
                        </li>
                         
                    </ul>
                </li>
                
                <li class="opened">
                    <a href="L2Topology.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        
                        <li>
                            <a href="L2Topology.jsp">
                                <span>Viewer</span>
                            </a>
                            <ul>
                        <li>
                            <a href="L2Topology.jsp">
                                <span>L2 Topology</span>
                            </a>
                        </li>
                        <li>
                            <a href="L3Topology.jsp">
                                <span>L3 Topology</span>
                            </a>
                        </li>
                        
                        
                          <li>
                            <a href="${pageContext.request.contextPath}/ecode-data/findL2Path">
                                <span>Path Detection</span>
                            </a>
                        </li>
                        
                       
                    </ul>
                        </li>
  
                    </ul>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/ecode-data/services">
                        <i class="entypo-cog"></i>
                        <span>Service Chaining</span>
                    </a>

                </li>
                
                <li>
                    <a href="License.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>License</span>
                    </a>


                </li>
               
            </ul>



        </div>
        <div class="main-content">

            <div class="row">
                <div class="col-md-6 col-sm-8 clearfix"></div>
                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <strong>Ecode Networks</strong> <i class="entypo-users"></i>

                        </li>


                        <li class="sep"></li>

                        <li>
                            <a href="setout.jsp">Log Out <i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>




                </div>
            </div>
            
            
             <div class="container-fluid">
		         <div data-wizard-init>
		  <ul class="steps">
			<li data-step="1">Create Licence Key</li>
			<li data-step="2">Verify Licence Key</li>
			
		</ul>
		  <div class="steps-content">
			<div data-step="1">
			  
                <div class="box">							
				<div class="icon">
					<div class="image"><i class="fa fa-key"></i></div>
					<div class="info">
						<h3 class="title">Generate Your Licence Key</h3>
    					<p>
							Click on the below Create Key button to generate a Licence Key.
						</p>

						<div class="more " style="text-align:center">
							<a href="#" title="Click to Create Key" onclick="showDiv()">
								Create Key <i class="fa fa-gear"></i>
							</a>
                            </div>

                        <div id="welcomeDiv" style="display:none; margin-top:20px; margin-bottom:20px; color:black" class="container-fluid">
                            <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                            <div class="input-group">
                             <span class="input-group-addon" id="basic-addon1">Verification Code</span>
                              <input type="text" id="keyid" class="form-control" aria-describedby="basic-addon1">
                          </div></div>
                            <div class="col-md-2"></div>
                                </div>
                            <div class="row" style="margin-top:20px">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                            <div class="input-group">
                             <span class="input-group-addon" id="Span2"></span>
                             
                          </div></div>
                            <div class="col-md-2"></div>
                                </div>
                        </div>
					</div>
				</div>
				<script>
                function showDiv() {
                	$.ajax({ 
						url: 'getkey.jsp',
						type: 'GET',
						dataType: 'text',
						async: false,						
						success: function(data) {
							id=data;
							document.getElementById("keyid").value=id;
						}
					});
                	var emaildata="<a class='active' href='mailto:license@ecodenetworks.com?subject=Request for License&body=Evolve Id is "+
					id+"'>Email license@ecodenetworks.com</a>";
					$("#Span2").html(emaildata);
                    document.getElementById('welcomeDiv').style.display = "block";
                }
            </script>
		
                <div class="space"></div>
			</div>
			</div>
			<div data-step="2">
			  <div class="box">							
				<div class="icon">
					<div class="image"><i class="fa fa-check-circle"></i></div>
					<div class="info">
						<h3 class="title">Verify Your Licence Key</h3>
    					<p>
							Please Enter the your Licence Key.
						</p>

                        <div style="margin-top:20px; margin-bottom:20px; color:black" class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                            <div class="input-group">
                             <span class="input-group-addon" id="Span1">Licence Verification</span>
                              <input type="text"  id="lkey" class="form-control" placeholder="Enter your Licence Key" aria-describedby="basic-addon1">
                          </div></div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="more " style="text-align:center">
							<a href="#" title="Click to Create Key" onClick="keyverification()">
								<i class="fa fa-check"></i> Verify Key 
							</a>
                            </div>

					</div>
				</div>
				 <script>
					var count=2;
                    function keyverification() {
                        var key1 = document.getElementById('lkey').value;
                        $.ajax({ 
    						url: 'verifykey.jsp',
    						type: 'GET',
    						dataType: 'text',
    						data: {key: key1},
    						async: false,						
    						success: function(data) {
    							var value=data;
    							if(value==1){
    								alert("License is verified successfully");
    								
    							}
    							else{
    								var t = " tries";
    								if (count == 1) {
    									t = " try"
    								}

    								if (count >= 1) {
    									alert("Invalid License key.  You have " + count + t
    											+ " left.");
    									document.getElementById('lkey').value = "";
    									
    									count--;
    								}

    								else {
    									alert("Still incorrect! You have no more tries left!");
    									document.getElementById('lkey').value = "No more tries allowed!";
    									
    									return false;
    								}
    					}
    						}
    					});
                    }
                    
                    

                </script>
				
       
                <div class="space"></div>
			</div>
			</div>
			
		  </div>
		</div>
    </div>
        
        </div>
        </div>
  
    <link rel="stylesheet" href="assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="assets/js/rickshaw/rickshaw.min.css">

    <!-- Bottom Scripts -->
    <script src="assets/js/gsap/main-gsap.js"></script>
    <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/neon-api.js"></script>
    <script src="assets/js/neon-chat.js"></script>
    <script src="assets/js/neon-custom.js"></script>
    <script src="assets/js/neon-demo.js"></script>

</body>

</html>



 
