<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
    <%@ page language="java" import="com.validate.*"%>


<%


  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	
}
else{
 String redirectURL = "Login.jsp";
 response.sendRedirect(redirectURL);
}

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Evolve Panel" />
    <meta name="author" content="" />

    <title>Evolve</title>


    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="zoomcharts-zoomcharts/lib/zoomcharts.js"></script>
 
    <style>
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
            background-color: #D4E0E0;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #E5EEEE;
        }
        .modal-dialog {
            width:500px !important;
            margin:0% 41% !important;
        }
   
    </style>
</head>


<body  class="page-body" data-url="http://ecodenetworks.com">

    <div class="page-container">
      
        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                   <a href="#" class="user-link" style="margin-top:-5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width:57px;height:53px;"/>

                        <span style="margin-left:8px">Welcome<strong>Admin</strong></span>
                       
                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>

            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                 <li class="opened">
                    <a href="Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="Inventory.jsp">
                                <span>Inventory</span>
                            </a>
                        </li>
                         
                    </ul>
                </li>
                
                <li class="opened">
                    <a href="L2Topology.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        
                        <li>
                            <a href="L2Topology.jsp">
                                <span>Viewer</span>
                            </a>
                            <ul>
                        <li>
                            <a href="L2Topology.jsp">
                                <span>L2 Topology</span>
                            </a>
                        </li>
                        <li>
                            <a href="L3Topology.jsp">
                                <span>L3 Topology</span>
                            </a>
                        </li>
                        
                        
                          <li>
                            <a href="${pageContext.request.contextPath}/ecode-data/findL2Path">
                                <span>Path Detection</span>
                            </a>
                        </li>
                        
                       
                    </ul>
                        </li>
  
                    </ul>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/ecode-data/services">
                        <i class="entypo-cog"></i>
                        <span>Service Chaining</span>
                    </a>

                </li>
                
                <li>
                    <a href="License.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>License</span>
                    </a>


                </li>
               
            </ul>



        </div>

        <div class="main-content">

            <div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    
                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <a><strong>Ecode Networks</strong> <i class="entypo-users"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});">
                            <strong>Settings</strong> <i class="entypo-cog"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-5').modal('show', {backdrop: 'static'});">
                            <strong>Upload</strong> <i class="entypo-upload"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                            <a href="setout.jsp">
                                <strong>LogOut</strong>  <i class="entypo-logout right"></i></a>
                        </li>
                    </ul>
                </div>

            </div>

            <hr />

            <div class="row">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="panel-title">Inventory</div>

                        <div class="panel-options">
                            <a href="#" onclick="jQuery('#modal-6').modal('show', {backdrop: 'static'});"><i class="bg"><i class="entypo-cog"></i></i></a>
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <table class="table table-bordered responsive table-hover table-striped datatable" id="table-1">
                        <thead>
                            <tr>
                                <th width="10%">&nbsp;</th>
                                <th width="10%">ID</th>
                                <th width="10%">Name</th>
                                <th width="10%">Hardware</th>
                                <th width="10%">Serial-Number</th>
                                <th width="10%">Software</th>
                                <th width="10%">IP Address</th>
                                <th width="10%">Manufacturer</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                            <tr>
                               <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                           </tbody>
                    </table>
                    
                    <div class="col-md-12 text-center">
                        <ul class="pagination pagination-lg pager" id="myPager"></ul>
                    </div>
                    <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-danger btn-grey" onclick="redirect();">Topology Viewer</button>
                </div>
				 </div>
                <script>
                
                function redirect(){
                	window.location.href = "L2Topology.html";
                }
                    $.fn.pageMe = function (opts) {
                        var $this = this,
                            defaults = {
                                perPage: 7,
                                showPrevNext: false,
                                hidePageNumbers: false
                            },
                            settings = $.extend(defaults, opts);

                        var listElement = $this;
                        var perPage = settings.perPage;
                        var children = listElement.children();
                        var pager = $('.pager');

                        if (typeof settings.childSelector != "undefined") {
                            children = listElement.find(settings.childSelector);
                        }window

                        if (typeof settings.pagerSelector != "undefined") {
                            pager = $(settings.pagerSelector);
                        }

                        var numItems = children.size();
                        var numPages = Math.ceil(numItems / perPage);

                        pager.data("curr", 0);

                        if (settings.showPrevNext) {
                            $('<li><a href="#" class="prev_link"> <i class="entypo-left-open-mini"></i> </a></li>').appendTo(pager);
                        }

                        var curr = 0;
                        while (numPages > curr && (settings.hidePageNumbers == false)) {
                            $('<li><a href="#" class="page_link">' + (curr + 1) + '</a></li>').appendTo(pager);
                            curr++;
                        }

                        if (settings.showPrevNext) {
                            $('<li><a href="#" class="next_link"><i class="entypo-right-open-mini"></a></li>').appendTo(pager);
                        }

                        pager.find('.page_link:first').addClass('active');
                        pager.find('.prev_link').hide();
                        if (numPages <= 1) {
                            pager.find('.next_link').hide();
                        }
                        pager.children().eq(1).addClass("active");

                        children.hide();
                        children.slice(0, perPage).show();

                        pager.find('li .page_link').click(function () {
                            var clickedPage = $(this).html().valueOf() - 1;
                            goTo(clickedPage, perPage);
                            return false;
                        });
                        pager.find('li .prev_link').click(function () {
                            previous();
                            return false;
                        });
                        pager.find('li .next_link').click(function () {
                            next();
                            return false;
                        });

                        function previous() {
                            var goToPage = parseInt(pager.data("curr")) - 1;
                            goTo(goToPage);
                        }

                        function next() {
                            goToPage = parseInt(pager.data("curr")) + 1;
                            goTo(goToPage);
                        }

                        function goTo(page) {
                            var startAt = page * perPage,
                                endOn = startAt + perPage;

                            children.css('display', 'none').slice(startAt, endOn).show();

                            if (page >= 1) {
                                pager.find('.prev_link').show();
                            }
                            else {
                                pager.find('.prev_link').hide();
                            }

                            if (page < (numPages - 1)) {
                                pager.find('.next_link').show();
                            }
                            else {
                                pager.find('.next_link').hide();
                            }

                            pager.data("curr", page);
                            pager.children().removeClass("active");
                            pager.children().eq(page + 1).addClass("active");

                        }
                    };

                    $(document).ready(function () {

                        $('#myTable').pageMe({ pagerSelector: '#myPager', showPrevNext: true, hidePageNumbers: false, perPage: 10 });

                    });
                </script>
            </div>
            <!-- Footer -->
            <footer class="main">
                Copyright &copy; 2015 <strong>Ecode Networks,</strong>All Rights Reservered

	
            </footer>
        </div>


    </div>


    <div class="modal fade" id="modal-6">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"  style="text-align:center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title"><strong>Add a device</strong></h3>
			</div>
			
			<div class="modal-body">
               
                <form role="form" id="Form1">
			    <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Host Name</label>
							
							<input type="text" class="form-control" id="host" placeholder="Enter Host Name">
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
			
				<div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">IP Address</label>
							
							<input type="text" class="form-control" id="ip" data-validate="required" placeholder="Enter IP Address">
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
			
                <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">User Name</label>
							
							<input type="text" class="form-control" id="un" data-validate="required" placeholder="Enter UserName">
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>

                <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Password</label>
							
							<input type="password" class="form-control" id="pass" data-validate="required" placeholder="Enter Password">
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
                <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">SSH Key</label>
								<input type="file" class="form-control" id="file" accept='text/plain' onchange='openFile(event)'>
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
                <div class="roform-group"  style="text-align:center">
               <button type="button" class="btn btn-danger btn-grey" data-dismiss="modal">Close</button>&nbsp &nbsp
               <button type="submit" class="btn btn-info btn-grey" onclick="submt()">Submit</button>&nbsp &nbsp
               <button type="reset" class="btn btn-info btn-grey">Reset</button>
           </div>
</form>
			</div>
			
                <script>
                var text;
                var openFile = function(event) {
                    var input = event.target;

                    var reader = new FileReader();
                    reader.onload = function(){
                       text = reader.result;
                       
                      //alert(text.length);               
                      
                      
                    };
                    reader.readAsText(input.files[0]);
                  };
               
                var message;
                
                    function submt() {
                      var host = document.getElementById("host").value;
                        var ip = document.getElementById("ip").value;
                        var un = document.getElementById("un").value;
                        var file = document.getElementById("file").value;
                        var pass = document.getElementById("pass").value;
                       message=host+"-"+ip+"-"+un+"-"+pass+"-"+text;
                       	//alert(message);
                        if (host === '' || ip === '' || un === '' || file === '' || pass === '') {
                            alert("Please fill all fields...!!!!!!");
                        }
                        else{
                        
                        alert(" host : " + host + " \n ip : " + ip + " \n un : " + un + "\n\n Form Submitted Successfully......");
                        document.getElementById("Form1").submit();
                        
                        }
                    }
   
                  </script>

                
		</div>
	</div>
</div>

 <div class="modal fade" id="modal-5">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header"  style="text-align:center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title"><strong>Upload</strong></h3>
			</div>
			
			<div class="modal-body">
               
                

<div class="row">
                    <div class="col-md-3"></div>
     <div class="col-md-6">
      
      <div class="form-group" id="drop">
       <label for="field-1" class="control-label">Upload File</label>
        
<input type="file" name="xlfile" id="xlf" class="form-control"  /> 

      </div> 
      
     </div>
     <div class="col-md-3"></div>
    </div>

                
<div class="roform-group"  style="text-align:center">
               <button type="button" class="btn btn-info btn-grey" onclick="upload1();b64it();" data-dismiss="modal">Upload</button>
               <button type="button" class="btn btn-danger btn-grey" data-dismiss="modal">Close</button>
</div>
<select name="format" style="display:none">
<option value="csv" selected> CSV</option>
</select>
<input type="checkbox" name="useworker" checked style="visibility:hidden">
<input type="checkbox" name="xferable" checked style="visibility:hidden">
<input type="checkbox" name="userabs" checked style="visibility:hidden">


<!-- uncomment the next line here and in xlsxworker.js for encoding support -->
<!--<script src="dist/cpexcel.js"></script>-->
<script src="shim.js"></script>
<script src="jszip.js"></script>
<script src="xlsx.js"></script>
<!-- uncomment the next line here and in xlsxworker.js for ODS support -->
<script src="dist/ods.js"></script>
<script>
var X = XLSX;
var XW = {
	/* worker message */
	msg: 'xlsx',
	/* worker scripts */
	rABS: './xlsxworker2.js',
	norABS: './xlsxworker1.js',
	noxfer: './xlsxworker.js'
};

var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" && typeof FileReader.prototype.readAsBinaryString !== "undefined";

var use_worker = typeof Worker !== 'undefined';

var transferable = use_worker;

var wtf_mode = false;

function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint8Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint8Array(data.slice(l*w)));
	return o;
}

function ab2str(data) {
	var o = "", l = 0, w = 10240;
	for(; l<data.byteLength/w; ++l) o+=String.fromCharCode.apply(null,new Uint16Array(data.slice(l*w,l*w+w)));
	o+=String.fromCharCode.apply(null, new Uint16Array(data.slice(l*w)));
	return o;
}

function s2ab(s) {
	var b = new ArrayBuffer(s.length*2), v = new Uint16Array(b);
	for (var i=0; i != s.length; ++i) v[i] = s.charCodeAt(i);
	return [v, b];
}

function xw_noxfer(data, cb) {
	var worker = new Worker(XW.noxfer);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			case XW.msg: cb(JSON.parse(e.data.d)); break;
		}
	};
	var arr = rABS ? data : btoa(fixdata(data));
	worker.postMessage({d:arr,b:rABS});
}

function xw_xfer(data, cb) {
	var worker = new Worker(rABS ? XW.rABS : XW.norABS);
	worker.onmessage = function(e) {
		switch(e.data.t) {
			case 'ready': break;
			case 'e': console.error(e.data.d); break;
			default: xx=ab2str(e.data).replace(/\n/g,"\\n").replace(/\r/g,"\\r"); console.log("done"); cb(JSON.parse(xx)); break;
		}
	};
	if(rABS) {
		var val = s2ab(data);
		worker.postMessage(val[1], [val[1]]);
	} else {
		worker.postMessage(data, [data]);
	}
}

function xw(data, cb) {
	transferable = document.getElementsByName("xferable")[0].checked;
	if(transferable) xw_xfer(data, cb);
	else xw_noxfer(data, cb);
}

function get_radio_value( radioName ) {
	var radios = document.getElementsByName( radioName );
	for( var i = 0; i < radios.length; i++ ) {
		if( radios[i].checked || radios.length === 1 ) {
			return radios[i].value;
		}
	}
}

function to_json(workbook) {
	var result = {};
	workbook.SheetNames.forEach(function(sheetName) {
		var roa = X.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
		if(roa.length > 0){
			result[sheetName] = roa;
		}
	});
	return result;
}

function to_csv(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var csv = X.utils.sheet_to_csv(workbook.Sheets[sheetName]);
		if(csv.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(csv);
		}
	});
	return result.join("\n");
}



function to_formulae(workbook) {
	var result = [];
	workbook.SheetNames.forEach(function(sheetName) {
		var formulae = X.utils.get_formulae(workbook.Sheets[sheetName]);
		if(formulae.length > 0){
			result.push("SHEET: " + sheetName);
			result.push("");
			result.push(formulae.join("\n"));
		}
	});
	return result.join("\n");
}

var tarea = document.getElementById('b64data');
function b64it() {
	if(typeof console !== 'undefined') console.log("onload", new Date());
	var wb = X.read(tarea.value, {type: 'base64',WTF:wtf_mode});
	process_wb(wb);
	alert("in b64it fun");
}
var output = "";
function process_wb(wb) {
	
	switch(get_radio_value("format")) {
		case "json":
			output = JSON.stringify(to_json(wb), 2, 2);
			break;
		case "form":
			output = to_formulae(wb);
			break;
		default:
		output = to_csv(wb);
		
	
	}
	
	if(typeof console !== 'undefined') console.log("output", new Date());
}

var drop = document.getElementById('drop');
function handleDrop(e) {
	e.stopPropagation();
	e.preventDefault();
	rABS = document.getElementsByName("userabs")[0].checked;
	//use_worker = document.getElementsByName("useworker")[0].checked;
	var files = e.dataTransfer.files;
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(use_worker) {
				xw(data, process_wb);
			} else {
				var wb;
				if(rABS) {
					wb = X.read(data, {type: 'binary'});
				} else {
				var arr = fixdata(data);
					wb = X.read(btoa(arr), {type: 'base64'});
				}
				process_wb(wb);
			}
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	}
}

function handleDragover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.dataTransfer.dropEffect = 'copy';
}

if(drop.addEventListener) {
	drop.addEventListener('dragenter', handleDragover, false);
	drop.addEventListener('dragover', handleDragover, false);
	drop.addEventListener('drop', handleDrop, false);
}

function upload1(){
	
	var ufile=document.getElementById('xlf').value;
	
	
	if(ufile === ''){
		alert("Please chose a file to Upload");					
	}
	else{	
	
	
	var exte=ufile.split('.');
	
	
	if(exte[1]=="xlsx"){
		//alert(ufile);
		
	
	
	}
	
	else{
		alert("Please select Excel file with extension .xlsx");
	}
	$('#modal-5').hide();
	if(output!=''){
		$.ajax({
			url: 'Sendingexcelfile.jsp',
			dataType: 'html',
			data : {filename:ufile},
			success:function(data){
				console.log(data);
			}
			
		});
		var rdata=output.split('\n');
		//alert(rdata[3]);
		var Tbl = document.getElementById('myTable');
		//while(Tbl.childNodes.length>2){Tbl.removeChild(Tbl.lastChild);}	
		var table1 = document.getElementById("myTable");
		//var rowlength=table1.rows.length;
		$("#myTable").empty();
		var k=1;
		for(var i=3;i<rdata.length-1;i++){
			var rowdata=rdata[i].split(',');
		var row=table1.insertRow(table1.rows.length);
		var cell1=row.insertCell(0);
		var element1 = document.createElement("input");
        element1.type = "checkbox";
       
        cell1.appendChild(element1);
		var sid=row.insertCell(1);
		var rname=row.insertCell(2);
		var rhardware=row.insertCell(3);
		var rversion=row.insertCell(4);
		var rsoftware=row.insertCell(5);
		var rip=row.insertCell(6);
		var rmanufacturer=row.insertCell(7);
		
		 sid.innerHTML =k;
		 rname.innerHTML =rowdata[1];
		 rhardware.innerHTML =rowdata[8];
		 rversion.innerHTML =rowdata[3];
		 rsoftware.innerHTML =rowdata[4];
		 rip.innerHTML =rowdata[2];
		 rmanufacturer.innerHTML =rowdata[8];
		 
		 $.ajax({
				url: 'Routerdb.jsp',
				async: false,
				dataType: 'html',
				data : {ip:rowdata[2],username:rowdata[6],password:rowdata[7],hostname:rowdata[1],type:rowdata[8]},
				success:function(data){
					console.log(data);
				}
				
			});
		
		
		 k=k+1;
	}
		$.ajax({
			url: 'Insertrouters.jsp',
			dataType: 'html',
			
			success:function(data){
				console.log(data);
			}
			
		});
		//alert(output);
	}
	
}
	
	
    }
function sendrouter(){
	alert("in sendrouterdetails");
	$.ajax({
		url: 'sendrouterdetails.jsp', 
	    dataType: 'html',
	    data : {routers:output},
	     	
	}); 
}
var xlf = document.getElementById('xlf');
function handleFile(e) {
	rABS = document.getElementsByName("userabs")[0].checked;
	//use_worker = document.getElementsByName("useworker")[0].checked;
	var files = e.target.files;
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			if(typeof console !== 'undefined') console.log("onload", new Date(), rABS, use_worker);
			var data = e.target.result;
			if(use_worker) {
				xw(data, process_wb);
			} else {
				var wb;
				if(rABS) {
					wb = X.read(data, {type: 'binary'});
				} else {
				var arr = fixdata(data);
					wb = X.read(btoa(arr), {type: 'base64'});
				}
				process_wb(wb);
			}
		};
		if(rABS) reader.readAsBinaryString(f);
		else reader.readAsArrayBuffer(f);
	}
}

if(xlf.addEventListener) xlf.addEventListener('change', handleFile, false);


</script>
				
               
</form>
			</div>
			
                

                
		</div>
	</div>
</div>


    <link rel="stylesheet" href="assets/js/zurb-responsive-tables/responsive-tables.css">

    <!-- Bottom Scripts -->
    <script src="assets/js/gsap/main-gsap.js"></script>
    <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/neon-api.js"></script>
    <script src="assets/js/zurb-responsive-tables/responsive-tables.js"></script>
    <script src="assets/js/neon-chat.js"></script>
    <script src="assets/js/neon-custom.js"></script>
    <script src="assets/js/neon-demo.js"></script>

</body>
</html>
