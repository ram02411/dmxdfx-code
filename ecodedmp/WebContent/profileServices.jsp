<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="com.validate.*"%>


<%


  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	
}
else{
 String redirectURL = "Login.jsp";
 response.sendRedirect(redirectURL);
}

%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>Evolve</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon-core.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon-theme.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon-forms.css">

    <script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/zoomcharts/lib/Zoomcharts_licence.js"></script>
    <script src="${pageContext.request.contextPath}/zoomcharts/lib/zoomcharts.js"></script>
    
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    		
     <script type="text/javascript">
     		function addProfile(serviceName){
     			//alert("serviceName = "+serviceName);
     			document.getElementById("serviceNameId").value=serviceName;
     			 //alert("Add profile...");
     			 jQuery('#modal-1').modal('show', {backdrop: 'static'});
     		}
     		
     		function submitBgpForm(){
     			//alert("submit bgp profile...");
     			 document.bgpVpForm.submit();
    			 
    		}
     		function viewProfile(serviceName){
     			//alert("serviceName = "+serviceName);
     			$("#serviceNameValue").val(serviceName);
     			$("#ProfileFormId").submit();
     			
     		}
     		
     	
     
     </script>
    
    <style>
        .control-label {
            color:black;
        }
   #box th {
    font-size: 13px;
    font-weight: normal;
    background: #b9c9fe;
    border-top: 4px solid #aabcfe;
    border-bottom: 1px solid #fff;
    color: #039;
    text-align:center;
    padding: 8px;
}
   #box td {
    background: #e8edff;
    border-bottom: 1px solid #fff;
    color: #669;
    text-align:center;
    border-top: 1px solid transparent;
    padding: 8px;
}
   #box tr:hover td {
    background: #d0dafd;
    color: #339;
}
        .dropdown:hover .dropdown-menu {
            display: block;
        }
    </style>
    
</head>

<body class="page-body">

    <div class="page-container">

        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="${pageContext.request.contextPath}/assets/images/Admin.png" alt="" class="img-circle" style="width: 57px; height: 53px;" />

                        <span style="margin-left: 8px">Welcome<strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>



            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                 <li class="opened">
                    <a href="${pageContext.request.contextPath}/Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="${pageContext.request.contextPath}/Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/Inventory.jsp">
                                <span>Inventory</span>
                            </a>
                        </li>
                         
                    </ul>
                </li>
                
                <li class="opened">
                    <a href="${pageContext.request.contextPath}/L2Topology.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        
                        <li>
                            <a href="${pageContext.request.contextPath}/L2Topology.jsp">
                                <span>Viewer</span>
                            </a>
                            <ul>
                        <li>
                            <a href="${pageContext.request.contextPath}/L2Topology.jsp">
                                <span>L2 Topology</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/L3Topology.jsp">
                                <span>L3 Topology</span>
                            </a>
                        </li>
                        
                          <li>
                            <a href="${pageContext.request.contextPath}/ecode-data/findL2Path">
                                <span>Path Detection</span>
                            </a>
                        </li>
                        
                       
                    </ul>
                        </li>
  
                    </ul>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/ecode-data/services">
                        <i class="entypo-cog"></i>
                        <span>Service Chaining</span>
                    </a>

                </li>
                
                <li>
                    <a href="${pageContext.request.contextPath}/License.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>License</span>
                    </a>


                </li>
               
               
            </ul>



        </div>
        <div class="main-content">

            <div class="row">
                <div class="col-md-6 col-sm-8 clearfix"></div>
                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <strong>Ecode Networks</strong> <i class="entypo-users"></i>

                        </li>


                        <li class="sep"></li>

                        <li>
                            <a href="${pageContext.request.contextPath}/setout.jsp">Log Out <i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>




                </div>
            </div>
            <nav class="navbar navbar-inverse" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Service Chaining</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" data-toggle="modal" data-target="#popup">Create Service<b class="caret"></b></a>
                           
                        </li>
                        
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            

            <div class="container-fluid">
                <div class="col-lg-12">
                 <div class="panel panel-primary">
                    

                    <table class="table-responsive table" id="box">
                        <thead>
                            <tr>
                                 <th>SNo</th>
                                <th>Service Name</th>
                                <th>Description</th>
                                <th>Type</th>
                                <th>Client Details</th>
                                <th>Date Provisioned</th>
                                <th>Status</th>
                                <th>Number of Profiles</th>
                                <th>Add</th>
                            </tr>
                        </thead>
                        <tbody>
                          <c:forEach var="item" items="${serviceProfilesList}" varStatus="sn">
                            <tr>
                                  <td>${sn.count}</td>
                                <td>${item.servicename}</td>
                                        <td>${item.description}</td>
                                <td>${item.type}</td>
                                <td>${item.clientdetail}</td>
                                <td>${item.sys_created_on}</td>
                                <td>${item.status}</td> 
                                <td><button type="button" class="btn btn-success"  onclick="viewProfile('${item.servicename}');">${item.nop}</button></td>
                                <td><button type="button" class="btn btn-success"  onclick="addProfile('${item.servicename}');">Add Profile</button></td>
                            </tr>
                            </c:forEach>
                           
                        </tbody>
                    </table>
                    <form id="ProfileFormId" method="post" action="${pageContext.request.contextPath}/ecode-data/showProfilesByName">
                    <input type="hidden" id="serviceNameValue" name="ProfileserviceName"/>
                    </form>
                    
                </div>
</div>


            </div>
               
             </div>

   </div>



    <div class="modal fade" id="popup" role="dialog">
    <div class="modal-dialog" style="width:457px !important">
    
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><strong>Create New Services</strong></h4>
        </div>
           <form class="form-horizontal" role="form" action="${pageContext.request.contextPath}/ecode-data/addService"  method="post">
        <div class="modal-body">
        
             <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom:3px !important">
                    <label class="control-label" >Service Name</label>
                    <div>
                        <input type="text" class="form-control" id="inputName" placeholder="Enter Name" name="serviceName"/>
                    </div>
                  </div>
					</div>
					<div class="col-md-2"></div>
				</div>
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom:3px !important">
                    <label class="control-label" >Client Detail</label>
                    <div>
                        <input type="text" class="form-control" id="clientdetail1" placeholder="Enter Clientdetail" name="clientDetail"/>
                    </div>
                  </div>
					</div>
					<div class="col-md-2"></div>
				</div>
            <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom:3px !important">
                    <label class="control-label" >Description</label>
                    <div>
                        <input type="text" class="form-control" id="ProfileName1" placeholder="Enter Description" name="description"/>
                    </div>
                  </div>
					</div>
					<div class="col-md-2"></div>
				</div>
             
            <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom:3px !important">
                    <label class="control-label" >Type</label>
                    <div>
                       <select class="form-control" id="sel1" name="serviceType">
                       <option>BGP</option>
                       <option>IS-IS</option>
                       <option>MLPS</option>
                       
                    </select>
                    </div>
                  </div>
					</div>
					<div class="col-md-2"></div>
				</div>
				 <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group" style="margin-bottom:3px !important">
                    <label class="control-label" >Status</label>
                    <div>
                       <select class="form-control" id="sel2" name="serviceStatus">
                       <option>UP</option>
                       <option>DOWN</option>
                       
                       
                    </select>
                    </div>
                  </div>
					</div>
					<div class="col-md-2"></div>
				</div>
            
            
        </div>
        <div class="modal-footer">
				<button type="submit" class="btn btn-info">Add Service</button>&nbsp &nbsp
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			</div>
        </form>
      </div>
      
    </div>
  </div>
  
  
     <div class="modal fade custom-width" id="modal-1">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content">
			<span id="errorPopupMessage" style="color:blue;font-weight: bold;"></span>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><b>Adding Profile</b></h4>
			</div>
			
			<div class="modal-body">
		<form id="bgpVpForm" name="bgpVpForm" method="post" role="form" class="form-horizontal form-groups-bordered" action="${pageContext.request.contextPath}/ecode-data/addProfileService">
		        <input type="hidden" name="serviceName" id="serviceNameId"/>    
               <div class="row">
                <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label" style="text-align:center;">Profile Name :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text15" placeholder="Enter Profile Name" name="profileName"     required></div><div class="col-sm-1"></div></div>
                 
                   <div class="col-md-6">
                   
              
                           <br />
           <div class="panel panel-gradient">
      <div class="panel-heading"><h4 style="text-align:center;font-size:18px">Local Config</h4></div>
      <div class="panel-body">
                   
               <div class="form-group">
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Group Name :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text15" placeholder="Enter GroupName" name="groupName"     required></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Local IP :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text20" placeholder="Enter Local IP"  name="sourceLocalIP"></div><div class="col-sm-1"></div></div>
                       <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Type :</label>
                 <div class="col-sm-5">
                  	<select class="form-control"  id="ttype" name="type">
                           		<option>internal</option>
                           		<option>external</option>
                           </select>
                 
                 </div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Local As :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text222" placeholder="Enter Local As" name="localas"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Peer As :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text223" placeholder="Peer As" name="peeras"></div><div class="col-sm-1"></div></div>
                 
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Neighbour IP :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text27" placeholder="Enter Neighbour IP" name="sourceNeighborIP"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Authentication-Key :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text23" placeholder="Enter Authentication-Key" name="authenticationKey"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label" >Import :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text26" placeholder="Enter Import" name="imports"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Export :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text25" placeholder="Enter Export" name="exports""></div><div class="col-sm-1"></div></div>
                 </div>
      

                 </div>
                     </div>

                   </div>
  
                   <div class="col-md-6">
              <br />
                        <div class="panel panel-gradient">
      <div class="panel-heading"><h4 style="text-align:center;font-size:18px">Peer Config</h4></div>
      <div class="panel-body">
                   
               <div class="form-group">
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Group Name :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text22" placeholder="Enter GroupName"   name="groupName" required="required"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Local IP :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text28" placeholder="Enter Local IP" name="targetLocalIP"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Type :</label>
                 <div class="col-sm-5">
                           <select class="form-control"  id="stype">
                           		<option>internal</option>
                           		<option>external</option>
                           </select>
                 
                 </div><div class="col-sm-1"></div></div>
                 
                   <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Local As :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text302" placeholder="Local As" name="tlocalas"></div><div class="col-sm-1"></div></div>
                   <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Peer As :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text322" placeholder="Peer As" name="tpeeras"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Neighbour IP :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text29" placeholder="Enter Neighbour IP" name="targetNeighborIP"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Authentication-Key :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text31" placeholder="Enter Authentication-Key" name="tauthenticationKey"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Import :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text34" placeholder="Enter Import" name="timports"></div><div class="col-sm-1"></div></div>
                 <div class="form-group"><div class="col-sm-1"></div><label class="col-sm-5 control-label">Export :</label>
                 <div class="col-sm-5"><input type="text" class="form-control" id="Text32" placeholder="Enter Export" name="texports"></div><div class="col-sm-1"></div></div>
                 </div>
      
                 </div>
                     </div>

                          
                   </div>
 			</form>
             </div>
                <div class="row" style="text-align:center">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-info" id="applyBgpProfile" onclick="submitBgpForm();">Add Profile</button>
			</div>
               
                 </div>
			
		</div>
	</div>

  
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/rickshaw/rickshaw.min.css">

    <!-- Bottom Scripts -->
    <script src="${pageContext.request.contextPath}/assets/js/gsap/main-gsap.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/joinable.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/resizeable.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-api.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-chat.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-custom.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-demo.js"></script>

</body>

</html>



 
