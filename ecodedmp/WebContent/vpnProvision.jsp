<%@ taglib uri="http://www.springframework.org/tags/form" prefix="ff"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Neon Admin Panel" />
    <meta name="author" content="" />

    <title>Evolve</title>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/bootstrap.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon-core.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon-theme.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/css/neon-forms.css">
    <link href="${pageContext.request.contextPath}/zoomcharts/lib/assets/zc.css" rel="stylesheet" />
   
  <!-- <script src="../../zoomcharts-zoomcharts/lib/zoomcharts-dev.js"></script>
    <script src="zoomcharts-zoomcharts/lib/zoomcharts.js"></script>-->
    <script src="${pageContext.request.contextPath}/assets/js/jquery-1.11.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/zoomcharts/lib/zoomcharts.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="${pageContext.request.contextPath}/zoomcharts/lib/Zoomcharts_licence.js"></script>
    <style>
        .control-label {
            color:black;
            font-weight:bold;
        }  
   .form-group {
   margin-bottom: 1px !important;
}    
.panel > .panel-heading {
position: relative;
padding: 2px;
}
.panel-title > a.collapsed:after {
content: '+';
position: absolute;
right: 24px;
}

.panel-title > a:after {
 content: '-';
position: absolute;
right: 24px;

}

        h2 {
            color: #000000;
            padding: 0em;
            font-size: 1.5em;
            margin: 4px 0 16px 0;
            font-weight: bold;
            position: relative;
        }

            h2::after {
                position: absolute;
                top: 100%;
                left: -1px;
                content: " ";
                width: 35%;
                height: 1px;
                background: black;
                background-image: -webkit-linear-gradient(left, #B2B2B2, black, #B2B2B2);
                background-image: -moz-linear-gradient(left, #B2B2B2, #a60000, #B2B2B2);
                background-image: -ms-linear-gradient(left, #B2B2B2, #a60000, #B2B2B2);
                background-image: -o-linear-gradient(left, #B2B2B2, #a60000, #B2B2B2);
            }

        .modalDialog1 {
            position: fixed;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            font-size: 13px;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(0,0,0,0.8);
            z-index: 99999;
            opacity: 0;
            -webkit-transition: opacity 400ms ease-in;
            -moz-transition: opacity 400ms ease-in;
            transition: opacity 400ms ease-in;
            pointer-events: none;
        }

            .modalDialog1:target {
                opacity: 1;
                pointer-events: auto;
            }

            .modalDialog1 > div {
                width: 460px;
                position: relative;
                margin: 15% 39%;
                padding: 5px 20px 13px 20px;
                border-radius: 0px;
                background: #fff;
                /*background: -moz-linear-gradient(#fff, #999);
	background: -webkit-linear-gradient(#fff, #999);
	background: -o-linear-gradient(#fff, #999);*/
                background: -moz-linear-gradient(#d9d9d9,#737373);
                background: -webkit-linear-gradient(#d9d9d9,#737373);
                background: -o-linear-gradient(#d9d9d9,#737373);
                box-shadow: 8px 8px 5px #1a1a1a;
            }

        .close {
            background: #606061;
            color: #FFFFFF;
            line-height: 25px;
            position: absolute;
            right: -12px;
            text-align: center;
            top: -10px;
            width: 24px;
            text-decoration: none;
            font-weight: bold;
            -webkit-border-radius: 12px;
            -moz-border-radius: 12px;
            border-radius: 12px;
            -moz-box-shadow: 1px 1px 3px #000;
            -webkit-box-shadow: 1px 1px 3px #000;
            box-shadow: 1px 1px 3px #000;
        }

            .close:hover {
                background: #0052cc;
            }

 .dropdown:hover .dropdown-menu {
            display: block;
        }
    </style>
    <script type="text/javascript">
        $(function () {
        	
         	$("#destRouter").change(
             	   function(){
             		   $("#errorMessage").html("");
             	   }		
             	);
             	
             	$("#sourceRouter").change(
                  	   function(){
                  		   $("#errorMessage").html("");
                  	   }		
                  	);
             	
             	$("#pathFindId").click(function(){
     				var sourceRouter=$("#sourceRouter").val();
     				var destRouter=$("#destRouter").val();
     				if(sourceRouter==destRouter){
     					$("#errorMessage").html("Sorry ! source and destination routers cannot be same!,please select right option.");
     					return;
     				}
             		$("#PathFinderFormId").submit();
             	}
             	);
        	
            //Loop through all Labels with class 'editable'.
            $(".editable").each(function () {
                //Reference the Label.
                var label = $(this);

                //Add a TextBox next to the Label.
                label.after("<input type = 'text' style = 'display:none' />");

                //Reference the TextBox.
                var textbox = $(this).next();

                //Set the name attribute of the TextBox.
                textbox[0].name = this.id.replace("lbl", "txt");

                //Assign the value of Label to TextBox.
                textbox.val(label.html());

                //When Label is clicked, hide Label and show TextBox.
                label.click(function () {
                    $(this).hide();
                    $(this).next().show();
                });

                //When focus is lost from TextBox, hide TextBox and show Label.
                textbox.focusout(function () {
                    $(this).hide();
                    $(this).prev().html($(this).val());
                    $(this).prev().show();
                });
            });
        });
    </script>
    
    <script type="text/javascript">
    			
    			function makeBgpGroupReadOnly(){
    				$('input[type="text"][name="bgp.bgpGroup[0].name"]').attr('readonly','readonly');
               		$("#bgpGroupType").attr('readonly','readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].asnumber"]').attr('readonly','readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"]').attr('readonly','readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].peeras"]').attr('readonly','readonly');
					$('input[type="text"][name="bgp.bgpGroup[0].localaddress"]').attr('readonly','readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].name"]').attr('readonly','readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].authenticationKey"]').attr('readonly','readonly');	
               		$('input[type="text"][name="bgp.bgpGroup[0].timport"]').attr('readonly','readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].texport"]').attr('readonly','readonly');
    			}
    			
    			function createNewBgpGroup(){
    				$('input[type="text"][name="bgp.bgpGroup[0].name"]').removeAttr('readonly');
               		$("#bgpGroupType").removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].asnumber"]').removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"]').removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].peeras"]').removeAttr('readonly');
					$('input[type="text"][name="bgp.bgpGroup[0].localaddress"]').removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].name"]').removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].authenticationKey"]').removeAttr('readonly');	
               		$('input[type="text"][name="bgp.bgpGroup[0].timport"]').removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].texport"]').removeAttr('readonly');
               		
               		$('input[type="text"][name="bgp.bgpGroup[0].name"]').val('');
               		//$("#bgpGroupType").removeAttr('readonly');
               		$('input[type="text"][name="bgp.bgpGroup[0].asnumber"]').val('');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"]').val('');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].peeras"]').val('');
					$('input[type="text"][name="bgp.bgpGroup[0].localaddress"]').val('');
               		$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].name"]').val('');
               		$('input[type="text"][name="bgp.bgpGroup[0].authenticationKey"]').val('');	
               		$('input[type="text"][name="bgp.bgpGroup[0].timport"]').val('');
               		$('input[type="text"][name="bgp.bgpGroup[0].texport"]').val('');
               		
    			}
    
    			var datato='';
                   var data;
                   var contextPath="http://localhost:8080/ecodedmp/ecode-data";
                   
                   
                   function fetchDataModelForDevice(deviceId){
                	   
                	   $.getJSON(contextPath+"/juniperData", {routerName: deviceId}, function(jsonResponse) {
                		   data=jsonResponse;
               		        console.log("data = "+jsonResponse);  
               		        $("#bgpGroupHostnameId").val(deviceId);
             	   			if(!jQuery.isEmptyObject(jsonResponse)) {
             	   			  if(jsonResponse.configuration.system!=null) 	
             	   			  alert("____hostname__"+jsonResponse.configuration.system.hostname);
             	   			  
             	   			  
             	   				//alert(jsonResponse);
             	   				//alert("____Data is coming from_______ = "+jsonResponse.resultStatus);
             	   			if(jsonResponse.configuration.protocols.bgp!=null){	
             	   				if(jsonResponse.configuration.protocols.bgp.traceoptions!=null){
             	   					$('input[type="text"][name="bgp.traceoptions.file.filename"]').val(jsonResponse.configuration.protocols.bgp.traceoptions.file.filename);
             	   					$('input[type="text"][name="bgp.traceoptions.flag.name"]').val(jsonResponse.configuration.protocols.bgp.traceoptions.flag.name);
             	   				}
             	   				//$('input[type="text"][name="bgp.traceoptions.flag.detail"]').val(jsonResponse.configuration.protocols.bgp.traceoptions.flag.detail);
             	   				//setting the value for BGP Groups
             	   				if(jsonResponse.configuration.protocols.bgp.family!=null)
             	   					$('input[type="text"][name="bgp.family.inet6.labeledunicast.explicitnull"]').val(jsonResponse.configuration.protocols.bgp.family.inet6.labeledunicast.explicitnull);
             	   			
             	   				$('input[type="text"][name="bgp.authenticationkey"]').val(jsonResponse.configuration.protocols.bgp.authenticationkey);
	              	   		
             	   				if(jsonResponse.configuration.protocols.bgp.bgpGroup!=null){	
             	   							$('input[type="text"][name="bgp.bgpGroup[0].name"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].name);
             	   							$('input[type="text"][name="bgp.bgpGroup[0].type"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].type);
             	   							$('input[type="text"][name="bgp.bgpGroup[0].localaddress"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].localaddress);
             	   							$('input[type="text"][name="bgp.bgpGroup[0].keep"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].keep);
             	   					
             	   							if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].family!=null){	
			              	   					$('input[type="text"][name="bgp.bgpGroup[0].family.inet.unicast"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].family.inet.unicast);
			              	   					$('input[type="text"][name="bgp.bgpGroup[0].family.inetVpn.unicast"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].family.inetVpn.unicast);
			              	   			    	$('input[type="text"][name="bgp.bgpGroup[0].family.inet6.labeledUnicast.explicitnull"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].family.inet6.labeledUnicast.explicitnull);
			              	   					$('input[type="text"][name="bgp.bgpGroup[0].family.inet6Vpn.unicast"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].family.inet6Vpn.unicast);
             	   							}
             	   						
			              	   			    $('input[type="text"][name="bgp.bgpGroup[0].authenticationKey"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].authenticationKey);
			              	   			if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].export!=null){
			              	   				$('input[type="text"][name="bgp.bgpGroup[0].export[0]"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].export[0]);
			              	   				$('input[type="text"][name="bgp.bgpGroup[0].export[1]"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].export[1]);
			              	   			}	
			              	   				
			              	   				
			              	   			if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].gracefulRestart){
		              	   					
			              	   				$('input[type="text"][name="bgp.bgpGroup[0].gracefulRestart.restarttime"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].gracefulRestart.restarttime);
			              	   			 	$('input[type="text"][name="bgp.bgpGroup[0].gracefulRestart.staleRoutestime"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].gracefulRestart.staleRoutestime);
			              	   			}	 	
			              	   			 
			              	   		if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].multipath!=null){
			              	   			 $('input[type="text"][name="bgp.bgpGroup[0].multipath.inactive"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].multipath.inactive);
			              	   		}	 
			              	   		
						              	   	if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].neighbor!=null){
			              	   					if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].neighbor[0]!=null)
				              	   			    $('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].name"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].neighbor[0].name);
				              	   				if(jsonResponse.configuration.protocols.bgp.bgpGroup[0].neighbor[1]!=null)
			              	   			   	    $('input[type="text"][name="bgp.bgpGroup[0].neighbor[1].name"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[0].neighbor[1].name);
			              	   				}
			              	   						    
			    	              	   	    ////second
			    	              	   	    if(jsonResponse.configuration.protocols.bgp.bgpGroup[1]!=null){
		    	              	   	   				$('input[type="text"][name="bgp.bgpGroup[1].name"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].name);
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].type"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].type);
		    	              	   					if(jsonResponse.configuration.protocols.bgp.bgpGroup[1].traceoptions!=null && jsonResponse.configuration.protocols.bgp.bgpGroup[1].traceoptions.file!=null)
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].traceoptions.file.filename"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].traceoptions.file.filename);
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].localaddress"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].localaddress);
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].family.inet6.multicast.ribGroup.ribgroupname"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].family.inet6.multicast.ribGroup.ribgroupname);
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].authenticationKey"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].authenticationKey);
		    	              	   					
		    	              	   					if(jsonResponse.configuration.protocols.bgp.bgpGroup[1].export!=null)
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].export[0]"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].export[0]);
		    	              	   					
		    	              	   					
		    	              	   					$('input[type="text"][name="bgp.bgpGroup[1].neighbor[0].name"]').val(jsonResponse.configuration.protocols.bgp.bgpGroup[1].neighbor[0].name);
		    	              	   					
			    	              	   	    }
             	   				
             	   				}
   	              	      		console.log(jsonResponse);
             	   			} //end of bgp null check 	
   	              	           read();
             	   		}//empty
             	   		else{
             	   			alert("##########SORRY Data Model for this router could not be loaded!!!##########");
             	   		}
                  	}); //end of the json call
                	   
                   }
                   
                   function read(){
                	   if(data.configuration != null ){
   	              		  // alert("in config");
   	              		   //alert(data.configuration.snmp.community.name);
   	   	              	   if(data.configuration.snmp!= null ){
   	   	              		   alert(data.configuration.snmp.trapgroup.version);
   	         				  if(data.configuration.snmp.community!= null){
   	         					  document.getElementById("SNMP_Community_Name1").value=data.configuration.snmp.community.name;
   	         						 document.getElementById("SNMP_Community_Authorization").value=data.configuration.snmp.community.authorization;
   	         						 document.getElementById("SNMP_Community_Client_Name").value=data.configuration.snmp.community.clients.name;
   	 							 }
   	         				  if(data.configuration.snmp.trapgroup!= null){
   	         					  document.getElementById("SNMP_Trap_Name").value=data.configuration.snmp.trapgroup.name; 
   	         						 document.getElementById("SNMP_Trap_Version").value=data.configuration.snmp.trapgroup.version;
   	         						 document.getElementById("SNMP_Trap_Destination-port").value=data.configuration.snmp.trapgroup.destinationPort;
   	         						 if(data.configuration.snmp.trapgroup.targets!= null){
   	         							 document.getElementById("SNMP_Trap_Target_Name").value=data.configuration.snmp.trapgroup.targets.name;
   	         							 
   	         						 }
   	         				  }
   	         		 		 } 
   	         		 		 if(data.configuration.interfaces!= null && data.configuration.interfaces.interface!= null){
   	         		 		 	 document.getElementById("Text73").value=data.configuration.interfaces.interface[0].name;
   	         					if(data.configuration.interfaces.interface[0].unit != null){
   	         						document.getElementById("Text74").value=data.configuration.interfaces.interface[0].unit[0].name;
   	         						document.getElementById("Text75").value=data.configuration.interfaces.interface[0].unit[0].family.inet.address.name;
   	         					
   	         					}
   	         					document.getElementById("Text76").value=data.configuration.interfaces.interface[1].name;
   	         					if(data.configuration.interfaces.interface[1].unit!= null){
   	         						document.getElementById("Text77").value=data.configuration.interfaces.interface[1].unit.name;
   	         						document.getElementById("Text78").value=data.configuration.interfaces.interface[1].unit.family.inet.address.name;
   	         					
   	         					}
   	         							 
   	         		 		 }
   	         		 		 
   	         		 		 
   	         		 		 
   	         		 		 
   	         		 		 
   	   	              	    if(data.configuration.system.syslog != null){
   	        			  if(data.configuration.system.syslog.user != null){
   	        				  document.getElementById("Text1").value=data.configuration.system.syslog.user.name;				 
   	        					 
   	        			  
   	        			  if(data.configuration.system.syslog.user.contents != null){
   	        				  document.getElementById("Text97").value=data.configuration.system.syslog.user.contents.name;
   	        					 document.getElementById("Text98").value=data.configuration.system.syslog.user.contents.emergency;
   	        					         					 
   	        			  }
   	        			  }
   	        			  if(data.configuration.system.syslog.host != null){
   	        				  document.getElementById("Text99").value=data.configuration.system.syslog.host.name;
   	        				  if(data.configuration.system.syslog.host.contents != null){
   	        					  document.getElementById("Text101").value=data.configuration.system.syslog.host.contents.name;
   	        						 document.getElementById("Text102").value=data.configuration.system.syslog.host.contents.notice;
   	        						      					 
   	            			  }	
   	        			  }
   	        			  if(data.configuration.system.syslog.file != null){
   	        				  document.getElementById("Text103").value=data.configuration.system.syslog.file[0].name;
   	        					 
   	        				  if(data.configuration.system.syslog.file[0].contents != null){
   	        					  document.getElementById("Text105").value=data.configuration.system.syslog.file[0].contents[0].name;
   	        						 document.getElementById("Text106").value=data.configuration.system.syslog.file[0].contents[0].error;
   	        						 document.getElementById("Text107").value=data.configuration.system.syslog.file[0].contents[1].name;
   	        						 document.getElementById("Text108").value=data.configuration.system.syslog.file[0].contents[1].info;
   	        						 				 
   	            			  }	
   	        				  document.getElementById("Text109").value=data.configuration.system.syslog.file[1].name;
   	     					 
   	        				  if(data.configuration.system.syslog.file[1].contents != null){
   	        					  document.getElementById("Text111").value=data.configuration.system.syslog.file[1].contents[0].name;
   	        						 document.getElementById("Text112").value=data.configuration.system.syslog.file[1].contents[0].any;
   	        						  
   	            			  }	
   	        				  if(data.configuration.system.syslog.file[2] != null){
   	        				  document.getElementById("Text113").value=data.configuration.system.syslog.file[2].name;
   	      					 
   	        				  if(data.configuration.system.syslog.file[2].contents != null){
   	        					  document.getElementById("Text116").value=data.configuration.system.syslog.file[2].contents[0].name;
   	        						 document.getElementById("Text115").value=data.configuration.system.syslog.file[2].contents[0].info;
   	        						 
   	            			  }	
   	        				  } 
   	        				  
   	        			  }
   	        			  
   	        			  
   	        		  }
   	        		  if(data.configuration.system.services != null && data.configuration.system.services.netconf != null){
   	        			  document.getElementById("Text117").value=data.configuration.system.services.netconf.ssh.port;
   	        			}
   	        		  if(data.configuration.routingInstances != null && data.configuration.routingInstances.instance != null){
   	        			  document.getElementById("Text118").value=data.configuration.routingInstances.instance[0].name;
   	        				 document.getElementById("Text119").value=	data.configuration.routingInstances.instance[0].description;
   	        				 document.getElementById("Text120").value=data.configuration.routingInstances.instance[0].instanceType;
   	        				 document.getElementById("Text121").value=data.configuration.routingInstances.instance[0].routeDistinguisher;
   	        				 document.getElementById("Text122").value=data.configuration.routingInstances.instance[0].vrfImport;	
   	        				 document.getElementById("Text123").value=data.configuration.routingInstances.instance[0].vrfExport;		
   	        				 document.getElementById("Text124").value=data.configuration.routingInstances.instance[0].vrfTableLabel;
   	        			
   	        				 document.getElementById("Text125").value=data.configuration.routingInstances.instance[1].name;
   	        				 document.getElementById("Text126").value=	data.configuration.routingInstances.instance[1].description;
   	        				 document.getElementById("Text127").value=data.configuration.routingInstances.instance[1].instanceType;
   	        				 document.getElementById("Text128").value=data.configuration.routingInstances.instance[1].routeDistinguisher;
   	        				 if(data.configuration.routingInstances.instance[1].routingInterfaces != null){
   	        				 document.getElementById("Text129").value=data.configuration.routingInstances.instance[1].routingInterfaces[0].name;
   	        				 document.getElementById("Text130").value=data.configuration.routingInstances.instance[1].routingInterfaces[1].name;
   	        				 document.getElementById("Text131").value=data.configuration.routingInstances.instance[1].routingInterfaces[2].name;
   	        				 }
   	        				 document.getElementById("Text132").value=data.configuration.routingInstances.instance[1].routeDistinguisher;
   	        				 document.getElementById("Text133").value=data.configuration.routingInstances.instance[1].vrfImport;	
   	        				 document.getElementById("Text134").value=data.configuration.routingInstances.instance[1].vrfExport;		
   	        				 document.getElementById("Text135").value=data.configuration.routingInstances.instance[1].vrfTableLabel;
   	        				 
   	        				 document.getElementById("Text136").value=data.configuration.routingInstances.instance[2].name;
   	        				 document.getElementById("Text137").value=	data.configuration.routingInstances.instance[2].description;
   	        				 document.getElementById("Text138").value=data.configuration.routingInstances.instance[2].instanceType;
   	        				 document.getElementById("Text139").value=data.configuration.routingInstances.instance[2].routeDistinguisher;
   	        				 document.getElementById("Text140").value=data.configuration.routingInstances.instance[2].vrfImport;	
   	        				 document.getElementById("Text141").value=data.configuration.routingInstances.instance[2].vrfExport;		
   	        				 document.getElementById("Text142").value=data.configuration.routingInstances.instance[2].vrfTableLabel;
   	        				 
   	        				 document.getElementById("Text143").value=data.configuration.routingInstances.instance[3].name;
   	        				 document.getElementById("Text144").value=	data.configuration.routingInstances.instance[3].description;
   	        				 document.getElementById("Text145").value=data.configuration.routingInstances.instance[3].instanceType;
   	        				 document.getElementById("Text146").value=data.configuration.routingInstances.instance[3].routeDistinguisher;
   	        				 document.getElementById("Text147").value=data.configuration.routingInstances.instance[3].vrfImport;	
   	        				 document.getElementById("Text148").value=data.configuration.routingInstances.instance[3].vrfExport;		
   	        				 document.getElementById("Text149").value=data.configuration.routingInstances.instance[3].vrfTableLabel;
   	        				
   	        				 document.getElementById("Text150").value=data.configuration.routingInstances.instance[4].name;
   	        				 document.getElementById("Text151").value=	data.configuration.routingInstances.instance[4].description;
   	        				 document.getElementById("Text152").value=data.configuration.routingInstances.instance[4].instanceType;
   	        				 if(data.configuration.routingInstances.instance[4].routingInterfaces != null){
   	        				 document.getElementById("Text153").value=data.configuration.routingInstances.instance[4].routingInterfaces[0].name;;	
   	        				 }
   	        				 document.getElementById("Text154").value=data.configuration.routingInstances.instance[4].routeDistinguisher;
   	        				 document.getElementById("Text155").value=data.configuration.routingInstances.instance[4].vrfTableLabel;
   	        				
   	        		  
   	        		  	}
   	        		  
   	        		  
   	        		  document.getElementById("Text157").value=data.configuration.system.hostname;
   	        
   	   	              	   }
   	              	 
                   }
                   $(document).ready(function() {
                	   $("#clearBgpGroupButton").click(function(){
                		   createNewBgpGroup();
                	   });
                	   
                	   $("#internalspan").hide();
        		       $("#externalspan").show();
                	   $("#bgpGroupType").change(function(){
                		    //var groupType=$(this).val();
                		    /* if(groupType=='internal'){
                		       $("#internalspan").show();
                		       $("#externalspan").hide();
                		       $('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"]').val('');
                		       $('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].peeras"]').val('');
                		    }else{
                		    	$("#externalspan").show();
                		    	$("#internalspan").hide();
                		    	$('input[type="text"][name="bgp.bgpGroup[0].asnumber"]').val('');
                		    } */
                			$("#externalspan").show();
            		    	$("#internalspan").hide();
            		    	$('input[type="text"][name="bgp.bgpGroup[0].asnumber"]').val('');
                		    
                		    
                	   });
                	   
                	   $("input[type='text']").click(function(){
			    	       	$(this).removeAttr('readonly');
			      	   });
                	   
                	   
                   	   
                   	$("#bgpTraceOptionsBt").click(function(){
                   		
                   		
    					var bgpTraceOptions = $("#bgpTraceOptionsForm").serialize();
    					alert("Oyeeeee= "+bgpTraceOptions);
    					console.log(bgpTraceOptions);
    					$.ajax({
    						url : contextPath + "/cpTraceOptions",
    						dataType: 'json',
    						data : bgpTraceOptions,
    						type : 'POST',
    						success : function(cdata) {
    							console.log(cdata);
    							alert(cdata.message);
    						}
    					});
    		       });
                   	   
                	$("#bgpGroupFormBt").click(function(){
                		//var gauthkey=$('input[type="text"][name="bgp.authenticationkey"]').val();
                		//alert("_@))# = "+gauthkey);
                		//$('#gbgpauthenticationkeyid').val(gauthkey);
                		if ( $('input[type="text"][name="bgp.bgpGroup[0].name"]').is('[readonly]') ) { 
                			alert(" Please click on clear button to create new bgp group.");
                			return;
                		}
                		
                		var gname=$('input[type="text"][name="bgp.bgpGroup[0].name"]').val();
                   		if(gname.length==0){
                   			alert("Group Name cannnot be blank");
                   			$('input[type="text"][name="bgp.bgpGroup[0].name"]').focus();
                   			return;
                   		}
                   		var bgpGroupType=$("#bgpGroupType").val();
                   			var localasnumber=$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"]').val();
                   			if(localasnumber.length==0){
                       			alert("Local asnumber cannnot be blank");
                       			$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"]').focus();
                       			return;
                       		}
                   			
                   			var peerasnumber=$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].peeras"]').val();
                   			if(peerasnumber.length==0){
                       			alert("Peer asnumber cannnot be blank");
                       			$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].peeras"]').focus();
                       			return;
                       		}
                   			
                   		
                   		var localaddress=$('input[type="text"][name="bgp.bgpGroup[0].localaddress"]').val();
                   		if(localaddress.length==0){
                   			alert("Local Address cannnot be blank");
                   			$('input[type="text"][name="bgp.bgpGroup[0].localaddress"]').focus();
                   			return;
                   		}
 						var neighborname=$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].name"]').val();
 						if(neighborname.length==0){
                   			alert("Neighbor IP cannnot be blank");
                   			$('input[type="text"][name="bgp.bgpGroup[0].neighbor[0].name"]').focus();
                   			return;
                   		}
    					var bgpGroup = $("#bgpGroupForm").serialize();
    					//alert("Oyeeeee= "+bgpGroup);
    					console.log(bgpGroup);
    					$.ajax({
    						url : contextPath + "/bgpContainerProvision",
    						dataType: 'json',
    						data : bgpGroup,
    						type : 'POST',
    						success : function(cdata) {
    							console.log(cdata);
    							alert(cdata.message);
    							if(cdata.status!='failed')
    							makeBgpGroupReadOnly();
    						}
    					});
    		       });   
                   	   
                   		   
    			
                	  
                	   
                   }); //end of the jQuery handler
                  	 
     </script>              
</head>
<body class="page-body">

    <div class="page-container">

        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="${pageContext.request.contextPath}/assets/images/Admin.png" alt="" class="img-circle" style="width: 57px; height: 53px;" />

                        <span style="margin-left: 8px">Welcome<strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>






            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                <li class="opened">
                    <a href="Dashboard.html">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.html">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="Inventory.html">
                                <span>Inventory</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                <li class="opened">
                    <a href="canvas.html">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        <li>
                            <a href="canvas.html">
                                <span>Canvas</span>
                            </a>
                        </li>
                        <li>
                            <a href="Juniper.html">
                                <span>Viewer</span>
                            </a>
                            <ul>
                        <li>
                            <a href="Juniper.html">
                                <span>L2 Topology</span>
                            </a>
                        </li>
                        <li>
                            <a href="Topologyview.html">
                                <span>L3 Topology</span>
                            </a>
                        </li>
                    </ul>
                        </li>
                    </ul>
                </li>
                <li class="opened">
                    <a href="simulate.html">
                        <i class="entypo-layout"></i>
                        <span>Control</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Rulebase.html">
                                <span>Rule Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="PolicyBase.html">
                                <span>Policy Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="simulate.html">
                                <span>Simulate</span>
                            </a>
                        </li>
                    </ul>

                </li>


                <li>
                    <a href="Netlyzer.html">
                        <i class="entypo-doc-text"></i>
                        <span>Netlyzer</span>
                    </a>

                </li>
                <li>
                    <a href="flowrector.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Flow Rector</span>
                    </a>


                </li>
            </ul>



        </div>
        <div class="main-content">

            <br />
            <div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">

                    <ul class="user-info pull-left pull-none-xsm">
                    </ul>
                    <ul class="user-info pull-left pull-right-xs pull-none-xsm">

                        <!-- Raw Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-attention"></i>
                                <span class="badge badge-info">6</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p class="small">
                                        <a href="#" class="pull-right">Mark all Read</a>
                                        You have <strong>3</strong> new notifications.
                                    </p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="unread notification-success">
                                            <a href="#">
                                                <i class="entypo-user-add pull-right"></i>

                                                <span class="line">
                                                    <strong>New user registered</strong>
                                                </span>

                                                <span class="line small">30 seconds ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="unread notification-secondary">
                                            <a href="#">
                                                <i class="entypo-heart pull-right"></i>

                                                <span class="line">
                                                    <strong>Someone special liked this</strong>
                                                </span>

                                                <span class="line small">2 minutes ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-primary">
                                            <a href="#">
                                                <i class="entypo-user pull-right"></i>

                                                <span class="line">
                                                    <strong>Privacy settings have been changed</strong>
                                                </span>

                                                <span class="line small">3 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-danger">
                                            <a href="#">
                                                <i class="entypo-cancel-circled pull-right"></i>

                                                <span class="line">John cancelled the event
                                                </span>

                                                <span class="line small">9 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-info">
                                            <a href="#">
                                                <i class="entypo-info pull-right"></i>

                                                <span class="line">The server is status is stable
                                                </span>

                                                <span class="line small">yesterday at 10:30am
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-warning">
                                            <a href="#">
                                                <i class="entypo-rss pull-right"></i>

                                                <span class="line">New comments waiting approval
                                                </span>

                                                <span class="line small">last week
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">View all notifications</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Message Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-mail"></i>
                                <span class="badge badge-secondary">10</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right"></span>

                                                <span class="line">
                                                    <strong>Luc Chartier</strong>
                                                    - yesterday
                                                </span>

                                                <span class="line desc small">This ain’t our first item, it is the best of the rest.
                                                </span>
                                            </a>
                                        </li>

                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right"></span>

                                                <span class="line">
                                                    <strong>Salma Nyberg</strong>
                                                    - 2 days ago
                                                </span>

                                                <span class="line desc small">Oh he decisively impression attachment friendship so if everything. 
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right"></span>

                                                <span class="line">Hayden Cartwright
					- a week ago
                                                </span>

                                                <span class="line desc small">Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right"></span>

                                                <span class="line">Sandra Eberhardt
					- 16 days ago
                                                </span>

                                                <span class="line desc small">On so attention necessary at by provision otherwise existence direction.
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="mailbox.html">All Messages</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Task Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-list"></i>
                                <span class="badge badge-warning">1</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p>You have 6 pending tasks</p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Procurement</span>
                                                    <span class="percent">27%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 27%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">27% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">App Development</span>
                                                    <span class="percent">83%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 83%;" class="progress-bar progress-bar-danger">
                                                        <span class="sr-only">83% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">HTML Slicing</span>
                                                    <span class="percent">91%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 91%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">91% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Database Repair</span>
                                                    <span class="percent">12%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 12%;" class="progress-bar progress-bar-warning">
                                                        <span class="sr-only">12% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Backup Create Progress</span>
                                                    <span class="percent">54%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 54%;" class="progress-bar progress-bar-info">
                                                        <span class="sr-only">54% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Upgrade Progress</span>
                                                    <span class="percent">17%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 17%;" class="progress-bar progress-bar-important">
                                                        <span class="sr-only">17% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">See all tasks</a>
                                </li>
                            </ul>

                        </li>

                    </ul>

                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <strong>Ecode Networks</strong> <i class="entypo-users"></i>

                        </li>


                        <li class="sep"></li>

                        <li>
                            <a href="extra-login.html">Log Out <i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>




                </div>
            </div>
            <nav class="navbar navbar-inverse" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Viewer</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Add<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#openModal">Add Flow</a>

                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Go To<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="simulate.html">PNT-SNT</a>
                                </li>
                                <li><a href="ViewerSnt.html">Viewer SNT</a>
                                </li>
                                <li><a href="Rulebase.html">PNT RuleBase</a>
                                </li>
                                <li><a href="PolicyBase.html">PNT PolicyBase</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="openModal" class="modalDialog1" style="overflow: auto;">
                <div>
                    <a href="#close" title="Close" class="close">X</a>
                    <h2 style="margin-left: 152px; margin-top: 6px;">Add Flow</h2>
                    <label style="margin-left: 72px; font: 16px; font-weight: bold; color: black;">Switch:</label>

                    <select name="switches" style="width: 180px; height: 23px; margin-left: 16px;">
                        <option selected="selected">---select------</option>
                        <option value="switch1">switch1</option>
                        <option value="switch2">switch2</option>
                        <option value="switch3">switch3</option>
                    </select>


                    <label style="margin-left: 74px; margin-top: 12px; font: 16px; font-weight: bold; color: black;">Priority:</label>
                    <input id="Text1" type="text" style="width: 181px; margin-left: 10px;" />

                    <div id="row" style="width: 600px; margin-top: 17px;">
                        <div id="col1" style="width: 300px; float: left;">
                            <label style="margin-left: 29px; color: black; font-size: 12px;">Match Parameters No.</label>
                            <br />
                            <input id="matchpnum" type="text" style="margin-left: 19px; margin-top: 5px;" />
                            <br />
                            <a style="margin-left: 25px; font-size: 13px;" onclick="callmandap();">+Match Parameters No.</a>
                            <div id="lblmatch"></div>

                        </div>
                        <div id="col2" style="width: 280px; float: left;">

                            <label style="margin-left: -58px; color: black; font-size: 12px;">Action Parameter No.</label>
                            <br />
                            <input id="matchanum" type="text" style="margin-left: -71px; margin-top: 4px;" />
                            <br />
                            <a onclick="callmandaa()" style="margin-left: -62px; font-size: 13px;">+Action Parameters No.</a>
                            <div id="lblaction" style="margin-left: -81px;"></div>
                        </div>
                    </div>

                    <button type="button" class="btn btn-primary btn-grey btn-lg btn-icon" style="opacity: 0.5; margin-left: 150px; padding: 7px 25px; margin-top: 10px;">Add Flow</button>
                    <br />
                </div>
            </div>

            <div class="container-fluid">
                <div class="col-lg-12" id="demo" style="background-color: aliceblue;">
   <script>
   var netchartdata;
   
   $.ajax({
	   url: "http://localhost:8080/ecodedmp/ecode-data/viewNetworkTopology",
	   dataType: 'json',
	   async: false,
	   success: function(jsonresponse)  {
   			if(!jQuery.isEmptyObject(jsonresponse)) {
             //datato=jsonResponse;
             var topology=jsonresponse;
             //data='{"nodes":[{"id":"juniper ACX2200","loaded":true,"style":{"label":"juniper ACX2200"}},{"id":"juniper ACX200","loaded":true,"style":{"label":"juniper ACX200"}},{"id":"juniper ACX2100","loaded":true,"style":{"label":"juniper ACX2100"}}],"links":[{"id":"juniper ACX2200em0","style":{"fillColor":"green","toDecoration":"arrow"},"from":"juniper ACX2200","to":"juniper ACX200"},{"id":"juniper ACX2200em1","style":{"fillColor":"green","toDecoration":"arrow"},"from":"juniper ACX2200","to":"juniper ACX2100"}]} '
            	 netchartdata=topology.jsonMessage;
             
   			}
	   }
   });
  
       var lastPieChartNode;
       var lastSettings;
       function hidePieChart() {
           pieChart.updateSettings({
               area: { left: -100, top: -100, width: 0, height: 0 }
           });
           lastSettings = null;
           lastPieChartNode = null;
       }

       function updatePieChart() {
           var selectedNodes = netChart.selection();
           if (selectedNodes.length === 0) {
               hidePieChart();
               return;
           }

           var currentNode = selectedNodes[0];
           if (currentNode.data.categories == null || currentNode.data.categories.length === 0) {
               hidePieChart();
               return;
           }

           if (currentNode === lastPieChartNode) {
               return;
           }
           lastPieChartNode = currentNode;
           var pieChartSettings = getPieChartDimensions(currentNode);
           pieChartSettings.data = { preloaded: { subvalues: currentNode.data.categories } };


           pieChartSettings.navigation = {};

           pieChart.updateSettings(pieChartSettings);
       }

       function getPieChartDimensions(node) {
           var dimensions = netChart.getNodeDimensions(node);
           var output = {
               area: {
                   left: dimensions.x - dimensions.radius * 5,
                   top: dimensions.y - dimensions.radius * 5,
                   width: dimensions.radius * 10,
                   height: dimensions.radius * 10
               },
               pie: {
                   radius: dimensions.radius + (dimensions.radius / 2),
                   innerRadius: dimensions.radius
               }
           };
           return output;
       }

       function movePieChart() {
           var selectedNodes = netChart.selection();
           if (selectedNodes.length === 0) {
               return;
           }
           var currentNode = selectedNodes[0];
           if (currentNode.data.categories == null || currentNode.data.categories.length === 0) {
               return;
           }

           var settings = getPieChartDimensions(currentNode);
           var currentArea = settings.area;
           if (lastSettings
                   && lastSettings.area.left === currentArea.left
                   && lastSettings.area.top === currentArea.top
                   && lastSettings.pie.radius === settings.pie.radius) {
               return;
           }
           lastSettings = settings;
           pieChart.updateSettings(settings);
       }

       var netChart = new NetChart({
           container: document.getElementById("demo"),
           area: { height: 450 },
           events: {

               onRightClick: clickEvent,
               onSelectionChange: updatePieChart,
               onPositionChange: movePieChart
           },

           style: {
               node: {
                   imageCropping: "fit",
               },
               nodeAutoScaling: "none",
               nodeHovered: {
                   fillColor: "white",
                   shadowColor: "#419a00",
                   shadowOffsetY: 2
               },
               nodeLabel: { backgroundStyle: { fillColor: 'orange', } },
               nodeStyleFunction: function (node) {
                   node.label = node.selected && node.data.categories ? '' : node.data.name;
                   var radius = 30;
                   if (node.data.categories) {
                       for (var i = 0; i < node.data.categories.length; i++) {
                           radius += node.data.categories[i].value;
                       }
                   }
                   node.radius = radius;


               },
               selection: {
                   sizeConstant: 0,
                   sizeProportional: 0
               },
               nodeLabel: {
                   padding: 1,
                   borderRadius: 2,
                   textStyle: {
                       font: "10px Roboto",
                       fillColor: "white"
                   },
                   backgroundStyle: {
                       fillColor: "rgba(0,153,204,0.8)"
                   }
               },
           },
           data: {
               preloaded: netchartdata   
               
           }
       });
       function clickEvent(event, node) {
    	  // alert(event.clickNode.id);
    	   fetchDataModelForDevice(event.clickNode.id);
    	   $('#popup1').show();

       }
       var col = [
          "green",
          "red"
       ]

       var pieChart = new PieChart({
           parentChart: netChart,
           pie: {
               style: {
                   fillColor: "transparent",
                   sliceColors: col,
               }
           },
           data: {
               preloaded: {
                   subvalues: []
               }
           },
           labels: {
               enabled: false
           }
       });
</script>
   
                </div></div>
                <br />
              
             <br />
           <div class="container-fluid">
                <div class="col-md-1"></div>
             <div class="col-md-10">  
              
                <div class="panel panel-primary modalDialog" data-collapsed="0" id="popup1" style="display:none" >

        <div class="panel-heading">
            <div class="panel-title">
             Router Data Model
            </div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <div class="panel-body">
 <div class="panel-group" id="accordion">
                
                    <div class="panel panel-info">
                            <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" class="collapsed">
                                       <strong>SNMP</strong>
                                    </a>
                                </h4>
                              
                            </div>
                        <div id="collapse1" class="panel-collapse collapse" >
                              <div class="panel-body">
                                  <div class="panel-group" id="accordion1">
                                   <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#demo1" class="collapsed">
                                       <strong>Community</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo1" class="panel-collapse collapse" >
                      <form id="bgpTraceOptionsForm11" role="form"  method="post" class="form-horizontal form-groups-bordered">    
            
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="SNMP_Community_Name1"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Authorization :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="SNMP_Community_Authorization"></div><div class="col-sm-2"></div></div>
                   
                    <div class="form-group"><label class="col-sm-4 control-label">Clients :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="SNMP_Community_Client_Name"></div><div class="col-sm-2"></div></div>
                  <br /><br />
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                    
                  </div>
                  </form>
                      
         </div>
</div>
                                   <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#demo2" class="collapsed">
                                       <strong>Trap-Group</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo2" class="panel-collapse collapse" >
                          <form id="bgpTraceOptionsForm22" role="form"  method="post" class="form-horizontal form-groups-bordered">    
            
             <div class="panel-body">
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text2"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Version :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="SNMP_Trap_Version"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Destination-port :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="SNMP_Trap_Destination-port"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Targets :</label></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="SNMP_Trap_Target_Name"></div><div class="col-sm-2"></div></div>
               <br /><br />
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>     
             </div>
             </form>
         </div>
 </div>
                              </div>
                              </div>
                      </div>
                    </div>
                      
                 
                     <div class="panel panel-info">
                            <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed">
                                       <strong>BGP</strong>
                                    </a>
                                </h4>
                              
                            </div>
                        <div id="collapse2" class="panel-collapse collapse" >
                          <form id="bgpTraceOptionsForm" method="post" class="form-horizontal form-groups-bordered">
            
                   <div class="panel-body">
                 <!--   	 <form id="bgpTraceOptionsForm55" role="form"  method="post" class="form-horizontal form-groups-bordered">  -->   
                     <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     INET6 :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text3" name="bgp.family.inet6.labeledunicast.explicitnull" readonly="readonly"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><label class="col-sm-5 control-label">Authentication Key :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Key :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text4" name="bgp.authenticationkey" readonly="readonly"></div><div class="col-sm-2"></div></div>
                  <!-- 	</form> -->
                  <div class="panel-group" id="accordion2">
                     
                     <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#demo3" class="collapsed">
                                       <strong>Trace Options</strong>
                                    </a>
                                </h4>
                              
                            </div>
                 
                   <div id="demo3" class="panel-collapse collapse" >
                      
                  
             <div class="panel-body">
                     <div class="form-group"><label class="col-sm-5 control-label">File :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="BGP_Trace_File_Nme" name="bgp.traceoptions.file.filename" readonly="readonly"/></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><label class="col-sm-5 control-label">Flag :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="BGP_Trace_Flag_Nme" name="bgp.traceoptions.flag.name" readonly="readonly"/></div><div class="col-sm-2"></div></div>
                    <!--  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Flag Modifier :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="BGP_Trace_Flag_FlagModi" name="bgp.traceoptions.flag.detail" readonly="readonly"/></div><div class="col-sm-2"></div></div> -->
                  <br /><br />   
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" id="bgpTraceOptionsBt">
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                     </form>
                      </div>
         		</div>
         		

                                  <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion2" href="#demo4" class="collapsed">
                                       <strong>BGP-Groups</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo4" class="panel-collapse collapse" >
                <form id="bgpGroupForm" role="form"  method="post" class="form-horizontal form-groups-bordered">    
                    <input type="hidden" class="form-control"  name="bgp.bgpGroup[0].hostname" id="bgpGroupHostnameId"> 
            
                 <div class="panel-body">
                <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :<span style="color:red;font-weight: bold;font-size:16px;">*</span></label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text6" name="bgp.bgpGroup[0].name" readonly="readonly"></div><div class="col-sm-2"></div></div>
                 <br/>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Type :<span style="color:red;font-weight: bold;font-size:16px;">*</span></label>
                 
                 <div class="col-sm-4">
                    <select name="bgp.bgpGroup[0].type"  class="form-control" id="bgpGroupType">
                 	        <option>internal</option>     
                 	        <option>external</option>
                 	</select>
                 	<!-- <input type="text" class="form-control" id="Text7" name="bgp.bgpGroup[0].type" readonly="readonly"> -->
                 
                 </div>
                 <div class="col-sm-2">
                 </div>
                 </div>
                  <br/>
                  <span id="internalspan">
                 	<div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">As Number :<span style="color:red;font-weight: bold;font-size:16px;">*</span></label>
                 	<div class="col-sm-4"><input type="text" class="form-control" id="asnumber" name="bgp.bgpGroup[0].asnumber"></div><div class="col-sm-2"></div></div>
                </span>
                 <span id="externalspan">
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Local As Number :<span style="color:red;font-weight: bold;font-size:16px;">*</span></label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="localasnumber" name="bgp.bgpGroup[0].neighbor[0].localas.asnumber"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Peer As Number :<span style="color:red;font-weight: bold;font-size:16px;">*</span></label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="peerasnumber" name="bgp.bgpGroup[0].neighbor[0].peeras"></div><div class="col-sm-2"></div></div>
                 </span>
                 <br/>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Local Address :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text8" name="bgp.bgpGroup[0].localaddress" readonly="readonly"></div><div class="col-sm-2"></div></div>
                 <br/>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Authentication-Key :</label>
                 <div class="col-sm-4">
                 <input type="text" class="form-control" id="Text17" name="bgp.bgpGroup[0].authenticationKey"  readonly="readonly"></div><div class="col-sm-2"></div></div>
                 <br/>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Import :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text18" name="bgp.bgpGroup[0].timport"  style="width:340px;"></div><div class="col-sm-2"></div></div>
                 <br/>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Export :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text19" name="bgp.bgpGroup[0].texport"  style="width:340px;"></div><div class="col-sm-2"></div></div>
                 <br/>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Neighbor IP :<span style="color:red;font-weight: bold;font-size:16px;">*</span></label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text23" name="bgp.bgpGroup[0].neighbor[0].name"></div><div class="col-sm-2"></div></div>
                 <!-- <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Neighbor IP :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text24" name="bgp.bgpGroup[0].neighbor[1].name"  readonly="readonly"></div><div class="col-sm-2"></div></div> -->  
                   
                      <br /><br />
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left"  id="clearBgpGroupButton">
						Clear<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" id="bgpGroupFormBt">
						Create New Group<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                      </div>
                    </form>
                   </div>
         </div>
 </div>
                                      </div>
                              </div>
                              </div>
 
                    <div class="panel panel-info">
                            <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed">
                                       <strong>Interface</strong>
                                    </a>
                                </h4>
                              
                            </div>
                        <div id="collapse3" class="panel-collapse collapse" >
                              <div class="panel-body">
                                  <div class="panel-group" id="accordion3">
                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo5" class="collapsed">
                                       <strong>GE-0/1/1</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo5" class="panel-collapse collapse" >
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text5"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text11"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text13"></div><div class="col-sm-2"></div></div>
                  <br /><br />
                  <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                      
         </div>
</div>
                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo6" class="collapsed">
                                       <strong>GE-0/1/2</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo6" class="panel-collapse collapse" >
             <div class="panel-body">
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text15"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text29"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text37"></div><div class="col-sm-2"></div></div>
                  <br /><br />
             <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>     
             </div>
         </div>
 </div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo7" class="collapsed">
                                       <strong>GE-0/2/1</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo7" class="panel-collapse collapse" >
             <div class="panel-body">
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Interface Inactive :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text43"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Desciption :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text38"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">MTU :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text41"></div><div class="col-sm-2"></div></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Encapsulation :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text42"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text39"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                <div class="col-sm-4"><input type="text" class="form-control" id="Text40"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text44"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text45"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text46"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text47"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text48"></div><div class="col-sm-2"></div></div>
                  <br /><br />
             <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>     
             </div>
         </div>
 </div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo8" class="collapsed">
                                       <strong>XE-0/3/0</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo8" class="panel-collapse collapse" >
             <div class="panel-body">
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Interface Inactive :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text49"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Desciption :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text50"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Native VLAN Id :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text60"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">MTU :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text51"></div><div class="col-sm-2"></div></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Encapsulation :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text52"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text53"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                <div class="col-sm-4"><input type="text" class="form-control" id="Text54"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">VLAN Id :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text55"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text56"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV6 :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text57"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IS-IS :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text58"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">MPLS :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text59"></div><div class="col-sm-2"></div></div>
                  <br /><br />
             <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>     
             </div>
         </div>
 </div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo9" class="collapsed">
                                       <strong>XE-0/3/1</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo9" class="panel-collapse collapse" >
             <div class="panel-body">
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Interface Inactive :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text61"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Desciption :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text62"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Native VLAN Id :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text63"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">MTU :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text64"></div><div class="col-sm-2"></div></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Encapsulation :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text65"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text66"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                <div class="col-sm-4"><input type="text" class="form-control" id="Text67"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">VLAN Id :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text68"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text69"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV6 :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text70"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IS-IS :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text71"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                  <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">MPLS :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text72"></div><div class="col-sm-2"></div></div>
                  <br /><br />
             <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>     
             </div>
         </div>
 </div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo10" class="collapsed">
                                       <strong>EM-0</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo10" class="panel-collapse collapse" >
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text73"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text74"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text75"></div><div class="col-sm-2"></div></div>
                  <br /><br />
                  <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                      
         </div>
</div>
                                  
                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo11" class="collapsed">
                                       <strong>EM-1</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo11" class="panel-collapse collapse" >
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text76"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text77"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text78"></div><div class="col-sm-2"></div></div>
                  <br /><br />
                    <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                      
         </div>
</div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo12" class="collapsed">
                                       <strong>EM-2</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo12" class="panel-collapse collapse" >
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text79"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text80"></div><div class="col-sm-2"></div></div>
                  <br /><br />
                   <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                      
         </div>
</div>
                                   
                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo13" class="collapsed">
                                       <strong>EM-3</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo13" class="panel-collapse collapse" >
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text82"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text83"></div><div class="col-sm-2"></div></div>
                  <br /><br />
                    <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                      
         </div>
</div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo14" class="collapsed">
                                       <strong>EM-4</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo14" class="panel-collapse collapse" >
             <div class="panel-body">
                 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text81"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text84"></div><div class="col-sm-2"></div></div>
                     <br /><br />
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                     </div>
                      
         </div>
</div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo15" class="collapsed">
                                       <strong>FXP0</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo15" class="panel-collapse collapse" >
             <div class="panel-body">
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text85"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text86"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text87"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV6 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text88"></div><div class="col-sm-2"></div></div>
                      <br /><br />
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                   </div>
         </div>
 </div>

                                <div class="panel panel-success">
                                   <div class="panel-heading">
                              
                                   <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion3" href="#demo16" class="collapsed">
                                       <strong>IRB</strong>
                                    </a>
                                </h4>
                              
                            </div>
                   <div id="demo16" class="panel-collapse collapse" >
             <div class="panel-body">
                 <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                 <div class="col-sm-4"><input type="text" class="form-control" id="Text89"></div><div class="col-sm-2"></div></div>
                 <div class="form-group"><label class="col-sm-4 control-label">Sub-Interface :</label></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text90"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><label class="col-sm-5 control-label">Family :</label></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV4 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text91"></div><div class="col-sm-2"></div></div>
                    <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">IPV6 :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text92"></div><div class="col-sm-2"></div></div>
                 <br /><br />
                 <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                  </div>
         </div>
 </div>

                              </div>
                              </div>
                              </div>
                      </div>
                    
                   <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed">
                                     <strong>Syslog</strong>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" >
                                <form role="form" class="form-horizontal form-groups-bordered">
                                <div class="panel-body">
                                 <div class="form-group"><label class="col-sm-3 control-label">Syslog:</label></div>
                                  <div class="form-group"><label class="col-sm-4 control-label">User:</label></div>

                       <div class="form-group">
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text1"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Contents :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text96"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text97"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Emergency :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text98"></div><div class="col-sm-2"></div></div>
                     </div>
                     <div class="form-group"><label class="col-sm-4 control-label">Host :</label></div>
                      <div class="form-group">          
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text99"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Contents :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text100"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text101"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Notice :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text102"></div><div class="col-sm-2"></div></div>
                     </div>
                     <div class="form-group"><label class="col-sm-4 control-label">File :</label></div>
                        <div class="form-group">          
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text103"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Contents :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text104"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text105"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Error :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text106"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text107"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Info :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text108"></div><div class="col-sm-2"></div></div>

                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text109"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Contents :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text110"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text111"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Any :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text112"></div><div class="col-sm-2"></div></div>
                    
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text113"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Contents :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text114"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text116"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">          Info :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text115"></div><div class="col-sm-2"></div></div>
                     </div>
<br /><br />         
                  <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>

                                </div>
                                </form>
                            </div>
                        </div>

                  <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" class="collapsed">
                                    <strong>Netconf</strong> 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" >
                                <form role="form" class="form-horizontal form-groups-bordered">
                                <div class="panel-body">
                                 <div class="form-group"><label class="col-sm-3 control-label">Netconf :</label></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">SSH :</label></div>
					             <div class="form-group">
                    <div class="col-sm-3"></div><label class="col-sm-3 control-label">Port :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text117"></div><div class="col-sm-2"></div>
     </div>  
                           <br /><br />         
                  <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>   
                                </div>
                                    </form>
                            </div>
                        </div>

                  <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" class="collapsed">
                                   <strong>VRF/ Routing Instances</strong> 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse" >
                                <form role="form" class="form-horizontal form-groups-bordered">
                                <div class="panel-body">
                                 <div class="form-group"><label class="col-sm-3 control-label">Routing Instances :</label></div>
                                    <div class="form-group"><label class="col-sm-4 control-label">Instance:</label></div>
                                <div class="form-group">
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                      <div class="col-sm-4"><input type="text" class="form-control" id="Text118"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                      <div class="col-sm-4"><input type="text" class="form-control" id="Text119"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Instance Type :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text120"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Route Distinguisher :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text121"></div><div class="col-sm-2"></div></div> 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">vrf Import :</label>
                      <div class="col-sm-4"><input type="text" class="form-control" id="Text122"></div><div class="col-sm-2"></div></div> 
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">vrf Export :</label>
                      <div class="col-sm-4"><input type="text" class="form-control" id="Text123"></div><div class="col-sm-2"></div></div>  
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">vrf TableLabel :</label>
                      <div class="col-sm-4"><input type="text" class="form-control" id="Text124"></div><div class="col-sm-2"></div></div>           
                </div> 
                                <div class="form-group">
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                      <div class="col-sm-4"><input type="text" class="form-control" id="Text125"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text126"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Instance Type :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text127"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Routing Interfaces :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text128"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text129"></div><div class="col-sm-2"></div></div>  
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text130"></div><div class="col-sm-2"></div></div>
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">     Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text131"></div><div class="col-sm-2"></div></div>
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Route Distinguisher :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text132"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf Import :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text133"></div><div class="col-sm-2"></div></div> 
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf Export :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text134"></div><div class="col-sm-2"></div></div> 
                      <div class="form-group"> <div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf TableLabel :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text135"></div><div class="col-sm-2"></div></div>             
              </div>
                                <div class="form-group">
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text136"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text137"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Instance Type :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text138"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Route Distinguisher :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text139"></div><div class="col-sm-2"></div></div> 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf Import :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text140"></div><div class="col-sm-2"></div></div> 
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf Export :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text141"></div><div class="col-sm-2"></div></div> 
                      <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf TableLabel :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text142"></div><div class="col-sm-2"></div></div>          
                </div>  
                      <div class="form-group">
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text143"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text144"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Instance Type :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text145"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Route Distinguisher :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text146"></div><div class="col-sm-2"></div></div> 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf Import :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text147"></div><div class="col-sm-2"></div></div>  
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf Export :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text148"></div><div class="col-sm-2"></div></div> 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf TableLabel :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text149"></div><div class="col-sm-2"></div></div>          
                </div>
                      <div class="form-group">
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text150"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Description :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text151"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Instance Type :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text152"></div><div class="col-sm-2"></div></div>
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Route Interface :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text153"></div><div class="col-sm-2"></div></div> 
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Name :</label>
                     <div class="col-sm-4"><input type="text" class="form-control" id="Text154"></div><div class="col-sm-2"></div></div>  
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Route Distinguisher :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text155"></div><div class="col-sm-2"></div></div>  
                     <div class="form-group"><div class="col-sm-3"></div><label class="col-sm-3 control-label">Vrf TableLabel :</label>
                    <div class="col-sm-4"><input type="text" class="form-control" id="Text156"></div><div class="col-sm-2"></div></div>           
                </div>

                                    <br /><br />         
                  <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                                </div> 
                                    </form>
                            </div>
                        </div>

                  <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse7" class="collapsed">
                                   <strong>Hostname</strong> 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse7" class="panel-collapse collapse" >
                                <form role="form" class="form-horizontal form-groups-bordered">
                                <div class="panel-body">
                                 <div class="form-group">
						<label class="col-sm-3 control-label">Hostname :</label>
					</div>
					<div class="form-group">
                    <label class="col-sm-4 control-label">System:</label>
                    
                </div>
                    <div class="form-group">
                   <div class="col-sm-3"></div><label class="col-sm-3 control-label">Hostname :</label>
                  <div class="col-sm-4"><input type="text" class="form-control" id="Text157"></div><div class="col-sm-2"></div>
                   
                </div>  
                     
                                    <br /><br />         
                  <div class="form-group" style="text-align:center;">
                   
                       <button type="button" class="btn btn-green btn-icon icon-left" >
						Save<i class="entypo-floppy"></i>
					</button>  &nbsp;&nbsp;&nbsp;&nbsp;   
                      <button type="button" class="btn btn-blue btn-icon icon-left" >
						Sync<i class="entypo-arrows-ccw"></i>
					</button>
                  </div>
                        </div>
                                    </form>
                 </div>

                    </div>
                    
                     <span id="errorMessage" style="color:blue;font-weight: bold;">${ApplicationMessage}</span>
           <hr/>
          <ff:form id="PathFinderFormId" commandName="pathFinderVO" method="post" action="${pageContext.request.contextPath}/ecode-data/findL2Path">
          <table style="color:black;">
             <tbody>
             
            <tr style="color:black;">
                    <td>
                    Source Router  :
                    <ff:select class="form-control" style="width: 200px;" path="sourceRouter">
                                 <ff:options items="${sourceRouterList}"/>
                            </ff:select>
            
                    </td>   
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>
                    Destination Router  :
                    <ff:select class="form-control" style="width: 200px;" path="destRouter">>
                    		<ff:options items="${destRouterList}"/>
                      </ff:select>
            
                    </td>  
                    
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                      <td>
                    Topology Type  :
                    <ff:select class="form-control" style="width: 200px;" path="topoType">>
                    		<ff:options items="${topoTyperList}"/>
                      </ff:select>
            
                    </td>  
                    </tr>
                    <tr>
                     <td colspan="5" style="text-align: right;">
                      <button type="button" class="btn btn-blue btn-icon icon-left" style="margin-top:18px;" id="pathFindId">
						Find Path<i class="entypo-arrows-ccw"></i>
					</button>
            
                    </td>        
              </tr>              
          </table>   
          </ff:form>               
                            
     <hr/>           
   <table class="table table-hover" style="color:black;">
        <thead>
            <tr style="background-color: rgb(48, 54, 65);">
                <th colspan="2" style="font-size: 12px;">
                  <font style="color:white;">All Available Path(s) :-</font> 
                </th>
            </tr>
        </thead>
        <tbody>
 			<c:forEach var= "possiblePath" items = "${allPossiblePaths}" varStatus="status">     
            <tr style="color:black;">
                <td>${status.count}.</td>
                <td>
                <c:forEach var= "item" items = "${possiblePath}">
                	${item}   --> 
                </c:forEach>
                 </td>
            </tr>
            </c:forEach>
            
            
        </tbody>
    </table>
               
					 </div>
                    
                    
                      </div>
                    </div> 
           
                    </div>
 
               <div class="col-md-1"></div>
</div></div></div>
       
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/assets/js/rickshaw/rickshaw.min.css">

    <!-- Bottom Scripts -->
    <script src="${pageContext.request.contextPath}/assets/js/gsap/main-gsap.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/bootstrap.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/joinable.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/resizeable.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-api.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-chat.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-custom.js"></script>
    <script src="${pageContext.request.contextPath}/assets/js/neon-demo.js"></script>

</body>

</html>

