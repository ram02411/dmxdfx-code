

import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

public class Exec12 {
	public static void main(String[] arg) {
		Session session=null;
		Channel channel =null;
		try {
			JSch jsch = new JSch();

			String host = null;
			if (arg.length > 0) {
				host = arg[0];
			}
			// else{
			host = "cisco@192.168.1.70";
			// }
			String user = host.substring(0, host.indexOf('@'));
			host = host.substring(host.indexOf('@') + 1);
			 session = jsch.getSession(user, host, 22);
			// username and password will be given via UserInfo interface.
			UserInfo ui = new MyUserInfo();
			session.setUserInfo(ui);
			session.connect();
			 channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand("show ipv6 neighbors");
			// X Forwarding
			// channel.setXForwarding(true);

			// channel.setInputStream(System.in);
			channel.setInputStream(null);

			// channel.setOutputStream(System.out);

			// FileOutputStream fos=new FileOutputStream("/tmp/stderr");
			// ((ChannelExec)channel).setErrStream(fos);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();

			channel.connect();
			StringBuilder output=new StringBuilder();

			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					//System.out.print("p= "+new String(tmp, 0, i));
					output.append(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					if (in.available() > 0)
						continue;
					System.out.println("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
					ee.printStackTrace();
				}
			}
			//System.out.println("__(tmp)______  = "+tmp);
			System.out.println("__(output)______  = "+output);
		} catch (Exception e) {
			System.out.println(e);
		}finally{
			if(channel!=null){
				channel.disconnect();
			}
			if(session!=null){
				session.disconnect();
			}
		}
	}

	public static class MyUserInfo implements UserInfo {
		public String getPassword() {
			return passwd;
		}

		public boolean promptYesNo(String str) {
			return true;
		}

		private String passwd;

		public String getPassphrase() {
			return null;
		}

		public boolean promptPassphrase(String message) {
			return true;
		}

		public boolean promptPassword(String message) {
			passwd = "cisco";
			return true;
		}

		public void showMessage(String message) {
			// JOptionPane.showMessageDialog(null, message);
		}

	}
}
