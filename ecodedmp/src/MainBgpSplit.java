import java.util.ArrayList;
import java.util.List;

public class MainBgpSplit {
	
	public static void main(String[] args) {
		String selectedVpnPath = "IRLARCPGPAR001-AG1-1(172.25.0.24),IRLARCPGESR004-920iBot1R03(172.25.0.37),IRLARCPGES006-ACX2200TOP(172.25.0.151),IRLARCPGESR005-920_O_1R04(172.25.0.40)";
		String stringTokens[]=selectedVpnPath.split(",");
		List<String> ipAddressList=new ArrayList<>();
		for(String token:stringTokens){
			String ipAddress=token.substring(token.indexOf("(")+1,token.length()-1);
			ipAddressList.add(ipAddress);
		}
		System.out.println(ipAddressList);
	}

}
