package GlobalAf;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.cisco.container.model.bgp.GlobalAf;

/**
 * 
 * @author ecode
 *
 */
public class GlobalAfs {
	
	private List<GlobalAf>  globalAf;

	@XmlElement(name="global-afs",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public List<GlobalAf> getGlobalAf() {
		return globalAf;
	}

	public void setGlobalAf(List<GlobalAf> globalAf) {
		this.globalAf = globalAf;
	}

	@Override
	public String toString() {
		return "GlobalAfs [globalAf=" + globalAf + "]";
	}
	
}
