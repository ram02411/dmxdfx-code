package com.dom.parser;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class DOMParser {
	


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 //Get the DOM Builder Factory
		    DocumentBuilderFactory factory =
		        DocumentBuilderFactory.newInstance();
		    DocumentBuilder builder;
			try {
				builder = factory.newDocumentBuilder();
				Document document =
					      builder.parse(ClassLoader.getSystemResourceAsStream("pconf.xml"));
				 NodeList nodeList = document.getDocumentElement().getChildNodes();
				 for (int i = 0; i < nodeList.getLength(); i++) {
					       //We have encountered an <employee> tag.
					       Node node = nodeList.item(i);
					       	System.out.println("Node - "+node);
				 }	       

				System.out.println("+___Cool_____");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
