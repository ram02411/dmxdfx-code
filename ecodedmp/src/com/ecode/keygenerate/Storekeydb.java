package com.ecode.keygenerate;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Storekeydb {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost:3306/ecodedb";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "ecode123";
	   
	   public static String keyfromdb() {
			String value = null;
			ResultSet rs=null;
			String key = null;
			try{
				System.out.println("Process on1");
				Class.forName("com.mysql.jdbc.Driver");
				Connection conn = DriverManager.getConnection(DB_URL,USER,PASS);
			

				Statement st = conn.createStatement();

				System.out.println("Process on2");
				 
				System.out.println(st
						.executeQuery("SELECT * FROM licensekey_tbl"));

				int check=0;
				Statement stmt = conn.createStatement();
				String query = "SELECT * FROM licensekey_tbl;";
				rs = stmt.executeQuery(query);
				if(!(rs.next())){
					key=null;
				}
				else{
					key=rs.getObject(1).toString();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			return key;
	   }
	   
	public static void appendkey(String key){
		String status="notverified";
		Connection conn = null;
		   Statement stmt = null;
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      //System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);

		      //STEP 4: Execute a query
		     // System.out.println("Creating statement...");
		      stmt = conn.createStatement();
		      String sql;
		      status="'"+status+"'";
		      key="'"+key+"'";
		     
		      sql = "insert into licensekey_tbl values("+key+",'',"+status+")";
		      System.out.println("sql statement :"+sql);
		      stmt.executeUpdate(sql);

		      //STEP 5: Extract data from result set
		      
		      stmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   //System.out.println("Goodbye!");
		}


}
