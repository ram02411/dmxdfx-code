package com.ecode.cisco.command;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.oro.text.regex.MalformedPatternException;

import com.ecode.topology.viewer.data.device.Router;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.juniper.container.data.model.configuration.protocols.Bgp;
import com.juniper.container.data.model.configuration.protocols.BgpGroup;

import expect4j.Closure;
import expect4j.Expect4j;
import expect4j.ExpectState;
import expect4j.matches.Match;
import expect4j.matches.RegExpMatch;
 
public class SSHClient {
 
    private static final int COMMAND_EXECUTION_SUCCESS_OPCODE = 0;
    private static String ENTER_CHARACTER = "\r";
    private static final int SSH_PORT = 22;
    private List<String> lstCmds = new ArrayList<String>();
    private static String[] linuxPromptRegEx = new String[]{"\\>","#", "~#","%",">"};
 
    private Expect4j expect = null;
    private StringBuilder buffer = new StringBuilder();
    private String userName;
    private String password;
    private String host;
    private  Session session;
    ChannelShell channel;
 
    /**
     *
     * @param host
     * @param userName
     * @param password
     */
    public SSHClient(String host, String userName, String password) {
        this.host = host;
        this.userName = userName;
        this.password = password;
    }
    /**
     *
     * @param cmdsToExecute
     */
    public String execute(List<String> cmdsToExecute) {
        this.lstCmds = cmdsToExecute;
 
        Closure closure = new Closure() {
            public void run(ExpectState expectState) throws Exception {
                buffer.append(expectState.getBuffer());
            }
        };
        List<Match> lstPattern =  new ArrayList<Match>();
        for (String regexElement : linuxPromptRegEx) {
            try {
                Match mat = new RegExpMatch(regexElement, closure);
                lstPattern.add(mat);
            } catch (MalformedPatternException e) {
                e.printStackTrace();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
 
        try {
            expect = SSH();
            boolean isSuccess = true;
            for(String strCmd : lstCmds) {
                isSuccess = isSuccess(lstPattern,strCmd);
                if (!isSuccess) {
                    isSuccess = isSuccess(lstPattern,strCmd);
                }
            }
 
            checkResult(expect.expect(lstPattern));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            closeConnection();
        }
        return buffer.toString();
    }
    /**
     *
     * @param objPattern
     * @param strCommandPattern
     * @return
     */
    private boolean isSuccess(List<Match> objPattern,String strCommandPattern) {
        try {
            boolean isFailed = checkResult(expect.expect(objPattern));
 
            if (!isFailed) {
                expect.send(strCommandPattern);
                expect.send(ENTER_CHARACTER);
                return true;
            }
            return false;
        } catch (MalformedPatternException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    /**
     *
     * @param hostname
     * @param username
     * @param password
     * @param port
     * @return
     * @throws Exception
     */
    private Expect4j SSH() throws Exception {
        JSch jsch = new JSch();
       session= jsch.getSession(userName, host, SSH_PORT);
        if (password != null) {
            session.setPassword(password);
        }
        Hashtable<String,String> config = new Hashtable<String,String>();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect(60000);
        channel  = (ChannelShell) session.openChannel("shell");
        Expect4j expect = new Expect4j(channel.getInputStream(), channel.getOutputStream());
        channel.connect();
        return expect;
    }
    
    public void disconnectionSession(){
    	if(channel!=null){
    		channel.disconnect();
    	}
    	if(session!=null){
    		session.disconnect();
    		System.out.println("Session is disconnected!!!!!");
    	}
    }
    /**
     *
     * @param intRetVal
     * @return
     */
    private boolean checkResult(int intRetVal) {
        if (intRetVal == COMMAND_EXECUTION_SUCCESS_OPCODE) {
            return true;
        }
        return false;
    }
    /**
     *
     */
    private void closeConnection() {
        if (expect!=null) {
            expect.close();
        }
    }
    
    
    /**
     *  Command to create BGP group for Cisco Router
     *  using CLI 
     * @param router
     * @param bgp
     * @param withPolicy
     * @param commit
     * @return
     */
    public String  exportImportCiscoBgpGroup(Router router,Bgp bgp,boolean withPolicy,boolean commit){
    	 //Fetching first group
    	 BgpGroup bgpGroup=bgp.getBgpGroup().get(0);
    	 SSHClient ssh =null;
         List<String> cmdsToExecute = new ArrayList<String>();
         String outputLog="";
         try {
         ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
         cmdsToExecute.add("config");
         if(withPolicy) {
        	 cmdsToExecute.add("route-policy IMPORT_PASS_ALL");///PLEASE 
        	 cmdsToExecute.add("pass");
        	 cmdsToExecute.add("end-policy");
        	 
        	 cmdsToExecute.add("route-policy EXPORT_PASS_ALL");///PLEASE 
        	 cmdsToExecute.add("pass");
        	 cmdsToExecute.add("end-policy");
         }
         cmdsToExecute.add("neighbor "+bgpGroup.getNeighbor().get(0).getName());
         cmdsToExecute.add("address-family ipv4 unicast");
         cmdsToExecute.add("route-policy "+bgpGroup.getIimport().get(0)+" in");
         cmdsToExecute.add("route-policy "+bgpGroup.getExport().get(0)+" out");
         cmdsToExecute.add("!");
         cmdsToExecute.add("!");
         cmdsToExecute.add("exit");
         if(commit){
          cmdsToExecute.add("commit");
         } 
         System.out.println("__Please wait CLI command is getting executed on Cisco device!__ ");
         outputLog = ssh.execute(cmdsToExecute);
         System.out.println(outputLog);
         
     	}catch(Exception exe){
     		exe.printStackTrace();
     	}finally{
     		if(ssh!=null)
     		  ssh.disconnectionSession();
     	}
         return outputLog;
    }
    
    /**
     *  Command to create BGP group for Cisco Router
     *  using CLI 
     * @param router
     * @param bgp
     * @param withPolicy
     * @param commit
     * @return
     */
    public String createCiscoBgpGroup(Router router,Bgp bgp,boolean withPolicy,boolean commit){
    	 //Fetching first group
    	 BgpGroup bgpGroup=bgp.getBgpGroup().get(0);
    	 SSHClient ssh =null;
         List<String> cmdsToExecute = new ArrayList<String>();
         String outputLog="";
         try {
         ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
         cmdsToExecute.add("config");
         if(withPolicy) {
        	 cmdsToExecute.add("route-policy IMPORT_PASS_ALL");///PLEASE 
        	 cmdsToExecute.add("pass");
        	 cmdsToExecute.add("end-policy");
        	 
        	 cmdsToExecute.add("route-policy EXPORT_PASS_ALL");///PLEASE 
        	 cmdsToExecute.add("pass");
        	 cmdsToExecute.add("end-policy");
         }
         cmdsToExecute.add("router bgp "+bgpGroup.getNeighbor().get(0).getLocalas().getAsnumber());
         cmdsToExecute.add("bgp router-id "+router.getIp()); 
         cmdsToExecute.add("address-family ipv4 unicast");
         cmdsToExecute.add("network 192.168.200.0/24");/////PLEASE
         cmdsToExecute.add("!");
         cmdsToExecute.add("neighbor "+bgpGroup.getNeighbor().get(0).getName());
         cmdsToExecute.add("remote-as "+bgpGroup.getNeighbor().get(0).getPeeras());
         cmdsToExecute.add("address-family ipv4 unicast");
         cmdsToExecute.add("route-policy "+bgpGroup.getIimport().get(0)+" in");
         cmdsToExecute.add("route-policy "+bgpGroup.getExport().get(0)+" out");
         cmdsToExecute.add("!");
         cmdsToExecute.add("!");
         cmdsToExecute.add("exit");
         if(commit){
          cmdsToExecute.add("commit");
         } 
         System.out.println("__Please wait CLI command is getting executed on Cisco device!__ ");
         outputLog = ssh.execute(cmdsToExecute);
         System.out.println(outputLog);
         
     	}catch(Exception exe){
     		exe.printStackTrace();
     	}finally{
     		if(ssh!=null)
     		  ssh.disconnectionSession();
     	}
         return outputLog;
    }
    
    public String createCiscoRoutePolicy(Router router,Bgp bgp){
   	 //Fetching first group
    	BgpGroup bgpGroup=bgp.getBgpGroup().get(0);
    	SSHClient ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
        List<String> cmdsToExecute = new ArrayList<String>();
        cmdsToExecute.add("config");
        //cmdsToExecute.add("router bgp 9000");
        if(bgpGroup.getIimport()!=null && bgpGroup.getIimport().size()>0){
        	cmdsToExecute.add("route-policy "+bgpGroup.getIimport().get(0));
        	cmdsToExecute.add("if protocol is static then done endif end-policy");
        }	
        cmdsToExecute.add("exit");
        cmdsToExecute.add("commit");
        String outputLog = ssh.execute(cmdsToExecute);
        System.out.println(outputLog);
        ssh.disconnectionSession();
        return outputLog;
   }
    
    public String applyCiscoImportExport(Router router,Bgp bgp){
     	 //Fetching first group
      	BgpGroup bgpGroup=bgp.getBgpGroup().get(0);
      	SSHClient ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
          List<String> cmdsToExecute = new ArrayList<String>();
          cmdsToExecute.add("config");
          //cmdsToExecute.add("router bgp 9000");
          if(bgpGroup.getIimport()!=null && bgpGroup.getIimport().size()>0){
          	cmdsToExecute.add("router bgp "+bgpGroup.getNeighbor().get(0).getLocalas());
          	cmdsToExecute.add("neighbor "+bgpGroup.getNeighbor().get(0).getName());
          	cmdsToExecute.add("address-family ipv4 unicast");
           if(bgpGroup.getIimport()!=null && bgpGroup.getIimport().size()>0){
           	cmdsToExecute.add("route-policy "+bgpGroup.getIimport().get(0)+" in");
           	cmdsToExecute.add("route-policy "+bgpGroup.getExport().get(0)+" out");
           }	
          }	
          cmdsToExecute.add("exit");
          cmdsToExecute.add("commit");
          String outputLog = ssh.execute(cmdsToExecute);
          System.out.println(outputLog);
          ssh.disconnectionSession();
          return outputLog;
     }
    
    public String applyCiscoRoutePolicy(Router router,Bgp bgp){
      	 //Fetching first group
       	BgpGroup bgpGroup=bgp.getBgpGroup().get(0);
       	SSHClient ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
           List<String> cmdsToExecute = new ArrayList<String>();
           cmdsToExecute.add("config");
           //cmdsToExecute.add("router bgp 9000");
           if(bgpGroup.getIimport()!=null && bgpGroup.getIimport().size()>0){
           	cmdsToExecute.add("router bgp "+bgpGroup.getNeighbor().get(0).getLocalas());
           	cmdsToExecute.add("neighbor "+bgpGroup.getNeighbor().get(0).getName());
           	cmdsToExecute.add("address-family ipv4 unicast");
            if(bgpGroup.getIimport()!=null && bgpGroup.getIimport().size()>0){
            	cmdsToExecute.add("route-policy "+bgpGroup.getIimport().get(0)+" in");
            	cmdsToExecute.add("route-policy "+bgpGroup.getExport().get(0)+" out");
            }	
           }	
           cmdsToExecute.add("exit");
           cmdsToExecute.add("commit");
           String outputLog = ssh.execute(cmdsToExecute);
           System.out.println(outputLog);
           ssh.disconnectionSession();
           return outputLog;
      }
    
    
    /**
     *
     * @param args
     */
    public static void main(String[] args) {
    	 SSHClient ssh=null;
    	try {
        System.out.println("_#####Executing command  Please wait#######");
    	//ssh = new SSHClient("192.168.1.80", "cisco", "cisco");
        List<String> cmdsToExecute = new ArrayList<String>();
        
       /* cmdsToExecute.add("config");
    	cmdsToExecute.add("route-policy TEST_PASS_ALL");
    	cmdsToExecute.add("pass");
    	cmdsToExecute.add("end-policy");
    	cmdsToExecute.add("!");
   	    cmdsToExecute.add("exit");
        cmdsToExecute.add("commit");
        String outputLog = ssh.execute(cmdsToExecute);
        System.out.println(outputLog);
        ssh.disconnectionSession();*/
     
        ssh = new SSHClient("192.168.60.2", "root", "ecode123");
         cmdsToExecute = new ArrayList<String>();
         cmdsToExecute.add("cli");
         cmdsToExecute.add("show version");
           //cmdsToExecute.add("router bgp 65001");
       //cmdsToExecute.add("bgp log-neighbor-changes");
        //cmdsToExecute.add("network 4.4.4.0");
       //cmdsToExecute.add("end-policy");
       // cmdsToExecute.add("neighbor 192.168.100.10 remote-as 65001");
        //cmdsToExecute.add("neighbor 192.168.100.40 remote-as 65001");
        
       /* cmdsToExecute.add("network 192.168.200.0/24");
        cmdsToExecute.add("!");
        cmdsToExecute.add("neighbor 192.168.1.50");
        cmdsToExecute.add("remote-as 1234");
        cmdsToExecute.add("address-family ipv4 unicast");
        cmdsToExecute.add("route-policy TEST_PASS_ALL in");
        cmdsToExecute.add("route-policy TEST_PASS_ALL out");*/
        
       
        
        
        System.out.println("__Please wait processing is going on__ ");
        String outputLog = ssh.execute(cmdsToExecute);
        System.out.println(outputLog);
        
    	}catch(Exception exe){
    		exe.printStackTrace();
    	}finally{
    		if(ssh!=null)
    		  ssh.disconnectionSession();
    	}
      
    }
}