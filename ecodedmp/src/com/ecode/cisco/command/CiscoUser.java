package com.ecode.cisco.command;

import com.jcraft.jsch.UserInfo;

/**
 * 
 * @author ecode
 *
 */
public class CiscoUser implements UserInfo {
	
	public CiscoUser() {
	}

	public CiscoUser(String passwd) {
		this.passwd = passwd;
	}

	public String getPassword() {
		return passwd;
	}

	public boolean promptYesNo(String str) {
		return true;
	}

	private String passwd;

	public String getPassphrase() {
		return null;
	}

	public boolean promptPassphrase(String message) {
		return true;
	}

	public boolean promptPassword(String message) {
		passwd = "cisco";
		return true;
	}

	public void showMessage(String message) {
		// JOptionPane.showMessageDialog(null, message);
	}

}
