package com.ecode.cisco.command;

public class FindNeighborCommandMain {
	
	public static void main(String[] args) {
		/*String config="configure\n"+
			"router bgp 1234\n"+
			"address-family ipv4 unicast\n"+
			"neighbor 192.168.1.50\n"+
			"remote-as 1234\n"+
			"address-family ipv4 unicast\n"+
			"exit\n"+
			"commit\n"+
			"exit";*/
		   String config="show ipv6 neighbors";
		   	 CiscoRouter ciscoRouter=new CiscoRouter("cisco","cisco", "192.168.1.80");
			  String output=CiscoRouterCliCommandExecutor.executeCommand(ciscoRouter, config);
			  System.out.println("________________________________________________________________");
			  System.out.println(output);
			  System.out.println("________________________________________________________________");
			  System.out.println("Parsing the output..............................");
			  String lines[]=output.split("\n");
			  for(String line:lines){
				  if(line!=null && line.trim().length()==0 || line.equalsIgnoreCase("\n\n"))
					  continue;
				  if(line!=null && line.length()>2)
					  line=line.substring(0,line.length()-2);
				  System.out.println(line);
			  }
			  System.out.println("___Command is completed!!!");
		    	
		    
	}

}
