package com.ecode.cisco.command;

/**
 * 
 * @author ecode
 *
 */
public class CiscoRouterCliCommandExecutor {
	
	    public static String executeCommand(CiscoRouter ciscoRouter,String command){
	    	return	 ciscoRouter.executeCommand(command);
	    }
	
}
