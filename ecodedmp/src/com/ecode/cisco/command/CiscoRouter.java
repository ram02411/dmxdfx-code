package com.ecode.cisco.command;

import java.io.InputStream;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

/**
 * 
 * @author ecode
 *
 */
public class CiscoRouter {

	private String username;
	private String password;
	private String hostname;
	

	public CiscoRouter(String username, String password, String hostname) {
		this.username = username;
		this.password = password;
		this.hostname = hostname;
	}

	/**
	 * method to fire cisco cli command
	 * @param command
	 * @return
	 */
	public String executeCommand(String command) {

		Session session=null;
		Channel channel =null;
		StringBuilder output=new StringBuilder();
		try {
			JSch jsch = new JSch();
			 session = jsch.getSession(username, hostname, 22);
			// username and password will be given via UserInfo interface.
			UserInfo ui = new CiscoUser(password);
			session.setUserInfo(ui);
			session.connect();
			 channel = session.openChannel("exec");
			((ChannelExec) channel).setCommand(command);
			channel.setInputStream(null);
			// ((ChannelExec)channel).setErrStream(fos);
			((ChannelExec) channel).setErrStream(System.err);

			InputStream in = channel.getInputStream();
			channel.connect();
			
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					//System.out.print("p= "+new String(tmp, 0, i));
					output.append(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					if (in.available() > 0)
						continue;
					System.out.println("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
					ee.printStackTrace();
				}
			}
			//System.out.println("__(tmp)______  = "+tmp);
			//System.out.println("__(output)______  = "+output);
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(channel!=null){
				channel.disconnect();
			}
			if(session!=null){
				session.disconnect();
			}
		}
		return output.toString();
	}

}
