package com.ecode.cisco.command;

/**
 * 
 * @author ecode
 *
 */
public interface CiscoCommandConstant {
	
	public static String SHOW_IPV6_NEIGHBORS="show ipv6 neighbors";

}
