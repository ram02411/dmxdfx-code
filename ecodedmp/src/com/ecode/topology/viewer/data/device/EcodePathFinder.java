package com.ecode.topology.viewer.data.device;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * @author ecode
 *
 */
public class EcodePathFinder {

    private  final String START;
    private  final String END;
    private  List<List<String>> allPossiblePath=new ArrayList<>();
    private EcodeGraph graph = new EcodeGraph();


	public EcodePathFinder(String source,String destination){
    	START=source;
    	END=destination;
    }
    
    
    
    public void addNewworkPath(String source,String target){
    	  graph.addEdge(source,target);
    }
    
    public  List<List<String>> generateAllPossibleNetworks(){
    	  LinkedList<String> visited = new LinkedList<>();
          visited.add(START);
          depthFirst(visited);
          return allPossiblePath;
    }

    public static void main(String[] args) {
        // this graph is directional
      
    }

    private void depthFirst(LinkedList<String> visited) {
        LinkedList<String> nodes = graph.adjacentNodes(visited.getLast());
        // examine adjacent nodes
        for (String node : nodes) {
            if (visited.contains(node)) {
                continue;
            }
            if (node.equals(END)) {
                visited.add(node);
                printPath(visited);
                visited.removeLast();
                break;
            }
        }
        for (String node : nodes) {
            if (visited.contains(node) || node.equals(END)) {
                continue;
            }
            visited.addLast(node);
            depthFirst(visited);
            visited.removeLast();
        }
    }

    private void printPath(LinkedList<String> visited) {
    	List<String> onePath=new ArrayList<>();
        for (String node : visited) {
            System.out.print(node);
            System.out.print(" ");
            onePath.add(node);
        }
        System.out.println();
        allPossiblePath.add(onePath);
    }
}