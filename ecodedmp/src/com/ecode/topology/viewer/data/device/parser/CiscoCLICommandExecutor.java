package com.ecode.topology.viewer.data.device.parser;







import java.io.BufferedReader;



import java.io.IOException;



import java.io.InputStream;



import java.io.InputStreamReader;



import java.util.ArrayList;



import java.util.List;



import java.util.Properties;







import org.apache.commons.logging.Log;



import org.apache.commons.logging.LogFactory;







import com.jcraft.jsch.Channel;



import com.jcraft.jsch.ChannelExec;



import com.jcraft.jsch.JSch;



import com.jcraft.jsch.Session;







/**



 * 



 * @author java



 *



 */



public class CiscoCLICommandExecutor {



	



	 private static final Log logger = LogFactory.getLog(CiscoCLICommandExecutor.class);



	



	public static  List<String> exeCiscoCliCommand(final String hostip,final String username,final String password,final String command){

		List<String> ipv6StringList = new ArrayList<>();

		String startWith = "GigabitEthernet";

		String orStartWith = "FastEthernet";
		String status = "up";
		String statusd="down";


		String exclude = "unassigned";



		int pickLine = 0;



		//if(logger.isDebugEnabled())



		System.out.println("Executing method exeCiscoCliCommand hostip =  "+hostip+" ,username = "+username+" , password = "+password);



		try {



		 //String command = "ls -la";



         String host = hostip;



         String user = username;



         JSch jsch = new JSch();



         Session session = jsch.getSession(user, host, 22);



         Properties config = new Properties();



         config.put("StrictHostKeyChecking", "no");



         session.setConfig(config);;



         session.setPassword(password);



         session.connect();



         Channel channel = session.openChannel("exec");



         ((ChannelExec)channel).setCommand(command);



         channel.setInputStream(null);



         ((ChannelExec)channel).setErrStream(System.err);



         InputStream input = channel.getInputStream();



         channel.connect();



         System.out.println("Channel Connected to machine " + host + " server with command: " + command );



         InputStreamReader inputReader=null;



         BufferedReader bufferedReader=null;



         try{



        	 inputReader = new InputStreamReader(input);



             bufferedReader = new BufferedReader(inputReader);



             String line = null;



             while((line = bufferedReader.readLine()) != null){



            	 //System.out.println("OL = "+line);



 				if (line != null && line.trim().length() == 0 || line.equalsIgnoreCase("\n\n"))

 					continue;
 						
 				
 				if ((line != null && line.contains(status)) && (line.contains(orStartWith)||line.contains(startWith))) {

 					pickLine = 1;

 					continue;

 				}
 				if ((line != null && line.contains(statusd))  ) {

 					pickLine = 0;

 					continue;

 				}
 				
 				


 				if (pickLine == 1) {
 					if (line != null && line.contains("FE80")) {
 						String ipv6Address = line.trim();
 						if (!exclude.equalsIgnoreCase(ipv6Address))
 							ipv6StringList.add(ipv6Address);
 						pickLine = 0;
 					}
 				}



             }



             



         }catch(Throwable ex){



             ex.printStackTrace();



             if(logger.isErrorEnabled())



                 logger.error(ex.getMessage());



         }finally{



        	 if(bufferedReader!=null)



        	 bufferedReader.close();



        	 if(inputReader!=null)



             inputReader.close();



         }



         channel.disconnect();



         session.disconnect();



     }catch(Throwable ex){



        	 if(logger.isErrorEnabled())



             logger.error(ex.getMessage());



             ex.printStackTrace();



        }



		//if(logger.isDebugEnabled())



		System.out.println("_____________Command output_____ = "+ipv6StringList);



		return ipv6StringList;



	}







}



