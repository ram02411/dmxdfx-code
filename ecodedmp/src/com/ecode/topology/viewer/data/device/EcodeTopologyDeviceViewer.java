package com.ecode.topology.viewer.data.device;

import java.util.List;

/**
 * 
 * @author ecode
 *
 *
 */
public interface EcodeTopologyDeviceViewer {

	public TopologyViewerVO showNetworkTopology(List<Router> routers);

	public TopologyViewerVO showNetworkLogicalTopology(List<Router> prouters);
	public TopologyViewerVO showNetworkLogicalTopologyMpls(List<Router> prouters);

	public List<List<String>> findAllPossiblePathSourceDestination(Router source, Router destination,List<Router> prouters);

	public List<List<String>> findAllPossibleLogicalPathSourceDestination(Router source, Router destination,
			List<Router> prouters);
	
}
