package com.ecode.topology.viewer.data.device;

import java.util.ArrayList;
import java.util.List;

public class MainClass {
	
	public static void main(String[] args) {
		EcodeTopologyDeviceViewer deviceViewer=new EcodeTopologyDeviceViewerImpl();
		List<Router> routers=new ArrayList<>();
		
		//TopologyViewerVO result=deviceViewer.showNetworkTopology(routers);
		TopologyViewerVO result=deviceViewer.showNetworkLogicalTopology(routers);
		
		System.out.println("_____________________________________________________---_)((*@**@@");
		System.out.println("Output = = == " +result);
		System.out.println("_____________________________________________________---_)((*@**@@");
		System.out.println("_____________________________________________________---_)((*@**@@");
		
	}

}
