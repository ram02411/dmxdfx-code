package com.ecode.topology.viewer.data.device.parser;



import java.util.ArrayList;

import java.util.Arrays;

import java.util.Iterator;

import java.util.List;



import org.w3c.dom.Node;

import org.w3c.dom.NodeList;



import com.ecode.topology.viewer.data.device.Router;

import com.ecode.web.app.constant.ApplicationConstant;



import net.juniper.netconf.Device;

import net.juniper.netconf.XML;

import net.juniper.netconf.XMLBuilder;



public class IPV6AddressXmlParser {



	public static void main(String[] args) {

		Router router7 = new Router();

		router7.setType(ApplicationConstant.CISCO);

		router7.setHostname("CPE1");

		router7.setIp("192.168.1.70");

		router7.setUsername("cisco");

		router7.setPassword("cisco");

		System.out.println(getCiscoIpv6List(router7));

	}



	/**

	 * 

	 * @param router

	 * @return

	 */

	public static List<String> getCiscoIpv6List(Router router) {

		    List<String> ipv6StringList = new ArrayList<>();

			String ipv6NeighborCommand = "show ipv6 interface brief";

			ipv6StringList= CiscoCLICommandExecutor.exeCiscoCliCommand(router.getIp(), router.getUsername(),

					router.getPassword(), ipv6NeighborCommand);

			System.out.println("in getCiscoIpv6List show ipv6 interface brief command output "+router.getIp()+"  = "+ipv6StringList);

			return ipv6StringList;

	}



	public static List<String> getNetworkInfo(Router router) {

		Device device = null;

		List<String> ipv6List = new ArrayList<>();

		try {

			device = new Device(router.getIp(),router.getUsername(),router.getPassword(),null, 830);
			
			device.connect();

			String ipv6Address = "<get-interface-information>" + "<terse></terse>" + "</get-interface-information>";

			XMLBuilder query = new XMLBuilder();

			// XML q = query.createNewXML(ipv6Address);

			XML rpcReply = device.executeRPC(ipv6Address);

			ipv6List = parse(rpcReply);



		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			if (device != null)

				device.close();

		}

		return ipv6List;

	}



	private static List<String> parse(XML rpcReply) {

		List<String> list = Arrays.asList("physical-interface");

		List interfaceAddressList = rpcReply.findNodes(list);

		Iterator iter = interfaceAddressList.iterator();

		List<String> ipv6List = new ArrayList<String>();

		while (iter.hasNext()) {

			Node node = (Node) iter.next();

			NodeList physicalInterfaceList = node.getChildNodes();

			for (int i = 0; i < physicalInterfaceList.getLength(); i++) {

				Node logicalInterfaceNode = (Node) physicalInterfaceList.item(i);

				if (logicalInterfaceNode.getNodeName().equals("logical-interface")) {

					NodeList logicalInterfaceList = logicalInterfaceNode.getChildNodes();

					for (int p = 0; p < logicalInterfaceList.getLength(); p++) {

						Node addressfamilyNode = (Node) logicalInterfaceList.item(p);

						if (addressfamilyNode.getNodeName().equals("address-family")) {

							NodeList interfaceAddressNodeList = addressfamilyNode.getChildNodes();

							for (int k = 0; k < interfaceAddressNodeList.getLength(); k++) {

								Node interfaceAddressNode = interfaceAddressNodeList.item(k);

								if (interfaceAddressNode.getNodeName().equals("interface-address")) {

									// System.out.println("__@)@)@)@)_ =

									// "+interfaceAddressNode.getTextContent());

									NodeList ifalocalNodeList = interfaceAddressNode.getChildNodes();

									for (int m = 0; m < ifalocalNodeList.getLength(); m++) {

										Node ifalocalNode = ifalocalNodeList.item(m);

										if (ifalocalNode.getNodeType() != Node.ELEMENT_NODE) {

											continue;

										}



										if (ifalocalNode.getNodeName().equals("ifa-local")) {

											String[] ifalocal_string = ifalocalNode.getTextContent().split("\n");

											// ifs.setIftype(iftype_string[1]);

											if (ifalocal_string[1] != null && ifalocal_string[1].indexOf("/") != -1) {

												ipv6List.add(ifalocal_string[1].substring(0,

														ifalocal_string[1].indexOf("/")));

											} else {

												ipv6List.add(ifalocal_string[1].trim());

											}

										}

									}



								}

							}

						}

					}

				}

			}

		}

		//System.out.println(ipv6List);

		return ipv6List;

	}

}