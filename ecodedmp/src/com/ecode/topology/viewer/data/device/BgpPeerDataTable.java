package com.ecode.topology.viewer.data.device;

public class BgpPeerDataTable {

	private String localAddress;
	private String peerAddress;
	private String peeras;
	private String localas;
	private String peerType;
	private String peerState;

	public String getLocalAddress() {
		return localAddress;
	}

	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	public String getPeerAddress() {
		return peerAddress;
	}

	public void setPeerAddress(String peerAddress) {
		this.peerAddress = peerAddress;
	}

	public String getPeeras() {
		return peeras;
	}

	public void setPeeras(String peeras) {
		this.peeras = peeras;
	}

	public String getLocalas() {
		return localas;
	}

	public void setLocalas(String localas) {
		this.localas = localas;
	}

	public String getPeerType() {
		return peerType;
	}

	public void setPeerType(String peerType) {
		this.peerType = peerType;
	}

	public String getPeerState() {
		return peerState;
	}

	public void setPeerState(String peerState) {
		this.peerState = peerState;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((localAddress == null) ? 0 : localAddress.hashCode());
		result = prime * result + ((peerAddress == null) ? 0 : peerAddress.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BgpPeerDataTable other = (BgpPeerDataTable) obj;
		if (localAddress == null) {
			if (other.localAddress != null)
				return false;
		} else if (!localAddress.equals(other.localAddress))
			return false;
		if (peerAddress == null) {
			if (other.peerAddress != null)
				return false;
		} else if (!peerAddress.equals(other.peerAddress))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BgpPeerDataTable [localAddress=" + localAddress + ", peerAddress=" + peerAddress + ", peeras=" + peeras
				+ ", localas=" + localas + ", peerType=" + peerType + ", peerState=" + peerState + "]";
	}

}
