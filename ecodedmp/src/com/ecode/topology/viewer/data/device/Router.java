package com.ecode.topology.viewer.data.device;

import java.util.HashMap;
import java.util.List;

public class Router {

	String username;
	String password;
	String hostname;
	String ip;
	private String type;
	
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	List<Interfaces> interfaces;
	HashMap<String, String> neighbors;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public List<Interfaces> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(List<Interfaces> interfaces) {
		this.interfaces = interfaces;
	}

	public HashMap<String, String> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(HashMap<String, String> neighbors) {
		this.neighbors = neighbors;
	}

	@Override
	public String toString() {
		return "Router [username=" + username + ", password=" + password + ", hostname=" + hostname + ", ip=" + ip
				+ ", type=" + type + "]";
	}


}
