package com.ecode.topology.viewer.data.device.model;

import java.util.List;
import java.util.Map;

import com.ecode.topology.viewer.data.device.Category;

public class NeighborDetailNodeCategoryVO {

	private Map<String, List<String>> neighborDetails;
	private List<Category> nodeCategotyList;

	public Map<String, List<String>> getNeighborDetails() {
		return neighborDetails;
	}

	public void setNeighborDetails(Map<String, List<String>> neighborDetails) {
		this.neighborDetails = neighborDetails;
	}

	public List<Category> getNodeCategotyList() {
		return nodeCategotyList;
	}

	public void setNodeCategotyList(List<Category> nodeCategotyList) {
		this.nodeCategotyList = nodeCategotyList;
	}

	@Override
	public String toString() {
		return "NeighborDetailNodeCategoryVO [neighborDetails=" + neighborDetails + ", nodeCategotyList="
				+ nodeCategotyList + "]";
	}

}
