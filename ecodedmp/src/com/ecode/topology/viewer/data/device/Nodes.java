package com.ecode.topology.viewer.data.device;

import java.util.List;

public class Nodes {

	private List<Node> nodes;
	private List<Link> links;

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	@Override
	public String toString() {
		return "Nodes [nodes=" + nodes + ", links=" + links + "]";
	}

}
