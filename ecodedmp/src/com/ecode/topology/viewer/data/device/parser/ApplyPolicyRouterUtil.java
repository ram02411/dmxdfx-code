package com.ecode.topology.viewer.data.device.parser;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.SAXException;

import com.ecode.topology.viewer.data.device.EcodeDeviceEndToEndProvisionImpl;
import com.ecode.topology.viewer.data.device.Router;

import net.juniper.netconf.CommitException;
import net.juniper.netconf.Device;
import net.juniper.netconf.LoadException;

/**
 * 
 * @author java
 *
 */
public class ApplyPolicyRouterUtil {
	
	private static final Log logger = LogFactory.getLog(EcodeDeviceEndToEndProvisionImpl.class);

	public static String applyRouterPolicy(Router router,String imports,String exports){
        if (logger.isDebugEnabled()) {
			logger.debug("Executing the method applyRouterPolicy=, router = "+router+" , imports= "+imports+" , exports = "+exports);
	    }
         String str="<policy-options>"
         +"<policy-statement>"
            +" <name>"+exports+"</name>"
             +"<from>"
                +" <protocol>static</protocol>"
             +"</from>"
             +"<then>"
                +" <accept/>"
             +"</then>"
         +"</policy-statement>"
         +"<policy-statement>"
            +" <name>"+imports+"</name>"
             +"<from>"
                +" <protocol>static</protocol>"
             +"</from>"
             +"<then>"
                +" <accept/>"
             +"</then>"
         +"</policy-statement>"
     +"</policy-options>";

        if (logger.isDebugEnabled()) {
			logger.debug("policy = "+str);
	    }
         //Create the device
         //Lock the configuration first
         Device device =null;
         //if(device==null)
        	 //return null;
         try {
        	   device = new Device(router.getIp(),router.getUsername(),router.getPassword(),null);
              device.connect();
         boolean isLocked = device.lockConfig();
         if(!isLocked) {
        	 if (logger.isDebugEnabled()) {
       			 logger.debug("Could not lock configuration. Exit now.");
        	 }
   			 System.out.println("Could not lock configuration. Exit now.");
             return "fail";
         }
         //Load and commit the configuration
             device.loadXMLConfiguration(str, "merge");
             device.commit();
         } catch(LoadException e) {
             System.out.println(e.getMessage());
             if (logger.isDebugEnabled()) 
             logger.debug("LoadException . Exit now."+e.getMessage());
             return "fail";
         } catch(CommitException e) {
             if (logger.isDebugEnabled()) 
                 logger.debug("CommitException . Exit now."+e.getMessage());
             System.out.println(e.getMessage());
             return "fail";
         }catch(Exception e) {
        	  if (logger.isDebugEnabled()) 
                  logger.debug("Exception in executing the command... exit now."+e.getMessage());
             System.out.println(e.getMessage());
             return "fail";
         }
         finally	{
		// Unlock the configuration and close the device.
		try {
			if(device!=null) {
		      	device.unlockConfig();
			     device.close();
			}
		} catch (IOException | SAXException e) {
			e.printStackTrace();
		}
		
	}
         return "success";
}

}
