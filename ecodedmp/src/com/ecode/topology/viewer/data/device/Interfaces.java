package com.ecode.topology.viewer.data.device;

import java.util.List;

public class Interfaces {
	private String name;
	private String mac;
	private String ip;
	private String state;
	private String iftype;
	private List<LogicalInterface> lifs;
	
	public String getIftype() {
		return iftype;
	}
	public void setIftype(String iftype) {
		this.iftype = iftype;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public List<LogicalInterface> getLifs() {
		return lifs;
	}
	public void setLifs(List<LogicalInterface> lifs) {
		this.lifs = lifs;
	}
	@Override
	public String toString() {
		return "Interfaces [name=" + name + ", mac=" + mac + ", ip=" + ip + ", state=" + state + ", iftype=" + iftype
				+ ", lifs=" + lifs + "]";
	}
	
	
	
}
