package com.ecode.topology.viewer.data.device;

public class Link {

	
	private String id;
	private Style style;// style":{"fillColor":"green","toDecoration":"arrow"},
	private String from;
	private String to;

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Style getStyle() {
		return style;
	}

	public void setStyle(Style style) {
		this.style = style;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	
	

	@Override
	public boolean equals(Object obj) {
		Link link=(Link)obj;
		if(from.equalsIgnoreCase(link.from) && to.equalsIgnoreCase(link.to)){
			return true;
		}else if(to.equalsIgnoreCase(link.from) && from.equalsIgnoreCase(link.to)){
			return true;	
		}
		return false;
		
	}

	@Override
	public String toString() {
		return "Link [to=" + to + ", id=" + id + ", style=" + style + ", from=" + from + "]";
	}

}
