package com.ecode.topology.viewer.data.device;

public class LogicalInterface {

	String name;
	String ipv4address;
	String ipv6address;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIpv4address() {
		return ipv4address;
	}
	public void setIpv4address(String ipv4address) {
		this.ipv4address = ipv4address;
	}
	public String getIpv6address() {
		return ipv6address;
	}
	public void setIpv6address(String ipv6address) {
		this.ipv6address = ipv6address;
	}
	
	
}
