package com.ecode.topology.viewer.data.device;

public class MplsNeighborDataTable {
	private String LdpneighborAddress;
	private String InterfaceName;
	private String LdplabelspaceId;
	private String LdpremainingTime;
	public String getLdpneighborAddress() {
		return LdpneighborAddress;
	}
	public void setLdpneighborAddress(String ldpneighborAddress) {
		LdpneighborAddress = ldpneighborAddress;
	}
	public String getInterfaceName() {
		return InterfaceName;
	}
	public void setInterfaceName(String interfaceName) {
		InterfaceName = interfaceName;
	}
	public String getLdplabelspaceId() {
		return LdplabelspaceId;
	}
	public void setLdplabelspaceId(String ldplabelspaceId) {
		LdplabelspaceId = ldplabelspaceId;
	}
	public String getLdpremainingTime() {
		return LdpremainingTime;
	}
	public void setLdpremainingTime(String ldpremainingTime) {
		LdpremainingTime = ldpremainingTime;
	}
	@Override
	public String toString() {
		return "MplsNeighborDataTable [LdpneighborAddress=" + LdpneighborAddress + ", InterfaceName=" + InterfaceName
				+ ", LdplabelspaceId=" + LdplabelspaceId + ", LdpremainingTime=" + LdpremainingTime + "]";
	}
	
	

}
