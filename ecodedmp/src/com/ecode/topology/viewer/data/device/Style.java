package com.ecode.topology.viewer.data.device;

public class Style {
	private String fillColor;
	private String toDecoration;
	private String label;

	public String getFillColor() {
		return fillColor;
	}

	public void setFillColor(String fillColor) {
		this.fillColor = fillColor;
	}

	public String getToDecoration() {
		return toDecoration;
	}

	public void setToDecoration(String toDecoration) {
		this.toDecoration = toDecoration;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	@Override
	public String toString() {
		return "Style [fillColor=" + fillColor + ", toDecoration=" + toDecoration + ", label=" + label + "]";
	}

	
}
