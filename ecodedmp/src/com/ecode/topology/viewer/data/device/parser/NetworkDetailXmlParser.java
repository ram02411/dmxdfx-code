package com.ecode.topology.viewer.data.device.parser;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ecode.topology.viewer.data.device.Interfaces;
import com.ecode.topology.viewer.data.device.Router;

import net.juniper.netconf.NetconfSession;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;


public class NetworkDetailXmlParser {

	public static  void getNetworkInfo(HashMap<String, List<String>> mac_router, Router rt,NetconfSession ns) {
		try{
			 
			XMLBuilder query = new XMLBuilder();
		    XML q =	query.createNewXML("get-interface-information");
			XML rpc_reply = ns.executeRPC(q);
		    parse(rt,rpc_reply, mac_router);
	
	}catch(Exception e) {
			e.printStackTrace();
		}
}

private static void parse(Router rt, XML rpc_reply, HashMap<String,List<String>> mac_router) {
		
		List<String> list = Arrays.asList("interface-information","physical-interface");
		
		List physical_interfaces_list = rpc_reply.findNodes(list);
		Iterator iter = physical_interfaces_list.iterator();
		
		List<Interfaces> list_interfaces = new ArrayList<Interfaces>();

		while (iter.hasNext())
		{

			Node node = (Node) iter.next();
			NodeList child_nodes_phy_interface = node.getChildNodes();
			Interfaces ifs = new Interfaces();

			for(int i=0;i<child_nodes_phy_interface.getLength();i++)
			{
				
				//System.out.println("phy length is "+child_nodes_phy_interface.getLength());
				Node child_node = (Node) child_nodes_phy_interface.item(i);
				if(child_node.getNodeType()!=Node.ELEMENT_NODE)
				{
					continue;
				}
				
				if(child_node.getNodeName().equals("if-type"))
				{
					String[] iftype_string = child_node.getTextContent().split("\n");
					ifs.setIftype(iftype_string[1]);
					//ifs.setIftype(child_node.getTextContent());
				}
				
				if(child_node.getNodeName().contains("name"))
				{
					String[] name_string = child_node.getTextContent().split("\n");

					ifs.setName(name_string[1]);
					//System.out.println("setting name : "+name_string[1]);
				}
				if(child_node.getNodeName().contains("oper-status"))
				{
					String[] state_string = child_node.getTextContent().split("\n");

					ifs.setState(state_string[1]);
				}
				
				if(child_node.getNodeName().equals("hardware-physical-address"))
				{
					String[] mac_string = child_node.getTextContent().split("\n");
					ifs.setMac(mac_string[1]);
					//System.out.println("mac address : "+mac_string[1]);
				}
				

		}
			//System.out.println(ifs.getName()+"  "+ifs.getMac());
			list_interfaces.add(ifs);

		}
		rt.setInterfaces(list_interfaces);

		//System.out.println(rt.getInterfaces().size());
	
		for(int j=0;j<rt.getInterfaces().size();j++)
		{
			//String macaddress = rt.getInterfaces().get(j).getMac();
			String iftype = rt.getInterfaces().get(j).getIftype();
			if(!iftype.equalsIgnoreCase("Ethernet"))
			{
				

				 rt.getInterfaces().remove(j);

			}
			
		}
		for(int j=0;j<rt.getInterfaces().size();j++)
		{
			String macaddress = rt.getInterfaces().get(j).getMac();
			String iftype = rt.getInterfaces().get(j).getIftype();
			if(!iftype.equalsIgnoreCase("Ethernet"))
			{
				

				 rt.getInterfaces().remove(j);

			}
			else{
				List<String>rt_interface = new ArrayList<String>();
						rt_interface.add(rt.getHostname());
						rt_interface.add(rt.getInterfaces().get(j).getName());
						rt_interface.add(rt.getIp());
					//	System.out.println("inserting mac address for :"+rt.getHostname()+" "+macaddress);
				mac_router.put(macaddress, rt_interface);
			}
		}	
			
	}
}
