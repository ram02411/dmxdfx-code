package com.ecode.topology.viewer.data.device;

import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.topology.viewer.data.device.model.VPNProvisionVO;

public interface EcodeDeviceEndToEndProvision  {

	public String applyVpnSetup(VPNProvisionVO vpnProvisionVO);
	public String applyVpnProvisionForSelectedSourceDest(ServicenowProfileVO vpnProvisionVO);

}
