package com.ecode.topology.viewer.data.device.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ecode.topology.viewer.data.device.BgpPeerDataTable;
import com.ecode.topology.viewer.data.device.MplsNeighborDataTable;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

public class MplsLogicalNeighborsParser {
	public static 	List<MplsNeighborDataTable> getDeviceMplsInformation(Device ns) {
		List<MplsNeighborDataTable> mplsNeighborDataTablelist = new ArrayList<MplsNeighborDataTable>();
		try {
			XMLBuilder query = new XMLBuilder();
			XML q = query.createNewXML("get-ldp-neighbor-information");
			XML rpc_reply = ns.executeRPC(q);
			mplsNeighborDataTablelist = parse(rpc_reply);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mplsNeighborDataTablelist;
	}
	
	private static 	List<MplsNeighborDataTable> parse(XML rpc_reply) {
		List<MplsNeighborDataTable> mplsNeighborDataTablesList = new ArrayList<MplsNeighborDataTable>();
		List<String> list = Arrays.asList("ldp-neighbor-information", "ldp-neighbor");
		List  mplsneighborList = rpc_reply.findNodes(list);
		if(mplsneighborList==null)
		return mplsNeighborDataTablesList;
		
	    Iterator iter = mplsneighborList.iterator();
		
		while (iter.hasNext()) {
			Node node = (Node) iter.next();
			NodeList mplsNodes = node.getChildNodes();
			MplsNeighborDataTable  mplsneighborDataTable=new MplsNeighborDataTable();
			for (int i = 0; i < mplsNodes.getLength(); i++) {
				Node mplsNode = (Node) mplsNodes.item(i);
				if (mplsNode.getNodeName().equals("ldp-neighbor-address")) {
					String  ldpneighboraddressText= mplsNode.getTextContent().trim();
					mplsneighborDataTable.setLdpneighborAddress(ldpneighboraddressText);
				}
				else if (mplsNode.getNodeName().equals("interface-name")) {
					String  interfacenameText= mplsNode.getTextContent().trim();
					mplsneighborDataTable.setInterfaceName(interfacenameText);
				}else if (mplsNode.getNodeName().equals("ldp-label-space-id")) {
					String  ldplabelspaceidText= mplsNode.getTextContent().trim();
					String[] mplsneighborip=ldplabelspaceidText.split(":");
					mplsneighborDataTable.setLdplabelspaceId(mplsneighborip[0]);
				}else if (mplsNode.getNodeName().equals("ldp-remaining-time")) {
					String  ldpremainingtimeText= mplsNode.getTextContent().trim();
					mplsneighborDataTable.setLdpremainingTime(ldpremainingtimeText);
				}
			}
			mplsNeighborDataTablesList.add(mplsneighborDataTable);
		}
		return mplsNeighborDataTablesList;
	}
	

}
