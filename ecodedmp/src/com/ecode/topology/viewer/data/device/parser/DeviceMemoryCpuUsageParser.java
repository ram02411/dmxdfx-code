package com.ecode.topology.viewer.data.device.parser;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.juniper.container.data.controller.model.RamMemoryUsageVO;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;

/**
 * 
 * @author java
 *
 */
public class DeviceMemoryCpuUsageParser {

	private static final Log logger = LogFactory.getLog(DeviceMemoryCpuUsageParser.class);
	
	/**
	 * 
	 * @param device
	 * @return
	 */
	public static  RamMemoryUsageVO fetchDeviceCpuUsage(Device device) {
		if(logger.isDebugEnabled())
			logger.debug("Executing the method fetchDeviceCpuUsage hostname "+device.gethostName());
			
		XML rpcReply = null;
		RamMemoryUsageVO cpuUsageVO = new RamMemoryUsageVO();
		try {
			// Creating the command to fetch the data from live device
			String command = "<rpc>" + " <get-system-processes>" + "</get-system-processes>"
					+ " </rpc>";
			
			if(logger.isDebugEnabled())
				logger.debug("Fetching Cpu detail = "+command);
			rpcReply = device.executeRPC(command);
			List<String> cpuFreeTag = Arrays.asList("route-engine", "cpu-idle");
			String cpuFree = rpcReply.findValue(cpuFreeTag);
			if(logger.isDebugEnabled())
				logger.debug("cpuFree "+cpuFree);
			if (cpuFree != null && cpuFree.length() > 0) {
				String usedCpu =(100- Integer.parseInt(cpuFree))+"";
				cpuUsageVO.setFreecpu(cpuFree);
				cpuUsageVO.setUsedcpu(usedCpu);
			} else {
				cpuUsageVO.setFreecpu("0");
				cpuUsageVO.setUsedcpu("0");
			}
			
		} catch (Exception exe) {
			if(logger.isErrorEnabled())
				logger.error("Error Message... -> "+exe.getMessage());
			exe.printStackTrace();
		}
		
		if(logger.isDebugEnabled())
			logger.debug("CpuUsage Detail =  "+cpuUsageVO);
		return cpuUsageVO;
	}
	
	
	/**
	 * 
	 * @param device
	 * @return
	 */
	public static  RamMemoryUsageVO fetchDeviceMemoryUsage(Device device) {
		if(logger.isDebugEnabled())
			logger.debug("Executing the method fetchDeviceMemoryUsage hostname "+device.gethostName());
			
		XML rpcReply = null;
		RamMemoryUsageVO memoryUsageVO = new RamMemoryUsageVO();
		try {
			// Creating the command to fetch the data from live device
			String command = "<rpc>" + " <get-system-memory-information>" + "</get-system-memory-information>"
					+ " </rpc>";
			rpcReply = device.executeRPC(command);
			List<String> memoryTotal = Arrays.asList("system-memory-summary-information", "system-memory-total");
			List<String> memoryfree = Arrays.asList("system-memory-summary-information", "system-memory-free");

			String freeMemory = rpcReply.findValue(memoryfree);
			String totalMemory = rpcReply.findValue(memoryTotal);
			if(logger.isDebugEnabled())
				logger.debug("freeMemory "+freeMemory+" , totalMemory = "+totalMemory);
			if (totalMemory != null && totalMemory.length() > 0) {
				String usedMemory = Integer.toString(Integer.parseInt(totalMemory) - Integer.parseInt(freeMemory));
				memoryUsageVO.setFreememory(freeMemory);
				memoryUsageVO.setUsedmemory(usedMemory);
			} else {
				memoryUsageVO.setFreememory("0");
				memoryUsageVO.setUsedmemory("0");
			}
			if(logger.isDebugEnabled())
				logger.debug("Fetching Cpu detail from memory usage method");
			RamMemoryUsageVO cpuUsageVO=fetchDeviceCpuUsage(device);
			memoryUsageVO.setFreecpu(cpuUsageVO.getFreecpu());
			memoryUsageVO.setUsedcpu(cpuUsageVO.getUsedcpu());
			if(logger.isDebugEnabled())
				logger.debug("Memory and cpu usage detail = "+memoryUsageVO);
			
		} catch (Exception exe) {
			if(logger.isErrorEnabled())
				logger.error("Error Message... -> "+exe.getMessage());
			exe.printStackTrace();
		}
		
		if(logger.isDebugEnabled())
			logger.debug("MemoryUsageVO =  "+memoryUsageVO);
		return memoryUsageVO;
	}
}
