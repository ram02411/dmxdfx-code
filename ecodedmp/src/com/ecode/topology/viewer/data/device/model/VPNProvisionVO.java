package com.ecode.topology.viewer.data.device.model;

public class VPNProvisionVO {

	private String sourceRouter;
	private String targetRouter;
	private String lsourceRouter;
	private String ltargetRouter;
	private String groupName;
	private String sys_id;
	private String exports;
	private String imports;
	private String sourceLocalIP;
	private String sourceNeighborIP;
	private String targetLocalIP;
	private String targetNeighborIP;
	private String type;
	private String authenticationKey;
	private String localas;
	private String peeras;
	private String selectedLogicPath;
	private String profileName;
	private String serviceName;
	public String getSourceRouter() {
		return sourceRouter;
	}
	public void setSourceRouter(String sourceRouter) {
		this.sourceRouter = sourceRouter;
	}
	public String getTargetRouter() {
		return targetRouter;
	}
	public void setTargetRouter(String targetRouter) {
		this.targetRouter = targetRouter;
	}
	public String getLsourceRouter() {
		return lsourceRouter;
	}
	public void setLsourceRouter(String lsourceRouter) {
		this.lsourceRouter = lsourceRouter;
	}
	public String getLtargetRouter() {
		return ltargetRouter;
	}
	public void setLtargetRouter(String ltargetRouter) {
		this.ltargetRouter = ltargetRouter;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getSys_id() {
		return sys_id;
	}
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getExports() {
		return exports;
	}
	public void setExports(String exports) {
		this.exports = exports;
	}
	public String getImports() {
		return imports;
	}
	public void setImports(String imports) {
		this.imports = imports;
	}
	public String getSourceLocalIP() {
		return sourceLocalIP;
	}
	public void setSourceLocalIP(String sourceLocalIP) {
		this.sourceLocalIP = sourceLocalIP;
	}
	public String getSourceNeighborIP() {
		return sourceNeighborIP;
	}
	public void setSourceNeighborIP(String sourceNeighborIP) {
		this.sourceNeighborIP = sourceNeighborIP;
	}
	public String getTargetLocalIP() {
		return targetLocalIP;
	}
	public void setTargetLocalIP(String targetLocalIP) {
		this.targetLocalIP = targetLocalIP;
	}
	public String getTargetNeighborIP() {
		return targetNeighborIP;
	}
	public void setTargetNeighborIP(String targetNeighborIP) {
		this.targetNeighborIP = targetNeighborIP;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getAuthenticationKey() {
		return authenticationKey;
	}
	public void setAuthenticationKey(String authenticationKey) {
		this.authenticationKey = authenticationKey;
	}
	public String getLocalas() {
		return localas;
	}
	public void setLocalas(String localas) {
		this.localas = localas;
	}
	public String getPeeras() {
		return peeras;
	}
	public void setPeeras(String peeras) {
		this.peeras = peeras;
	}
	public String getSelectedLogicPath() {
		return selectedLogicPath;
	}
	public void setSelectedLogicPath(String selectedLogicPath) {
		this.selectedLogicPath = selectedLogicPath;
	}
	public String getProfileName() {
		return profileName;
	}
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	@Override
	public String toString() {
		return "VPNProvisionVO [sourceRouter=" + sourceRouter + ", targetRouter=" + targetRouter + ", lsourceRouter="
				+ lsourceRouter + ", ltargetRouter=" + ltargetRouter + ", groupName=" + groupName + ", sys_id=" + sys_id
				+ ", exports=" + exports + ", imports=" + imports + ", sourceLocalIP=" + sourceLocalIP
				+ ", sourceNeighborIP=" + sourceNeighborIP + ", targetLocalIP=" + targetLocalIP + ", targetNeighborIP="
				+ targetNeighborIP + ", type=" + type + ", authenticationKey=" + authenticationKey + ", localas="
				+ localas + ", peeras=" + peeras + ", selectedLogicPath=" + selectedLogicPath + ", profileName="
				+ profileName + ", serviceName=" + serviceName + "]";
	}

	
	


}
