package com.ecode.topology.viewer.data.device.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

/**
 * 
 * @author 305
 *
 */
public class NeighborsArpLinkParser {

	/*public static Map<String, List<String>> getNeighborArpDetail(Device ns) {
		Map<String, List<String>> mac_interface_map = new HashMap<String, List<String>>();
		try {
			XMLBuilder query = new XMLBuilder();
			XML q = query.createNewXML("get-arp-table-information");
			XML rpc_reply = ns.executeRPC(q);
			mac_interface_map = parse(rpc_reply);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mac_interface_map;
	}*/

	private static Map<String, List<String>> parse(XML rpc_reply) {
		List<String> list = Arrays.asList("arp-table-information", "arp-table-entry");
		List  arpMacInterfaceList = rpc_reply.findNodes(list);
		Iterator iter = arpMacInterfaceList.iterator();
		Map<String, List<String>> arpMacInterfaceMapList = new HashMap<String, List<String>>();
		while (iter.hasNext()) {
			List<String> arpInterfaceList=new ArrayList<String>();
			Node node = (Node) iter.next();
			NodeList child_nodes_ipv6 = node.getChildNodes();
			String l2_neighbor_mac = "";
			for (int i = 0; i < child_nodes_ipv6.getLength(); i++) {
				Node child_node = (Node) child_nodes_ipv6.item(i);
				if (child_node.getNodeName().equals("mac-address")) {
					String[] l2neighbor_string = child_node.getTextContent().split("\n");
					l2_neighbor_mac = (l2neighbor_string[1]);
					
				}
				else if (child_node.getNodeName().equals("interface-name")) {
					String[] l2interface_string = child_node.getTextContent().split("\n");
					//String l2interface_string_sub = l2interface_string[1].trim().split("\\.")[0];
					//l2_self_interface = (l2interface_string_sub);
					arpInterfaceList.add(l2interface_string[1]);
				}
			}
			arpMacInterfaceMapList.put(l2_neighbor_mac, arpInterfaceList);
		}
		return arpMacInterfaceMapList;
	}
}
