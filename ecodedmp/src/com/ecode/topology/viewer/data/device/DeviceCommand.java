package com.ecode.topology.viewer.data.device;

import java.util.List;

import com.ecode.topology.viewer.data.device.model.BgpGroupData;
import com.ecode.topology.viewer.data.device.model.NeighborDetailNodeCategoryVO;

public interface DeviceCommand {

	public NeighborDetailNodeCategoryVO getNeighborDetails(Router router);
	
	public NeighborDetailNodeCategoryVO getPathNeighborDetails(Router router);

	public List<BgpPeerDataTable> findBgpNeighborDetails(Router router);
	public List<MplsNeighborDataTable> findMplsNeighborDetails(Router router);

	public List<BgpGroupData> findDeviceBgpGroups(Router router);

}
