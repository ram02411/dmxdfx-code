package com.ecode.topology.viewer.data.device.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ecode.topology.viewer.data.device.BgpPeerDataTable;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

/**
 * 
 * @author 305
 *
 */
public class BgpLogicalNeighborsParser {

	public static 	List<BgpPeerDataTable> getDeviceBgpInformation(Device ns) {
		List<BgpPeerDataTable> bgpPeerDataTablesList = new ArrayList<BgpPeerDataTable>();
		try {
			XMLBuilder query = new XMLBuilder();
			XML q = query.createNewXML("get-bgp-neighbor-information");
			XML rpc_reply = ns.executeRPC(q);
			bgpPeerDataTablesList = parse(rpc_reply);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bgpPeerDataTablesList;
	}

	private static 	List<BgpPeerDataTable> parse(XML rpc_reply) {
		List<BgpPeerDataTable> bgpPeerDataTablesList = new ArrayList<BgpPeerDataTable>();
		List<String> list = Arrays.asList("bgp-information", "bgp-peer");
		List  bgppeerList = rpc_reply.findNodes(list);
		if(bgppeerList==null)
		return bgpPeerDataTablesList;
		
	    Iterator iter = bgppeerList.iterator();
		
		while (iter.hasNext()) {
			Node node = (Node) iter.next();
			NodeList bgppeerNodes = node.getChildNodes();
			BgpPeerDataTable  bgpPeerDataTable=new BgpPeerDataTable();
			for (int i = 0; i < bgppeerNodes.getLength(); i++) {
				Node bgppeerNode = (Node) bgppeerNodes.item(i);
				if (bgppeerNode.getNodeName().equals("peer-address")) {
					String  peeraddressText= bgppeerNode.getTextContent().trim();
					int index=peeraddressText.indexOf("+");
					if(index==-1){
						bgpPeerDataTable.setPeerAddress(peeraddressText.trim());
					}else {
						String peerAddress =peeraddressText.substring(0,index).trim();
						bgpPeerDataTable.setPeerAddress(peerAddress);
					}	
				}
				else if (bgppeerNode.getNodeName().equals("peer-as")) {
					String  peerasText= bgppeerNode.getTextContent().trim();
					bgpPeerDataTable.setPeeras(peerasText);
				}else if (bgppeerNode.getNodeName().equals("local-as")) {
					String  localasText= bgppeerNode.getTextContent().trim();
					bgpPeerDataTable.setLocalas(localasText);
				}else if (bgppeerNode.getNodeName().equals("local-address")) {
					String  localaddressText= bgppeerNode.getTextContent().trim();
					int index=localaddressText.indexOf("+");
					if(index==-1){
						bgpPeerDataTable.setLocalAddress(localaddressText!=null?localaddressText.trim():"");
					}else {
					    String localAddress =localaddressText.substring(0,index).trim();
				       bgpPeerDataTable.setLocalAddress(localAddress);
					}
				}else if (bgppeerNode.getNodeName().equals("peer-type")) {
					String  peertypeText= bgppeerNode.getTextContent().trim();
					bgpPeerDataTable.setPeerType(peertypeText);
				}else if (bgppeerNode.getNodeName().equals("peer-state")) {
					String  peerstateText= bgppeerNode.getTextContent().trim();
					bgpPeerDataTable.setPeerState(peerstateText);
				}
			}
			bgpPeerDataTablesList.add(bgpPeerDataTable);
		}
		return bgpPeerDataTablesList;
	}
}
