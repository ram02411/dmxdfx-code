package com.ecode.topology.viewer.data.device.parser;



import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ecode.topology.viewer.data.device.BgpPeerDataTable;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;



/**

 * 

 * @author ecode
 *
 */

public class CiscoCLIBgpNeighborCommandExecutor {
	public static void main(String[] args) {
		 List<BgpPeerDataTable> abcd=exeCiscoCliCommand("172.25.0.24","rjil","Rjil123","show bgp neighbor");
		 System.out.println("Out put abcd : --"+abcd);
	}	 private static final Log logger = LogFactory.getLog(CiscoCLIBgpNeighborCommandExecutor.class);
	 public static   List<BgpPeerDataTable> exeCiscoCliCommand(final String hostip,final String username,final String password,final String command){
			List<BgpPeerDataTable> bgpPeerDataList = new ArrayList<BgpPeerDataTable>();
			 String startPattern="BGP neighbor is";
			 String bgpState="BGP state = Established";
			 if(logger.isDebugEnabled())
			 logger.debug("Executing method exeCiscoCliCommand hostip =  "+hostip+" ,username = "+username+" , password = "+password);
			 
			 try {
				 //String command = "ls -la";
		         String host = hostip;
		         String user = username;
		         JSch jsch = new JSch();
		         Session session = jsch.getSession(user, host, 22);
		         Properties config = new Properties();
		         config.put("StrictHostKeyChecking", "no");
		         session.setConfig(config);;
		         session.setPassword(password);
		         session.connect();
		         Channel channel = session.openChannel("exec");
		         ((ChannelExec)channel).setCommand(command);
		         channel.setInputStream(null);
		         ((ChannelExec)channel).setErrStream(System.err);
		         InputStream input = channel.getInputStream();
		         channel.connect();
		         if(logger.isDebugEnabled())
		         logger.debug("Channel Connected to machine " + host + " server with command: " + command );
		         InputStreamReader inputReader=null;
		         BufferedReader bufferedReader=null;
		         try{
		        	 inputReader = new InputStreamReader(input);
		             bufferedReader = new BufferedReader(inputReader);
		             String line = null;
		             BgpPeerDataTable bgpPeerDataTable=null;
		             while((line = bufferedReader.readLine()) != null){
		            	  if(logger.isDebugEnabled())
		            		  logger.debug("PL = "+line);
		            	   //when line is blank ..continue
		            	   if(line!=null && line.trim().length()==0 || line.equalsIgnoreCase("\n\n"))
							  continue;
		            	   
		            	 	if(line!=null && line.toUpperCase().contains(startPattern.toUpperCase())) {
		            	 	   bgpPeerDataTable=new BgpPeerDataTable();
		            	 	   
		            	 	  //BGP neighbor is 172.25.0.41,  remote AS 64521, internal link
		            	 	   if(!line.contains("internal link")) {
		            	 		   	//String str="BGP neighbor is 192.168.1.50";
		            	 		    String tokens[]=line.split("[ ]+");
		            	 		    String peerAddress=tokens[3];
		            	 		   bgpPeerDataTable.setPeerAddress(peerAddress!=null?peerAddress.trim():peerAddress);
		            	 		   bgpPeerDataTable.setLocalAddress(hostip);
			  					  //String str="Remote AS 1234, local AS 9000, external link";
			  					   //Reading next Line!
			  					   String nextLine=bufferedReader.readLine();
			  					 if(logger.isDebugEnabled())
				            		  logger.debug("PL Next Line = "+nextLine);
			  					 
			  					   if(nextLine==null ||  nextLine.length()==0)
			  						   continue;
			  					   if(nextLine.contains("Remote AS")) {
			  						   nextLine = nextLine.replaceAll("[^0-9.,:]+","");
			  						   String localPeerAs[]=nextLine.split(",");
			  						   bgpPeerDataTable.setPeeras(localPeerAs[0]);
			  						   bgpPeerDataTable.setLocalas(localPeerAs[1]);
			  						   if(localPeerAs[0]!=null && localPeerAs[0].equals(localPeerAs[1])){
			  							   bgpPeerDataTable.setPeerType("Internal");
			  						   }else{
			  							   bgpPeerDataTable.setPeerType("External");
			  						   }
			  					   }else{
			  						   bgpPeerDataTable.setPeeras("NK");
			  						   bgpPeerDataTable.setLocalas("NK");
			  						   bgpPeerDataTable.setPeerType("NK");
			  					   }
		            	 	   }else{
		            				if(line.contains("remote AS")){
		            					String tokens[]=line.split("[ ]+");
		            					logger.debug("parsed = "+Arrays.asList(tokens));
		            					String peerAddress=tokens[3].substring(0, tokens[3].length()-1);
		            				    bgpPeerDataTable.setPeerAddress(peerAddress.trim());
				            	 	    bgpPeerDataTable.setLocalAddress(hostip);
		            					String peeras=tokens[6].substring(0, tokens[6].length()-1);
		            				    bgpPeerDataTable.setPeeras(peeras);
					  				    bgpPeerDataTable.setLocalas(peeras);
		            					String peerType=tokens[7].trim();
		            					bgpPeerDataTable.setPeerType(peerType);
		            					System.out.println(peerType.trim());
		            				}
		            	 	   }
		  				  }	else if(line!=null && line.toUpperCase().contains(bgpState.toUpperCase())) {
		  					     if(bgpPeerDataTable==null){
		  					    	 	bgpPeerDataTable=new BgpPeerDataTable();
		  					     }
		  					    //Established
		  					    //I have to discuss with Nimit
		  					    if(bgpPeerDataTable.getLocalAddress()!=null) {
		  					    		bgpPeerDataTable.setPeerState("Established");
		  					    		if(!bgpPeerDataList.contains(bgpPeerDataTable))
		  					    		bgpPeerDataList.add(bgpPeerDataTable);
		  					    }
		  				  }
		  				else if(line!=null && line.toUpperCase().contains("Idle")||line.toUpperCase().contains("Active")) {
	  					     
	  					    	 	bgpPeerDataTable=new BgpPeerDataTable();
	  					    
	  				  }

		             }
		         }catch(Throwable ex){
		        	 StringWriter stack = new StringWriter();
		        	 ex.printStackTrace(new PrintWriter(stack));
		             //ex.printStackTrace();
		             if(logger.isErrorEnabled())
		                 logger.error(stack.toString());
		             
		         }finally{
		        	 if(bufferedReader!=null)
		        	 bufferedReader.close();
		        	 if(inputReader!=null)
		             inputReader.close();
		         }
		         channel.disconnect();
		         session.disconnect();
		     }catch(Throwable ex){
		        	 StringWriter stack = new StringWriter();
		        	 ex.printStackTrace(new PrintWriter(stack));
		        	 if(logger.isErrorEnabled())
			             logger.error(stack.toString());
			        	 
		             //ex.printStackTrace();
		        }
				if(logger.isDebugEnabled())
				logger.debug("_____________Command output_____ bgpPeerDataList = "+bgpPeerDataList);
				
				return bgpPeerDataList;
	 }		

	public static   List<BgpPeerDataTable> exeJuniperCliCommand(final String hostip,final String username,final String password,final String command){
		List<BgpPeerDataTable> bgpPeerDataList = new ArrayList<BgpPeerDataTable>();
		String peerPattern = "Peer: ";
		String localPattern = "Local: ";
		String typeInternal = "Type: Internal";
		String typeExternal = "Type: External";
		String statePattern = "State: ";
		//if(logger.isDebugEnabled())
		System.out.println("Executing method exeCiscoCliCommand hostip =  "+hostip+" ,username = "+username+" , password = "+password);
		try {
		 //String command = "ls -la";
         String host = hostip;
         String user = username;
         JSch jsch = new JSch();
         Session session = jsch.getSession(user, host, 22);
         Properties config = new Properties();
         config.put("StrictHostKeyChecking", "no");
         session.setConfig(config);;
         session.setPassword(password);
         session.connect();
         Channel channel = session.openChannel("exec");
         ((ChannelExec)channel).setCommand(command);
         channel.setInputStream(null);
         ((ChannelExec)channel).setErrStream(System.err);
         InputStream input = channel.getInputStream();
         channel.connect();
         System.out.println("Channel Connected to machine " + host + " server with command: " + command );
         InputStreamReader inputReader=null;
         BufferedReader bufferedReader=null;
         try{
        	 inputReader = new InputStreamReader(input);
             bufferedReader = new BufferedReader(inputReader);
             String line = null;
             while((line = bufferedReader.readLine()) != null){
            	 	System.out.println("OL = "+line);
            	 	BgpPeerDataTable bgpPeerDataTable=new BgpPeerDataTable();
            	 	if(line!=null && line.contains(peerPattern) && line.contains(localPattern)){
            	 		String lineTokens[]=line.trim().split("[ ]+");
            	 		System.out.println(Arrays.asList(lineTokens));
            	 		String peerAddress="'";
            	 		if(lineTokens[1].contains("+")) {
            	 			peerAddress=lineTokens[1].substring(0, lineTokens[1].indexOf("+"));
            	 		}	 
            			else {
            				peerAddress=lineTokens[1].trim();
            			}
            	 		bgpPeerDataTable.setPeerAddress(peerAddress);
            	 		bgpPeerDataTable.setPeeras(lineTokens[3].trim());
            	 		bgpPeerDataTable.setLocalAddress(hostip);
            	 		bgpPeerDataTable.setLocalas(lineTokens[7].trim());
            	 		
            	 		//Reading next line
            	 		line=bufferedReader.readLine();
            	 		
            	 		if(line==null)
            	 			break;
            	 	} if(line!=null && line.contains(statePattern) && (line.contains(typeInternal) || line.contains(typeExternal))){
            	 			String lineTokens[]=line.trim().split("[ ]+");
            	 			System.out.println(Arrays.asList(lineTokens));
            	 			String peerState=lineTokens[3];
            	 		  	if(line.contains(typeInternal)) {
            	 		  		bgpPeerDataTable.setPeerType("Internal");
            	 		  	}
            	 		  	if(line.contains(typeExternal)){
            	 		  		bgpPeerDataTable.setPeerType("External");
            	 		  	}
            	 		  	bgpPeerDataTable.setPeerState(peerState.trim());
            	 		  	logger.debug("bgpPeerDataTable = "+bgpPeerDataTable);
            	 	}
            	 	
            	 	if(bgpPeerDataTable.getLocalAddress()!=null){
            	 		bgpPeerDataList.add(bgpPeerDataTable);
            	 	}
             }
         }catch(Throwable ex){
             ex.printStackTrace();
             if(logger.isErrorEnabled())
                 logger.error(ex.getMessage());
         }finally{
        	 if(bufferedReader!=null)
        	 bufferedReader.close();
        	 if(inputReader!=null)
             inputReader.close();
         }
         channel.disconnect();
         session.disconnect();
     }catch(Throwable ex){
        	 if(logger.isErrorEnabled())
             logger.error(ex.getMessage());
             ex.printStackTrace();
        }
		//if(logger.isDebugEnabled())
		System.out.println("_____________Command output_____ bgpPeerDataList = "+bgpPeerDataList);
		return bgpPeerDataList;
	}

}

