package com.ecode.topology.viewer.data.device.model;

import java.util.List;

public class BgpGroupData {
	private String type;
	private String peeras;
	private String localas;
	private String name;
	private String localAddress;
	private String peerAddress;
	private List<String> iimports;
	private List<String> exports;
	private int peercount;

	public int getPeercount() {
		return peercount;
	}

	public void setPeercount(int peercount) {
		this.peercount = peercount;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPeeras() {
		return peeras;
	}

	public void setPeeras(String peeras) {
		this.peeras = peeras;
	}

	public String getLocalas() {
		return localas;
	}

	public void setLocalas(String localas) {
		this.localas = localas;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocalAddress() {
		return localAddress;
	}

	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	public String getPeerAddress() {
		return peerAddress;
	}

	public void setPeerAddress(String peerAddress) {
		this.peerAddress = peerAddress;
	}

	public List<String> getIimports() {
		return iimports;
	}

	public void setIimports(List<String> iimports) {
		this.iimports = iimports;
	}

	public List<String> getExports() {
		return exports;
	}

	public void setExports(List<String> exports) {
		this.exports = exports;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BgpGroupData other = (BgpGroupData) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "BgpGroupData [type=" + type + ", peeras=" + peeras + ", localas=" + localas + ", name=" + name
				+ ", localAddress=" + localAddress + ", peerAddress=" + peerAddress + ", iimports=" + iimports
				+ ", exports=" + exports + ", peercount=" + peercount + "]";
	}

}
