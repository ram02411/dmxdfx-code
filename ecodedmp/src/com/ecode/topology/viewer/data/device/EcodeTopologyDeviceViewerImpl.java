package com.ecode.topology.viewer.data.device;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.ecode.topology.viewer.data.device.model.NeighborDetailNodeCategoryVO;
import com.ecode.topology.viewer.data.device.parser.DeviceMemoryCpuUsageParser;
import com.ecode.web.app.constant.ApplicationConstant;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;
import com.juniper.container.data.controller.model.RamMemoryUsageVO;

import net.juniper.netconf.Device;

/**
 * 
 * @author java
 * 
 * This class is used to show l2 and l3 topology
 *  it also has logic to show different possible l2 and l3 path for selected 
 *  source and destination router
 *   
 */
@Repository("EcodeTopologyDeviceViewerImpl")
@Scope("singleton")
public class EcodeTopologyDeviceViewerImpl implements EcodeTopologyDeviceViewer {
	
	  private static final Log logger = LogFactory.getLog(EcodeTopologyDeviceViewerImpl.class);
	  
	  @Autowired
	  @Qualifier("JuniperDeviceCommandImpl")
	  private DeviceCommand juniperDeviceCommand;
	  
	  @Autowired
	  @Qualifier("CiscoDeviceCommandImpl")
	  private DeviceCommand ciscoDeviceCommand;
	  
	
	 /**
	  *  This method shows all the possible L2 path between selected source and destination
	  *   among available routers.
	  *   @param source  
	  *   Source Router
	  *   @param destination  
	  *   Destination Router
	  *   
	  */
	@Override
	public List<List<String>> findAllPossiblePathSourceDestination(Router source,Router destination,List<Router> prouters) {
		if(logger.isDebugEnabled()){
			logger.debug("�xecuting the method.... findAllPossiblePathSourceDestination........source router= "+source+" , destination router = "+destination);
		}
		
		EcodePathFinder ecodePathFinder=null;
		List<Node> nodes=new ArrayList<Node>();
		try {
			prouters=ApplicationRouterConfigurationUtil.loadAllAppRouters();
			if(logger.isDebugEnabled()){
				logger.debug("prouters = "+prouters);
			}
			Map<String,Router> routerDetailIP6=ApplicationRouterConfigurationUtil.mapIv6toRouters();
			System.out.println("application router configuration : "+routerDetailIP6);
			//source=ApplicationRouterConfigurationUtil.findSourceRouter();
			//destination=ApplicationRouterConfigurationUtil.findSourceRouter();
			ecodePathFinder=new EcodePathFinder(source.getHostname()+"("+source.getIp()+")", destination.getHostname()+"("+destination.getIp()+")");
			//???????????????????????????????????????????????????/
			//DLETE THIS CODE OFR PRODUCTION
			for(Router router:prouters) {
				   try {
					     //Creating a device
					   if(logger.isDebugEnabled()){
							logger.debug("____Please wait ....device is connecting and processing the data for topology________");
						}
					   DeviceCommand command=null;
					   if(router.getType()!=null && router.getType().equalsIgnoreCase("cisco")) {
						   command= ciscoDeviceCommand;
					   }else{
						   command= juniperDeviceCommand;
					   }
					   NeighborDetailNodeCategoryVO neighborDetailNodeCategoryVO=command.getPathNeighborDetails(router);
					   Map<String, List<String>> neighborDetails=neighborDetailNodeCategoryVO.getNeighborDetails();
					   System.out.println("____Output for IPV6 Neighbor  hostname = "+router.getHostname()+" , HostIp  = "+neighborDetails);
					 //  Map<String, List<String>> neighborDetails=command.getNeighborDetails(router);
					   if(logger.isDebugEnabled()){
						   logger.debug("____Output for IPV6 Neighbor  hostname = "+router.getHostname()+" , HostIp  = "+neighborDetails);
						}
					   
					    Node node=new Node();
					    node.setId(router.getIp()+"_"+router.getUsername()+"_"+router.getPassword()+"_"+router.getHostname());
					    node.setName(router.getHostname());
					    node.setLoaded(true);
						NStyle style1=new NStyle();
						style1.setLabel(router.getHostname());
						style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");
						node.setStyle(style1);
						nodes.add(node);
						if(logger.isDebugEnabled()){
							   logger.debug("Creating new node with detail = "+node);
						}
						//logic to create links for routers........................................................................
						//logic to create links for routers........................................................................ 
                        for (Map.Entry<String, List<String>> entry : neighborDetails.entrySet()) {
                            String neighormac = entry.getKey();
                            List<String> ip6interfacenameList = entry.getValue();
                            String ip6=ip6interfacenameList.get(0);
                            String remoteInterface=ip6interfacenameList.get(1);
                          //  String neighborInterfaceName=ip6interfacenameList.get(1);
                            //List<String> arpList=mac_interface_map.get(neighormac);
                            if(remoteInterface!=null){
                        		Router toRouter=routerDetailIP6.get(ip6);
                        		if(logger.isDebugEnabled()){
                        			logger.debug("toRouter for ip6 = "+ip6);
                        		}
                        		if(toRouter!=null)
                                ecodePathFinder.addNewworkPath(router.getHostname()+"("+router.getIp()+")", toRouter.getHostname()+"("+toRouter.getIp()+")");
                            }
                        }
						//////////////////////////////////////////////////////////////////////////////
					  // ns.close();
					  // device.close();
				   }catch(Exception exe){
					   if(logger.isErrorEnabled()){
						   logger.error("Error is = "+exe.getMessage());
					   }
					   exe.printStackTrace();
				   }
		  	}
		}catch(Exception exe){
			  if(logger.isErrorEnabled()){
				   logger.error("Error is = "+exe.getMessage());
			   }
			exe.printStackTrace();
		}
		    List<List<String>>  networkPaths= ecodePathFinder.generateAllPossibleNetworks();
		    if(logger.isDebugEnabled()){
		    	logger.debug("##############################################PROCSSING IS COMPLETED##################################################");
		    	logger.debug(networkPaths);
		    	logger.debug("##############################################PROCSSING IS COMPLETED##################################################");
		    }
			 return networkPaths;
	}

	@Override
	public List<List<String>> findAllPossibleLogicalPathSourceDestination(Router source,Router destination,List<Router> prouters) {
		 if(logger.isDebugEnabled()){
			 logger.debug("___findAllPossibleLogicalPathSourceDestination___ source = "+source+" and destination = "+destination);
		 }
		List<Node> nodes=new ArrayList<Node>();
		EcodePathFinder ecodePathFinder=null;
		try {
			prouters=ApplicationRouterConfigurationUtil.loadAllAppRouters();
			Map<String,Router> routerDetailIP6=ApplicationRouterConfigurationUtil.mapIv6toRouters();
			Map<String,String> hostIps=ApplicationRouterConfigurationUtil.findHostIpNameMap();
			//source=ApplicationRouterConfigurationUtil.findSourceRouter();
			//destination=ApplicationRouterConfigurationUtil.findSourceRouter();
			ecodePathFinder=new EcodePathFinder(source.getHostname()+"("+source.getIp()+")", destination.getHostname()+"("+destination.getIp()+")");
			for(Router router:prouters) {
				  //NetconfSession ns =null;
				   try {
					   boolean isrouter=false;	
					     //Creating a device
					   DeviceCommand command=null;
					   if(router.getType()!=null && router.getType().equalsIgnoreCase("cisco")) {
						   command= ciscoDeviceCommand;
					   }else{
						   command= juniperDeviceCommand;
					   }
					    //ns = device.createNetconfSession();
					   //finding neighbor detail first
					   if(logger.isDebugEnabled()){
						   logger.debug("Finding all bgp neighbor for the router = "+router);
					   }
					   List<BgpPeerDataTable> bgpPeerDataTableList=command.findBgpNeighborDetails(router);
					   if(logger.isDebugEnabled()){
							 logger.debug("___findAllPossibleLogicalPathSourceDestination___ source = "+source+" and destination = "+destination);
					 }
					   System.out.println("____OUTPUT FOR B FOR HOST "+router.getHostname()+" IS  = "+bgpPeerDataTableList);
					   if(logger.isDebugEnabled()){
						   logger.debug("Bgp Neighbor list for  Hostname = "+router.getHostname()+", hostip  = "+router.getIp()+"  = "+bgpPeerDataTableList);
					   }
					   for(BgpPeerDataTable bgpdt : bgpPeerDataTableList) {
						   if("Established".equalsIgnoreCase(bgpdt.getPeerState())) {
							        if(!isrouter) {
							        	 Node node=new Node();
									     node.setId(bgpdt.getLocalAddress());
									     node.setName(bgpdt.getLocalAddress()+"(AS : "+bgpdt.getLocalas()+")");
									     node.setLoaded(true);
										 NStyle style1=new NStyle();
										 style1.setLabel(bgpdt.getLocalAddress());
										 if(router.getType()!=null && router.getType().equalsIgnoreCase("cisco")) {
											   style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.jpg");
										 }else{
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");
										 }
										 node.setStyle(style1);
										 if(!nodes.contains(node)) {
										     nodes.add(node);
										     if(logger.isDebugEnabled()){
												   logger.debug("New Node is added into the node list ="+ node);
										     }
										 }
										  isrouter=true; //means it should not be added twice
							        }
							     
							        //Adding peer node in node list
							        String  peerAddress=bgpdt.getPeerAddress();
							        Router torouter=routerDetailIP6.get(peerAddress);
							        if(torouter==null) {
							        	  if(logger.isWarnEnabled()){
											   logger.warn("Sorry! this "+torouter+"  does not exist into the "+prouters );
									     }
							        	  if(logger.isWarnEnabled()){
											   logger.warn("Archestrator is skipping this router from topology. "+torouter );
									     }
							        	  continue;
							        }
							      
							        if(peerAddress!=null) {
							        	 Node node=new Node();
									     node.setId(peerAddress);
									     node.setName(peerAddress+"(AS : "+bgpdt.getPeeras()+")");
									     node.setLoaded(true);
										 NStyle style1=new NStyle();
										 style1.setLabel(peerAddress);
										 if(router.getType()!=null && router.getType().equalsIgnoreCase("cisco")) {
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.jpg");	 
										 }else {
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");
										 }	 
										 node.setStyle(style1);
										  
										 if(!nodes.contains(node)) {
										    nodes.add(node);
										    if(logger.isDebugEnabled()){
												   logger.debug("New Node is added into the node list ="+ node);
										     }
										 }
										  isrouter=true; //means 
			                             ecodePathFinder.addNewworkPath(hostIps.get(bgpdt.getLocalAddress())+"("+bgpdt.getLocalAddress()+")", hostIps.get(peerAddress)+"("+peerAddress+")");
							        }
						   }else{
							   continue;
						   }
						   			 
					   }
				   }catch(Exception exe){
					   if(logger.isErrorEnabled()){
				     	    logger.error("______OHHHHHHHH________"+router.getHostname()+" is not able to connect !"+exe.getMessage());
					   }
					   exe.printStackTrace();
				   }
		  	}
			
		}catch(Exception exe){
			exe.printStackTrace();
			  if(logger.isErrorEnabled()){
		     	    logger.error("Error occurs ...-> "+exe.getMessage());
			   }
		}
		
		List<List<String>> result=ecodePathFinder.generateAllPossibleNetworks();
		  if(logger.isDebugEnabled()){
				logger.debug("Final Result is = "+result);
				logger.debug("##############################################PROCSSING IS COMPLETED##################################################");
	    }
    	 return result;
		
	}
	
	/**
	 * 
	 * @param prouters
	 * @return
	 */
	@Override
	public TopologyViewerVO showNetworkLogicalTopology(List<Router> prouters) {
		System.out.println("Executing the method showNetworkLogicalTopology()..................................................");
		if(logger.isDebugEnabled())
		logger.debug("Executing the method showNetworkLogicalTopology()..................................................");
		//JSONArray nodes = new JSONArray();
		List<Node> nodes=new ArrayList<Node>();
		List<Link> links=new ArrayList<>();
		TopologyViewerVO topologyViewerVO=new TopologyViewerVO();
		try {
			
			prouters=ApplicationRouterConfigurationUtil.loadAllAppRouters();
			Map<String,Router> routerDetailIP6=ApplicationRouterConfigurationUtil.mapIv6toRouters();
			int count=1;
			for(Router router:prouters) {
				  //NetconfSession ns =null;
				   try {
					 //  boolean isrouter=false;	
					   DeviceCommand command=null;
					   if(router.getType()!=null && router.getType().equalsIgnoreCase("cisco")) {
						   command= ciscoDeviceCommand;
					   }else{
						   command= juniperDeviceCommand;
					   }
					   	    //Creating and adding the source router for the l3 topology
					   		if(logger.isDebugEnabled()) {
					   					logger.debug("Creating and adding the source Node for the l3 topology = "+router.getIp());
					   	  	}	
					         Node snode=new Node();
						     snode.setId(router.getIp());
						     snode.setName(router.getHostname()+"-"+router.getIp());
						     snode.setLoaded(true);
							 NStyle sstyle=new NStyle();
							 sstyle.setLabel(router.getIp());
							if(ApplicationConstant.JUNIPER.equals(router.getType())){
								 sstyle.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");	 
							 }else {
								 sstyle.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.png");
							 }	 
							 snode.setStyle(sstyle);
							 if(!nodes.contains(snode)) {
										  nodes.add(snode);
										  if(logger.isDebugEnabled()) {
									        	logger.debug("source router is added into Node List= "+router);
									      }	
					          } 
					   if(logger.isDebugEnabled())
								logger.debug("Finding  the bgp neighbor details for hostname = "+router.getHostname()+" , hostip = "+router.getIp());
					//   System.out.println("Finding  the bgp neighbor details for hostname = "+router.getHostname()+" , hostip = "+router.getIp());
					   List<BgpPeerDataTable> bgpPeerDataTableList=command.findBgpNeighborDetails(router);
					   //System.out.println("____Output of bgp neighbor details for hostname =  "+router.getHostname()+", hostip = "+router.getIp()+" is  = "+bgpPeerDataTableList);
					   if(logger.isDebugEnabled())
						logger.debug("____Output of bgp neighbor details for hostname =  "+router.getHostname()+", hostip = "+router.getIp()+" is  = "+bgpPeerDataTableList);
					   for(BgpPeerDataTable bgpdt : bgpPeerDataTableList) {
						    if(logger.isDebugEnabled()) {
									logger.debug("BGP Link from node ="+bgpdt.getLocalAddress()+" to Node = "+bgpdt.getPeerAddress());
						    		logger.debug("bgpdt = "+bgpdt);
					         }		
						   if("Established".equalsIgnoreCase(bgpdt.getPeerState())) {
							        Router toRouter=routerDetailIP6.get(bgpdt.getPeerAddress());
							        if(logger.isDebugEnabled()) {
							        	logger.debug("selected to router for bgp pairing is = "+toRouter);
							        }	
							        if(toRouter==null){
							        	logger.debug("This router , hostname=NA"+" , hostip = "+bgpdt.getPeerAddress()+" does not exist into the source router list");
							        }
							        if(logger.isDebugEnabled()) {
							        	logger.debug("Creating BGP pairing between from = "+router.getIp()+" to router = "+bgpdt.getPeerAddress());
							        }
							        if(toRouter==null) {
							        	//means we are showing only selected routers in which we are interested
							        	continue;
							        }
							        //Adding peer node in node list
							        String  peerAddress=bgpdt.getPeerAddress();
							        if(logger.isDebugEnabled()) {
							        	logger.debug("Taking Peer address to create the link .. "+peerAddress);
							        }
							        //Router torouter=routerDetailIP6.get(peerAddress);
							        if(peerAddress!=null) {
							        	 Node node=new Node();
									     node.setId(peerAddress);
									     if(toRouter!=null) {
									    	 node.setName(toRouter.getHostname()+"-"+bgpdt.getPeerAddress());
									     }
									     else {
									    	 node.setName(bgpdt.getPeerAddress());
									     }
									     node.setLoaded(true);
										 NStyle style1=new NStyle();
										 style1.setLabel(peerAddress);
										 if(ApplicationConstant.JUNIPER.equals(toRouter.getType())) {
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");	 
										 }else {
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.png");
										 }	 
										 node.setStyle(style1);
										     if(logger.isDebugEnabled()) {
									              	logger.debug("New Created Node is  .. "+node);
									    }
									   if(!nodes.contains(node)) {
										 				nodes.add(node);
									   }	 
										if(logger.isDebugEnabled()) {
											 				logger.debug("adding new node into node list  .. "+node);
										}
												//creating Logical Links
											        Link link=new Link();
					                            	link.setId(bgpdt.getLocalAddress()+count);
					                            	link.setFrom(bgpdt.getLocalAddress());
					                        		Style lstyle1=new Style();
					                        		lstyle1.setFillColor("green");
					                        		lstyle1.setToDecoration("line");
					                        		lstyle1.setLabel(bgpdt.getLocalas());
					                        		link.setStyle(lstyle1);
					                        		link.setTo(peerAddress);
					                        		 if(logger.isDebugEnabled()) {
												        	logger.debug("New Created Link  is  .. "+link);
												     }
					                        		if(!links.contains(link)) {
					                                links.add(link);
					                                count++;
			                        		}
							        }
						   }else{
							   continue;
						   }
					   }
						//////////////////////////////////////////////////////////////////////////////
					  // ns.close();
					  // device.close();
				   }catch(Exception exe){
					  //   exe.printStackTrace();
						 StringWriter stack = new StringWriter();
						 exe.printStackTrace(new PrintWriter(stack));
			        	 if(logger.isErrorEnabled())
				             logger.error(stack.toString());
				   }
				   System.out.println("____Node__ "+nodes.toString());
		  	}

		}catch(Exception exe){
			 StringWriter stack = new StringWriter();
			 exe.printStackTrace(new PrintWriter(stack));
        	 if(logger.isErrorEnabled())
	             logger.error(stack.toString());
			//exe.printStackTrace();
		}
			Nodes nodesList=new Nodes();
			nodesList.setLinks(links);
			nodesList.setNodes(nodes);
			topologyViewerVO.setJsonMessage(nodesList);
			topologyViewerVO.setStatus("success");
			logger.debug("_____________Node summary is_________ ");
			for(Node node:nodes){
				logger.debug("Node name ="+node.getName()+" Node ID = "+node.getId()); 
			}
			System.out.println("_____________Link summary is_________ ");
			for(Link plink:links){
				logger.debug("From Node = "+plink.getFrom()+" with link id = "+plink.getId()+" To Node = "+plink.getTo());
			}
			if(logger.isDebugEnabled())
			logger.debug(topologyViewerVO);
			System.out.println("##############################################PROCSSING IS COMPLETED##################################################");
			 return topologyViewerVO;	 
	}
	/**
	 *  This method contains device logic to implement
	 *   L3 topology among selected routers using Mpls.
	 *      
	 *  @param  prouters
	 */
	
	@Override
	public TopologyViewerVO showNetworkLogicalTopologyMpls(List<Router> prouters) {
		// TODO Auto-generated method stub
		System.out.println("Executing the method showNetworkLogicalTopology()..................................................");
		if(logger.isDebugEnabled())
		logger.debug("Executing the method showNetworkLogicalTopology()..................................................");
		//JSONArray nodes = new JSONArray();
		List<Node> nodes=new ArrayList<Node>();
		List<Link> links=new ArrayList<>();
		TopologyViewerVO topologyViewerVO=new TopologyViewerVO();
		try {
			
			prouters=ApplicationRouterConfigurationUtil.loadAllAppRouters();
			Map<String,Router> routerDetailIP6=ApplicationRouterConfigurationUtil.mapIv6toRouters();
			int count=1;
			for(Router router:prouters) {
				  //NetconfSession ns =null;
				   try {
					 //  boolean isrouter=false;	
					   DeviceCommand command=null;
					   if(router.getType()!=null && router.getType().equalsIgnoreCase("cisco")) {
						   command= ciscoDeviceCommand;
					   }else{
						   command= juniperDeviceCommand;
					   }
					   	    //Creating and adding the source router for the l3 topology
					   		if(logger.isDebugEnabled()) {
					   					logger.debug("Creating and adding the source Node for the l3 topology = "+router.getIp());
					   	  	}	
					         Node snode=new Node();
						     snode.setId(router.getIp());
						     snode.setName(router.getHostname()+"-"+router.getIp());
						     snode.setLoaded(true);
							 NStyle sstyle=new NStyle();
							 sstyle.setLabel(router.getIp());
							if(ApplicationConstant.JUNIPER.equals(router.getType())){
								 sstyle.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");	 
							 }else {
								 sstyle.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.png");
							 }	 
							 snode.setStyle(sstyle);
							 if(!nodes.contains(snode)) {
										  nodes.add(snode);
										  if(logger.isDebugEnabled()) {
									        	logger.debug("source router is added into Node List= "+router);
									      }	
					          } 
					   if(logger.isDebugEnabled())
								logger.debug("Finding  the mpls neighbor details for hostname = "+router.getHostname()+" , hostip = "+router.getIp());
					//   System.out.println("Finding  the mpls neighbor details for hostname = "+router.getHostname()+" , hostip = "+router.getIp());
					   List<MplsNeighborDataTable> mplsNeighborDataTableList=command.findMplsNeighborDetails(router);
					   //System.out.println("____Output of mpls neighbor details for hostname =  "+router.getHostname()+", hostip = "+router.getIp()+" is  = "+bgpPeerDataTableList);
					   System.out.println("mplsNeighborDataTableList :  "+mplsNeighborDataTableList);
					   if(logger.isDebugEnabled())
						logger.debug("____Output of mpls neighbor details for hostname =  "+router.getHostname()+", hostip = "+router.getIp()+" is  = "+mplsNeighborDataTableList);
					   for(MplsNeighborDataTable mplsdt : mplsNeighborDataTableList) {
						    if(logger.isDebugEnabled()) {
									//logger.debug("BGP Link from node ="+bgpdt.getLocalAddress()+" to Node = "+bgpdt.getPeerAddress());
						    		logger.debug("mplsdt = "+mplsdt);
					         }		
						   if(mplsdt.getLdplabelspaceId()!=null) {
							        Router toRouter=routerDetailIP6.get(mplsdt.getLdplabelspaceId());
							        if(logger.isDebugEnabled()) {
							        	logger.debug("selected to router for bgp pairing is = "+toRouter);
							        }	
							        if(toRouter==null){
							        	logger.debug("This router , hostname=NA"+" , hostip = "+mplsdt.getLdplabelspaceId()+" does not exist into the source router list");
							        }
							        if(logger.isDebugEnabled()) {
							        	logger.debug("Creating BGP pairing between from = "+router.getIp()+" to router = "+mplsdt.getLdplabelspaceId());
							        }
							        if(toRouter==null) {
							        	//means we are showing only selected routers in which we are interested
							        	continue;
							        }
							        //Adding peer node in node list
							        String  peerAddress=mplsdt.getLdplabelspaceId();
							        if(logger.isDebugEnabled()) {
							        	logger.debug("Taking Peer address to create the link .. "+peerAddress);
							        }
							        //Router torouter=routerDetailIP6.get(peerAddress);
							        if(peerAddress!=null) {
							        	 Node node=new Node();
									     node.setId(peerAddress);
									     if(toRouter!=null) {
									    	 node.setName(toRouter.getHostname()+"-"+mplsdt.getLdplabelspaceId());
									     }
									     else {
									    	 node.setName(mplsdt.getLdplabelspaceId());
									     }
									     node.setLoaded(true);
										 NStyle style1=new NStyle();
										 style1.setLabel(peerAddress);
										 if(ApplicationConstant.JUNIPER.equals(toRouter.getType())) {
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");	 
										 }else {
											 style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.png");
										 }	 
										 node.setStyle(style1);
										     if(logger.isDebugEnabled()) {
									              	logger.debug("New Created Node is  .. "+node);
									    }
									   if(!nodes.contains(node)) {
										 				nodes.add(node);
									   }	 
										if(logger.isDebugEnabled()) {
											 				logger.debug("adding new node into node list  .. "+node);
										}
												//creating Logical Links
											        Link link=new Link();
					                            	link.setId(router.getIp()+count);
					                            	link.setFrom(router.getIp());
					                        		Style lstyle1=new Style();
					                        		lstyle1.setFillColor("green");
					                        		lstyle1.setToDecoration("line");
					                        		lstyle1.setLabel("");
					                        		link.setStyle(lstyle1);
					                        		link.setTo(peerAddress);
					                        		 if(logger.isDebugEnabled()) {
												        	logger.debug("New Created Link  is  .. "+link);
												     }
					                        		if(!links.contains(link)) {
					                                links.add(link);
					                                count++;
			                        		}
							        }
						   }else{
							   continue;
						   }
					   }
						//////////////////////////////////////////////////////////////////////////////
					  // ns.close();
					  // device.close();
				   }catch(Exception exe){
					  //   exe.printStackTrace();
						 StringWriter stack = new StringWriter();
						 exe.printStackTrace(new PrintWriter(stack));
			        	 if(logger.isErrorEnabled())
				             logger.error(stack.toString());
				   }
				   System.out.println("____Node__ "+nodes.toString());
		  	}

		}catch(Exception exe){
			 StringWriter stack = new StringWriter();
			 exe.printStackTrace(new PrintWriter(stack));
        	 if(logger.isErrorEnabled())
	             logger.error(stack.toString());
			//exe.printStackTrace();
		}
			Nodes nodesList=new Nodes();
			nodesList.setLinks(links);
			nodesList.setNodes(nodes);
			topologyViewerVO.setJsonMessage(nodesList);
			topologyViewerVO.setStatus("success");
			logger.debug("_____________Node summary is_________ ");
			for(Node node:nodes){
				logger.debug("Node name ="+node.getName()+" Node ID = "+node.getId()); 
			}
			System.out.println("_____________Link summary is_________ ");
			for(Link plink:links){
				logger.debug("From Node = "+plink.getFrom()+" with link id = "+plink.getId()+" To Node = "+plink.getTo());
			}
			if(logger.isDebugEnabled())
			logger.debug(topologyViewerVO);
			System.out.println("##############################################PROCSSING IS COMPLETED##################################################");

		return topologyViewerVO;
	}

	/**
	 *  This method contains device logic to implement
	 *   L2 topology among selected routers.
	 *      
	 *  @param  prouters
	 */
	@Override
	public TopologyViewerVO showNetworkTopology(List<Router> prouters) {
		if(logger.isDebugEnabled())
			   logger.debug("Executing the showNetworkTopology method.");
		//JSONArray nodes = new JSONArray();
		List<Node> nodes=new ArrayList<Node>();
		List<Link> links=new ArrayList<>();
		TopologyViewerVO topologyViewerVO=new TopologyViewerVO();
		try {
			
			prouters=ApplicationRouterConfigurationUtil.loadAllAppRouters();
			Map<String,Router> routerDetailIP6=new HashMap<>();
			routerDetailIP6=ApplicationRouterConfigurationUtil.mapIv6toRouters();
			for(Router router:prouters) {
				  //NetconfSession ns =null;
				   try {
					     //Creating a device
					    //ns = device.createNetconfSession();
					   //finding neighbor detail first
					   DeviceCommand command=null;
					   if(router.getType()!=null && router.getType().equalsIgnoreCase(ApplicationConstant.CISCO)){
						   command= ciscoDeviceCommand;
					   }else{
						   command=juniperDeviceCommand;
					   }
					   NeighborDetailNodeCategoryVO neighborDetailNodeCategoryVO=command.getNeighborDetails(router);
					  // System.out.println("Neighbor details of "+router.ip+"is :"+neighborDetailNodeCategoryVO.getNeighborDetails());
					   if(logger.isDebugEnabled())
						   logger.debug("____neighborDetailNodeCategoryVO = "+neighborDetailNodeCategoryVO);
					   Map<String, List<String>> neighborDetails=neighborDetailNodeCategoryVO.getNeighborDetails();
					   System.out.println("____Output for neighbors for host "+router.getHostname()+" , hostip ="+router.getIp()+" IS  = "+neighborDetails);
					   if(logger.isDebugEnabled())
					   logger.debug("____Output for neighbors for host "+router.getHostname()+" , hostip ="+router.getIp()+" IS  = "+neighborDetails);
					  // Map<String, List<String>> mac_interface_map= NeighborsArpLinkParser.getNeighborArpDetail(device);
					    Node node=new Node();
					    node.setId(router.getIp()+"_"+router.getUsername()+"_"+router.getPassword()+"_"+router.getHostname());
					    node.setName(router.getHostname());
					    node.setLoaded(true);
						NStyle style1=new NStyle();
						style1.setLabel(router.getHostname());
						if(ApplicationConstant.JUNIPER.equals(router.getType()))
						style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/Juniper.png");
						else
						style1.setImage(ApplicationConstant.APPLICATION_BASE_PATH+"/assets/images/cisco.png");	
						node.setStyle(style1);
						//setting the category
						//THIS IS COMMMTED TAKE CARE
						   if(logger.isDebugEnabled())
							   logger.debug("____neighborDetailNodeCategoryVO.getNodeCategotyList() =  "+neighborDetailNodeCategoryVO.getNodeCategotyList());
						if(neighborDetailNodeCategoryVO.getNodeCategotyList()!=null && neighborDetailNodeCategoryVO.getNodeCategotyList().size()>0) {
							node.setCategories(neighborDetailNodeCategoryVO.getNodeCategotyList());
						}
						
					   if(logger.isDebugEnabled())
						   logger.debug("Adding new node  =  "+node);
					   
						nodes.add(node);
						//logic to create links for routers........................................................................
						//logic to create links for routers........................................................................ 
                        int interfaceCount=100;
						for (Map.Entry<String, List<String>> entry : neighborDetails.entrySet()) {
                            String neighormac = entry.getKey();
                            List<String> ip6interfacenameList = entry.getValue();
                            String ip6=ip6interfacenameList.get(0);
                            String remoteInterface=ip6interfacenameList.get(1);
                          //  String neighborInterfaceName=ip6interfacenameList.get(1);
                           // List<String> arpList=mac_interface_map.get(neighormac);
                            if(remoteInterface!=null){
                            	//String localInterface=arpList.get(0);
                            	Link link=new Link();
                            	link.setId(router.getHostname() + remoteInterface+interfaceCount);
                            	interfaceCount++;
                            	link.setFrom(router.getIp()+"_"+router.getUsername()+"_"+router.getPassword()+"_"+router.getHostname());
                        		Style lstyle1=new Style();
                        		lstyle1.setFillColor("black");
                        		lstyle1.setToDecoration("line");
                        		link.setStyle(lstyle1);
                        		Router toRouter=routerDetailIP6.get(ip6);
                        		if(toRouter!=null){
                        			link.setTo(toRouter.getIp()+"_"+toRouter.getUsername()+"_"+toRouter.getPassword()+"_"+toRouter.getHostname());
                        			if(!links.contains(link))
                        			links.add(link);
                        		}	
                        		//else
                        			//link.setTo("UK");
                                 // links.add(link); 
                            }
                        }
						//////////////////////////////////////////////////////////////////////////////
				   }catch(Exception exe){
					   StringWriter stack = new StringWriter();
						 exe.printStackTrace(new PrintWriter(stack));
			        	 if(logger.isErrorEnabled())
				             logger.error(stack.toString());
				   }
				   //System.out.println("____Node__ "+nodes.toString());
		  	}
			
		}catch(Exception exe){
			 StringWriter stack = new StringWriter();
			 exe.printStackTrace(new PrintWriter(stack));
        	 if(logger.isErrorEnabled())
	             logger.error(stack.toString());
		}
			
		Nodes nodesList=new Nodes();
		 nodesList.setLinks(links);
	 	 nodesList.setNodes(nodes);
			topologyViewerVO.setJsonMessage(nodesList);
			 topologyViewerVO.setStatus("success");
			
			 if(logger.isDebugEnabled())
				   logger.debug("Nodes = "+nodesList);
			 if(logger.isDebugEnabled())
				   logger.debug("Links  = "+links);
			 if(logger.isDebugEnabled())
				   logger.debug("_____________Node summary is_________ ");
			for(Node node:nodes){
				if(logger.isDebugEnabled())
					   logger.debug("Node name ="+node.getName()+" Node ID = "+node.getId()); 
			}
			if(logger.isDebugEnabled())
				   logger.debug("_____________Link summary is_________ ");
			for(Link plink:links){
				if(logger.isDebugEnabled())
					   logger.debug("From Node = "+plink.getFrom()+" with link id = "+plink.getId()+" To Node = "+plink.getTo());
			}
			System.out.println("_________________________"); 
			if(logger.isDebugEnabled())
				   logger.debug("##############################################PROCSSING IS COMPLETED##################################################");
			return topologyViewerVO;
	}
	
	private List<Category>   findCategoriesForNodes(Device    device){
		RamMemoryUsageVO memoryUsageVO=DeviceMemoryCpuUsageParser.fetchDeviceMemoryUsage(device);
		Category category1=new Category();
		Category category2=new Category();
		Category category3=new Category();
		Category category4=new Category();
		
		float totalMemory=Float.parseFloat(memoryUsageVO.getUsedmemory())+Float.parseFloat(memoryUsageVO.getFreememory());
		float usedMemory=Float.parseFloat(memoryUsageVO.getUsedmemory());
		float freeMemory=Float.parseFloat(memoryUsageVO.getFreememory());
		float usedmemoryper=0;
		float freememoryper=0;
		float usedMemoryPie=0;
		float freeMemoryPie=0;
		if(totalMemory>0){
		usedmemoryper=(usedMemory/totalMemory)*100;
		freememoryper=(freeMemory/totalMemory)*100;
		usedMemoryPie=(float) ((0.01*5.0*usedmemoryper*1000)/1000);
		freeMemoryPie=(float) ((0.01*5.0*freememoryper*1000)/1000);
		}
		float totalcpu=Float.parseFloat(memoryUsageVO.getUsedcpu())+Float.parseFloat(memoryUsageVO.getFreecpu());
		float usedcpu=Float.parseFloat(memoryUsageVO.getUsedcpu());
		float freecpu=Float.parseFloat(memoryUsageVO.getFreecpu());
		//float totalcpu=100;
		//float freecpu=98;
		//float usedcpu=totalcpu-freecpu;
		float usedcpuper=0;
		float freecpuper=0;
		float usedcpuPie=0;
		float freecpuPie=0;
		if(totalcpu>0){
		usedcpuper=(usedcpu/totalcpu)*100;
		freecpuper=(freecpu/totalcpu)*100;
		usedcpuPie=(float) ((0.01*5.0*usedcpuper*1000)/1000);
		freecpuPie=(float) ((0.01*5.0*freecpuper*1000)/1000);
		}
		
		category1.setId(1);
		category1.setName("Used Memory :"+usedMemory+"KB");
		category1.setValue(usedMemoryPie);
		category2.setId(2);
		category2.setName("Free Memory :" +freeMemory+"KB");
		category2.setValue(freeMemoryPie);
		category3.setId(3);
		category3.setName("Used CPU :"+usedcpu);
		category3.setValue(usedcpuPie);
		category4.setId(4);
		category4.setName("Free CPU :"+freecpu);
		category4.setValue(freecpuPie);
		
		List<Category>  categories=new ArrayList<>();
		categories.add(category1);
		categories.add(category2);
		categories.add(category3);
		categories.add(category4);
		return categories;
	}

	
}
