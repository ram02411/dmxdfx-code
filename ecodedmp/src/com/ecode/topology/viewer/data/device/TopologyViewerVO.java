package com.ecode.topology.viewer.data.device;


public class TopologyViewerVO {

	private String status;
	private Nodes jsonMessage;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Nodes getJsonMessage() {
		return jsonMessage;
	}

	public void setJsonMessage(Nodes jsonMessage) {
		this.jsonMessage = jsonMessage;
	}

	@Override
	public String toString() {
		return "TopologyViewerVO [status=" + status + ", jsonMessage=" + jsonMessage + "]";
	}

}
