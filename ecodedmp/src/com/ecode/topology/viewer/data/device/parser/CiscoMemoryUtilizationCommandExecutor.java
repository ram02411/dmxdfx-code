package com.ecode.topology.viewer.data.device.parser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ecode.topology.viewer.data.device.Router;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.juniper.container.data.controller.model.RamMemoryUsageVO;

/**
 * 
 *   @author java
 *   class used to computes the memory utilization based on CLI
 *
 */
public class CiscoMemoryUtilizationCommandExecutor {
	
	 private static final Log logger = LogFactory.getLog(CiscoMemoryUtilizationCommandExecutor.class);
	
	/**
	 *  This method computes memory cpu utilization for 
	 *  device .
	 * @param router         
	 * @return
	 */
	public static RamMemoryUsageVO  findCiscoMemoryUtilization(Router router) {
        final   String command="show memory summary";
		RamMemoryUsageVO ramMemoryUsageVO=new RamMemoryUsageVO();
		if(logger.isDebugEnabled()){
			logger.debug("Executing the method findCiscoMemoryUtilization with router = "+router);
		}
		/*float totalcpu=100;
		float freecpu=98;
		float usedcpu=totalcpu-freecpu;
		float usedcpuper=(usedcpu/totalcpu)*100;
		float freecpuper=(freecpu/totalcpu)*100;
		float usedcpuPie=(float) ((0.01*5.0*usedcpuper*1000)/1000);
		float freecpuPie=(float) ((0.01*5.0*freecpuper*1000)/1000);
		
		category1.setId(1);
		category1.setName("Used Memory :"+usedMemory+"KB");
		category1.setValue(usedMemoryPie);
		category2.setId(2);
		category2.setName("Free Memory :" +freeMemory+"KB");
		category2.setValue(freeMemoryPie);
		category3.setId(3);
		category3.setName("Used CPU :"+usedcpu);
		category3.setValue(usedcpuPie);
		category4.setId(4);
		category4.setName("Free CPU :"+freecpu);
		category4.setValue(freecpuPie);
		
		List<Category>  categories=new ArrayList<>();
		categories.add(category1);
		categories.add(category2);
		categories.add(category3);
		categories.add(category4);*/
	 try {
		 //String command = "ls -la";
         String host = router.getIp();
         String user = router.getUsername();
         JSch jsch = new JSch();
         Session session = jsch.getSession(user, host, 22);
         Properties config = new Properties();
         config.put("StrictHostKeyChecking", "no");
         session.setConfig(config);;
         session.setPassword(router.getPassword());
         session.connect();
         Channel channel = session.openChannel("exec");
         ((ChannelExec)channel).setCommand(command);
         channel.setInputStream(null);
         ((ChannelExec)channel).setErrStream(System.err);
         InputStream input = channel.getInputStream();
         channel.connect();
         if(logger.isDebugEnabled())
         logger.debug("Channel Connected to machine " + host + " server with command: " + command );
         InputStreamReader inputReader=null;
         BufferedReader bufferedReader=null;
         try{
        	 inputReader = new InputStreamReader(input);
             bufferedReader = new BufferedReader(inputReader);
             String line = null;
             int count=0;
             while((line = bufferedReader.readLine()) != null) {
            	    if(count==10)
            	    	break;
            	    else
            	    	count++;
            	 	if(logger.isDebugEnabled())
            	 		logger.debug("CO  = "+line);
            		if(line!=null && line.toUpperCase().contains("Physical Memory:".toUpperCase())) {
        			    String memUtilizationTokens[]=line.split(":");
        			    if(memUtilizationTokens!=null && memUtilizationTokens.length>0) {
        			    	String tempMemToken=memUtilizationTokens[1];
        			    	if(tempMemToken!=null){
        			    		  String tempLineTokens[]=tempMemToken.trim().split("[ ]+");
        			    		  if(logger.isDebugEnabled()) {
        			    			  logger.debug(Arrays.asList(tempLineTokens));
        			    		  }
        			    		  String totalMemory=tempLineTokens[0].substring(0,tempLineTokens[0].length()-1);
        			    		  String tavailableMemory=tempLineTokens[2].substring(1, tempLineTokens[2].length()-1);
        			    		  String totalMemUsed=(Integer.parseInt(totalMemory)-Integer.parseInt(tavailableMemory))+"";
        			    		  if(logger.isDebugEnabled()) {
        			    			  logger.debug("Total Memory = "+totalMemory+" , tavailableMemory = "+tavailableMemory+" , totalMemUsed = "+totalMemUsed);
        			    		  }
        			    		  ramMemoryUsageVO.setFreememory(tavailableMemory);
        			    		  ramMemoryUsageVO.setUsedmemory(totalMemUsed);
        			    		  break;
        			    	}
        			    }
        		}else if(line!=null && line.toUpperCase().contains("Processor".toUpperCase())) {
    			    String memUtilizationTokens[]=line.split("[ ]+");
    			    if(memUtilizationTokens!=null && memUtilizationTokens.length>0) {
    			    		  if(logger.isDebugEnabled()) {
    			    			  logger.debug(Arrays.asList(memUtilizationTokens));
    			    		  }
    			    		  if(memUtilizationTokens.length>4) {
    			    			  String totalMemory=memUtilizationTokens[2].trim();
    			    			  String totalMemUsed=memUtilizationTokens[3].trim();
    			    			  String tavailableMemory=memUtilizationTokens[4].trim();
    			    			  if(logger.isDebugEnabled()) {
    			    				  logger.debug("Total Memory = "+totalMemory+" , tavailableMemory = "+tavailableMemory+" , totalMemUsed = "+totalMemUsed);
    			    			  }
    			    			  ramMemoryUsageVO.setFreememory(tavailableMemory);
    			    			  ramMemoryUsageVO.setUsedmemory(totalMemUsed);
    			    		  }
    			    		  break;
    			    	}
    			    }
    		}
         }catch(Exception ex){
        	 StringWriter stack = new StringWriter();
        	 ex.printStackTrace(new PrintWriter(stack));
             //ex.printStackTrace();
             if(logger.isErrorEnabled())
                 logger.error(stack.toString());
         }finally{
        	 if(bufferedReader!=null)
        	 bufferedReader.close();
        	 if(inputReader!=null)
             inputReader.close();
         }
         channel.disconnect();
         session.disconnect();
     }catch(Throwable ex){
        	 StringWriter stack = new StringWriter();
        	 ex.printStackTrace(new PrintWriter(stack));
        	 if(logger.isErrorEnabled())
	             logger.error(stack.toString());
	        	 
             //ex.printStackTrace();
        }
	 return ramMemoryUsageVO;
	}
}
