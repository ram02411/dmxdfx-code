package com.ecode.topology.viewer.data.device.parser;

import java.util.ArrayList;
import java.util.List;

import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.model.BgpGroupData;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;
import com.juniper.container.data.model.configuration.protocols.Bgp;
import com.juniper.container.data.model.configuration.protocols.BgpGroup;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.juniper.container.data.model.configuration.protocols.LocalAs;
import com.juniper.container.data.model.configuration.protocols.Neighbor;

import net.juniper.netconf.Device;

public class BgpGroupsDeviceUtil {
	
	
	/**
	 * 
	 * @param bgpGroupData
	 * @return
	 */
	public static CProtocols createBgpGroup(BgpGroupData bgpGroupData,String localAddress,List<String> exports,List<String> imports) {
		 CProtocols cProtocols=new CProtocols();
		 Bgp  bgp=new  Bgp();
	  	cProtocols.setBgp(bgp);
		 List<BgpGroup> bgpGroupList=new ArrayList<>();
		 BgpGroup bgpGroup=new BgpGroup();
		 bgpGroup.setName(bgpGroupData.getName());
		 bgpGroup.setLocaladdress(localAddress);
		 List<Neighbor> neighbors=new ArrayList<>();
		 Neighbor neighbor=new Neighbor();
		 LocalAs localAs=new LocalAs();
		 localAs.setAsnumber(bgpGroupData.getLocalas());
		 neighbor.setLocalas(localAs);
		 neighbor.setName(bgpGroupData.getPeerAddress());
		 neighbor.setPeeras(bgpGroupData.getPeeras());
		 neighbors.add(neighbor);
		 bgpGroup.setNeighbor(neighbors);
		 bgpGroup.setType(bgpGroupData.getType());
		 //List<String> export=new ArrayList<>();
		 //export.add(exports);
		 //List<String> iimport=new ArrayList<>();
		 //iimport.add("1.1.1.1/24");
		/* if(bgpGroupData.getIimports()!=null){
			 List<String> tlist=new ArrayList<String>();
			 for(String imp:bgpGroupData.getIimports()){
				 if(bgpGroup.getIimport()!=null && !bgpGroup.getIimport().contains(imp)){
					 tlist.add(imp);
				 } else if(bgpGroup.getIimport()==null){
					 tlist.add(imp);
				 }
			 }
		     bgpGroup.setIimport(tlist);
		 }
		 if(bgpGroup.getIimport()!=null) {
			 for(String im:imports) {
				 if(bgpGroup.getIimport()!=null && !bgpGroup.getIimport().contains(im))	 {
					 bgpGroup.getIimport().add(im);
				 }  
			 }
		 }else{
			 bgpGroup.setIimport(imports);
		 }*/
		 if(imports!=null) {
			     List<String> tlist=new ArrayList<String>();
				 for(String im:imports) {
					  if(bgpGroupData.getIimports()!=null && bgpGroupData.getIimports().contains(im)){
						  continue;
					  }
					  if(!tlist.contains(im)){
						  tlist.add(im);
					  }
				 }
				 bgpGroup.setIimport(tlist);
		 } 
		 
		 if(exports!=null) {
		     List<String> elist=new ArrayList<String>();
			 for(String em:exports) {
				  if(bgpGroupData.getExports()!=null && bgpGroupData.getExports().contains(em)){
					  continue;
				  }
				  if(!elist.contains(em)){
					  elist.add(em);
				  }
			 }
			 bgpGroup.setExport(elist);
	 } 
		 
		 
		/* if(bgpGroupData.getExports()!=null){
			 List<String> tlist=new ArrayList<String>();
			 for(String exp:bgpGroupData.getExports()){
				 if(bgpGroup.getExport()!=null && !bgpGroup.getExport().contains(exp)) {
					 tlist.add(exp);
				 } 	else if(bgpGroup.getExport()==null){
					 tlist.add(exp);
				 }
			 }
		     bgpGroup.setExport(tlist);
		 }
		 if(bgpGroup.getExport()!=null){
			 for(String ep:exports) {
				 if(bgpGroup.getExport()!=null && !bgpGroup.getExport().contains(ep)){
					 bgpGroup.getExport().add(ep);
				 }	 
			 }
		 }else{
			 bgpGroup.setExport(exports);
		 }*/
		 
		 
		 
		 bgpGroupList.add(bgpGroup);
		 bgp.setBgpGroup(bgpGroupList);
		return cProtocols;
	}
	
	public static String getDeviceHostName(String hostnameIp){
		  int index=hostnameIp.indexOf("(");
		  String hostName=hostnameIp.substring(0,index);
		  return hostName;
	}
	
	public static String getDeviceIpAddress(String hostnameIp){
		  int index=hostnameIp.indexOf("(");
		  String ipAddress=hostnameIp.substring(index+1,hostnameIp.length()-1);
		  return ipAddress;
	}
	
	public static  List<BgpGroupData> findDeviceBgpGroups(String hostName){
		 List<BgpGroupData> bgpGroupDataList=new ArrayList<BgpGroupData>();
		 // System.out.println("Host Name = "+hostName+" , IP Address = "+ipAddress);
		  Device device=null;
		  try {
			  Router router=ApplicationRouterConfigurationUtil.findRouterByHostname(hostName);
			  device = new Device(router.getIp(), router.getUsername(), router.getPassword(), null, 830);
			  device.connect();
			  //bgpGroupDataList=BgpGroupsParser.findDeviceBgpGroups(device);
			  bgpGroupDataList=BgpGroupsParser.findDeviceBgpGroupsByConfiguration(device);
		  }catch(Exception exe){
			  exe.printStackTrace();
		  }finally{
			  if(device!=null){
				  device.close();
				  device=null;
			   }
		   }
		  return bgpGroupDataList;
	}

}
