package com.ecode.topology.viewer.data.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.ecode.cisco.command.CiscoRouter;
import com.ecode.cisco.command.CiscoRouterCliCommandExecutor;
import com.ecode.cisco.command.SSHClient;
import com.ecode.topology.viewer.data.device.model.BgpGroupData;
import com.ecode.topology.viewer.data.device.model.NeighborDetailNodeCategoryVO;
import com.ecode.topology.viewer.data.device.parser.CiscoCLIBgpNeighborCommandExecutor;
import com.ecode.topology.viewer.data.device.parser.CiscoCpuUtilizationCommandExecutor;
import com.ecode.topology.viewer.data.device.parser.CiscoMemoryUtilizationCommandExecutor;
import com.juniper.container.data.controller.model.RamMemoryUsageVO;


@Service("CiscoDeviceCommandImpl")
@Scope("singleton")
public class CiscoDeviceCommandImpl implements DeviceCommand {
	
	 private static final Log logger = LogFactory.getLog(CiscoDeviceCommandImpl.class);
	 
	  /**
	   *  compute the cpu and memory utilization for cisco
	  * @param router
	  * @return
	  */
	 private List<Category>   findCategoriesForNodes(Router    router) {
		  if(logger.isDebugEnabled()){
			  logger.debug("Executing the method findCategoriesForNodes with router = "+router);
		  }
		  RamMemoryUsageVO memoryUsageVo=CiscoMemoryUtilizationCommandExecutor.findCiscoMemoryUtilization(router);
		  float totalMemory=Float.parseFloat(memoryUsageVo.getFreememory())+Float.parseFloat(memoryUsageVo.getUsedmemory());
		  float usedMemory=Float.parseFloat(memoryUsageVo.getUsedmemory());
		  float freeMemory=Float.parseFloat(memoryUsageVo.getFreememory());
		  float usedmemoryper=0;
		  float freememoryper=0;
		  float usedMemoryPie=0;
		  float freeMemoryPie=0;
		  if(totalMemory>0){
		  usedmemoryper=(usedMemory/totalMemory)*100;
		  freememoryper=(freeMemory/totalMemory)*100;
		  usedMemoryPie=(float) ((0.01*5.0*usedmemoryper*1000)/1000);
		  freeMemoryPie=(float) ((0.01*5.0*freememoryper*1000)/1000);
		  }
		  Category category1=new Category();
		  Category category2=new Category();
		  category1.setId(1);
		  category1.setName("Used Memory :"+usedMemory+"MB");
		  category1.setValue(usedMemoryPie);
		  category2.setId(2);
		  category2.setName("Free Memory :" +freeMemory+"MB");
		  category2.setValue(freeMemoryPie);
		  
		  if(logger.isDebugEnabled()){
			  logger.debug("totalMemory =  "+totalMemory+" , usedMemory = "+usedMemory+" , feeMemory = "+freeMemory);
			  logger.debug("usedmemoryper =  "+usedmemoryper+" , freememoryper = "+freememoryper+" , usedMemoryPie = "+usedMemoryPie+" , freeMemoryPie = "+freeMemoryPie);
		  }
		  
		  RamMemoryUsageVO cpuUsage=CiscoCpuUtilizationCommandExecutor.findCiscoCpuUtilization(router);
		  float totalCpu=Float.parseFloat(cpuUsage.getFreecpu())+Float.parseFloat(cpuUsage.getUsedcpu());
		  float usedCpu=Float.parseFloat(cpuUsage.getUsedcpu());
		  float freeCpu=Float.parseFloat(cpuUsage.getFreecpu());
		  float usedcpuper=0;
		  float freecpuper=0;
		  float usedCpuPie=0;
		  float freeCpuPie=0;
		  if(totalCpu>0){
		   usedcpuper=(usedCpu/totalCpu)*100;
		   freecpuper=(freeCpu/totalCpu)*100;
		   usedCpuPie=(float) ((0.01*5.0*usedcpuper*1000)/1000);
		  freeCpuPie=(float) ((0.01*5.0*freecpuper*1000)/1000);
		  }
		  
		  if(logger.isDebugEnabled()){
			  logger.debug("totalCpu =  "+totalMemory+" , usedCpu = "+usedCpu+" , freeCpu = "+freeCpu);
			  logger.debug("usedcpuper =  "+usedcpuper+" , freecpuper = "+freecpuper+" , usedCpuPie = "+usedCpuPie+" , freeCpuPie = "+freeCpuPie);
		  }
		
		  
		  Category category3=new Category();
		  Category category4=new Category();
		  category3.setId(3);
		  category3.setName("Used CPU :"+Float.parseFloat(memoryUsageVo.getUsedmemory()));
		  category3.setValue(usedCpuPie);
		  category4.setId(4);
		  category4.setName("Free CPU :"+Float.parseFloat(memoryUsageVo.getFreememory()));
		  category4.setValue(freeCpuPie);
		  
		  List<Category>  categories=new ArrayList<>();
		  categories.add(category1);
		  categories.add(category2);
		  categories.add(category3);
		  categories.add(category4);
		  if(logger.isDebugEnabled())
				logger.debug("categories = "+categories);
		 return categories;
	 }
	
	@Override
	public NeighborDetailNodeCategoryVO getNeighborDetails(Router router) {
		  if(logger.isDebugEnabled()){
			  logger.debug("Executing the method getNeighborDetails with router = "+router);
		  }
		 String startPattern="IPv6 Address";
		 String endPattern="Mcast adjacency";
		 Map<String, List<String>> neighborMapList=new HashMap<>();
		 NeighborDetailNodeCategoryVO neighborDetailNodeCategoryVO=new  NeighborDetailNodeCategoryVO();
		 neighborDetailNodeCategoryVO.setNeighborDetails(neighborMapList);
	 	 SSHClient ssh=null;
	    	try {
	       // System.out.println("_#####Executing command on Cisco Please wait#######");
	        if(logger.isDebugEnabled()){
				  logger.debug("_#####Executing command on Cisco Please wait####### = "+router);
		    }
	    	//ssh = new SSHClient("192.168.1.80", "cisco", "cisco");
	        List<String> cmdsToExecute = new ArrayList<String>();
	         ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
	        cmdsToExecute = new ArrayList<String>();
	        cmdsToExecute.add("show ipv6 neighbors");
	        if(logger.isDebugEnabled()){
				  logger.debug("__Please wait processing is going on__ ");
		    }
	        String output = ssh.execute(cmdsToExecute);
	        if(logger.isDebugEnabled()){
				  logger.debug("output of show ipv6 neighbors= "+output);
		    }
	        if(output==null||output.length()==0)
	        	return neighborDetailNodeCategoryVO;
	         // System.out.println(output);
	        if(logger.isDebugEnabled()){
				  logger.debug("________________________________________________________________");
		    }
			  //System.out.println("Parsing the output..............................");
			  String lines[]=output.split("\n");
			  boolean isread=false;
			  for(String line:lines){
				  if(logger.isDebugEnabled()){
					  logger.debug("OC = "+line);
			      }
				  if(line!=null && line.trim().length()==0 || line.equalsIgnoreCase("\n\n"))
					  continue;
				  if(line!=null && line.length()>2)
					  line=line.substring(0,line.length()-2);
				  if(line!=null && line.toUpperCase().contains(endPattern.toUpperCase())) {
					  break;
				  }
				  if(isread){
					  String results[]=line.split("[ ]+");
					  if(logger.isDebugEnabled()){
						  logger.debug(Arrays.asList(results));
					  }
					  if(results!=null && results.length>4) {
					       List<String> neighborList=new ArrayList<String>(2);
					       neighborList.add(results[0]!=null?results[0].toUpperCase():results[0]);
					       neighborList.add(results[4]);
					       neighborMapList.put(results[2].trim(), neighborList);
					       
					      }
				  }
				  else if(line!=null && line.toUpperCase().contains(startPattern.toUpperCase())) {
					  isread=true;
				  }
			  }
		
	    	}catch(Exception exe){
	    		  if(logger.isErrorEnabled()){
					  logger.error(exe.getMessage());
				  }
	    		exe.printStackTrace();
	    		
	    	}finally{
	    		if(ssh!=null)
	    		  ssh.disconnectionSession();
	    	}
		// CiscoRouter ciscoRouter=new CiscoRouter(router.getUsername(),router.getPassword(), router.getHostname());
		  //String output=CiscoRouterCliCommandExecutor.executeCommand(ciscoRouter, "show ipv6 neighbors");
	    	try {
	    		List<Category> categories=findCategoriesForNodes(router);
	    		logger.debug(" setting categories into NeighborDetail Node categorie ="+categories);
	    		neighborDetailNodeCategoryVO.setNodeCategotyList(categories);
	    	}catch(Exception exe){
	    		exe.printStackTrace();
	    		  if(logger.isErrorEnabled()){
					  logger.error(exe.getMessage());
				  }
	    	}
		 	return neighborDetailNodeCategoryVO;
	}
	
	@Override
	public NeighborDetailNodeCategoryVO getPathNeighborDetails(Router router) {
		  if(logger.isDebugEnabled()){
			  logger.debug("Executing the method getNeighborDetails with router = "+router);
		  }
		 String startPattern="IPv6 Address";
		 String endPattern="Mcast adjacency";
		 Map<String, List<String>> neighborMapList=new HashMap<>();
		 NeighborDetailNodeCategoryVO neighborDetailNodeCategoryVO=new  NeighborDetailNodeCategoryVO();
		 neighborDetailNodeCategoryVO.setNeighborDetails(neighborMapList);
	 	 SSHClient ssh=null;
	    	try {
	        System.out.println("_#####Executing command on Cisco Please wait#######");
	        if(logger.isDebugEnabled()){
				  logger.debug("_#####Executing command on Cisco Please wait####### = "+router);
		    }
	    	//ssh = new SSHClient("192.168.1.80", "cisco", "cisco");
	        List<String> cmdsToExecute = new ArrayList<String>();
	         ssh = new SSHClient(router.getIp(), router.getUsername(), router.getPassword());
	        cmdsToExecute = new ArrayList<String>();
	        cmdsToExecute.add("show ipv6 neighbors");
	        if(logger.isDebugEnabled()){
				  logger.debug("__Please wait processing is going on__ ");
		    }
	        String output = ssh.execute(cmdsToExecute);
	        if(logger.isDebugEnabled()){
				  logger.debug("output of show ipv6 neighbors= "+output);
		    }
	        if(output==null||output.length()==0)
	        	return neighborDetailNodeCategoryVO;
	         // System.out.println(output);
	        if(logger.isDebugEnabled()){
				  logger.debug("________________________________________________________________");
		    }
			  //System.out.println("Parsing the output..............................");
			  String lines[]=output.split("\n");
			  boolean isread=false;
			  for(String line:lines){
				  if(logger.isDebugEnabled()){
					  logger.debug("OC = "+line);
			      }
				  if(line!=null && line.trim().length()==0 || line.equalsIgnoreCase("\n\n"))
					  continue;
				  if(line!=null && line.length()>2)
					  line=line.substring(0,line.length()-2);
				  if(line!=null && line.toUpperCase().contains(endPattern.toUpperCase())) {
					  break;
				  }
				  if(isread){
					  String results[]=line.split("[ ]+");
					  if(logger.isDebugEnabled()){
						  logger.debug(Arrays.asList(results));
					  }
					  if(results!=null && results.length>4) {
					       List<String> neighborList=new ArrayList<String>(2);
					       neighborList.add(results[0]!=null?results[0].toUpperCase():results[0]);
					       neighborList.add(results[4]);
					       neighborMapList.put(results[2].trim(), neighborList);
					       
					      }
				  }
				  else if(line!=null && line.toUpperCase().contains(startPattern.toUpperCase())) {
					  isread=true;
				  }
			  }
		
	    	}catch(Exception exe){
	    		  if(logger.isErrorEnabled()){
					  logger.error(exe.getMessage());
				  }
	    		exe.printStackTrace();
	    		
	    	}finally{
	    		if(ssh!=null)
	    		  ssh.disconnectionSession();
	    	}
		// CiscoRouter ciscoRouter=new CiscoRouter(router.getUsername(),router.getPassword(), router.getHostname());
		  //String output=CiscoRouterCliCommandExecutor.executeCommand(ciscoRouter, "show ipv6 neighbors");
	    	
		 	return neighborDetailNodeCategoryVO;
	}
	
		public static void main(String[] args) {
			String str="show system processes summary";
		   	CiscoRouter ciscoRouter=new CiscoRouter("root","ecode123", "192.168.100.80");
		   	//DeviceCommand command=new CiscoDeviceCommandImpl();
		   	String output=CiscoRouterCliCommandExecutor.executeCommand(ciscoRouter, str);
		   	System.out.println("oooooooooo   = "+output);
			
	}

		@Override
		public List<BgpPeerDataTable> findBgpNeighborDetails(Router router) {
			if(logger.isDebugEnabled())

				logger.debug("Executing the method findBgpNeighborDetails with hostname = "+router.getHostname()+" , hostip = "+router.ip);

				List<BgpPeerDataTable> bgpPeerDataTablesList = new ArrayList<BgpPeerDataTable>();

				String command="show ip bgp neighbors";

				//System.out.println("Output of command -> show bgp neighbor-< with host name = "+router.getHostname()+" , hostip = "+router.getIp()+" is = "+output);

				bgpPeerDataTablesList=CiscoCLIBgpNeighborCommandExecutor.exeCiscoCliCommand(router.getIp(), router.getUsername(), router.getPassword(), command);

				if(logger.isDebugEnabled())

				     logger.debug("Output of command -> show bgp neighbor-< with host name = "+router.getHostname()+" , hostip = "+router.getIp()+" is = "+bgpPeerDataTablesList);

				return bgpPeerDataTablesList;
		}

		/**
		 * We have to write logic for exports and imports
		 * 
		 */
		@Override
		public List<BgpGroupData> findDeviceBgpGroups(Router router) {
			//router bgp
			 String startPattern="router bgp";
			List<BgpGroupData> bgpGroupDataList = new ArrayList<BgpGroupData>();
			CiscoRouter ciscoRouter=new CiscoRouter(router.getUsername(),router.getPassword(), router.getHostname());
			String output=CiscoRouterCliCommandExecutor.executeCommand(ciscoRouter, "show running-config router bgp");
			 String lines[]=output.split("\n");
			 if(lines==null){
				 return bgpGroupDataList;
			 }
			 BgpGroupData bgpGroupData=null;
			  for(int x=0;x<lines.length;x++){
				  String line=lines[x];
				  if(line!=null && line.trim().length()==0 || line.equalsIgnoreCase("\n\n"))
					  continue;
				  
				  if(line!=null && line.toUpperCase().contains(startPattern.toUpperCase())) {
					   bgpGroupData=new BgpGroupData();
					   //setting import and exports
					   bgpGroupData.setName("NA");
					  //String str="BGP neighbor is 192.168.1.50";
					  String  localas = line.replaceAll("[^0-9]+","");
					  //CAREFULL TODO HERE
					  bgpGroupData.setLocalas(localas);
					  bgpGroupData.setLocalAddress(router.getIp());
					  bgpGroupDataList.add(bgpGroupData);
				  }	  
				  if(line!=null && line.toUpperCase().contains("neighbor".toUpperCase())) {
					  String  peerAddress = line.replaceAll("[^0-9.,]+","");
					  if(bgpGroupData!=null){
						  bgpGroupData.setPeerAddress(peerAddress);
						  String remoteAs=lines[x+1];
						  String  remoteas = remoteAs.replaceAll("[^0-9]+","");
						  bgpGroupData.setPeeras(remoteas);
						  x++;
						  if(bgpGroupData.getLocalas()!=null  && bgpGroupData.getLocalas().equals(bgpGroupData.getPeeras())){
							  bgpGroupData.setType("internal");
						   }else{
							   bgpGroupData.setType("external");
						   }
						  
					  }
				  }
				  //System.out.println(line);
			  } //end of the loop
			//show running-config router bgp
			return bgpGroupDataList;
		}

		@Override
		public List<MplsNeighborDataTable> findMplsNeighborDetails(Router router) {
			// TODO Auto-generated method stub
			return null;
		}

}
