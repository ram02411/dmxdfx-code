package com.ecode.topology.viewer.data.device.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ecode.topology.viewer.data.device.model.BgpGroupData;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

/**
 * 
 * @author 305 This parser is used to fetch Bgp group detail for devices
 *
 */
public class BgpGroupsParser {

	/**
	 * Method which returns al the group for devices...
	 * 
	 * @param device
	 * @return
	 */
	public static List<BgpGroupData> findDeviceBgpGroupsByConfiguration(Device device) {
		List<BgpGroupData> bgpGroupDataList = new ArrayList<BgpGroupData>();
		try {
			XMLBuilder query = new XMLBuilder();
			XML xmlQuery = query.createNewXML("get-configuration");
			XML rpcReply = device.executeRPC(xmlQuery);
			bgpGroupDataList = parseConfiguration(rpcReply);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bgpGroupDataList;
	}

	private static List<BgpGroupData> parseConfiguration(XML rpc_reply) {
		List<BgpGroupData> bgpGroupDataList = new ArrayList<BgpGroupData>();
		List<String> list = Arrays.asList("bgp", "group");
		List bgpGroupResult = rpc_reply.findNodes(list);
		if (bgpGroupResult == null)
			return bgpGroupDataList;
	
		Iterator iter = bgpGroupResult.iterator();
		while (iter.hasNext()) {
			List<String> imports = new ArrayList<String>();
			List<String> exports = new ArrayList<String>();
			Node node = (Node) iter.next();
			NodeList bgpGroupNodes = node.getChildNodes();
			BgpGroupData bgpGroupData = new BgpGroupData();
			for (int i = 0; i < bgpGroupNodes.getLength(); i++) {
				Node bgpGroupNode = (Node) bgpGroupNodes.item(i);
				if (bgpGroupNode.getNodeName().equals("import")) {
					String importText = bgpGroupNode.getTextContent().trim();
					if (importText != null)
						imports.add(importText);
				} else if (bgpGroupNode.getNodeName().equals("export")) {
					String exportText = bgpGroupNode.getTextContent().trim();
					if (exportText != null)
						exports.add(exportText);
				} else if (bgpGroupNode.getNodeName().equals("type")) {
					String typeText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setType(typeText != null ? typeText.trim() : "");
				} else if (bgpGroupNode.getNodeName().equals("name")) {
					String nameText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setName(nameText != null ? nameText.trim() : "");
				} else if (bgpGroupNode.getNodeName().equals("local-address")) {
					String localaddressText = bgpGroupNode.getTextContent().trim();
					int index = localaddressText.indexOf("+");
					if (index == -1) {
						bgpGroupData.setLocalAddress(localaddressText != null ? localaddressText.trim() : "");
					} else {
						String localAddress = localaddressText.substring(0, index).trim();
						bgpGroupData.setLocalAddress(localAddress);
					}
				} else if (bgpGroupNode.getNodeName().equals("neighbor")) {
					NodeList neighborChildList = bgpGroupNode.getChildNodes();
					for (int c = 0; c < neighborChildList.getLength(); c++) {
						Node neighborChildNode = (Node) neighborChildList.item(c);
						if (neighborChildNode.getNodeName().equals("peer-as")) {
							String pearasText = neighborChildNode.getTextContent().trim();
							bgpGroupData.setPeeras(pearasText);
						} else if (neighborChildNode.getNodeName().equals("name")) {
							String peeraddressText = neighborChildNode.getTextContent().trim();
							int index = peeraddressText.indexOf("+");
							if (index == -1) {
								bgpGroupData.setPeerAddress(peeraddressText.trim());
							} else {
								String peerAddress = peeraddressText.substring(0, index).trim();
								bgpGroupData.setPeerAddress(peerAddress);
							}
						} else if (neighborChildNode.getNodeName().equals("local-as")) {
							 NodeList localasNodeList=neighborChildNode.getChildNodes();
								for (int l = 0; l < localasNodeList.getLength(); l++) {
									Node cnode = localasNodeList.item(l);
									if("as-number".equals(cnode.getNodeName())){
									  String localas = cnode.getTextContent().trim();//as-number
									 bgpGroupData.setLocalas(localas);
									 break;
									} 
								}
						}
					}
				} // end of neighbor
				bgpGroupData.setExports(exports);
				bgpGroupData.setIimports(imports);
			} // end of for loop
			bgpGroupDataList.add(bgpGroupData);
		 }//end of while loop
		return bgpGroupDataList;
	}

	/**
	 * Method which returns al the group for devices...
	 * 
	 * @param device
	 * @return
	 */
	public static List<BgpGroupData> findDeviceBgpGroups(Device device) {
		List<BgpGroupData> bgpGroupDataList = new ArrayList<BgpGroupData>();
		try {
			XMLBuilder query = new XMLBuilder();
			XML xmlQuery = query.createNewXML("get-bgp-group-information");
			XML rpcReply = device.executeRPC(xmlQuery);
			bgpGroupDataList = parse(rpcReply);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bgpGroupDataList;
	}

	private static List<BgpGroupData> parse(XML rpc_reply) {
		List<BgpGroupData> bgpGroupDataList = new ArrayList<BgpGroupData>();
		List<String> list = Arrays.asList("bgp-group-information", "bgp-group");
		List bgpGroupResult = rpc_reply.findNodes(list);
		if (bgpGroupResult == null)
			return bgpGroupDataList;

		Iterator iter = bgpGroupResult.iterator();
		while (iter.hasNext()) {
			Node node = (Node) iter.next();
			NodeList bgpGroupNodes = node.getChildNodes();
			BgpGroupData bgpGroupData = new BgpGroupData();
			for (int i = 0; i < bgpGroupNodes.getLength(); i++) {
				Node bgpGroupNode = (Node) bgpGroupNodes.item(i);
				if (bgpGroupNode.getNodeName().equals("peer-address")) {
					String peeraddressText = bgpGroupNode.getTextContent().trim();
					int index = peeraddressText.indexOf("+");
					if (index == -1) {
						bgpGroupData.setPeerAddress(peeraddressText.trim());
					} else {
						String peerAddress = peeraddressText.substring(0, index).trim();
						bgpGroupData.setPeerAddress(peerAddress);
					}
				} else if (bgpGroupNode.getNodeName().equals("type")) {
					String typeText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setType(typeText != null ? typeText.trim() : "");
				} else if (bgpGroupNode.getNodeName().equals("name")) {
					String nameText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setName(nameText != null ? nameText.trim() : "");
				} else if (bgpGroupNode.getNodeName().equals("peer-as")) {
					String peerasText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setPeeras(peerasText);
				} else if (bgpGroupNode.getNodeName().equals("local-as")) {
					String localasText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setLocalas(localasText);
				} else if (bgpGroupNode.getNodeName().equals("local-address")) {
					String localaddressText = bgpGroupNode.getTextContent().trim();
					int index = localaddressText.indexOf("+");
					if (index == -1) {
						bgpGroupData.setLocalAddress(localaddressText != null ? localaddressText.trim() : "");
					} else {
						String localAddress = localaddressText.substring(0, index).trim();
						bgpGroupData.setLocalAddress(localAddress);
					}
				} else if (bgpGroupNode.getNodeName().equals("peer-count")) {
					String peerCountText = bgpGroupNode.getTextContent().trim();
					bgpGroupData.setPeercount(Integer.parseInt(peerCountText));
				}
			}
			bgpGroupDataList.add(bgpGroupData);
		}
		return bgpGroupDataList;
	}
}
