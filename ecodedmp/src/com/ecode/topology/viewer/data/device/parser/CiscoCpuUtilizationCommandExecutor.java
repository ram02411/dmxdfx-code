package com.ecode.topology.viewer.data.device.parser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ecode.topology.viewer.data.device.Category;
import com.ecode.topology.viewer.data.device.Router;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.juniper.container.data.controller.model.RamMemoryUsageVO;

/**
 * 
 *   @author java
 *   class used to computes the cpu utilization based on CLI
 *
 */
public class CiscoCpuUtilizationCommandExecutor {
	
	 private static final Log logger = LogFactory.getLog(CiscoCpuUtilizationCommandExecutor.class);
	
	/**
	 *  This method computes the cpu utilization for 
	 *  device .
	 * @param router         
	 * @return
	 */
	public static RamMemoryUsageVO  findCiscoCpuUtilization(Router router) {
        final   String command="show processes cpu";
		RamMemoryUsageVO ramMemoryUsageVO=new RamMemoryUsageVO();
		if(logger.isDebugEnabled()){
			logger.debug("Executing the method findCiscoCpuUtilization with router = "+router);
		}
		/*float totalcpu=100;
		float freecpu=98;
		float usedcpu=totalcpu-freecpu;
		float usedcpuper=(usedcpu/totalcpu)*100;
		float freecpuper=(freecpu/totalcpu)*100;
		float usedcpuPie=(float) ((0.01*5.0*usedcpuper*1000)/1000);
		float freecpuPie=(float) ((0.01*5.0*freecpuper*1000)/1000);
		
		category1.setId(1);
		category1.setName("Used Memory :"+usedMemory+"KB");
		category1.setValue(usedMemoryPie);
		category2.setId(2);
		category2.setName("Free Memory :" +freeMemory+"KB");
		category2.setValue(freeMemoryPie);
		category3.setId(3);
		category3.setName("Used CPU :"+usedcpu);
		category3.setValue(usedcpuPie);
		category4.setId(4);
		category4.setName("Free CPU :"+freecpu);
		category4.setValue(freecpuPie);
		
		List<Category>  categories=new ArrayList<>();
		categories.add(category1);
		categories.add(category2);
		categories.add(category3);
		categories.add(category4);*/
	 try {
		 //String command = "ls -la";
         String host = router.getIp();
         String user = router.getUsername();
         JSch jsch = new JSch();
         Session session = jsch.getSession(user, host, 22);
         Properties config = new Properties();
         config.put("StrictHostKeyChecking", "no");
         session.setConfig(config);;
         session.setPassword(router.getPassword());
         session.connect();
         Channel channel = session.openChannel("exec");
         ((ChannelExec)channel).setCommand(command);
         channel.setInputStream(null);
         ((ChannelExec)channel).setErrStream(System.err);
         InputStream input = channel.getInputStream();
         channel.connect();
         if(logger.isDebugEnabled())
         logger.debug("Channel Connected to machine " + host + " server with command: " + command );
         InputStreamReader inputReader=null;
         BufferedReader bufferedReader=null;
         try{
        	 inputReader = new InputStreamReader(input);
             bufferedReader = new BufferedReader(inputReader);
             String line = null;
             int count=0;
             while((line = bufferedReader.readLine()) != null) {
            	  	if(count==10) {
          	    			break;
            	  	}		
          	    	else {
          	    			count++;
          	    	}
            	 	if(logger.isDebugEnabled())
            	 		logger.debug("CO  = "+line);
	            	 if(line!=null && line.toUpperCase().contains("CPU utilization for".toUpperCase())) {
	            		  String cpuUtilizationTokens[]=line.split(";");
	      			    if(cpuUtilizationTokens!=null && cpuUtilizationTokens.length>0){
	      			    	for(String token:cpuUtilizationTokens){
	      			    		 if(token.toUpperCase().contains("ONE MINUTE")){
	      			    			 	String cpuUsedPercentage=token.substring(token.indexOf(":")+1,token.indexOf("%"));
	      			    			 	if(cpuUsedPercentage!=null)
	      			    			 		cpuUsedPercentage=cpuUsedPercentage.trim();
	      			    			 	else
	      			    			 		cpuUsedPercentage="0";
	      						    	String cpuUnusedPercentage=(100-Integer.parseInt(cpuUsedPercentage))+"";
	      						    	ramMemoryUsageVO.setFreecpu(cpuUnusedPercentage); 
	      						    	ramMemoryUsageVO.setUsedcpu(cpuUsedPercentage);
	      						    	//System.out.println("cpuUsedPercentage  = "+cpuUsedPercentage+" , cpuUnusedPercentage = "+cpuUnusedPercentage);			    			 
	      			    		 }
	      			    	}
	      			    }
	     		    }//end of if
             }	 	//end of while
         }catch(Exception ex){
        	 StringWriter stack = new StringWriter();
        	 ex.printStackTrace(new PrintWriter(stack));
             //ex.printStackTrace();
             if(logger.isErrorEnabled())
                 logger.error(stack.toString());
         }finally{
        	 if(bufferedReader!=null)
        	 bufferedReader.close();
        	 if(inputReader!=null)
             inputReader.close();
         }
         channel.disconnect();
         session.disconnect();
     }catch(Throwable ex){
        	 StringWriter stack = new StringWriter();
        	 ex.printStackTrace(new PrintWriter(stack));
        	 if(logger.isErrorEnabled())
	             logger.error(stack.toString());
	        	 
             //ex.printStackTrace();
        }
	 return ramMemoryUsageVO;
	}
}
