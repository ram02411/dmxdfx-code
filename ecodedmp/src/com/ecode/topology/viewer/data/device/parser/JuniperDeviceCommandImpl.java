package com.ecode.topology.viewer.data.device.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ecode.topology.viewer.data.device.BgpPeerDataTable;
import com.ecode.topology.viewer.data.device.Category;
import com.ecode.topology.viewer.data.device.DeviceCommand;
import com.ecode.topology.viewer.data.device.EcodeTopologyDeviceViewerImpl;
import com.ecode.topology.viewer.data.device.MplsNeighborDataTable;
import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.model.BgpGroupData;
import com.ecode.topology.viewer.data.device.model.NeighborDetailNodeCategoryVO;
import com.juniper.container.data.controller.model.RamMemoryUsageVO;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

/**
 * 
 * @author 305
 *
 */
@Service("JuniperDeviceCommandImpl")
@Scope("singleton")
public class JuniperDeviceCommandImpl implements DeviceCommand {
	
	private static final Log logger = LogFactory.getLog(JuniperDeviceCommandImpl.class);
	
	@Override
	public  NeighborDetailNodeCategoryVO getNeighborDetails(Router router) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method... getNeighborDetails..router = "+router);

		    Map<String, List<String>> mac_interface = new HashMap<String, List<String>>();
	        NeighborDetailNodeCategoryVO neighborDetailNodeCategoryVO=new  NeighborDetailNodeCategoryVO();
			 neighborDetailNodeCategoryVO.setNeighborDetails(mac_interface);
			 Device device =null;
			 try {
			    device = new Device(router.getIp(), router.getUsername(), router.getPassword(), null, 830);
			    if(logger.isDebugEnabled())
					logger.debug("____Please wait ....device is connecting and processing the data for topology_______hostname = "+device.gethostName()+" , hostip = "+device.gethostName());
			    
			   // System.out.println("____Please wait ....device is connecting and processing the data for topology________"+device.gethostName());
			   //logger.debug("____Please wait ....device is connecting and processing the data for topology________"+device.gethostName());
			   device.connect();
				try {
					XMLBuilder query = new XMLBuilder();
					XML q = query.createNewXML("get-ipv6-nd-information");
					XML rpc_reply = device.executeRPC(q);
					mac_interface = parse(rpc_reply);
					//PLEASE COMMENTED TEMPORARLY
					List<Category>     nodeCategoryList=findCategoriesForNodes(device);
					  if(logger.isDebugEnabled())
						  logger.debug("nodeCategoryList = "+nodeCategoryList);
					  
				    neighborDetailNodeCategoryVO.setNodeCategotyList(nodeCategoryList);
					neighborDetailNodeCategoryVO.setNeighborDetails(mac_interface);
					  if(logger.isDebugEnabled())
						  logger.debug("neighborDetailNodeCategoryVO = "+neighborDetailNodeCategoryVO);
				} catch (Exception e) {
					  if(logger.isErrorEnabled())
						  logger.error("Error Message = "+e.getMessage());
					e.printStackTrace();
				}
			 }catch(Exception exe){
				  if(logger.isErrorEnabled())
					  logger.error("Error Message = "+exe.getMessage());
				 exe.printStackTrace();
			 }finally{
				   if(device!=null){
						  device.close();
					   }
				   }

		return neighborDetailNodeCategoryVO;
	}

	private  Map<String, List<String>> parse(XML rpc_reply) {
		List<String> list = Arrays.asList("ipv6-nd-information", "ipv6-nd-entry");
		Map<String, List<String>> neighbors = new HashMap<String, List<String>>();
		List ipv6_neighbour_list = rpc_reply.findNodes(list);
		if(ipv6_neighbour_list==null){
			return neighbors;
		}
		Iterator iter = ipv6_neighbour_list.iterator();
		
		while (iter.hasNext()) {
			List<String> neighborList=new ArrayList<String>(2);
			neighborList.add("UK");
			neighborList.add("UK");
			Node node = (Node) iter.next();
			NodeList child_nodes_ipv6 = node.getChildNodes();
			String l2_neighbor_mac = "";
			String l2_self_interface = "";
			String neighborIv6Address="";
			for (int i = 0; i < child_nodes_ipv6.getLength(); i++) {
				Node child_node = (Node) child_nodes_ipv6.item(i);
				if (child_node.getNodeName().equals("ipv6-nd-neighbor-l2-address")) {
					String[] l2neighbor_string = child_node.getTextContent().split("\n");
					l2_neighbor_mac = (l2neighbor_string[1]);
					
				}
				else if (child_node.getNodeName().equals("ipv6-nd-neighbor-address")) {
					String[] neighborIv6AddressString = child_node.getTextContent().split("\n");
					neighborIv6Address = (neighborIv6AddressString[1]);
					neighborList.set(0,neighborIv6Address!=null?neighborIv6Address.toUpperCase():neighborIv6Address);
				}
				else if (child_node.getNodeName().equals("ipv6-nd-interface-name")) {
					String[] l2interface_string = child_node.getTextContent().split("\n");
					//String l2interface_string_sub = l2interface_string[1].trim().split("\\.")[0];
					//l2_self_interface = (l2interface_string_sub);
					neighborList.set(1,l2interface_string[1]);
					//neighborList.add(l2interface_string[1]);
				}
			}
			neighbors.put(l2_neighbor_mac, neighborList);
		}
		return neighbors;
	}
	
	private List<Category>   findCategoriesForNodes(Device    device){
		if(logger.isDebugEnabled())
			logger.debug("Executing the method .. - findCategoriesForNodes");
		RamMemoryUsageVO memoryUsageVO=DeviceMemoryCpuUsageParser.fetchDeviceMemoryUsage(device);
		if(logger.isDebugEnabled())
			logger.debug("memoryUsageVO  = "+memoryUsageVO+" for hostname = "+device.gethostName());
		Category category1=new Category();
		Category category2=new Category();
		Category category3=new Category();
		Category category4=new Category();
		
		float totalMemory=Float.parseFloat(memoryUsageVO.getUsedmemory())+Float.parseFloat(memoryUsageVO.getFreememory());
		float usedMemory=Float.parseFloat(memoryUsageVO.getUsedmemory());
		float freeMemory=Float.parseFloat(memoryUsageVO.getFreememory());
		logger.debug("usedmemory = "+usedMemory   +"freememory ="+freeMemory);
		float usedmemoryper=0;
		float freememoryper=0;
		float usedMemoryPie=0;
		float freeMemoryPie=0;
		if(totalMemory>0){
		usedmemoryper=(usedMemory/totalMemory)*100;
		freememoryper=(freeMemory/totalMemory)*100;
		usedMemoryPie=(float) ((0.01*5.0*usedmemoryper*1000)/1000);
		freeMemoryPie=(float) ((0.01*5.0*freememoryper*1000)/1000);
		}
		float totalcpu=Float.parseFloat(memoryUsageVO.getUsedcpu())+Float.parseFloat(memoryUsageVO.getFreecpu());
		float usedcpu=Float.parseFloat(memoryUsageVO.getUsedcpu());
		float freecpu=Float.parseFloat(memoryUsageVO.getFreecpu());
		//float totalcpu=100;
		//float freecpu=98;
		//float usedcpu=totalcpu-freecpu;
		logger.debug("freecpu= "+freecpu   +"usedcpu ="+usedcpu);
		float usedcpuper=0;
		float freecpuper=0;
		float usedcpuPie=0;
		float freecpuPie=0;
		if(totalcpu>0){
		usedcpuper=(usedcpu/totalcpu)*100;
		freecpuper=(freecpu/totalcpu)*100;
		usedcpuPie=(float) ((0.01*5.0*usedcpuper*1000)/1000);
		freecpuPie=(float) ((0.01*5.0*freecpuper*1000)/1000);
		}
		category1.setId(1);
		category1.setName("Used Memory :"+usedMemory+"KB");
		category1.setValue(usedMemoryPie);
		category2.setId(2);
		category2.setName("Free Memory :" +freeMemory+"KB");
		category2.setValue(freeMemoryPie);
		category3.setId(3);
		category3.setName("Used CPU :"+usedcpu);
		category3.setValue(usedcpuPie);
		category4.setId(4);
		category4.setName("Free CPU :"+freecpu);
		category4.setValue(freecpuPie);
		
		List<Category>  categories=new ArrayList<>();
		categories.add(category1);
		categories.add(category2);
		categories.add(category3);
		categories.add(category4);
		if(logger.isDebugEnabled())
			logger.debug("categories = "+categories);
		return categories;
	}

	@Override
	public List<BgpPeerDataTable> findBgpNeighborDetails(Router router) {
		Device device=null;
		List<BgpPeerDataTable> bgpPeerDataTablesList = new ArrayList<BgpPeerDataTable>();
		try {
			   //System.out.println("____Please wait ....device is connecting and processing the data for topology________");
			   device = new Device(router.getIp(), router.getUsername(), router.getPassword(), null, 830);
			   device.connect();
			   bgpPeerDataTablesList=BgpLogicalNeighborsParser.getDeviceBgpInformation(device);
		}catch(Exception exe){
			exe.printStackTrace();
		}finally{
			if(device!=null){
				  device.close();
				  device=null;
			   }
		}	
		return bgpPeerDataTablesList;
	}
	@Override
	public List<MplsNeighborDataTable> findMplsNeighborDetails(Router router) {
		Device device=null;
		List<MplsNeighborDataTable> mplsneighborDataTable=new ArrayList<MplsNeighborDataTable>();
		try {
			   //System.out.println("____Please wait ....device is connecting and processing the data for topology________");
			   device = new Device(router.getIp(), router.getUsername(), router.getPassword(), null, 830);
			   device.connect();
			   mplsneighborDataTable=MplsLogicalNeighborsParser.getDeviceMplsInformation(device);
		}catch(Exception exe){
			exe.printStackTrace();
		}finally{
			if(device!=null){
				  device.close();
				  device=null;
			   }
		}	
		return mplsneighborDataTable;
	}
	@Override
	public List<BgpGroupData> findDeviceBgpGroups(Router router) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NeighborDetailNodeCategoryVO getPathNeighborDetails(Router router) {
		if(logger.isDebugEnabled())
			logger.debug("Executing method... getNeighborDetails..router = "+router);

		    Map<String, List<String>> mac_interface = new HashMap<String, List<String>>();
	        NeighborDetailNodeCategoryVO neighborDetailNodeCategoryVO=new  NeighborDetailNodeCategoryVO();
			 neighborDetailNodeCategoryVO.setNeighborDetails(mac_interface);
			 Device device =null;
			 try {
			    device = new Device(router.getIp(), router.getUsername(), router.getPassword(), null, 830);
			    if(logger.isDebugEnabled())
					logger.debug("____Please wait ....device is connecting and processing the data for topology_______hostname = "+device.gethostName()+" , hostip = "+device.gethostName());
			    
			    System.out.println("____Please wait ....device is connecting and processing the data for topology________"+device.gethostName());
			   //logger.debug("____Please wait ....device is connecting and processing the data for topology________"+device.gethostName());
			   device.connect();
				try {
					XMLBuilder query = new XMLBuilder();
					XML q = query.createNewXML("get-ipv6-nd-information");
					XML rpc_reply = device.executeRPC(q);
					mac_interface = parse(rpc_reply);
					//PLEASE COMMENTED TEMPORARLY
					
					neighborDetailNodeCategoryVO.setNeighborDetails(mac_interface);
					  if(logger.isDebugEnabled())
						  logger.debug("neighborDetailNodeCategoryVO = "+neighborDetailNodeCategoryVO);
				} catch (Exception e) {
					  if(logger.isErrorEnabled())
						  logger.error("Error Message = "+e.getMessage());
					e.printStackTrace();
				}
			 }catch(Exception exe){
				  if(logger.isErrorEnabled())
					  logger.error("Error Message = "+exe.getMessage());
				 exe.printStackTrace();
			 }finally{
				   if(device!=null){
						  device.close();
					   }
				   }

		return neighborDetailNodeCategoryVO;
	}

	
}
