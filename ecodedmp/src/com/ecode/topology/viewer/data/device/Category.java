package com.ecode.topology.viewer.data.device;

import java.util.List;

public class Category {

	private int id;
	private String name;
	private float value;
	private List<SubValue> subvalues;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	
	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + ", value=" + value + "]";
	}

}
