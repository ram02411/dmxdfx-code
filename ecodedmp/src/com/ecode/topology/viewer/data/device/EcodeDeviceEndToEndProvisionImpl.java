package com.ecode.topology.viewer.data.device;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import com.ecode.cisco.command.SSHClient;
import com.ecode.topology.viewer.data.device.model.BgpGroupData;
import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.topology.viewer.data.device.model.VPNProvisionVO;
import com.ecode.topology.viewer.data.device.parser.ApplyPolicyRouterUtil;
import com.ecode.topology.viewer.data.device.parser.BgpGroupsDeviceUtil;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;
import com.juniper.container.data.controller.CreateBgp;
import com.juniper.container.data.model.configuration.protocols.Bgp;
import com.juniper.container.data.model.configuration.protocols.BgpGroup;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.juniper.container.data.model.configuration.protocols.LocalAs;
import com.juniper.container.data.model.configuration.protocols.Neighbor;
import com.juniper.container.data.service.JuniperBgpService;

/**
 * 
 * @author java
 *  This class is used to perform end to  end provisioning
 *  of selected router in L2 topology
 *
 */
@Service("EcodeDeviceEndToEndProvisionImpl")
@Scope("singleton")
public class EcodeDeviceEndToEndProvisionImpl implements EcodeDeviceEndToEndProvision {

	private static final Log logger = LogFactory.getLog(EcodeDeviceEndToEndProvisionImpl.class);

	@Autowired
	@Qualifier("JuniperBgpServiceImpl")
	private JuniperBgpService juniperBgpServiceImpl;

	@Autowired
	@Qualifier("JuniperDeviceCommandImpl")
	private DeviceCommand juniperDevice;

	@Autowired
	@Qualifier("CiscoDeviceCommandImpl")
	private DeviceCommand ciscoDevice;

	/**
	 * 
	 * @param currentRouter
	 * @param nextRouter
	 * @return false if both are not bgp paired otherwise true if both are bgp
	 *         paired
	 */
	private boolean isBothRoutersBgpPaired(String currentRouterIp, String nextRouterIp) {
		if (logger.isDebugEnabled())
			logger.debug("isBothRoutersBgpPaired method is called with currentRouterIp = " + currentRouterIp
					+ " , nextRouterIp =" + nextRouterIp);
		// Fetching all the selected routers
		Map<String, Router> routerDetailIP6 = ApplicationRouterConfigurationUtil.mapIv6toRouters();
		Router currentRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouterIp);

		List<BgpPeerDataTable> bgpPeerDataList = new ArrayList<>();
		if ("cisco".equalsIgnoreCase(currentRouter.getType())) {
			bgpPeerDataList = ciscoDevice.findBgpNeighborDetails(currentRouter);
		} else {
			bgpPeerDataList = juniperDevice.findBgpNeighborDetails(currentRouter);
		}

		if (logger.isDebugEnabled())
			logger.debug("bgpPeerDataList =" + bgpPeerDataList);
		for (BgpPeerDataTable bgpPeerData : bgpPeerDataList) {
			Router toRouter = routerDetailIP6.get(bgpPeerData.getPeerAddress());
			if (logger.isDebugEnabled())
				logger.debug("toRouter =" + toRouter);
			if (toRouter != null) {
				if (nextRouterIp.equalsIgnoreCase(toRouter.getIp())) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * @param groupName
	 * @param type
	 * @param hostip
	 * @param neighborIp
	 * @param localAsNumber
	 * @param peerAs
	 * @param cexport
	 * @param cimport
	 * @return
	 */
	private CProtocols createNewBgpGroup(String groupName, String type, String hostip, String neighborIp,
			String localAsNumber, String peerAs,String cimport, String cexport) {
		// Creating new Group for Juniper
		CProtocols cProtocols = new CProtocols();
		Bgp bgp = new Bgp();
		cProtocols.setBgp(bgp);
		List<BgpGroup> bgpGroupList = new ArrayList<>();
		BgpGroup bgpGroup = new BgpGroup();
		bgpGroup.setName(groupName);
		bgpGroup.setLocaladdress(hostip); // CSR1
		List<Neighbor> neighbors = new ArrayList<>();
		Neighbor neighbor = new Neighbor();
		LocalAs localAs = new LocalAs();
		localAs.setAsnumber(localAsNumber);
		neighbor.setLocalas(localAs);
		neighbor.setName(neighborIp); // AG1
		neighbor.setPeeras(peerAs);
		neighbors.add(neighbor);
		bgpGroup.setNeighbor(neighbors);
		bgpGroup.setType(type);
		
		//List<String> export = new ArrayList<>();
		//export.add(cexport);
		
		//List<String> iimport = new ArrayList<>();
		//iimport.add(cimport);
		
		//bgpGroup.setIimport(iimport);
		//bgpGroup.setExport(export);
		
		bgpGroupList.add(bgpGroup);
		bgp.setBgpGroup(bgpGroupList);
		return cProtocols;
	}

	/**
	 * 
	 * @param vpnProvisionVO
	 * @return
	 */
	public String applyVpnProvisionForSelectedSourceDest(ServicenowProfileVO vpnProvisionVO) {
		if(logger.isDebugEnabled()){
			logger.debug("....................Executing inside the method = "+vpnProvisionVO+"..........................................");
		}
		/*vpnProvisionVO = new ServicenowProfileVO();
		vpnProvisionVO.setLocalas(vpnProvisionVO.getLocalas());
		vpnProvisionVO.setPeeras(vpnProvisionVO.getPeeras());
		vpnProvisionVO.setImports(vpnProvisionVO.getImports());
		vpnProvisionVO.setExports(vpnProvisionVO.getExports());
		vpnProvisionVO.setSourcelocalip(vpnProvisionVO.getSourcelocalip());
		vpnProvisionVO.setTargetlocalip(vpnProvisionVO.getTargetlocalip());*/
		//String selectedVpnPath = "IRLARCPGPAR001-AG1-1(172.25.0.24),IRLARCPGESR004-920iBot1R03(172.25.0.37),IRLARCPGES006-ACX2200TOP(172.25.0.151),IRLARCPGESR005-920_O_1R04(172.25.0.40)";
		//vpnProvisionVO.setSelectedLogicPath(selectedVpnPath);
		//vpnProvisionVO.setType(vpnProvisionVO.getType());

		if (logger.isDebugEnabled())
			logger.debug("Executing the method with vpn provisioning detail is  = " + vpnProvisionVO);
        String selectedVpnPath=vpnProvisionVO.getSelectedLogicPath();
        String asnumber=vpnProvisionVO.getLocalas();
		String stringTokens[] = selectedVpnPath.split(",");
		List<String> l2PathRouterIpList = new ArrayList<>();
		for (String token : stringTokens) {
			String ipAddress = token.substring(token.indexOf("(") + 1, token.length() - 1);
			l2PathRouterIpList.add(ipAddress);
		}
		if(logger.isDebugEnabled()){
			logger.debug("l2PathRouterIpList = "+l2PathRouterIpList);
		}
		String gropus[] = vpnProvisionVO.getGroupname().split(",");
		//setting the group name
		vpnProvisionVO.setGroupname(gropus[0]);

		/*
		 * String cimport="1.1.1.1/24"; String cexport="2.2.2.2/24";
		 */
		final String cimport = vpnProvisionVO.getImports();
		final String cexport = vpnProvisionVO.getExports();
		
		if(logger.isDebugEnabled()) {
			 logger.debug("Form cimport = "+cimport+" , cexport = "+cexport );
		}
		
		// System.out.println("____new group for CSR1");
		if (logger.isDebugEnabled())
			logger.debug("New Group created for Juniper device is = " + vpnProvisionVO);
		
		// l2PathRouterIpList ->contains iv4 ips for all the routers in selected
		// path
		String currentRouterIp;
		String nextRouterIp;
		if(  l2PathRouterIpList.size()!=0) {
					
			 currentRouterIp = l2PathRouterIpList.get(0);
			 nextRouterIp = l2PathRouterIpList.get(l2PathRouterIpList.size()-1);
			
			
			if (logger.isDebugEnabled())
				logger.debug("currentRouterIp = "+currentRouterIp+" , nextRouterIp = "+nextRouterIp);
		
			//Creating new group and swapping the import and export statement what ever we have filled in the form
			 CProtocols cProtocols=createNewBgpGroup(vpnProvisionVO.getGroupname()+"_"+nextRouterIp,vpnProvisionVO.getType(),currentRouterIp,nextRouterIp,vpnProvisionVO.getLocalas(),vpnProvisionVO.getPeeras(),cexport,cimport);
			 if(logger.isDebugEnabled()){
					logger.debug("New local group = "+cProtocols );
			} 
			//updating the group value for Neighbor router
			 CProtocols gProtocols=createNewBgpGroup(vpnProvisionVO.getGroupname()+"_"+currentRouterIp,vpnProvisionVO.getType(),nextRouterIp,currentRouterIp,vpnProvisionVO.getLocalas(),vpnProvisionVO.getPeeras(),cimport,cexport);
			 if(logger.isDebugEnabled()){
					logger.debug("New Neghbor group = "+gProtocols );
			} 
			// checking current router is bgp paired with others or not....
			boolean isBgpPaired = isBothRoutersBgpPaired(currentRouterIp, nextRouterIp);
			 if(logger.isDebugEnabled()){
					logger.debug("isBgpPaired= "+isBgpPaired );
			} 
			// Case1 ->> when current router is bgp paired with next router
			if (isBgpPaired) {
				
				/*Router currentRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouterIp);
				Router nextRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouterIp);
				 if(logger.isDebugEnabled()){
					 logger.debug("currentRouter= "+currentRouter+" , nextRouter =  "+nextRouter );
				 }
				 
				//Logic for local router 
				if ("cisco".equalsIgnoreCase(currentRouter.getType())) {
					SSHClient ssh = new SSHClient(currentRouter.getIp(), currentRouter.getUsername(),	currentRouter.getPassword());
					 if(logger.isDebugEnabled()) {
						 logger.debug("Applying import and exports statemetns "+cProtocols.getBgp()+" on router ="+currentRouter );
					 }
					ssh.exportImportCiscoBgpGroup(currentRouter, cProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouter.getIp()),
							cexport, cimport);// swapping the import and export statements as per mentioned in the form
					//cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+cProtocols);
					 }
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", currentRouter.getIp() + "_"
							+ currentRouter.getUsername() + "_" + currentRouter.getPassword(), true);
				}
				//	//Logic for neighbor router
				if ("cisco".equalsIgnoreCase(nextRouter.getType())) {
					SSHClient ssh = new SSHClient(nextRouter.getIp(), nextRouter.getUsername(),
							nextRouter.getPassword());
					ssh.exportImportCiscoBgpGroup(nextRouter, gProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouter.getIp()),
							cimport, cexport);// 
					//cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+gProtocols);
					 }
					juniperBgpServiceImpl.provisionTraceOptions(gProtocols, "merge",
							nextRouter.getIp() + "_" + nextRouter.getUsername() + "_" + nextRouter.getPassword(), true);
				}*/

			} // Case2 ->> when current router is not bgp paired with next
				// router
			else {
				
				Router currentRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouterIp);
				Router nextRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouterIp);
				 if(logger.isDebugEnabled()){
					 logger.debug("currentRouter= "+currentRouter+" , nextRouter =  "+nextRouter );
				 }
				//Logic for local router 
				if ("cisco".equalsIgnoreCase(currentRouter.getType())) {
					CreateBgp.createBGPCisco(currentRouter, asnumber, nextRouterIp);
				} else {
					//ApplyPolicyRouterUtil.applyRouterPolicy(
							//ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouter.getIp()),
							//cexport,cimport);// CSR2
					//CreateBgp.createBGPJuniper(currentRouter, asnumber, nextRouterIp);
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+gProtocols);
					 }
					//cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", currentRouter.getIp() + "_"
							+ currentRouter.getUsername() + "_" + currentRouter.getPassword(), true);
				}

				//Logic for  neighbor router 
				if ("cisco".equalsIgnoreCase(nextRouter.getType())) {
					CreateBgp.createBGPCisco(nextRouter, asnumber, currentRouterIp);
				} else {
					//ApplyPolicyRouterUtil.applyRouterPolicy(
						//	ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouter.getIp()),
							//cimport, cexport);// CSR2
					//CreateBgp.createBGPJuniper(nextRouter, asnumber, currentRouterIp);
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+gProtocols);
					 }
					juniperBgpServiceImpl.provisionTraceOptions(gProtocols, "merge",
							nextRouter.getIp() + "_" + nextRouter.getUsername() + "_" + nextRouter.getPassword(), true);
				}
			}
		}
		// Special case first Node and last node
		return "done";
	}
	
	
	/**
	 * 
	 * @param vpnProvisionVO
	 * @return
	 */
	public String testing(VPNProvisionVO vpnProvisionVO) {
		if(logger.isDebugEnabled()){
			logger.debug("....................Executing inside the method = "+vpnProvisionVO+"..........................................");
		}
		vpnProvisionVO = new VPNProvisionVO();
		vpnProvisionVO.setLocalas("64321");
		vpnProvisionVO.setPeeras("64321");
		vpnProvisionVO.setImports("1.1.1.1/24");
		vpnProvisionVO.setExports("2.2.2.2/24");
		vpnProvisionVO.setSourceLocalIP("172.25.0.24");
		vpnProvisionVO.setTargetLocalIP("172.25.0.40");
		String selectedVpnPath = "IRLARCPGPAR001-AG1-1(172.25.0.24),IRLARCPGESR004-920iBot1R03(172.25.0.37),IRLARCPGES006-ACX2200TOP(172.25.0.151),IRLARCPGESR005-920_O_1R04(172.25.0.40)";
		vpnProvisionVO.setSelectedLogicPath(selectedVpnPath);
		vpnProvisionVO.setType("�nternal");
		vpnProvisionVO.setGroupName("Junit,Junit");

		if (logger.isDebugEnabled())
			logger.debug("Executing the method with vpn provisioning detail is  = " + vpnProvisionVO);

		String stringTokens[] = selectedVpnPath.split(",");
		List<String> l2PathRouterIpList = new ArrayList<>();
		for (String token : stringTokens) {
			String ipAddress = token.substring(token.indexOf("(") + 1, token.length() - 1);
			l2PathRouterIpList.add(ipAddress);
		}
		if(logger.isDebugEnabled()){
			logger.debug("l2PathRouterIpList = "+l2PathRouterIpList);
		}
		String gropus[] = vpnProvisionVO.getGroupName().split(",");
		//setting the group name
		vpnProvisionVO.setGroupName("Ecode");

		/*
		 * String cimport="1.1.1.1/24"; String cexport="2.2.2.2/24";
		 */
		final String cimport = vpnProvisionVO.getImports();
		final String cexport = vpnProvisionVO.getExports();
		
		if(logger.isDebugEnabled()) {
			 logger.debug("Form cimport = "+cimport+" , cexport = "+cexport );
		}
		
		// System.out.println("____new group for CSR1");
		if (logger.isDebugEnabled())
			logger.debug("New Group created for Juniper device is = " + vpnProvisionVO);
		
		// l2PathRouterIpList ->contains iv4 ips for all the routers in selected
		// path
		for (int rindex = 0; rindex < l2PathRouterIpList.size() - 1; rindex++) {
			String currentRouterIp = l2PathRouterIpList.get(rindex);
			String nextRouterIp = l2PathRouterIpList.get(rindex + 1);
			if (logger.isDebugEnabled())
				logger.debug("currentRouterIp = "+currentRouterIp+" , nextRouterIp = "+nextRouterIp);
		
			//Creating new group and swapping the import and export statement what ever we have filled in the form
			 CProtocols cProtocols=createNewBgpGroup(vpnProvisionVO.getGroupName()+"_"+nextRouterIp,vpnProvisionVO.getType(),currentRouterIp,nextRouterIp,vpnProvisionVO.getLocalas(),vpnProvisionVO.getPeeras(),cexport,cimport);
			 if(logger.isDebugEnabled()){
					logger.debug("New local group = "+cProtocols );
			} 
			//updating the group value for Neighbor router
			 CProtocols gProtocols=createNewBgpGroup(vpnProvisionVO.getGroupName()+"_"+currentRouterIp,vpnProvisionVO.getType(),nextRouterIp,currentRouterIp,vpnProvisionVO.getLocalas(),vpnProvisionVO.getPeeras(),cimport,cexport);
			 if(logger.isDebugEnabled()){
					logger.debug("New Neghbor group = "+gProtocols );
			} 
			// checking current router is bgp paired with others or not....
			boolean isBgpPaired = isBothRoutersBgpPaired(currentRouterIp, nextRouterIp);
			 if(logger.isDebugEnabled()){
					logger.debug("isBgpPaired= "+isBgpPaired );
			} 
			// Case1 ->> when current router is bgp paired with next router
			if (isBgpPaired) {
				Router currentRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouterIp);
				Router nextRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouterIp);
				 if(logger.isDebugEnabled()){
					 logger.debug("currentRouter= "+currentRouter+" , nextRouter =  "+nextRouter );
				 }
				 
				//Logic for local router 
				if ("cisco".equalsIgnoreCase(currentRouter.getType())) {
					SSHClient ssh = new SSHClient(currentRouter.getIp(), currentRouter.getUsername(),	currentRouter.getPassword());
					 if(logger.isDebugEnabled()) {
						 logger.debug("Applying import and exports statemetns "+cProtocols.getBgp()+" on router ="+currentRouter );
					 }
					ssh.exportImportCiscoBgpGroup(currentRouter, cProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouter.getIp()),
							cexport, cimport);// swapping the import and export statements as per mentioned in the form
					//cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+cProtocols);
					 }
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", currentRouter.getIp() + "_"
							+ currentRouter.getUsername() + "_" + currentRouter.getPassword(), true);
				}
				//	//Logic for neighbor router
				if ("cisco".equalsIgnoreCase(nextRouter.getType())) {
					SSHClient ssh = new SSHClient(nextRouter.getIp(), nextRouter.getUsername(),
							nextRouter.getPassword());
					ssh.exportImportCiscoBgpGroup(nextRouter, gProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouter.getIp()),
							cimport, cexport);// 
					//cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+gProtocols);
					 }
					juniperBgpServiceImpl.provisionTraceOptions(gProtocols, "merge",
							nextRouter.getIp() + "_" + nextRouter.getUsername() + "_" + nextRouter.getPassword(), true);
				}

			} // Case2 ->> when current router is not bgp paired with next
				// router
			else {
				Router currentRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouterIp);
				Router nextRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouterIp);
				 if(logger.isDebugEnabled()){
					 logger.debug("currentRouter= "+currentRouter+" , nextRouter =  "+nextRouter );
				 }
				//Logic for local router 
				if ("cisco".equalsIgnoreCase(currentRouter.getType())) {
					SSHClient ssh = new SSHClient(currentRouter.getIp(), currentRouter.getUsername(),
							currentRouter.getPassword());
					ssh.createCiscoBgpGroup(currentRouter, cProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(currentRouter.getIp()),
							cexport,cimport);// CSR2
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+gProtocols);
					 }
					//cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", currentRouter.getIp() + "_"
							+ currentRouter.getUsername() + "_" + currentRouter.getPassword(), true);
				}

				//Logic for  neighbor router 
				if ("cisco".equalsIgnoreCase(nextRouter.getType())) {
					SSHClient ssh = new SSHClient(nextRouter.getIp(), nextRouter.getUsername(),
							nextRouter.getPassword());
					ssh.createCiscoBgpGroup(nextRouter, gProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(nextRouter.getIp()),
							cimport, cexport);// CSR2
					 if(logger.isDebugEnabled()){
						 logger.debug("Updating group for Juniper  router = "+gProtocols);
					 }
					juniperBgpServiceImpl.provisionTraceOptions(gProtocols, "merge",
							nextRouter.getIp() + "_" + nextRouter.getUsername() + "_" + nextRouter.getPassword(), true);
				}
			}
		}
		// Special case first Node and last node
		return "done";
	}

	@Override
	public String applyVpnSetup(VPNProvisionVO vpnProvisionVO) {

		String gropus[] = vpnProvisionVO.getGroupName().split(",");

		/*
		 * String cimport="1.1.1.1/24"; String cexport="2.2.2.2/24";
		 */
		final String cimport = vpnProvisionVO.getImports();
		final String cexport = vpnProvisionVO.getExports();
		// ApplicationContext applicationContext=new
		// ClassPathXmlApplicationContext("ecode-service-context.xml");
		// EcodeTopologyDeviceViewer
		// ecodeTopologyDeviceViewer=(EcodeTopologyDeviceViewer)applicationContext.getBean("EcodeTopologyDeviceViewerImpl");
		// JuniperBgpServiceImpl
		// juniperBgpServiceImpl=(JuniperBgpServiceImpl)applicationContext.getBean("JuniperBgpServiceImpl");
		// Create a bgp group form for new router CE2
		CProtocols cProtocols = new CProtocols();
		Bgp bgp = new Bgp();
		cProtocols.setBgp(bgp);
		List<BgpGroup> bgpGroupList = new ArrayList<>();
		BgpGroup bgpGroup = new BgpGroup();
		bgpGroup.setName(gropus[0]);
		bgpGroup.setLocaladdress(vpnProvisionVO.getSourceLocalIP()); // CSR1
		List<Neighbor> neighbors = new ArrayList<>();
		Neighbor neighbor = new Neighbor();
		LocalAs localAs = new LocalAs();
		localAs.setAsnumber(vpnProvisionVO.getLocalas());
		neighbor.setLocalas(localAs);
		neighbor.setName(vpnProvisionVO.getSourceNeighborIP()); // AG1
		neighbor.setPeeras(vpnProvisionVO.getPeeras());
		neighbors.add(neighbor);
		bgpGroup.setNeighbor(neighbors);
		bgpGroup.setType(vpnProvisionVO.getType());
		List<String> export = new ArrayList<>();
		export.add(cexport);
		List<String> iimport = new ArrayList<>();
		iimport.add(cimport);
		bgpGroup.setIimport(iimport);
		bgpGroup.setExport(export);
		bgpGroupList.add(bgpGroup);
		bgp.setBgpGroup(bgpGroupList);
		System.out.println("____new group for CSR1");
		String startRouterIP = vpnProvisionVO.getSourceLocalIP();
		Router startRouter = ApplicationRouterConfigurationUtil.findRouterByIpv4(startRouterIP);
		if (startRouter.getType() != null && startRouter.getType().equalsIgnoreCase("cisco")) {
			SSHClient ssh = new SSHClient(startRouter.getIp(), startRouter.getUsername(), startRouter.getPassword());
			ssh.createCiscoBgpGroup(startRouter, cProtocols.getBgp(), true, true);
			ssh.disconnectionSession();
		} else {
			ApplyPolicyRouterUtil.applyRouterPolicy(
					ApplicationRouterConfigurationUtil.findRouterByIpv4(vpnProvisionVO.getTargetLocalIP()), cimport,
					cexport);// CSR2
			System.out.println("_____new group for CSR2_____");
			//// cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
			juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge",
					startRouter.getIp() + "_" + startRouter.getUsername() + "_" + startRouter.getPassword(), true);
		}
		// juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge",
		// startRouter.getIp()+"_"+startRouter.getUsername()+"_"+startRouter.getPassword(),true);

		System.out.println("____new group for AG1");
		bgpGroup.setIimport(export);
		bgpGroup.setExport(iimport);
		bgpGroup.setLocaladdress(vpnProvisionVO.getLsourceRouter());//// AG1
		neighbor.setName(vpnProvisionVO.getSourceLocalIP());// CSR1

		// List<List<String>>
		// result=ecodeTopologyDeviceViewer.findAllPossibleLogicalPathSourceDestination(source,
		// dest, routers);
		String selectedLogicalPath = vpnProvisionVO.getSelectedLogicPath();
		String selectedLogicalPathTokens[] = selectedLogicalPath.split(",");

		// System.out.println("___result___ = "+result);
		List<String> dselectedLogicPath = Arrays.asList(selectedLogicalPathTokens);

		/// BE CAREFUL FOR ORDERING..
		List<String> selectedLogicPath = new ArrayList<>();
		for (int m = dselectedLogicPath.size() - 1; m >= 0; m--) {
			selectedLogicPath.add(dselectedLogicPath.get(m));
		}

		String calculatedImport = cimport;
		String calculatedExport = cexport;
		for (int p = selectedLogicPath.size() - 1; p >= 0; p--) {
			String currentHostNameIp = selectedLogicPath.get(p);

			ApplyPolicyRouterUtil.applyRouterPolicy(ApplicationRouterConfigurationUtil
					.findRouterByIpv4(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp)), cimport, cexport);

			if (p == selectedLogicPath.size() - 1) { // only first time
				Router lsourceRouter = ApplicationRouterConfigurationUtil
						.findRouterByIpv4(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
				if (lsourceRouter.getType() != null && lsourceRouter.getType().equalsIgnoreCase("cisco")) {
					SSHClient ssh = new SSHClient(lsourceRouter.getIp(), lsourceRouter.getUsername(),
							lsourceRouter.getPassword());
					ssh.createCiscoBgpGroup(lsourceRouter, cProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					// ApplyPolicyRouterUtil.applyRouterPolicy(ApplicationRouterConfigurationUtil.findRouterByIpv4(vpnProvisionVO.getTargetLocalIP()),cimport,cexport);//CSR2
					System.out.println("_____new group for CSR2_____");
					// cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", lsourceRouter.getIp() + "_"
							+ lsourceRouter.getUsername() + "_" + lsourceRouter.getPassword(), true);
				}
				// juniperBgpServiceImpl.provisionTraceOptions(cProtocols,
				// "merge",
				// lsourceRouter.getIp()+"_"+lsourceRouter.getUsername()+"_"+lsourceRouter.getPassword(),true);

			}
			System.out.println("___Policy is applied for host    = " + currentHostNameIp);

			if (p > 0) {
				String nextHostNameIp = selectedLogicPath.get(p - 1);
				String hostName = BgpGroupsDeviceUtil.getDeviceHostName(nextHostNameIp);
				Router nrouter = ApplicationRouterConfigurationUtil.findRouterByHostname(hostName);
				String chostName = BgpGroupsDeviceUtil.getDeviceHostName(currentHostNameIp);
				Router crouter = ApplicationRouterConfigurationUtil.findRouterByHostname(chostName);

				if (crouter.getType() != null && crouter.getType().equalsIgnoreCase("juniper")
						&& nrouter.getType() != null && nrouter.getType().equalsIgnoreCase("juniper")) {

					// BGP group list for current
					List<BgpGroupData> cbgpGroupList = BgpGroupsDeviceUtil
							.findDeviceBgpGroups(BgpGroupsDeviceUtil.getDeviceHostName(currentHostNameIp));
					System.out.println(currentHostNameIp + "  Groups= " + cbgpGroupList);

					// FIND BGP Group for nextRouter
					List<BgpGroupData> nextBgpGroupList = BgpGroupsDeviceUtil
							.findDeviceBgpGroups(BgpGroupsDeviceUtil.getDeviceHostName(nextHostNameIp));
					System.out.println(nextHostNameIp + " fetching  all f Groups from device= " + nextBgpGroupList);
					// finding common groups
					boolean b = nextBgpGroupList.retainAll(cbgpGroupList);
					System.out.println("___Common Groups are found___    = " + b);
					System.out.println("---Common groups _--- = " + nextBgpGroupList);
					System.out.println("Update group for  = " + nextBgpGroupList);

					List<String> ccexport = new ArrayList<>();
					ccexport.add(calculatedExport);
					List<String> ciimport = new ArrayList<>();
					ciimport.add(calculatedImport);
					CProtocols cProtocols1 = BgpGroupsDeviceUtil.createBgpGroup(nextBgpGroupList.get(0),
							BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp), ccexport, ciimport);
					cProtocols1.getBgp().getBgpGroup().get(0).getNeighbor().get(0)
							.setName(BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp));
					Router cRouter = ApplicationRouterConfigurationUtil
							.findRouterByIpv4(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols1, "merge",
							BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp) + "_" + cRouter.getUsername()
									+ "_" + cRouter.getPassword(),
							true);
					System.out.println("Update group for  = " + nextHostNameIp);
					List<String> nexport = new ArrayList<>();
					nexport.add(calculatedImport);
					List<String> niimport = new ArrayList<>();
					niimport.add(calculatedExport);
					CProtocols cProtocols2 = BgpGroupsDeviceUtil.createBgpGroup(nextBgpGroupList.get(0),
							BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp), nexport, niimport);
					cProtocols2.getBgp().getBgpGroup().get(0).getNeighbor().get(0)
							.setName(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
					Router nextRouter = ApplicationRouterConfigurationUtil
							.findRouterByIpv4(BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp));
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols2, "merge",
							BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp) + "_" + nextRouter.getUsername()
									+ "_" + nextRouter.getPassword(),
							true);
				} else {
					// TODO for CISCO ->>Other use cases......................

				}
				// String temp=calculatedImport;
				// calculatedImport=calculatedExport;
				// calculatedExport=temp;
			} else {
				/////////////////////////////////////////////
				System.out.println("________output_____________");
				//////////////////////////////////////////////
				// make a new group
				// for another host
				// for CSR2
				List<String> ce1export = new ArrayList<>();
				ce1export.add(cimport);
				List<String> ce1iimport = new ArrayList<>();
				ce1iimport.add(cexport);
				bgpGroup.setIimport(ce1iimport);
				bgpGroup.setExport(ce1export);
				bgpGroup.setLocaladdress(vpnProvisionVO.getTargetLocalIP()); // CSR2
				bgpGroup.getNeighbor().get(0).setName(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
				Router endRouter = ApplicationRouterConfigurationUtil
						.findRouterByIpv4(vpnProvisionVO.getTargetLocalIP());
				if (endRouter.getType() != null && endRouter.getType().equalsIgnoreCase("cisco")) {
					SSHClient ssh = new SSHClient(endRouter.getIp(), endRouter.getUsername(), endRouter.getPassword());
					ssh.createCiscoBgpGroup(endRouter, cProtocols.getBgp(), true, true);
					ssh.disconnectionSession();
				} else {
					ApplyPolicyRouterUtil.applyRouterPolicy(
							ApplicationRouterConfigurationUtil.findRouterByIpv4(vpnProvisionVO.getTargetLocalIP()),
							cimport, cexport);// CSR2
					System.out.println("_____new group for CSR2_____");
					cProtocols.getBgp().getBgpGroup().get(0).setType(vpnProvisionVO.getType());
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge",
							endRouter.getIp() + "_" + endRouter.getUsername() + "_" + endRouter.getPassword(), true);
				}
				///
				Router targetLogicalRouter = ApplicationRouterConfigurationUtil
						.findRouterByIpv4(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));

				System.out
						.println("Adding new Group for = " + BgpGroupsDeviceUtil.getDeviceHostName(currentHostNameIp));
				if (targetLogicalRouter.getType() != null
						&& targetLogicalRouter.getType().equalsIgnoreCase("juniper")) {
					List<String> pe1export = new ArrayList<>();
					pe1export.add(cexport);
					List<String> pe1iimport = new ArrayList<>();
					pe1iimport.add(cimport);
					bgpGroup.setIimport(pe1iimport);
					bgpGroup.setExport(pe1export);
					bgpGroup.setLocaladdress(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
					bgpGroup.getNeighbor().get(0).setName(vpnProvisionVO.getTargetLocalIP());// CSR2
					juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge",
							BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp) + "_"
									+ targetLogicalRouter.getUsername() + "_" + targetLogicalRouter.getPassword(),
							true);
				} else {
					// Code for CISCO TODO

				}

			}

		}

		System.out.println(
				"__@)@)@((@COMPLETED!!!!!!!!!!!!!END OF THE MAIN METHOD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

		return null;
	}
	
	public static void main(String[] args) {
		 org.springframework.context.ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ecode-service-context.xml");
		 EcodeDeviceEndToEndProvisionImpl deviceEndToEndProvisionImpl=(EcodeDeviceEndToEndProvisionImpl)applicationContext.getBean("EcodeDeviceEndToEndProvisionImpl");
		 deviceEndToEndProvisionImpl.testing(null);
	}

}
