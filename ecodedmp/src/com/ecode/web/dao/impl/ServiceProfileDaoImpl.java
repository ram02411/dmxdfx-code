package com.ecode.web.dao.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.ecode.web.dao.ServiceProfileDao;
import com.ecode.web.dao.entity.PutServiceProfileEntity;
import com.ecode.web.dao.entity.ServiceProfileEntity;
import com.ecode.web.dao.entity.VPNProvisionEntity;
import com.ecode.web.dao.query.ServiceProfileQuery;
import com.ecode.web.util.EcodeDateUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.servicenow.GetProfiles;
import com.servicenow.GetServices;
import com.servicenow.ServicenowResult;

@Repository("ServiceProfileDaoImpl")
@Scope("singleton")
public class ServiceProfileDaoImpl extends JdbcDaoSupport  implements ServiceProfileDao{

		private static final Log logger = LogFactory.getLog(ServiceProfileDaoImpl.class);
		
		@Autowired
		@Qualifier("ecodeDataSource")
		public void injectDataSource(DataSource dataSource){
			super.setDataSource(dataSource);
		}
		
		@Override
		public String addVpnProfile(VPNProvisionEntity vpnProvisionEntity){
			if(logger.isDebugEnabled())
				logger.debug("Executing addVpnProfile......");
			//serviceName,type,clientDetail,status,dop,dom,description
			//vpn_profile_tbl(groupName,exports,imports,sourceLocalIP,sourceNeighborIP,targetLocalIP,targetNeighborIP,type,authenticationKey,localas,peeras,serviceName,doe,dom) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			//Object data[]=new Object[]{vpnProvisionEntity.getGroupName(),vpnProvisionEntity.getExports(),vpnProvisionEntity.getImports(),vpnProvisionEntity.getSourceLocalIP(),vpnProvisionEntity.getSourceNeighborIP(),vpnProvisionEntity.getTargetLocalIP(),vpnProvisionEntity.getTargetNeighborIP(),vpnProvisionEntity.getType(),vpnProvisionEntity.getAuthenticationKey(),vpnProvisionEntity.getLocalas(),vpnProvisionEntity.getPeeras(),vpnProvisionEntity.getProfileName(),vpnProvisionEntity.getServiceName(),EcodeDateUtils.getCurrentTimestatmp(),EcodeDateUtils.getCurrentTimestatmp()};
			//INSERT_NEW_SERVICE_PROFILE
			//super.getJdbcTemplate().update(ServiceProfileQuery.INSERT_NEW_VPN_PROFILE,data);
			if(logger.isDebugEnabled())
				logger.debug("New Service Profile is created with = "+vpnProvisionEntity);
			return "Created";
		}
		
		
		@Override
		public String addServiceProfile(PutServiceProfileEntity putserviceProfileEntity){
			if(logger.isDebugEnabled())
				logger.debug("Executing addServiceProfile......");
			//serviceName,type,clientDetail,status,dop,dom,description
			//Object data[]=new Object[]{serviceProfileEntity.getServiceName(),serviceProfileEntity.getType(),"Ecode","NA",serviceProfileEntity.getDop(),serviceProfileEntity.getDom(),serviceProfileEntity.getDescription()};
			//INSERT_NEW_SERVICE_PROFILE
			//super.getJdbcTemplate().update(ServiceProfileQuery.INSERT_NEW_SERVICE_PROFILE,data);
			
			String insertservice=putserviceProfileEntity.toString();
			System.out.println("insert service :"+insertservice);
			if(logger.isDebugEnabled())
				logger.debug("New Service Profile is created with = "+putserviceProfileEntity);
			return "Created";
		}
		
		
		@Override
		public List<ServiceProfileEntity> findAllVpnProfileServices(){
			if(logger.isDebugEnabled())
				logger.debug("Executing findAllVpnProfileServices......................");
			
			List<ServiceProfileEntity> vpnStatusList=new ArrayList<>();
			try {
				String result=GetServices.getservices();
				result=result.substring(10, result.length()-1);
				Gson gson = new Gson();
				Type type = new TypeToken<List<ServiceProfileEntity>>(){}.getType();
				
				vpnStatusList=gson.fromJson(result, type);
					
			  for(ServiceProfileEntity spe:vpnStatusList){
				   try {
					   
					   List<VPNProvisionEntity> vpnProvisionList  =findVpnProfileByServiceName(spe.getServicename());
					   spe.setNop(vpnProvisionList!=null?vpnProvisionList.size():0);
				   }catch(Exception ex){
					   System.out.println(ex.getMessage());
				   }
			 }
			  
			if(logger.isDebugEnabled())
				logger.debug("  vpnStatusList..................... = "+vpnStatusList);
			}catch(Exception ex){
				if(logger.isErrorEnabled())
					logger.error("  vpnStatusList..................... = "+vpnStatusList);
				ex.printStackTrace();
			}
			return vpnStatusList;
		}

		@Override
		public List<VPNProvisionEntity> findVpnProfileByServiceName(String serviceName) {
			if(logger.isDebugEnabled())
				logger.debug("Executing findVpnProfileByServiceName......................"+serviceName);
			//SELECT_ALL_PROFILES_BY_SERVICE
			
			List<VPNProvisionEntity> serviceProfilesList=new ArrayList<>();
			List<VPNProvisionEntity> serviceProfiles=new ArrayList<>();
			try {
				
				String result=GetProfiles.getprofiles();
				result=result.substring(10, result.length()-1);
				Gson gson = new Gson();
				Type type = new TypeToken<List<VPNProvisionEntity>>(){}.getType();
				
				serviceProfilesList=gson.fromJson(result, type);
				for (VPNProvisionEntity vpnProvisionEntity : serviceProfilesList) {
					if(serviceName.equals(vpnProvisionEntity.getServicename())){
						serviceProfiles.add(vpnProvisionEntity);
					}
				}
				if(logger.isDebugEnabled())
				logger.debug("  serviceProfilesList..................... = "+serviceProfilesList);
			}catch(Exception ex){
				if(logger.isErrorEnabled())
					logger.error("  serviceProfilesList..................... = "+serviceProfilesList);
				    ex.printStackTrace();
			}
			return serviceProfiles;
		} 
		
		@Override
		public VPNProvisionEntity findVpnProfileByProfileName(String profileName) {
			if(logger.isDebugEnabled())
				logger.debug("Executing findVpnProfileByProfileName......................");
			List<VPNProvisionEntity> serviceProfilesList=new ArrayList<>();
			VPNProvisionEntity ProfileByName=new VPNProvisionEntity();
			try {
				
				String result=GetProfiles.getprofiles();
				result=result.substring(10, result.length()-1);
				Gson gson = new Gson();
				Type type = new TypeToken<List<VPNProvisionEntity>>(){}.getType();
				
				serviceProfilesList=gson.fromJson(result, type);
				for (VPNProvisionEntity vpnProvisionEntity : serviceProfilesList) {
					if(profileName.equals(vpnProvisionEntity.getProfilename())){
						BeanUtils.copyProperties(vpnProvisionEntity, ProfileByName);
					}
				}
				if(logger.isDebugEnabled())
				logger.debug("  ProfileByName..................... = "+ProfileByName);
			}catch(Exception ex){
				if(logger.isErrorEnabled())
					logger.error("  serviceProfilesList..................... = "+serviceProfilesList);
				    ex.printStackTrace();
			}
			return ProfileByName;
		} 


}
