package com.ecode.web.dao;

import java.util.List;

import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.web.dao.entity.PutServiceProfileEntity;
import com.ecode.web.dao.entity.ServiceProfileEntity;
import com.ecode.web.dao.entity.VPNProvisionEntity;

public interface ServiceProfileDao {

	public List<ServiceProfileEntity> findAllVpnProfileServices();
	public String addServiceProfile(PutServiceProfileEntity putserviceProfileEntity);
	public String addVpnProfile(VPNProvisionEntity vpnProvisionEntity);
	public List<VPNProvisionEntity> findVpnProfileByServiceName(String serviceName);
	public VPNProvisionEntity findVpnProfileByProfileName(String profileName);
}
