package com.ecode.web.dao.entity;

import java.sql.Timestamp;

public class ServiceProfileEntity {
	
	private String sys_id;
	private String sys_updated_by;
	private String sys_created_on;
	private String clientdetail;
	private String sys_mod_count;
	private String description;
	private String servicename;
	private String sys_updated_on;
	private String sys_tags;
	private String type;
	private String sys_created_by;
	private String status;
	private int nop;
	public String getSys_id() {
		return sys_id;
	}
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getSys_updated_by() {
		return sys_updated_by;
	}
	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}
	public String getSys_created_on() {
		return sys_created_on;
	}
	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}
	public String getClientdetail() {
		return clientdetail;
	}
	public void setClientdetail(String clientdetail) {
		this.clientdetail = clientdetail;
	}
	public String getSys_mod_count() {
		return sys_mod_count;
	}
	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getServicename() {
		return servicename;
	}
	public void setServicename(String servicename) {
		this.servicename = servicename;
	}
	public String getSys_updated_on() {
		return sys_updated_on;
	}
	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}
	public String getSys_tags() {
		return sys_tags;
	}
	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSys_created_by() {
		return sys_created_by;
	}
	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getNop() {
		return nop;
	}
	public void setNop(int nop) {
		this.nop = nop;
	}
	@Override
	public String toString() {
		return "ServiceProfileEntity [sys_id=" + sys_id + ", sys_updated_by=" + sys_updated_by + ", sys_created_on="
				+ sys_created_on + ", clientdetail=" + clientdetail + ", sys_mod_count=" + sys_mod_count
				+ ", description=" + description + ", servicename=" + servicename + ", sys_updated_on=" + sys_updated_on
				+ ", sys_tags=" + sys_tags + ", type=" + type + ", sys_created_by=" + sys_created_by + ", status="
				+ status + ", nop=" + nop + "]";
	}
	
}
