package com.ecode.web.dao.entity;

public class VPNProvisionEntity {
	private String sourcelocalip;
	private String targetneighborip;
	private String imports;
	private String exports;	
	private String targetlocalip;
	private String sys_mod_count;
	private String peeras;
	private String authenticationkey;
	private String sys_updated_on;
	private String sys_tags;
	private String type;
	private String groupname;
	private String sys_id;
	private String sys_updated_by;
	private String localas;
	private String sys_created_on;
	private String profilename;
	private String servicename;
	private String sourceneighborip;
	private String sys_created_by;
	public String getSourcelocalip() {
		return sourcelocalip;
	}
	public void setSourcelocalip(String sourcelocalip) {
		this.sourcelocalip = sourcelocalip;
	}
	public String getTargetneighborip() {
		return targetneighborip;
	}
	public void setTargetneighborip(String targetneighborip) {
		this.targetneighborip = targetneighborip;
	}
	public String getImports() {
		return imports;
	}
	public void setImports(String imports) {
		this.imports = imports;
	}
	public String getExports() {
		return exports;
	}
	public void setExports(String exports) {
		this.exports = exports;
	}
	public String getTargetlocalip() {
		return targetlocalip;
	}
	public void setTargetlocalip(String targetlocalip) {
		this.targetlocalip = targetlocalip;
	}
	public String getSys_mod_count() {
		return sys_mod_count;
	}
	public void setSys_mod_count(String sys_mod_count) {
		this.sys_mod_count = sys_mod_count;
	}
	public String getPeeras() {
		return peeras;
	}
	public void setPeeras(String peeras) {
		this.peeras = peeras;
	}
	public String getAuthenticationkey() {
		return authenticationkey;
	}
	public void setAuthenticationkey(String authenticationkey) {
		this.authenticationkey = authenticationkey;
	}
	public String getSys_updated_on() {
		return sys_updated_on;
	}
	public void setSys_updated_on(String sys_updated_on) {
		this.sys_updated_on = sys_updated_on;
	}
	public String getSys_tags() {
		return sys_tags;
	}
	public void setSys_tags(String sys_tags) {
		this.sys_tags = sys_tags;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getSys_id() {
		return sys_id;
	}
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getSys_updated_by() {
		return sys_updated_by;
	}
	public void setSys_updated_by(String sys_updated_by) {
		this.sys_updated_by = sys_updated_by;
	}
	public String getLocalas() {
		return localas;
	}
	public void setLocalas(String localas) {
		this.localas = localas;
	}
	public String getSys_created_on() {
		return sys_created_on;
	}
	public void setSys_created_on(String sys_created_on) {
		this.sys_created_on = sys_created_on;
	}
	public String getProfilename() {
		return profilename;
	}
	public void setProfilename(String profilename) {
		this.profilename = profilename;
	}
	public String getServicename() {
		return servicename;
	}
	public void setServicename(String servicename) {
		this.servicename = servicename;
	}
	public String getSourceneighborip() {
		return sourceneighborip;
	}
	public void setSourceneighborip(String sourceneighborip) {
		this.sourceneighborip = sourceneighborip;
	}
	public String getSys_created_by() {
		return sys_created_by;
	}
	public void setSys_created_by(String sys_created_by) {
		this.sys_created_by = sys_created_by;
	}
	@Override
	public String toString() {
		return "VPNProvisionEntity [sourcelocalip=" + sourcelocalip + ", targetneighborip=" + targetneighborip
				+ ", imports=" + imports + ", exports=" + exports + ", targetlocalip=" + targetlocalip
				+ ", sys_mod_count=" + sys_mod_count + ", peeras=" + peeras + ", authenticationkey=" + authenticationkey
				+ ", sys_updated_on=" + sys_updated_on + ", sys_tags=" + sys_tags + ", type=" + type + ", groupname="
				+ groupname + ", sys_id=" + sys_id + ", sys_updated_by=" + sys_updated_by + ", localas=" + localas
				+ ", sys_created_on=" + sys_created_on + ", profilename=" + profilename + ", servicename=" + servicename
				+ ", sourceneighborip=" + sourceneighborip + ", sys_created_by=" + sys_created_by + "]";
	}
	
}
