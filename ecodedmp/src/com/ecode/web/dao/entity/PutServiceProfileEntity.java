package com.ecode.web.dao.entity;

public class PutServiceProfileEntity {
	private String clientdetail;
	private String servicename;
	private String type;
	private String status;
	private String description;
	public String getClientdetail() {
		return clientdetail;
	}
	public void setClientdetail(String clientdetail) {
		this.clientdetail = clientdetail;
	}
	public String getServicename() {
		return servicename;
	}
	public void setServicename(String servicename) {
		this.servicename = servicename;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "PutServiceProfileEntity [clientdetail=" + clientdetail + ", servicename=" + servicename + ", type="
				+ type + ", status=" + status + ", description=" + description + "]";
	}

}
