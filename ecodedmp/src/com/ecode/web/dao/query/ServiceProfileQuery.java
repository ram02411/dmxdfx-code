package com.ecode.web.dao.query;

public interface ServiceProfileQuery {
	public static final String FIND_ALL_SERVICE_PROFILES="select serviceName,type,clientDetail,status,dop,dom,description from service_profiles_tbl"; 
	public static final String INSERT_NEW_SERVICE_PROFILE="insert into  service_profiles_tbl(serviceName,type,clientDetail,status,dop,dom,description) values(?,?,?,?,?,?,?)";
	public static final String INSERT_NEW_VPN_PROFILE="insert into  vpn_profile_tbl(groupName,exports,imports,sourceLocalIP,sourceNeighborIP,targetLocalIP,targetNeighborIP,type,authenticationKey,localas,peeras,profileName,serviceName,doe,dom) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	public static final String SELECT_ALL_PROFILES_BY_SERVICE="select *  from vpn_profile_tbl where serviceName=?";
	public static final String SELECT_PROFILE_BY_PRFILE_NAME="select *  from vpn_profile_tbl where profileName=?";
}
