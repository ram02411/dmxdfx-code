package com.ecode.web.service;


import java.util.List;

import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.topology.viewer.data.device.model.VPNProvisionVO;
import com.juniper.container.data.controller.model.PutServiceProfileVO;
import com.juniper.container.data.controller.model.ServiceProfileVO;

public interface ServiceProfileService {

	public List<ServiceProfileVO> findAllVpnProfileServices();
	public	String addServiceProfile(PutServiceProfileVO serviceProfileVO);
	public	String addVpnProfile(VPNProvisionVO vpnProvisionVO);
	public List<ServicenowProfileVO> findVpnProfileByServiceName(String serviceName);
	public ServicenowProfileVO findVpnProfileByProfileName(String profileName);

}

