package com.ecode.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.topology.viewer.data.device.model.VPNProvisionVO;
import com.ecode.web.dao.ServiceProfileDao;
import com.ecode.web.dao.entity.PutServiceProfileEntity;
import com.ecode.web.dao.entity.ServiceProfileEntity;
import com.ecode.web.dao.entity.VPNProvisionEntity;
import com.ecode.web.service.ServiceProfileService;
import com.juniper.container.data.controller.model.PutServiceProfileVO;
import com.juniper.container.data.controller.model.ServiceProfileVO;

@Service("ServiceProfileServiceImpl")
@Scope("singleton")
public class ServiceProfileServiceImpl implements ServiceProfileService{
	
	private static final Log logger = LogFactory.getLog(ServiceProfileServiceImpl.class);
	
	@Autowired
	@Qualifier("ServiceProfileDaoImpl")
	private ServiceProfileDao serviceProfileDao;
	
	@Override
	public String addServiceProfile(PutServiceProfileVO putserviceProfileVO){
		PutServiceProfileEntity putserviceProfileEntity=new PutServiceProfileEntity();
		BeanUtils.copyProperties(putserviceProfileVO, putserviceProfileEntity);
		return serviceProfileDao.addServiceProfile(putserviceProfileEntity);
	}

	@Override
	public List<ServiceProfileVO> findAllVpnProfileServices() {
		List<ServiceProfileEntity> serviceProfileEntityList=serviceProfileDao.findAllVpnProfileServices();
		List<ServiceProfileVO> serviceProfileList=new ArrayList<>();
		for(ServiceProfileEntity serviceProfileEntity:serviceProfileEntityList) {
				ServiceProfileVO serviceProfileVO=new ServiceProfileVO();
				BeanUtils.copyProperties(serviceProfileEntity,serviceProfileVO);
				serviceProfileList.add(serviceProfileVO);
		}
		return serviceProfileList;
	}
	
	@Override
	public List<ServicenowProfileVO> findVpnProfileByServiceName(String serviceName) {
		List<VPNProvisionEntity> provisionEntityList=serviceProfileDao.findVpnProfileByServiceName(serviceName);
		List<ServicenowProfileVO> vpnProvisionVOList=new ArrayList<>();
		for(VPNProvisionEntity serviceProfileEntity:provisionEntityList) {
			ServicenowProfileVO serviceProfileVO=new ServicenowProfileVO();
				BeanUtils.copyProperties(serviceProfileEntity,serviceProfileVO);
				vpnProvisionVOList.add(serviceProfileVO);
		}
		return vpnProvisionVOList;
	}
	
	@Override
	public ServicenowProfileVO findVpnProfileByProfileName(String profileName){
		VPNProvisionEntity vpnProvisionEntity=serviceProfileDao.findVpnProfileByProfileName(profileName);
		ServicenowProfileVO vpnProvisionVO=new ServicenowProfileVO();
		BeanUtils.copyProperties(vpnProvisionEntity,vpnProvisionVO);
		return vpnProvisionVO;
	}

	@Override
	public String addVpnProfile(VPNProvisionVO vpnProvisionVO) {
		VPNProvisionEntity vpnProvisionEntity=new VPNProvisionEntity();
		BeanUtils.copyProperties(vpnProvisionVO, vpnProvisionEntity);
		return serviceProfileDao.addVpnProfile(vpnProvisionEntity);
	}

}
