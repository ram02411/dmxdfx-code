package com.ecode.web.app.constant;

/**
 * 
 * @author 305
 *
 */
public class ApplicationConstant {
	public static String LIVE_DEVICE="livedevice";
	public static String XML_FILE_DEVICE="filesystem";
	public static String FAILED_STATUS="failed";
	public static String SUCCESS_STATUS="success";
	public static String TWO_HUNDRED="200";
	public static String FIVE_HUNDRED="500";
	public static String MERGE_LOAD_TYPE="merge";
	public static String REPLACE_LOAD_TYPE="replace";
	public static String APPLICATION_BASE_PATH="http://86.22.43.201:8080/ecodedmp";
	public static String JUNIPER="juniper";
	public static String CISCO="cisco";
	
	
}
