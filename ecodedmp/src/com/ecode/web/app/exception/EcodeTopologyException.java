package com.ecode.web.app.exception;

/**
 * 
 * @author ecode
 *
 */
public class EcodeTopologyException extends RuntimeException {
	

	public EcodeTopologyException() {
		super();
	}

	public EcodeTopologyException(String message) {
		super(message);
	}

	public EcodeTopologyException(String message, Throwable cause) {
		super(message, cause);
	}

}
