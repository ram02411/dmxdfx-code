package com.ecode.web.app.email.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

@Service("EmailMessageSenderService")
public class EmailMessageSenderService {


	@Autowired
	@Qualifier("mailSender")
	private MailSender mailSender;
	
	public void sendMail(String from, String to, String subject, String body) {
		    SimpleMailMessage message = new SimpleMailMessage();
	        message.setFrom(from);
	        message.setTo(to);
	        message.setSubject(subject);
	        message.setText(body);
	        mailSender.send(message);
	}
	
	public static void main(String[] args) {
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("email-service.xml");
		EmailMessageSenderService emailMessageSenderService=(EmailMessageSenderService)applicationContext.getBean("EmailMessageSenderService");
		emailMessageSenderService.sendMail("hietams@gmail.com","nagendra.yadav.niit@gmail.com", "Hello Email Testing","How are you doing today?");
	}

}

