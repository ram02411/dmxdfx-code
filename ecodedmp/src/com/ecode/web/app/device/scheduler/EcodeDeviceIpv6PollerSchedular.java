package com.ecode.web.app.device.scheduler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;

@Service("EcodeDeviceIpv6PollerSchedular")
public class EcodeDeviceIpv6PollerSchedular {
	private static final Log logger = LogFactory.getLog(EcodeDeviceIpv6PollerSchedular.class);
	 /**
	   * This method sends alert to the user
	   * about VPN status is UP and DOWN 
	   * as per the rules.
	   *  
	   */
	      //@Scheduled(fixedDelay = 600000, initialDelay = 60000)
	     //@Scheduled(fixedRate  = 60000, initialDelay = 1000)
	    // @Scheduled(cron="*/100 * * * * ?")
	  public void ecodel3TopologyDbPoller() {
	    	 long startTime=System.currentTimeMillis();
	    	 if(logger.isDebugEnabled()){
	    	 logger.debug("########## �xecuting ecodeTopologyDbPoller and loading  latest IPV6 addresses ########## ");
	    	 }
	    	ApplicationRouterConfigurationUtil.updateIPV6AddressesNetworkDevice();
	    	 if(logger.isDebugEnabled()){
		    	 logger.debug("########## Loading of latest IPV6 addresses are finished##############");
		    	 }
	    	 long endTime=System.currentTimeMillis();
	     System.out.println("time :--"+(endTime-startTime));
	     }
	     
}
