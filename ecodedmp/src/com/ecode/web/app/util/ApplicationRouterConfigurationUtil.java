package com.ecode.web.app.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.parser.IPV6AddressXmlParser;
import com.ecode.web.app.constant.ApplicationConstant;
import com.hiet.excelReader.InventoryDb;

/**
 * 
 * @author java
 *
 */
public class ApplicationRouterConfigurationUtil {

	private static List<Router> routerList = new ArrayList<>();
	private static List<Router> routerldb = new ArrayList<>();
	private static Map<String, Router> routerDetailIP6 = new HashMap<>();
	private static Map<String, Router> routerIPV4Map = new HashMap<>();
	private static Map<String, String> ipsHostNames = new TreeMap<>();
	private static Map<String, Router> hostnameRouterMap = new HashMap<>();
	public static void load() {
	}

	public static void addRouter() {
		
		routerList = new ArrayList<>();
		routerldb=InventoryDb.findAllRouters();
		for(Router router1:routerldb){
			routerDetailIP6.put(router1.getIp(), router1);
			ipsHostNames.put(router1.getIp(), router1.getHostname());
			hostnameRouterMap.put(router1.getHostname(), router1);
			routerIPV4Map.put(router1.getIp(), router1);
			routerList.add(router1);
		}
		
	}

	static {
		// fe80::a00:27ff:fe72:2d82
		/* routerList = new ArrayList<>();
		Router router1 = new Router();
		router1.setType(ApplicationConstant.JUNIPER);
		router1.setHostname("CSR1");
		router1.setIp("192.168.100.10");
		router1.setUsername("root");
		router1.setPassword("ecode123");
		routerDetailIP6.put(router1.getIp(), router1);
		ipsHostNames.put(router1.getIp(), router1.getHostname());
		hostnameRouterMap.put(router1.getHostname(), router1);
		routerIPV4Map.put(router1.getIp(), router1);

		Router router2 = new Router();
		router2.setType(ApplicationConstant.JUNIPER);
		router2.setHostname("CSR2");
		router2.setIp("192.168.100.20");
		router2.setUsername("root");
		router2.setPassword("ecode123");
		// routerDetailIP6.put("fe80::a00:27ff:feef:762", router2);
		// routerDetailIP6.put("fe80::a00:27ff:fe63:c8ea", router2);
		routerDetailIP6.put(router2.getIp(), router2);
		ipsHostNames.put(router2.getIp(), router2.getHostname());
		hostnameRouterMap.put(router2.getHostname(), router2);
		routerIPV4Map.put(router2.getIp(), router2);

		Router router3 = new Router();
		router3.setType(ApplicationConstant.JUNIPER);
		router3.setHostname("CSR3");
		router3.setIp("192.168.100.30");
		router3.setUsername("root");
		router3.setPassword("ecode123");
		routerDetailIP6.put(router3.getIp(), router3);
		ipsHostNames.put(router3.getIp(), router3.getHostname());
		hostnameRouterMap.put(router3.getHostname(), router3);
		routerIPV4Map.put(router3.getIp(), router3);
		
		Router router4 = new Router();
		router4.setType(ApplicationConstant.JUNIPER);
		router4.setHostname("CSR4");
		router4.setIp("192.168.100.40");
		router4.setUsername("root");
		router4.setPassword("ecode123");
		routerDetailIP6.put(router4.getIp(), router4);
		ipsHostNames.put(router4.getIp(), router4.getHostname());
		hostnameRouterMap.put(router4.getHostname(), router4);
		routerIPV4Map.put(router4.getIp(), router4);
		
		Router router5 = new Router();
		router5.setType(ApplicationConstant.JUNIPER);
		router5.setHostname("CSR5");
		router5.setIp("192.168.100.50");
		router5.setUsername("root");
		router5.setPassword("ecode123");
		routerDetailIP6.put(router5.getIp(), router5);
		ipsHostNames.put(router5.getIp(), router5.getHostname());
		hostnameRouterMap.put(router5.getHostname(), router5);
		routerIPV4Map.put(router5.getIp(), router5);
		
		Router router6 = new Router();
		router6.setType(ApplicationConstant.JUNIPER);
		router6.setHostname("CSR6");
		router6.setIp("192.168.100.60");
		router6.setUsername("root");
		router6.setPassword("ecode123");
		routerDetailIP6.put(router6.getIp(), router6);
		ipsHostNames.put(router6.getIp(), router6.getHostname());
		hostnameRouterMap.put(router6.getHostname(), router6);
		routerIPV4Map.put(router6.getIp(), router6);
		
		Router router7 = new Router();
		router7.setType(ApplicationConstant.JUNIPER);
		router7.setHostname("CSR7");
		router7.setIp("192.168.100.70");
		router7.setUsername("root");
		router7.setPassword("ecode123");
		routerDetailIP6.put(router7.getIp(), router7);
		ipsHostNames.put(router7.getIp(), router7.getHostname());
		hostnameRouterMap.put(router7.getHostname(), router7);
		routerIPV4Map.put(router7.getIp(), router7);

		Router router8 = new Router();
		router8.setType(ApplicationConstant.JUNIPER);
		router8.setHostname("CSR8");
		router8.setIp("192.168.100.80");
		router8.setUsername("root");
		router8.setPassword("ecode123");
		routerDetailIP6.put(router8.getIp(), router8);
		ipsHostNames.put(router8.getIp(), router8.getHostname());
		hostnameRouterMap.put(router8.getHostname(), router8);
		routerIPV4Map.put(router8.getIp(), router8);
		
		
		routerList.add(router1);
		routerList.add(router2);
		routerList.add(router3);
		routerList.add(router4);*/
		//routerList.add(router5);
		//routerList.add(router6);
		//routerList.add(router7);
		//routerList.add(router8);
		
		routerList = new ArrayList<>();
		addRouter();
		// System.out.println("fetching Ipv6");
		fetchIpv6AddressRouters(routerList);
	}

	public static void updateIPV6AddressesNetworkDevice(){
		System.out.println("Excuting the method updateIPV6AddressesNetworkDevice ="+new Date());
		System.out.println("Current status of routerDetailIPV6 is ="+routerDetailIP6);
		System.out.println("Deleting All routerDetailIP6  --- ");
		routerDetailIP6.clear();
		routerDetailIP6.putAll(routerIPV4Map);
		System.out.println("Excuting fetchIpv6AddressRouters --- ");		
		fetchIpv6AddressRouters(routerList);
		System.out.println("New status of routerDetailIPV6 is ="+routerDetailIP6);
	}
	
	
	public static void fetchIpv6AddressRouters(List<Router> routerList) {
		for (Router router : routerList) {
			//System.out.println("_+++++++_Getting network information of  +++++++ " + router.getHostname() + " - "
				//	+ router.getIp());
			List<String> ipv6AddressList = null;
			try {
				if (router.getType() != null && router.getType().equals("cisco")) {
					ipv6AddressList = IPV6AddressXmlParser.getCiscoIpv6List(router);

				} else {
					ipv6AddressList = IPV6AddressXmlParser.getNetworkInfo(router);
				}

				for (String ipv6 : ipv6AddressList) {
					if (ipv6 != null)
						routerDetailIP6.put(ipv6.toUpperCase(), router);
				}
			} catch (Exception exe) {
				//System.out.println("fetchIpv6AddressRouters exception  executor");
				exe.printStackTrace();
			}

		}
		//System.out.println("........................................");
		/*for (Entry<String, Router> mrouter : routerDetailIP6.entrySet()) {
			System.out.println("IPV6 IP = " + mrouter.getKey() + " Value= " + mrouter.getValue());
		}*/
	}

	public static Router findRouterByHostname(String hostName) {
		return hostnameRouterMap.get(hostName);
	}

	public static Map<String, String> findHostIpNameMap() {
		return ipsHostNames;
	}

	public static Map<String, Router> mapIv6toRouters() {
		return routerDetailIP6;
	}

	public static List<Router> loadAllAppRouters() {
		return routerList;
	}

	public static Router findSourceRouter() {
		return routerList.get(0);
	}

	public static Router findDestinationRouter() {
		return routerList.get(4);
	}

	public static Router findRouterByIpv4(String ipv4) {
		return routerDetailIP6.get(ipv4);
	}

}
