/*package com.ecode.web.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.ecode.topology.viewer.data.device.Router;
import com.ecode.web.app.constant.ApplicationConstant;
import com.hiet.excelReader.routerDetalisFromExcel;

public class ApplicationRouterConfigurationUtil2 {
	
	private static List<Router> routerList=new ArrayList<>();
	private static Map<String,Router> routerDetailIP6=new HashMap<>();
	private static Map<String,String> ipsHostNames=new TreeMap<>();
	private static Map<String,Router> hostnameRouterMap=new HashMap<>();
	private static ArrayList routerdetails=routerDetalisFromExcel.getRouterdetails();
	
	
	public static void addRouter(Router router,List<String> ipv6List){
		routerList.add(router);
		for(String ipv6:ipv6List){
			routerDetailIP6.put(ipv6, router);
		}
		routerDetailIP6.put(router.getIp(), router);
		ipsHostNames.put(router.getIp(), router.getHostname());
		hostnameRouterMap.put(router.getHostname(), router);
	}
	
	static{
		//fe80::a00:27ff:fe72:2d82
		routerList=new ArrayList<>();
		Router router1=new Router();
		router1.setType(ApplicationConstant.JUNIPER);
		router1.setHostname("TTFWCluster02");
		router1.setIp("192.168.201.253");
		router1.setUsername("root");
		router1.setPassword("T34ch3r!");
		router1.setIpv6("fe80::a00:27ff:fe63:c8ea");
		//routerDetailIP6.put("fe80::a00:27ff:feef:762", router1);
		//routerDetailIP6.put("fe80::a00:27ff:fe63:c8ea", router1);
		
		routerDetailIP6.put("192.168.201.253", router1);
		ipsHostNames.put("192.168.201.253", "TTFWCluster02");
		hostnameRouterMap.put("TTFWCluster02", router1);
		
		Router router2=new Router();
		router2.setType(ApplicationConstant.JUNIPER);
		router2.setHostname("CSR2");
		router2.setIp("192.168.1.45");
		router2.setUsername("root");
		router2.setPassword("ecode123");
		router2.setIpv6("fe80::a00:27ff:fe91:684e");
		routerDetailIP6.put("fe80::a00:27ff:fe92:9f2d", router2);
		routerDetailIP6.put("fe80::a00:27ff:fe91:684e", router2);
		routerDetailIP6.put("fe80::a00:27ff:fe56:96ed", router2);
		routerDetailIP6.put("192.168.1.45", router2);
		ipsHostNames.put("192.168.1.45", "CSR2");
		hostnameRouterMap.put("CSR2", router2);
		
		Router router3=new Router();
		router3.setType(ApplicationConstant.JUNIPER);
		router3.setHostname("AG1");
		router3.setIp("192.168.1.44");
		router3.setUsername("root");
		router3.setPassword("ecode123");
		router3.setIpv6("fe80::a00:27ff:fe0c:89bb");
		routerDetailIP6.put("fe80::a00:27ff:fe0c:89bb",router3);//
		routerDetailIP6.put("fe80::a00:27ff:fe7b:54d4",router3);
		routerDetailIP6.put("fe80::a00:27ff:fefc:ef3d",router3);
		routerDetailIP6.put(" fe80::a00:27ff:fef2:557f",router3);
		routerDetailIP6.put("192.168.1.44", router3);
		ipsHostNames.put("192.168.1.44", "AG1");
		hostnameRouterMap.put("AG1", router3);
		
		
		Router router4=new Router();
		router4.setType(ApplicationConstant.JUNIPER);
		router4.setHostname("AG3");
		router4.setIp("192.168.1.43");
		router4.setUsername("root");
		router4.setPassword("ecode123");
		router4.setIpv6("fe80::a00:27ff:fe80:9a10");
		
		routerDetailIP6.put("fe80::a00:27ff:fe80:9a10",router4);
		routerDetailIP6.put("fe80::a00:27ff:fe14:628d",router4);
		routerDetailIP6.put("fe80::a00:27ff:fe97:95ea",router4);
		
		
		routerDetailIP6.put("192.168.1.43", router4);
		ipsHostNames.put("192.168.1.43", "AG3");
		hostnameRouterMap.put("AG3", router4);
		
		Router router5=new Router();
		router5.setType(ApplicationConstant.JUNIPER);
		router5.setHostname("AG1.1");
		router5.setIp("192.168.1.46");
		router5.setUsername("root");
		router5.setPassword("ecode123");
		router5.setIpv6("fe80::a00:27ff:fe72:2d82");
		routerDetailIP6.put("fe80::a00:27ff:fe72:2d82",router5);
		routerDetailIP6.put("fe80::a00:27ff:fe7b:54d4",router5);
		routerDetailIP6.put("fe80::a00:27ff:fef2:557f",router5);
		routerDetailIP6.put("192.168.1.46", router5);
		ipsHostNames.put("192.168.1.46", "AG1.1");
		hostnameRouterMap.put("AG1.1", router5);
		
		Router router6=new Router();
		router6.setType(ApplicationConstant.JUNIPER);
		router6.setHostname("AG3.1");
		router6.setIp("192.168.1.42");
		router6.setUsername("root");
		router6.setPassword("ecode123");
		router6.setIpv6("fe80::a00:27ff:fe61:6947");
		routerDetailIP6.put("fe80::a00:27ff:fe61:6947",router6);
		routerDetailIP6.put("fe80::a00:27ff:fe3a:e4ef",router6);//fe80::a00:27ff:fe8b:ca34
		routerDetailIP6.put("fe80::a00:27ff:fe19:3114",router6);
		routerDetailIP6.put("fe80::a00:27ff:fe8b:ca34",router6);
		routerDetailIP6.put("192.168.1.42", router6);
		ipsHostNames.put("192.168.1.42", "AG3.1");
		hostnameRouterMap.put("AG3.1", router6);
		
		
		Router router7=new Router();
		router7.setType(ApplicationConstant.CISCO);
		router7.setHostname("192.168.1.70");
		router7.setIp("192.168.1.70");
		router7.setUsername("cisco");
		router7.setPassword("cisco");
		router7.setIpv6("fe80::a00:27ff:fe9d:f4a3");
		routerDetailIP6.put("fe80::a00:27ff:fe9d:f4a3",router7);
		routerDetailIP6.put("192.168.1.70", router7);
		ipsHostNames.put("192.168.1.70", "SRV5");
		hostnameRouterMap.put("SRV5", router7);
		
		//routerList.add(router3);
		//routerList.add(router2);
		routerList.add(router1);
		//routerList.add(router5);
		//routerList.add(router4);
       // routerList.add(router6);
      //  routerList.add(router7);

		
	}
	
	 public static Router findRouterByHostname(String hostName){
			return hostnameRouterMap.get(hostName);
			
		}	
	
	 public static Map<String,String> findHostIpNameMap(){
		return ipsHostNames;
		
	}	
	
	public static Map<String,Router> mapIv6toRouters(){
		
		return routerDetailIP6;
		
	}
	public static List<Router> loadAllAppRouters(){
		return routerList;
		
	}
	
	public static Router findSourceRouter(){
		return routerList.get(0);
	}

	
	public static Router findDestinationRouter(){
		return routerList.get(4);
	}
	
	public static Router findRouterByIpv4(String ipv4){
		return routerDetailIP6.get(ipv4);
	}
}
*/