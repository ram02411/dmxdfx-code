package com.ecode.web.app.util;

/**
 * 
 * @author 305
 *
 */
public class ApplicationRpcXmlGeneratorUtil {
	
		 public static String makeRequestInRpcFormatForUpdate(String target, String configuration, 
		            String loadType){
			  
		        configuration = configuration.trim();
		        if (!configuration.startsWith("<configuration")) {
		            configuration = "<configuration>" + configuration 
		                    + "</configuration>";
		        }
		        StringBuffer rpc = new StringBuffer("");
		        rpc.append("<rpc>");
		        rpc.append("<edit-config>");
		        rpc.append("<target>");
		        rpc.append("<" + target + "/>");
		        rpc.append("</target>");
		        rpc.append("<default-operation>");
		        rpc.append(loadType);
		        rpc.append("</default-operation>");
		        rpc.append("<config>");
		        rpc.append(configuration);
		        rpc.append("</config>");
		        rpc.append("</edit-config>");
		        rpc.append("</rpc>");
		        rpc.append("]]>]]>");
		        return rpc.toString();
	}

}
