package com.ecode.web.app.util;

import java.io.IOException;

import org.xml.sax.SAXException;

import ch.ethz.ssh2.channel.ChannelClosedException;
import net.juniper.netconf.Device;
import net.juniper.netconf.XML;

/**
 * 
 * @author ecode
 *
 */
public class EcodeCommonUtils {
	
	public static boolean isNetConfEnabled(Device device) throws SAXException, IOException{
			String str="<get-interface-information>"+ "<terse/>"+ "</get-interface-information>";
			
	        try {
	        	device.executeRPC(str);
	        }catch(ChannelClosedException cle){
	        	System.out.println("Sorry ! ,netconf is not enabled or not supported on this device");
	        	return false;
	        }
	        
	        return true;
	}

}
