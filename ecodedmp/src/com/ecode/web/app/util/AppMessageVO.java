package com.ecode.web.app.util;

/**
 * 
 * @author 305
 *
 */
public class AppMessageVO {
	
	private String scode;
	private String message;
	private String status;
	
	public String getScode() {
		return scode;
	}

	public void setScode(String scode) {
		this.scode = scode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "AppMessageVO [scode=" + scode + ", message=" + message + ", status=" + status + "]";
	}

}
