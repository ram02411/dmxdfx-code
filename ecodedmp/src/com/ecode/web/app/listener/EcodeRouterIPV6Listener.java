package com.ecode.web.app.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;

public class EcodeRouterIPV6Listener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		System.out.println("Loading IPV6 addresses for all the routers............");
		ApplicationRouterConfigurationUtil.load();
	}

}
