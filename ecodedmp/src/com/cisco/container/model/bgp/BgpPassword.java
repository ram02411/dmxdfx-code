package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class BgpPassword {

	private String passwordDisable;
	private String password;

	@XmlElement(name="password-disable",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getPasswordDisable() {
		return passwordDisable;
	}

	public void setPasswordDisable(String passwordDisable) {
		this.passwordDisable = passwordDisable;
	}

	@XmlElement(name="password",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "BgpPassword [passwordDisable=" + passwordDisable
				+ ", password=" + password + "]";
	}

}
