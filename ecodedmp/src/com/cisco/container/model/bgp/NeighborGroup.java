package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

public class NeighborGroup {

	private String neighborGroupName;
	private String create;
	private BgpRemoteAs biBgpRemoteAs;
	private BgpPassword bgpPassword;
	private String updateSourceInterface;
	private String neighborGracefulRestart;
	private NeighborGroupAfs neighborGroupAfs;

	@XmlElement(name="neighbor-group-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getNeighborGroupName() {
		return neighborGroupName;
	}

	public void setNeighborGroupName(String neighborGroupName) {
		this.neighborGroupName = neighborGroupName;
	}

	@XmlElement(name="create",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getCreate() {
		return create;
	}

	public void setCreate(String create) {
		this.create = create;
	}

	@XmlElement(name="remote-as",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public BgpRemoteAs getBiBgpRemoteAs() {
		return biBgpRemoteAs;
	}

	public void setBiBgpRemoteAs(BgpRemoteAs biBgpRemoteAs) {
		this.biBgpRemoteAs = biBgpRemoteAs;
	}

	@XmlElement(name="password",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public BgpPassword getBgpPassword() {
		return bgpPassword;
	}

	public void setBgpPassword(BgpPassword bgpPassword) {
		this.bgpPassword = bgpPassword;
	}

	@XmlElement(name="update-source-interface",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getUpdateSourceInterface() {
		return updateSourceInterface;
	}

	public void setUpdateSourceInterface(String updateSourceInterface) {
		this.updateSourceInterface = updateSourceInterface;
	}

	@XmlElement(name="neighbor-graceful-restart",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getNeighborGracefulRestart() {
		return neighborGracefulRestart;
	}

	public void setNeighborGracefulRestart(String neighborGracefulRestart) {
		this.neighborGracefulRestart = neighborGracefulRestart;
	}

	@XmlElement(name="neighbor-group-afs",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public NeighborGroupAfs getNeighborGroupAfs() {
		return neighborGroupAfs;
	}

	public void setNeighborGroupAfs(NeighborGroupAfs neighborGroupAfs) {
		this.neighborGroupAfs = neighborGroupAfs;
	}

	@Override
	public String toString() {
		return "NeighborGroup [neighborGroupName=" + neighborGroupName
				+ ", create=" + create + ", biBgpRemoteAs=" + biBgpRemoteAs
				+ ", bgpPassword=" + bgpPassword + ", updateSourceInterface="
				+ updateSourceInterface + ", neighborGracefulRestart="
				+ neighborGracefulRestart + ", neighborGroupAf="
				+ neighborGroupAfs + "]";
	}

}
