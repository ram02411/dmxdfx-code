package com.cisco.container.model.bgp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class NeighborGroups {
	
	private List<NeighborGroup> neighborGroup;

	@XmlElement(name="neighbor-group",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public List<NeighborGroup> getNeighborGroup() {
		return neighborGroup;
	}

	public void setNeighborGroup(List<NeighborGroup> neighborGroup) {
		this.neighborGroup = neighborGroup;
	}

	@Override
	public String toString() {
		return "NeighborGroups [neighborGroup=" + neighborGroup + "]";
	}
	
	

}
