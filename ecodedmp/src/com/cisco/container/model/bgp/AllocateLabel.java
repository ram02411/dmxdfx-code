package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class AllocateLabel {
	private String all;
	private String routePolicyName;

	@XmlElement(name="all",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	@XmlElement(name="route-policy-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getRoutePolicyName() {
		return routePolicyName;
	}

	public void setRoutePolicyName(String routePolicyName) {
		this.routePolicyName = routePolicyName;
	}

	@Override
	public String toString() {
		return "AllocateLabel [all=" + all + ", routePolicyName="
				+ routePolicyName + "]";
	}

}
