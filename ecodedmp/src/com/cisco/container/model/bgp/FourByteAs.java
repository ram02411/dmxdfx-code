package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

public class FourByteAs {
	private String as;
	private String bgpRunning;
	private DefaultVrf defaultVrf;

	@XmlElement(name="as",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAs() {
		return as;
	}

	public void setAs(String as) {
		this.as = as;
	}

	
	@XmlElement(name="bgp-running",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getBgpRunning() {
		return bgpRunning;
	}

	public void setBgpRunning(String bgpRunning) {
		this.bgpRunning = bgpRunning;
	}

	@XmlElement(name="default-vrf",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public DefaultVrf getDefaultVrf() {
		return defaultVrf;
	}

	public void setDefaultVrf(DefaultVrf defaultVrf) {
		this.defaultVrf = defaultVrf;
	}

	@Override
	public String toString() {
		return "FourByteAs [as=" + as + ", bgpRunning=" + bgpRunning
				+ ", defaultVrf=" + defaultVrf + "]";
	}

}
