package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

import GlobalAf.GlobalAfs;

/**
 * 
 * @author ecode
 *
 */
public class Global {

	private String nsr;
	private String routerId;
	private String gracefulRestart;
	private String enforceIbgpOutPolicy;
	private GlobalAfs globalafs;

	@XmlElement(name="nsr",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getNsr() {
		return nsr;
	}

	public void setNsr(String nsr) {
		this.nsr = nsr;
	}

	@XmlElement(name="router-id",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getRouterId() {
		return routerId;
	}

	public void setRouterId(String routerId) {
		this.routerId = routerId;
	}

	@XmlElement(name="graceful-restart",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getGracefulRestart() {
		return gracefulRestart;
	}

	public void setGracefulRestart(String gracefulRestart) {
		this.gracefulRestart = gracefulRestart;
	}

	@XmlElement(name="enforce-ibgp-out-policy",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getEnforceIbgpOutPolicy() {
		return enforceIbgpOutPolicy;
	}

	public void setEnforceIbgpOutPolicy(String enforceIbgpOutPolicy) {
		this.enforceIbgpOutPolicy = enforceIbgpOutPolicy;
	}

	@XmlElement(name="global-afs",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public GlobalAfs getGlobalafs() {
		return globalafs;
	}

	public void setGlobalafs(GlobalAfs globalafs) {
		this.globalafs = globalafs;
	}

	@Override
	public String toString() {
		return "Global [nsr=" + nsr + ", routerId=" + routerId
				+ ", gracefulRestart=" + gracefulRestart
				+ ", enforceIbgpOutPolicy=" + enforceIbgpOutPolicy
				+ ", globalafs=" + globalafs + "]";
	}

}
