package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

public class BgpEntity {

	private NeighborGroups neighborGroups;

	@XmlElement(name="neighbor-groups",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public NeighborGroups getNeighborGroups() {
		return neighborGroups;
	}

	public void setNeighborGroups(NeighborGroups neighborGroups) {
		this.neighborGroups = neighborGroups;
	}

	@Override
	public String toString() {
		return "BgpEntity [neighborGroups=" + neighborGroups + "]";
	}

}
