package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class NeighborGroupAf {

	private String afName;
	private String activate;
	private String routePolicyOut;

	@XmlElement(name="af-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAfName() {
		return afName;
	}

	public void setAfName(String afName) {
		this.afName = afName;
	}

	@XmlElement(name="activate",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getActivate() {
		return activate;
	}

	public void setActivate(String activate) {
		this.activate = activate;
	}

	@XmlElement(name="route-policy-out",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getRoutePolicyOut() {
		return routePolicyOut;
	}

	public void setRoutePolicyOut(String routePolicyOut) {
		this.routePolicyOut = routePolicyOut;
	}

	@Override
	public String toString() {
		return "NeighborGroupAf [afName=" + afName + ", activate=" + activate
				+ ", routePolicyOut=" + routePolicyOut + "]";
	}

}
