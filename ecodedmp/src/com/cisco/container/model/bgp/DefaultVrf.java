package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

public class DefaultVrf {
	private Global global;
    private BgpEntity bgpEntity;
     
    @XmlElement(name="bgp-entity",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
     public BgpEntity getBgpEntity() {
		return bgpEntity;
	}

	public void setBgpEntity(BgpEntity bgpEntity) {
		this.bgpEntity = bgpEntity;
	}

	@XmlElement(name="global",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public Global getGlobal() {
		return global;
	}

	public void setGlobal(Global global) {
		this.global = global;
	}

	@Override
	public String toString() {
		return "DefaultVrf [global=" + global + ", bgpEntity=" + bgpEntity
				+ "]";
	}


	
	

}
