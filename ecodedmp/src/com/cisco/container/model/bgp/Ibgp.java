package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Ibgp {
	private String pathsValue;
	private String unequalCost;
	private String selective;
	private String orderByIgpMetric;

	
	@XmlElement(name="paths-value",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getPathsValue() {
		return pathsValue;
	}

	
	public void setPathsValue(String pathsValue) {
		this.pathsValue = pathsValue;
	}

	@XmlElement(name="unequal-cost",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getUnequalCost() {
		return unequalCost;
	}

	
	public void setUnequalCost(String unequalCost) {
		this.unequalCost = unequalCost;
	}

	@XmlElement(name="selective",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getSelective() {
		return selective;
	}

	public void setSelective(String selective) {
		this.selective = selective;
	}

	@XmlElement(name="order-by-igp-metric",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getOrderByIgpMetric() {
		return orderByIgpMetric;
	}

	public void setOrderByIgpMetric(String orderByIgpMetric) {
		this.orderByIgpMetric = orderByIgpMetric;
	}

	@Override
	public String toString() {
		return "Ibgp [pathsValue2=" + pathsValue + ", unequalCost="
				+ unequalCost + ", selective=" + selective
				+ ", orderByIgpMetric=" + orderByIgpMetric + "]";
	}

}
