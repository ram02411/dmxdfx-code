package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class Instance {

	private String instanceName;
	private InstanceAs instanceAs;

	@XmlElement(name="instance-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	
	@XmlElement(name="instance-as",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public InstanceAs getInstanceAs() {
		return instanceAs;
	}

	public void setInstanceAs(InstanceAs instanceAs) {
		this.instanceAs = instanceAs;
	}

	@Override
	public String toString() {
		return "Instance [instanceName=" + instanceName + ", instanceAs="
				+ instanceAs + "]";
	}

}
