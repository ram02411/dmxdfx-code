package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class AdditionalPathsSelection {
	
	private String selection;
	private String routePolicyName;
	
	@XmlElement(name="selection",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getSelection() {
		return selection;
	}
	public void setSelection(String selection) {
		this.selection = selection;
	}
	
	@XmlElement(name="route-policy-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getRoutePolicyName() {
		return routePolicyName;
	}
	public void setRoutePolicyName(String routePolicyName) {
		this.routePolicyName = routePolicyName;
	}
	@Override
	public String toString() {
		return "AdditionalPathsSelection [selection=" + selection
				+ ", routePolicyName=" + routePolicyName + "]";
	}
	

}
