package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class GlobalAf {
	private String afName;
	private String enable;
	private String additionalPathsReceive;
	private String additionalPathsSend;
	private Ibgp ibgp;
	private AdditionalPathsSelection additionalPathsSelection;
	private String nextHopResolutionPrefixLengthMinimum;
	private String nextHopCriticalTriggerDelay;
	private String nextHopNonCriticalTriggerDelay;

	
    @XmlElement(name="af-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAfName() {
		return afName;
	}

	public void setAfName(String afName) {
		this.afName = afName;
	}

	 @XmlElement(name="enable",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	 @XmlElement(name="additional-paths-receive",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAdditionalPathsReceive() {
		return additionalPathsReceive;
	}

	public void setAdditionalPathsReceive(String additionalPathsReceive) {
		this.additionalPathsReceive = additionalPathsReceive;
	}

	@XmlElement(name="additional-paths-send",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAdditionalPathsSend() {
		return additionalPathsSend;
	}

	public void setAdditionalPathsSend(String additionalPathsSend) {
		this.additionalPathsSend = additionalPathsSend;
	}

	@XmlElement(name="ibgp",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public Ibgp getIbgp() {
		return ibgp;
	}

	public void setIbgp(Ibgp ibgp) {
		this.ibgp = ibgp;
	}

	
	@XmlElement(name="additional-paths-selection",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public AdditionalPathsSelection getAdditionalPathsSelection() {
		return additionalPathsSelection;
	}

	public void setAdditionalPathsSelection(
			AdditionalPathsSelection additionalPathsSelection) {
		this.additionalPathsSelection = additionalPathsSelection;
	}

	@XmlElement(name="next-hop-resolution-prefix-length-minimum",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getNextHopResolutionPrefixLengthMinimum() {
		return nextHopResolutionPrefixLengthMinimum;
	}

	public void setNextHopResolutionPrefixLengthMinimum(
			String nextHopResolutionPrefixLengthMinimum) {
		this.nextHopResolutionPrefixLengthMinimum = nextHopResolutionPrefixLengthMinimum;
	}

	@XmlElement(name="next-hop-resolution-prefix-length-minimum",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getNextHopCriticalTriggerDelay() {
		return nextHopCriticalTriggerDelay;
	}

	public void setNextHopCriticalTriggerDelay(
			String nextHopCriticalTriggerDelay) {
		this.nextHopCriticalTriggerDelay = nextHopCriticalTriggerDelay;
	}
	
	
	@XmlElement(name="next-hop-critical-trigger-delay",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getNextHopNonCriticalTriggerDelay() {
		return nextHopNonCriticalTriggerDelay;
	}

	public void setNextHopNonCriticalTriggerDelay(
			String nextHopNonCriticalTriggerDelay) {
		this.nextHopNonCriticalTriggerDelay = nextHopNonCriticalTriggerDelay;
	}

	@Override
	public String toString() {
		return "GlobalAf [afName=" + afName + ", enable=" + enable
				+ ", additionalPathsReceive=" + additionalPathsReceive
				+ ", additionalPathsSend=" + additionalPathsSend + ", ibgp="
				+ ibgp + ", additionalPathsSelection="
				+ additionalPathsSelection
				+ ", nextHopResolutionPrefixLengthMinimum="
				+ nextHopResolutionPrefixLengthMinimum
				+ ", nextHopCriticalTriggerDelay="
				+ nextHopCriticalTriggerDelay
				+ ", nextHopNonCriticalTriggerDelay="
				+ nextHopNonCriticalTriggerDelay + "]";
	}
	
	

}
