package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class InstanceAs {

	private String as;
	private FourByteAs fourByteAs;

	@XmlElement(name="as",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAs() {
		return as;
	}

	public void setAs(String as) {
		this.as = as;
	}

	@XmlElement(name="four-byte-as",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public FourByteAs getFourByteAs() {
		return fourByteAs;
	}

	public void setFourByteAs(FourByteAs fourByteAs) {
		this.fourByteAs = fourByteAs;
	}

	@Override
	public String toString() {
		return "InstanceAs [as=" + as + ", fourByteAs=" + fourByteAs + "]";
	}

}
