package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class BgpContainer {
     
	private Instance instance;

	@XmlElement(name="instance",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	@Override
	public String toString() {
		return "Bgp [instance=" + instance + "]";
	}
	
	
}
