package com.cisco.container.model.bgp;

import javax.xml.bind.annotation.XmlElement;

public class BgpRemoteAs {

	private String asxx;
	private String asyy;


	@XmlElement(name="as-xx",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAsxx() {
		return asxx;
	}

	public void setAsxx(String asxx) {
		this.asxx = asxx;
	}

	@XmlElement(name="as-yy",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getAsyy() {
		return asyy;
	}

	public void setAsyy(String asyy) {
		this.asyy = asyy;
	}

	@Override
	public String toString() {
		return "BgpRemoteAs [asxx=" + asxx + ", asyy=" + asyy + "]";
	}

}
