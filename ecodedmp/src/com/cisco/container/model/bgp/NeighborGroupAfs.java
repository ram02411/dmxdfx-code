package com.cisco.container.model.bgp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class NeighborGroupAfs {
	
	private List<NeighborGroupAf> neighborGroupAf;

	
	@XmlElement(name="neighbor-group-af",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public List<NeighborGroupAf> getNeighborGroupAf() {
		return neighborGroupAf;
	}

	public void setNeighborGroupAf(List<NeighborGroupAf> neighborGroupAf) {
		this.neighborGroupAf = neighborGroupAf;
	}

	@Override
	public String toString() {
		return "NeighborGroupAfs [neighborGroupAf=" + neighborGroupAf + "]";
	}
	
	

}
