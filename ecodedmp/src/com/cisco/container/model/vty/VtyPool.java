package com.cisco.container.model.vty;

import javax.xml.bind.annotation.XmlElement;


public class VtyPool {
	private String poolName;
	private String firstVty;
	private String lastVty;
	private String lineTemplate;
	
	@XmlElement(name="pool-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public String getPoolName() {
		return poolName;
	}

	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}

	
	@XmlElement(name="first-vty",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public String getFirstVty() {
		return firstVty;
	}

	public void setFirstVty(String firstVty) {
		this.firstVty = firstVty;
	}


	@XmlElement(name="last-vty",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public String getLastVty() {
		return lastVty;
	}

	
	public void setLastVty(String lastVty) {
		this.lastVty = lastVty;
	}


	@XmlElement(name="line-template",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public String getLineTemplate() {
		return lineTemplate;
	}

	public void setLineTemplate(String lineTemplate) {
		this.lineTemplate = lineTemplate;
	}

	@Override
	public String toString() {
		return "VtyPool [poolName=" + poolName + ", firstVty=" + firstVty
				+ ", lastVty=" + lastVty + ", lineTemplate=" + lineTemplate
				+ "]";
	}

}
