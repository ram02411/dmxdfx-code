package com.cisco.container.model.vty;

import javax.xml.bind.annotation.XmlElement;

public class VtyPools {
	
	private VtyPool vtyPool;

	@XmlElement(name="vty-pool",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public VtyPool getVtyPool() {
		return vtyPool;
	}

	public void setVtyPool(VtyPool vtyPool) {
		this.vtyPool = vtyPool;
	}

	@Override
	public String toString() {
		return "VtyPools [vtyPool=" + vtyPool + "]";
	}
	
}
