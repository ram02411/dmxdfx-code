package com.cisco.container.model.vty;

import javax.xml.bind.annotation.XmlElement;

public class Vty {

	private VtyPools vtyPools;

	@XmlElement(name="vty-pools",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public VtyPools getVtyPools() {
		return vtyPools;
	}

	public void setVtyPools(VtyPools vtyPools) {
		this.vtyPools = vtyPools;
	}

	@Override
	public String toString() {
		return "Vty [vtyPools=" + vtyPools + "]";
	}

	
}
