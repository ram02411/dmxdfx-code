package com.cisco.container.model.keychains;

import javax.xml.bind.annotation.XmlElement;

public class Key {
	private String keyid;
	private AcceptLifetime acceptLifetime;
	private String keyString;
	private SendLifetime sendLifetime;
	private String cryptographicAlgorithm;

	
	@XmlElement(name="key-id",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getKeyid() {
		return keyid;
	}

	public void setKeyid(String keyid) {
		this.keyid = keyid;
	}

	@XmlElement(name="accept-lifetime",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public AcceptLifetime getAcceptLifetime() {
		return acceptLifetime;
	}

	public void setAcceptLifetime(AcceptLifetime acceptLifetime) {
		this.acceptLifetime = acceptLifetime;
	}

	@XmlElement(name="key-string",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getKeyString() {
		return keyString;
	}

	public void setKeyString(String keyString) {
		this.keyString = keyString;
	}

	@XmlElement(name="send-lifetime",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public SendLifetime getSendLifetime() {
		return sendLifetime;
	}

	public void setSendLifetime(SendLifetime sendLifetime) {
		this.sendLifetime = sendLifetime;
	}

	@XmlElement(name="cryptographic-algorithm",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getCryptographicAlgorithm() {
		return cryptographicAlgorithm;
	}

	public void setCryptographicAlgorithm(String cryptographicAlgorithm) {
		this.cryptographicAlgorithm = cryptographicAlgorithm;
	}

	@Override
	public String toString() {
		return "Key [keyid=" + keyid + ", acceptLifetime=" + acceptLifetime
				+ ", keyString=" + keyString + ", sendLifetime=" + sendLifetime
				+ ", cryptographicAlgorithm=" + cryptographicAlgorithm + "]";
	}

}
