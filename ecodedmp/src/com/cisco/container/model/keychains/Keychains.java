package com.cisco.container.model.keychains;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Keychains {

	private Keychain keychain;
	
	@XmlElement(name="keychain",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public Keychain getKeychain() {
		return keychain;
	}

	public void setKeychain(Keychain keychain) {
		this.keychain = keychain;
	}

	@Override
	public String toString() {
		return "Keychains [keychain=" + keychain + "]";
	}
	
	
}
