package com.cisco.container.model.keychains;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *  http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg
 */
public class SendLifetime {
	
	private String startHour;
	private String startMinutes;
	private String startSeconds;
	private String startDate;
	private String startMonth;
	private String startYear;
	private String infiniteFlag;

	@XmlElement(name="start-hour",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getStartHour() {
		return startHour;
	}

	public void setStartHour(String startHour) {
		this.startHour = startHour;
	}

	@XmlElement(name="start-minutes",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getStartMinutes() {
		return startMinutes;
	}

	public void setStartMinutes(String startMinutes) {
		this.startMinutes = startMinutes;
	}

	@XmlElement(name="start-seconds",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getStartSeconds() {
		return startSeconds;
	}

	public void setStartSeconds(String startSeconds) {
		this.startSeconds = startSeconds;
	}

	@XmlElement(name="start-date",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	@XmlElement(name="start-month",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getStartMonth() {
		return startMonth;
	}

	public void setStartMonth(String startMonth) {
		this.startMonth = startMonth;
	}

	@XmlElement(name="start-year",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	@XmlElement(name="infinite-flag",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getInfiniteFlag() {
		return infiniteFlag;
	}

	public void setInfiniteFlag(String infiniteFlag) {
		this.infiniteFlag = infiniteFlag;
	}

	@Override
	public String toString() {
		return "SendLifetime [startHour=" + startHour + ", startMinutes="
				+ startMinutes + ", startSeconds=" + startSeconds
				+ ", startDate=" + startDate + ", startMonth=" + startMonth
				+ ", startYear=" + startYear + ", infiniteFlag=" + infiniteFlag
				+ "]";
	}

}
