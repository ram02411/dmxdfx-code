package com.cisco.container.model.keychains;

import javax.xml.bind.annotation.XmlElement;

public class Keychain {

	private String chainName;
	private Keies keies;

	@XmlElement(name="chain-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public String getChainName() {
		return chainName;
	}

	public void setChainName(String chainName) {
		this.chainName = chainName;
	}

	@XmlElement(name="keies",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public Keies getKeies() {
		return keies;
	}

	public void setKeies(Keies keies) {
		this.keies = keies;
	}

	@Override
	public String toString() {
		return "Keychain [chainName=" + chainName + ", keies=" + keies + "]";
	}

}
