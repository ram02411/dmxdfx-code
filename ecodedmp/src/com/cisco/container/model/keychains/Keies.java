package com.cisco.container.model.keychains;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Keies {

	private List<Key> key;

	@XmlElement(name="key",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public List<Key> getKey() {
		return key;
	}

	public void setKey(List<Key> key) {
		this.key = key;
	}

	@Override
	public String toString() {
		return "Keies [key=" + key + "]";
	}

}
