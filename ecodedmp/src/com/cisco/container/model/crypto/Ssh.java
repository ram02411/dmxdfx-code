package com.cisco.container.model.crypto;

import javax.xml.bind.annotation.XmlElement;

public class Ssh {
	
	private Server server;

	@XmlElement(name="server",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-crypto-ssh-cfg")
	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	@Override
	public String toString() {
		return "Ssh [server=" + server + "]";
	}
	
	

}
