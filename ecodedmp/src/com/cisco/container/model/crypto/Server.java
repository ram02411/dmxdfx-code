package com.cisco.container.model.crypto;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Server {
	private String v2;
	private String netconf;

	@XmlElement(name="v2",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-crypto-ssh-cfg")
	public String getV2() {
		return v2;
	}

	public void setV2(String v2) {
		this.v2 = v2;
	}

	@XmlElement(name="netconf",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-crypto-ssh-cfg")
	public String getNetconf() {
		return netconf;
	}

	public void setNetconf(String netconf) {
		this.netconf = netconf;
	}

	@Override
	public String toString() {
		return "Server [v2=" + v2 + ", netconf=" + netconf + "]";
	}

}
