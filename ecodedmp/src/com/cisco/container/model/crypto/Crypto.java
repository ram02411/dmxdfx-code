package com.cisco.container.model.crypto;

import javax.xml.bind.annotation.XmlElement;

public class Crypto {

	private Ssh ssh;

	@XmlElement(name="ssh",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-crypto-ssh-cfg")
	public Ssh getSsh() {
		return ssh;
	}

	public void setSsh(Ssh ssh) {
		this.ssh = ssh;
	}

	@Override
	public String toString() {
		return "Crypto [ssh=" + ssh + "]";
	}

}
