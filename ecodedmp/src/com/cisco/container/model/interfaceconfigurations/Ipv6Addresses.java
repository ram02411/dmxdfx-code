package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

public class Ipv6Addresses {
	
	
	private RegularAddresses regularAddresses;

	@XmlElement(name="regular-addresses",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")
	public RegularAddresses getRegularAddresses() {
		return regularAddresses;
	}

	public void setRegularAddresses(RegularAddresses regularAddresses) {
		this.regularAddresses = regularAddresses;
	}

	@Override
	public String toString() {
		return "Ipv6Addresses [regularAddresses=" + regularAddresses + "]";
	}
	
	

}
