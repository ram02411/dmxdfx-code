package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

public class Ipv6Network {

	private Ipv6Addresses ipv6Addresses;

	@XmlElement(name="addresses",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")
	public Ipv6Addresses getIpv6Addresses() {
		return ipv6Addresses;
	}

	public void setIpv6Addresses(Ipv6Addresses ipv6Addresses) {
		this.ipv6Addresses = ipv6Addresses;
	}

	@Override
	public String toString() {
		return "Ipv6Network [ipv6Addresses=" + ipv6Addresses + "]";
	}
	
}
