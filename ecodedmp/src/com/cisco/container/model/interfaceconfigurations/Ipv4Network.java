package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

public class Ipv4Network {
  
	private Addresses addresses;

	 @XmlElement(name="addresses",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-io-cfg")
	public Addresses getAddresses() {
		return addresses;
	}

	public void setAddresses(Addresses addresses) {
		this.addresses = addresses;
	}

	@Override
	public String toString() {
		return "Ipv4Network [addresses=" + addresses + "]";
	}
	
	
}
