package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class Primary {
	private String address;
	private String netmask;

	
    @XmlElement(name="address",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-io-cfg")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@XmlElement(name="netmask",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-io-cfg")
	public String getNetmask() {
		return netmask;
	}

	public void setNetmask(String netmask) {
		this.netmask = netmask;
	}

	@Override
	public String toString() {
		return "Primary [address=" + address + ", netmask=" + netmask + "]";
	}

}
