package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

public class RegularAddress {

	private String address;
	private String prefixLength;
	private String zone;
	
		
	@XmlElement(name="address",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@XmlElement(name="prefix-length",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")	
	public String getPrefixLength() {
		return prefixLength;
	}

	public void setPrefixLength(String prefixLength) {
		this.prefixLength = prefixLength;
	}

	@XmlElement(name="zone",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")	
	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	@Override
	public String toString() {
		return "RegularAddress [address=" + address + ", prefixLength="
				+ prefixLength + ", zone=" + zone + "]";
	}

}
