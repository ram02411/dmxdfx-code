package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class InterfaceConfiguration {
	private String active;
	private String interfaceName;
	private String interfaceVirtual;
	private String description;
	private Ipv4Network ipv4Network;
	private Ipv6Network ipv6Network;

	
	
    @XmlElement(name="active",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg")
	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	@XmlElement(name="interface-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg")
	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	@XmlElement(name="interface-virtual",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg")
	public String getInterfaceVirtual() {
		return interfaceVirtual;
	}

	public void setInterfaceVirtual(String interfaceVirtual) {
		this.interfaceVirtual = interfaceVirtual;
	}

	@XmlElement(name="description",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="ipv4-network",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-io-cfg")
	public Ipv4Network getIpv4Network() {
		return ipv4Network;
	}

	public void setIpv4Network(Ipv4Network ipv4Network) {
		this.ipv4Network = ipv4Network;
	}

	@XmlElement(name="ipv6-network",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")
	public Ipv6Network getIpv6Network() {
		return ipv6Network;
	}

	public void setIpv6Network(Ipv6Network ipv6Network) {
		this.ipv6Network = ipv6Network;
	}

	@Override
	public String toString() {
		return "InterfaceConfiguration [active=" + active + ", interfaceName="
				+ interfaceName + ", interfaceVirtual=" + interfaceVirtual
				+ ", description=" + description + ", ipv4Network="
				+ ipv4Network + ", ipv6Network=" + ipv6Network + "]";
	}

}
