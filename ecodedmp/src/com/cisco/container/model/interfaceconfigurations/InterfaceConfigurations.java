package com.cisco.container.model.interfaceconfigurations;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceConfigurations {

	private List<InterfaceConfiguration> interfaceConfiguration;

	
	@XmlElement(name="interface-configuration",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg")
	public List<InterfaceConfiguration> getInterfaceConfiguration() {
		return interfaceConfiguration;
	}

	public void setInterfaceConfiguration(
			List<InterfaceConfiguration> interfaceConfiguration) {
		this.interfaceConfiguration = interfaceConfiguration;
	}

	@Override
	public String toString() {
		return "InterfaceConfigurations [interfaceConfiguration="
				+ interfaceConfiguration + "]";
	}
	
}
