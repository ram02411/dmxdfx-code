package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

public class Addresses {
	private Primary primary;

	@XmlElement(name="primary",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-io-cfg")
	public Primary getPrimary() {
		return primary;
	}

	public void setPrimary(Primary primary) {
		this.primary = primary;
	}

	@Override
	public String toString() {
		return "Addresses [primary=" + primary + "]";
	}
	
	

}
