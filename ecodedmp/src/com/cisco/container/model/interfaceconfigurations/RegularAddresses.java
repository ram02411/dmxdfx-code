package com.cisco.container.model.interfaceconfigurations;

import javax.xml.bind.annotation.XmlElement;

public class RegularAddresses {

	private RegularAddress regularAddress;

	@XmlElement(name="regular-address",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv6-ma-cfg")	
	public RegularAddress getRegularAddress() {
		return regularAddress;
	}

	public void setRegularAddress(RegularAddress regularAddress) {
		this.regularAddress = regularAddress;
	}

	@Override
	public String toString() {
		return "RegularAddresses [regularAddress=" + regularAddress + "]";
	}

}
