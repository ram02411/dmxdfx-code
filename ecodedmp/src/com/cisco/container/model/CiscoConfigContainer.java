package com.cisco.container.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.cisco.container.model.bgp.BgpContainer;
import com.cisco.container.model.crypto.Crypto;
import com.cisco.container.model.interfaceconfigurations.InterfaceConfigurations;
import com.cisco.container.model.keychains.Keychains;
import com.cisco.container.model.mib.Mib;
import com.cisco.container.model.netconfyong.NetConfYong;
import com.cisco.container.model.routerstatic.RouterStatic;
import com.cisco.container.model.routingpolicy.RoutingPolicy;
import com.cisco.container.model.snmp.Snmp;
import com.cisco.container.model.syslog.Syslog;
import com.cisco.container.model.tty.Tty;
import com.cisco.container.model.vrfs.Vrfs;
import com.cisco.container.model.vty.Vty;
import com.cisco.container.model.yangbgp.YangBgp;

@XmlRootElement(name = "data", namespace = "urn:ietf:params:xml:ns:netconf:base:1.0")
public class CiscoConfigContainer {
	private Crypto crypto;
	private InterfaceConfigurations interfaceConfigurations;
	private Vrfs vrfs;
	private Syslog syslog;
	private RouterStatic routerStatic;
	private BgpContainer bgpContainer;
	private YangBgp bgp;
	private Keychains keychains;
	private NetConfYong netConfYong;
	private RoutingPolicy routingPolicy;
	private String hostname;
	private Snmp snmp;
	private Mib mib;
	private Tty tty;
	private Vty vty;
	
	@XmlElement(name = "bgp", namespace = "http://openconfig.net/yang/bgp")
	public YangBgp getBgp() {
		return bgp;
	}

	public void setBgp(YangBgp bgp) {
		this.bgp = bgp;
	}

	@XmlElement(name = "routing-policy", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public RoutingPolicy getRoutingPolicy() {
		return routingPolicy;
	}

	public void setRoutingPolicy(RoutingPolicy routingPolicy) {
		this.routingPolicy = routingPolicy;
	}

	@XmlElement(name = "bgp", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public BgpContainer getBgpContainer() {
		return bgpContainer;
	}

	public void setBgpContainer(BgpContainer bgpContainer) {
		this.bgpContainer = bgpContainer;
	}

	@XmlElement(name = "vrfs", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public Vrfs getVrfs() {
		return vrfs;
	}

	public void setVrfs(Vrfs vrfs) {
		this.vrfs = vrfs;
	}

	@XmlElement(name = "syslog", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-infra-syslog-cfg")
	public Syslog getSyslog() {
		return syslog;
	}

	public void setSyslog(Syslog syslog) {
		this.syslog = syslog;
	}

	@XmlElement(name = "keychains", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-lib-keychain-cfg")
	public Keychains getKeychains() {
		return keychains;
	}

	public void setKeychains(Keychains keychains) {
		this.keychains = keychains;
	}

	@XmlElement(name = "netconf-yang", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-man-netconf-cfg")
	public NetConfYong getNetConfYong() {
		return netConfYong;
	}

	public void setNetConfYong(NetConfYong netConfYong) {
		this.netConfYong = netConfYong;
	}

	@XmlElement(name = "host-name", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-shellutil-cfg")
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@XmlElement(name = "snmp", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Snmp getSnmp() {
		return snmp;
	}

	public void setSnmp(Snmp snmp) {
		this.snmp = snmp;
	}

	@XmlElement(name = "mib", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Mib getMib() {
		return mib;
	}

	public void setMib(Mib mib) {
		this.mib = mib;
	}

	@XmlElement(name = "tty", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public Tty getTty() {
		return tty;
	}

	public void setTty(Tty tty) {
		this.tty = tty;
	}

	@XmlElement(name = "vty", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-tty-vty-cfg")
	public Vty getVty() {
		return vty;
	}

	public void setVty(Vty vty) {
		this.vty = vty;
	}

	@XmlElement(name = "crypto", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-crypto-sam-cfg")
	public Crypto getCrypto() {
		return crypto;
	}

	public void setCrypto(Crypto crypto) {
		this.crypto = crypto;
	}
	
	@XmlElement(name = "router-static", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public RouterStatic getRouterStatic() {
		return routerStatic;
	}

	public void setRouterStatic(RouterStatic routerStatic) {
		this.routerStatic = routerStatic;
	}

	@XmlElement(name = "interface-configurations", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ifmgr-cfg")
	public InterfaceConfigurations getInterfaceConfigurations() {
		return interfaceConfigurations;
	}

	public void setInterfaceConfigurations(
			InterfaceConfigurations interfaceConfigurations) {
		this.interfaceConfigurations = interfaceConfigurations;
	}

	@Override
	public String toString() {
		return "CiscoConfigContainer [crypto=" + crypto
				+ ", interfaceConfigurations=" + interfaceConfigurations
				+ ", vrfs=" + vrfs + ", syslog=" + syslog + ", routerStatic="
				+ routerStatic + ", bgpContainer=" + bgpContainer + ", bgp="
				+ bgp + ", keychains=" + keychains + ", netConfYong="
				+ netConfYong + ", routingPolicy=" + routingPolicy
				+ ", hostname=" + hostname + ", snmp=" + snmp + ", mib=" + mib
				+ ", tty=" + tty + ", vty=" + vty + "]";
	}


}
