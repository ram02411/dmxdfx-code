package com.cisco.container.model.routingpolicy;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class PrefixSets {
	
	private List<PrefixSet> prefixSet;

	@XmlElement(name="prefix-set",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public List<PrefixSet> getPrefixSet() {
		return prefixSet;
	}

	public void setPrefixSet(List<PrefixSet> prefixSet) {
		this.prefixSet = prefixSet;
	}

	@Override
	public String toString() {
		return "PrefixSets [prefixSet=" + prefixSet + "]";
	}
}
