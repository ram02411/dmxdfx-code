package com.cisco.container.model.routingpolicy;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class RoutePolicies {
	
	private List<RoutePolicy> routePolicy;

	@XmlElement(name="route-policy",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public List<RoutePolicy> getRoutePolicy() {
		return routePolicy;
	}

	public void setRoutePolicy(List<RoutePolicy> routePolicy) {
		this.routePolicy = routePolicy;
	}

	@Override
	public String toString() {
		return "RoutePolicies [routePolicy=" + routePolicy + "]";
	}
	

}
