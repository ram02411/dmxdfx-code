package com.cisco.container.model.routingpolicy;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author bgp
 *
 */
public class RoutePolicy {

	private String routePolicyName;
	private String rplRoutePolicy;

	
	@XmlElement(name="route-policy-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public String getRoutePolicyName() {
		return routePolicyName;
	}

	public void setRoutePolicyName(String routePolicyName) {
		this.routePolicyName = routePolicyName;
	}

	@XmlElement(name="rpl-route-policy",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public String getRplRoutePolicy() {
		return rplRoutePolicy;
	}

	public void setRplRoutePolicy(String rplRoutePolicy) {
		this.rplRoutePolicy = rplRoutePolicy;
	}

	@Override
	public String toString() {
		return "RoutePolicy [routePolicyName=" + routePolicyName
				+ ", rplRoutePolicy=" + rplRoutePolicy + "]";
	}

}
