package com.cisco.container.model.routingpolicy;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class RoutingPolicy {

	private Sets sets;
	private RoutePolicies routePolicies;

	@XmlElement(name="sets",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public Sets getSets() {
		return sets;
	}

	public void setSets(Sets sets) {
		this.sets = sets;
	}

	@XmlElement(name="route-policies",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public RoutePolicies getRoutePolicies() {
		return routePolicies;
	}

	public void setRoutePolicies(RoutePolicies routePolicies) {
		this.routePolicies = routePolicies;
	}

	@Override
	public String toString() {
		return "RoutingPolicy [sets=" + sets + ", routePolicies="
				+ routePolicies + "]";
	}

}
