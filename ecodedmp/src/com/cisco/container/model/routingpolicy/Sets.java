package com.cisco.container.model.routingpolicy;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class Sets {

	private PrefixSets prefixSets;
	private CommunitySets communitySets;

	@XmlElement(name="prefix-sets",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public PrefixSets getPrefixSets() {
		return prefixSets;
	}

	public void setPrefixSets(PrefixSets prefixSets) {
		this.prefixSets = prefixSets;
	}

	@XmlElement(name="community-sets",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public CommunitySets getCommunitySets() {
		return communitySets;
	}

	public void setCommunitySets(CommunitySets communitySets) {
		this.communitySets = communitySets;
	}

	@Override
	public String toString() {
		return "Sets [prefixSets=" + prefixSets + ", communitySets="
				+ communitySets + "]";
	}

}
