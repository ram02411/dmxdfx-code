package com.cisco.container.model.routingpolicy;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class PrefixSet {
	private String setName;
	private String rplPrefixSet;

	@XmlElement(name="set-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	
	@XmlElement(name="rpl-prefix-set",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public String getRplPrefixSet() {
		return rplPrefixSet;
	}

	public void setRplPrefixSet(String rplPrefixSet) {
		this.rplPrefixSet = rplPrefixSet;
	}

	@Override
	public String toString() {
		return "PrefixSet [setName=" + setName + ", rplPrefixSet="
				+ rplPrefixSet + "]";
	}

}
