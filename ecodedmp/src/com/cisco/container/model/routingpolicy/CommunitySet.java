package com.cisco.container.model.routingpolicy;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class CommunitySet {

	private String setName;
	private String rplCommunitySet;

	@XmlElement(name="set-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public String getSetName() {
		return setName;
	}

	public void setSetName(String setName) {
		this.setName = setName;
	}

	@XmlElement(name="rpl-community-set",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public String getRplCommunitySet() {
		return rplCommunitySet;
	}

	public void setRplCommunitySet(String rplCommunitySet) {
		this.rplCommunitySet = rplCommunitySet;
	}

	@Override
	public String toString() {
		return "CommunitySet [setName=" + setName + ", rplCommunitySet="
				+ rplCommunitySet + "]";
	}

}
