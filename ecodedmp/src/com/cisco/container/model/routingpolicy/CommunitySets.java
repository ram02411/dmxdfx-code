package com.cisco.container.model.routingpolicy;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class CommunitySets {
	
	private List<CommunitySet> communitySet;

	@XmlElement(name="community-set",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-policy-repository-cfg")
	public List<CommunitySet> getCommunitySet() {
		return communitySet;
	}

	public void setCommunitySet(List<CommunitySet> communitySet) {
		this.communitySet = communitySet;
	}

	@Override
	public String toString() {
		return "CommunitySets [communitySet=" + communitySet + "]";
	}

}
