package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

public class RouterStatic {
	
	private RouteStaticDefaultVrf defaultVrf;

	@XmlElement(name = "default-vrf", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public RouteStaticDefaultVrf getDefaultVrf() {
		return defaultVrf;
	}

	public void setDefaultVrf(RouteStaticDefaultVrf defaultVrf) {
		this.defaultVrf = defaultVrf;
	}

	@Override
	public String toString() {
		return "RouterStatic [defaultVrf=" + defaultVrf + "]";
	}
	
	
	

}
