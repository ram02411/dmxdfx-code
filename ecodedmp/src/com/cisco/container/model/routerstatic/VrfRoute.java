package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

public class VrfRoute {
	
	private VrfNextHops vrfNextHops;

	@XmlElement(name = "vrf-next-hops", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public VrfNextHops getVrfNextHops() {
		return vrfNextHops;
	}

	public void setVrfNextHops(VrfNextHops vrfNextHops) {
		this.vrfNextHops = vrfNextHops;
	}

	@Override
	public String toString() {
		return "VrfRoute [vrfNextHops=" + vrfNextHops + "]";
	}

}
