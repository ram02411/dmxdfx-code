package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

public class Vrfipv4 {
	
	private VrfUnicast vrfUnicast;

	@XmlElement(name = "vrf-unicast", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public VrfUnicast getVrfUnicast() {
		return vrfUnicast;
	}

	public void setVrfUnicast(VrfUnicast vrfUnicast) {
		this.vrfUnicast = vrfUnicast;
	}

	@Override
	public String toString() {
		return "Vrfipv4 [vrfUnicast=" + vrfUnicast + "]";
	}

}
