package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

public class RouteStaticDefaultVrf {
	private AddressFamily addressFamily;

	@XmlElement(name = "address-family", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public AddressFamily getAddressFamily() {
		return addressFamily;
	}

	public void setAddressFamily(AddressFamily addressFamily) {
		this.addressFamily = addressFamily;
	}

	@Override
	public String toString() {
		return "RouteStaticDefaultVrf [addressFamily=" + addressFamily + "]";
	}

}
