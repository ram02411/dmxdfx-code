package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class AddressFamily {
	
	private Vrfipv4  vrfipv4;

	@XmlElement(name = "vrfipv4", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public Vrfipv4 getVrfipv4() {
		return vrfipv4;
	}

	public void setVrfipv4(Vrfipv4 vrfipv4) {
		this.vrfipv4 = vrfipv4;
	}

	@Override
	public String toString() {
		return "AddressFamily [vrfipv4=" + vrfipv4 + "]";
	}
	
}
