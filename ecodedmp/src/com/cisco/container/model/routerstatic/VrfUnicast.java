package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

public class VrfUnicast {
	
	private VrfPrefixes vrfPrefixes;

	 @XmlElement(name = "vrf-prefixes", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public VrfPrefixes getVrfPrefixes() {
		return vrfPrefixes;
	}

	public void setVrfPrefixes(VrfPrefixes vrfPrefixes) {
		this.vrfPrefixes = vrfPrefixes;
	}

	@Override
	public String toString() {
		return "VrfUnicast [vrfPrefixes=" + vrfPrefixes + "]";
	}
	
	
}
