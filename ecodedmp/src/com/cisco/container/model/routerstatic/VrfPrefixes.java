package com.cisco.container.model.routerstatic;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class VrfPrefixes {
	
    private List<VrfPrefix> vrfPrefix;

    @XmlElement(name = "vrf-prefix", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public List<VrfPrefix> getVrfPrefix() {
		return vrfPrefix;
	}

	public void setVrfPrefix(List<VrfPrefix> vrfPrefix) {
		this.vrfPrefix = vrfPrefix;
	}

	@Override
	public String toString() {
		return "VrfPrefixes [vrfPrefix=" + vrfPrefix + "]";
	}
    
}
