package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

public class VrfPrefix {
	private String prefix;
	private String prefixLength;
	private VrfRoute vrfRoute;

	@XmlElement(name = "prefix", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	@XmlElement(name = "prefix-length", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public String getPrefixLength() {
		return prefixLength;
	}

	public void setPrefixLength(String prefixLength) {
		this.prefixLength = prefixLength;
	}

	@XmlElement(name = "vrf-route", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public VrfRoute getVrfRoute() {
		return vrfRoute;
	}

	public void setVrfRoute(VrfRoute vrfRoute) {
		this.vrfRoute = vrfRoute;
	}

	@Override
	public String toString() {
		return "VrfPrefix [prefix=" + prefix + ", prefixLength=" + prefixLength
				+ ", vrfRoute=" + vrfRoute + "]";
	}

}
