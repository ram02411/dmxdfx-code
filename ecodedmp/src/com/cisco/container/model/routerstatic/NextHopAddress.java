package com.cisco.container.model.routerstatic;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class NextHopAddress {
	
	private List<String> nextHopAddress;

	@XmlElement(name = "next-hop-address", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public List<String> getNextHopAddress() {
		return nextHopAddress;
	}

	public void setNextHopAddress(List<String> nextHopAddress) {
		this.nextHopAddress = nextHopAddress;
	}

	@Override
	public String toString() {
		return "NextHopAddress [nextHopAddress=" + nextHopAddress + "]";
	}

}
