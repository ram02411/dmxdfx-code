package com.cisco.container.model.routerstatic;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class VrfNextHops {
	
	private NextHopAddress nextHopAddress;

	@XmlElement(name = "next-hop-address", namespace = "http://cisco.com/ns/yang/Cisco-IOS-XR-ip-static-cfg")
	public NextHopAddress getNextHopAddress() {
		return nextHopAddress;
	}

	public void setNextHopAddress(NextHopAddress nextHopAddress) {
		this.nextHopAddress = nextHopAddress;
	}
	
	@Override
	public String toString() {
		return "VrfNextHops [nextHopAddress=" + nextHopAddress + "]";
	}
	

}
