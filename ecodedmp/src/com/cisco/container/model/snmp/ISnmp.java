package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class ISnmp {

	private String linkup;
	private String linkdown;

	@XmlElement(name="linkup",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-ifmib-cfg")
	public String getLinkup() {
		return linkup;
	}

	public void setLinkup(String linkup) {
		this.linkup = linkup;
	}

	@XmlElement(name="linkdown",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-ifmib-cfg")
	public String getLinkdown() {
		return linkdown;
	}

	public void setLinkdown(String linkdown) {
		this.linkdown = linkdown;
	}

	@Override
	public String toString() {
		return "ISnmp [linkup=" + linkup + ", linkdown=" + linkdown + "]";
	}

}
