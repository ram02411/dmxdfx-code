package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Ipv4 {
	
	private Tos tos;

	@XmlElement(name="tos",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Tos getTos() {
		return tos;
	}

	public void setTos(Tos tos) {
		this.tos = tos;
	}

	@Override
	public String toString() {
		return "Ipv4 [tos=" + tos + "]";
	}
	
	

}
