package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class EntityState {

	private String operStatus;
	private String switchover;

	@XmlElement(name="oper-status",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-entstatemib-cfg")
	public String getOperStatus() {
		return operStatus;
	}

	public void setOperStatus(String operStatus) {
		this.operStatus = operStatus;
	}

	@XmlElement(name="switchover",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-entstatemib-cfg")
	public String getSwitchover() {
		return switchover;
	}

	public void setSwitchover(String switchover) {
		this.switchover = switchover;
	}

	@Override
	public String toString() {
		return "EntityState [operStatus=" + operStatus + ", switchover="
				+ switchover + "]";
	}

}
