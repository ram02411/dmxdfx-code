package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class DefaultUserCommunity {
	private String communityName;
	private String version;
	private String basicTrapTypes;
	private String advancedTrapTypes1;
	private String advancedTrapTypes2;

	@XmlElement(name="community-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

	@XmlElement(name="version",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name="basic-trap-types",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getBasicTrapTypes() {
		return basicTrapTypes;
	}

	public void setBasicTrapTypes(String basicTrapTypes) {
		this.basicTrapTypes = basicTrapTypes;
	}

	@XmlElement(name="advanced-trap-types1",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getAdvancedTrapTypes1() {
		return advancedTrapTypes1;
	}

	public void setAdvancedTrapTypes1(String advancedTrapTypes1) {
		this.advancedTrapTypes1 = advancedTrapTypes1;
	}

	@XmlElement(name="advanced-trap-types2",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getAdvancedTrapTypes2() {
		return advancedTrapTypes2;
	}

	public void setAdvancedTrapTypes2(String advancedTrapTypes2) {
		this.advancedTrapTypes2 = advancedTrapTypes2;
	}

	@Override
	public String toString() {
		return "DefaultUserCommunity [communityName=" + communityName
				+ ", version=" + version + ", basicTrapTypes=" + basicTrapTypes
				+ ", advancedTrapTypes1=" + advancedTrapTypes1
				+ ", advancedTrapTypes2=" + advancedTrapTypes2 + "]";
	}

}
