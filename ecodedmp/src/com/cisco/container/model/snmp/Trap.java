package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Trap {
	private String queueLength;

	@XmlElement(name="queue-length",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getQueueLength() {
		return queueLength;
	}

	public void setQueueLength(String queueLength) {
		this.queueLength = queueLength;
	}

	@Override
	public String toString() {
		return "Trap [queueLength=" + queueLength + "]";
	}

}
