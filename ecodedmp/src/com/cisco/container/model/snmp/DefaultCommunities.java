package com.cisco.container.model.snmp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class DefaultCommunities {

	private List<DefaultCommunity> defaultCommunity;

	@XmlElement(name="default-community",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public List<DefaultCommunity> getDefaultCommunity() {
		return defaultCommunity;
	}

	public void setDefaultCommunity(List<DefaultCommunity> defaultCommunity) {
		this.defaultCommunity = defaultCommunity;
	}

	@Override
	public String toString() {
		return "DefaultCommunities [defaultCommunity=" + defaultCommunity + "]";
	}
	
	
}
