package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Notification {

	private Bgp bgp;
	private ISnmp iSnmp;
	private ConfigMan configMan;
	private Entity entity;
	private FruControl fruControl;
	private EntityState entityState;
	private EntityRedundancy entityRedundancy;

	
	@XmlElement(name="bgp",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")	
	public Bgp getBgp() {
		return bgp;
	}

	public void setBgp(Bgp bgp) {
		this.bgp = bgp;
	}

	@XmlElement(name="snmp",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public ISnmp getiSnmp() {
		return iSnmp;
	}

	public void setiSnmp(ISnmp iSnmp) {
		this.iSnmp = iSnmp;
	}

	@XmlElement(name="config-man",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-config-mibs-cfg")
	public ConfigMan getConfigMan() {
		return configMan;
	}

	public void setConfigMan(ConfigMan configMan) {
		this.configMan = configMan;
	}

	@XmlElement(name="entity",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-entitymib-cfg")
	public Entity getEntity() {
		return entity;
	}

	public void setEntity(Entity entity) {
		this.entity = entity;
	}

	@XmlElement(name="fru-control",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-frucontrolmib-cfg")
	public FruControl getFruControl() {
		return fruControl;
	}

	public void setFruControl(FruControl fruControl) {
		this.fruControl = fruControl;
	}

	@XmlElement(name="entity-state",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-entstatemib-cfg")
	public EntityState getEntityState() {
		return entityState;
	}

	public void setEntityState(EntityState entityState) {
		this.entityState = entityState;
	}

	@XmlElement(name="entity-redundancy",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-ceredundancymib-cfg")
	public EntityRedundancy getEntityRedundancy() {
		return entityRedundancy;
	}

	public void setEntityRedundancy(EntityRedundancy entityRedundancy) {
		this.entityRedundancy = entityRedundancy;
	}

	@Override
	public String toString() {
		return "Notification [bgp=" + bgp + ", iSnmp=" + iSnmp + ", configMan="
				+ configMan + ", entity=" + entity + ", fruControl="
				+ fruControl + ", entityState=" + entityState
				+ ", entityRedundancy=" + entityRedundancy + "]";
	}

}
