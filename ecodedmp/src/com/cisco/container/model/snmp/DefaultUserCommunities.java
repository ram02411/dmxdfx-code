package com.cisco.container.model.snmp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class DefaultUserCommunities {

	private List<DefaultUserCommunity> defaultUserCommunity;

	
	@XmlElement(name="default-user-community",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public List<DefaultUserCommunity> getDefaultUserCommunity() {
		return defaultUserCommunity;
	}

	public void setDefaultUserCommunity(
			List<DefaultUserCommunity> defaultUserCommunity) {
		this.defaultUserCommunity = defaultUserCommunity;
	}

	@Override
	public String toString() {
		return "DefaultUserCommunities [defaultUserCommunity="
				+ defaultUserCommunity + "]";
	}

}
