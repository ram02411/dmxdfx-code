package com.cisco.container.model.snmp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class TrapHosts {
	
	private List<TrapHost> trapHost;

	@XmlElement(name="trap-host",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public List<TrapHost> getTrapHost() {
		return trapHost;
	}

	public void setTrapHost(List<TrapHost> trapHost) {
		this.trapHost = trapHost;
	}

	@Override
	public String toString() {
		return "TrapHosts [trapHost=" + trapHost + "]";
	}

}
