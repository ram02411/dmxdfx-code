package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Administration {
 
	private DefaultCommunities defaultCommunities;

	@XmlElement(name="default-communities",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public DefaultCommunities getDefaultCommunities() {
		return defaultCommunities;
	}

	public void setDefaultCommunities(DefaultCommunities defaultCommunities) {
		this.defaultCommunities = defaultCommunities;
	}

	@Override
	public String toString() {
		return "Administration [defaultCommunities=" + defaultCommunities + "]";
	}
	
	
}
