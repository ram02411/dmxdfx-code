package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class DefaultCommunity {
	
	private  String communityName;
	private  String priviledge;
	private  String v4aclType;
	private  String v4accessList;
	
	@XmlElement(name="community-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getCommunityName() {
		return communityName;
	}
	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}
	
	@XmlElement(name="priviledge",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getPriviledge() {
		return priviledge;
	}
	public void setPriviledge(String priviledge) {
		this.priviledge = priviledge;
	}
	
	@XmlElement(name="v4acl-type",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getV4aclType() {
		return v4aclType;
	}
	public void setV4aclType(String v4aclType) {
		this.v4aclType = v4aclType;
	}
	
	@XmlElement(name="v4-access-list",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getV4accessList() {
		return v4accessList;
	}
	public void setV4accessList(String v4accessList) {
		this.v4accessList = v4accessList;
	}
	@Override
	public String toString() {
		return "DefaultCommunity [communityName=" + communityName
				+ ", priviledge=" + priviledge + ", v4aclType=" + v4aclType
				+ ", v4accessList=" + v4accessList + "]";
	}
	
	

}
