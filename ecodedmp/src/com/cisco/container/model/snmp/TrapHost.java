package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class TrapHost {

	private String ipaddress;
	private DefaultUserCommunities defaultUserCommunities;

	@XmlElement(name="ip-address",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getIpaddress() {
		return ipaddress;
	}

	public void setIpaddress(String ipaddress) {
		this.ipaddress = ipaddress;
	}

	@XmlElement(name="default-user-communities",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public DefaultUserCommunities getDefaultUserCommunities() {
		return defaultUserCommunities;
	}

	public void setDefaultUserCommunities(
			DefaultUserCommunities defaultUserCommunities) {
		this.defaultUserCommunities = defaultUserCommunities;
	}

	@Override
	public String toString() {
		return "TrapHost [ipaddress=" + ipaddress + ", defaultUserCommunities="
				+ defaultUserCommunities + "]";
	}

}
