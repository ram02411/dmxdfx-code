package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Bgp4Mib {
	
     private String enable;
     
    @XmlElement(name="enable",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "CiscoBgp4Mib [enable=" + enable + "]";
	}
     
	
}
