package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Bgp {

	private CiscoBgp4Mib ciscoBgp4Mib;
	private Bgp4Mib bgp4Mib;

    @XmlElement(name="cisco-bgp4mib",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public CiscoBgp4Mib getCiscoBgp4Mib() {
		return ciscoBgp4Mib;
	}

	public void setCiscoBgp4Mib(CiscoBgp4Mib ciscoBgp4Mib) {
		this.ciscoBgp4Mib = ciscoBgp4Mib;
	}

    @XmlElement(name="bgp4mib",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-ipv4-bgp-cfg")
	public Bgp4Mib getBgp4Mib() {
		return bgp4Mib;
	}

	public void setBgp4Mib(Bgp4Mib bgp4Mib) {
		this.bgp4Mib = bgp4Mib;
	}

	@Override
	public String toString() {
		return "Bgp [ciscoBgp4Mib=" + ciscoBgp4Mib + ", bgp4Mib=" + bgp4Mib
				+ "]";
	}

}
