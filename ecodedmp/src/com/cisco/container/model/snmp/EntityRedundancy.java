package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class EntityRedundancy {

	private String enable;
	private String status;
	private String switchover;

	@XmlElement(name="enable",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-ceredundancymib-cfg")
	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	@XmlElement(name="status",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-ceredundancymib-cfg")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@XmlElement(name="switchover",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-ceredundancymib-cfg")
	public String getSwitchover() {
		return switchover;
	}

	public void setSwitchover(String switchover) {
		this.switchover = switchover;
	}

	@Override
	public String toString() {
		return "EntityRedundancy [enable=" + enable + ", status=" + status
				+ ", switchover=" + switchover + "]";
	}

}
