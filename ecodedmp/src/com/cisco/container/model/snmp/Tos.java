package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Tos {
	private String type;
	private String dscp;

	@XmlElement(name="type",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name="dscp",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getDscp() {
		return dscp;
	}

	public void setDscp(String dscp) {
		this.dscp = dscp;
	}

	@Override
	public String toString() {
		return "Tos [type=" + type + ", dscp=" + dscp + "]";
	}

}
