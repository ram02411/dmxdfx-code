package com.cisco.container.model.snmp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Snmp {
	private String trapSource;
	private Administration administration;
	private Trap trap;
	private Notification notification;
	private Ipv4 ipv4;
	private TrapHosts trapHosts;

	@XmlElement(name="trap-source",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public String getTrapSource() {
		return trapSource;
	}

	public void setTrapSource(String trapSource) {
		this.trapSource = trapSource;
	}

	@XmlElement(name="administration",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Administration getAdministration() {
		return administration;
	}

	public void setAdministration(Administration administration) {
		this.administration = administration;
	}

	@XmlElement(name="trap",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Trap getTrap() {
		return trap;
	}

	public void setTrap(Trap trap) {
		this.trap = trap;
	}

	@XmlElement(name="notification",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Notification getNotification() {
		return notification;
	}

	public void setNotification(Notification notification) {
		this.notification = notification;
	}

	@XmlElement(name="ipv4",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public Ipv4 getIpv4() {
		return ipv4;
	}

	public void setIpv4(Ipv4 ipv4) {
		this.ipv4 = ipv4;
	}

	@XmlElement(name="trap-hosts",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-agent-cfg")
	public TrapHosts getTrapHosts() {
		return trapHosts;
	}

	public void setTrapHosts(TrapHosts trapHosts) {
		this.trapHosts = trapHosts;
	}

	@Override
	public String toString() {
		return "Snmp [trapSource=" + trapSource + ", administration="
				+ administration + ", trap=" + trap + ", notification="
				+ notification + ", ipv4=" + ipv4 + ", trapHosts=" + trapHosts
				+ "]";
	}

}
