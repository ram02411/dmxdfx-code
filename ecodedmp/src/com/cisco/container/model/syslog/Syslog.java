package com.cisco.container.model.syslog;

import javax.xml.bind.annotation.XmlElement;

public class Syslog {

	private ConsoleLogging consoleLogging;
	private MonitorLogging monitorLogging;

	@XmlElement(name="console-logging",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-syslog-cfg")
	public ConsoleLogging getConsoleLogging() {
		return consoleLogging;
	}

	public void setConsoleLogging(ConsoleLogging consoleLogging) {
		this.consoleLogging = consoleLogging;
	}

	@XmlElement(name="monitor-logging",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-syslog-cfg")
	public MonitorLogging getMonitorLogging() {
		return monitorLogging;
	}

	public void setMonitorLogging(MonitorLogging monitorLogging) {
		this.monitorLogging = monitorLogging;
	}

	@Override
	public String toString() {
		return "Syslog [consoleLogging=" + consoleLogging + ", monitorLogging="
				+ monitorLogging + "]";
	}

}
