package com.cisco.container.model.syslog;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class MonitorLogging {
	
	private String loggingLevel;

	
	@XmlElement(name="logging-level",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-syslog-cfg")
	public String getLoggingLevel() {
		return loggingLevel;
	}

	public void setLoggingLevel(String loggingLevel) {
		this.loggingLevel = loggingLevel;
	}

	@Override
	public String toString() {
		return "MonitorLogging [loggingLevel=" + loggingLevel + "]";
	}
	

}
