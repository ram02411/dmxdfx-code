package com.cisco.container.model.mib;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceMib {

	private String interfaceAliasLong;

	@XmlElement(name="interface-alias-long",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-ifmib-cfg")
	public String getInterfaceAliasLong() {
		return interfaceAliasLong;
	}

	public void setInterfaceAliasLong(String interfaceAliasLong) {
		this.interfaceAliasLong = interfaceAliasLong;
	}

	@Override
	public String toString() {
		return "InterfaceMib [interfaceAliasLong=" + interfaceAliasLong + "]";
	}

}
