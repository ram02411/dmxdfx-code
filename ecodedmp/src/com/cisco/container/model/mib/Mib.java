package com.cisco.container.model.mib;

import javax.xml.bind.annotation.XmlElement;

public class Mib {
	
	private InterfaceMib interfaceMib;

	@XmlElement(name="interface-mib",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-snmp-ifmib-cfg")
	public InterfaceMib getInterfaceMib() {
		return interfaceMib;
	}

	public void setInterfaceMib(InterfaceMib interfaceMib) {
		this.interfaceMib = interfaceMib;
	}

	@Override
	public String toString() {
		return "Mib [interfaceMib=" + interfaceMib + "]";
	}
	
	

}
