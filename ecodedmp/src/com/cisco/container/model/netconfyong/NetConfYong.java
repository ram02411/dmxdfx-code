package com.cisco.container.model.netconfyong;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 * 
 */
public class NetConfYong {
	
	private Agent agent;

	@XmlElement(name="agent",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-man-netconf-cfg")
	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	@Override
	public String toString() {
		return "NetConfYong [agent=" + agent + "]";
	}

}
