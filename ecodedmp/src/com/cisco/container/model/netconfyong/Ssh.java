package com.cisco.container.model.netconfyong;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Ssh {
	
     private String enable;

     @XmlElement(name="enable",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-man-netconf-cfg")
	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Ssh [enable=" + enable + "]";
	}
     
     
}
