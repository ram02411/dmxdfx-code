package com.cisco.container.model.netconfyong;

import javax.xml.bind.annotation.XmlElement;

public class Agent {
	private NetSsh netSsh;

    @XmlElement(name="ssh",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-man-netconf-cfg")
	public NetSsh getNetSsh() {
		return netSsh;
	}

	public void setNetSsh(NetSsh netSsh) {
		this.netSsh = netSsh;
	}
	
	@Override
	public String toString() {
		return "Agent [netSsh=" + netSsh + "]";
	}

}
