package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

public class TtyLine {
	private String name;
	private Aaa aaa;
	private Exe exec;
	private General general;
	private Connection connection;

	
	@XmlElement(name="name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="aaa",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public Aaa getAaa() {
		return aaa;
	}

	public void setAaa(Aaa aaa) {
		this.aaa = aaa;
	}

	
	@XmlElement(name="exec",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public Exe getExec() {
		return exec;
	}

	public void setExec(Exe exec) {
		this.exec = exec;
	}

	
	@XmlElement(name="general",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public General getGeneral() {
		return general;
	}

	public void setGeneral(General general) {
		this.general = general;
	}

	
	@XmlElement(name="connection",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")
	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Override
	public String toString() {
		return "TtyLine [name=" + name + ", aaa=" + aaa + ", exec=" + exec
				+ ", general=" + general + ", connection=" + connection + "]";
	}

}
