package com.cisco.container.model.tty;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class TtyLines {
	
	private List<TtyLine> ttyLine;

	@XmlElement(name="tty-line",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public List<TtyLine> getTtyLine() {
		return ttyLine;
	}

	public void setTtyLine(List<TtyLine> ttyLine) {
		this.ttyLine = ttyLine;
	}
	
	

}
