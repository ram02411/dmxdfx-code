package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Exe {
	private String timestamp;
	private Timeout timeout;

	@XmlElement(name="time-stamp",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@XmlElement(name="timeout",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public Timeout getTimeout() {
		return timeout;
	}

	public void setTimeout(Timeout timeout) {
		this.timeout = timeout;
	}

	@Override
	public String toString() {
		return "Exe [timestamp=" + timestamp + ", timeout=" + timeout + "]";
	}

}
