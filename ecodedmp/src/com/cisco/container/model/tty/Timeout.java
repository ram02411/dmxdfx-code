package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Timeout {
	
	private int minutes;
	private int seconds;
	
	@XmlElement(name="minutes",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public int getMinutes() {
		return minutes;
	}
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	
	@XmlElement(name="seconds",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public int getSeconds() {
		return seconds;
	}
	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}
	@Override
	public String toString() {
		return "Timeout [minutes=" + minutes + ", seconds=" + seconds + "]";
	}
	
	
}
