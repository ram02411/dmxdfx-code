package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

public class TransportInput {
	
	private String select;
	private String protocol1;
	private String protocol2;
	
	@XmlElement(name="select",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getSelect() {
		return select;
	}
	public void setSelect(String select) {
		this.select = select;
	}
	
	@XmlElement(name="protocol1",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getProtocol1() {
		return protocol1;
	}
	public void setProtocol1(String protocol1) {
		this.protocol1 = protocol1;
	}
	
	@XmlElement(name="protocol2",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getProtocol2() {
		return protocol2;
	}
	public void setProtocol2(String protocol2) {
		this.protocol2 = protocol2;
	}
	@Override
	public String toString() {
		return "TransportInput [select=" + select + ", protocol1=" + protocol1
				+ ", protocol2=" + protocol2 + "]";
	}
}
