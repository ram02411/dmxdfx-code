package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

public class Tty {
	
	private TtyLines ttyLines;

	@XmlElement(name="tty-lines",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public TtyLines getTtyLines() {
		return ttyLines;
	}

	public void setTtyLines(TtyLines ttyLines) {
		this.ttyLines = ttyLines;
	}

	@Override
	public String toString() {
		return "Tty [ttyLines=" + ttyLines + "]";
	}
	
	

}
