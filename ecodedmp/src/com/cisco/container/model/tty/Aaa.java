package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

public class Aaa {
	
	private String password;

	@XmlElement(name="password",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-server-cfg")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Aaa [password=" + password + "]";
	}
	

}
