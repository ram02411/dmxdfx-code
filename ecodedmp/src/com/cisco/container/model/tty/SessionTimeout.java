package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

public class SessionTimeout {
	
	private String timeout;
	private String direction;
	
	
	@XmlElement(name="timeout",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getTimeout() {
		return timeout;
	}
	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}
	
	@XmlElement(name="direction",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	
}
