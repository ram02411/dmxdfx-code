package com.cisco.container.model.tty;

import javax.xml.bind.annotation.XmlElement;

public class Connection {
	private String aclIn;
	private String sessionLimit;
	private SessionTimeout sessionTimeout;
	private TransportInput transportInput;

	@XmlElement(name="acl-in",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getAclIn() {
		return aclIn;
	}

	public void setAclIn(String aclIn) {
		this.aclIn = aclIn;
	}

	@XmlElement(name="session-limit",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public String getSessionLimit() {
		return sessionLimit;
	}

	public void setSessionLimit(String sessionLimit) {
		this.sessionLimit = sessionLimit;
	}

	@XmlElement(name="session-timeout",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public SessionTimeout getSessionTimeout() {
		return sessionTimeout;
	}

	public void setSessionTimeout(SessionTimeout sessionTimeout) {
		this.sessionTimeout = sessionTimeout;
	}

	@XmlElement(name="transport-input",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-tty-management-cfg")	
	public TransportInput getTransportInput() {
		return transportInput;
	}

	public void setTransportInput(TransportInput transportInput) {
		this.transportInput = transportInput;
	}

	@Override
	public String toString() {
		return "Connection [aclIn=" + aclIn + ", sessionLimit=" + sessionLimit
				+ ", sessionTimeout=" + sessionTimeout + ", transportInput="
				+ transportInput + "]";
	}

}
