package com.cisco.container.model.tty;

public class General {
	private int length;

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	@Override
	public String toString() {
		return "General [length=" + length + "]";
	}

}
