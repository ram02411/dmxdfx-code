package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class YangBgp {

	private YangBgpGlobal global;
	private PeerGroups peerGroups;
	private Neighbors neighbors;

	@XmlElement(name = "global", namespace = "http://openconfig.net/yang/bgp")
	public YangBgpGlobal getGlobal() {
		return global;
	}

	public void setGlobal(YangBgpGlobal global) {
		this.global = global;
	}

	@XmlElement(name = "peer-groups", namespace = "http://openconfig.net/yang/bgp")
	public PeerGroups getPeerGroups() {
		return peerGroups;
	}

	public void setPeerGroups(PeerGroups peerGroups) {
		this.peerGroups = peerGroups;
	}

	@XmlElement(name = "neighbors", namespace = "http://openconfig.net/yang/bgp")
	public Neighbors getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(Neighbors neighbors) {
		this.neighbors = neighbors;
	}

	@Override
	public String toString() {
		return "YangBgp [global=" + global + ", peerGroups=" + peerGroups
				+ ", neighbors=" + neighbors + "]";
	}

}
