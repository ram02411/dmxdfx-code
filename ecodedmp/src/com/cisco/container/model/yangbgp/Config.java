package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Config {
	private String peerGroup;
	private String description;
	
	@XmlElement(name = "peer-group", namespace = "http://openconfig.net/yang/bgp")
	public String getPeerGroup() {
		return peerGroup;
	}

	public void setPeerGroup(String peerGroup) {
		this.peerGroup = peerGroup;
	}

	@XmlElement(name = "description", namespace = "http://openconfig.net/yang/bgp")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Config [peerGroup=" + peerGroup + ", description="
				+ description + "]";
	}

}
