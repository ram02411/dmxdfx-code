package com.cisco.container.model.yangbgp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class PeerGroups {

	private List<PeerGroup> peerGroup;
	
	@XmlElement(name = "peer-group", namespace = "http://openconfig.net/yang/bgp")
	public List<PeerGroup> getPeerGroup() {
		return peerGroup;
	}

	public void setPeerGroup(List<PeerGroup> peerGroup) {
		this.peerGroup = peerGroup;
	}

	@Override
	public String toString() {
		return "PeerGroups [peerGroup=" + peerGroup + "]";
	}

}
