package com.cisco.container.model.yangbgp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class AfiSafis {
	private List<AfiSafi> afiSafi;

	@XmlElement(name = "afi-safi", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public List<AfiSafi> getAfiSafi() {
		return afiSafi;
	}

	public void setAfiSafi(List<AfiSafi> afiSafi) {
		this.afiSafi = afiSafi;
	}

	@Override
	public String toString() {
		return "AfiSafis [afiSafi=" + afiSafi + "]";
	}

}
