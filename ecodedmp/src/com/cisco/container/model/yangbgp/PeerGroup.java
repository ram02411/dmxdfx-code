package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

public class PeerGroup {

	private String peerGroupName;
	private PeerGroupConfig config;
	private String gracefulRestart;
	private AfiSafi afiSafi;

	@XmlElement(name = "peer-group-name", namespace = "http://openconfig.net/yang/bgp")
	public String getPeerGroupName() {
		return peerGroupName;
	}

	public void setPeerGroupName(String peerGroupName) {
		this.peerGroupName = peerGroupName;
	}

	@XmlElement(name = "config", namespace = "http://openconfig.net/yang/bgp")
	public PeerGroupConfig getConfig() {
		return config;
	}

	public void setConfig(PeerGroupConfig config) {
		this.config = config;
	}

	@XmlElement(name = "graceful-restart", namespace = "http://openconfig.net/yang/bgp")
	public String getGracefulRestart() {
		return gracefulRestart;
	}

	public void setGracefulRestart(String gracefulRestart) {
		this.gracefulRestart = gracefulRestart;
	}

	@XmlElement(name = "afi-safi", namespace = "http://openconfig.net/yang/bgp")
	public AfiSafi getAfiSafi() {
		return afiSafi;
	}

	public void setAfiSafi(AfiSafi afiSafi) {
		this.afiSafi = afiSafi;
	}

	@Override
	public String toString() {
		return "PeerGroup [peerGroupName=" + peerGroupName + ", config="
				+ config + ", gracefulRestart=" + gracefulRestart
				+ ", afiSafi=" + afiSafi + "]";
	}

}
