package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Neighbor {
	private String neighborAddress;
	private Config config;
	
	@XmlElement(name = "neighbor-address", namespace = "http://openconfig.net/yang/bgp")
	public String getNeighborAddress() {
		return neighborAddress;
	}

	public void setNeighborAddress(String neighborAddress) {
		this.neighborAddress = neighborAddress;
	}

	@XmlElement(name = "config", namespace = "http://openconfig.net/yang/bgp")
	public Config getConfig() {
		return config;
	}
	public void setConfig(Config config) {
		this.config = config;
	}



	@Override
	public String toString() {
		return "Neighbor [neighborAddress=" + neighborAddress + ", config="
				+ config + "]";
	}
	
	
}
