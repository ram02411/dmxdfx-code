package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

public class AfiSafi {

	private String afisafiName;
	private AfiSafiConfig config;
	private ApplyPolicy applyPolicy;

	
	@XmlElement(name = "afi-safi-name", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public String getAfisafiName() {
		return afisafiName;
	}

	public void setAfisafiName(String afisafiName) {
		this.afisafiName = afisafiName;
	}

	@XmlElement(name = "config", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public AfiSafiConfig getConfig() {
		return config;
	}

	public void setConfig(AfiSafiConfig config) {
		this.config = config;
	}

	@XmlElement(name = "apply-policy", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public ApplyPolicy getApplyPolicy() {
		return applyPolicy;
	}

	public void setApplyPolicy(ApplyPolicy applyPolicy) {
		this.applyPolicy = applyPolicy;
	}

	@Override
	public String toString() {
		return "AfiSafi [afisafiName=" + afisafiName + ", config=" + config
				+ ", applyPolicy=" + applyPolicy + "]";
	}

}
