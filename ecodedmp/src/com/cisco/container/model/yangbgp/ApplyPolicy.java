package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class ApplyPolicy {
	
	private String exportPolicies;

	
	@XmlElement(name = "export-policies", namespace = "http://openconfig.net/yang/routing-policy")
	public String getExportPolicies() {
		return exportPolicies;
	}

	public void setExportPolicies(String exportPolicies) {
		this.exportPolicies = exportPolicies;
	}

	@Override
	public String toString() {
		return "ApplyPolicy [exportPolicies=" + exportPolicies + "]";
	}
	
	
}
