package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

public class PeerGroupConfig {
	private String peerAs;
	private String authPassword;

	@XmlElement(name = "peer-as", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public String getPeerAs() {
		return peerAs;
	}

	
	public void setPeerAs(String peerAs) {
		this.peerAs = peerAs;
	}

	@XmlElement(name = "auth-password", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public String getAuthPassword() {
		return authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}

	@Override
	public String toString() {
		return "PeerGroupConfig [peerAs=" + peerAs + ", authPassword="
				+ authPassword + "]";
	}

}
