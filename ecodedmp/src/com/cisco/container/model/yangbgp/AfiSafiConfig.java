package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

public class AfiSafiConfig {
	
	private String enabled;
	
	@XmlElement(name = "enabled", namespace = "http://openconfig.net/yang/bgp-multiprotocol")
	public String getEnabled() {
		return enabled;
	}

	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}

	@Override
	public String toString() {
		return "AfiSafiConfig [enabled=" + enabled + "]";
	}

}
