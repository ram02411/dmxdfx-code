package com.cisco.container.model.yangbgp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Neighbors {
	
	private List<Neighbor> neighbor;

	@XmlElement(name = "neighbor", namespace = "http://openconfig.net/yang/bgp")
	public List<Neighbor> getNeighbor() {
		return neighbor;
	}

	public void setNeighbor(List<Neighbor> neighbor) {
		this.neighbor = neighbor;
	}

	@Override
	public String toString() {
		return "Neighbors [neighbor=" + neighbor + "]";
	}
	
	
	
}
