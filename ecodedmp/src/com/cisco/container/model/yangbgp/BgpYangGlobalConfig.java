package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

public class BgpYangGlobalConfig {
	private String as;
	private String routerId;

	@XmlElement(name = "as", namespace = "http://openconfig.net/yang/bgp")
	public String getAs() {
		return as;
	}

	public void setAs(String as) {
		this.as = as;
	}

	@XmlElement(name = "router-Id", namespace = "http://openconfig.net/yang/bgp")
	public String getRouterId() {
		return routerId;
	}

	public void setRouterId(String routerId) {
		this.routerId = routerId;
	}

	@Override
	public String toString() {
		return "BgpYangGlobalConfig [as=" + as + ", routerId=" + routerId + "]";
	}

}
