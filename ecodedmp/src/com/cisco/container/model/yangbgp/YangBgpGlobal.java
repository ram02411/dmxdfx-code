package com.cisco.container.model.yangbgp;

import javax.xml.bind.annotation.XmlElement;

public class YangBgpGlobal {

	private BgpYangGlobalConfig config;
	private String gracefulRestart;
	private AfiSafis afiSafis;

	@XmlElement(name = "config", namespace = "http://openconfig.net/yang/bgp")
	public BgpYangGlobalConfig getConfig() {
		return config;
	}

	public void setConfig(BgpYangGlobalConfig config) {
		this.config = config;
	}
	
	
	@XmlElement(name = "graceful-restart", namespace = "http://openconfig.net/yang/bgp")
	public String getGracefulRestart() {
		return gracefulRestart;
	}

	public void setGracefulRestart(String gracefulRestart) {
		this.gracefulRestart = gracefulRestart;
	}

	@XmlElement(name = "afi-safi", namespace = "http://openconfig.net/yang/bgp")
	public AfiSafis getAfiSafis() {
		return afiSafis;
	}

	public void setAfiSafis(AfiSafis afiSafis) {
		this.afiSafis = afiSafis;
	}

	@Override
	public String toString() {
		return "YangBgpGlobal [config=" + config + ", gracefulRestart="
				+ gracefulRestart + ", afiSafis=" + afiSafis + "]";
	}

}
