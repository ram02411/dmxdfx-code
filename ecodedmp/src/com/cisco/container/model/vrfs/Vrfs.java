package com.cisco.container.model.vrfs;

import javax.xml.bind.annotation.XmlElement;

public class Vrfs {
	private Vrf vrf;

	@XmlElement(name="vrf",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public Vrf getVrf() {
		return vrf;
	}

	public void setVrf(Vrf vrf) {
		this.vrf = vrf;
	}

	@Override
	public String toString() {
		return "Vrfs [vrf=" + vrf + "]";
	}

}
