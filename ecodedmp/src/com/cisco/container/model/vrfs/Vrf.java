package com.cisco.container.model.vrfs;

import javax.xml.bind.annotation.XmlElement;

public class Vrf {
	private String vrfName;
	private String create;
	private Afs afs;

	@XmlElement(name="vrf-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public String getVrfName() {
		return vrfName;
	}

	public void setVrfName(String vrfName) {
		this.vrfName = vrfName;
	}

	@XmlElement(name="create",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public String getCreate() {
		return create;
	}

	public void setCreate(String create) {
		this.create = create;
	}

	@XmlElement(name="afs",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public Afs getAfs() {
		return afs;
	}

	public void setAfs(Afs afs) {
		this.afs = afs;
	}

	@Override
	public String toString() {
		return "Vrf [vrfName=" + vrfName + ", create=" + create + ", afs="
				+ afs + "]";
	}

}
