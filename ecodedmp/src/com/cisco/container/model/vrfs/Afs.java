package com.cisco.container.model.vrfs;

import javax.xml.bind.annotation.XmlElement;

public class Afs {

	private Af af;

	@XmlElement(name="af",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public Af getAf() {
		return af;
	}

	public void setAf(Af af) {
		this.af = af;
	}

	@Override
	public String toString() {
		return "Afs [af=" + af + "]";
	}

}
