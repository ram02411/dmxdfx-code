package com.cisco.container.model.vrfs;

import javax.xml.bind.annotation.XmlElement;

/**
 * 
 * @author ecode
 *
 */
public class Af {
	
	private String afName;
	private String safName;
	private String topologyName;
	private String create;
	
	@XmlElement(name="af-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public String getAfName() {
		return afName;
	}
	public void setAfName(String afName) {
		this.afName = afName;
	}
	
	@XmlElement(name="saf-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public String getSafName() {
		return safName;
	}
	public void setSafName(String safName) {
		this.safName = safName;
	}
	
	@XmlElement(name="topology-name",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public String getTopologyName() {
		return topologyName;
	}
	public void setTopologyName(String topologyName) {
		this.topologyName = topologyName;
	}
	
	@XmlElement(name="create",namespace="http://cisco.com/ns/yang/Cisco-IOS-XR-infra-rsi-cfg")
	public String getCreate() {
		return create;
	}
	public void setCreate(String create) {
		this.create = create;
	}
	@Override
	public String toString() {
		return "Af [afName=" + afName + ", safName=" + safName
				+ ", topologyName=" + topologyName + ", create=" + create + "]";
	}
	
	

}
