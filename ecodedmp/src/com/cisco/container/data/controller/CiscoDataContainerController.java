package com.cisco.container.data.controller;

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cisco.container.data.service.CiscoDataService;
import com.cisco.container.data.service.CiscoDataServiceImpl;
import com.cisco.container.model.CiscoConfigContainer;

@Controller
public class CiscoDataContainerController {
	
	private CiscoDataService ciscodataservice=new CiscoDataServiceImpl();
	
	@RequestMapping(value="/ciscoData",method=RequestMethod.GET)
	public @ResponseBody CiscoConfigContainer fetchDataModelForCisco(HttpSession session){
		ServletContext context=session.getServletContext();
		InputStream inputStream = context.getResourceAsStream("/xmldata/get-config_running_cisco.xml");
		CiscoConfigContainer ciscoconfigcontainer=ciscodataservice.findCiscoDataModel(inputStream);
		return ciscoconfigcontainer;
	}
	

}
