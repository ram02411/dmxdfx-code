package com.cisco.container.data.device;

import java.io.InputStream;

import com.cisco.container.model.CiscoConfigContainer;

public interface CiscoDataDevice {

	public CiscoConfigContainer findCiscoDataModel(InputStream fileInputStream);

	public CiscoConfigContainer findCiscoDataModel(String deviceName);

}
