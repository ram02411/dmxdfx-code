package com.cisco.container.data.device;

import java.io.InputStream;
import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.xml.sax.InputSource;

import com.cisco.container.model.CiscoConfigContainer;

/**
 * 
 * @author 305
 *
 */
public class CiscoDataDeviceImpl implements CiscoDataDevice{

	@Override
	public CiscoConfigContainer findCiscoDataModel(InputStream fileInputStream) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CiscoConfigContainer findCiscoDataModel(String deviceName) {
		String xmlReply=null;
		StringReader inStream = new StringReader(xmlReply);
		//because XML DOM parser understand InputSource
		InputSource isource = new InputSource(inStream);
		CiscoConfigContainer ciscoConfigContainer=null;
		  try {
				//File file = new File("get-config_running_cisco.xml");
				JAXBContext jaxbContext = JAXBContext.newInstance(CiscoConfigContainer.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			     ciscoConfigContainer = (CiscoConfigContainer) 
						jaxbUnmarshaller.unmarshal(isource);
			  } catch (JAXBException e) {
				e.printStackTrace();
			  }
		  return ciscoConfigContainer;
	}

}
