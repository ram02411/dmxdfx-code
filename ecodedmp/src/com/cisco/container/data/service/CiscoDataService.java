package com.cisco.container.data.service;

import java.io.InputStream;

import com.cisco.container.model.CiscoConfigContainer;

public interface CiscoDataService {

	public CiscoConfigContainer findCiscoDataModel(InputStream fileInputStream);

	public CiscoConfigContainer findCiscoDataModel(String xmlRpcData);

}
