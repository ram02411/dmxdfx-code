package com.cisco.container.data.service;

import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.cisco.container.model.CiscoConfigContainer;

public class CiscoDataServiceImpl implements CiscoDataService{
	
	 @Override
	  public CiscoConfigContainer findCiscoDataModel(InputStream fileInputStream){
		  CiscoConfigContainer ciscoConfigContainer=null;
		  try {
				//File file = new File("get-config_running_cisco.xml");
				JAXBContext jaxbContext = JAXBContext.newInstance(CiscoConfigContainer.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			     ciscoConfigContainer = (CiscoConfigContainer) 
						jaxbUnmarshaller.unmarshal(fileInputStream);
			  } catch (JAXBException e) {
				e.printStackTrace();
			  }
		  return ciscoConfigContainer;
	  }
	 
	 @Override
	  public CiscoConfigContainer findCiscoDataModel(String xmlRpcData){
		  CiscoConfigContainer ciscoConfigContainer=null;
		  return ciscoConfigContainer;
	  }


}
