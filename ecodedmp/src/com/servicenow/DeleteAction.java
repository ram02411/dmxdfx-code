package com.servicenow;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
 
public class DeleteAction {
	public static void main(String[] args) throws IOException, HttpException {
 		DeleteAction restAction = new DeleteAction();
 		restAction.deleteRequest();
 	}
 
 	public void deleteRequest() throws HttpException, IOException {
 	// This must be valid json string with valid fields and values from table
 
 	 		CredentialsProvider credsProvider = new BasicCredentialsProvider();
 	        credsProvider.setCredentials(
 	                new AuthScope(new HttpHost("dev18188.service-now.com")),
 	                new UsernamePasswordCredentials("admin", "ecode123"));
 	        CloseableHttpClient httpclient = HttpClients.custom()
 	                .setDefaultCredentialsProvider(credsProvider)
 	                .build();
 
        try {
        	HttpDelete httpDelete = new HttpDelete("https://dev18188.service-now.com/api/now/table/u_bgp_profiles/81f2ff80dbb42200f45672ffbf961955");
        	httpDelete.setHeader("Accept", "application/json");
 
            System.out.println("Executing request " + httpDelete.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httpDelete);
            try {
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
 	}
}