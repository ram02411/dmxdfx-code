package com.servicenow;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class GetProfiles {
	public static void main(String[] args) throws IOException, HttpException {
		String data=GetProfiles.getprofiles();
		System.out.println(data);
 	}
	
	
	public static String getprofiles() throws HttpException, IOException {
 		CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(new HttpHost("dev18188.service-now.com")),
                new UsernamePasswordCredentials("admin", "ecode123"));
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build();
        String responseBody ="";
        try {
            HttpGet httpget = new HttpGet("https://dev18188.service-now.com/api/now/table/x_67008_servicepro_serviceprofiles");
            httpget.setHeader("Accept", "application/json");
            System.out.println("Executing request " + httpget.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httpget);
            
            try {
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
                responseBody = EntityUtils.toString(response.getEntity());
                System.out.println(responseBody);
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
        return responseBody;
 	}
 
}
