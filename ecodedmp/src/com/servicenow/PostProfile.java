package com.servicenow;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

public class PostProfile {

 	public static void postRequest(String postData) throws HttpException, IOException {
 		
 		
 		// This must be valid json string with valid fields and values from table
 	 		CredentialsProvider credsProvider = new BasicCredentialsProvider();
 	        credsProvider.setCredentials(
 	                new AuthScope(new HttpHost("dev18188.service-now.com")),
 	                new UsernamePasswordCredentials("admin", "ecode123"));
 	        CloseableHttpClient httpclient = HttpClients.custom()
 	                .setDefaultCredentialsProvider(credsProvider)
 	                .build();
 
        try {
        	HttpPost httpPost = new HttpPost("https://dev18188.service-now.com/api/now/table/x_67008_servicepro_serviceprofiles");
 	 		httpPost.setHeader("Accept", "application/json");
 	 		httpPost.setHeader("Content-Type", "application/json");
 	        HttpEntity entity = new ByteArrayEntity(postData.getBytes("utf-8"));
 	 		httpPost.setEntity(entity);
 
            System.out.println("Executing request " + httpPost.getRequestLine());
            CloseableHttpResponse response = httpclient.execute(httpPost);
            try {
                System.out.println("----------------------------------------");
                System.out.println(response.getStatusLine());
                String responseBody = EntityUtils.toString(response.getEntity());
                System.out.println(responseBody);
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
 	}

}
