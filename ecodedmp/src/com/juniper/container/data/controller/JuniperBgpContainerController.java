package com.juniper.container.data.controller;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecode.topology.viewer.data.device.Router;
import com.ecode.web.app.constant.ApplicationConstant;
import com.ecode.web.app.constant.ApplicationMessage;
import com.ecode.web.app.util.AppMessageVO;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;
import com.juniper.container.data.model.configuration.protocols.BgpGroup;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.juniper.container.data.model.configuration.protocols.Neighbor;
import com.juniper.container.data.service.JuniperBgpService;

@Controller
public class JuniperBgpContainerController {
	
	
	@Autowired
	@Qualifier("JuniperBgpServiceImpl")
	private JuniperBgpService juniperBgpService;
	
	/**
	 * 
	 * @param session
	 * @return
	 *//*
	@RequestMapping(value="/deviceCpuUsage",method=RequestMethod.GET)
	public @ResponseBody CPUMemoryUsageVO findDeviceCpuUsage(){
		CPUMemoryUsageVO CpuUsageVO=juniperDataService.fetchDeviceCPUUsage(null);
		return CpuUsageVO;
	}
	
	*//**
	 * 
	 * @param session
	 * @return
	 *//*
	@RequestMapping(value="/deviceMemoryUsage",method=RequestMethod.GET)
	public @ResponseBody RamMemoryUsageVO findDeviceMemoryUsage(){
		RamMemoryUsageVO ramMemoryUsageVO=juniperDataService.fetchDeviceMemoryUsage(null);
		return ramMemoryUsageVO;
	}
	*/
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/cpTraceOptions",method=RequestMethod.POST)
	public @ResponseBody AppMessageVO provisionCpTraceOptions(@ModelAttribute CProtocols cProtocols){
		System.out.println(cProtocols);
		String result=juniperBgpService.provisionTraceOptions(cProtocols,ApplicationConstant.MERGE_LOAD_TYPE,null);
		AppMessageVO appMessageVO=new AppMessageVO();
		if(ApplicationConstant.SUCCESS_STATUS.equalsIgnoreCase(result)){
			appMessageVO.setMessage(ApplicationMessage.DMP_DONE_SUCCESSFULLY);
			appMessageVO.setScode(ApplicationConstant.TWO_HUNDRED);
			appMessageVO.setStatus(ApplicationConstant.SUCCESS_STATUS);
		}else{
			appMessageVO.setMessage(result);
			appMessageVO.setScode(ApplicationConstant.FIVE_HUNDRED);
			appMessageVO.setStatus(ApplicationConstant.FAILED_STATUS);
		}
		return appMessageVO;
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/bgpContainerProvision",method=RequestMethod.POST)
	public @ResponseBody AppMessageVO provisionCpBgpGroups(@ModelAttribute CProtocols cProtocols){
		System.out.println(cProtocols);
		String hostname="";
		String possiblePathValue="";
		if(cProtocols.getBgp()!=null){
			if(cProtocols.getBgp()!=null)
			cProtocols.getBgp().setAuthenticationkey(null);
			if(cProtocols.getBgp().getBgpGroup()!=null && cProtocols.getBgp().getBgpGroup().size()>0){
				BgpGroup bgpGroup =cProtocols.getBgp().getBgpGroup().get(0);
				String texports=bgpGroup.getTexport();
				if(texports!=null && texports.trim().length()>0){
					String[] stexports=texports.split(",");
					List<String> exports=Arrays.asList(stexports);
					bgpGroup.setExport(exports);
				}
				String timports=bgpGroup.getTimport();
				if(timports!=null && timports.trim().length()>0){
					String[] stimports=timports.split(",");
					List<String> imports=Arrays.asList(stimports);
					bgpGroup.setIimport(imports);
				}
				bgpGroup.setTexport(null);
				bgpGroup.setTimport(null);
				//String type=bgpGroup.getType();
				//if("internal".equalsIgnoreCase(type)){
					bgpGroup.setLocalasnumber(null);
				bgpGroup.setPeerasnumber(null);
				//}else{
					bgpGroup.setAsnumber(null);
				//}
			    hostname=bgpGroup.getHostname();
			    System.out.println("____HOSTNAME_____IN BGP ="+hostname);
			    bgpGroup.setHostname(null);
				
				List<Neighbor> neighbors=cProtocols.getBgp().getBgpGroup().get(0).getNeighbor();
				if(neighbors!=null) {
					Iterator<Neighbor> nt=neighbors.iterator();
					while(nt.hasNext()){
						Neighbor pt=nt.next();
						if(pt==null || pt.getName()==null|| pt.getName().length()==0){
							nt.remove();
						}
					}
				}	
				possiblePathValue=bgpGroup.getPossiblePathValue();
				bgpGroup.setPossiblePathValue(null);
		   } //end of group if
		}
		String result="";
		
		if(possiblePathValue==null || possiblePathValue.length()==0) {
		result=juniperBgpService.provisionTraceOptions(cProtocols,ApplicationConstant.MERGE_LOAD_TYPE,hostname);
		}
		else {
			 //logic for vpn provision
			String groupName="";
			if(cProtocols.getBgp().getBgpGroup()!=null && cProtocols.getBgp().getBgpGroup().size()>0){
				BgpGroup bgpGroup =cProtocols.getBgp().getBgpGroup().get(0);
				groupName=bgpGroup.getName();
			}
			 
			result=juniperBgpService.vpnSetupForL3Topology(cProtocols,ApplicationConstant.MERGE_LOAD_TYPE,hostname,possiblePathValue);
			String possiblePathValueTokens[]=possiblePathValue.split(",");
			//int count=1;
			//2
			int nolr=possiblePathValueTokens.length;
			//nolr=nolr-1;
			for(int tp=0;tp<nolr;tp++){
				// String hostNameIpTokens[]=hostip.split("(");
				//find the remote address of server
				if(tp<nolr-1) {
					String hostip=possiblePathValueTokens[tp+1];
					 int p=hostip.indexOf("(");
					 String hostName=hostip.substring(0,p).trim();
					 Router remoterouter=ApplicationRouterConfigurationUtil.findRouterByHostname(hostName);
					 cProtocols.getBgp().getBgpGroup().get(0).getNeighbor().get(0).setName(remoterouter.getIp());
				}else{
					String hostip=possiblePathValueTokens[tp-1];
					 int p=hostip.indexOf("(");
					 String hostName=hostip.substring(0,p).trim();
					 Router remoterouter=ApplicationRouterConfigurationUtil.findRouterByHostname(hostName);
					 cProtocols.getBgp().getBgpGroup().get(0).getNeighbor().get(0).setName(remoterouter.getIp());
				}
			 	String hostip=possiblePathValueTokens[tp];
				 int p=hostip.indexOf("(");
				 String hostName=hostip.substring(0,p).trim();
				 Router router=ApplicationRouterConfigurationUtil.findRouterByHostname(hostName);
				 String phostname=router.getIp()+"_"+router.getUsername()+"_"+router.getPassword()+"_"+router.getHostname();
				 if(groupName.length()>0){
					// cProtocols.getBgp().getBgpGroup().get(0).setName(groupName+count);
					 cProtocols.getBgp().getBgpGroup().get(0).setLocaladdress(router.getIp());
				 }
				 result=juniperBgpService.provisionTraceOptions(cProtocols,ApplicationConstant.MERGE_LOAD_TYPE,phostname);
				// count++; 
			}
			 AppMessageVO appMessageVO=new AppMessageVO();
				if(ApplicationConstant.SUCCESS_STATUS.equalsIgnoreCase(result)){
					appMessageVO.setMessage(ApplicationMessage.VPN_PROVISION_SUCCESSFULLY);
					appMessageVO.setScode(ApplicationConstant.TWO_HUNDRED);
					appMessageVO.setStatus(ApplicationConstant.SUCCESS_STATUS);
				}else{
					appMessageVO.setMessage(result);
					appMessageVO.setScode(ApplicationConstant.FIVE_HUNDRED);
					appMessageVO.setStatus(ApplicationConstant.FAILED_STATUS);
				}
				return appMessageVO;
		}
		
		AppMessageVO appMessageVO=new AppMessageVO();
		if(ApplicationConstant.SUCCESS_STATUS.equalsIgnoreCase(result)){
			appMessageVO.setMessage(ApplicationMessage.DMP_DONE_SUCCESSFULLY);
			appMessageVO.setScode(ApplicationConstant.TWO_HUNDRED);
			appMessageVO.setStatus(ApplicationConstant.SUCCESS_STATUS);
		}else{
			appMessageVO.setMessage(result);
			appMessageVO.setScode(ApplicationConstant.FIVE_HUNDRED);
			appMessageVO.setStatus(ApplicationConstant.FAILED_STATUS);
		}
		return appMessageVO;
	}

}
