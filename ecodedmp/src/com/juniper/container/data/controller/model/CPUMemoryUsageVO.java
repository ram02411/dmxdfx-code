package com.juniper.container.data.controller.model;
		/**
		 * 
		 * @author 305
		 *
		 */

		public class CPUMemoryUsageVO {
			private String freecpu="0";
			private String usedcpu="0";



			public String getFreecpu() {
				return freecpu;
			}

			public void setFreecpu(String freecpu) {
				this.freecpu = freecpu;
			}

			public String getUsedcpu() {
				return usedcpu;
			}

			public void setUsedcpu(String usedcpu) {
				this.usedcpu = usedcpu;
			}

			@Override
			public String toString() {
				return "CPUcpuUsageVO [freecpu=" + freecpu + ", usedcpu=" + usedcpu + "]";
			
			}
			

		}