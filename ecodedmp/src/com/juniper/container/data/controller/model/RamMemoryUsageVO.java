package com.juniper.container.data.controller.model;

/**
 * 
 * @author 305
 *
 */
public class RamMemoryUsageVO {

	private String freememory="0";
	private String usedmemory="0";
	
	private String freecpu="0";
	private String usedcpu="0";


	

	public String getFreememory() {
		return freememory;
	}

	public void setFreememory(String freememory) {
		this.freememory = freememory;
	}

	public String getUsedmemory() {
		return usedmemory;
	}

	public void setUsedmemory(String usedmemory) {
		this.usedmemory = usedmemory;
	}
	
	

	public String getFreecpu() {
		return freecpu;
	}

	public void setFreecpu(String freecpu) {
		this.freecpu = freecpu;
	}

	public String getUsedcpu() {
		return usedcpu;
	}

	public void setUsedcpu(String usedcpu) {
		this.usedcpu = usedcpu;
	}

	@Override
	public String toString() {
		return "RamMemoryUsageVO [freememory=" + freememory + ", usedmemory=" + usedmemory + ", freecpu=" + freecpu
				+ ", usedcpu=" + usedcpu + "]";
	}

	
	

}
