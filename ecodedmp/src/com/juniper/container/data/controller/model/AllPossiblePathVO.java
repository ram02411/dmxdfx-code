package com.juniper.container.data.controller.model;

import java.util.List;

public class AllPossiblePathVO {
	private String possiblePath;
	private List<String> routersList;

	public String getPossiblePath() {
		return possiblePath;
	}

	public void setPossiblePath(String possiblePath) {
		this.possiblePath = possiblePath;
	}

	public List<String> getRoutersList() {
		return routersList;
	}

	public void setRoutersList(List<String> routersList) {
		this.routersList = routersList;
	}

	@Override
	public String toString() {
		return "AllPossiblePathVO [possiblePath=" + possiblePath + ", routersList=" + routersList + "]";
	}

}
