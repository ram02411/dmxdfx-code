package com.juniper.container.data.controller.model;

public class PathFinderVO {

	private String sourceRouter;
	private String destRouter;
	private String topoType;
	

	public String getTopoType() {
		return topoType;
	}

	public void setTopoType(String topoType) {
		this.topoType = topoType;
	}

	public String getSourceRouter() {
		return sourceRouter;
	}

	public void setSourceRouter(String sourceRouter) {
		this.sourceRouter = sourceRouter;
	}

	public String getDestRouter() {
		return destRouter;
	}

	public void setDestRouter(String destRouter) {
		this.destRouter = destRouter;
	}

	@Override
	public String toString() {
		return "PathFinderVO [sourceRouter=" + sourceRouter + ", destRouter=" + destRouter + ", topoType=" + topoType
				+ "]";
	}

	

}
