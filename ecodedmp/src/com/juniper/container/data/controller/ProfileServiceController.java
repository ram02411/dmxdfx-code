package com.juniper.container.data.controller;

import java.io.IOException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.topology.viewer.data.device.model.VPNProvisionVO;
import com.ecode.web.service.ServiceProfileService;
import com.ecode.web.util.EcodeDateUtils;
import com.juniper.container.data.controller.model.PutServiceProfileVO;
import com.juniper.container.data.controller.model.ServiceProfileVO;
import com.servicenow.PostProfile;
import com.servicenow.PostService;
import com.servicenow.UpdateProfile;

@Controller
@Scope("request")
public class ProfileServiceController {
	
	private static final Log logger = LogFactory.getLog(ProfileServiceController.class);
	
	@Autowired
	@Qualifier("ServiceProfileServiceImpl")
	private ServiceProfileService serviceProfileService;
	
	@RequestMapping(value="services",method=RequestMethod.GET)
	public String showServices(Model model){
		List<ServiceProfileVO> serviceProfilesList=serviceProfileService.findAllVpnProfileServices();
		model.addAttribute("serviceProfilesList",serviceProfilesList);
		return "profileServices";
	}
	
	@RequestMapping(value="showProfilesByName",method=RequestMethod.POST)
	public String showProfileByServiceName(@RequestParam(value="ProfileserviceName",required=false) String serviceName,Model model) {
		List<ServicenowProfileVO> vpnProvisionVOList=serviceProfileService.findVpnProfileByServiceName(serviceName);
		model.addAttribute("vpnProvisionVOList",vpnProvisionVOList);
		return "showProfiles";
	}
	
	@RequestMapping(value="profileByServiceName",method=RequestMethod.GET)
	@ResponseBody public List<ServicenowProfileVO> profileByServiceName(@RequestParam(value="serviceName",required=false) String serviceName,Model model) {
		List<ServicenowProfileVO> vpnProvisionVOList=serviceProfileService.findVpnProfileByServiceName(serviceName);
		return vpnProvisionVOList;
	}
	@RequestMapping(value="profileByProfileName",method=RequestMethod.GET)
	@ResponseBody public ServicenowProfileVO profileByProfileName(@RequestParam(value="profilename",required=false) String profileName,Model model) {
		ServicenowProfileVO vpnProvisionVOList=serviceProfileService.findVpnProfileByProfileName(profileName);
		return vpnProvisionVOList;
	}
	
	
	@RequestMapping(value="addService",method=RequestMethod.GET)
	public String addServiceProfile(Model model){
		ServiceProfileVO serviceProfileVO=new ServiceProfileVO();
		model.addAttribute("serviceProfileVO",serviceProfileVO);
		return "profileServices";
	}
	
	@RequestMapping(value="addService",method=RequestMethod.POST)
	public String addServiceProfileSubmit(@RequestParam(value="serviceName",required=false) String serviceName,@RequestParam(value="clientDetail",required=false) String clientdetail,@RequestParam(value="description",required=false) String description,@RequestParam(value="serviceType",required=false) String serviceType,@RequestParam(value="serviceStatus",required=false) String status,Model model){
		String insertservice="{\"servicename\":\""+serviceName+"\",\"clientdetail\":\""+clientdetail+"\",\"description\":\""+description+"\",\"type\":\""+serviceType+"\",\"status\":\""+status+"\"}";
		
		if(logger.isDebugEnabled())
			logger.debug("executing addServiceProfileSubmit method ="+insertservice);

		try {
			System.out.println("insert service : "+insertservice);
			PostService.postRequest(insertservice);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ServiceProfileVO> serviceProfilesList=serviceProfileService.findAllVpnProfileServices();
		model.addAttribute("serviceProfilesList",serviceProfilesList);
		return "profileServices";
	}
	
	@RequestMapping(value="addProfileService",method=RequestMethod.POST)
	public String addVpnProfileSubmit(@ModelAttribute("vpnProvisionVO") VPNProvisionVO vpnProvisionVO,Model model){
		String groupname=vpnProvisionVO.getGroupName();
		if(groupname!=null){
		String[] groupnamearr=groupname.split(",");
		vpnProvisionVO.setGroupName(groupnamearr[0]);
		}
		String insertProfile="{\"authenticationkey\":\""+vpnProvisionVO.getAuthenticationKey()+"\",\"exports\":\""+vpnProvisionVO.getExports()+"\",\"groupname\":\""+vpnProvisionVO.getGroupName()+"\",\"imports\":\""+vpnProvisionVO.getImports()+"\",\"localas\":\""+vpnProvisionVO.getLocalas()+"\",\"peeras\":\""+vpnProvisionVO.getPeeras()+"\",\"profilename\":\""+vpnProvisionVO.getProfileName()+"\",\"servicename\":\""+vpnProvisionVO.getServiceName()+"\",\"sourcelocalip\":\""+vpnProvisionVO.getSourceLocalIP()+"\",\"sourceneighborip\":\""+vpnProvisionVO.getSourceNeighborIP()+"\",\"targetlocalip\":\""+vpnProvisionVO.getTargetLocalIP()+"\",\"targetneighborip\":\""+vpnProvisionVO.getTargetNeighborIP()+"\",\"type\":\""+vpnProvisionVO.getType()+"\"}";
		System.out.println("insert profile : "+insertProfile);
		if(logger.isDebugEnabled())
			logger.debug("executing addServiceProfileSubmit method ="+vpnProvisionVO);
		try {
			PostProfile.postRequest(insertProfile);
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ServiceProfileVO> serviceProfilesList=serviceProfileService.findAllVpnProfileServices();
		model.addAttribute("serviceProfilesList",serviceProfilesList);
		return "profileServices";
	}
	
	@RequestMapping(value="updateProfile",method=RequestMethod.POST)
	public String updateVpnProfileSubmit(@ModelAttribute("vpnProvisionVO") VPNProvisionVO vpnProvisionVO,Model model){
		String groupname=vpnProvisionVO.getGroupName();
		if(groupname!=null){
		String[] groupnamearr=groupname.split(",");
		vpnProvisionVO.setGroupName(groupnamearr[0]);
		}
		String insertProfile="{\"authenticationkey\":\""+vpnProvisionVO.getAuthenticationKey()+"\",\"exports\":\""+vpnProvisionVO.getExports()+"\",\"groupname\":\""+vpnProvisionVO.getGroupName()+"\",\"imports\":\""+vpnProvisionVO.getImports()+"\",\"localas\":\""+vpnProvisionVO.getLocalas()+"\",\"peeras\":\""+vpnProvisionVO.getPeeras()+"\",\"profilename\":\""+vpnProvisionVO.getProfileName()+"\",\"sourcelocalip\":\""+vpnProvisionVO.getSourceLocalIP()+"\",\"sourceneighborip\":\""+vpnProvisionVO.getSourceNeighborIP()+"\",\"targetlocalip\":\""+vpnProvisionVO.getTargetLocalIP()+"\",\"targetneighborip\":\""+vpnProvisionVO.getTargetNeighborIP()+"\",\"type\":\""+vpnProvisionVO.getType()+"\"}";
		System.out.println("Update Profile : "+insertProfile);
		if(logger.isDebugEnabled())
			logger.debug("executing addServiceProfileSubmit method ="+vpnProvisionVO);
		try {
			UpdateProfile.profileUpdate(insertProfile, vpnProvisionVO.getSys_id());
		} catch (HttpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ServiceProfileVO> serviceProfilesList=serviceProfileService.findAllVpnProfileServices();
		model.addAttribute("serviceProfilesList",serviceProfilesList);
		return "profileServices";
	}


}
