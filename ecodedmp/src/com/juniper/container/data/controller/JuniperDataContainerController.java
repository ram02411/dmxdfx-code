package com.juniper.container.data.controller;

import java.io.InputStream;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecode.web.app.constant.ApplicationConstant;
import com.juniper.container.data.controller.model.RamMemoryUsageVO;
import com.juniper.container.data.model.DataContainer;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.juniper.container.data.service.JuniperDataService;

@Controller
public class JuniperDataContainerController {
	
	
	@Autowired
	@Qualifier("JuniperDataServiceImpl")
	private JuniperDataService juniperDataService;
	
	public JuniperDataContainerController(){
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
		System.out.println("___@)@@Controller is ready_________________________________");
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/cool",method=RequestMethod.GET)
	public @ResponseBody String cool(){
		return "I am mr. cool!!!!!!!!!!!!";
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/deviceMemoryUsage",method=RequestMethod.GET)
	public @ResponseBody RamMemoryUsageVO findDeviceMemoryUsage(){
		RamMemoryUsageVO ramMemoryUsageVO=juniperDataService.fetchDeviceMemoryUsage(null);
		return ramMemoryUsageVO;
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/juniperData",method=RequestMethod.GET)
	public @ResponseBody DataContainer juniperDataFromLiveDevice(@RequestParam(value="routerName",required=false) String routerName){
		
		System.out.println("___@)@@juniperDataFromLiveDevice________________________________"+new Date());
		System.out.println("___@)@@juniperDataFromLiveDevice_______________________________"+routerName);
		System.out.println("___@)@@juniperDataFromLiveDevice_____________________________"+routerName);
		System.out.println("___@)@@juniperDataFromLiveDevice_________________________________"+routerName);
		System.out.println("___@)@@juniperDataFromLiveDevice________________________________"+routerName);
		System.out.println("___@)@@juniperDataFromLiveDevice_________________________________"+routerName);
		System.out.println("___@)@@juniperDataFromLiveDevice________________________________"+routerName);
		System.out.println("___@)@@juniperDataFromLiveDevice________________________________"+new Date());
		DataContainer juniperDataContainer=juniperDataService.fetchJuniperDataModel(routerName);
		if(juniperDataContainer!=null){
			juniperDataContainer.setResultStatus(ApplicationConstant.LIVE_DEVICE);
			return juniperDataContainer;
		}else{
			juniperDataContainer=new DataContainer();
			juniperDataContainer.setResultStatus(ApplicationConstant.FAILED_STATUS);
		}
		return juniperDataContainer;
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/juniperFileData",method=RequestMethod.GET)
	public @ResponseBody DataContainer juniperDataFromFileSystem(HttpSession session){
		ServletContext context=session.getServletContext();
		InputStream fileInputStream = context.getResourceAsStream("/xmldata/rpc_reply.xml");
		DataContainer juniperDataContainer=juniperDataService.findJuniperDataModel(fileInputStream);
		juniperDataContainer.setResultStatus(ApplicationConstant.XML_FILE_DEVICE);
		return juniperDataContainer;
	}
	
	/**
	 * 
	 * @param session
	 * @return
	 */
	@RequestMapping(value="/configProtocol",method=RequestMethod.GET)
	public @ResponseBody CProtocols findConfigurationProtocol(HttpSession session){
		ServletContext context=session.getServletContext();
		InputStream fileInputStream = context.getResourceAsStream("/xmldata/rpc_reply.xml");
		DataContainer juniperDataContainer=juniperDataService.findJuniperDataModel(fileInputStream);
		juniperDataContainer.setResultStatus(ApplicationConstant.XML_FILE_DEVICE);
		return juniperDataContainer.getConfiguration().getProtocols();
	}
	
	

}
