package com.juniper.container.data.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecode.topology.viewer.data.device.EcodeDeviceEndToEndProvision;
import com.ecode.topology.viewer.data.device.EcodeTopologyDeviceViewer;
import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.model.ServicenowProfileVO;
import com.ecode.topology.viewer.data.device.model.VPNProvisionVO;
import com.ecode.web.app.util.AppMessageVO;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;
import com.ecode.web.service.ServiceProfileService;
import com.juniper.container.data.controller.model.AllPossiblePathVO;
import com.juniper.container.data.controller.model.PathFinderVO;
import com.juniper.container.data.controller.model.ServiceProfileVO;

@Controller
@Scope("request")
public class PathFinderTopologyController {
	
	 private static final Log logger = LogFactory.getLog(PathFinderTopologyController.class);

	 @Autowired
		@Qualifier("ServiceProfileServiceImpl")
		private ServiceProfileService serviceProfileService;
	 
	 @Autowired
	@Qualifier("EcodeTopologyDeviceViewerImpl")
	private EcodeTopologyDeviceViewer ecodeTopologyDeviceViewer;
	
	@Autowired
	@Qualifier("EcodeDeviceEndToEndProvisionImpl")
	private EcodeDeviceEndToEndProvision ecodeDeviceEndToEndProvision;
	
	@RequestMapping(value="endToEndVpnProvision",method=RequestMethod.POST)
	public @ResponseBody AppMessageVO endToEndVpnProvision(@ModelAttribute("vpnProvisionVO") ServicenowProfileVO vpnProvisionVO){
		if(logger.isDebugEnabled())
		logger.debug(vpnProvisionVO);
		ecodeDeviceEndToEndProvision.applyVpnProvisionForSelectedSourceDest(vpnProvisionVO);
		System.out.println("endToEndVpnProvision controller = "+vpnProvisionVO);
		AppMessageVO appMessageVO=new AppMessageVO();
		appMessageVO.setScode("200");
		appMessageVO.setMessage("Vpn has been setup successfully for selected source and destination routers..");
		appMessageVO.setStatus("success");
		//System.out.println(vpnProvisionVO);
		return appMessageVO;
	}
	
	@RequestMapping(value="endToEndVpnProvisionWithProfile",method=RequestMethod.POST)
	public String endToEndVpnProvisionWithProfile(@RequestParam(value="sppath",required=false) String sppath,@RequestParam(value="svpnProfile",required=false) String svpnProfile) {
		if(logger.isDebugEnabled())
			logger.debug("Executing the endToEndVpnProvisionWithProfile............");
		ServicenowProfileVO vpnProvisionVO=serviceProfileService.findVpnProfileByProfileName(svpnProfile);
		String path=sppath.substring(0,sppath.length()-1);
		vpnProvisionVO.setSelectedLogicPath(path);
		if(logger.isDebugEnabled())
		logger.debug(vpnProvisionVO);
		ecodeDeviceEndToEndProvision.applyVpnProvisionForSelectedSourceDest(vpnProvisionVO);
		//System.out.println("endToEndVpnProvision controller = "+vpnProvisionVO);
		AppMessageVO appMessageVO=new AppMessageVO();
		appMessageVO.setScode("200");
		appMessageVO.setMessage("Profile has been setup successfully for selected source and destination routers..");
		appMessageVO.setStatus("success");
		//System.out.println(vpnProvisionVO);
		return "redirect:findL2Path";
	}
	
	@RequestMapping(value="tfindL2Path",method=RequestMethod.GET)
	public String showTPathFinderPage(Model model){
		List<ServiceProfileVO> serviceProfileVOs=serviceProfileService.findAllVpnProfileServices();
		model.addAttribute("serviceProfileVOs",serviceProfileVOs);
		PathFinderVO pathFinderVO=new PathFinderVO();
		model.addAttribute("pathFinderVO",pathFinderVO);
		return "showTPath";
	}
	
	
	@RequestMapping(value="findL2Path",method=RequestMethod.GET)
	public String showPathFinderPage(Model model){
		List<ServiceProfileVO> serviceProfileVOs=serviceProfileService.findAllVpnProfileServices();
		model.addAttribute("serviceProfileVOs",serviceProfileVOs);
		PathFinderVO pathFinderVO=new PathFinderVO();
		model.addAttribute("pathFinderVO",pathFinderVO);
	
		return "showPath";
	}
	
	@RequestMapping(value="findL2PathTest",method=RequestMethod.POST)
	public @ResponseBody List<AllPossiblePathVO> showPathFinderPageTest(@ModelAttribute("pathFinderVO") PathFinderVO pathFinderVO,Model model){
		List<AllPossiblePathVO> allPossiblePathVOs=new ArrayList<>();
		AllPossiblePathVO allPossiblePathVO=new AllPossiblePathVO();
		allPossiblePathVO.setPossiblePath("IRLARCPGPAR001-AG1-1(172.25.0.24),IRLARCPGESR004-920iBot1R03(172.25.0.37),IRLARCPGES006-ACX2200TOP(172.25.0.151),IRLARCPGESR005-920_O_1R04(172.25.0.40)");
		List<String> list=new ArrayList<>();
		list.add("RLARCPGPAR001-AG1-1(172.25.0.24)");
		list.add("IRLARCPGESR004-920iBot1R03(172.25.0.37)");
		list.add("IRLARCPGES006-ACX2200TOP(172.25.0.151)");
		allPossiblePathVO.setRoutersList(list);
		allPossiblePathVOs.add(allPossiblePathVO);
		
		if(logger.isDebugEnabled())
			logger.debug(pathFinderVO);
		return allPossiblePathVOs;
	}	
	
	@RequestMapping(value="findL2Path",method=RequestMethod.POST)
	public @ResponseBody List<AllPossiblePathVO> showPathFinderPage(@ModelAttribute("pathFinderVO") PathFinderVO pathFinderVO,Model model){
		
		if(logger.isDebugEnabled())
			logger.debug(pathFinderVO);
		
		List<AllPossiblePathVO> allPossiblePathVOs=new ArrayList<>();
		Router source=ApplicationRouterConfigurationUtil.findRouterByIpv4(pathFinderVO.getSourceRouter());
		Router dest=ApplicationRouterConfigurationUtil.findRouterByIpv4(pathFinderVO.getDestRouter());
		if(logger.isDebugEnabled())
			logger.debug(" Source router = "+source+" , dest = "+dest);
		
		List<Router> routers=new  ArrayList<>();
		if("L2TOPO".equalsIgnoreCase(pathFinderVO.getTopoType())){
			List<List<String>> allPossiblePaths=ecodeTopologyDeviceViewer.findAllPossiblePathSourceDestination(source, dest, routers);
			if(logger.isDebugEnabled())
				logger.debug("allPossiblePaths = "+allPossiblePaths);
		
			if(allPossiblePaths!=null && allPossiblePaths.size()==0){
				model.addAttribute("ApplicationMessage", "Sorry! No L2 Topology is discovered for selected source and destination of routers");
			}
			for(List<String> list:allPossiblePaths){
				String concat="";
				for(String str:list){
					concat=concat+str+",";
				}
				AllPossiblePathVO allPossiblePathVO=new AllPossiblePathVO();
				allPossiblePathVO.setPossiblePath(concat);
				allPossiblePathVO.setRoutersList(list);
				allPossiblePathVOs.add(allPossiblePathVO);
			}
			if(logger.isDebugEnabled())
				logger.debug("allPossiblePaths = "+allPossiblePaths);
			//model.addAttribute("allPossiblePathVOs", allPossiblePathVOs);	
			//model.addAttribute("allPossiblePaths", allPossiblePathVOs);	
		}else{
			List<List<String>> allPossiblePaths=ecodeTopologyDeviceViewer.findAllPossibleLogicalPathSourceDestination(source, dest, routers);
			if(logger.isDebugEnabled())
				logger.debug("allPossiblePaths = "+allPossiblePaths);
			//List<AllPossiblePathVO> allPossiblePathVOs=new ArrayList<>();
			if(allPossiblePaths!=null && allPossiblePaths.size()==0){
				model.addAttribute("ApplicationMessage", "Sorry! No L3 Topology is discovered for selected source and destination of routers");
			}
			for(List<String> list:allPossiblePaths){
				String concat="";
				for(String str:list){
					concat=concat+str+",";
				}
				AllPossiblePathVO allPossiblePathVO=new AllPossiblePathVO();
				allPossiblePathVO.setPossiblePath(concat);
				allPossiblePathVO.setRoutersList(list);
				allPossiblePathVOs.add(allPossiblePathVO);
			}
			if(logger.isDebugEnabled())
				logger.debug("allPossiblePathVOs = "+allPossiblePathVOs);
		}
		return allPossiblePathVOs;
	}
	
	@ModelAttribute("sourceRouterList")
	public Map<String,String> sourceRouter(){
		return ApplicationRouterConfigurationUtil.findHostIpNameMap();
	}
	

	@ModelAttribute("topoTyperList")
	public Map<String,String> topoTyperList(){
		Map<String,String> map=new LinkedHashMap<>();
		map.put("L2TOPO", "L2 Topology");
		map.put("L3TOPO", "L3 Topology");
		return map;
	}
	
	
	
	@ModelAttribute("destRouterList")
	public Map<String,String> destRouter(){
		return ApplicationRouterConfigurationUtil.findHostIpNameMap();
	}
	
}
