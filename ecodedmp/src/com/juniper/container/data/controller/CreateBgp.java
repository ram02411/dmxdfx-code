package com.juniper.container.data.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.ecode.cisco.command.SSHClient;
import com.ecode.topology.viewer.data.device.Router;

import net.juniper.netconf.Device;
import net.juniper.netconf.NetconfException;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;
import test.sshclientj;

public class CreateBgp {
	public static void createBGPCisco(Router router,String asno,String neighborip){
		SSHClient ssh = new SSHClient(router.getIp(), router.getUsername(),router.getPassword());
		List<String> cmdsToExecute = new ArrayList<String>();
        cmdsToExecute.add("configure terminal");
          cmdsToExecute.add("router bgp "+asno+"");
      cmdsToExecute.add("bgp log-neighbor-changes");
      cmdsToExecute.add("neighbor "+neighborip+" remote-as "+asno+"");
      
      System.out.println("__Please wait BGP Create processing is going on__ ");
      //String outputLog = ssh.execute(cmdsToExecute);
      //System.out.println(outputLog);
      ssh.disconnectionSession();
		
		
	}
	public static void createBGPJuniper(){
		sshclientj ssh = new sshclientj("192.168.60.2", "root","ecode123");
		//List<String> cmdsToExecute = new ArrayList<String>();
        //cmdsToExecute.add("cli");
        //cmdsToExecute.add("version");
      
      System.out.println("__Please wait command is going on__ ");
      String outputLog = ssh.execute("cli");
      //System.out.println(outputLog);
      String outputLog1 = ssh.execute("show bgp neighbor");
      System.out.println(outputLog1);
      String data=outputLog1;
      System.out.println("data is "+data);
      ssh.disconnectionSession();
		
		
	}
	public static void main(String[] arg){
		createBGPJuniper();
	}
	public static void xmlfun(){
		try {
			Device device=new Device("192.168.100.70","root","ecode123",null,830);
			device.connect();
			
				XMLBuilder query = new XMLBuilder();
				//XML q = query.createNewXML("get-ipv6-nd-information");
				String ipv6= "<rpc>" + " <get-route-engine-information>" + "</get-route-engine-information>"
						+ " </rpc>";
				try {
					XML rpc_reply = device.executeRPC("");
					System.out.println(rpc_reply);
				} catch (SAXException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		} catch (NetconfException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
