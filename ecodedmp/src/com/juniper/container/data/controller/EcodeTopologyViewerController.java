package com.juniper.container.data.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecode.topology.viewer.data.device.Category;
import com.ecode.topology.viewer.data.device.EcodeTopologyDeviceViewer;
import com.ecode.topology.viewer.data.device.Link;
import com.ecode.topology.viewer.data.device.NStyle;
import com.ecode.topology.viewer.data.device.Node;
import com.ecode.topology.viewer.data.device.Nodes;
import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.Style;
import com.ecode.topology.viewer.data.device.SubValue;
import com.ecode.topology.viewer.data.device.TopologyViewerVO;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;


@Controller
public class EcodeTopologyViewerController {
	
	
	@Autowired
	@Qualifier("EcodeTopologyDeviceViewerImpl")
	private EcodeTopologyDeviceViewer ecodeTopologyDeviceViewer;
	
	/**
	 * 
	 * @return 
	 */
	@RequestMapping(value="showLogicalNetworkPath",method=RequestMethod.GET)
   @ResponseBody public 	List<List<String>> findAllPossibleLogicalPathSourceDestination(){
		List<Router> routers=new  ArrayList<>();
		List<List<String>> allPossiblePaths=ecodeTopologyDeviceViewer.findAllPossibleLogicalPathSourceDestination(null, null, routers);
    	return allPossiblePaths;
    }
	
	
	/**
	 * 
	 * @return 
	 */
	@RequestMapping(value="showNetworkPath",method=RequestMethod.GET)
   @ResponseBody public 	List<List<String>> showNetworkPath(){
		List<Router> routers=new  ArrayList<>();
		List<List<String>> allPossiblePaths=ecodeTopologyDeviceViewer.findAllPossiblePathSourceDestination(null, null, routers);
    	return allPossiblePaths;
    }
	
	/**
	 * 
	 * @return 
	 */
	@RequestMapping(value="viewNetworkLogicalTopology",method=RequestMethod.GET)
   @ResponseBody public TopologyViewerVO showNetworkLogicalTopology(){
		List<Router> routers=new  ArrayList<>();
		ApplicationRouterConfigurationUtil.addRouter();
		TopologyViewerVO topologyViewerVO=ecodeTopologyDeviceViewer.showNetworkLogicalTopology(routers);
		topologyViewerVO.setStatus("success");
    	return topologyViewerVO;
    }
	/**
	 * 
	 * @return 
	 */
	@RequestMapping(value="viewNetworkLogicalTopologyMpls",method=RequestMethod.GET)
   @ResponseBody public TopologyViewerVO showNetworkLogicalTopologympls(){
		List<Router> routers=new  ArrayList<>();
		ApplicationRouterConfigurationUtil.addRouter();
		TopologyViewerVO topologyViewerVO=ecodeTopologyDeviceViewer.showNetworkLogicalTopologyMpls(routers);
		topologyViewerVO.setStatus("success");
    	return topologyViewerVO;
    }
	
	/**
	 * 
	 * @return 
	 */
	@RequestMapping(value="viewNetworkTopology",method=RequestMethod.GET)
   @ResponseBody public TopologyViewerVO showNewworkRouterTopology(){
		List<Router> routers=new  ArrayList<>();
		//ApplicationRouterConfigurationUtil.addRouter();
		TopologyViewerVO topologyViewerVO=ecodeTopologyDeviceViewer.showNetworkTopology(routers);
		topologyViewerVO.setStatus("success");
    	return topologyViewerVO;
    }
	
	
	/**
	 * 
	 * @return 
	 */
	/**
	 * @return
	 */
	@RequestMapping(value="testingTopo",method=RequestMethod.GET)
   @ResponseBody public TopologyViewerVO testing(){
		
		Node node1=new Node();
		node1.setId("Olive1192.168.56.3");
		node1.setLoaded(true);
		NStyle style1=new NStyle();
		style1.setLabel("Olive1");
		node1.setStyle(style1);
		//we have to set categories for node1.
		
		SubValue subValue1=new SubValue();
		subValue1.setId(3);
		subValue1.setName("Used Memory");
		subValue1.setValue(2.5F);
		
		SubValue subValue2=new SubValue();
		subValue2.setId(4);
		subValue2.setName("Free CPU");
		subValue2.setValue(2.5F);
		
		List<SubValue> subValues1=new ArrayList<>();
		subValues1.add(subValue1);
		subValues1.add(subValue2);
		
		Category category1=new Category();
		Category category2=new Category();
		Category category3=new Category();
		Category category4=new Category();
		category1.setId(1);
		category1.setName("Used Memory :");
		category1.setValue(2.5F);
		category2.setId(2);
		category2.setName("Free Memory :");
		category2.setValue(2.5F);
		category3.setId(3);
		category3.setName("Used CPU :");
		category3.setValue(2.5F);
		category4.setId(4);
		category4.setName("Free CPU :");
		category4.setValue(2.5F);
		
		
		List<Category>  categories=new ArrayList<>();
		categories.add(category1);
		categories.add(category2);
		categories.add(category3);
		categories.add(category4);
		node1.setCategories(categories);
		
		
		Node node2=new Node();
		node2.setId("Olive2192.168.56.4");
		node2.setLoaded(true);
		NStyle style2=new NStyle();
		style2.setLabel("Olive2");
		node2.setStyle(style2);
		Category category5=new Category();
		Category category6=new Category();
		Category category7=new Category();
		Category category8=new Category();
		category5.setId(1);
		category5.setName("Used Memory :");
		category5.setValue(2.5F);
		category6.setId(2);
		category6.setName("Free Memory :");
		category6.setValue(2.5F);
		category7.setId(3);
		category7.setName("Used CPU :");
		category7.setValue(2.5F);
		category8.setId(4);
		category8.setName("Free CPU :");
		category8.setValue(2.5F);
		
		
		List<Category>  categories1=new ArrayList<>();
		categories1.add(category5);
		categories1.add(category6);
		categories1.add(category7);
		categories1.add(category8);
		node2.setCategories(categories1);
		
		
		List<Node> nodes=new ArrayList<>();
		nodes.add(node2);
		nodes.add(node1);
		
		Link link1=new Link();
		link1.setFrom("Olive1192.168.56.3");
		link1.setId("Olive1em0");
		Style lstyle1=new Style();
		lstyle1.setFillColor("green");
		lstyle1.setToDecoration("line");
		link1.setStyle(lstyle1);
		link1.setTo("Olive1192.168.56.3");
		
		Link link2=new Link();
		link2.setFrom("Olive2192.168.56.4");
		link2.setId("Olive2em0");
		Style lstyle2=new Style();
		lstyle2.setFillColor("green");
		lstyle2.setToDecoration("line");
		link2.setStyle(lstyle2);
		link2.setTo("Olive2192.168.56.4");
		
		List<Link> links=new ArrayList<>();
		links.add(link1);
		links.add(link2);
		
		
		Nodes nodesList=new Nodes();
		nodesList.setLinks(links);
		nodesList.setNodes(nodes);
		
		TopologyViewerVO topologyViewerVO=new TopologyViewerVO();
		topologyViewerVO.setJsonMessage(nodesList);
		topologyViewerVO.setStatus("success");
    	return topologyViewerVO;
    }

}
