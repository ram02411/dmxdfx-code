package com.juniper.container.data.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cisco.container.model.yangbgp.ApplyPolicy;
import com.ecode.topology.viewer.data.device.EcodeTopologyDeviceViewer;
import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.model.BgpGroupData;
import com.ecode.topology.viewer.data.device.parser.ApplyPolicyRouterUtil;
import com.ecode.topology.viewer.data.device.parser.BgpGroupsDeviceUtil;
import com.ecode.web.app.util.ApplicationRouterConfigurationUtil;
import com.juniper.container.data.controller.model.AllPossiblePathVO;
import com.juniper.container.data.model.configuration.protocols.Bgp;
import com.juniper.container.data.model.configuration.protocols.BgpGroup;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.juniper.container.data.model.configuration.protocols.LocalAs;
import com.juniper.container.data.model.configuration.protocols.Neighbor;
import com.juniper.container.data.service.JuniperBgpServiceImpl;

public class EndToEndProvisioning {
	
	public static void main(String[] args) {
		
	/*	String cimport="1.1.1.1/24";
		String cexport="2.2.2.2/24";
		*/
		
		String cimport="testimp1";
		String cexport="testexp1";
		
		List<AllPossiblePathVO> allPossiblePathVOs=new ArrayList<>();
		Router source=ApplicationRouterConfigurationUtil.findRouterByIpv4("192.168.1.44");//This is ip of AG1
		Router dest=ApplicationRouterConfigurationUtil.findRouterByIpv4("192.168.1.46"); //THIS IS IP of AG1.1
		List<Router> routers=new  ArrayList<>();
		ApplicationContext applicationContext=new ClassPathXmlApplicationContext("ecode-service-context.xml");
		EcodeTopologyDeviceViewer ecodeTopologyDeviceViewer=(EcodeTopologyDeviceViewer)applicationContext.getBean("EcodeTopologyDeviceViewerImpl");
		JuniperBgpServiceImpl juniperBgpServiceImpl=(JuniperBgpServiceImpl)applicationContext.getBean("JuniperBgpServiceImpl");
		//Create a bgp group form for new router CE2
		CProtocols cProtocols=new CProtocols();
		Bgp  bgp=new  Bgp();
	 	cProtocols.setBgp(bgp);
		 List<BgpGroup> bgpGroupList=new ArrayList<>();
		 BgpGroup bgpGroup=new BgpGroup();
		 bgpGroup.setName("GroupGK");
		 bgpGroup.setLocaladdress("192.168.1.50"); //CSR1
		 List<Neighbor> neighbors=new ArrayList<>();
		 Neighbor neighbor=new Neighbor();
		 LocalAs localAs=new LocalAs();
		 localAs.setAsnumber(1234+"");
		 neighbor.setLocalas(localAs);
		 neighbor.setName("192.168.1.44"); //AG1
		 neighbor.setPeeras(1234+"");
		 neighbors.add(neighbor);
		 bgpGroup.setNeighbor(neighbors);
		 bgpGroup.setType("internal");
		 List<String> export=new ArrayList<>();
		 export.add(cexport);
		 List<String> iimport=new ArrayList<>();
		 iimport.add(cimport);
		 bgpGroup.setIimport(iimport);
		 bgpGroup.setExport(export);
		 bgpGroupList.add(bgpGroup);
		 bgp.setBgpGroup(bgpGroupList);
		 System.out.println("____new group for CSR1");
		 juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", "192.168.1.50_root_ecode123",true);
		 
		 System.out.println("____new group for AG1");
		 bgpGroup.setIimport(export);
		 bgpGroup.setExport(iimport);
		 bgpGroup.setLocaladdress("192.168.1.44");////AG1
		 neighbor.setName("192.168.1.50");//CSR1
		 
  	    List<List<String>>  result=ecodeTopologyDeviceViewer.findAllPossibleLogicalPathSourceDestination(source, dest, routers);
		
		System.out.println("___result___ = "+result);
		List<String> dselectedLogicPath=result.get(0);
		
		///BE CAREFUL FOR ORDERING..
		List<String> selectedLogicPath =new ArrayList<>();
		for(int m=dselectedLogicPath.size()-1;m>=0;m--) {
			selectedLogicPath.add(dselectedLogicPath.get(m));
		}
		
		String calculatedImport=cimport;
		String calculatedExport=cexport;
		for(int p=selectedLogicPath.size()-1;p>=0;p--)   {
			 String currentHostNameIp=selectedLogicPath.get(p);
			 
			 ApplyPolicyRouterUtil.applyRouterPolicy(ApplicationRouterConfigurationUtil.findRouterByIpv4(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp)),cimport,cexport);
			 
			 if(p==selectedLogicPath.size()-1){ //only first time
				 juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", "192.168.1.44_root_ecode123",true);
				 
			 }
			 System.out.println("___Policy is applied for host    = "+currentHostNameIp);
			 List<BgpGroupData> cbgpGroupList=BgpGroupsDeviceUtil.findDeviceBgpGroups(BgpGroupsDeviceUtil.getDeviceHostName(currentHostNameIp));
			 System.out.println(currentHostNameIp+"  Groups= "+cbgpGroupList);
			 
			 if(p>0) {
			     String nextHostNameIp=selectedLogicPath.get(p-1);
				 List<BgpGroupData> nextBgpGroupList=BgpGroupsDeviceUtil.findDeviceBgpGroups(BgpGroupsDeviceUtil.getDeviceHostName(nextHostNameIp));
				 System.out.println(nextHostNameIp+" fetching  all f Groups from device= "+nextBgpGroupList);
				 //finding common groups
				 boolean b =nextBgpGroupList.retainAll(cbgpGroupList);
				 System.out.println("___Common Groups are found___    = "+b);
				 System.out.println("---Common groups _--- = "+nextBgpGroupList);
				 System.out.println("Update group for  = "+nextBgpGroupList);
				 
				 
				 
				 List<String> ccexport=new ArrayList<>();
				 ccexport.add(calculatedExport);
				 List<String> ciimport=new ArrayList<>();
				 ciimport.add(calculatedImport);
				 CProtocols cProtocols1=BgpGroupsDeviceUtil.createBgpGroup(nextBgpGroupList.get(0), BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp), ccexport, ciimport);
				 cProtocols1.getBgp().getBgpGroup().get(0).getNeighbor().get(0).setName(BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp));
				 juniperBgpServiceImpl.provisionTraceOptions(cProtocols1, "merge", BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp)+"_root_ecode123",true);
				 System.out.println("Update group for  = "+nextHostNameIp);
				 List<String> nexport=new ArrayList<>();
				 nexport.add(calculatedImport);
				 List<String> niimport=new ArrayList<>();
				 niimport.add(calculatedExport);
				 CProtocols cProtocols2=BgpGroupsDeviceUtil.createBgpGroup(nextBgpGroupList.get(0), BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp), nexport, niimport);
				 cProtocols2.getBgp().getBgpGroup().get(0).getNeighbor().get(0).setName(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
				 juniperBgpServiceImpl.provisionTraceOptions(cProtocols2, "merge",  BgpGroupsDeviceUtil.getDeviceIpAddress(nextHostNameIp)+"_root_ecode123",true);
				// String temp=calculatedImport;
				 //calculatedImport=calculatedExport;
				 //calculatedExport=temp;
			}else {
				/////////////////////////////////////////////
				System.out.println("________output_____________");
				//////////////////////////////////////////////
				//make a new group
				 //for another host
				//for CE1
				 List<String> ce1export=new ArrayList<>();
				 ce1export.add(cimport);
				 List<String> ce1iimport=new ArrayList<>();
				 ce1iimport.add(cexport);
				 bgpGroup.setIimport(ce1iimport);
				 bgpGroup.setExport(ce1export);
				 bgpGroup.setLocaladdress("192.168.1.45"); //CSR2
				 bgpGroup.getNeighbor().get(0).setName(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
				 ApplyPolicyRouterUtil.applyRouterPolicy(ApplicationRouterConfigurationUtil.findRouterByIpv4("192.168.1.45"),cimport,cexport);
				 System.out.println("_____new group for CSR2_____");
				 juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", "192.168.1.45_root_ecode123",true);
				 ///
				 System.out.println("Adding new Group for = "+BgpGroupsDeviceUtil.getDeviceHostName(currentHostNameIp));
				 List<String> pe1export=new ArrayList<>();
				 pe1export.add(cexport);
				 List<String> pe1iimport=new ArrayList<>();
				 pe1iimport.add(cimport);
				 bgpGroup.setIimport(pe1iimport);
				 bgpGroup.setExport(pe1export);
				 bgpGroup.setLocaladdress(BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp));
				 bgpGroup.getNeighbor().get(0).setName("192.168.1.45");//CSR2
				 juniperBgpServiceImpl.provisionTraceOptions(cProtocols, "merge", BgpGroupsDeviceUtil.getDeviceIpAddress(currentHostNameIp)+"_root_ecode123",true);
				
			}
			  
		}
		
		System.out.println("__@)@)@((@COMPLETED!!!!!!!!!!!!!END OF THE MAIN METHOD!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}

}
