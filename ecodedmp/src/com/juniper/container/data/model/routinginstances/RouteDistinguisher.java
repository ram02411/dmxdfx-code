package com.juniper.container.data.model.routinginstances;

import javax.xml.bind.annotation.XmlElement;

public class RouteDistinguisher {
	private String rdtype;

	@XmlElement(name="rd-type",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRdtype() {
		return rdtype;
	}

	public void setRdtype(String rdtype) {
		this.rdtype = rdtype;
	}

	@Override
	public String toString() {
		return "RouteDistinguisher [rdtype=" + rdtype + "]";
	}

}
