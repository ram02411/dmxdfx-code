package com.juniper.container.data.model.routinginstances;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class RoutingInstances {
	
	private List<Instance> instance;

	@XmlElement(name="instance",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Instance> getInstance() {
		return instance;
	}

	public void setInstance(List<Instance> instance) {
		this.instance = instance;
	}

	@Override
	public String toString() {
		return "RoutingInstances [instance=" + instance + "]";
	}
	
}
