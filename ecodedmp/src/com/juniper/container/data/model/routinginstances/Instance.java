package com.juniper.container.data.model.routinginstances;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Instance {
	private String name;
	private String description;
	private String instanceType;
	private List<RoutingInterface> routingInterfaces;
	private String routeDistinguisher;
	private String vrfImport;
	private String vrfExport;
	private String vrfTableLabel;
	
	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<RoutingInterface> getRoutingInterfaces() {
		return routingInterfaces;
	}

	public void setRoutingInterfaces(List<RoutingInterface> routingInterfaces) {
		this.routingInterfaces = routingInterfaces;
	}

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="description",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="instance-type",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getInstanceType() {
		return instanceType;
	}

	public void setInstanceType(String instanceType) {
		this.instanceType = instanceType;
	}

	@XmlElement(name="route-distinguisher",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRouteDistinguisher() {
		return routeDistinguisher;
	}

	public void setRouteDistinguisher(String routeDistinguisher) {
		this.routeDistinguisher = routeDistinguisher;
	}

	@XmlElement(name="vrf-import",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVrfImport() {
		return vrfImport;
	}

	public void setVrfImport(String vrfImport) {
		this.vrfImport = vrfImport;
	}

	@XmlElement(name="vrf-export",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVrfExport() {
		return vrfExport;
	}

	public void setVrfExport(String vrfExport) {
		this.vrfExport = vrfExport;
	}

	@XmlElement(name="vrf-table-label",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVrfTableLabel() {
		return vrfTableLabel;
	}

	public void setVrfTableLabel(String vrfTableLabel) {
		this.vrfTableLabel = vrfTableLabel;
	}

	@Override
	public String toString() {
		return "Instance [name=" + name + ", description=" + description + ", instanceType=" + instanceType
				+ ", routingInterfaces=" + routingInterfaces + ", routeDistinguisher=" + routeDistinguisher
				+ ", vrfImport=" + vrfImport + ", vrfExport=" + vrfExport + ", vrfTableLabel=" + vrfTableLabel + "]";
	}

}
