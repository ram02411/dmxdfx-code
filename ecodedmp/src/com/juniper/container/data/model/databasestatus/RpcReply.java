package com.juniper.container.data.model.databasestatus;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.juniper.container.data.model.DataContainer;

@XmlRootElement(name = "rpc-reply", namespace = "urn:ietf:params:xml:ns:netconf:base:1.0")
public class RpcReply {
	private DataContainer dataContainer;

	@XmlElement(name="data",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public DataContainer getDataContainer() {
		return dataContainer;
	}

	public void setDataContainer(DataContainer dataContainer) {
		this.dataContainer = dataContainer;
	}

	@Override
	public String toString() {
		return "RpcReply [dataContainer=" + dataContainer + "]";
	}

}
