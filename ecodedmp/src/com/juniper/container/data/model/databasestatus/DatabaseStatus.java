package com.juniper.container.data.model.databasestatus;

import javax.xml.bind.annotation.XmlElement;

public class DatabaseStatus {

	private String user;
	private String terminal;
	private int pid;
	private String startTime;
	private String editPath;

	@XmlElement(name="user",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@XmlElement(name="terminal",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public String getTerminal() {
		return terminal;
	}

	public void setTerminal(String terminal) {
		this.terminal = terminal;
	}

	@XmlElement(name="pid",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	@XmlElement(name="start-time",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	@XmlElement(name="edit-path",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public String getEditPath() {
		return editPath;
	}

	public void setEditPath(String editPath) {
		this.editPath = editPath;
	}

	@Override
	public String toString() {
		return "DatabaseStatus [user=" + user + ", terminal=" + terminal + ", pid=" + pid + ", startTime=" + startTime
				+ ", editPath=" + editPath + "]";
	}

}
