package com.juniper.container.data.model.databasestatus;

import javax.xml.bind.annotation.XmlElement;

public class DatabaseStatusInformation {
	
	private DatabaseStatus databaseStatus;

	@XmlElement(name="database-status",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public DatabaseStatus getDatabaseStatus() {
		return databaseStatus;
	}

	public void setDatabaseStatus(DatabaseStatus databaseStatus) {
		this.databaseStatus = databaseStatus;
	}

	@Override
	public String toString() {
		return "DatabaseStatusInformation [databaseStatus=" + databaseStatus + "]";
	}
	
	

}
