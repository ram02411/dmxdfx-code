package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class User {
	private String name;
	private String uid;
	private String uclass;
	private Authentication authentication;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="uid",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	@XmlElement(name="class",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getUclass() {
		return uclass;
	}

	public void setUclass(String uclass) {
		this.uclass = uclass;
	}

	@XmlElement(name="authentication",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Authentication getAuthentication() {
		return authentication;
	}

	public void setAuthentication(Authentication authentication) {
		this.authentication = authentication;
	}

	@Override
	public String toString() {
		return "User [name=" + name + ", uid=" + uid + ", uclass=" + uclass
				+ ", authentication=" + authentication + "]";
	}

}
