package com.juniper.container.data.model.configuration.policyoptions;

import javax.xml.bind.annotation.XmlElement;

public class From {
	private String protocol;
	private String community;
	
	@XmlElement(name="protocol",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getProtocol() {
		return protocol;
	}
	
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	@XmlElement(name="community",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getCommunity() {
		return community;
	}
	
	public void setCommunity(String community) {
		this.community = community;
	}
	@Override
	public String toString() {
		return "From [protocol=" + protocol + ", community=" + community + "]";
	}
	 
	 
}
