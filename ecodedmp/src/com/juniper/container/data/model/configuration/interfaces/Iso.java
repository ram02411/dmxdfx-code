package com.juniper.container.data.model.configuration.interfaces;

import javax.xml.bind.annotation.XmlElement;


public class Iso {

	private Address address;
	
	@XmlElement(name="address",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Iso [address=" + address + "]";
	}
	
	
}
