package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class ShapingRate {
	private String rate;

	@XmlElement(name="rate",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	@Override
	public String toString() {
		return "ShapingRate [rate=" + rate + "]";
	}
	
}
