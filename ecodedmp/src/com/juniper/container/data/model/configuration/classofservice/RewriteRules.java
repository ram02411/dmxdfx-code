package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class RewriteRules {
	
	private Exp exp;

	@XmlElement(name="exp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Exp getExp() {
		return exp;
	}

	public void setExp(Exp exp) {
		this.exp = exp;
	}

	@Override
	public String toString() {
		return "RewriteRules [exp=" + exp + "]";
	}
	
	
}
