package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class MldGroup {

	private String name;
	private MldSource source;
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

	@XmlElement(name="source",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public MldSource getSource() {
		return source;
	}

	public void setSource(MldSource source) {
		this.source = source;
	}

	@Override
	public String toString() {
		return "MldGroup [name=" + name + ", source=" + source + "]";
	}

	

}
