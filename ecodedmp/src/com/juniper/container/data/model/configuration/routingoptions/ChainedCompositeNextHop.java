package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class ChainedCompositeNextHop {
	private Ingress ingress;

	@XmlElement(name="ingress",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Ingress getIngress() {
		return ingress;
	}

	public void setIngress(Ingress ingress) {
		this.ingress = ingress;
	}

	@Override
	public String toString() {
		return "ChainedCompositeNextHop [ingress=" + ingress + "]";
	}

}
