package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class ControlChannel {
	
	private String controlChannelName;

	@XmlElement(name="control-channel-name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getControlChannelName() {
		return controlChannelName;
	}

	public void setControlChannelName(String controlChannelName) {
		this.controlChannelName = controlChannelName;
	}

	@Override
	public String toString() {
		return "ControlChannel [controlChannelName=" + controlChannelName + "]";
	}
	
	
	
}
