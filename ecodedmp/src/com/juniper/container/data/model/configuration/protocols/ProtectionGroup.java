package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class ProtectionGroup {
	
	private EthernetRing ethernetRing;

	@XmlElement(name="ethernet-ring",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public EthernetRing getEthernetRing() {
		return ethernetRing;
	}

	public void setEthernetRing(EthernetRing ethernetRing) {
		this.ethernetRing = ethernetRing;
	}

	@Override
	public String toString() {
		return "ProtectionGroup [ethernetRing=" + ethernetRing + "]";
	}
	
	
	
}
