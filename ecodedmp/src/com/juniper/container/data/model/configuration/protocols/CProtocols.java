package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="protocols",namespace="http://xml.juniper.net/xnm/1.1/xnm")
public class CProtocols {
	private Mld mld;
	private RouterAdvertisement routerAdvertisement;
	private Mpls mpls;
	private Bgp bgp;
	private Isis isis;
	private ProtocolsLdp ldp;
	private Pim pim;
	private Oam oam;
	private ProtectionGroup protectionGroup;
	
	@XmlElement(name="mld",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Mld getMld() {
		return mld;
	}
	public void setMld(Mld mld) {
		this.mld = mld;
	}
	
	@XmlElement(name="router-advertisement",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RouterAdvertisement getRouterAdvertisement() {
		return routerAdvertisement;
	}
	public void setRouterAdvertisement(RouterAdvertisement routerAdvertisement) {
		this.routerAdvertisement = routerAdvertisement;
	}
	
	@XmlElement(name="mpls",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Mpls getMpls() {
		return mpls;
	}
	public void setMpls(Mpls mpls) {
		this.mpls = mpls;
	}
	
	@XmlElement(name="bgp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Bgp getBgp() {
		return bgp;
	}
	public void setBgp(Bgp bgp) {
		this.bgp = bgp;
	}
	
	@XmlElement(name="isis",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Isis getIsis() {
		return isis;
	}
	public void setIsis(Isis isis) {
		this.isis = isis;
	}
	
	@XmlElement(name="ldp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ProtocolsLdp getLdp() {
		return ldp;
	}
	public void setLdp(ProtocolsLdp ldp) {
		this.ldp = ldp;
	}
	
	@XmlElement(name="pim",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Pim getPim() {
		return pim;
	}
	public void setPim(Pim pim) {
		this.pim = pim;
	}
	
	@XmlElement(name="oam",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Oam getOam() {
		return oam;
	}
	public void setOam(Oam oam) {
		this.oam = oam;
	}
	
	@XmlElement(name="protection-group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ProtectionGroup getProtectionGroup() {
		return protectionGroup;
	}
	public void setProtectionGroup(ProtectionGroup protectionGroup) {
		this.protectionGroup = protectionGroup;
	}
	@Override
	public String toString() {
		return "CProtocols [mld=" + mld + ", routerAdvertisement=" + routerAdvertisement + ", mpls=" + mpls + ", bgp="
				+ bgp + ", isis=" + isis + ", ldp=" + ldp + ", pim=" + pim + ", oam=" + oam + ", protectionGroup="
				+ protectionGroup + "]";
	}
	
}
