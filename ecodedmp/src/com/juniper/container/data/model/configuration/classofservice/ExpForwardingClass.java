package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class ExpForwardingClass {
	private String name;
	private LossPriority lossPriority;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="loss-priority",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public LossPriority getLossPriority() {
		return lossPriority;
	}

	public void setLossPriority(LossPriority lossPriority) {
		this.lossPriority = lossPriority;
	}

	@Override
	public String toString() {
		return "ExpForwardingClass [name=" + name + ", lossPriority=" + lossPriority + "]";
	}

}
