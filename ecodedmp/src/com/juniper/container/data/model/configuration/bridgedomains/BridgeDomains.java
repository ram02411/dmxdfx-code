package com.juniper.container.data.model.configuration.bridgedomains;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class BridgeDomains {
	private List<Domain> domain;
	
	@XmlElement(name="domain",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Domain> getDomain() {
		return domain;
	}

	public void setDomain(List<Domain> domain) {
		this.domain = domain;
	}

	@Override
	public String toString() {
		return "BridgeDomains [domain=" + domain + "]";
	}

}
