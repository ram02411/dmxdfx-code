package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class ClassOfServiceRoutingInstances {
	private String name;
	private Classifiers classifiers;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="classifiers",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Classifiers getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(Classifiers classifiers) {
		this.classifiers = classifiers;
	}

	@Override
	public String toString() {
		return "ClassOfServiceRoutingInstances [name=" + name + ", classifiers=" + classifiers + "]";
	}

}
