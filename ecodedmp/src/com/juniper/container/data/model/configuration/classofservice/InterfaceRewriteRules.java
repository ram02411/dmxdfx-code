package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceRewriteRules {	
	
	private InterfaceExp interfaceExp;

	@XmlElement(name="exp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public InterfaceExp getInterfaceExp() {
		return interfaceExp;
	}

	public void setInterfaceExp(InterfaceExp interfaceExp) {
		this.interfaceExp = interfaceExp;
	}

	@Override
	public String toString() {
		return "InterfaceRewriteRules [interfaceExp=" + interfaceExp + "]";
	}
	
	
}
