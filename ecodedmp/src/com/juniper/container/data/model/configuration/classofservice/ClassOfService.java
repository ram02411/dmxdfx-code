package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import com.juniper.container.data.model.routinginstances.RoutingInstances;

public class ClassOfService {

	private ClassOfServiceClassifiers classifiers;
	private CodePointAliases codePointAliases;
	private HostOutboundTraffic hostOutboundTraffic;
	private ForwardingClasses forwardingClasses;
	private SystemDefaults systemDefaults;
	private Interfaces interfaces;
	private List<ClassOfServiceRoutingInstances> routingInstances;
	private RewriteRules rewriteRules;
	private List<SchedulerMaps> schedulerMaps;
	private List<Schedulers> schedulers;

	@XmlElement(name="classifiers",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ClassOfServiceClassifiers getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(ClassOfServiceClassifiers classifiers) {
		this.classifiers = classifiers;
	}
	
	@XmlElement(name="code-point-aliases",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public CodePointAliases getCodePointAliases() {
		return codePointAliases;
	}

	public void setCodePointAliases(CodePointAliases codePointAliases) {
		this.codePointAliases = codePointAliases;
	}

	@XmlElement(name="host-outbound-traffic",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public HostOutboundTraffic getHostOutboundTraffic() {
		return hostOutboundTraffic;
	}

	public void setHostOutboundTraffic(HostOutboundTraffic hostOutboundTraffic) {
		this.hostOutboundTraffic = hostOutboundTraffic;
	}
	
	@XmlElement(name="forwarding-classes",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ForwardingClasses getForwardingClasses() {
		return forwardingClasses;
	}

	public void setForwardingClasses(ForwardingClasses forwardingClasses) {
		this.forwardingClasses = forwardingClasses;
	}

	@XmlElement(name="system-defaults",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public SystemDefaults getSystemDefaults() {
		return systemDefaults;
	}

	public void setSystemDefaults(SystemDefaults systemDefaults) {
		this.systemDefaults = systemDefaults;
	}

	@XmlElement(name="interfaces",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Interfaces getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(Interfaces interfaces) {
		this.interfaces = interfaces;
	}

	@XmlElement(name="routing-instances",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<ClassOfServiceRoutingInstances> getRoutingInstances() {
		return routingInstances;
	}

	public void setRoutingInstances(List<ClassOfServiceRoutingInstances> routingInstances) {
		this.routingInstances = routingInstances;
	}

	@XmlElement(name="rewrite-rules",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RewriteRules getRewriteRules() {
		return rewriteRules;
	}

	public void setRewriteRules(RewriteRules rewriteRules) {
		this.rewriteRules = rewriteRules;
	}

	@XmlElement(name="scheduler-maps",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<SchedulerMaps> getSchedulerMaps() {
		return schedulerMaps;
	}

	public void setSchedulerMaps(List<SchedulerMaps> schedulerMaps) {
		this.schedulerMaps = schedulerMaps;
	}

	@XmlElement(name="schedulers",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Schedulers> getSchedulers() {
		return schedulers;
	}

	public void setSchedulers(List<Schedulers> schedulers) {
		this.schedulers = schedulers;
	}

	@Override
	public String toString() {
		return "ClassOfService [classifiers=" + classifiers + ", codePointAliases=" + codePointAliases
				+ ", hostOutboundTraffic=" + hostOutboundTraffic + ", forwardingClasses=" + forwardingClasses
				+ ", systemDefaults=" + systemDefaults + ", interfaces=" + interfaces + ", routingInstances="
				+ routingInstances + ", rewriteRules=" + rewriteRules + ", schedulerMaps=" + schedulerMaps
				+ ", schedulers=" + schedulers + "]";
	}

}
