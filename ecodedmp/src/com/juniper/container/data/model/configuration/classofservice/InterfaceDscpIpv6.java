package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceDscpIpv6 {
	private String name;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "InterfaceDscpIpv6 [name=" + name + "]";
	}
	
}
