package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceClassifiers {
	private InterfaceDscpIpv6 interfaceDscpIpv6;

	@XmlElement(name="dscp-ipv6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public InterfaceDscpIpv6 getInterfaceDscpIpv6() {
		return interfaceDscpIpv6;
	}

	public void setInterfaceDscpIpv6(InterfaceDscpIpv6 interfaceDscpIpv6) {
		this.interfaceDscpIpv6 = interfaceDscpIpv6;
	}

	@Override
	public String toString() {
		return "InterfaceClassifiers [interfaceDscpIpv6=" + interfaceDscpIpv6 + "]";
	}

}
