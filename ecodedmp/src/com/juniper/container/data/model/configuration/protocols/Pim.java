package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Pim {
	
	private List<PimInterface> pinterface;

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<PimInterface> getPinterface() {
		return pinterface;
	}

	public void setPinterface(List<PimInterface> pinterface) {
		this.pinterface = pinterface;
	}

	@Override
	public String toString() {
		return "Pim [pinterface=" + pinterface + "]";
	}
	
}
