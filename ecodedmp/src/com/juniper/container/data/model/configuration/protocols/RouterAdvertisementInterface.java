package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class RouterAdvertisementInterface {
	
	private String name;
	private Prefix prefix;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="prefix",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Prefix getPrefix() {
		return prefix;
	}

	public void setPrefix(Prefix prefix) {
		this.prefix = prefix;
	}

	@Override
	public String toString() {
		return "RouterAdvertisementInterface [name=" + name + ", prefix=" + prefix + "]";
	}

}
