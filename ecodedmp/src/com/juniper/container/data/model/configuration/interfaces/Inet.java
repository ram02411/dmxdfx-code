package com.juniper.container.data.model.configuration.interfaces;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Inet {
	
	private List<Address> address;

	@XmlElement(name="address",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Address> getAddress() {
		return address;
	}

	public void setAddress(List<Address> address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Inet [address=" + address + "]";
	}
	
	
}
