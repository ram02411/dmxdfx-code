package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class CodeDscpIpv6 {
	private String name;
	private String bits;
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="bits",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getBits() {
		return bits;
	}
	public void setBits(String bits) {
		this.bits = bits;
	}
	@Override
	public String toString() {
		return "CodeDscpIpv6 [name=" + name + ", bits=" + bits + "]";
	}
	
	
}
