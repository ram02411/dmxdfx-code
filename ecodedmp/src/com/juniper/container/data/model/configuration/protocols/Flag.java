package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Flag {
	private String name;
    private String detail;
    
    @XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	  @XmlElement(name="detail",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDetail() {
		return detail;
	}


	public void setDetail(String detail) {
		this.detail = detail;
	}


	@Override
	public String toString() {
		return "Flag [name=" + name + ", detail=" + detail + "]";
	}
    
    
}
