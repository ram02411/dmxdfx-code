package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class BgpFile {
	
	private String filename;

	@XmlElement(name="filename",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Override
	public String toString() {
		return "BgpFile [filename=" + filename + "]";
	}

}
