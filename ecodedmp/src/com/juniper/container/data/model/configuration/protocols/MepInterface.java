package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class MepInterface {
	
	private String interfacename;

	@XmlElement(name="interface-name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getInterfacename() {
		return interfacename;
	}

	public void setInterfacename(String interfacename) {
		this.interfacename = interfacename;
	}

	@Override
	public String toString() {
		return "MepInterface [interfacename=" + interfacename + "]";
	}
	
	
	
}
