package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class IsisInterface {
	
	private String name;
	private String ldpsynchronization;
	private String lspinterval;
	private String pointtopoint;
	private String nodelinkprotection;
	private BfdLivenessDetection bfdLivenessDetection;
	private InterfaceLevel interfaceLevel;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="ldp-synchronization",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getLdpsynchronization() {
		return ldpsynchronization;
	}

	public void setLdpsynchronization(String ldpsynchronization) {
		this.ldpsynchronization = ldpsynchronization;
	}

	@XmlElement(name="lsp-interval",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getLspinterval() {
		return lspinterval;
	}

	public void setLspinterval(String lspinterval) {
		this.lspinterval = lspinterval;
	}

	@XmlElement(name="point-to-point",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPointtopoint() {
		return pointtopoint;
	}

	public void setPointtopoint(String pointtopoint) {
		this.pointtopoint = pointtopoint;
	}

	@XmlElement(name="node-link-protection",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getNodelinkprotection() {
		return nodelinkprotection;
	}

	public void setNodelinkprotection(String nodelinkprotection) {
		this.nodelinkprotection = nodelinkprotection;
	}

	@XmlElement(name="bfd-liveness-detection",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public BfdLivenessDetection getBfdLivenessDetection() {
		return bfdLivenessDetection;
	}

	public void setBfdLivenessDetection(BfdLivenessDetection bfdLivenessDetection) {
		this.bfdLivenessDetection = bfdLivenessDetection;
	}

	@XmlElement(name="level",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public InterfaceLevel getInterfaceLevel() {
		return interfaceLevel;
	}

	public void setInterfaceLevel(InterfaceLevel interfaceLevel) {
		this.interfaceLevel = interfaceLevel;
	}

	@Override
	public String toString() {
		return "IsisInterface [name=" + name + ", ldpsynchronization=" + ldpsynchronization + ", lspinterval="
				+ lspinterval + ", pointtopoint=" + pointtopoint + ", nodelinkprotection=" + nodelinkprotection
				+ ", bfdLivenessDetection=" + bfdLivenessDetection + ", interfaceLevel=" + interfaceLevel + "]";
	}

}
