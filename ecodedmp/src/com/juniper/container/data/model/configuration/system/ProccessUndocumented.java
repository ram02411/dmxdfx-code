package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class ProccessUndocumented {

	private Management management;

	@XmlElement(name="management",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Management getManagement() {
		return management;
	}

	public void setManagement(Management management) {
		this.management = management;
	}

	@Override
	public String toString() {
		return "ProccessUndocumented [management=" + management + "]";
	}

}
