package com.juniper.container.data.model.configuration.groups;

import javax.xml.bind.annotation.XmlElement;

public class Ldp {
	
	private SessionGroup sessionGroup;

	@XmlElement(name="session-group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public SessionGroup getSessionGroup() {
		return sessionGroup;
	}

	public void setSessionGroup(SessionGroup sessionGroup) {
		this.sessionGroup = sessionGroup;
	}

	@Override
	public String toString() {
		return "Ldp [sessionGroup=" + sessionGroup + "]";
	}
}
