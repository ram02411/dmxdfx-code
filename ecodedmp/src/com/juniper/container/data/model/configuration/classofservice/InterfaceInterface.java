package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceInterface {
	private String name;
	private ShapingRate shapingRate;
	private String schedulerMap;
	private List<Unit> unit;
	private InterfaceClassifiers  classifiers;
	
	
	@XmlElement(name="shaping-rate",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ShapingRate getShapingRate() {
		return shapingRate;
	}

	public void setShapingRate(ShapingRate shapingRate) {
		this.shapingRate = shapingRate;
	}

	@XmlElement(name="classifiers",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public InterfaceClassifiers getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(InterfaceClassifiers classifiers) {
		this.classifiers = classifiers;
	}

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="scheduler-map",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getSchedulerMap() {
		return schedulerMap;
	}

	public void setSchedulerMap(String schedulerMap) {
		this.schedulerMap = schedulerMap;
	}

	@XmlElement(name="unit",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Unit> getUnit() {
		return unit;
	}

	public void setUnit(List<Unit> unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "InterfaceInterface [name=" + name + ", shapingRate=" + shapingRate + ", schedulerMap=" + schedulerMap
				+ ", unit=" + unit + ", classifiers=" + classifiers + "]";
	}


}
