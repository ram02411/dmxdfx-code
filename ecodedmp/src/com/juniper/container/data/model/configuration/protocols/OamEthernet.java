package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class OamEthernet {
	
	private ConnectivityFaultManagement connectivityFaultManagement;

	@XmlElement(name="connectivity-fault-management",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ConnectivityFaultManagement getConnectivityFaultManagement() {
		return connectivityFaultManagement;
	}

	public void setConnectivityFaultManagement(
			ConnectivityFaultManagement connectivityFaultManagement) {
		this.connectivityFaultManagement = connectivityFaultManagement;
	}

	@Override
	public String toString() {
		return "OamEthernet [connectivityFaultManagement="
				+ connectivityFaultManagement + "]";
	}
	
		
}
