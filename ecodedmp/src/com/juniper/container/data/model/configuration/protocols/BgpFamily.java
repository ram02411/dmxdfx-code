package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class BgpFamily {
	private BgpFamilyInet6 inet6;

	@XmlElement(name="inet6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public BgpFamilyInet6 getInet6() {
		return inet6;
	}

	public void setInet6(BgpFamilyInet6 inet6) {
		this.inet6 = inet6;
	}

	@Override
	public String toString() {
		return "BgpFamily [inet6=" + inet6 + "]";
	}
	
	
}
