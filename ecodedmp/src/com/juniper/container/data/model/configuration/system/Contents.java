package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class Contents {
	private String name;
	private String emergency;
	private String notice;
	private String error;
	private String info;
	private String any;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="emergency",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getEmergency() {
		return emergency;
	}

	public void setEmergency(String emergency) {
		this.emergency = emergency;
	}

	@XmlElement(name="notice",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getNotice() {
		return notice;
	}

	public void setNotice(String notice) {
		this.notice = notice;
	}

	@XmlElement(name="error",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@XmlElement(name="info",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@XmlElement(name="any",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAny() {
		return any;
	}

	public void setAny(String any) {
		this.any = any;
	}

	@Override
	public String toString() {
		return "Contents [name=" + name + ", emergency=" + emergency
				+ ", notice=" + notice + ", error=" + error + ", info=" + info
				+ ", any=" + any + "]";
	}

}
