package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Bgp {
	private Traceoptions traceoptions;
	private BgpFamily family;
	private String authenticationkey;
	private List<BgpGroup> bgpGroup;

	@XmlElement(name="traceoptions",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Traceoptions getTraceoptions() {
		return traceoptions;
	}

	public void setTraceoptions(Traceoptions traceoptions) {
		this.traceoptions = traceoptions;
	}
	
	@XmlElement(name="family",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public BgpFamily getFamily() {
		return family;
	}

	public void setFamily(BgpFamily family) {
		this.family = family;
	}

	@XmlElement(name="authentication-key",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthenticationkey() {
		return authenticationkey;
	}

	public void setAuthenticationkey(String authenticationkey) {
		this.authenticationkey = authenticationkey;
	}

	@XmlElement(name="group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<BgpGroup> getBgpGroup() {
		return bgpGroup;
	}

	public void setBgpGroup(List<BgpGroup> bgpGroup) {
		this.bgpGroup = bgpGroup;
	}

	@Override
	public String toString() {
		return "Bgp [traceoptions=" + traceoptions + ", family=" + family + ", authenticationkey=" + authenticationkey
				+ ", bgpGroup=" + bgpGroup + "]";
	}
}
