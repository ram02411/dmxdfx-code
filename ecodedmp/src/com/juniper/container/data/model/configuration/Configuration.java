package com.juniper.container.data.model.configuration;

import javax.xml.bind.annotation.XmlElement;

import com.juniper.container.data.model.configuration.bridgedomains.BridgeDomains;
import com.juniper.container.data.model.configuration.classofservice.ClassOfService;
import com.juniper.container.data.model.configuration.groups.Groups;
import com.juniper.container.data.model.configuration.interfaces.CInterfaces;
import com.juniper.container.data.model.configuration.policyoptions.PolicyOptions;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.juniper.container.data.model.configuration.routingoptions.RoutingOptions;
import com.juniper.container.data.model.configuration.snmp.Snmp;
import com.juniper.container.data.model.routinginstances.RoutingInstances;

public class Configuration {
	private String  version;
	private Groups groups;
	private com.juniper.container.data.model.configuration.system.System system;
	private CInterfaces interfaces;
	private Snmp snmp;
	private RoutingOptions routingOptions;
	private CProtocols protocols;
	private PolicyOptions policyOptions;
	private ClassOfService classOfService;
	private RoutingInstances routingInstances;
	private BridgeDomains bridgeDomains;

	@XmlElement(name="version",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name="protocols",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public CProtocols getProtocols() {
		return protocols;
	}

	public void setProtocols(CProtocols protocols) {
		this.protocols = protocols;
	}

	@XmlElement(name="routing-options",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RoutingOptions getRoutingOptions() {
		return routingOptions;
	}

	public void setRoutingOptions(RoutingOptions routingOptions) {
		this.routingOptions = routingOptions;
	}

	@XmlElement(name="snmp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Snmp getSnmp() {
		return snmp;
	}

	public void setSnmp(Snmp snmp) {
		this.snmp = snmp;
	}

	@XmlElement(name="interfaces",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public CInterfaces getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(CInterfaces interfaces) {
		this.interfaces = interfaces;
	}

	@XmlElement(name="system",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public com.juniper.container.data.model.configuration.system.System getSystem() {
		return system;
	}

	public void setSystem(com.juniper.container.data.model.configuration.system.System system) {
		this.system = system;
	}

	@XmlElement(name="policy-options",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public PolicyOptions getPolicyOptions() {
		return policyOptions;
	}

	public void setPolicyOptions(PolicyOptions policyOptions) {
		this.policyOptions = policyOptions;
	}

	@XmlElement(name="class-of-service",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ClassOfService getClassOfService() {
		return classOfService;
	}

	public void setClassOfService(ClassOfService classOfService) {
		this.classOfService = classOfService;
	}

	@XmlElement(name="routing-instances",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RoutingInstances getRoutingInstances() {
		return routingInstances;
	}

	public void setRoutingInstances(RoutingInstances routingInstances) {
		this.routingInstances = routingInstances;
	}

	@XmlElement(name="bridge-domains",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public BridgeDomains getBridgeDomains() {
		return bridgeDomains;
	}

	public void setBridgeDomains(BridgeDomains bridgeDomains) {
		this.bridgeDomains = bridgeDomains;
	}

	@XmlElement(name="groups",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Groups getGroups() {
		return groups;
	}

	public void setGroups(Groups groups) {
		this.groups = groups;
	}

	@Override
	public String toString() {
		return "Configuration [version=" + version + ", groups=" + groups + ", system=" + system + ", interfaces="
				+ interfaces + ", snmp=" + snmp + ", routingOptions=" + routingOptions + ", protocols=" + protocols
				+ ", policyOptions=" + policyOptions + ", classOfService=" + classOfService + ", routingInstances="
				+ routingInstances + ", bridgeDomains=" + bridgeDomains + "]";
	}

	



}
