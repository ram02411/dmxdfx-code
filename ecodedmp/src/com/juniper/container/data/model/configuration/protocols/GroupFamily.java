package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class GroupFamily {
	
	private FamilyInet6 inet6;
	private FamilyInet inet;
	private FamilyInetVpn inetVpn;
	private FamilyInet6Vpn inet6Vpn;
	
	@XmlElement(name="inet",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public FamilyInet getInet() {
		return inet;
	}

	public void setInet(FamilyInet inet) {
		this.inet = inet;
	}

	@XmlElement(name="inet-vpn",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public FamilyInetVpn getInetVpn() {
		return inetVpn;
	}

	public void setInetVpn(FamilyInetVpn inetVpn) {
		this.inetVpn = inetVpn;
	}

	@XmlElement(name="inet6-vpn",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public FamilyInet6Vpn getInet6Vpn() {
		return inet6Vpn;
	}

	public void setInet6Vpn(FamilyInet6Vpn inet6Vpn) {
		this.inet6Vpn = inet6Vpn;
	}

	@XmlElement(name="inet6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public FamilyInet6 getInet6() {
		return inet6;
	}

	public void setInet6(FamilyInet6 inet6) {
		this.inet6 = inet6;
	}

	@Override
	public String toString() {
		return "GroupFamily [inet6=" + inet6 + ", inet=" + inet + ", inetVpn=" + inetVpn + ", inet6Vpn=" + inet6Vpn
				+ "]";
	}
	
}
