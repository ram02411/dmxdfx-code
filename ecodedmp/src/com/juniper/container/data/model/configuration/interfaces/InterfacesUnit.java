package com.juniper.container.data.model.configuration.interfaces;

import javax.xml.bind.annotation.XmlElement;

public class InterfacesUnit {
	private String name;
	private String description;
	private String vlanid;
	private String statistics;
	private Family family;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="description",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="vlan-id",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVlanid() {
		return vlanid;
	}

	public void setVlanid(String vlanid) {
		this.vlanid = vlanid;
	}

	@XmlElement(name="statistics",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getStatistics() {
		return statistics;
	}

	public void setStatistics(String statistics) {
		this.statistics = statistics;
	}

	@XmlElement(name="family",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	@Override
	public String toString() {
		return "InterfacesUnit [name=" + name + ", description=" + description
				+ ", vlanid=" + vlanid + ", statistics=" + statistics
				+ ", family=" + family + "]";
	}

}
