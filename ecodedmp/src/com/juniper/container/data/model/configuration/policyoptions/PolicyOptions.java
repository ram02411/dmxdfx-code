package com.juniper.container.data.model.configuration.policyoptions;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class PolicyOptions {
	
	private List<Community> community;
	private List<PolicyStatement> policyStatement;

	@XmlElement(name="policy-statement",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<PolicyStatement> getPolicyStatement() {
		return policyStatement;
	}

	public void setPolicyStatement(List<PolicyStatement> policyStatement) {
		this.policyStatement = policyStatement;
	}

	@XmlElement(name="community",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Community> getCommunity() {
		return community;
	}

	public void setCommunity(List<Community> community) {
		this.community = community;
	}

	@Override
	public String toString() {
		return "PolicyOptions [community=" + community + ", policyStatement=" + policyStatement + "]";
	}
	
}
