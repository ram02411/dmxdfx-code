package com.juniper.container.data.model.configuration.groups;

import javax.xml.bind.annotation.XmlElement;

public class SessionGroup {

	private String name;
	private String authenticationKey;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="authentication-key",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthenticationKey() {
		return authenticationKey;
	}

	public void setAuthenticationKey(String authenticationKey) {
		this.authenticationKey = authenticationKey;
	}

	@Override
	public String toString() {
		return "SessionGroup [name=" + name + ", authenticationKey=" + authenticationKey + "]";
	}

}
