package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class ForwardingTable {

	private String export;
	private ChainedCompositeNextHop chainedCompositeNextHop;

	@XmlElement(name="export",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getExport() {
		return export;
	}

	public void setExport(String export) {
		this.export = export;
	}

	@XmlElement(name="chained-composite-next-hop",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ChainedCompositeNextHop getChainedCompositeNextHop() {
		return chainedCompositeNextHop;
	}

	public void setChainedCompositeNextHop(
			ChainedCompositeNextHop chainedCompositeNextHop) {
		this.chainedCompositeNextHop = chainedCompositeNextHop;
	}

	@Override
	public String toString() {
		return "ForwardingTable [export=" + export
				+ ", chainedCompositeNextHop=" + chainedCompositeNextHop + "]";
	}

}
