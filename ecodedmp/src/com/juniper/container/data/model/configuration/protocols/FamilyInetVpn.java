package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class FamilyInetVpn {
	
	private String unicast;

	@XmlElement(name="unicast",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getUnicast() {
		return unicast;
	}

	public void setUnicast(String unicast) {
		this.unicast = unicast;
	}

	@Override
	public String toString() {
		return "FamilyInet [unicast=" + unicast + "]";
	}

}
