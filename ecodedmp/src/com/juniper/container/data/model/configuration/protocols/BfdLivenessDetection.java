package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class BfdLivenessDetection {
	
	private String minimuminterval;
	private String multiplier;
	private String noadaptation;

	@XmlElement(name="minimum-interval",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMinimuminterval() {
		return minimuminterval;
	}

	public void setMinimuminterval(String minimuminterval) {
		this.minimuminterval = minimuminterval;
	}

	@XmlElement(name="multiplier",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(String multiplier) {
		this.multiplier = multiplier;
	}

	@XmlElement(name="no-adaptation",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getNoadaptation() {
		return noadaptation;
	}

	public void setNoadaptation(String noadaptation) {
		this.noadaptation = noadaptation;
	}

	@Override
	public String toString() {
		return "BfdLivenessDetection [minimuminterval=" + minimuminterval + ", multiplier=" + multiplier
				+ ", noadaptation=" + noadaptation + "]";
	}

}
