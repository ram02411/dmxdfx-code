package com.juniper.container.data.model.configuration.snmp;

import javax.xml.bind.annotation.XmlElement;

public class SnmpCommunity {
	private String name;
	private String authorization;
	private Clients clients;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	@XmlElement(name="authorization",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthorization() {
		return authorization;
	}

	public void setAuthorization(String authorization) {
		this.authorization = authorization;
	}

	@XmlElement(name="clients",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Clients getClients() {
		return clients;
	}

	public void setClients(Clients clients) {
		this.clients = clients;
	}

	@Override
	public String toString() {
		return "Community [name=" + name + ", authorization=" + authorization
				+ ", clients=" + clients + "]";
	}
	
	
}
