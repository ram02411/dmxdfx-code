package com.juniper.container.data.model.configuration.groups;

import javax.xml.bind.annotation.XmlElement;

public class Groups {
	private String name;
	private Protocols protocols;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="protocols",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Protocols getProtocols() {
		return protocols;
	}

	public void setProtocols(Protocols protocols) {
		this.protocols = protocols;
	}

	@Override
	public String toString() {
		return "Groups [name=" + name + ", protocols=" + protocols + "]";
	}
	
	

}
