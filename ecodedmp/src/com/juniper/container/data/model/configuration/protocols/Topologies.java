package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Topologies {
	private String ipv6unicast;

	@XmlElement(name="ipv6-unicast",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getIpv6unicast() {
		return ipv6unicast;
	}

	public void setIpv6unicast(String ipv6unicast) {
		this.ipv6unicast = ipv6unicast;
	}

	@Override
	public String toString() {
		return "Topologies [ipv6unicast=" + ipv6unicast + "]";
	}

}
