package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class ProtocolsLdp {

	private String applygroups;
	private AutoTargetedSession autoTargetedSession;
	private String deaggregate;
	private String explicitnull;
	private LdpInterface linterface;
	private ProtocolSessionGroup sessionGroup;
	private String sessionprotection;
	private IgpSynchronization igpSynchronization;

	@XmlElement(name="apply-groups",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getApplygroups() {
		return applygroups;
	}

	public void setApplygroups(String applygroups) {
		this.applygroups = applygroups;
	}

	@XmlElement(name="auto-targeted-session",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public AutoTargetedSession getAutoTargetedSession() {
		return autoTargetedSession;
	}

	public void setAutoTargetedSession(AutoTargetedSession autoTargetedSession) {
		this.autoTargetedSession = autoTargetedSession;
	}

	@XmlElement(name="deaggregate",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDeaggregate() {
		return deaggregate;
	}

	public void setDeaggregate(String deaggregate) {
		this.deaggregate = deaggregate;
	}
	
	@XmlElement(name="explicit-null",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getExplicitnull() {
		return explicitnull;
	}

	public void setExplicitnull(String explicitnull) {
		this.explicitnull = explicitnull;
	}

	
	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public LdpInterface getLinterface() {
		return linterface;
	}

	public void setLinterface(LdpInterface linterface) {
		this.linterface = linterface;
	}

	
	@XmlElement(name="session-group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ProtocolSessionGroup getSessionGroup() {
		return sessionGroup;
	}

	public void setSessionGroup(ProtocolSessionGroup sessionGroup) {
		this.sessionGroup = sessionGroup;
	}

	@XmlElement(name="session-protection",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getSessionprotection() {
		return sessionprotection;
	}

	public void setSessionprotection(String sessionprotection) {
		this.sessionprotection = sessionprotection;
	}

	@XmlElement(name="igp-synchronization",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public IgpSynchronization getIgpSynchronization() {
		return igpSynchronization;
	}

	public void setIgpSynchronization(IgpSynchronization igpSynchronization) {
		this.igpSynchronization = igpSynchronization;
	}

	@Override
	public String toString() {
		return "ProtocolsLdp [applygroups=" + applygroups
				+ ", autoTargetedSession=" + autoTargetedSession
				+ ", deaggregate=" + deaggregate + ", explicitnull="
				+ explicitnull + ", linterface=" + linterface
				+ ", sessionGroup=" + sessionGroup + ", sessionprotection="
				+ sessionprotection + ", igpSynchronization="
				+ igpSynchronization + "]";
	}

}
