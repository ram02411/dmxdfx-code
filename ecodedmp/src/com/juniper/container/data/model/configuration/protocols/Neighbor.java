package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"name", "peeras","localas"})
public class Neighbor {
	
	private String name;
	private String peeras;
	private LocalAs localas;
	
	@XmlElement(name="peer-as",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPeeras() {
		return peeras;
	}

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	
	@XmlElement(name="local-as",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public LocalAs getLocalas() {
		return localas;
	}

	public void setLocalas(LocalAs localas) {
		this.localas = localas;
	}

	

	public void setPeeras(String peeras) {
		this.peeras = peeras;
	}


	public void setName(String name) {
		this.name = name;
	}

}
