package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class MaintenanceAssociation {
	
	private String name;
	private Mep mep;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="mep",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Mep getMep() {
		return mep;
	}
	public void setMep(Mep mep) {
		this.mep = mep;
	}
	
	@Override
	public String toString() {
		return "MaintenanceAssociation [name=" + name + ", mep=" + mep + "]";
	}

	
	
}
