package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ClassifiersExp {
	private String name;
	private List<ClassifiersForwardingClass> forwardingClass;
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="forwarding-class",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<ClassifiersForwardingClass> getForwardingClass() {
		return forwardingClass;
	}
	public void setForwardingClass(List<ClassifiersForwardingClass> forwardingClass) {
		this.forwardingClass = forwardingClass;
	}
	@Override
	public String toString() {
		return "ClassifiersExp [name=" + name + ", forwardingClass=" + forwardingClass + "]";
	}
	
}
