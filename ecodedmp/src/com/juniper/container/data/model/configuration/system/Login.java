package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class Login {
	private String message;
	private User user;

	@XmlElement(name="message",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@XmlElement(name="user",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "Login [message=" + message + ", user=" + user + "]";
	}

}
