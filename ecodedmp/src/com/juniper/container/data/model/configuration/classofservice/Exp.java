package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Exp {
	private String name;
	private List<ExpForwardingClass> expForwardingClasses;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="forwarding-class",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<ExpForwardingClass> getExpForwardingClasses() {
		return expForwardingClasses;
	}

	public void setExpForwardingClasses(List<ExpForwardingClass> expForwardingClasses) {
		this.expForwardingClasses = expForwardingClasses;
	}

	@Override
	public String toString() {
		return "Exp [name=" + name + ", expForwardingClasses=" + expForwardingClasses + "]";
	}

}
