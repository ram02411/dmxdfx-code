package com.juniper.container.data.model.configuration.groups;

import javax.xml.bind.annotation.XmlElement;

public class Protocols {
	private Ldp ldp;

	@XmlElement(name="ldp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Ldp getLdp() {
		return ldp;
	}

	public void setLdp(Ldp ldp) {
		this.ldp = ldp;
	}

	@Override
	public String toString() {
		return "Protocols [ldp=" + ldp + "]";
	}

}
