package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class RouterAdvertisement {
	
		private List<RouterAdvertisementInterface> rinterface;

		@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
		public List<RouterAdvertisementInterface> getRinterface() {
			return rinterface;
		}

		public void setRinterface(List<RouterAdvertisementInterface> rinterface) {
			this.rinterface = rinterface;
		}

		@Override
		public String toString() {
			return "RouterAdvertisement [rinterface=" + rinterface + "]";
		}
}
