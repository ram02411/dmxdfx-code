package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class GroupGracefulRestart {
	private String restarttime;
	private String staleRoutestime;

	@XmlElement(name="restart-time",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRestarttime() {
		return restarttime;
	}

	public void setRestarttime(String restarttime) {
		this.restarttime = restarttime;
	}

	@XmlElement(name="stale-routes-time",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getStaleRoutestime() {
		return staleRoutestime;
	}

	public void setStaleRoutestime(String staleRoutestime) {
		this.staleRoutestime = staleRoutestime;
	}

	@Override
	public String toString() {
		return "GroupGracefulRestart [restarttime=" + restarttime + ", staleRoutestime=" + staleRoutestime + "]";
	}

}
