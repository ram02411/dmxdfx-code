package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class Classifiers {
	
	private DscpIpv6 dscpIpv6;

	@XmlElement(name="dscp-ipv6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public DscpIpv6 getDscpIpv6() {
		return dscpIpv6;
	}

	public void setDscpIpv6(DscpIpv6 dscpIpv6) {
		this.dscpIpv6 = dscpIpv6;
	}

	@Override
	public String toString() {
		return "Classifiers [dscpIpv6=" + dscpIpv6 + "]";
	}
	
	
}	
