package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class Ingress {
	
	private L3vpn l3vpn;

	@XmlElement(name="l3vpn",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public L3vpn getL3vpn() {
		return l3vpn;
	}

	public void setL3vpn(L3vpn l3vpn) {
		this.l3vpn = l3vpn;
	}

	@Override
	public String toString() {
		return "Ingress [l3vpn=" + l3vpn + "]";
	}
	
	
}
