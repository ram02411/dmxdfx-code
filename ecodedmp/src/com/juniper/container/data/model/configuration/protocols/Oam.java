package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class Oam {
	
	private String inactive;
	private OamEthernet ethernet;
	
	@XmlAttribute(name="inactive",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getInactive() {
		return inactive;
	}
	public void setInactive(String inactive) {
		this.inactive = inactive;
	}
	
	@XmlElement(name="ethernet",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public OamEthernet getEthernet() {
		return ethernet;
	}
	public void setEthernet(OamEthernet ethernet) {
		this.ethernet = ethernet;
	}
	@Override
	public String toString() {
		return "Oam [inactive=" + inactive + ", ethernet=" + ethernet + "]";
	}
	 
	
	
}
