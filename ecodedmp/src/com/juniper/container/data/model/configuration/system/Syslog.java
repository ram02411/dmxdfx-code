package com.juniper.container.data.model.configuration.system;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Syslog {
	private SyslogUser user;
	private Host host;
	private List<File> file;

	@XmlElement(name="user",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public SyslogUser getUser() {
		return user;
	}

	public void setUser(SyslogUser user) {
		this.user = user;
	}

	@XmlElement(name="host",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
	}

	@XmlElement(name="file",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<File> getFile() {
		return file;
	}

	public void setFile(List<File> file) {
		this.file = file;
	}

	@Override
	public String toString() {
		return "Syslog [user=" + user + ", host=" + host + ", file=" + file
				+ "]";
	}

}
