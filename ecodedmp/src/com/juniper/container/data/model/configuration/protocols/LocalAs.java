package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class LocalAs {

	private String asnumber;

	@XmlElement(name="as-number",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAsnumber() {
		return asnumber;
	}

	public void setAsnumber(String asnumber) {
		this.asnumber = asnumber;
	}

	@Override
	public String toString() {
		return "LocalAs [asnumber=" + asnumber + "]";
	}

}
