package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SchedulerMaps {
	private String name;
	private  List<ForwardingClass> forwardingClass;
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="forwarding-class",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<ForwardingClass> getForwardingClass() {
		return forwardingClass;
	}
	public void setForwardingClass(List<ForwardingClass> forwardingClass) {
		this.forwardingClass = forwardingClass;
	}
	@Override
	public String toString() {
		return "SchedulerMaps [name=" + name + ", forwardingClass=" + forwardingClass + "]";
	}
	
}
