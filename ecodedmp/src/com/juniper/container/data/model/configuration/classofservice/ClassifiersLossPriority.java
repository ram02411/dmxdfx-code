package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ClassifiersLossPriority {
	private String   name;
	private List<String> codepoints;
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name="code-points",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public  List<String> getCodepoints() {
		return codepoints;
	}
	public void setCodepoints( List<String> codepoints) {
		this.codepoints = codepoints;
	}
	@Override
	public String toString() {
		return "ClassifiersLossPriority [name=" + name + ", codepoints=" + codepoints + "]";
	}
	
	
}
