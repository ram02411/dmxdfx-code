package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class NetConf {
	
	private NetConfSSH ssh;

	@XmlElement(name="ssh",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public NetConfSSH getSsh() {
		return ssh;
	}

	public void setSsh(NetConfSSH ssh) {
		this.ssh = ssh;
	}

	@Override
	public String toString() {
		return "NetConf [ssh=" + ssh + "]";
	}
}
