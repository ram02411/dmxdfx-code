package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Multicast {
	
	private RibGroup ribGroup;

	@XmlElement(name="rib-group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RibGroup getRibGroup() {
		return ribGroup;
	}

	public void setRibGroup(RibGroup ribGroup) {
		this.ribGroup = ribGroup;
	}

	@Override
	public String toString() {
		return "Multicast [ribGroup=" + ribGroup + "]";
	}

}
