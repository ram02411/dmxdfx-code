package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class TransmitRate {
	
	private String percent;

	@XmlElement(name="percent",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPercent() {
		return percent;
	}

	public void setPercent(String percent) {
		this.percent = percent;
	}

	@Override
	public String toString() {
		return "TransmitRate [percent=" + percent + "]";
	}

}
