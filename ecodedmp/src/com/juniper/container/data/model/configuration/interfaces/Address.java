package com.juniper.container.data.model.configuration.interfaces;

import javax.xml.bind.annotation.XmlElement;

public class Address {
	private String name;
	private String primary;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="primary",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPrimary() {
		return primary;
	}

	public void setPrimary(String primary) {
		this.primary = primary;
	}

	@Override
	public String toString() {
		return "Address [name=" + name + ", primary=" + primary + "]";
	}

}
