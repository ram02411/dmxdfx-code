package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class RoutingOptions {
	private String medigpupdateinterval;
	private GracefulRestart gracefulRestart;
	private RibGroups ribGroups;
	private String routerid;
	private AutonomousSystem autonomousSystem;
	private ForwardingTable forwardingTable;

	@XmlElement(name="med-igp-update-interval",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMedigpupdateinterval() {
		return medigpupdateinterval;
	}

	public void setMedigpupdateinterval(String medigpupdateinterval) {
		this.medigpupdateinterval = medigpupdateinterval;
	}

	@XmlElement(name="graceful-restart",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public GracefulRestart getGracefulRestart() {
		return gracefulRestart;
	}

	public void setGracefulRestart(GracefulRestart gracefulRestart) {
		this.gracefulRestart = gracefulRestart;
	}

	@XmlElement(name="rib-groups",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RibGroups getRibGroups() {
		return ribGroups;
	}

	public void setRibGroups(RibGroups ribGroups) {
		this.ribGroups = ribGroups;
	}

	
	@XmlElement(name="router-id",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRouterid() {
		return routerid;
	}

	public void setRouterid(String routerid) {
		this.routerid = routerid;
	}

	@XmlElement(name="autonomous-system",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public AutonomousSystem getAutonomousSystem() {
		return autonomousSystem;
	}

	public void setAutonomousSystem(AutonomousSystem autonomousSystem) {
		this.autonomousSystem = autonomousSystem;
	}

	@XmlElement(name="forwarding-table",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ForwardingTable getForwardingTable() {
		return forwardingTable;
	}

	public void setForwardingTable(ForwardingTable forwardingTable) {
		this.forwardingTable = forwardingTable;
	}

	@Override
	public String toString() {
		return "RoutingOptions [medigpupdateinterval=" + medigpupdateinterval
				+ ", gracefulRestart=" + gracefulRestart + ", ribGroups="
				+ ribGroups + ", routerid=" + routerid + ", autonomousSystem="
				+ autonomousSystem + ", forwardingTable=" + forwardingTable
				+ "]";
	}

}
