package com.juniper.container.data.model.configuration.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Categories {
	private String authentication;
	private String chassis;
	private String link;
	private String remoteOperations;
	private String routing;
	private String startup;
	private String configuration;
	private String services;

	@XmlElement(name="authentication",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthentication() {
		return authentication;
	}

	public void setAuthentication(String authentication) {
		this.authentication = authentication;
	}

	@XmlElement(name="chassis",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getChassis() {
		return chassis;
	}

	public void setChassis(String chassis) {
		this.chassis = chassis;
	}

	@XmlElement(name="link",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@XmlElement(name="remote-operations",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRemoteOperations() {
		return remoteOperations;
	}

	public void setRemoteOperations(String remoteOperations) {
		this.remoteOperations = remoteOperations;
	}

	@XmlElement(name="routing",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	@XmlElement(name="startup",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getStartup() {
		return startup;
	}

	public void setStartup(String startup) {
		this.startup = startup;
	}

	@XmlElement(name="configuration",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getConfiguration() {
		return configuration;
	}

	public void setConfiguration(String configuration) {
		this.configuration = configuration;
	}

	@XmlElement(name="services",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getServices() {
		return services;
	}

	public void setServices(String services) {
		this.services = services;
	}

	@Override
	public String toString() {
		return "Categories [authentication=" + authentication + ", chassis="
				+ chassis + ", link=" + link + ", remoteOperations="
				+ remoteOperations + ", routing=" + routing + ", startup="
				+ startup + ", configuration=" + configuration + ", services="
				+ services + "]";
	}

}
