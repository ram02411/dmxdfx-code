package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Mpls {
	private String ipv6tunneling;
	private List<MplsInterface> minterface;

	@XmlElement(name="ipv6-tunneling",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getIpv6tunneling() {
		return ipv6tunneling;
	}

	public void setIpv6tunneling(String ipv6tunneling) {
		this.ipv6tunneling = ipv6tunneling;
	}

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<MplsInterface> getMinterface() {
		return minterface;
	}

	public void setMinterface(List<MplsInterface> minterface) {
		this.minterface = minterface;
	}

	@Override
	public String toString() {
		return "Mpls [ipv6tunneling=" + ipv6tunneling + ", minterface=" + minterface + "]";
	}

}
