package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class ClassOfServiceClassifiers {
	private ClassifiersDscpIpv6 dscpipv6;
	private ClassifiersExp exp;
	
	@XmlElement(name="dscp-ipv6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ClassifiersDscpIpv6 getDscpipv6() {
		return dscpipv6;
	}
	public void setDscpipv6(ClassifiersDscpIpv6 dscpipv6) {
		this.dscpipv6 = dscpipv6;
	}
	
	@XmlElement(name="exp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ClassifiersExp getExp() {
		return exp;
	}
	public void setExp(ClassifiersExp exp) {
		this.exp = exp;
	}
	@Override
	public String toString() {
		return "ClassOfServiceClassifiers [dscpipv6=" + dscpipv6 + ", exp=" + exp + "]";
	}
	
}
