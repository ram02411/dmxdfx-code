package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class Management {
	
	private String enable;

	@XmlElement(name="enable",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getEnable() {
		return enable;
	}

	public void setEnable(String enable) {
		this.enable = enable;
	}

	@Override
	public String toString() {
		return "Management [enable=" + enable + "]";
	}
	
	
}
