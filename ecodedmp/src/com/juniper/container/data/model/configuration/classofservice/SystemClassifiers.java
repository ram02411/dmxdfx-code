package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class SystemClassifiers {
		private SystemExp exp;

		@XmlElement(name="exp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
		public SystemExp getExp() {
			return exp;
		}

		public void setExp(SystemExp exp) {
			this.exp = exp;
		}

		@Override
		public String toString() {
			return "SystemClassifiers [exp=" + exp + "]";
		}
		
}
