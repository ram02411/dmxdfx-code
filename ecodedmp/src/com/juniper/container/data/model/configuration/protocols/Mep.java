package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Mep {
	
	private String name;
	private MepInterface minterface;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public MepInterface getMinterface() {
		return minterface;
	}
	public void setMinterface(MepInterface minterface) {
		this.minterface = minterface;
	}
	@Override
	public String toString() {
		return "Mep [name=" + name + ", minterface=" + minterface + "]";
	}
	
	
}
