package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class EastInterface {
	private ControlChannel controlChannel;

	@XmlElement(name="control-channel",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ControlChannel getControlChannel() {
		return controlChannel;
	}

	public void setControlChannel(ControlChannel controlChannel) {
		this.controlChannel = controlChannel;
	}

	@Override
	public String toString() {
		return "WestInterface [controlChannel=" + controlChannel + "]";
	}
	
	
}	
