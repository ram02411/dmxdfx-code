package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class LossPriority {
	private String name;
	private String codepoint;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="code-point",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getCodepoint() {
		return codepoint;
	}

	public void setCodepoint(String codepoint) {
		this.codepoint = codepoint;
	}

	@Override
	public String toString() {
		return "LossPriority [name=" + name + ", codepoint=" + codepoint + "]";
	}

}
