package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class Host {
	private String name;
	private Contents contents;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="contents",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Contents getContents() {
		return contents;
	}

	public void setContents(Contents contents) {
		this.contents = contents;
	}

	@Override
	public String toString() {
		return "Host [name=" + name + ", contents=" + contents + "]";
	}

}
