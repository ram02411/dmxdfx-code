package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class FClass {
	private String name;
	private String queuenum;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="queue-num",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getQueuenum() {
		return queuenum;
	}

	public void setQueuenum(String queuenum) {
		this.queuenum = queuenum;
	}

	@Override
	public String toString() {
		return "FClass [name=" + name + ", queuenum=" + queuenum + "]";
	}

}
