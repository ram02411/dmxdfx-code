package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class SystemExp {
	private String classifierName;

	@XmlElement(name="classifier-name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getClassifierName() {
		return classifierName;
	}

	public void setClassifierName(String classifierName) {
		this.classifierName = classifierName;
	}

	@Override
	public String toString() {
		return "SystemExp [classifierName=" + classifierName + "]";
	}
	
}
