package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class InterfaceLevel {
	private String name;
	private String disable;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="disable",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDisable() {
		return disable;
	}

	public void setDisable(String disable) {
		this.disable = disable;
	}

	@Override
	public String toString() {
		return "InterfaceLevel [name=" + name + ", disable=" + disable + "]";
	}

}
