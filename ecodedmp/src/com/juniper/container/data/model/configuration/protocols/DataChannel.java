package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class DataChannel {
	
	private List<String> vlan;

	@XmlElement(name="vlan",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<String> getVlan() {
		return vlan;
	}

	public void setVlan(List<String> vlan) {
		this.vlan = vlan;
	}

	@Override
	public String toString() {
		return "DataChannel [vlan=" + vlan + "]";
	}
		
}
