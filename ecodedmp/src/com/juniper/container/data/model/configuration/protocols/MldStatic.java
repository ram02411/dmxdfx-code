package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class MldStatic {
	
	private MldGroup group;

	@XmlElement(name="group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public MldGroup getGroup() {
		return group;
	}

	public void setGroup(MldGroup group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "MldStatic [group=" + group + "]";
	}
	

}
