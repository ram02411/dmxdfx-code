package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Interfaces {
	private List<InterfaceInterface> interfaces;

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<InterfaceInterface> getInterfaces() {
		return interfaces;
	}

	public void setInterfaces(List<InterfaceInterface> interfaces) {
		this.interfaces = interfaces;
	}

	@Override
	public String toString() {
		return "Interfaces [interfaces=" + interfaces + "]";
	}
	
}
