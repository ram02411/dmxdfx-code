package com.juniper.container.data.model.configuration.interfaces;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class CInterfaces {
	
	private List<InterfacesInterface> cinterface;

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<InterfacesInterface> getCinterface() {
		return cinterface;
	}

	public void setCinterface(List<InterfacesInterface> cinterface) {
		this.cinterface = cinterface;
	}

	@Override
	public String toString() {
		return "CInterfaces [cinterface=" + cinterface + "]";
	}
	
}
