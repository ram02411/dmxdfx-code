package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class LabeledUnicast {

	private String explicitnull;

	@XmlElement(name="explicit-null",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getExplicitnull() {
		return explicitnull;
	}



	public void setExplicitnull(String explicitnull) {
		this.explicitnull = explicitnull;
	}

	@Override
	public String toString() {
		return "LabeledUnicast [explicitnull=" + explicitnull + "]";
	}
	
}
