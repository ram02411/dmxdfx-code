package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class MldInterface {
	private String name;
	private String version;
	private MldStatic mstatic;
	private String  grouppolicy;
	private String  grouplimit;
	private String disable;
	
	@XmlElement(name="disable",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDisable() {
		return disable;
	}

	public void setDisable(String disable) {
		this.disable = disable;
	}

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="version",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name="static",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public MldStatic getMstatic() {
		return mstatic;
	}

	public void setMstatic(MldStatic mstatic) {
		this.mstatic = mstatic;
	}

	@XmlElement(name="group-policy",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getGrouppolicy() {
		return grouppolicy;
	}

	public void setGrouppolicy(String grouppolicy) {
		this.grouppolicy = grouppolicy;
	}

	@XmlElement(name="group-limit",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getGrouplimit() {
		return grouplimit;
	}

	public void setGrouplimit(String grouplimit) {
		this.grouplimit = grouplimit;
	}

	@Override
	public String toString() {
		return "MldInterface [name=" + name + ", version=" + version
				+ ", mstatic=" + mstatic + ", grouppolicy=" + grouppolicy
				+ ", grouplimit=" + grouplimit + ", disable=" + disable + "]";
	}

	
}
