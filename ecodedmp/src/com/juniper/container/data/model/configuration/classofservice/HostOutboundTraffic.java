package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class HostOutboundTraffic {
	
	private String forwardingClass;

	@XmlElement(name="forwarding-class",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getForwardingClass() {
		return forwardingClass;
	}

	public void setForwardingClass(String forwardingClass) {
		this.forwardingClass = forwardingClass;
	}

	@Override
	public String toString() {
		return "HostOutboundTraffic [forwardingClass=" + forwardingClass + "]";
	}
	
	
}
