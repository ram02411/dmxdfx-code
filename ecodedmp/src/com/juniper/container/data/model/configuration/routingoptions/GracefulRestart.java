package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class GracefulRestart {
	
	private String restartduration;

	@XmlElement(name="restart-duration",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRestartduration() {
		return restartduration;
	}

	public void setRestartduration(String restartduration) {
		this.restartduration = restartduration;
	}

	@Override
	public String toString() {
		return "GracefulRestart [restartduration=" + restartduration + "]";
	}
	
	
}
