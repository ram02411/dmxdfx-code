package com.juniper.container.data.model.configuration.system;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Processes {
	private ProccessUndocumented undocumented;
	private List<DaemonProcess> daemonProcesses;

	@XmlElement(name="undocumented",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ProccessUndocumented getUndocumented() {
		return undocumented;
	}

	public void setUndocumented(ProccessUndocumented undocumented) {
		this.undocumented = undocumented;
	}

	@XmlElement(name="daemon-process",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<DaemonProcess> getDaemonProcesses() {
		return daemonProcesses;
	}

	public void setDaemonProcesses(List<DaemonProcess> daemonProcesses) {
		this.daemonProcesses = daemonProcesses;
	}

	@Override
	public String toString() {
		return "Proccess [undocumented=" + undocumented + ", daemonProcesses="
				+ daemonProcesses + "]";
	}

}
