package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class Unit {
	private String name;
	private InterfaceRewriteRules interfaceRewriteRules;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="rewrite-rules",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public InterfaceRewriteRules getInterfaceRewriteRules() {
		return interfaceRewriteRules;
	}

	public void setInterfaceRewriteRules(InterfaceRewriteRules interfaceRewriteRules) {
		this.interfaceRewriteRules = interfaceRewriteRules;
	}

	@Override
	public String toString() {
		return "Unit [interfaceRewriteRules=" + interfaceRewriteRules + "]";
	}

}
