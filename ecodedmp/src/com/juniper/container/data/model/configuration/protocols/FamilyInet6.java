package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class FamilyInet6 {
	
	private Multicast multicast;
	private LabeledUnicast labeledUnicast;
	
	@XmlElement(name="labeled-unicast",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public LabeledUnicast getLabeledUnicast() {
		return labeledUnicast;
	}

	public void setLabeledUnicast(LabeledUnicast labeledUnicast) {
		this.labeledUnicast = labeledUnicast;
	}

	@XmlElement(name="multicast",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Multicast getMulticast() {
		return multicast;
	}

	public void setMulticast(Multicast multicast) {
		this.multicast = multicast;
	}

	@Override
	public String toString() {
		return "FamilyInet6 [multicast=" + multicast + ", labeledUnicast=" + labeledUnicast + "]";
	}

	
	

}
