package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class RootAuthentication {
	
	private String encryptedPassword;

	@XmlElement(name="encrypted-password",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getEncryptedPassword() {
		return encryptedPassword;
	}

	public void setEncryptedPassword(String encryptedPassword) {
		this.encryptedPassword = encryptedPassword;
	}

	@Override
	public String toString() {
		return "RootAuthentication [encryptedPassword=" + encryptedPassword
				+ "]";
	}

}
