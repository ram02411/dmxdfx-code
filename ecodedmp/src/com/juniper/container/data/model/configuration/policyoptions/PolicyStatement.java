package com.juniper.container.data.model.configuration.policyoptions;

import javax.xml.bind.annotation.XmlElement;

public class PolicyStatement {
	private String name;
	private Term term;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="term",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Term getTerm() {
		return term;
	}

	public void setTerm(Term term) {
		this.term = term;
	}

	@Override
	public String toString() {
		return "PolicyStatement [name=" + name + ", term=" + term + "]";
	}

}
