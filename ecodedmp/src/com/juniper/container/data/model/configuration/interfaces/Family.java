package com.juniper.container.data.model.configuration.interfaces;

import javax.xml.bind.annotation.XmlElement;

public class Family {

	private Inet inet;
	private Iso iso;
	private Inet6 inet6;
	private String mpls;
	private String bridge;
	
	@XmlElement(name="bridge",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getBridge() {
		return bridge;
	}

	public void setBridge(String bridge) {
		this.bridge = bridge;
	}

	@XmlElement(name="mpls",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMpls() {
		return mpls;
	}

	public void setMpls(String mpls) {
		this.mpls = mpls;
	}

	@XmlElement(name="inet",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Inet getInet() {
		return inet;
	}

	public void setInet(Inet inet) {
		this.inet = inet;
	}

	@XmlElement(name="iso",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Iso getIso() {
		return iso;
	}

	public void setIso(Iso iso) {
		this.iso = iso;
	}

	@XmlElement(name="inet6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Inet6 getInet6() {
		return inet6;
	}

	public void setInet6(Inet6 inet6) {
		this.inet6 = inet6;
	}

	@Override
	public String toString() {
		return "Family [inet=" + inet + ", iso=" + iso + ", inet6=" + inet6
				+ ", mpls=" + mpls + ", bridge=" + bridge + "]";
	}

}
