package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class SystemDefaults {
	
	private SystemClassifiers classifiers;

	@XmlElement(name="classifiers",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public SystemClassifiers getClassifiers() {
		return classifiers;
	}

	public void setClassifiers(SystemClassifiers classifiers) {
		this.classifiers = classifiers;
	}

	@Override
	public String toString() {
		return "SystemDefaults [classifiers=" + classifiers + "]";
	}
	
	
}
