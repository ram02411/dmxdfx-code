package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class ClassifiersForwardingClass {
	private String name;
	private ClassifiersLossPriority lossPriority;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="loss-priority",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ClassifiersLossPriority getLossPriority() {
		return lossPriority;
	}

	public void setLossPriority(ClassifiersLossPriority lossPriority) {
		this.lossPriority = lossPriority;
	}

	@Override
	public String toString() {
		return "ClassifiersForwardingClass [name=" + name + ", lossPriority=" + lossPriority + "]";
	}

}
