package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class AutoTargetedSession {
	private String teardownDelay;
	private String maximumSessions;

	@XmlElement(name="teardown-delay",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getTeardownDelay() {
		return teardownDelay;
	}

	public void setTeardownDelay(String teardownDelay) {
		this.teardownDelay = teardownDelay;
	}

	@XmlElement(name="maximum-sessions",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMaximumSessions() {
		return maximumSessions;
	}

	public void setMaximumSessions(String maximumSessions) {
		this.maximumSessions = maximumSessions;
	}

	@Override
	public String toString() {
		return "AutoTargetedSession [teardownDelay=" + teardownDelay
				+ ", maximumSessions=" + maximumSessions + "]";
	}

}
