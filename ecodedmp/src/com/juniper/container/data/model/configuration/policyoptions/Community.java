package com.juniper.container.data.model.configuration.policyoptions;

import javax.xml.bind.annotation.XmlElement;

public class Community {
	private String name;
	private String members;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@XmlElement(name="members",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getMembers() {
		return members;
	}

	public void setMembers(String members) {
		this.members = members;
	}

	@Override
	public String toString() {
		return "Community [name=" + name + ", members=" + members + "]";
	}

}
