package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class BgpFamilyInet6 {

	private BgpFamilyInet6LableUnicast labeledunicast;

	
	@XmlElement(name="labeled-unicast",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public BgpFamilyInet6LableUnicast getLabeledunicast() {
		return labeledunicast;
	}

	public void setLabeledunicast(BgpFamilyInet6LableUnicast labeledunicast) {
		this.labeledunicast = labeledunicast;
	}

	@Override
	public String toString() {
		return "BgpFamilyInet6 [labeledunicast=" + labeledunicast + "]";
	}
	
	
	 
}
