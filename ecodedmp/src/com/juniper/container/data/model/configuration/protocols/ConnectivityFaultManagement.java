package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ConnectivityFaultManagement {
	
	private List<MaintenanceDomain> maintenanceDomain;

	@XmlElement(name="maintenance-domain",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<MaintenanceDomain> getMaintenanceDomain() {
		return maintenanceDomain;
	}

	public void setMaintenanceDomain(List<MaintenanceDomain> maintenanceDomain) {
		this.maintenanceDomain = maintenanceDomain;
	}

	@Override
	public String toString() {
		return "ConnectivityFaultManagement [maintenanceDomain="
				+ maintenanceDomain + "]";
	}

}
