package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class Ntp {
	private String bootserver;
	private Server server;

	@XmlElement(name="boot-server",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getBootserver() {
		return bootserver;
	}

	public void setBootserver(String bootserver) {
		this.bootserver = bootserver;
	}

	@XmlElement(name="server",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Server getServer() {
		return server;
	}

	public void setServer(Server server) {
		this.server = server;
	}

	@Override
	public String toString() {
		return "Ntp [bootserver=" + bootserver + ", server=" + server + "]";
	}

}
