package com.juniper.container.data.model.configuration.interfaces;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class InterfacesInterface {
	private String name;
	private String description;
	private String flexibleVlanTagging;
	private String nativeVlanid;
	private String encapsulation;
	private List<InterfacesUnit> unit;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="description",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@XmlElement(name="flexible-vlan-tagging",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getFlexibleVlanTagging() {
		return flexibleVlanTagging;
	}

	public void setFlexibleVlanTagging(String flexibleVlanTagging) {
		this.flexibleVlanTagging = flexibleVlanTagging;
	}

	@XmlElement(name="native-vlan-id",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getNativeVlanid() {
		return nativeVlanid;
	}

	public void setNativeVlanid(String nativeVlanid) {
		this.nativeVlanid = nativeVlanid;
	}

	@XmlElement(name="encapsulation",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getEncapsulation() {
		return encapsulation;
	}

	public void setEncapsulation(String encapsulation) {
		this.encapsulation = encapsulation;
	}

	@XmlElement(name="unit",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<InterfacesUnit> getUnit() {
		return unit;
	}

	public void setUnit(List<InterfacesUnit> unit) {
		this.unit = unit;
	}

	@Override
	public String toString() {
		return "InterfacesInterface [name=" + name + ", description="
				+ description + ", flexibleVlanTagging=" + flexibleVlanTagging
				+ ", nativeVlanid=" + nativeVlanid + ", encapsulation="
				+ encapsulation + ", unit=" + unit + "]";
	}

}
