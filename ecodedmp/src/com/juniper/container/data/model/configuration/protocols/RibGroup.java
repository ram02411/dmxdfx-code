package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class RibGroup {
	
	private String ribgroupname;

	@XmlElement(name="ribgroup-name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRibgroupname() {
		return ribgroupname;
	}

	public void setRibgroupname(String ribgroupname) {
		this.ribgroupname = ribgroupname;
	}

	@Override
	public String toString() {
		return "RibGroup [ribgroupname=" + ribgroupname + "]";
	}

}
