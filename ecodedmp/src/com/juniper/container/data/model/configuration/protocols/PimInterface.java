package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class PimInterface {
  
	private String name;
	private String hellointerval;
	private String disable;
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="hello-interval",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getHellointerval() {
		return hellointerval;
	}

	public void setHellointerval(String hellointerval) {
		this.hellointerval = hellointerval;
	}

	@XmlElement(name="disable",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDisable() {
		return disable;
	}

	public void setDisable(String disable) {
		this.disable = disable;
	}

	@Override
	public String toString() {
		return "PimInterface [name=" + name + ", hellointerval="
				+ hellointerval + ", disable=" + disable + "]";
	}

	
}
