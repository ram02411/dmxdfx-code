package com.juniper.container.data.model.configuration.bridgedomains;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Domain {

	private String name;
	private String vlanid;
	private List<Interface> dinterface;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="vlan-id",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVlanid() {
		return vlanid;
	}

	public void setVlanid(String vlanid) {
		this.vlanid = vlanid;
	}

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Interface> getDinterface() {
		return dinterface;
	}

	public void setDinterface(List<Interface> dinterface) {
		this.dinterface = dinterface;
	}

	@Override
	public String toString() {
		return "Domain [name=" + name + ", vlanid=" + vlanid + ", dinterface=" + dinterface + "]";
	}

}
