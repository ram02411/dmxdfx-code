package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class NetConfSSH {
	
	private String port;

	@XmlElement(name="port",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "NetConfSSH [port=" + port + "]";
	}
	
}
