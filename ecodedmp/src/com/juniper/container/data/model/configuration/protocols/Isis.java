package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class Isis {
	private String export;
	private String referencebandwidth;
	private Topologies topologies;
	private Overload overload;
	private Level level;
	private List<IsisInterface> isisInterface;

	@XmlElement(name="export",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getExport() {
		return export;
	}

	public void setExport(String export) {
		this.export = export;
	}

	@XmlElement(name="reference-bandwidth",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getReferencebandwidth() {
		return referencebandwidth;
	}

	public void setReferencebandwidth(String referencebandwidth) {
		this.referencebandwidth = referencebandwidth;
	}

	@XmlElement(name="topologies",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Topologies getTopologies() {
		return topologies;
	}

	public void setTopologies(Topologies topologies) {
		this.topologies = topologies;
	}

	@XmlElement(name="overload",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Overload getOverload() {
		return overload;
	}

	public void setOverload(Overload overload) {
		this.overload = overload;
	}

	@XmlElement(name="level",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<IsisInterface> getIsisInterface() {
		return isisInterface;
	}

	public void setIsisInterface(List<IsisInterface> isisInterface) {
		this.isisInterface = isisInterface;
	}

	@Override
	public String toString() {
		return "Isis [export=" + export + ", referencebandwidth=" + referencebandwidth + ", topologies=" + topologies
				+ ", overload=" + overload + ", level=" + level + ", isisInterface=" + isisInterface + "]";
	}

}
