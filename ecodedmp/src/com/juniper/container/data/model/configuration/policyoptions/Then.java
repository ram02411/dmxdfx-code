package com.juniper.container.data.model.configuration.policyoptions;

import javax.xml.bind.annotation.XmlElement;

public class Then {
	private String accept;

	@XmlElement(name="accept",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAccept() {
		return accept;
	}

	public void setAccept(String accept) {
		this.accept = accept;
	}

	@Override
	public String toString() {
		return "Then [accept=" + accept + "]";
	}
	
	
}
