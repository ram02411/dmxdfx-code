package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class ForwardingClass {
	private String name;
	private String scheduler;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="scheduler",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getScheduler() {
		return scheduler;
	}

	public void setScheduler(String scheduler) {
		this.scheduler = scheduler;
	}

	@Override
	public String toString() {
		return "ForwardingClass [name=" + name + ", scheduler=" + scheduler + "]";
	}

}
