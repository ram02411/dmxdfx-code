package com.juniper.container.data.model.configuration.snmp;

import javax.xml.bind.annotation.XmlElement;

public class TrapGroup {

	private String name;
	private String version;
	private String destinationPort;
	private Categories categories;
	private Targets targets;
	
	
	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="version",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@XmlElement(name="destination-port",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDestinationPort() {
		return destinationPort;
	}

	public void setDestinationPort(String destinationPort) {
		this.destinationPort = destinationPort;
	}

	@XmlElement(name="categories",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Categories getCategories() {
		return categories;
	}

	public void setCategories(Categories categories) {
		this.categories = categories;
	}

	@XmlElement(name="targets",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Targets getTargets() {
		return targets;
	}

	public void setTargets(Targets targets) {
		this.targets = targets;
	}

	@Override
	public String toString() {
		return "TrapGroup [name=" + name + ", version=" + version
				+ ", destinationPort=" + destinationPort + ", categories="
				+ categories + ", targets=" + targets + "]";
	}
	
	

}
