package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class MaintenanceDomain {

	private String name;
	private String level;
	private MaintenanceAssociation maintenanceAssociation;

	@XmlElement(name = "name", namespace = "http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "level", namespace = "http://xml.juniper.net/xnm/1.1/xnm")
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@XmlElement(name = "maintenance-association", namespace = "http://xml.juniper.net/xnm/1.1/xnm")
	public MaintenanceAssociation getMaintenanceAssociation() {
		return maintenanceAssociation;
	}

	public void setMaintenanceAssociation(
			MaintenanceAssociation maintenanceAssociation) {
		this.maintenanceAssociation = maintenanceAssociation;
	}

	@Override
	public String toString() {
		return "MaintenanceDomain [name=" + name + ", level=" + level
				+ ", maintenanceAssociation=" + maintenanceAssociation + "]";
	}

}
