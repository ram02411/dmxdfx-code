package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class L3vpn {
	
	private RoutingUndocumented undocumented;

	@XmlElement(name="undocumented",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RoutingUndocumented getUndocumented() {
		return undocumented;
	}

	public void setUndocumented(RoutingUndocumented undocumented) {
		this.undocumented = undocumented;
	}

	@Override
	public String toString() {
		return "L3vpn [undocumented=" + undocumented + "]";
	}
	

}
