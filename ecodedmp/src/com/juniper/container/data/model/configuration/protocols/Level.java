package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Level {

	private String name;
	private String authenticationKey;
	private String authenticationType;
	private String nohelloAuthentication;
	private String widemetricsonly;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="authentication-key",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthenticationKey() {
		return authenticationKey;
	}

	public void setAuthenticationKey(String authenticationKey) {
		this.authenticationKey = authenticationKey;
	}

	@XmlElement(name="authentication-type",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthenticationType() {
		return authenticationType;
	}

	public void setAuthenticationType(String authenticationType) {
		this.authenticationType = authenticationType;
	}

	@XmlElement(name="no-hello-authentication",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getNohelloAuthentication() {
		return nohelloAuthentication;
	}

	public void setNohelloAuthentication(String nohelloAuthentication) {
		this.nohelloAuthentication = nohelloAuthentication;
	}

	@XmlElement(name="wide-metrics-only",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getWidemetricsonly() {
		return widemetricsonly;
	}

	public void setWidemetricsonly(String widemetricsonly) {
		this.widemetricsonly = widemetricsonly;
	}

	@Override
	public String toString() {
		return "Level [name=" + name + ", authenticationKey=" + authenticationKey + ", authenticationType="
				+ authenticationType + ", nohelloAuthentication=" + nohelloAuthentication + ", widemetricsonly="
				+ widemetricsonly + "]";
	}

}
