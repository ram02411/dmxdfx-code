package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class IgpSynchronization {

	private String holddowninterval;

	@XmlElement(name="holddown-interval",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getHolddowninterval() {
		return holddowninterval;
	}

	public void setHolddowninterval(String holddowninterval) {
		this.holddowninterval = holddowninterval;
	}

	@Override
	public String toString() {
		return "IgpSynchronization [holddowninterval=" + holddowninterval + "]";
	}
	
	
	
}
