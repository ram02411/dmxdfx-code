package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class ServicesSSH {
	private String protocolVersion;
	private String clientAliveCountMax;
	private String connectionLimit;

	@XmlElement(name="protocol-version",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getProtocolVersion() {
		return protocolVersion;
	}

	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	@XmlElement(name="client-alive-count-max",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getClientAliveCountMax() {
		return clientAliveCountMax;
	}

	public void setClientAliveCountMax(String clientAliveCountMax) {
		this.clientAliveCountMax = clientAliveCountMax;
	}

	@XmlElement(name="connection-limit",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getConnectionLimit() {
		return connectionLimit;
	}

	public void setConnectionLimit(String connectionLimit) {
		this.connectionLimit = connectionLimit;
	}

	@Override
	public String toString() {
		return "ServicesSSH [protocolVersion=" + protocolVersion
				+ ", clientAliveCountMax=" + clientAliveCountMax
				+ ", connectionLimit=" + connectionLimit + "]";
	}

}
