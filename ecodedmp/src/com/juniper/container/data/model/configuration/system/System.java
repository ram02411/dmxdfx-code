package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class System {
	private String hostname;
	private  RootAuthentication rootAuthentication;
	private Login login;
	private Services services;
	private Syslog syslog;
	private Processes processes;
	private Ntp ntp;
	
	@XmlElement(name="host-name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	@XmlElement(name="root-authentication",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public RootAuthentication getRootAuthentication() {
		return rootAuthentication;
	}
	public void setRootAuthentication(RootAuthentication rootAuthentication) {
		this.rootAuthentication = rootAuthentication;
	}
	
	@XmlElement(name="login",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Login getLogin() {
		return login;
	}
	
	public void setLogin(Login login) {
		this.login = login;
	}
	
	@XmlElement(name="services",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Services getServices() {
		return services;
	}
	public void setServices(Services services) {
		this.services = services;
	}
	
	@XmlElement(name="syslog",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Syslog getSyslog() {
		return syslog;
	}
	public void setSyslog(Syslog syslog) {
		this.syslog = syslog;
	}
	
	@XmlElement(name="processes",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Processes getProcesses() {
		return processes;
	}
	
	public void setProcesses(Processes processes) {
		this.processes = processes;
	}
	
	@XmlElement(name="processes",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Ntp getNtp() {
		return ntp;
	}
	public void setNtp(Ntp ntp) {
		this.ntp = ntp;
	}
	@Override
	public String toString() {
		return "System [hostname=" + hostname + ", rootAuthentication="
				+ rootAuthentication + ", login=" + login + ", services="
				+ services + ", syslog=" + syslog + ", process=" + processes
				+ ", ntp=" + ntp + "]";
	}
	
	
	
	
}
