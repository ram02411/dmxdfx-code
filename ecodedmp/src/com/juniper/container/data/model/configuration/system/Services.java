package com.juniper.container.data.model.configuration.system;

import javax.xml.bind.annotation.XmlElement;

public class Services {
	private String ftp;
	private ServicesSSH ssh;
	private String telnet;
	private NetConf netconf;

	@XmlElement(name="ftp",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getFtp() {
		return ftp;
	}

	public void setFtp(String ftp) {
		this.ftp = ftp;
	}

	@XmlElement(name="ssh",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public ServicesSSH getSsh() {
		return ssh;
	}

	public void setSsh(ServicesSSH ssh) {
		this.ssh = ssh;
	}

	@XmlElement(name="telnet",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getTelnet() {
		return telnet;
	}

	public void setTelnet(String telnet) {
		this.telnet = telnet;
	}

	@XmlElement(name="netconf",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public NetConf getNetconf() {
		return netconf;
	}

	public void setNetconf(NetConf netconf) {
		this.netconf = netconf;
	}

	@Override
	public String toString() {
		return "Services [ftp=" + ftp + ", ssh=" + ssh + ", telnet=" + telnet
				+ ", netconf=" + netconf + "]";
	}

}
