package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class Schedulers {
	private String name;
	private TransmitRate transmitRate;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="transmit-rate",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public TransmitRate getTransmitRate() {
		return transmitRate;
	}

	public void setTransmitRate(TransmitRate transmitRate) {
		this.transmitRate = transmitRate;
	}

	@Override
	public String toString() {
		return "Schedulers [name=" + name + ", transmitRate=" + transmitRate + "]";
	}

}
