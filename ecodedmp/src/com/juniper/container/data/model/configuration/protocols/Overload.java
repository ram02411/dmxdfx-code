package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Overload {
	
	private String timeout;

	@XmlElement(name="timeout",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	@Override
	public String toString() {
		return "Overload [timeout=" + timeout + "]";
	}

}
