package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlAttribute;

public class Multipath {
	
	private String inactive;

	@XmlAttribute(name="inactive",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getInactive() {
		return inactive;
	}

	public void setInactive(String inactive) {
		this.inactive = inactive;
	}

	@Override
	public String toString() {
		return "Multipath [inactive=" + inactive + "]";
	}
	
	
}
