package com.juniper.container.data.model.configuration.protocols;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder={"name", "type","asnumber","localasnumber","peerasnumber","traceoptions","localaddress","keep","family","authenticationKey","timport","texport","iimport","export","gracefulRestart","multipath","neighbor","hostname","possiblePathValue"})
public class BgpGroup {
	private String name;
	private String type;
	private Traceoptions traceoptions;
	private String localaddress;
	private String keep;
	private GroupFamily family;
	private String authenticationKey;
	private String texport;
	private String timport;
	private List<String> export;
	private List<String> iimport;
	private String asnumber;
	private String localasnumber;
	private String peerasnumber;
	private GroupGracefulRestart gracefulRestart;
	private Multipath multipath;
	private List<Neighbor> neighbor;
	private String hostname;
	private String possiblePathValue;
	
	@XmlElement(name="import",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getTexport() {
		return texport;
	}

	public void setTexport(String texport) {
		this.texport = texport;
	}

	@XmlElement(name="import",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getTimport() {
		return timport;
	}

	public void setTimport(String timport) {
		this.timport = timport;
	}

	@XmlElement(name="import",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<String> getIimport() {
		return iimport;
	}

	public void setIimport(List<String> iimport) {
		this.iimport = iimport;
	}

	@XmlElement(name="asnumber",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAsnumber() {
		return asnumber;
	}

	
	public void setAsnumber(String asnumber) {
		this.asnumber = asnumber;
	}

	@XmlElement(name="localasnumber",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getLocalasnumber() {
		return localasnumber;
	}

	public void setLocalasnumber(String localasnumber) {
		this.localasnumber = localasnumber;
	}

	@XmlElement(name="peerasnumber",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPeerasnumber() {
		return peerasnumber;
	}

	public void setPeerasnumber(String peerasnumber) {
		this.peerasnumber = peerasnumber;
	}

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="type",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement(name="traceoptions",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Traceoptions getTraceoptions() {
		return traceoptions;
	}

	public void setTraceoptions(Traceoptions traceoptions) {
		this.traceoptions = traceoptions;
	}

	@XmlElement(name="local-address",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getLocaladdress() {
		return localaddress;
	}

	public void setLocaladdress(String localaddress) {
		this.localaddress = localaddress;
	}

	@XmlElement(name="keep",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getKeep() {
		return keep;
	}

	public void setKeep(String keep) {
		this.keep = keep;
	}

	@XmlElement(name="family",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public GroupFamily getFamily() {
		return family;
	}

	public void setFamily(GroupFamily family) {
		this.family = family;
	}

	@XmlElement(name="authentication-key",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getAuthenticationKey() {
		return authenticationKey;
	}

	public void setAuthenticationKey(String authenticationKey) {
		this.authenticationKey = authenticationKey;
	}

	@XmlElement(name="export",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<String> getExport() {
		return export;
	}

	public void setExport(List<String> export) {
		this.export = export;
	}

	@XmlElement(name="graceful-restart",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public GroupGracefulRestart getGracefulRestart() {
		return gracefulRestart;
	}

	public void setGracefulRestart(GroupGracefulRestart gracefulRestart) {
		this.gracefulRestart = gracefulRestart;
	}

	@XmlElement(name="multipath",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Multipath getMultipath() {
		return multipath;
	}

	public void setMultipath(Multipath multipath) {
		this.multipath = multipath;
	}

	@XmlElement(name="neighbor",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Neighbor> getNeighbor() {
		return neighbor;
	}

	public void setNeighbor(List<Neighbor> neighbor) {
		this.neighbor = neighbor;
	}
	
	
	@XmlElement(name="hostName",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	
	
	@XmlElement(name="possiblePathValue",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPossiblePathValue() {
		return possiblePathValue;
	}

	public void setPossiblePathValue(String possiblePathValue) {
		this.possiblePathValue = possiblePathValue;
	}

	@Override
	public String toString() {
		return "BgpGroup [name=" + name + ", type=" + type + ", traceoptions=" + traceoptions + ", localaddress="
				+ localaddress + ", keep=" + keep + ", family=" + family + ", authenticationKey=" + authenticationKey
				+ ", texport=" + texport + ", timport=" + timport + ", export=" + export + ", iimport=" + iimport
				+ ", asnumber=" + asnumber + ", localasnumber=" + localasnumber + ", peerasnumber=" + peerasnumber
				+ ", gracefulRestart=" + gracefulRestart + ", multipath=" + multipath + ", neighbor=" + neighbor
				+ ", hostname=" + hostname + ", possiblePathValue=" + possiblePathValue + "]";
	}

	

}
