package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class EthernetRing {

	private String name;
	private String ringProtectionLinkOwner;
	private String dot1pPriority;
	private EastInterface eastInterface;
	private WestInterface westInterface;
	private DataChannel datachannel;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="ring-protection-link-owner",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRingProtectionLinkOwner() {
		return ringProtectionLinkOwner;
	}

	public void setRingProtectionLinkOwner(String ringProtectionLinkOwner) {
		this.ringProtectionLinkOwner = ringProtectionLinkOwner;
	}

	@XmlElement(name="dot1p-priority",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getDot1pPriority() {
		return dot1pPriority;
	}

	public void setDot1pPriority(String dot1pPriority) {
		this.dot1pPriority = dot1pPriority;
	}

	@XmlElement(name="east-interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public EastInterface getEastInterface() {
		return eastInterface;
	}

	public void setEastInterface(EastInterface eastInterface) {
		this.eastInterface = eastInterface;
	}

	@XmlElement(name="west-interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public WestInterface getWestInterface() {
		return westInterface;
	}

	public void setWestInterface(WestInterface westInterface) {
		this.westInterface = westInterface;
	}

	@XmlElement(name="data-channel",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public DataChannel getDatachannel() {
		return datachannel;
	}

	public void setDatachannel(DataChannel datachannel) {
		this.datachannel = datachannel;
	}

	@Override
	public String toString() {
		return "EthernetRing [name=" + name + ", ringProtectionLinkOwner="
				+ ringProtectionLinkOwner + ", dot1pPriority=" + dot1pPriority
				+ ", eastInterface=" + eastInterface + ", westInterface="
				+ westInterface + ", datachannel=" + datachannel + "]";
	}

}
