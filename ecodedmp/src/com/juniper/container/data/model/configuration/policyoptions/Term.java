package com.juniper.container.data.model.configuration.policyoptions;

import javax.xml.bind.annotation.XmlElement;

public class Term {
	private String name;
	private From from;
	private Then then;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="from",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public From getFrom() {
		return from;
	}

	public void setFrom(From from) {
		this.from = from;
	}

	@XmlElement(name="then",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Then getThen() {
		return then;
	}

	public void setThen(Then then) {
		this.then = then;
	}

	@Override
	public String toString() {
		return "Term [name=" + name + ", from=" + from + ", then=" + then + "]";
	}

}
