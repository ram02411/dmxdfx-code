package com.juniper.container.data.model.configuration.system;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class File {
	private String name;
	private List<Contents> contents;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="contents",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<Contents> getContents() {
		return contents;
	}

	public void setContents(List<Contents> contents) {
		this.contents = contents;
	}

	@Override
	public String toString() {
		return "File [name=" + name + ", contents=" + contents + "]";
	}

}
