package com.juniper.container.data.model.configuration.snmp;

import javax.xml.bind.annotation.XmlElement;

public class Snmp {
	private SnmpCommunity community;
	private TrapGroup trapgroup;
	
	@XmlElement(name="community",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public SnmpCommunity getCommunity() {
		return community;
	}
	public void setCommunity(SnmpCommunity community) {
		this.community = community;
	}
	
	@XmlElement(name="trap-group",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public TrapGroup getTrapgroup() {
		return trapgroup;
	}
	public void setTrapgroup(TrapGroup trapgroup) {
		this.trapgroup = trapgroup;
	}
	@Override
	public String toString() {
		return "Snmp [community=" + community + ", trapgroup=" + trapgroup
				+ "]";
	}
	
}
