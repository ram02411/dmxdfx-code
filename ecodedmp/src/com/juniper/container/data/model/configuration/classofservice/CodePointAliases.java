package com.juniper.container.data.model.configuration.classofservice;

import javax.xml.bind.annotation.XmlElement;

public class CodePointAliases {
	
	private CodeDscpIpv6 dscpIpv6;

	@XmlElement(name="dscp-ipv6",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public CodeDscpIpv6 getDscpIpv6() {
		return dscpIpv6;
	}

	public void setDscpIpv6(CodeDscpIpv6 dscpIpv6) {
		this.dscpIpv6 = dscpIpv6;
	}

	@Override
	public String toString() {
		return "CodePointAliases [dscpIpv6=" + dscpIpv6 + "]";
	}
	
	
}
