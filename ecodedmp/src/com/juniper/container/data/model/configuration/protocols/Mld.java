package com.juniper.container.data.model.configuration.protocols;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;

public class Mld {

	private String queryinterval;
	private String robustcount;
	private List<MldInterface> minterface;

	@XmlElement(name="query-interval",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getQueryinterval() {
		return queryinterval;
	}

	public void setQueryinterval(String queryinterval) {
		this.queryinterval = queryinterval;
	}

	@XmlElement(name="robust-count",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getRobustcount() {
		return robustcount;
	}

	public void setRobustcount(String robustcount) {
		this.robustcount = robustcount;
	}

	@XmlElement(name="interface",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<MldInterface> getMinterface() {
		return minterface;
	}

	public void setMinterface(List<MldInterface> minterface) {
		this.minterface = minterface;
	}

	@Override
	public String toString() {
		return "Mld [queryinterval=" + queryinterval + ", robustcount="
				+ robustcount + ", minterface=" + minterface + "]";
	}

}
