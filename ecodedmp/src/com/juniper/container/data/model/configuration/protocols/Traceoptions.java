package com.juniper.container.data.model.configuration.protocols;

import javax.xml.bind.annotation.XmlElement;

public class Traceoptions {
	
	private BgpFile file;
	private Flag flag;
	
	@XmlElement(name="file",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public BgpFile getFile() {
		return file;
	}

	public void setFile(BgpFile file) {
		this.file = file;
	}

	@XmlElement(name="flag",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Flag getFlag() {
		return flag;
	}

	public void setFlag(Flag flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "Traceoptions [file=" + file + ", flag=" + flag + "]";
	}

	

}
