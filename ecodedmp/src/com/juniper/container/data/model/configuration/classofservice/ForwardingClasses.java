package com.juniper.container.data.model.configuration.classofservice;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ForwardingClasses {
	private List<FClass> cclass;
	
	@XmlElement(name="class",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<FClass> getCclass() {
		return cclass;
	}

	public void setCclass(List<FClass> cclass) {
		this.cclass = cclass;
	}

	@Override
	public String toString() {
		return "ForwardingClasses [cclass=" + cclass + "]";
	}
	
}
