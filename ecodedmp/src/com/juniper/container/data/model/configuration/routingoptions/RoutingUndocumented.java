package com.juniper.container.data.model.configuration.routingoptions;

import javax.xml.bind.annotation.XmlElement;

public class RoutingUndocumented {
	
	private String pepeconnection;

	@XmlElement(name="pe-pe-connection",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getPepeconnection() {
		return pepeconnection;
	}

	public void setPepeconnection(String pepeconnection) {
		this.pepeconnection = pepeconnection;
	}

	@Override
	public String toString() {
		return "RoutingUndocumented [pepeconnection=" + pepeconnection + "]";
	}
	
	
}	
