package com.juniper.container.data.model.configuration.routingoptions;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class RibGroups {

	private String name;
	private List<String> importrib;

	@XmlElement(name="name",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="import-rib",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public List<String> getImportrib() {
		return importrib;
	}

	public void setImportrib(List<String> importrib) {
		this.importrib = importrib;
	}

	@Override
	public String toString() {
		return "RibGroups [name=" + name + ", importrib=" + importrib + "]";
	}

}
