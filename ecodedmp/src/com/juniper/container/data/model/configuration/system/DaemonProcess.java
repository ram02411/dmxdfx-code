package com.juniper.container.data.model.configuration.system;

public class DaemonProcess {
	private String name;
	private Undocumented undocumented;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Undocumented getUndocumented() {
		return undocumented;
	}

	public void setUndocumented(Undocumented undocumented) {
		this.undocumented = undocumented;
	}

	@Override
	public String toString() {
		return "DaemonProcess [name=" + name + ", undocumented=" + undocumented
				+ "]";
	}

}
