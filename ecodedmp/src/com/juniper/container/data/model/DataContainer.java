package com.juniper.container.data.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.juniper.container.data.model.configuration.Configuration;
import com.juniper.container.data.model.databasestatus.DatabaseStatusInformation;

public class DataContainer {
    private String resultStatus;
	private Configuration configuration;
	private DatabaseStatusInformation databaseStatusInformation;
	
	
	public String getResultStatus() {
		return resultStatus;
	}

	public void setResultStatus(String resultStatus) {
		this.resultStatus = resultStatus;
	}

	@XmlElement(name="configuration",namespace="http://xml.juniper.net/xnm/1.1/xnm")
	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	@XmlElement(name="database-status-information",namespace="urn:ietf:params:xml:ns:netconf:base:1.0")
	public DatabaseStatusInformation getDatabaseStatusInformation() {
		return databaseStatusInformation;
	}

	public void setDatabaseStatusInformation(DatabaseStatusInformation databaseStatusInformation) {
		this.databaseStatusInformation = databaseStatusInformation;
	}

	@Override
	public String toString() {
		return "DataContainer [configuration=" + configuration + ", databaseStatusInformation="
				+ databaseStatusInformation + "]";
	}

}
