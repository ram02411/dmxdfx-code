package com.juniper.container.data.device;

import java.io.InputStream;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.juniper.container.data.constant.DeviceConfigurationConstant;
import com.juniper.container.data.model.DataContainer;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

/**
 * 
 * @author 305
 *
 */
@Repository("JuniperDataDeviceImpl")
@Scope("singleton")
public class JuniperDataDeviceImpl implements JuniperDataDevice {
	
	
	@Override
	public XML fetchDeviceMemoryUsage(String deviceName) {
		  XML rpcReply=null;
		  try {
			XMLBuilder builder = new XMLBuilder();
			//Creating the command to fetch the data from live device 
			String command="<rpc>"+
				       " <get-system-memory-information>"+
				        "</get-system-memory-information>"+
				   " </rpc>";
	        //Create the device
	        Device device = new Device(DeviceConfigurationConstant.JUNIPER_ID_ADDRESS,DeviceConfigurationConstant.JUNIPER_USER_ID,DeviceConfigurationConstant.JUNIPER_PASSWORD,null);
	        //device.setTimeOut(8000);
	        device.connect();
	       // NetconfSession ns =device.createNetconfSession();
	       // System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@END__________________________________");
	       // boolean b=ns.isOK();
	        //System.out.println(")@@(@( = "+b);
	        //Send RPC and receive RPC Reply as XML
	        rpcReply = device.executeRPC(command);
	        device.close();
		  }catch(Exception exe){
			  exe.printStackTrace();
		  }
		  return rpcReply;
	}
	
	

	@Override
	public String fetchJuniperDataModel(String routerName) {
		  XML rpcReply=null;
		  try {
			XMLBuilder builder = new XMLBuilder();
			//Creating the command to fetch the data from live device 
	        String str="  <get-config>"+
	        		" <source>"+
	        		"     <running/>"+
	        		"</source>"+
	        		"  </get-config>";
	        //Create the device
	        String deviceTokens[]=routerName.split("_");
	        Device device = new Device(deviceTokens[0],deviceTokens[1],deviceTokens[2],null);
	        //device.setTimeOut(8000);
	        device.connect();
	       // NetconfSession ns =device.createNetconfSession();
	       // System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@END__________________________________");
	       // boolean b=ns.isOK();
	        //System.out.println(")@@(@( = "+b);
	        //Send RPC and receive RPC Reply as XML
	        rpcReply = device.executeRPC(str);
	        System.out.println("#################################################");
	        System.out.println(rpcReply.toString());
	        System.out.println("#################################################");
	        device.close();
		  }catch(Exception exe){
			  exe.printStackTrace();
		  }
		  return rpcReply==null?"":rpcReply.toString();
	}

	@Override
	public DataContainer findJuniperDataModel(InputStream fileInputStream) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
