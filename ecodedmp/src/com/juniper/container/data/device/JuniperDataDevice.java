package com.juniper.container.data.device;

import java.io.InputStream;

import com.juniper.container.data.model.DataContainer;

import net.juniper.netconf.XML;

/**
 * 
 * @author 305
 *
 */
public interface JuniperDataDevice {
	public String fetchJuniperDataModel(String routerName);
	public DataContainer findJuniperDataModel(InputStream fileInputStream);
	public XML fetchDeviceMemoryUsage(String deviceName);
}
