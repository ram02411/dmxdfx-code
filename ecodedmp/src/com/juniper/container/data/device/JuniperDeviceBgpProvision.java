package com.juniper.container.data.device;

/**
 * 
 * @author 305
 *
 */
public interface JuniperDeviceBgpProvision {
	public String provisionTraceOptions(String cProtocols,String loadType,String hostname);

	public String groupVpnProvision(String cProtocols, String loadType, String hostname, String possiblePathValue);
}
