package com.juniper.container.data.device;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import com.ecode.topology.viewer.data.device.EcodeTopologyDeviceViewerImpl;
import com.ecode.web.app.constant.ApplicationConstant;

import net.juniper.netconf.CommitException;
import net.juniper.netconf.Device;
import net.juniper.netconf.LoadException;

/**
 * 
 * @author 305
 *
 */
@Repository("JuniperDeviceBgpProvisionImpl")
@Scope("singleton")
public class JuniperDeviceBgpProvisionImpl implements JuniperDeviceBgpProvision{
	
	  private static final Log logger = LogFactory.getLog(JuniperDeviceBgpProvisionImpl.class);
	
	/**
	 * This method is used to implement the vpn provisioning for all the router 
	 * detected in the L3 topology   
	 * @param cProtocols
	 * @param loadType
	 * @param hostname
	 * @param possiblePathValue
	 * @return
	 */
	@Override
	public String groupVpnProvision(String cProtocols,String loadType,String hostname,String possiblePathValue) {
		//Create the device
		Device device=null;;
		   //Load and commit the configuration
		
		if(hostname==null){
			System.out.println("Router could not be picked up ,please contact to admin!!! hostname="+hostname);
			return "Router could not be picked up ,please contact to admin!!!";
		}
		String ptokens[]=hostname.split("_");
      try {
    	  	device = new Device(ptokens[0],ptokens[1],ptokens[2],null);
			device.connect();
			//Lock the configuration first
			boolean isLocked = device.lockConfig();
			if(!isLocked) {
				System.out.println("Could not lock configuration. Exit now.");
				return "Could not lock configuration , please contact to admin";
			}
            device.loadXMLConfiguration(cProtocols, loadType);
            device.commit();
        } catch(LoadException e) {
            System.out.println(e.getMessage());
            return "Could not load the configuration , please contact to admin";
        } catch(CommitException e) {
            System.out.println(e.getMessage());
            return "Could not lock the configuration , please contact to admin";
        }catch(Exception e) {
            System.out.println(e.getMessage());
            return "Problem occures during provising , please contact to admin";
        }finally {
        	 //Unlock the configuration and close the device.
            try {
            	if(device!=null) {
            		System.out.println("Unlocking the config and closing the device now............................");
            		device.unlockConfig();
            		device.close();
            	}	
			} catch (Exception e) {
				e.printStackTrace();
			} 
           
		}
		return ApplicationConstant.SUCCESS_STATUS;
	}


	@Override
	public String provisionTraceOptions(String cProtocols,String loadType,String hostname) {
		//Create the device
		Device device=null;;
		   //Load and commit the configuration
		
		if(hostname==null){
			System.out.println("Router could not be picked up ,please contact to admin!!! hostname="+hostname);
			return "Router could not be picked up ,please contact to admin!!!";
		}
		String ptokens[]=hostname.split("_");
      try {
    	  	device = new Device(ptokens[0],ptokens[1],ptokens[2],null);
    	  	System.out.println("_____Connectiong with Router IP    = "+ptokens[0]+" username = "+ptokens[1]+" Password = "+ptokens[2]);
			device.connect();
			//Lock the configuration first
			boolean isLocked = device.lockConfig();
			if(!isLocked) {
				System.out.println("Could not lock configuration. Exit now.");
				return "Could not lock configuration , please contact to admin";
			}
            device.loadXMLConfiguration(cProtocols, loadType);
            device.commit();
        } catch(LoadException e) {
            System.out.println(e.getMessage());
            return "Could not load the configuration , please contact to admin";
        } catch(CommitException e) {
            System.out.println(e.getMessage());
            return "Could not lock the configuration , please contact to admin";
        }catch(Exception e) {
            System.out.println(e.getMessage());
            return "Problem occures during provising , please contact to admin";
        }finally {
        	 //Unlock the configuration and close the device.
            try {
            	if(device!=null) {
            		System.out.println("Unlocking the config and closing the device now............................");
            		device.unlockConfig();
            		device.close();
            	}	
			} catch (Exception e) {
				e.printStackTrace();
			} 
           
		}
		return ApplicationConstant.SUCCESS_STATUS;
	}

}
