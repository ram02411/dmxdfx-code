package com.juniper.container.data.service;

import java.io.InputStream;

import com.juniper.container.data.controller.model.RamMemoryUsageVO;
import com.juniper.container.data.model.DataContainer;

/**
 * 
 * @author 305
 *
 */
public interface JuniperDataService {
	public DataContainer fetchJuniperDataModel(String routerName);
	public DataContainer findJuniperDataModel(InputStream fileInputStream);
	public RamMemoryUsageVO fetchDeviceMemoryUsage(String deviceName);
}
