package com.juniper.container.data.service;

import java.io.InputStream;
import java.io.StringReader;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import com.juniper.container.data.controller.model.RamMemoryUsageVO;
import com.juniper.container.data.device.JuniperDataDevice;
import com.juniper.container.data.model.DataContainer;
import com.juniper.container.data.model.databasestatus.RpcReply;

import net.juniper.netconf.XML;

/**
 * 
 * @author 305
 *
 */

@Service("JuniperDataServiceImpl")
@Scope("singleton")
public class JuniperDataServiceImpl implements JuniperDataService {
	
	@Autowired
	@Qualifier("JuniperDataDeviceImpl")
	private JuniperDataDevice juniperDataDevice;
	
	@Override
	public RamMemoryUsageVO fetchDeviceMemoryUsage(String deviceName) {
		XML rpcReply=juniperDataDevice.fetchDeviceMemoryUsage(deviceName);
	    List<String> memoryList = Arrays.asList("system-memory-summary-information","system-memory-free-percent");
	    RamMemoryUsageVO memoryUsageVO=new RamMemoryUsageVO();
	    String freePercentage= rpcReply.findValue(memoryList);
	    if(freePercentage!=null && freePercentage.length()>0){
	    	String freeMem=freePercentage.substring(0,freePercentage.length()-1);
	    	freeMem=freeMem.trim();
	    	memoryUsageVO.setFreememory(freeMem);
	    	memoryUsageVO.setUsedmemory((100-Integer.parseInt(freeMem))+"");
	    }else{
	    	memoryUsageVO.setFreememory("0");
	    	memoryUsageVO.setUsedmemory("0");
	    }
		return memoryUsageVO;
	}
	
	@Override
	  public DataContainer findJuniperDataModel(InputStream fileInputStream){
		RpcReply rpcReply=null;
		  try {
				//File file = new File("get-config_running_cisco.xml");
				JAXBContext jaxbContext = JAXBContext.newInstance(RpcReply.class);
				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				rpcReply = (RpcReply) 
						jaxbUnmarshaller.unmarshal(fileInputStream);
			  } catch (JAXBException e) {
				e.printStackTrace();
			  }
		  return rpcReply!=null?rpcReply.getDataContainer():null;
	  }
	
	@Override
	public DataContainer  fetchJuniperDataModel(String routerName){
		RpcReply rpcReply=null;
		try {
			String xmlRpcDeviceData=juniperDataDevice.fetchJuniperDataModel(routerName);
			if(xmlRpcDeviceData!=null && xmlRpcDeviceData.length()==0){
				return null;
			}
			StringReader inStream = new StringReader(xmlRpcDeviceData);
			//because XML DOM parser understand InputSource
			InputSource isource = new InputSource(inStream);
			
			JAXBContext jaxbContext = JAXBContext.newInstance(RpcReply.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			 rpcReply = (RpcReply) jaxbUnmarshaller.unmarshal(isource);
			//System.out.println(rpcReply);
		  } catch (JAXBException e) {
			e.printStackTrace();
		  }
		return rpcReply!=null?rpcReply.getDataContainer():null;
	}
}
