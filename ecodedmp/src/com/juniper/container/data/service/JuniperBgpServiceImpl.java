package com.juniper.container.data.service;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.ecode.web.app.constant.ApplicationConstant;
import com.ecode.web.app.util.ApplicationRpcXmlGeneratorUtil;
import com.juniper.container.data.device.JuniperDeviceBgpProvision;
import com.juniper.container.data.model.configuration.protocols.CProtocols;
import com.sun.xml.internal.bind.marshaller.NamespacePrefixMapper;

/**
 * 
 * @author 305
 *
 */
@Service("JuniperBgpServiceImpl")
@Scope("singleton")
public class JuniperBgpServiceImpl implements JuniperBgpService {
	
	@Autowired
	@Qualifier("JuniperDeviceBgpProvisionImpl")
	private JuniperDeviceBgpProvision juniperDeviceBgpProvision;
	
	@Override
	public String provisionTraceOptions(CProtocols cProtocols,String loadType,String hostname,boolean fireCommand) {
		 try {
	            JAXBContext context = JAXBContext.newInstance(CProtocols.class);
	            Marshaller marshaller = context.createMarshaller();
	            //for pretty-print XML in JAXB
	            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	            //remove header
	            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
	            // Write to System.out for debugging
	            NamespacePrefixMapper mapper = new NamespacePrefixMapper() {
	                public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
	                    return "";
	                }
	            };
	            marshaller.setProperty("com.sun.xml.internal.bind.namespacePrefixMapper", mapper);
	            StringWriter stringWriter = new StringWriter();
	            marshaller.marshal(cProtocols,stringWriter);
	            String configuration = stringWriter.toString();
	          //  System.out.println("alalla- - = "+configuration);
	            System.out.println("RPC Request = "+ApplicationRpcXmlGeneratorUtil.makeRequestInRpcFormatForUpdate("candidate", configuration, loadType));
	            if(fireCommand){
	            String result=juniperDeviceBgpProvision.provisionTraceOptions(configuration, loadType,hostname);
	            return result;
	            }
	            // Write to File
	          //  m.marshal(emp, new File(FILE_NAME));
	        } catch (JAXBException e) {
	            e.printStackTrace();
	        }
		
		return ApplicationConstant.FAILED_STATUS;
	}
	
	@Override
	public String vpnSetupForL3Topology(CProtocols cProtocols,String loadType,String hostname,String possiblePathValue) {
		 try {
	            JAXBContext context = JAXBContext.newInstance(CProtocols.class);
	            Marshaller marshaller = context.createMarshaller();
	            //for pretty-print XML in JAXB
	            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	            //remove header
	            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
	            // Write to System.out for debugging
	            NamespacePrefixMapper mapper = new NamespacePrefixMapper() {
	                public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
	                    return "";
	                }
	            };
	            marshaller.setProperty("com.sun.xml.internal.bind.namespacePrefixMapper", mapper);
	            StringWriter stringWriter = new StringWriter();
	            marshaller.marshal(cProtocols,stringWriter);
	            String configuration = stringWriter.toString();
	            System.out.println("alalla- - = "+configuration);
	            System.out.println("_________Rpc request for data model provisioning for Device_______ = "+ApplicationRpcXmlGeneratorUtil.makeRequestInRpcFormatForUpdate("candidate", configuration, loadType));
	            
	            String result=""; 
	            result=juniperDeviceBgpProvision.provisionTraceOptions(configuration, loadType,hostname);
	            return result;
	            // Write to File
	          //  m.marshal(emp, new File(FILE_NAME));
	        } catch (JAXBException e) {
	            e.printStackTrace();
	        }
		
		return ApplicationConstant.FAILED_STATUS;
	}

	@Override
	public String provisionTraceOptions(CProtocols cProtocols, String loadType, String hostname) {
		return provisionTraceOptions(cProtocols, loadType, hostname, true);
	}

}
