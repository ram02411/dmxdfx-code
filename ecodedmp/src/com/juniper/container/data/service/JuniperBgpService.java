package com.juniper.container.data.service;

import com.juniper.container.data.model.configuration.protocols.CProtocols;

/**
 * 
 * @author 305
 *
 */
public interface JuniperBgpService {
	
	public String provisionTraceOptions(CProtocols cProtocols,String loadType,String hostname);
	public String provisionTraceOptions(CProtocols cProtocols,String loadType,String hostname,boolean fireCommand);

	public String vpnSetupForL3Topology(CProtocols cProtocols, String loadType, String hostname, String possiblePathValue);

}
