package com.hiet.excelReader;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.ecode.topology.viewer.data.device.Router;


public class InventoryDb {
	public static List<String[]> routersinfo=new ArrayList<String[]>();
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	   static final String DB_URL = "jdbc:mysql://localhost:3306/ecodedb";

	   //  Database credentials
	   static final String USER = "root";
	   static final String PASS = "3volve123!";
	   
	   public static void addrouter(String[] router){
		   routersinfo.add(router);
	   }
	   public static void insertrouterdb(){
		   if(routersinfo!=null){
			   for(String[] router:routersinfo){
				   InventoryDb.insertRouters(router[0], router[3], router[1], router[2],router[4]);
			   }
		   }
	   }
	   
	   public static void deleteRouters() {
	   Connection conn = null;
	   Statement stmt = null;
	   try{
	      //STEP 2: Register JDBC driver
	      Class.forName("com.mysql.jdbc.Driver");

	      //STEP 3: Open a connection
	      System.out.println("Connecting to database...");
	      conn = DriverManager.getConnection(DB_URL,USER,PASS);

	      //STEP 4: Execute a query
	      System.out.println("Creating statement...");
	      stmt = conn.createStatement();
	      String sql;
	      sql = "delete from inventory_routers_tbl";
	      stmt.execute(sql);

	      //STEP 5: Extract data from result set
	      
	      stmt.close();
	      conn.close();
	   }catch(SQLException se){
	      //Handle errors for JDBC
	      se.printStackTrace();
	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(stmt!=null)
	            stmt.close();
	      }catch(SQLException se2){
	      }// nothing we can do
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }//end finally try
	   }//end try
	   System.out.println("Goodbye!");
	}
	   public static List<Router> findAllRouters() {
		   Connection conn = null;
		   Statement stmt = null;
		   List<Router> routerlist=new ArrayList<>();
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);

		      //STEP 4: Execute a query
		      System.out.println("Creating statement...");
		      stmt = conn.createStatement();
		      String sql;
		      sql = "select * from inventory_routers_tbl";
		      ResultSet rs = stmt.executeQuery(sql);
		      
		      while(rs.next()){
		    	  Router router=new Router(); 
		    	  router.setIp(rs.getString("ipAddress"));		    	  
		    	  router.setUsername(rs.getString("userName"));
		    	  router.setPassword(rs.getString("password"));
		    	  router.setHostname(rs.getString("hostName"));
		    	  router.setType(rs.getString("deviceType"));
		    	  routerlist.add(router);
		      }
		      //STEP 5: Extract data from result set
		      System.out.println("select * from invent statement..."+routerlist.toString());
		      stmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   System.out.println("Goodbye!");
		return routerlist;
	   }
	   public static void updaterouterType(String ip,String type) {
		   
		   
		   
		   Connection conn = null;
		   Statement stmt = null;
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);

		      //STEP 4: Execute a query
		      System.out.println("Creating statement...");
		      stmt = conn.createStatement();
		      String sql;
		      ip="'"+ip+"'";
		      type="'"+type+"'";
		     
		     
		      sql = "update inventory_routers_tbl set deviceType="+type+" where ipAddress="+ip;
		      System.out.println("sql statement :"+sql);
		      stmt.executeUpdate(sql);

		      //STEP 5: Extract data from result set
		      
		      stmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   System.out.println("Goodbye!");
		}
	   

	   
	   
	   public static void insertRouters(String ip,String hostname,String username,String password,String dtype) {
		   Connection conn = null;
		   Statement stmt = null;
		   String rip=ip;
		   String rusername=username;
		   String rpassword=password;
		   try{
		      //STEP 2: Register JDBC driver
		      Class.forName("com.mysql.jdbc.Driver");

		      //STEP 3: Open a connection
		      System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);

		      //STEP 4: Execute a query
		      System.out.println("Creating statement...");
		      stmt = conn.createStatement();
		      String sql;
		      
		      
		      ip="'"+ip+"'";
		      hostname="'"+hostname+"'";
		      username="'"+username+"'";
		      password="'"+password+"'";
		      dtype="'"+dtype+"'";
		      
		      sql = "insert into inventory_routers_tbl values("+ip+","+username+","+password+","+hostname+","+dtype+",'','','')";
		      System.out.println("sql statement :"+sql);
		      stmt.executeUpdate(sql);

		      //STEP 5: Extract data from result set
		      
		      stmt.close();
		      conn.close();
		   }catch(SQLException se){
		      //Handle errors for JDBC
		      se.printStackTrace();
		   }catch(Exception e){
		      //Handle errors for Class.forName
		      e.printStackTrace();
		   }finally{
		      //finally block used to close resources
		      try{
		         if(stmt!=null)
		            stmt.close();
		      }catch(SQLException se2){
		      }// nothing we can do
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		         se.printStackTrace();
		      }//end finally try
		   }//end try
		   System.out.println("Goodbye!");
		   //DeviceType.devicetype(rip, rusername, rpassword);
		}

}
