package com.hiet.excelReader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.ecode.cisco.command.SSHClient;
import com.ecode.web.app.constant.ApplicationConstant;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import net.juniper.netconf.Device;

public class DeviceType {
	public static String devicetype(String ip,String username,String password){

		String dtype=null;
		try {
			String model="Model";
			String junos="JUNOS";
			String status="Cisco";	
			String[] devicedata=null;
			String command="show version";
	         JSch jsch = new JSch();
	         Session session = jsch.getSession(username, ip, 22);
	         Properties config = new Properties();
	         config.put("StrictHostKeyChecking", "no");
	         session.setConfig(config);;
	         session.setPassword(password);
	         session.connect();
	         Channel channel = session.openChannel("exec");
	         ((ChannelExec)channel).setCommand(command);
	         channel.setInputStream(null);
	         ((ChannelExec)channel).setErrStream(System.err);
	         InputStream input = channel.getInputStream();
	         channel.connect();
	         System.out.println("Channel Connected to machine " + ip + " server with command: " + command );
	         InputStreamReader inputReader=null;
	         BufferedReader bufferedReader=null;
	         try{
	        	 inputReader = new InputStreamReader(input);
	             bufferedReader = new BufferedReader(inputReader);
	             System.out.println("OL = "+bufferedReader.readLine());
	             String line = null;
	             Device device=null;
	             int count=0;
	             if(bufferedReader.readLine()==null){
	            	 dtype=ApplicationConstant.JUNIPER;
	            	 device = new Device(ip,username,password, null, 830);
	  			   device.connect();
	  			 JuniperTypdeDbupdate.Routertypeupdate(device, ip, dtype);
	  			   
	             }
	             while((line = bufferedReader.readLine()) != null){
	            	System.out.println("OL = "+line);
	 				if (line != null && line.trim().length() == 0 || line.equalsIgnoreCase("\n\n"))
	 					continue;
	 				if(line.startsWith(model))
	 					devicedata[0]=line;
	 				if(line.startsWith(junos)){
	 					dtype=ApplicationConstant.JUNIPER;
	 					devicedata[1]=line;
	 				}
	 				if(devicedata.length==2){
	 					JuniperTypdeDbupdate.juniperupdate(devicedata, ip, dtype);
	 				}
	 				else
	 				{
	 					dtype=ApplicationConstant.CISCO;
	 					CiscoTypeDbupdate.Routertypeupdate(line, dtype, ip);
	 				}
	 				if ( line.startsWith(status)) 
	 					count=count+1;
	 				if(count==1){
	 					dtype=ApplicationConstant.CISCO;
	 					CiscoTypeDbupdate.Routertypeupdate(line, dtype, ip);	 					
	 					continue;
	 				}
	 			}
	         }catch(Throwable ex){
	             ex.printStackTrace();
	         }finally{
	        	 if(bufferedReader!=null)
	        	 bufferedReader.close();
	        	 if(inputReader!=null)
	             inputReader.close();
	         }
	         channel.disconnect();
	         session.disconnect();
		}catch(Throwable ex){
            ex.printStackTrace();
       }
		return dtype;
	}


	
}
