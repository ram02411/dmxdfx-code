package com.hiet.excelReader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.ecode.topology.viewer.data.device.MplsNeighborDataTable;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

public class JuniperTypdeDbupdate {
	public static void Routertypeupdate(Device device,String ip,String type){
		String data=null;
		try {
			XMLBuilder query = new XMLBuilder();
			XML q = query.createNewXML("get-software-information");
			XML rpc_reply = device.executeRPC(q);
			//System.out.println(rpc_reply);
			 data= parse(rpc_reply);
			 System.out.println("data is :"+data);
			 if(data!=null){
				 juniperDbupdate(data,ip,type);
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	private static String parse(XML rpc_reply) {
		String model=null;
		String version=null;
		String result=null;
		
		
		List<String> list = Arrays.asList("software-information", "package-information");
		List  dList = rpc_reply.findNodes(list);
		
		//System.out.println(list);
		
	    Iterator iter = dList.iterator();
		
		while (iter.hasNext()) {
			Node node = (Node) iter.next();
			NodeList dNodes = node.getChildNodes();
			
			for (int i = 0; i < dNodes.getLength(); i++) {
				Node dNode = (Node) dNodes.item(i);
				//System.out.println(mplsNode);
				if (dNode.getNodeName().equals("comment")) {
					version=dNode.getTextContent().trim();
					
				}
				
			}
			
		}
		
		List<String> listd = Arrays.asList("software-information");
		List  Listd = rpc_reply.findNodes(listd);
		
		//System.out.println(list);
		
	    Iterator iter1 = Listd.iterator();
		
		while (iter1.hasNext()) {
			Node noded = (Node) iter1.next();
			NodeList Nodesd = noded.getChildNodes();
			
			for (int i = 0; i < Nodesd.getLength(); i++) {
				Node Noded = (Node) Nodesd.item(i);
				//System.out.println(mplsNode);
				if (Noded.getNodeName().equals("product-model")) {
					model=Noded.getTextContent().trim();
					
				}
				
			}
			
		}
		
		
		if(model!=null && version!=null)
		result=model+"&"+version;
		
		return result;
		
		
	}
	
public static void juniperDbupdate(String line,String ip,String type){
	//System.out.println("line is :"+line);
	String dtype=null;
	String serice=null;
	String version=null;
	String[] lined=line.split("&");
	String[] linesp=lined[1].split("\\s+");
	
	
	
	dtype=lined[0];
	serice=linesp[0];
	version=linesp[4].substring(1, linesp[4].length()-1);
	if(type!=null && dtype!=null && serice!=null && version!=null){
		InventoryDb.updaterouterType(ip, type);
	}
	
	
}

public static void juniperupdate(String[] line,String ip,String type){
	//System.out.println("line is :"+line);
	
	String serice=null;
	String version=null;
	String[] jmodel=line[0].split(";");
	String model=jmodel[1];
	String[] linesp=line[1].split("\\s+");
	
	serice=linesp[0];
	version=linesp[1].substring(1, linesp[4].length()-1);
	if(type!=null && model!=null && serice!=null && version!=null){
		InventoryDb.updaterouterType(ip, type);
	}
	
	
}

}
