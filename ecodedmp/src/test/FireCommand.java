package test;

/*
 * Copyright (c) 2013 Juniper Networks, Inc.
 * All Rights Reserved
 *
 * Use is subject to license terms.
 *
 */

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.juniper.netconf.Device;
import net.juniper.netconf.NetconfException;
import net.juniper.netconf.NetconfSession;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

import org.xml.sax.SAXException;

public class FireCommand {
    public static void main(String args[]) throws NetconfException, 
              ParserConfigurationException, SAXException, IOException {
        
        XMLBuilder builder = new XMLBuilder();
        XML ftp_config = builder.createNewConfig("show-interface-information", "terse");
        System.out.println(ftp_config.toString());
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        
  /*      String str="  <get-config>"+
       " <source>"+
       "     <running/>"+
        "</source>"+
  "  </get-config>";*/
       //String str="<get-interface-information>"+ "<terse/>"+ "</get-interface-information>";
        //String str="<get-bgp-group-information><group-name>GroupC</group-name></get-bgp-group-information>";
        //String str="<get-bgp-group-information><group-name></group-name></get-bgp-group-information>";
       //String str="<get-configuration-protocols-bgp-><group-name></group-name></get-bgp-group-information>";
        String str="<get-interface-information>"
        		+ "<terse></terse>"
        		+ "</get-interface-information>";
     //   String str="<get-bgp-neighbor-information><neighbor-address>192.168.1.43</neighbor-address></get-bgp-neighbor-information>";
        //Create the device
        String str1= "show ipv6 neighbors";
        String ipv6Address = "<get-interface-information>" + "<terse></terse>" + "</get-interface-information>";

        
        try{
        	Device device = new Device("192.168.60.2","root","ecode123",null,830);
            device.connect();
        	XMLBuilder query = new XMLBuilder();
			XML q = query.createNewXML("get-software-information");
			XML rpc_reply = device.executeRPC(q);
        	        
        //String cli_reply = device.runCliCommand(str1);        
        System.out.println(rpc_reply.toString());
        device.close();
        }
    catch (Exception e) {
    e.printStackTrace();
    
    }
        //NetconfSession ns1 =device.createNetconfSession();
        
        System.out.println("_@_)@ = "+str1);
    	XMLBuilder query = new XMLBuilder();
    	/*String strcommand="<get-interface-information>"
    			+ "<terse/></get-interface-information>";*/
		//XML q = query.createNewXML("get-bgp-protocols-group-information");
    	//XML q = query.createNewXML(ftp_config);
        //Send RPC and receive RPC Reply as XML
       // XML rpc_reply = ns1.executeRPC(str);
        //Print the RPC-Reply and close the device.
        System.out.println("____________________________________________________________________");
       // System.out.println(rpc_reply.toString());
        System.out.println("____________________________________________________________________");
        

    
    }
}
