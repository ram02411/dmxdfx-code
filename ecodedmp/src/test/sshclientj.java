package test;



import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import org.apache.oro.text.regex.MalformedPatternException;

import com.ecode.topology.viewer.data.device.Router;
import com.jcraft.jsch.ChannelShell;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.juniper.container.data.model.configuration.protocols.Bgp;
import com.juniper.container.data.model.configuration.protocols.BgpGroup;

import expect4j.Closure;
import expect4j.Expect4j;
import expect4j.ExpectState;
import expect4j.matches.Match;
import expect4j.matches.RegExpMatch;
 
public class sshclientj {
 
    private static final int COMMAND_EXECUTION_SUCCESS_OPCODE = -1;
    private static String ENTER_CHARACTER = "\r";
    private static final int SSH_PORT = 22;
    private List<String> lstCmds = new ArrayList<String>();
    private static String[] linuxPromptRegEx = new String[]{"%",">"};
 
    private Expect4j expect = null;
    private StringBuilder buffer = new StringBuilder();
    private String userName;
    private String password;
    private String host;
    private  Session session;
    ChannelShell channel;
 
    /**
     *
     * @param host
     * @param userName
     * @param password
     */
    public sshclientj(String host, String userName, String password) {
        this.host = host;
        this.userName = userName;
        this.password = password;
    }
    /**
     *
     * @param cmdsToExecute
     */
    public String execute(String cmdsToExecute) {
        
 
        Closure closure = new Closure() {
            public void run(ExpectState expectState) throws Exception {
                buffer.append(expectState.getBuffer());
            }
        };
        List<Match> lstPattern =  new ArrayList<Match>();
        for (String regexElement : linuxPromptRegEx) {
            try {
                Match mat = new RegExpMatch(regexElement, closure);
                lstPattern.add(mat);
            } catch (MalformedPatternException e) {
                e.printStackTrace();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
 
        try {
            expect = SSH();
            boolean isSuccess = true;
            
                isSuccess = isSuccess(lstPattern,cmdsToExecute);
                if (!isSuccess) {
                    isSuccess = isSuccess(lstPattern,cmdsToExecute);
                }
           
 
            checkResult(expect.expect(lstPattern));
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            closeConnection();
        }
        return buffer.toString();
    }
    /**
     *
     * @param objPattern
     * @param strCommandPattern
     * @return
     */
    private boolean isSuccess(List<Match> objPattern,String strCommandPattern) {
        try {
            boolean isFailed = checkResult(expect.expect(objPattern));
 
            if (!isFailed) {
                expect.send(strCommandPattern);
                expect.send(ENTER_CHARACTER);
                return true;
            }
            return false;
        } catch (MalformedPatternException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    /**
     *
     * @param hostname
     * @param username
     * @param password
     * @param port
     * @return
     * @throws Exception
     */
    private Expect4j SSH() throws Exception {
        JSch jsch = new JSch();
       session= jsch.getSession(userName, host, SSH_PORT);
        if (password != null) {
            session.setPassword(password);
        }
        Hashtable<String,String> config = new Hashtable<String,String>();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect(60000);
        channel  = (ChannelShell) session.openChannel("shell");
        Expect4j expect = new Expect4j(channel.getInputStream(), channel.getOutputStream());
        channel.connect();
        return expect;
    }
    
    public void disconnectionSession(){
    	if(channel!=null){
    		channel.disconnect();
    	}
    	if(session!=null){
    		session.disconnect();
    		System.out.println("Session is disconnected!!!!!");
    	}
    }
    /**
     *
     * @param intRetVal
     * @return
     */
    private boolean checkResult(int intRetVal) {
        if (intRetVal == COMMAND_EXECUTION_SUCCESS_OPCODE) {
            return true;
        }
        return false;
    }
    /**
     *
     */
    private void closeConnection() {
        if (expect!=null) {
            expect.close();
        }
    }
    
    
   
    public static void main(String[] args) {
    	sshclientj ssh=null;
    	try {
        System.out.println("_#####Executing command  Please wait#######");
    	//ssh = new SSHClient("192.168.1.80", "cisco", "cisco");
        List<String> cmdsToExecute = new ArrayList<String>();
        
       /* cmdsToExecute.add("config");
    	cmdsToExecute.add("route-policy TEST_PASS_ALL");
    	cmdsToExecute.add("pass");
    	cmdsToExecute.add("end-policy");
    	cmdsToExecute.add("!");
   	    cmdsToExecute.add("exit");
        cmdsToExecute.add("commit");
        String outputLog = ssh.execute(cmdsToExecute);
        System.out.println(outputLog);
        ssh.disconnectionSession();*/
     
        //ssh = new sshclientj("192.168.60.2", "root", "ecode123");
         //cmdsToExecute = new ArrayList<String>();
         //cmdsToExecute.add("cli");
         //cmdsToExecute.add("show version");
           //cmdsToExecute.add("router bgp 65001");
       //cmdsToExecute.add("bgp log-neighbor-changes");
        //cmdsToExecute.add("network 4.4.4.0");
       //cmdsToExecute.add("end-policy");
       // cmdsToExecute.add("neighbor 192.168.100.10 remote-as 65001");
        //cmdsToExecute.add("neighbor 192.168.100.40 remote-as 65001");
        
       /* cmdsToExecute.add("network 192.168.200.0/24");
        cmdsToExecute.add("!");
        cmdsToExecute.add("neighbor 192.168.1.50");
        cmdsToExecute.add("remote-as 1234");
        cmdsToExecute.add("address-family ipv4 unicast");
        cmdsToExecute.add("route-policy TEST_PASS_ALL in");
        cmdsToExecute.add("route-policy TEST_PASS_ALL out");*/
        
       
        
        
        System.out.println("__Please wait processing is going on__ ");
        //String outputLog = ssh.execute("cli");
         //outputLog = ssh.execute("show version");
        //System.out.println(outputLog);
        
    	}catch(Exception exe){
    		exe.printStackTrace();
    	}finally{
    		if(ssh!=null)
    		  ssh.disconnectionSession();
    	}
      
    }
}
