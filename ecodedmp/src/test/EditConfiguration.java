package test;

/*
 * Copyright (c) 2013 Juniper Networks, Inc.
 * All Rights Reserved
 *
 * Use is subject to license terms.
 *
 */

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import net.juniper.netconf.CommitException;
import net.juniper.netconf.LoadException;
import net.juniper.netconf.NetconfException;
import org.xml.sax.SAXException;

import net.juniper.netconf.Device;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;


public class EditConfiguration {
    public static void main(String[] args) throws LoadException, IOException, 
            NetconfException, ParserConfigurationException, SAXException {
        
        /*Build the XML configuration
         *The XML configuration required is:
         *
         * <configuration>
         *     <system>
         *         <services>
         *             <ftp/>
         *         </services>
         *     </system>
         * </configuration>
         */
         XMLBuilder builder = new XMLBuilder();
         XML ftp_config = builder.createNewConfig("system", "services", "ftp");
         System.out.println(ftp_config.toString());
         System.out.println("_______________________________________");
         System.out.println("_______________________________________");
         System.out.println("_______________________________________");
         System.out.println("_______________________________________");
         System.out.println("_______________________________________");
         
         String str="<protocols xmlns=\"http://xml.juniper.net/xnm/1.1/xnm\">"
    +"<bgp>"
       +" <group>"
            +"<name>GroupGK</name>"
            +"<type>internal</type>"
            +"<local-address>192.168.10.2</local-address>"
            
            +"<neighbor>"
               +" <name>192.168.10.4</name>"
                +"<peer-as>65000</peer-as>"
                +"<local-as>"
                   +" <as-number>65000</as-number>"
                +"</local-as>"
            +"</neighbor>"
        +"</group>"
    +"</bgp>"
+"</protocols>";
         
         /*String str="<policy-options>"
         +"<policy-statement>"
            +" <name>testexp1</name>"
             +"<from>"
                +" <protocol>static</protocol>"
             +"</from>"
             +"<then>"
                +" <accept/>"
             +"</then>"
         +"</policy-statement>"
         +"<policy-statement>"
            +" <name>testimp1</name>"
             +"<from>"
                +" <protocol>static</protocol>"
             +"</from>"
             +"<then>"
                +" <accept/>"
             +"</then>"
         +"</policy-statement>"
     +"</policy-options>";*/
         
         String strcommand="show ipv6 neighbor";
         System.out.println(str);
         //Create the device
         Device device = new Device("192.168.10.2","root","ecode123",null);
         device.connect();

         //Lock the configuration first
         

         //Load and commit the configuration
         try {
             device.loadXMLConfiguration(str, "merge");
             device.commit();
             device.close();
         } catch(LoadException e) {
             System.out.println(e.getMessage());
             return;
         } catch(CommitException e) {
             System.out.println(e.getMessage());
             return;
         }
         }
    
}

