package test;

import com.ecode.topology.viewer.data.device.Router;
import com.ecode.topology.viewer.data.device.parser.IPV6AddressXmlParser;

public class RouterMain {
	
	public static void main(String[] args) {
		
		Router router=new Router();
		router.setHostname("Mockito");
		router.setIp("192.168.1.42");
		router.setUsername("root");
		router.setPassword("ecode123");
		IPV6AddressXmlParser.getNetworkInfo(router);
        System.out.println("____ =DONE=_______");
		
	}

}
