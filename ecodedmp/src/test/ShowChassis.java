package test;

/*
 * Copyright (c) 2013 Juniper Networks, Inc.
 * All Rights Reserved
 *
 * Use is subject to license terms.
 *
 */

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import net.juniper.netconf.Device;
import net.juniper.netconf.NetconfException;
import net.juniper.netconf.NetconfSession;
import net.juniper.netconf.XML;
import net.juniper.netconf.XMLBuilder;

public class ShowChassis {
    public static void main(String args[]) throws NetconfException, 
              ParserConfigurationException, SAXException, IOException {
        
        XMLBuilder builder = new XMLBuilder();
        XML ftp_config = builder.createNewConfig("get-config", "source", "running");
        System.out.println(ftp_config.toString());
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        System.out.println("_______________________________________");
        
        String str="  <get-config>"+
       " <source>"+
       "     <running/>"+
        "</source>"+
  "  </get-config>";
        
        //Create the device
        Device device = new Device("192.168.1.70","cisco","cisco",null);
        //Device device = new Device("192.168.1.50","root","ecode123",null);
        device.connect();
        
        XML capabilities = device.getRunningConfig();
        System.out.println("____capabilities_____   =  "+capabilities);
        
        //NetconfSession ns1 =device.createNetconfSession();
        
        System.out.println("_@_)@ = "+str);
        
        //Send RPC and receive RPC Reply as XML
        XML rpc_reply = device.executeRPC(str);
        /* OR
         * device.executeRPC("<get-chassis-inventory/>");
         * OR
         * device.executeRPC("<rpc><get-chassis-inventory/></rpc>");
         */
        
        //Print the RPC-Reply and close the device.
        System.out.println(rpc_reply.toString());
        device.close();
    }
}
