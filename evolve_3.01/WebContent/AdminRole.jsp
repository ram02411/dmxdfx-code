<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="bean.*"%>
<%@ page language="java" import="dto.*"%>
<%@ page import="java.util.*"%>	
	
<%
	
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<%List<List<String>> alert =new ArrayList<List<String>>();
List<String> switches=new ArrayList<String>();
 try{

  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	//alert = alertPicker.getAlerts(); 
	
}
else{
 String redirectURL = "invalid.jsp";
 //response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>
		
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Evolve Panel" />
    <meta name="author" content="" />

    <title>Evolve | Administrators</title>

    
	<link rel="stylesheet" href="assets/css/font-icons/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link href="assets/js/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    
    <script>
    var status;
    var pagename;
        $(document).ready(function () {

            $('#openBtn').click(function () {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on('show.bs.modal', '.modal', function (event) {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function () {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);
            });
            $('.menu-toggle i').on('click', function () {
           	 pagename=this.id;
           });
            
            $('.menu-toggle').on('click', function () {
                var iSelector = $(this).find('i:first');
                status="modify";
                if (iSelector.hasClass('fa-check-circle-o')) {
                    iSelector.removeClass('fa-check-circle-o')
                    iSelector.addClass('fa-times-circle-o')
                    status="disable";
                }
                else if (iSelector.hasClass('fa-times-circle-o')) {
                    iSelector.removeClass('fa-times-circle-o')
                    iSelector.addClass('fa-lock')
                    status="readonly";
                }
                else {
                    iSelector.removeClass('fa-lock')
                    iSelector.addClass('fa-check-circle-o')
                }
                
                $.ajax({ 
            		url: 'settingstatus.jsp',
            		dataType: 'text',
            		data: {page:pagename, pagestatus:status },
            		success: function(data) {
            			
            		}
                });
            });
            $('.editmenu-toggle').on('click', function () {
                var eSelector = $(this).find('i:first');
                
                if (eSelector.hasClass('fa-check-circle-o')) {
                    eSelector.removeClass('fa-check-circle-o')
                    eSelector.addClass('fa-times-circle-o')
                    
                }
                else if (eSelector.hasClass('fa-times-circle-o')) {
                    eSelector.removeClass('fa-times-circle-o')
                    eSelector.addClass('fa-lock')
                    
                }
                else {
                    eSelector.removeClass('fa-lock')
                    eSelector.addClass('fa-check-circle-o')
                }
                
               
                });
        });
    
        
$(document).ready(function () {
	var data="<%=Leftmenuhtml.getHtmldata() %>";
	 
	document.getElementById("menuleft").innerHTML=data;
});
</script>
    <style>
        
      th, td { border: 1px solid; padding: .2em .7em; }
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
            background-color: #D4E0E0;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #E5EEEE;
        }

       
    </style>
</head>


<body class="page-body" data-url="http://ecodenetworks.com">

    <div class="page-container">
      
        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                   <a href="#" class="user-link" style="margin-top:-5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width:57px;height:53px;"/>
                        <span style="margin-left:8px">Welcome<strong>Admin</strong></span>
                       
                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>

   <div id="menuleft"></div>          
        </div>

        <div class="main-content">

            <div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    
                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <a><strong>Ecode Networks</strong> <i class="entypo-users"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-2').modal('show', {backdrop: 'static'});">
                            <strong>Commit</strong> <i class="entypo-download"></i></a>
                        </li>
                         <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-1').modal('show', {backdrop: 'static'});">
                            <strong>Save</strong> <i class="entypo-floppy"></i></a>
                        </li>

                        <li class="sep"></li>
                        

                        <li>
                            <a href="setout.jsp">
                                <strong>LogOut</strong>  <i class="entypo-logout right"></i></a>
                        </li>
                    </ul>
                </div>

            </div>

            <hr />

            <div class="row">
<nav class="navbar navbar-inverse" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Admin Roles</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#" onclick="addrole();"><i class="entypo-plus-squared"></i><span>Add</span></a>
			</li>
			<li><a href="#"  onclick="deleterole();"><i class="entypo-minus-squared"></i><span>Delete</span></a>
			</li>
			<li><a href="#" onClick="clonerole();"><i class="entypo-docs"></i><span>Clone</span></a>
			</li>
			
		</ul>
		
	</div>
	<!-- /.navbar-collapse -->
</nav>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Administrators</div>

                        <div class="panel-options">
                           <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>
                    
           <table class="table table-bordered responsive table-hover table-striped datatable" id="tableId">
                        <thead>
                            <tr>
                             	 <th>&nbsp;</th>
                                <th id="name"> Name</th>
                                <th id="description">Description</th>
                                <th id="role">Role</th>
                                <th id="cli">CLI</th>
                                
                            </tr>
                        </thead>
                       <tbody>
                       			<%
									int i=0;
								%>
								<%
									List<Rolebean> bean=UserRole.getallroles();
								%>
								<%
									for(;i<bean.size();i++){
								%>
                       			<tr>
									<td><input type="checkbox"  onClick="checkname('<%=bean.get(i).getRole_name()%>');"/></td>
									<td><a href="#" onClick="editrole('<%=bean.get(i).getRole_name()%>');" class="blue_popup"><%=bean.get(i).getRole_name()%></a></td>
									<td><%=bean.get(i).getDesc()%></td>
									<td></td>
									<td></td>
                       			</tr>
                       			<%
									}
								%>
                       
    </tbody>
                    </table>

                    <script>
                    var drole=0;
                    function checkname(rname){
                    	$("#rolenamed").text(rname);
                    	$('#rolenamedelete').val(rname);
                    	$.ajax({ 
                    		url: 'checkroleinuser.jsp',
                    		dataType: 'text',
                    		data: {rolename:rname},
                    		success: function(data) {
                    			
                    				drole=data;
                    			
                    		}
                        });
                    	
                    }
                    var statusr=<%=Statusbean.getAdminrole()%>
                    
                    
                    
                    function addrole(){
                    	$.ajax({ 
                    		url: 'settingsetters.jsp',
                    		dataType: 'text',
                    		success: function(data) {
                    			
                    		}
                        });
                    	
                    	if(statusr==2){            	
                    	   	jQuery('#modal-3').modal('hide');
                    	   	}
                    	else{
                    		
                    	   		jQuery('#modal-3').modal('show');
                    	   	}
                    }
                    function deleterole(){
                    	if(statusr==2){
                    		jQuery('#modal-4').modal('hide');
                    	}
                    	else{
                    		jQuery('#modal-4').modal('show');
                    	}
                    	
                    }
                    function editrole(roleedit){
                    	
                    	
                    	$('#rolenameedit').val(roleedit);	
                    	if(statusr==2){
                    		jQuery('#modal-5').modal('hide');
                    	}
                    	else{
                    		$.ajax({ 
                        		url: 'getrole.jsp',
                        		dataType: 'text',
                        		data: {erolename:roleedit},
                        		success: function(data) {                       			
                        			role=data.split('&');
                        			
                        			$('#editrolename').val(role[0]);	
                        			$('#editroledesc').val(role[1]);	
                        			disable=role[3];
                        			
                        			if(disable === 'null'){}
                        			else if(disable.indexOf(",") >= 0){
                        				dpages=disable.split(',');
                        				for (i = 0; i < dpages.length; i++) {
                        					if(dpages[i].toLowerCase() === 'dashboardli'){
                        						$('#edashboardlist').removeClass('fa-check-circle-o');
                                    			$('#edashboardlist').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'dashboard'){
                        						$('#edashboard').removeClass('fa-check-circle-o');
                                    			$('#edashboard').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'controlleralerts'){
                        						$('#econtrolleralerts').removeClass('fa-check-circle-o');
                                    			$('#econtrolleralerts').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'administratorli'){
                        						$('#eadministratorli').removeClass('fa-check-circle-o');
                                    			$('#eadministratorli').toggleClass('fa-times-circle-o');
                                    			$('#eadministrator').removeClass('fa-check-circle-o');
                                    			$('#eadministrator').toggleClass('fa-times-circle-o');
                                    			$('#eadminrole').removeClass('fa-check-circle-o');
                                    			$('#eadminrole').toggleClass('fa-times-circle-o');
                        					}
                        					
                        					if(dpages[i].toLowerCase() === 'administrator'){
                        						$('#eadministrator').removeClass('fa-check-circle-o');
                                    			$('#eadministrator').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'adminrole'){
                        						$('#eadminrole').removeClass('fa-check-circle-o');
                                    			$('#eadminrole').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'design'){
                        						$('#edesign').removeClass('fa-check-circle-o');
                                    			$('#edesign').toggleClass('fa-times-circle-o');
                                    			$('#ecanvas').removeClass('fa-check-circle-o');
                                    			$('#ecanvas').toggleClass('fa-times-circle-o');
                                    			$('#eviewer').removeClass('fa-check-circle-o');
                                    			$('#eviewer').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'canvas'){
                        						$('#ecanvas').removeClass('fa-check-circle-o');
                                    			$('#ecanvas').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'viewer'){
                        						$('#eviewer').removeClass('fa-check-circle-o');
                                    			$('#eviewer').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'control'){
                        						$('#econtrol').removeClass('fa-check-circle-o');
                                    			$('#econtrol').toggleClass('fa-times-circle-o');
                                    			$('#erulebase').removeClass('fa-check-circle-o');
                                    			$('#erulebase').toggleClass('fa-times-circle-o');
                                    			$('#epolicybase').removeClass('fa-check-circle-o');
                                    			$('#epolicybase').toggleClass('fa-times-circle-o');
                                    			$('#esimulate').removeClass('fa-check-circle-o');
                                    			$('#esimulate').toggleClass('fa-times-circle-o');
                                    			
                        					}
                        					if(dpages[i].toLowerCase() === 'rulebase'){
                        						$('#erulebase').removeClass('fa-check-circle-o');
                                    			$('#erulebase').toggleClass('fa-times-circle-o');
                        					}
                        					
                        					if(dpages[i].toLowerCase() === 'policybase'){
                        						$('#epolicybase').removeClass('fa-check-circle-o');
                                    			$('#epolicybase').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'simulate'){
                        						$('#esimulate').removeClass('fa-check-circle-o');
                                    			$('#esimulate').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'tags'){
                        						$('#etags').removeClass('fa-check-circle-o');
                                    			$('#etags').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'securityrulebase'){
                        						$('#esecurityrulebase').removeClass('fa-check-circle-o');
                                    			$('#esecurityrulebase').toggleClass('fa-times-circle-o');
                        					}
                        					if(dpages[i].toLowerCase() === 'flowrector'){
                        						$('#eflowrector').removeClass('fa-check-circle-o');
                                    			$('#eflowrector').toggleClass('fa-times-circle-o');
                        					}
                        					
                        					
                        					
                        					
                        					
                        				}
                        			}
                        			else{
                        			
                        				if(disable.toLowerCase() === 'dashboardli'){
                    						$('#edashboardlist').removeClass('fa-check-circle-o');
                                			$('#edashboardlist').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'dashboard'){
                    						$('#edashboard').removeClass('fa-check-circle-o');
                                			$('#edashboard').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'controlleralerts'){
                    						$('#econtrolleralerts').removeClass('fa-check-circle-o');
                                			$('#econtrolleralerts').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'administratorli'){
                    						$('#eadministratorli').removeClass('fa-check-circle-o');
                                			$('#eadministratorli').toggleClass('fa-times-circle-o');
                                			$('#eadministrator').removeClass('fa-check-circle-o');
                                			$('#eadministrator').toggleClass('fa-times-circle-o');
                                			$('#eadminrole').removeClass('fa-check-circle-o');
                                			$('#eadminrole').toggleClass('fa-times-circle-o');
                                			
                    					}
                    					
                    					if(disable.toLowerCase() === 'administrator'){
                    						$('#eadministrator').removeClass('fa-check-circle-o');
                                			$('#eadministrator').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'adminrole'){
                    						$('#eadminrole').removeClass('fa-check-circle-o');
                                			$('#eadminrole').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'design'){
                    						$('#edesign').removeClass('fa-check-circle-o');
                                			$('#edesign').toggleClass('fa-times-circle-o');
                                			$('#ecanvas').removeClass('fa-check-circle-o');
                                			$('#ecanvas').toggleClass('fa-times-circle-o');
                                			$('#eviewer').removeClass('fa-check-circle-o');
                                			$('#eviewer').toggleClass('fa-times-circle-o');
                    					}
                    					
                    					if(disable.toLowerCase() === 'canvas'){
                    						$('#ecanvas').removeClass('fa-check-circle-o');
                                			$('#ecanvas').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'viewer'){
                    						$('#eviewer').removeClass('fa-check-circle-o');
                                			$('#eviewer').toggleClass('fa-times-circle-o');
                    					}
                    					
                    					if(disable.toLowerCase() === 'control'){
                    						$('#econtrol').removeClass('fa-check-circle-o');
                                			$('#econtrol').toggleClass('fa-times-circle-o');
                                			$('#erulebase').removeClass('fa-check-circle-o');
                                			$('#erulebase').toggleClass('fa-times-circle-o');
                                			$('#epolicybase').removeClass('fa-check-circle-o');
                                			$('#epolicybase').toggleClass('fa-times-circle-o');
                                			$('#esimulate').removeClass('fa-check-circle-o');
                                			$('#esimulate').toggleClass('fa-times-circle-o');
                    					}
                    					
                    					if(disable.toLowerCase() === 'rulebase'){
                    						$('#erulebase').removeClass('fa-check-circle-o');
                                			$('#erulebase').toggleClass('fa-times-circle-o');
                    					}
                    					
                    					if(disable.toLowerCase() === 'policybase'){
                    						$('#epolicybase').removeClass('fa-check-circle-o');
                                			$('#epolicybase').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'simulate'){
                    						$('#esimulate').removeClass('fa-check-circle-o');
                                			$('#esimulate').toggleClass('fa-times-circle-o');
                    					}
                    					
                    					if(disable.toLowerCase() === 'tags'){
                    						$('#etags').removeClass('fa-check-circle-o');
                                			$('#etags').toggleClass('fa-times-circle-o');
                    					}
                    					if(disable.toLowerCase() === 'securityrulebase'){
                    						$('#esecurityrulebase').removeClass('fa-check-circle-o');
                                			$('#esecurityrulebase').toggleClass('fa-times-circle-o');
                    					}
                    					
                    					if(disable.toLowerCase() === 'flowrector'){
                    						$('#eflowrector').removeClass('fa-check-circle-o');
                                			$('#eflowrector').toggleClass('fa-times-circle-o');
                    					}
                        			}
                        			
                        		reado=role[2];
                        			if(reado === 'null'){
                        				
                        			}
                        			else if(reado.indexOf(",") >= 0){
                        				rpages=reado.split(',');
                        				for (j = 0; j < reado.length;j++) {
                        					if(rpages[j].toLowerCase() === 'dashboardli'){
                        						$('#edashboardlist').removeClass('fa-check-circle-o');
                                    			$('#edashboardlist').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'dashboard'){
                        						$('#edashboard').removeClass('fa-check-circle-o');
                                    			$('#edashboard').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'controlleralerts'){
                        						$('#econtrolleralerts').removeClass('fa-check-circle-o');
                                    			$('#econtrolleralerts').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'administratorli'){
                        						$('#eadministratorli').removeClass('fa-check-circle-o');
                                    			$('#eadministratorli').toggleClass('fa-lock');
                                    			$('#eadministrator').removeClass('fa-check-circle-o');
                                    			$('#eadministrator').toggleClass('fa-lock');
                                    			$('#eadminrole').removeClass('fa-check-circle-o');
                                    			$('#eadminrole').toggleClass('fa-lock');
                        					}
                        					
                        					
                        					if(rpages[j].toLowerCase() === 'administrator'){
                        						$('#eadministrator').removeClass('fa-check-circle-o');
                                    			$('#eadministrator').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'adminrole'){
                        						$('#eadminrole').removeClass('fa-check-circle-o');
                                    			$('#eadminrole').toggleClass('fa-lock');
                                    			
                        					}
                        					
                        					if(rpages[j].toLowerCase() === 'design'){
                        						$('#edesign').removeClass('fa-check-circle-o');
                                    			$('#edesign').toggleClass('fa-lock');
                                    			$('#ecanvas').removeClass('fa-check-circle-o');
                                    			$('#ecanvas').toggleClass('fa-lock');
                                    			$('#eviewer').removeClass('fa-check-circle-o');
                                    			$('#eviewer').toggleClass('fa-lock');
                        					}
                        					
                        					if(rpages[j].toLowerCase() === 'canvas'){
                        						$('#ecanvas').removeClass('fa-check-circle-o');
                                    			$('#ecanvas').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'viewer'){
                        						$('#eviewer').removeClass('fa-check-circle-o');
                                    			$('#eviewer').toggleClass('fa-lock');
                        					}
                        					
                        					if(rpages[j].toLowerCase() === 'control'){
                        						$('#econtrol').removeClass('fa-check-circle-o');
                                    			$('#econtrol').toggleClass('fa-lock');
                                    			$('#erulebase').removeClass('fa-check-circle-o');
                                    			$('#erulebase').toggleClass('fa-lock');
                                    			$('#epolicybase').removeClass('fa-check-circle-o');
                                    			$('#epolicybase').toggleClass('fa-lock');
                                    			$('#esimulate').removeClass('fa-check-circle-o');
                                    			$('#esimulate').toggleClass('fa-lock');
                        					}
                        					
                        					if(rpages[j].toLowerCase() === 'rulebase'){
                        						$('#erulebase').removeClass('fa-check-circle-o');
                                    			$('#erulebase').toggleClass('fa-lock');
                        					}
                        					
                        					if(rpages[j].toLowerCase() === 'policybase'){
                        						$('#epolicybase').removeClass('fa-check-circle-o');
                                    			$('#epolicybase').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'simulate'){
                        						$('#esimulate').removeClass('fa-check-circle-o');
                                    			$('#esimulate').toggleClass('fa-lock');
                        					}
                        					
                        					if(rpages[j].toLowerCase() === 'tags'){
                        						$('#etags').removeClass('fa-check-circle-o');
                                    			$('#etags').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'securityrulebase'){
                        						$('#esecurityrulebase').removeClass('fa-check-circle-o');
                                    			$('#esecurityrulebase').toggleClass('fa-lock');
                        					}
                        					if(rpages[j].toLowerCase() === 'flowrector'){
                        						$('#eflowrector').removeClass('fa-check-circle-o');
                                    			$('#eflowrector').toggleClass('fa-lock');
                        					}
                        					
                        					
                        					
                        					
                        					
                        				}
                        			}
                        			else{
                        			
                        				if(reado.toLowerCase() === 'dashboardli'){
                    						$('#edashboardlist').removeClass('fa-check-circle-o');
                                			$('#edashboardlist').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'dashboard'){
                    						$('#edashboard').removeClass('fa-check-circle-o');
                                			$('#edashboard').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'controlleralerts'){
                    						$('#econtrolleralerts').removeClass('fa-check-circle-o');
                                			$('#econtrolleralerts').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'administratorli'){
                    						$('#eadministratorlist').removeClass('fa-check-circle-o');
                                			$('#eadministratorlist').toggleClass('fa-lock');
                                			$('#eadministrator').removeClass('fa-check-circle-o');
                                			$('#eadministrator').toggleClass('fa-lock');
                                			$('#eadminrole').removeClass('fa-check-circle-o');
                                			$('#eadminrole').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'administrator'){
                    						$('#eadministrator').removeClass('fa-check-circle-o');
                                			$('#eadministrator').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'adminrole'){
                    						$('#eadminrole').removeClass('fa-check-circle-o');
                                			$('#eadminrole').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'design'){
                    						$('#edesign').removeClass('fa-check-circle-o');
                                			$('#edesign').toggleClass('fa-lock');
                                			$('#ecanvas').removeClass('fa-check-circle-o');
                                			$('#ecanvas').toggleClass('fa-lock');
                                			$('#eviewer').removeClass('fa-check-circle-o');
                                			$('#eviewer').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'canvas'){
                    						$('#ecanvas').removeClass('fa-check-circle-o');
                                			$('#ecanvas').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'viewer'){
                    						$('#eviewer').removeClass('fa-check-circle-o');
                                			$('#eviewer').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'control'){
                    						$('#econtrol').removeClass('fa-check-circle-o');
                                			$('#econtrol').toggleClass('fa-lock');
                                			$('#erulebase').removeClass('fa-check-circle-o');
                                			$('#erulebase').toggleClass('fa-lock');
                                			$('#epolicybase').removeClass('fa-check-circle-o');
                                			$('#epolicybase').toggleClass('fa-lock');
                                			$('#esimulate').removeClass('fa-check-circle-o');
                                			$('#esimulate').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'rulebase'){
                    						$('#erulebase').removeClass('fa-check-circle-o');
                                			$('#erulebase').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'policybase'){
                    						$('#epolicybase').removeClass('fa-check-circle-o');
                                			$('#epolicybase').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'simulate'){
                    						$('#esimulate').removeClass('fa-check-circle-o');
                                			$('#esimulate').toggleClass('fa-lock');
                    					}
                    					
                    					if(reado.toLowerCase() === 'tags'){
                    						$('#etags').removeClass('fa-check-circle-o');
                                			$('#etags').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'securityrulebase'){
                    						$('#esecurityrulebase').removeClass('fa-check-circle-o');
                                			$('#esecurityrulebase').toggleClass('fa-lock');
                    					}
                    					if(reado.toLowerCase() === 'flowrector'){
                    						$('#eflowrector').removeClass('fa-check-circle-o');
                                			$('#eflowrector').toggleClass('fa-lock');
                    					}
                        			}
                        			
                        			
                        		}
                            });
                    		
                    		jQuery('#modal-5').modal('show');
                    	}
                    }
                                           
                    function clonerole(){
                    	var roname=document.getElementById("rolenamedelete").value;
                    	if(statusr==2){
                    		
                    	}
                    	else{
                    		
                    		$.ajax({ 
                        		url: 'clone.jsp',
                        		dataType: 'text',
                        		data: {rolename:roname},
                        		success: function(data) {
                        			
                        		}
                            });
                    	}
                    	setTimeout(function () { window.location.reload(); }, 10);
                    }
                    </script>
               

                </div>

                
            </div>
            <!-- Footer -->
            <footer class="main">
                Copyright &copy; 2016 <strong>Ecode Networks,</strong>All Rights Reservered

	
            </footer>
        </div>


    </div>


    <div class="modal fade" id="modal-2">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Commit</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Doing a commit will overwrite the running configuration. Do you want to continue?</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
                <button type="button" class="btn btn-info">Preview Changes</button>
                <button type="button" class="btn btn-info">Validate Changes</button>
                <button type="button" class="btn btn-info">Commit</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
              
			</div>
		</div>
	</div>
</div>

    <div class="modal fade" id="modal-1">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Save Configurations</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Saving this configuration will overwrite the previously saved configuration.
                              Do you want to continue?</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-info">Ok</button>
              
			</div>
		</div>
	</div>
</div>
    
    <div class="modal fade" id="modal-4">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Delete Role</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Do you really want to delete role : <span id="rolenamed"></span> 
							 <input type="hidden" id="rolenamedelete" name="rolenamedelete"/></label>
							 
                             
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-info" onClick="deletingrole();">Ok</button>
              
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    function deletingrole(){
    	
    	
    	if(drole == 1 ){
    		
    		var rolenamed=document.getElementById("rolenamedelete").value;
    		$.ajax({ 
        		url: 'deleterole.jsp',
        		dataType: 'text',
        		data: {rolename:rolenamed},
        		success: function(data) {
        			
        		}
            });
    	}
    	else{
    		alert("User existed with this role");
    	}
    	setTimeout(function () { window.location.reload(); }, 10);
    }
    </script>
    
   
   
    <div class="modal fade" id="modal-5">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Admin Role Profile</strong></h4>
			</div>
			
			<div class="modal-body">
               
                <div class="form-group">
						<label for="field-1" class="col-md-3 control-label">Name</label>
						
						<div class="col-md-9">
							<input type="text" name="editrolename" class="form-control" id="editrolename" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
							<input type="hidden" id="rolenameedit" name="rolenameedit"/>
						</div>
					</div>
                <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Description</label>
						
						<div class="col-md-9">
						<input type="text" class="form-control" name="editroledesc" id="editroledesc" data-validate="required" data-message-required="Required Field" placeholder="Enter Description">	
						</div>
					</div>
                <div class="form-group">
			 
                <ul class="nav nav-tabs bordered primary">
			<li class="active">
				<a href="#General" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">Web UI</span>
				</a>
			</li>
			<li>
				<a href="#source" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">XML API</span>
				</a> 
			</li>
			<li>
				<a href="#user" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">Command Line</span>
				</a>
			</li>
		</ul>
		        <div class="tab-content">
			    <div class="tab-pane active" id="General">
                <div class="scrollable" data-height="120">
               <ul style="padding:0px">
        <li>
            <a class="editmenu-toggle" href="#"><i  id="edashboardlist" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Dashboard</label>
            <ul>
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="edashboard" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Dashboard</label>
                 </li>
                 
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="econtrolleralerts" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">ControllerAlerts</label>
                 </li>
                  </ul>
        </li>
         <li>
            <a class="editmenu-toggle" href="#"><i id="eadministratorli" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Administrators</label>
              <ul>
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="eadministrator" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Administrator</label>
                 </li>
                 
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="eadminrole" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">AdminRole</label>
                 </li>
                  </ul>
        </li>
         <li>
            <a class="editmenu-toggle" href="#"><i id="edesign" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Design</label>
              <ul>
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="ecanvas" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Canvas</label>
                 </li>
                 
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="eviewer" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Viewer</label>
                 </li>
                  </ul>
        </li>
        <li>
           <a class="editmenu-toggle" href="#"><i id="etags" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Tags</label>
        </li>
        <li>
           <a class="editmenu-toggle" href="#"><i id="esecurityrulebase" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">SecurityRulebase</label>
        </li>
        <li>
            <a class="editmenu-toggle" href="#"><i id="econtrol" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Control</label>
              <ul>
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="erulebase" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">RuleBase</label>
                 </li>
                 
                 <li>
                     <a class="editmenu-toggle" href="#"><i id="epolicybase" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">PolicyBase</label>
                 </li>
                 
                  <li>
                     <a class="editmenu-toggle" href="#"><i id="esimulate" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Simulate</label>
                 </li>
                  </ul>
            
        </li>
         <li>
           <a class="editmenu-toggle" href="#"><i id="eflowrector" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Flowrector</label>
        </li>
         
    </ul>
                    </div>
			</div>
                <div class="tab-pane" id="source">
				<div class="scrollable" data-height="120">
                   <ul style="padding:0px">
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Report</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Log</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Configuration</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Operational Requests</label>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Commit</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">User-ID Agent</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Export</label>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Import</label>
        </li>
    </ul>
</div>
			</div>
			    <div class="tab-pane" id="user">
		         <div class="row">
                    <div class="form-group">
						
						<div class="col-md-12">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">superuser</option>
								<option value="2">superreader</option>
                                <option value="3">deviceadmin</option>
								<option value="4">devicereader</option>
							</select>
						</div>
					</div>
                       </div>
                 </div>
                		
			</div>	
		</div>
                <div class="row" style="background-color:ActiveCaption">
                    <label style="color:black;margin-left:10px">Legend: <i class="fa fa-check-circle-o" style="margin:8px;"><span style="margin-left:3px">Enable</span></i> <i class="fa fa-times-circle-o" style="margin:8px;"><span style="margin-left:3px">Disable</span></i><i class="fa fa-lock" style="margin:8px;"><span style="margin-left:3px">Read-Only</span></i></label>
                </div>
                <div class="form-group" style="text-align:right; margin-right:10px">
                <button type="button" class="btn btn-info" onClick="updaterole();">Ok</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
                </div>	
                    
		</div>
               
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function updaterole(){
		var roletoedit=document.getElementById("rolenameedit").value;
		var editname=document.getElementById("editrolename").value;
		var editdesc=document.getElementById("editroledesc").value;
		
		var readpages;
		var disablepages;
		if($('#eadministratorli').hasClass('fa-times-circle-o')){
			disablepages += ',administratorli';			
		}
		else if($('#eadministratorli').hasClass('fa-lock')){
			readpages += ',administratorli';			
		}
		if($('#eadministrator').hasClass('fa-times-circle-o')){
			disablepages += ',administrator';			
		}
		else if($('#eadministrator').hasClass('fa-lock')){
			readpages += ',administrator';			
		}
		if($('#eadminrole').hasClass('fa-times-circle-o')){
			disablepages += ',adminrole';			
		}
		else if($('#eadminrole').hasClass('fa-lock')){
			readpages += ',adminrole';			
		}
		
		if($('#edesign').hasClass('fa-times-circle-o')){
			disablepages += ',design';			
		}
		else if($('#edesign').hasClass('fa-lock')){
			readpages += ',design';			
		}
		
		if($('#ecanvas').hasClass('fa-times-circle-o')){
			disablepages += ',canvas';			
		}
		else if($('#ecanvas').hasClass('fa-lock')){
			readpages += ',canvas';			
		}
		
		if($('#eviewer').hasClass('fa-times-circle-o')){
			disablepages += ',viewer';			
		}
		else if($('#eviewer').hasClass('fa-lock')){
			readpages += ',viewer';			
		}
		if($('#econtrol').hasClass('fa-times-circle-o')){
			disablepages += ',control';			
		}
		else if($('#econtrol').hasClass('fa-lock')){
			readpages += ',control';			
		}
		
		if($('#etags').hasClass('fa-times-circle-o')){
			disablepages += ',tags';			
		}
		else if($('#etags').hasClass('fa-lock')){
			readpages += ',tags';			
		}
		if($('#esecurityrulebase').hasClass('fa-times-circle-o')){
			disablepages += ',securityrulebase';			
		}
		else if($('#esecurityrulebase').hasClass('fa-lock')){
			readpages += ',securityrulebase';			
		}
		
		if($('#eruleabse').hasClass('fa-times-circle-o')){
			disablepages += ',rulebase';			
		}
		else if($('#erulebase').hasClass('fa-lock')){
			readpages += ',rulebase';			
		}
		
		if($('#epolicybase').hasClass('fa-times-circle-o')){
			disablepages += ',policybase';			
		}
		else if($('#epolicybase').hasClass('fa-lock')){
			readpages += ',policybase';			
		}
		
		if($('#esimulate').hasClass('fa-times-circle-o')){
			disablepages += ',simulate';			
		}
		else if($('#esimulate').hasClass('fa-lock')){
			readpages += ',simulate';			
		}
		
		if($('#eflowrector').hasClass('fa-times-circle-o')){
			disablepages += ',flowrector';			
		}
		else if($('#eflowrector').hasClass('fa-lock')){
			readpages += ',flowrector';			
		}
		
		$.ajax({ 
    		url: 'Updaterole.jsp',
    		dataType: 'text',
    		data: {editrolename : editname,editroledesc : editdesc,rolenameedit : roletoedit,readonly : readpages,disable : disablepages },
    		success: function(data) {
    			
    		}
        });
		jQuery('#modal-5').modal('hide');
		
		setTimeout(function () { window.location.reload(); }, 10);
	}
	
	
	</script>
	
	<div class="modal fade" id="modal-3">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Admin Role Profile</strong></h4>
			</div>
			
			<div class="modal-body">
                <form role="form" action="Createrole.jsp" method="POST">
                <div class="form-group">
						<label for="field-1" class="col-md-3 control-label">Name</label>
						
						<div class="col-md-9">
							<input type="text" name="rolename" class="form-control" id="field-1" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
						</div>
					</div>
                <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Description</label>
						
						<div class="col-md-9">
						<input type="text" class="form-control" name="roledesc" id="field-2" data-validate="required" data-message-required="Required Field" placeholder="Enter Description">	
						
						</div>
					</div>
                <div class="form-group">
			 
                <ul class="nav nav-tabs bordered primary">
			<li class="active">
				<a href="#General" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">Web UI</span>
				</a>
			</li>
			<li>
				<a href="#source" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">XML API</span>
				</a> 
			</li>
			<li>
				<a href="#user" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">Command Line</span>
				</a>
			</li>
		</ul>
		        <div class="tab-content">
			    <div class="tab-pane active" id="General">
                <div class="scrollable" data-height="120">
               <ul style="padding:0px">
        <li>
            <a class="menu-toggle" href="#"><i  id="Dashboardlist" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Dashboard</label>
            <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="dashboard" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Dashboard</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="controlleralerts" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">ControllerAlerts</label>
                 </li>
                  </ul>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i id="administratorli" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Administrators</label>
              <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="administrator" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Administrator</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="adminrole" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">AdminRole</label>
                 </li>
                  </ul>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i id="design" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Design</label>
              <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="canvas" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Canvas</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="viewer" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Viewer</label>
                 </li>
                  </ul>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i id="tags" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Tags</label>
        </li>
        <li>
           <a class="menu-toggle" href="#"><i id="securityrulebase" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">SecurityRulebase</label>
        </li>
        
        <li>
            <a class="menu-toggle" href="#"><i id="control" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Control</label>
              <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="rulebase" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">RuleBase</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="policybase" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">PolicyBase</label>
                 </li>
                 
                  <li>
                     <a class="menu-toggle" href="#"><i id="simulate" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Simulate</label>
                 </li>
                  </ul>
            
        </li>
           <a class="menu-toggle" href="#"><i id="flowrector" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Flowrector</label>
        </li>
         
    </ul>
                    </div>
			</div>
                <div class="tab-pane" id="source">
				<div class="scrollable" data-height="120">
                   <ul style="padding:0px">
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Report</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Log</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Configuration</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Operational Requests</label>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Commit</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">User-ID Agent</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Export</label>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Import</label>
        </li>
    </ul>
</div>
			</div>
			    <div class="tab-pane" id="user">
		         <div class="row">
                    <div class="form-group">
						
						<div class="col-md-12">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">superuser</option>
								<option value="2">superreader</option>
                                <option value="3">deviceadmin</option>
								<option value="4">devicereader</option>
							</select>
						</div>
					</div>
                       </div>
                 </div>
                		
			</div>	
		</div>
                <div class="row" style="background-color:ActiveCaption">
                    <label style="color:black;margin-left:10px">Legend: <i class="fa fa-check-circle-o" style="margin:8px;"><span style="margin-left:3px">Enable</span></i> <i class="fa fa-times-circle-o" style="margin:8px;"><span style="margin-left:3px">Disable</span></i><i class="fa fa-lock" style="margin:8px;"><span style="margin-left:3px">Read-Only</span></i></label>
                </div>
                <div class="form-group" style="text-align:right; margin-right:10px">
                <button type="submit" class="btn btn-info">Ok</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
                </div>	
                    </form>
		</div>
               
			</div>
		</div>
	</div>
	
    <link rel="stylesheet" href="assets/js/icheck/skins/minimal/_all.css">
	<link rel="stylesheet" href="assets/js/icheck/skins/square/_all.css">
	<link rel="stylesheet" href="assets/js/icheck/skins/flat/_all.css">
	<link rel="stylesheet" href="assets/js/icheck/skins/futurico/futurico.css">
	<link rel="stylesheet" href="assets/js/icheck/skins/polaris/polaris.css">

    <link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/selectboxit/jquery.selectBoxIt.css">
    <script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="assets/js/typeahead.min.js"></script>
	<script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="assets/js/bootstrap-datepicker.js"></script>
	<script src="assets/js/bootstrap-timepicker.min.js"></script>
	<script src="assets/js/bootstrap-colorpicker.min.js"></script>
	<script src="assets/js/daterangepicker/moment.min.js"></script>
	<script src="assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="assets/js/jquery.multi-select.js"></script>
	<script src="assets/js/icheck/icheck.min.js"></script>
	<script src="assets/js/neon-chat.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>
