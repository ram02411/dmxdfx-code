<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" import="sources.*" %>
<%@ page language="java" import="java.util.*" %>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="java.io.*" %>
<%@ page language="java" import="bean.*" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<% try{
	
	Policybean pbean = new Policybean();
	pbean.setSwitchnum(request.getParameter("switchnum"));
	pbean.setInter(request.getParameter("inter"));
	pbean.setSourceip(request.getParameter("sourceip"));
	pbean.setDestip(request.getParameter("destip"));
	pbean.setProtocol(request.getParameter("protocol"));
	pbean.setPriority(request.getParameter("priority"));
	pbean.setAction(request.getParameter("action"));
	pbean.setPort(request.getParameter("port"));
	String gotopage = request.getParameter("srcpageaddp");
	if(gotopage.equalsIgnoreCase("forpnt")){
	PolicyBase.addvalue(pbean);
	System.out.println("original file");
	}
	if(gotopage.equalsIgnoreCase("forsnt")){
		PolicyBaseSNT.addvalue(pbean);
	}
	HashMap<String,String> hmp = new HashMap<String,String>();
	hmp.put("SWITCH DPID", request.getParameter("switchnum"));
	hmp.put("IN PORT", request.getParameter("inter"));
	hmp.put("ACTION", request.getParameter("action"));
	hmp.put("Source IP", request.getParameter("sourceip"));
	hmp.put("Destination IP", request.getParameter("destip"));
	hmp.put("Protocol", request.getParameter("protocol"));
	hmp.put("tp_destination", request.getParameter("port"));

	excelWriter.writeDict(hmp);
	
}
catch(Exception e)
{
	e.printStackTrace();
}

%>
<%
String redirectnow = request.getParameter("srcpageaddp");
if(redirectnow.equalsIgnoreCase("forpnt")){
	 String redirectURL = "PolicyBase.jsp";
	    response.sendRedirect(redirectURL);
	}
	if(redirectnow.equalsIgnoreCase("forsnt")){
		 String redirectURL = "PolicyBaseSNT.jsp";
		    response.sendRedirect(redirectURL);
	}

%>
</body>
</html>