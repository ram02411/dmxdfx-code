<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="bean.*"%>
<%@ page language="java" import="dto.*"%>
<%@ page import="java.util.*"%>

<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<%
	List<List<String>> alert = new ArrayList<List<String>>();
	List<String> switches = new ArrayList<String>();
	try {

		String check = setsessionvar.getSessioname();
		if (check.length() > 0 || !(check.equals(""))) {
			//alert = alertPicker.getAlerts(); 

		} else {
			String redirectURL = "invalid.jsp";
			//response.sendRedirect(redirectURL);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Evolve Panel" />
<meta name="author" content="" />
<meta content="text/html; charset=ASMO-708" http-equiv="content-type" />

<title>Evolve | Tags</title>
<script src="mcColorPicker/mcColorPicker.js"></script>
<link rel="stylesheet" href="mcColorPicker/mcColorPicker.css">
<link rel="stylesheet"
	href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet"
	href="assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/neon-core.css">
<link rel="stylesheet" href="assets/css/neon-theme.css">
<link rel="stylesheet" href="assets/css/neon-forms.css">
<link rel="stylesheet" href="assets/css/custom.css">
<link href="assets/js/daterangepicker/daterangepicker-bs3.css"
	rel="stylesheet" />
<script src="assets/js/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="assets/js/select2/select2.css">
<link rel="stylesheet"
	href="assets/js/selectboxit/jquery.selectBoxIt.css">

<style>
th, td {
	border: 1px solid;
	padding: .2em .7em;
}


.color {
    width: 80%;
    height: 27px;
    padding: 6px;
    font-size: 12px;
    line-height: 1.42857143;
    color: #555555;
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #ebebeb;
    border-radius: 3px;
    }
</style>
   <script>
    
 
function OnColorChanged(selectedColor, colorPickerIndex) {
    var divA = document.getElementById("divA");
    var divB = document.getElementById("divB");
    if (colorPickerIndex == 0)
        divA.style.backgroundColor = selectedColor;
}
$(document).ready(function () {
	var data="<%=Leftmenuhtml.getHtmldata() %>";
	 
	document.getElementById("menuleft").innerHTML=data;
});
	var status = <%=Statusbean.getTags()%>
	function deletetag() {
		if (status == 2) {
			jQuery('#modal-4').modal('hide');
		} else {
			jQuery('#modal-4').modal('show');
		}
	}
	function addtag() {
		if (status == 2) {
			jQuery('#modal-3').modal('hide');
		} else {
			jQuery('#modal-3').modal('show');
		}
	}

	function clonetag() {
		var tgname = document.getElementById("tagnamedelete").value;

		if (status == 2) {
		} else {

			$.ajax({
				url : 'CloneTag.jsp',
				dataType : 'text',
				data : {
					tagname : tgname
				},
				success : function(data) {

				}
			});
		}

		setTimeout(function() {
			window.location.reload();
		}, 10);

	}

	function edittag(tagedit) {

		$('#tagnameedit').val(tagedit);
		if (status == 2) {
		} else {
			$.ajax({
				url : 'gettags.jsp',
				dataType : 'text',
				data : {
					etagsname : tagedit
				},
				success : function(data) {
					tag = data.split('&');

					$('#etname').val(tag[0]);
					$('#ecolor1').val(tag[1]);
					$('#ecomment1').val(tag[2]);
				}
			});

			jQuery('#modal-6').modal('show');
		}
	}

	function deletingrole() {

		var tagnamed = document.getElementById("tagnamedelete").value;
		$.ajax({
			url : 'DeleteTag.jsp',
			dataType : 'text',
			data : {
				tagname : tagnamed
			},
			success : function(data) {

			}
		});

		setTimeout(function() {
			window.location.reload();
		}, 15);
	}
</script>
</head>


<body class="page-body" data-url="http://ecodenetworks.com">

	<div class="page-container">

		<div class="sidebar-menu">


			<header class="logo-env">

				<!-- logo -->
				<div class="sui-normal logo ">
					<a href="#" class="user-link" style="margin-top: -5px;"> <img
						src="assets/images/Admin.png" alt="" class="img-circle"
						style="width: 57px; height: 53px;" /> <span
						style="margin-left: 8px">Welcome<strong>Admin</strong></span>

					</a>
				</div>

				<!-- logo collapse icon -->

				<div class="sidebar-collapse">
					<a href="#" class="sidebar-collapse-icon with-animation"> <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
						<i class="entypo-menu"></i>
					</a>
				</div>



				<!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
				<div class="sidebar-mobile-menu visible-xs">
					<a href="#" class="with-animation"> <!-- add class "with-animation" to support animation -->
						<i class="entypo-menu"></i>
					</a>
				</div>

			</header>

			<div id="menuleft"></div>


		</div>

		<div class="main-content">

			<div class="row">

				<!-- Profile Info and Notifications -->
				<div class="col-md-6 col-sm-8 clearfix"></div>


				<!-- Raw Links -->
				<div class="col-md-6 col-sm-4 clearfix hidden-xs">

					<ul class="list-inline links-list pull-right">
						<li><a><strong>Ecode Networks</strong> <i
								class="entypo-users"></i></a></li>
						<li class="sep"></li>
						<li><a href="#"
							onclick="jQuery('#modal-2').modal('show', {backdrop: 'static'});">
								<strong>Commit</strong> <i class="entypo-download"></i>
						</a></li>
						<li class="sep"></li>
						<li><a href="#"
							onclick="jQuery('#modal-1').modal('show', {backdrop: 'static'});">
								<strong>Save</strong> <i class="entypo-floppy"></i>
						</a></li>

						<li class="sep"></li>


						<li><a href="setout.jsp"> <strong>LogOut</strong> <i
								class="entypo-logout right"></i></a></li>
					</ul>
				</div>

			</div>

			<hr />
			<nav class="navbar navbar-inverse" role="navigation">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#bs-example-navbar-collapse-2">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"></a>
				</div>

				<div class="collapse navbar-collapse"
					id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav">
						<li class="active"><a href="#" onclick="addtag();"><i
								class="entypo-plus-squared"></i><span>Add</span></a></li>
						<li><a href="#" onclick="deletetag();"><i
								class="entypo-minus-squared"></i><span>Delete</span></a></li>
						<li><a href="#" onClick="clonetag();"><i
								class="entypo-docs"></i><span>Clone</span></a></li>

					</ul>

				</div>

			</nav>
			<div class="row">

				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="panel-title">Administrators</div>

						<div class="panel-options">
							<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
							<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
						</div>
					</div>

					<table
						class="table table-bordered responsive datatable"
						id="myTableData">
						<thead>
							<tr>
								<th  width="1%"></th>
								<th>Name</th>
								<th>Location</th>
								<th>Colour</th>
								<th>Comments</th>
							</tr>
						</thead>
						<tbody>
							<%
								int i = 0;
							%>
							<%
								List<Tagbean> bean = UserTags.getalltags();
							%>
							<%
								for (; i < bean.size(); i++) {
							%>
							<tr>
								<td><input type="checkbox" name="tagslist" value="<%=bean.get(i).getName()%>"
									onClick="checkname('<%=bean.get(i).getName()%>');" /></td>
								<td><a href="#" style="padding:4px; border-radius:3px; background-color:<%=bean.get(i).getColor()%>"
									onClick="edittag('<%=bean.get(i).getName()%>');"
									class="blue_popup"><%=bean.get(i).getName()%></a></td>
								<td></td>
								<td><div><p Style="width:19px; height: 19px; background-color:<%=bean.get(i).getColor()%>;float:left"></p><p style="margin-left:20px"><%=bean.get(i).getColor()%></p></div></td>
								<td><%=bean.get(i).getComments()%></td>

							</tr>
							<%
								}
							%>


						</tbody>
					</table>

				</div>


			</div>

			<footer class="main">
				Copyright &copy; 2017 <strong>Ecode Networks,</strong>All Right Reserved
			</footer>
		</div>


	</div>


	<script type="text/javascript">
		function checkname(toname) {
			
			$('#tagnamedelete').val(toname);
			var checkboxValues=[];
			$('input[name=tagslist]:checked').map(function() {
	            checkboxValues.push($(this).val());
	            $.ajax({
	    			url : 'SetTaglist.jsp',
	    			data : {
	    				tagname : checkboxValues
	    			},
	    			success : function(data) {

	    			}
	    		});
	            $('#tagnamed').text(checkboxValues);
	});
			//alert(checkboxValues);
		}
	</script>


	<div class="modal fade" id="modal-3">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<strong>Add Tag</strong>
					</h4>
				</div>

				<div class="modal-body">
					<form role="form" action="Createtag.jsp" method="POST">
						<div class="row" style="margin-top: 5px">
							<div class="form-group">
								<label for="field-4" class="col-md-3 control-label">Name</label>

								<div class="col-md-9">
									<input type="text" class="form-control" id="tname"
										name="tagname">
								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 5px">
							<div class="form-group" id="drop1">
								<label for="field-7" class="col-md-3 control-label">Color</label>

							<div class="col-md-9">
									 <input type="text" class="color " value="#000000" id="color1" name="tagcolor"/><br />
								</div>
							</div>
						</div>
						 
						<div class="row" style="margin-top: 5px">
							<div class="form-group">
								<label for="field-4" class="col-md-3 control-label">Comment</label>

								<div class="col-md-9">
									<input type="text" class="form-control" id="comment1"
										name="Tagcomment">
								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 5px">
							<div class="form-group"
								style="text-align: right; margin-right: 10px">
								<button type="submit" class="btn btn-info" id="add" value="Add">Ok</button>
								<button type="button" class="btn btn-danger"
									data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-6">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<strong>Edit Tags</strong>
					</h4>
				</div>

				<div class="modal-body">
					<form role="form" action="Updatetag.jsp" method="POST">
						<div class="row" style="margin-top: 5px">
							<div class="form-group">
								<label for="field-4" class="col-md-3 control-label">Name</label>

								<div class="col-md-9">
									<input type="text" class="form-control" id="etname"
										name="etagname"> <input type="hidden" id="tagnameedit"
										name="tagnameedit" />
								</div>
							</div>
						</div>
						
						<div class="row" style="margin-top: 5px">
							<div class="form-group" id="drop1">
								<label for="field-7" class="col-md-3 control-label">Color</label>

							<div class="col-md-9">
									 <input type="text" class="color " value="ffffff" id="ecolor1" name="etagcolor"/><br />
								</div>
							</div>
						</div>
						 
						
						<div class="row" style="margin-top: 5px">
							<div class="form-group">
								<label for="field-4" class="col-md-3 control-label">Comment</label>

								<div class="col-md-9">
									<input type="text" class="form-control" id="ecomment1"
										name="eTagcomment">
								</div>
							</div>
						</div>
						<div class="row" style="margin-top: 5px">
							<div class="form-group"
								style="text-align: right; margin-right: 10px">
								<button type="submit" class="btn btn-info" id="add" value="Add">Ok</button>
								<button type="button" class="btn btn-danger"
									data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="modal-4">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">
						<strong>Delete Tag</strong>
					</h4>
				</div>

				<div class="modal-body">
					<div class="row">
						<div class="col-md-2"></div>
						<div class="col-md-8">

							<div class="form-group">
								<label for="sel1"><strong>
								Do you really want to delete tag : <span
									id="tagnamed"></span> <input type="hidden" id="tagnamedelete"
									name="tagnamedelete" /></strong></label>


							</div>

						</div>
						<div class="col-md-2"></div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-info"
						onClick="deletingrole();">Ok</button>

				</div>
			</div>
		</div>
	</div>



	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="assets/js/typeahead.min.js"></script>
	<script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="assets/js/jquery.multi-select.js"></script>
	<script src="assets/js/icheck/icheck.min.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>
