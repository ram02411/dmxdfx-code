<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="bean.*"%>
<%@ page language="java" import="dto.*"%>
<%@ page import="java.util.*"%>	
	
<%
	
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<%List<List<String>> alert =new ArrayList<List<String>>();
List<String> switches=new ArrayList<String>();
 try{

  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	//alert = alertPicker.getAlerts(); 
	
}
else{
 String redirectURL = "invalid.jsp";
 //response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>
			
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Evolve Panel" />
    <meta name="author" content="" />
    <meta content="text/html; charset=ASMO-708" http-equiv="content-type" />

    <title>Evolve | Security Rulebase</title>


    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link href="assets/js/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    
 
    <style>
      th, td { border: 1px solid; padding: .2em .7em; }
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
            background-color: #D4E0E0;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #E5EEEE;
        }
     
    </style>
</head>


<body class="page-body" data-url="http://ecodenetworks.com">

    <div class="page-container">
      
        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                   <a href="#" class="user-link" style="margin-top:-5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width:57px;height:53px;"/>
                        <span style="margin-left:8px">Welcome<strong>Admin</strong></span>
                       
                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>

            <div id="menuleft"></div> 

        </div>
        <script type="text/javascript">
        $(document).ready(function () {
        	var data="<%=Leftmenuhtml.getHtmldata() %>";
        	 
        	document.getElementById("menuleft").innerHTML=data;
        });
        </script>

        <div class="main-content">

            <div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    
                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <a><strong>Ecode Networks</strong> <i class="entypo-users"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-2').modal('show', {backdrop: 'static'});">
                            <strong>Commit</strong> <i class="entypo-download"></i></a>
                        </li>
                         <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-1').modal('show', {backdrop: 'static'});">
                            <strong>Save</strong> <i class="entypo-floppy"></i></a>
                        </li>

                        <li class="sep"></li>
                        

                        <li>
                            <a href="setout.jsp">
                                <strong>LogOut</strong>  <i class="entypo-logout right"></i></a>
                        </li>
                    </ul>
                </div>

            </div>

            <hr />
            <nav class="navbar navbar-inverse" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#"></a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#" onclick="addrule();"><i class="entypo-plus-squared"></i><span>Add</span></a>
			</li>
			<li><a href="#" onclick="deleterule();"><i class="entypo-minus-squared"></i><span>Delete</span></a>
			</li>
			<li><a href="#" onClick="clonerule();"><i class="entypo-docs"></i><span>Clone</span></a>
			</li>
			
		</ul>
		
	</div>

</nav>
            <div class="row">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Administrators</div>

                        <div class="panel-options">
                           <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>
                    
                    
           <table class="table table-bordered responsive table-hover table-striped datatable" id="tableId">
                        <thead>
                            <tr>
                              	<th>&nbsp;</th>
                                <th>Name</th>
                                <th>Tags</th>
                                <th>Type</th>
                                <th>Zone</th>
                                <th>Address</th>
                                <th>User</th>
                                <th>HIP Profile</th>
                                <th>User</th>
                                <th>HIP Profile</th>
                                <th>Application</th>
                                <th>Service</th>
                                <th>Action</th>
                                <th>Profile</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                       <tbody>
      <%
									int i=0;
								%>
								<%
									List<Securityrule> bean=Securityrulebase.getallrules();
								%>
								<%
									for(;i<bean.size();i++){
								%>
                       			<tr>
									<td><input type="checkbox"  onClick="checkname('<%=bean.get(i).getName()%>');"/></td>
									<td><a href="#" onClick="editrule('<%=bean.get(i).getName()%>');" class="blue_popup"><%=bean.get(i).getName()%></a></td>
									<td><%=bean.get(i).getTags()%></td>
									<td><%=bean.get(i).getType()%></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
                       			<%
									}
								%>
      
    </tbody>
                    </table>
<script type="text/javascript">
function checkname(rname){
	$("#rulenamed").text(rname);
	$('#rulenamedelete').val(rname);
	
}
var statusr=<%=Statusbean.getSecurityrulebase()%>
var tagsname='<%=UserTags.gettagnames()%>';
alert(tagsname);

function addrule(){
	
	
	if(statusr==2){            	
	   	jQuery('#modal-3').modal('hide');
	   	}
	else{
		
	   		jQuery('#modal-3').modal('show');
	   	}
}
function deleterule(){
	if(statusr==2){
		jQuery('#modal-4').modal('hide');
	}
	else{
		jQuery('#modal-4').modal('show');
	}
	
}
function clonerule(){
	var runame=document.getElementById("rulenamedelete").value;
	
	if(statusr==2){
		
	}
	else{
		
		$.ajax({ 
    		url: 'clonerule.jsp',
    		dataType: 'text',
    		data: {rulename:runame},
    		success: function(data) {
    			
    		}
        });
	}
	setTimeout(function () { window.location.reload(); }, 10);
}

function editrule(name){
if(statusr==2){
		
	}
	else{
		$('#editsr').val(name);
		$.ajax({ 
    		url: 'getrule.jsp',
    		dataType: 'text',
    		data: {rulename:name},
    		success: function(data) {
    			rule=data.split('&');
    			
    			$('#esrname').val(rule[0]);
    			$('#eruletype').val(rule[1]);
    			$('#eruledesc').val(rule[2]);
    			$('#ertags').val(rule[3]);
    			
    			
    			
    		}
        });
		jQuery('#modal-5').modal('show');
	}
}

</script>
                   
                </div>

                
            </div>
          
         <footer class="main">Copyright &copy; 2016 <strong>Ecode Networks,</strong>All Rights Reservered
             </footer>
        </div>


    </div>


    <div class="modal fade" id="modal-2">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Commit</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Doing a commit will overwrite the running configuration. Do you want to continue?</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
                <button type="button" class="btn btn-info">Preview Changes</button>
                <button type="button" class="btn btn-info">Validate Changes</button>
                <button type="button" class="btn btn-info">Commit</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
              
			</div>
		</div>
	</div>
        
</div>

    <div class="modal fade" id="modal-1">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Save Configurations</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Saving this configuration will overwrite the previously saved configuration.
                              Do you want to continue?</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-info">Ok</button>
              
			</div>
		</div>
	</div>
</div>
    
<div class="modal fade" id="modal-4">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Delete SecurityRulebase</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Do you really want to delete rule : <span id="rulenamed"></span> 
							 <input type="hidden" id="rulenamedelete" name="rulenamedelete"/></label>
							 
                             
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-info" onClick="deletingrule();">Ok</button>
              
			</div>
		</div>
	</div>
</div>  
<script type="text/javascript">
    function deletingrule(){	
    	
    	
    		
    		var rulenamed=document.getElementById("rulenamedelete").value;
    		$.ajax({ 
        		url: 'deletesecrule.jsp',
        		dataType: 'text',
        		data: {rulename:rulenamed},
        		success: function(data) {
        			
        		}
            });
    	
    	
    	setTimeout(function () { window.location.reload(); }, 10);
    }
    </script>  
    
    <div class="modal fade custom-width" id="modal-5">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Security Policy Rule</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                
					<div class="col-md-12">
						
						<div class="form-group">
		<ul class="nav nav-tabs bordered primary">
			<li class="active">
				<a href="#eGeneral" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			<li>
				<a href="#esource" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">Source</span>
				</a> 
			</li>
			<li>
				<a href="#euser" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">User</span>
				</a>
			</li>
			<li>
				<a href="#edestination" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-cog"></i></span>
					<span class="hidden-xs">Destination</span>
				</a>
			</li>
          <li>
				<a href="#eApplication" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">Application</span>
				</a>
			</li>
			<li>
				<a href="#eurl" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">Service/ URL Category</span>
				</a>
			</li>
			<li>
				<a href="#eAction" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-cog"></i></span>
					<span class="hidden-xs">Action</span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="eGeneral">
				<form role="form" action="Updaterule.jsp" method="POST">
                 <div class="row" style="margin-top:5px"> <div class="form-group">
						<label for="field-1" class="col-md-2 control-label">Name</label>
						
						<div class="col-md-10">
							<input type="text" class="form-control" id="esrname" name="esrname" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
							<input type="hidden" id="editsr" name="editsr"/>
						</div>
					</div></div> 
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-2" class="col-md-2 control-label">Rule Type</label>
						
						<div class="col-md-10">
							<input type="text" class="form-control" id="eruletype" name="eruletype" placeholder="Enter RuleType">
						</div>
					</div></div>
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-3" class="col-md-2 control-label">Description</label>
						
						<div class="col-md-10">
							<textarea class="form-control" id="eruledesc" name="eruledesc" placeholder="Enter Description"></textarea>
						</div>
					</div></div>
                  <div class="row" style="margin-top:5px">   <div class="form-group">
						<label for="field-4" class="col-md-2 control-label">Tags</label>
						
						<div class="col-md-10">
							<input type="text" class="form-control" id="ertags" name="ertags">
						</div>
					</div></div>
                  <div class="row" style="margin-top:5px; text-align:right; margin-right:5px"><div class="form-group">
				<button type="submit" class="btn btn-success">Validate</button>
				<button type="reset" class="btn">Reset</button>
			</div></div>
                 </form>
				
			</div>
            <script>
                function addRow(tableID) {

                    var table = document.getElementById(tableID);

                    var rowCount = table.rows.length;
                    var row = table.insertRow(rowCount);

                    var colCount = table.rows[0].cells.length;

                    for (var i = 0; i < colCount; i++) {

                        var newcell = row.insertCell(i);

                        newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                        //alert(newcell.childNodes);
                        switch (newcell.childNodes[0].type) {
                            case "text":
                                newcell.childNodes[0].value = "";
                                break;
                            case "checkbox":
                                newcell.childNodes[0].checked = false;
                                break;
                            case "select-one":
                                newcell.childNodes[0].selectedIndex = 0;
                                break;
                        }
                    }
                }

                function deleteRow(tableID) {
                    try {
                        var table = document.getElementById(tableID);
                        var rowCount = table.rows.length;

                        for (var i = 0; i < rowCount; i++) {
                            var row = table.rows[i];
                            var chkbox = row.cells[0].childNodes[0];
                            if (null != chkbox && true == chkbox.checked) {
                                if (rowCount <= 1) {
                                    alert("Cannot delete all the rows.");
                                    break;
                                }
                                table.deleteRow(i);
                                rowCount--;
                                i--;
                            }


                        }
                    } catch (e) {
                        alert(e);
                    }
                }

	</script>
			<div class="tab-pane" id="esource">
				<div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Source Zone</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="dataTable" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('dataTable')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('dataTable')" />

                        </div>

                         </div>
               
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black"">Source Address</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE1" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE1')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE1')" />

                        </div>

                         </div>
				</div>
			</div>
			<div class="tab-pane" id="euser">
			
                <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>pre-logged</option>
								<option>known-user</option>
								<option>unknown</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">HIP Profile</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE2" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE2')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE2')" />

                        </div>

                         </div>
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>no-hip</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">HIP Profile</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE3" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE3')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE3')" />

                        </div>

                         </div>     
				</div>
                		
			</div>
			<div class="tab-pane" id="edestination">
				  <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>Multicast</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Destination Zone</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE4" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE4')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE4')" />

                        </div>

                         </div>	

                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Destination Addresss</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE5" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                      <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE5')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE5')" />

                        </div>
                   </div>    

                    </div>
					</div>
            <div class="tab-pane" id="eApplication">
	         <div class="row">
                	<div class="col-md-12">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Destination Addresss</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE6" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                      <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE6')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE6')" />

                        </div>
                   </div>
        </div>	
            </div>
			<div class="tab-pane" id="eurl">
				
                <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>application-default</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Services</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE7" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE7')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE7')" />

                        </div>

                         </div>
                    <div class="col-md-6">
                        <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">URL Category</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE8" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE8')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE8')" />

                        </div>

                         </div>     
				</div>
                		</div>
			<div class="tab-pane" id="eAction">
			<div class="row">
                	
	<div class="col-md-6">
		
		<div class="panel panel-success" data-collapsed="0">
			
			<!-- panel head -->
			<div class="panel-heading">
				<div class="panel-title">Action Settings</div>
				
				<div class="panel-options">
					<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
					<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
				</div>
			</div>
			
			<!-- panel body -->
			<div class="panel-body">
			<div class="row">
	        <div class="col-md-12" style="margin-top:5px"> 
                <div class="form-group">
				<label for="field-2" class="col-md-2 control-label">Action</label>
					<div class="col-md-10">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">Allow</option>
								<option value="2">Deny</option>
                                <option value="3">Drop</option>
								<option value="4">Reset Client</option>
                                <option value="5">Reset Server</option>
								<option value="6">Reset both client and server</option>
                                
							</select>
						</div>
					</div>	</div>
	</div>
			</div>
			
		</div>
		
	</div>
    <div class="col-md-6">
		
		<div class="panel panel-success" data-collapsed="0">
			
			<!-- panel head -->
			<div class="panel-heading">
				<div class="panel-title"><strong>Profile Settings</strong></div>
				
				<div class="panel-options">
					<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
					<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
				</div>
			</div>
			
			<!-- panel body -->
			<div class="panel-body">
			<div class="row">
	        <div class="col-md-12" style="margin-top:5px"> 
                <div class="form-group">
				<label for="field-2" class="col-md-3 control-label">Profile Type</label>
					<div class="col-md-9">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">Profile</option>
								<option value="2">Group</option>
							</select>
						</div>
					</div>	</div>
	</div>
			</div>
			
		</div>
		
	</div>

			</div>
  
        
		
		
						</div>	
						
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
        </div>
    </div>
    </div
    >
    

    <div class="modal fade custom-width" id="modal-3">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Security Policy Rule</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                
					<div class="col-md-12">
						
						<div class="form-group">
		<ul class="nav nav-tabs bordered primary">
			<li class="active">
				<a href="#General" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">General</span>
				</a>
			</li>
			<li>
				<a href="#source" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">Source</span>
				</a> 
			</li>
			<li>
				<a href="#user" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">User</span>
				</a>
			</li>
			<li>
				<a href="#destination" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-cog"></i></span>
					<span class="hidden-xs">Destination</span>
				</a>
			</li>
          <li>
				<a href="#Application" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">Application</span>
				</a>
			</li>
			<li>
				<a href="#url" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">Service/ URL Category</span>
				</a>
			</li>
			<li>
				<a href="#Action" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-cog"></i></span>
					<span class="hidden-xs">Action</span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="General">
				<form role="form" action="Createrule.jsp" method="POST">
                 <div class="row" style="margin-top:5px"> <div class="form-group">
						<label for="field-1" class="col-md-2 control-label">Name</label>
						
						<div class="col-md-10">
							<input type="text" class="form-control" id="field-1" name="srname" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
						</div>
					</div></div> 
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-2" class="col-md-2 control-label">Rule Type</label>
						
						<div class="col-md-10">
							<input type="text" class="form-control" id="field-2" name="ruletype" placeholder="Enter RuleType">
						</div>
					</div></div>
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-3" class="col-md-2 control-label">Description</label>
						
						<div class="col-md-10">
							<textarea class="form-control" id="field-ta" name="ruledesc" placeholder="Enter Description"></textarea>
						</div>
					</div></div>
                  <div class="row" style="margin-top:5px">   <div class="form-group">
						<label for="field-4" class="col-md-2 control-label">Tags</label>
						
						<div class="col-md-10">
							<input type="text" class="form-control" id="field-4" name="rtags">
						</div>
					</div></div>
                  <div class="row" style="margin-top:5px; text-align:right; margin-right:5px"><div class="form-group">
				<button type="submit" class="btn btn-success">Validate</button>
				<button type="reset" class="btn">Reset</button>
			</div></div>
                 </form>
				
			</div>
            <script>
                function addRow(tableID) {

                    var table = document.getElementById(tableID);

                    var rowCount = table.rows.length;
                    var row = table.insertRow(rowCount);

                    var colCount = table.rows[0].cells.length;

                    for (var i = 0; i < colCount; i++) {

                        var newcell = row.insertCell(i);

                        newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                        //alert(newcell.childNodes);
                        switch (newcell.childNodes[0].type) {
                            case "text":
                                newcell.childNodes[0].value = "";
                                break;
                            case "checkbox":
                                newcell.childNodes[0].checked = false;
                                break;
                            case "select-one":
                                newcell.childNodes[0].selectedIndex = 0;
                                break;
                        }
                    }
                }

                function deleteRow(tableID) {
                    try {
                        var table = document.getElementById(tableID);
                        var rowCount = table.rows.length;

                        for (var i = 0; i < rowCount; i++) {
                            var row = table.rows[i];
                            var chkbox = row.cells[0].childNodes[0];
                            if (null != chkbox && true == chkbox.checked) {
                                if (rowCount <= 1) {
                                    alert("Cannot delete all the rows.");
                                    break;
                                }
                                table.deleteRow(i);
                                rowCount--;
                                i--;
                            }


                        }
                    } catch (e) {
                        alert(e);
                    }
                }

	</script>
			<div class="tab-pane" id="source">
				<div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Source Zone</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="dataTable" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('dataTable')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('dataTable')" />

                        </div>

                         </div>
               
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black"">Source Address</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE1" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE1')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE1')" />

                        </div>

                         </div>
				</div>
			</div>
			<div class="tab-pane" id="user">
			
                <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>pre-logged</option>
								<option>known-user</option>
								<option>unknown</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">HIP Profile</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE2" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE2')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE2')" />

                        </div>

                         </div>
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>no-hip</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">HIP Profile</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE3" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE3')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE3')" />

                        </div>

                         </div>     
				</div>
                		
			</div>
			<div class="tab-pane" id="destination">
				  <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>Multicast</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Destination Zone</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE4" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE4')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE4')" />

                        </div>

                         </div>	

                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Destination Addresss</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE5" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                      <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE5')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE5')" />

                        </div>
                   </div>    

                    </div>
					</div>
            <div class="tab-pane" id="Application">
	         <div class="row">
                	<div class="col-md-12">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Destination Addresss</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE6" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                      <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE6')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE6')" />

                        </div>
                   </div>
        </div>	
            </div>
			<div class="tab-pane" id="url">
				
                <div class="row">
                    <div class="col-md-6">
                      <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;">
                                <select class="dropdown" style="width:40%">
								<option>any</option>
								<option>application-default</option>
								<option>select</option>
							</select></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Services</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE7" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE7')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE7')" />

                        </div>

                         </div>
                    <div class="col-md-6">
                        <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">ANY</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> <TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">URL Category</label></TD>
                          </TR></tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE8" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add Row" onclick="addRow('TABLE8')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete Row" onclick="deleteRow('TABLE8')" />

                        </div>

                         </div>     
				</div>
                		</div>
			<div class="tab-pane" id="Action">
			<div class="row">
                	
	<div class="col-md-6">
		
		<div class="panel panel-success" data-collapsed="0">
			
			<!-- panel head -->
			<div class="panel-heading">
				<div class="panel-title">Action Settings</div>
				
				<div class="panel-options">
					<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
					<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
				</div>
			</div>
			
			<!-- panel body -->
			<div class="panel-body">
			<div class="row">
	        <div class="col-md-12" style="margin-top:5px"> 
                <div class="form-group">
				<label for="field-2" class="col-md-2 control-label">Action</label>
					<div class="col-md-10">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">Allow</option>
								<option value="2">Deny</option>
                                <option value="3">Drop</option>
								<option value="4">Reset Client</option>
                                <option value="5">Reset Server</option>
								<option value="6">Reset both client and server</option>
                                
							</select>
						</div>
					</div>	</div>
	</div>
			</div>
			
		</div>
		
	</div>
    <div class="col-md-6">
		
		<div class="panel panel-success" data-collapsed="0">
			
			<!-- panel head -->
			<div class="panel-heading">
				<div class="panel-title"><strong>Profile Settings</strong></div>
				
				<div class="panel-options">
					<a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i class="entypo-cog"></i></a>
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
					<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
					<a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
				</div>
			</div>
			
			<!-- panel body -->
			<div class="panel-body">
			<div class="row">
	        <div class="col-md-12" style="margin-top:5px"> 
                <div class="form-group">
				<label for="field-2" class="col-md-3 control-label">Profile Type</label>
					<div class="col-md-9">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">Profile</option>
								<option value="2">Group</option>
							</select>
						</div>
					</div>	</div>
	</div>
			</div>
			
		</div>
		
	</div>

			</div>
  
        
		
		
						</div>	
						
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
        </div>
    </div>
    </div
    >
    
     <link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/selectboxit/jquery.selectBoxIt.css">
    <script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="assets/js/typeahead.min.js"></script>
	<script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="assets/js/bootstrap-datepicker.js"></script>
	<script src="assets/js/bootstrap-timepicker.min.js"></script>
	<script src="assets/js/bootstrap-colorpicker.min.js"></script>
	<script src="assets/js/daterangepicker/moment.min.js"></script>
	<script src="assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="assets/js/jquery.multi-select.js"></script>
	<script src="assets/js/icheck/icheck.min.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>
    
</body>
</html>
	
	