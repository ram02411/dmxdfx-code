<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="bean.*"%>
<%@ page language="java" import="dto.*"%>
<%@ page import="java.util.*"%>	
<%
	
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<%List<List<String>> alert =new ArrayList<List<String>>();
List<String> switches=new ArrayList<String>();
 try{

  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	//alert = alertPicker.getAlerts(); 
	
}
else{
 String redirectURL = "invalid.jsp";
 //response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>
	

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Evolve Panel" />
    <meta name="author" content="" />

    <title>Evolve | Administrators</title>
    
	<link rel="stylesheet" href="assets/css/font-icons/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    <link href="assets/js/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <style>
        .rotate {
    transform:rotate(180deg);
    transition:all 0.5s;
}
.rotate.in {
    transform:rotate(1800deg);
    transition:all 1.5s;
}

    </style>

    <script>
        function addRow(tableID) {

            var table = document.getElementById(tableID);

            var rowCount = table.rows.length;
            var row = table.insertRow(rowCount);

            var colCount = table.rows[0].cells.length;

            for (var i = 0; i < colCount; i++) {

                var newcell = row.insertCell(i);

                newcell.innerHTML = table.rows[0].cells[i].innerHTML;
                //alert(newcell.childNodes);
                switch (newcell.childNodes[0].type) {
                    case "text":
                        newcell.childNodes[0].value = "";
                        break;
                    case "checkbox":
                        newcell.childNodes[0].checked = false;
                        break;
                    case "select-one":
                        newcell.childNodes[0].selectedIndex = 0;
                        break;
                }
            }
        }

        function deleteRow(tableID) {
            try {
                var table = document.getElementById(tableID);
                var rowCount = table.rows.length;

                for (var i = 0; i < rowCount; i++) {
                    var row = table.rows[i];
                    var chkbox = row.cells[0].childNodes[0];
                    if (null != chkbox && true == chkbox.checked) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows.");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                    }


                }
            } catch (e) {
                alert(e);
            }
        }

	</script>

    <script>
    var status;
    var pagename;
        $(document).ready(function () {

            $('#openBtn').click(function () {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on('show.bs.modal', '.modal', function (event) {
                var zIndex = 1040 + (10 * $('.modal:visible').length);
                $(this).css('z-index', zIndex);
                setTimeout(function () {
                    $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                }, 0);

            });
            
            $('input[type="radio"]').click(function () {
                if ($(this).attr("value") == "Dynamic") {
                    $("#drop1").show();
                    $("#drop2").hide();
                }

                else {
                    $("#drop2").show();
                    $("#drop1").hide();
                }
                if ($(this).attr("value") == "EDynamic") {
                    $("#drop3").show();
                    $("#drop4").hide();
                }

                else { 
                	
                    $("#drop4").show();
                    $("#drop3").hide();
                }
                
            });
            $('#check1').click(function () {
                if ($(this).is(':checked')) {
                    $("#importhide").show();
                }
                else {
                    $("#importhide").hide();
                }
            });
            $('.menu-toggle i').on('click', function () {
              	 pagename=this.id;
              });
            
            $('.menu-toggle').on('click', function () {
                var iSelector = $(this).find('i:first');
                if (iSelector.hasClass('fa-check-circle-o')) {
                    iSelector.removeClass('fa-check-circle-o')
                    iSelector.addClass('fa-times-circle-o')
                    status="disable";
                }
                else if (iSelector.hasClass('fa-times-circle-o')) {
                    iSelector.removeClass('fa-times-circle-o')
                    iSelector.addClass('fa-lock')
                    status="readonly";
                }
                else {
                    iSelector.removeClass('fa-lock')
                    iSelector.addClass('fa-check-circle-o')
                }
                
                $.ajax({ 
            		url: 'settingstatus.jsp',
            		dataType: 'text',
            		data: {page:pagename, pagestatus:status },
            		success: function(data) {
            			
            		}
                });
            });

        });
        function myFunction() {
            var option_value = document.getElementById("numbers").value;
            if (option_value == "1") {
                $('#adminModal').modal({
                    show: true
                });
            }
        }
        function pass1() {
            var a = document.getElementById("passProfile").value;
            if (a == "1") {
                $('#passModal').modal({
                    show: true
                });
            }
        }
        function adminChange() {
            var b = document.getElementById("adminChnge").value;
            if (b == "1") {
                $('#adminProfile').modal({
                    show: true
                });
            }
        }
        
        function eadminChange() {
            var b = document.getElementById("erolename").value;
            if (b == "1") {
                $('#adminProfile').modal({
                    show: true
                });
            }
        }

    </script>

    <style>
      th, td { border: 1px solid; padding: .2em .7em; }
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
            background-color: #D4E0E0;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #E5EEEE;
        }
     
    </style>
</head>

<script type="text/javascript">
$(document).ready(function () {
	var data="<%=Leftmenuhtml.getHtmldata() %>";
	 
	document.getElementById("menuleft").innerHTML=data;
});
</script>
<body class="page-body" data-url="http://ecodenetworks.com">

    <div class="page-container">
      
        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                   <a href="#" class="user-link" style="margin-top:-5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width:57px;height:53px;"/>
                        <span style="margin-left:8px">Welcome<strong>Admin</strong></span>
                       
                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>

            
<div id="menuleft"></div>  

        </div>

        <div class="main-content">

            <div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    
                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <a><strong>Ecode Networks</strong> <i class="entypo-users"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-2').modal('show', {backdrop: 'static'});">
                            <strong>Commit</strong> <i class="entypo-download"></i></a>
                        </li>
                         <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-1').modal('show', {backdrop: 'static'});">
                            <strong>Save</strong> <i class="entypo-floppy"></i></a>
                        </li>

                        <li class="sep"></li>
                        

                        <li>
                            <a href="setout.jsp">
                                <strong>LogOut</strong>  <i class="entypo-logout right"></i></a>
                        </li>
                    </ul>
                </div>

            </div>

            <hr />

            <div class="row">
<nav class="navbar navbar-inverse" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#">Admin Roles</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#" onclick="adduser();"><i class="entypo-plus-squared"></i><span>Add</span></a>
			</li>
			<li><a href="#"  onclick="deleteuser();"><i class="entypo-minus-squared"></i><span>Delete</span></a>
			</li>	
		</ul>
		
	</div>
	<!-- /.navbar-collapse -->
</nav>
<script type="text/javascript">
var statusr=<%=Statusbean.getAdministrator()%>
function adduser(){
	//alert(statusr);
	if(statusr==2){            	
	   	jQuery('#modal-3').modal('hide');
	   	}
	else{
	   		jQuery('#modal-3').modal('show');
	   	}
}
function deleteuser(){
	if(statusr==2){            	
	   	jQuery('#mymodal').modal('hide');
	   	}
	else{
	   		jQuery('#mymodal').modal('show');
	   	}
}

</script>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Administrators</div>

                        <div class="panel-options">
                           <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>
                    
           <table class="table table-bordered responsive table-hover table-striped datatable" id="tableId">
                        <thead>
                            <tr>
                               <!-- <th class="dropdown">
            <a href="#" data-toggle="dropdown" class="dropdown-toggle">Messages <b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="#">Inbox</a></li>
                <li><a href="#">Drafts</a></li>
                <li><a href="#">Sent Items</a></li>
                <li class="divider"></li>
                <li><a href="#">Trash</a></li>
            </ul>
</th>-->
                                <th>&nbsp;</th>
                                <th id="name">Name</th>
                                <th id="role">Role</th>
                                <th id="authentication">Authentication Profile</th>
                                <th id="password">Password Profile</th>
                                <th id="CCA">Client Certificate Authentication</th>
                                <th id="PKA">Public Key Authentication</th>
                                <th id="profile">Profile</th>
                                <th id="lockedUser">Locked User</th>
                            </tr>
                        </thead>
                       <tbody>
                       <%
									int j=0;
								%>
								<%
									List<Userbean> userbean=Credential.getallusers();
								%>
								<%
									for(;j<userbean.size();j++){
								%>
								<tr>
									<td><input type="checkbox"  onClick="checkname('<%=userbean.get(j).getUsername() %>');"/></td>
									<td><a href="#" onClick="edituser('<%=userbean.get(j).getUsername()%>');" class="blue_popup"><%=userbean.get(j).getUsername() %></a></td>
									<td><%=userbean.get(j).getRolename() %></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									
                       			</tr>
                       			<%
									}
								%>
                       
       </tbody>
                    </table>

                    <script>
                    
                        
                        
                        function checkname(rname){
                    	$("#usernamed").text(rname);
                    	$('#userenamedelete').val(rname);
                        }
                        
                        
                        function edituser(eusername){
                        	
                        	
                        	$('#eusername1').val(eusername);
                        	if(statusr==2){            	
                        	   	jQuery('#modal-5').modal('hide');
                        	   	}
                        	else{
                        		$.ajax({ 
                            		url: 'getuser.jsp',
                            		dataType: 'text',
                            		data: {username:eusername},
                            		success: function(data) {                       			
                            			user=data.split('&');
                            			
                            			$('#efield-1').val(user[0]);	
                            			$('#ePassword1').val(user[1]);
                            			$('#ePassword2').val(user[1]);
                            			$('#editpass').val(user[1]);
                            			
                            			$('input:radio[name=minimal-radio-2]').attr('checked',false);
                        				$("[name=minimal-radio-2]").filter("[value='EDynamic']").prop("checked",true);
                        				$('#editdynamic').val(user[2]).change();
                            			$("#drop3").show();
                                        $("#drop4").hide();
                            			var checkdy=$('#editdynamic').val();
                            			
                            			if(!checkdy){
                            				$('input:radio[name=minimal-radio-2]').attr('checked',false);
                            				$("[name=minimal-radio-2]").filter("[value='ERoleBase']").prop("checked",true);
                            				$('#erolename').val(user[2]).change();
                            				$("#drop4").show();
                                            $("#drop3").hide();
                            				
                            				
                            			}
                            			
                            			
                            			
                            		}
                                });
                        		
                        	   		jQuery('#modal-5').modal('show');
                        	   	}
                        	
                        }
                    </script>
 
                </div>

                
            </div>
            <!-- Footer -->
            <footer class="main">
                Copyright &copy; 2016 <strong>Ecode Networks,</strong>All Rights Reservered

	
            </footer>
        </div>


    </div>
    
     <div class="modal fade" id="mymodal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Delte User</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Do you really want to delete User : <span id="usernamed"></span> 
							 <input type="hidden" id="userenamedelete" name="userenamedelete"/></label>
						
                             
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-info" onClick="deletinguser();">Ok</button>
              
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
    function deletinguser(){
    	
    		var usernamed=document.getElementById("userenamedelete").value;
    		$.ajax({ 
        		url: 'deleteuser.jsp',
        		dataType: 'text',
        		data: {username:usernamed},
        		success: function(data) {
        			
        		}
            });
    	
    		setTimeout(function () { window.location.reload(); }, 1);
    }
    </script>
    
 
    <div class="modal fade" id="adminProfile">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Admin Role Profile</strong></h4>
			</div>
			
			<div class="modal-body">
               
              <div class="row" style="margin-top:5px"> <div class="form-group">
						<label for="field-1" class="col-md-3 control-label">Name</label>
						
						<div class="col-md-9">
							<input type="text"  name="rolename" class="form-control" id="rname" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
						</div>
					</div> </div> 
              <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Description</label>
						
						<div class="col-md-9">
						<input type="text" class="form-control" name="roledesc" id="rdesc" data-validate="required" data-message-required="Required Field" placeholder="Enter Description">	
						</div>
					</div> </div>
               <div class="row" style="margin-top:5px">  <div class="form-group">
			 
                <ul class="nav nav-tabs bordered primary">
			<li class="active">
				<a href="#General" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-home"></i></span>
					<span class="hidden-xs">Web UI</span>
				</a>
			</li>
			<li>
				<a href="#source" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-user"></i></span>
					<span class="hidden-xs">XML API</span>
				</a> 
			</li>
			<li>
				<a href="#user" data-toggle="tab">
					<span class="visible-xs"><i class="entypo-mail"></i></span>
					<span class="hidden-xs">Command Line</span>
				</a>
			</li>
		</ul>
		        <div class="tab-content">
			    <div class="tab-pane active" id="General">
                <div class="scrollable" data-height="120">
               <ul style="padding:0px">
        <li>
            <a class="menu-toggle" href="#"><i  id="Dashboardlist" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Dashboard</label>
            <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="dashboard" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Dashboard</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="controlleralerts" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">ControllerAlerts</label>
                 </li>
                  </ul>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i id="administratorli" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Administrators</label>
              <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="administrator" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Administrator</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="adminrole" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">AdminRole</label>
                 </li>
                  </ul>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i id="design" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Design</label>
              <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="canvas" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Canvas</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="viewer" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Viewer</label>
                 </li>
                  </ul>
        </li>
        <li>
           <a class="menu-toggle" href="#"><i id="tags" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Tags</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i id="control" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Control</label>
              <ul>
                 <li>
                     <a class="menu-toggle" href="#"><i id="rulebase" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">RuleBase</label>
                 </li>
                 
                 <li>
                     <a class="menu-toggle" href="#"><i id="policybase" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">PolicyBase</label>
                 </li>
                 
                  <li>
                     <a class="menu-toggle" href="#"><i id="simulate" class="fa fa-check-circle-o"></i></a>
                     <label for="short-1" class="custom-unchecked">Simulate</label>
                 </li>
                  </ul>
            
        </li>
         <li>
           <a class="menu-toggle" href="#"><i id="flowrector" class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Flowrector</label>
        </li>
    </ul>
                    </div>
			</div>
                <div class="tab-pane" id="source">
				<div class="scrollable" data-height="120">
                   <ul style="padding:0px">
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Report</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Log</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Configuration</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Operational Requests</label>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Commit</label>
        </li>
         <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">User-ID Agent</label>
        </li>
        <li>
            <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Export</label>
        </li>
         <li>
           <a class="menu-toggle" href="#"><i class="fa fa-check-circle-o"></i></a>
            <label for="tall" class="custom-unchecked">Import</label>
        </li>
    </ul>
</div>
			</div>
			    <div class="tab-pane" id="user">
		         <div class="row">
                    <div class="form-group">
						
						<div class="col-md-12">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">superuser</option>
								<option value="2">superreader</option>
                                <option value="3">deviceadmin</option>
								<option value="4">devicereader</option>
							</select>
						</div>
					</div>
                       </div>
                 </div>
                		
			</div>	
		</div> </div>
                <div class="row" style="background-color:ActiveCaption;margin-top:5px">
                    <label style="color:black;margin-left:10px">Legend: <i class="fa fa-check-circle-o" style="margin:8px;"><span style="margin-left:3px">Enable</span></i> <i class="fa fa-times-circle-o" style="margin:8px;"><span style="margin-left:3px">Disable</span></i><i class="fa fa-lock" style="margin:8px;"><span style="margin-left:3px">Read-Only</span></i></label>
                </div>
                   
		
                <div class="form-group" style="text-align:right; margin-right:10px">
                <button type="button" class="btn btn-info" onclick="addrole();">Ok</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
                </div>
                
                </div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	function addrole(){
		var rolename1=document.getElementById('rname').value;
		var roledesc1=document.getElementById('rdesc').value;
		
		$.ajax({ 
    		url: 'AdminCreaterole.jsp',
    		dataType: 'text',
    		data: {rolename:rolename1, roledesc:roledesc1 },
    		success: function(data) {
    			
    		}
        });
		
		
		$.ajax({ 
    		url: 'Getallrolres.jsp',    		
    		success: function(data) {
    			document.getElementById("erolename").innerHTML=data;
    			
    		}
        });
		
		
		
		jQuery('#adminProfile').modal('hide');
	}
	
	
	</script>

      <div class="modal fade" id="passModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Password Profiles</strong></h4>
			</div>
			
			<div class="modal-body">
				
                  <div class="row"> <div class="form-group">
					<label for="field-1" class="col-md-6 control-label">Name</label>
					<div class="col-md-6">
				<input type="text" class="form-control" id="Text6" data-validate="required" data-message-required="Required Field" placeholder="Enter User Domain">
						</div>
					</div> </div>
                  <div class="row" style="margin-top:5px">   <div class="form-group">
					<label for="field-2" class="col-md-6 control-label">Required Password Change Period (days)</label>
					<div class="col-md-6">
				<input type="text" class="form-control" id="Text7">
						</div>
					</div></div>
                 <div class="row" style="margin-top:5px">   <div class="form-group">
					<label for="field-3" class="col-md-6 control-label">Expiration Warning Period (days)</label>
					<div class="col-md-6">
				<input type="text" class="form-control" id="Text8">
						</div>
					</div></div>
                  <div class="row" style="margin-top:5px">  <div class="form-group">
					<label for="field-4" class="col-md-6 control-label">Post Expiration Admin Login Count</label>
					<div class="col-md-6">
				<input type="text" class="form-control" id="Text9">
						</div>
					</div></div>
                  <div class="row" style="margin-top:5px">   <div class="form-group">
					<label for="field-5" class="col-md-6 control-label">Post Expiration Grace Period (days)</label>
					<div class="col-md-6">
				<input type="text" class="form-control" id="Text10">
						</div>
					</div></div>
                
                    
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info">Save changes</button>
			</div>
		</div>
	</div>
</div>    

    <div class="modal fade" id="myModal4">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Commit</strong></h4>
			</div>
			
			<div class="modal-body">
				<label class="col-sm-3 control-label">Import File</label>
						
						<div class="col-sm-9">
						<input type="file" class="form-control" />
							</div>
			</div>
			
			<div class="modal-footer">
               
                <button type="button" class="btn btn-info">Ok</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
              
			</div>
		</div>
	</div>
</div>

    <div class="modal fade" id="modal-2">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Commit</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Doing a commit will overwrite the running configuration. Do you want to continue?</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
                <button type="button" class="btn btn-info">Preview Changes</button>
                <button type="button" class="btn btn-info">Validate Changes</button>
                <button type="button" class="btn btn-info">Commit</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
              
			</div>
		</div>
	</div>
</div>

    <div class="modal fade" id="modal-1">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Save Configurations</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Saving this configuration will overwrite the previously saved configuration.
                              Do you want to continue?</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-info">Ok</button>
              
			</div>
		</div>
	</div>
</div>
    
    <div class="modal fade" id="adminModal">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Admin Role Profile</strong></h4>
			</div>
			
			<div class="modal-body">
                <form role="form">
             <div class="row" style="margin-top:5px"><div class="form-group">
						<label for="field-1" class="col-md-3 control-label">Name</label>
						
						<div class="col-md-9">
							<input type="text" class="form-control" id="Text1" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
						</div>
					</div> </div>
                <div class="panel minimal minimal-gray" style="margin-top:5px">
			
			<div class="panel-heading">
				<div class="panel-options">
					
					<ul class="nav nav-tabs">
						<li class="active"><a href="#profile-1" data-toggle="tab">Athentication</a></li>
						<li><a href="#profile-2" data-toggle="tab">Advanced</a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				
				<div class="tab-content">
					<div class="tab-pane active" id="profile-1">
					<!--Type-->
                 <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Type</label>
						
						<div class="col-md-9">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">Local Database</option>
								<option value="2">RADIUS</option>
                                <option value="3">LDAP</option>
								<option value="4">TACACS+</option>
                                <option value="5">Kerberos</option>
							</select>
						</div>
					</div></div>
                 <!--Service Profile-->
                  <div class="row" style="margin-top:5px">  <div class="form-group">
				<label for="field-2" class="col-md-3 control-label">Service Profile</label>
				 <div class="col-md-9">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">New Service Profile</option>
							</select>
						</div>
                 <div class="row">
                           <label for="field-3" class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<ul class="icheck-list" style="margin-left:5px">
						    <li>
						        <input tabindex="5" type="checkbox" class="icheck" id="Checkbox1">
						        <label for="minimal-checkbox-1">Retrieve User Group from Radius</label>
						    </li>
                                </ul>
						</div>
                       </div>
					</div></div>
                <!--  Long Attribute-->
                 <div class="row" style="margin-top:5px">  <div class="form-group">
					<label for="field-1" class="col-md-3 control-label">Long Attribute</label>
					<div class="col-md-9">
				<input type="text" class="form-control" id="Text3" data-validate="required" data-message-required="Required Field" placeholder="Enter User Domain">
						</div>
					</div></div>
                  <!--  Password Expiry Warning-->
                  <div class="row" style="margin-top:5px">  <div class="form-group">
					<label for="field-1" class="col-md-3 control-label">Password Expiry</label>
					<div class="col-md-9">
				<input type="text" class="form-control" id="Text4" data-validate="required" data-message-required="Required Field">
						</div>
					</div></div>
                   
                     <!--user domaian-->
                <div class="row" style="margin-top:5px">    <div class="form-group">
					<label for="field-1" class="col-md-3 control-label">User Domain</label>
					<div class="col-md-9">
				<input type="text" class="form-control" id="Text2" data-validate="required" data-message-required="Required Field">
						</div>
					</div></div>
                    <!--Kerberos Realm-->
                <div class="row" style="margin-top:5px">     <div class="form-group">
					<label for="field-1" class="col-md-3 control-label">Kerberos Realm</label>
					<div class="col-md-9">
				<input type="text" class="form-control" id="Text5" data-validate="required" data-message-required="Required Field">
						</div>
					</div></div>

                  <!--Username Modifier-->
                 <div class="row" style="margin-top:5px"> <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Username Modifier</label>
						
						<div class="col-md-9">
							<select name="test" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="1">%USERDOMAIN%\%USERINPUT%</option>
								<option value="2">%USERINPUT%</option>
                                <option value="3">%USERINPUT%@%USERDOMAIN%</option>
                                
							</select>
						</div>
					</div>	</div>
                    
                    </div>
					
					<div class="tab-pane" id="profile-2">
                        <div class="col-md-12">
                        <div class="row">
                     <TABLE border="0" style="border: 0px; width:99%;background-color:#7bb3d1;float:left;margin-left:4px">
                   <thead><TR style="border: 0px solid black;">
			          <TD style="border: 0px solid black;"><INPUT type="checkbox" name="chk"/><label style="margin-left:5px;color:black">Allow List</label></TD>
                          </TR></thead> 
                        <tbody style="background-color:#c6deeb"> </tbody> 
	                    </TABLE>
	                 <TABLE id="TABLE8" border="0" style="width:99%;background-color:#c6deeb;float:left;margin-left:4px">
                     <TR>
			          <TD><INPUT type="checkbox" name="chk"/><input type="text" style="width:90%;margin-left:5px" name="txt" /></TD>
		               </TR>
	                    </TABLE>
                          </div>
                        <div class="row" style="margin-top:10px;text-align:center">
                       <INPUT type="button" class="btn btn-success" value="Add" onclick="addRow('TABLE8')" />
                       <INPUT type="button" class="btn btn-danger" value="Delete" onclick="deleteRow('TABLE8')" />

                        </div>

                         </div>
					</div>
				</div>
				
		</div>
			
		</div>
		
	
                
                    </form>
		</div>
                <div class="form-group" style="text-align:right; margin-right:10px">
                <button type="button" class="btn btn-info">Ok</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
                </div>	
			</div>
		</div>
	</div>
	
	<div class="modal fade custom-width" id="modal-5">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Administrators</strong></h4>
			</div>
			
			<div class="modal-body">
				<form role="form" action="Updateuser.jsp" method="POST">
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-1" class="col-md-3 control-label">Name</label>
						
						<div class="col-md-9">
							<input type="text" name="editname" class="form-control" id="efield-1" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
							<input type="hidden" name="eusername1" id="eusername1" /> 
						</div>
					</div></div>
                    
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Auth Profile</label>
						
						<div class="col-md-9">
							<select id ="numbers" onchange = "myFunction()" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="0">None</option>
								<option value="1">New Profile</option>
							</select>
						</div>
					</div></div>
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-3" class="col-md-3 control-label">Password</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="ePassword1" id="ePassword1" required="required">
							<input type="hidden" name="editpass" id="editpass" /> 
						</div>
                       <div class="row">
                           <label for="field-3" class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<ul class="icheck-list" style="margin-left:5px">
						    <li>
						        <input tabindex="5" type="checkbox" class="icheck" id="check1">
						        <label for="minimal-checkbox-1">Use Public Key Authentication (SSH)</label>
						    </li>
                                </ul>
						</div>
                       </div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group" id="importhide" style="display:none">
						<label for="field-3" class="col-md-3 control-label"><a data-toggle="modal" href="#myModal4">Key Import</a></label>
						
						<div class="col-md-9">
							<textarea class="form-control" id="Textarea1" placeholder="Click Import Key to configure this field"></textarea>
						</div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-4" class="col-md-3 control-label">Confirm Password</label>
						
						<div class="col-md-9">
						<input type="hidden" name="epasswardvalue" id="epasswordmatch_id" /> 
							<input type="password" class="form-control" name="ePassword2" id="ePassword2" required="required" onchange="echeck()">
						</div>
	<script type="text/javascript">
    function echeck() {
    	var password1=document.getElementById('ePassword1');
    	var password2=document.getElementById('ePassword2');
    	//alert(password1.value);
        if (password1.value == password2.value) {
        	document.getElementById('epasswordmatch_id').value='matched';
           alert(document.getElementById('epasswordmatch_id').value);
            
        } else {
            // input is valid -- reset the error message
            alert("Password not Matched");
           
        }
    }
</script>
					</div></div>
                   
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-5" class="col-md-3 control-label">Administrator Type</label>
						
						<div class="col-md-9">
						
						        <input class="icheck-11" type="radio" id="EDynamic" name="minimal-radio-2" value="EDynamic" checked>
						        <label for="minimal-radio-1-11">Dynamic</label>
						   
						        <input class="icheck-11" type="radio" id="ERoleBased" name="minimal-radio-2" value="ERoleBase" style="margin-left:10px">
						        <label for="minimal-radio-2-11">Role Based</label>
						    </div>
                        
					</div></div>
                   <div class="row" style="margin-top:5px">   <div class="form-group" id="drop3">
						<label for="field-7" class="col-md-3 control-label">Dynamic</label>
						
						<div class="col-md-9">
							<select name="editdynamic" id="editdynamic" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="superuser">Super User</option>
								<option value="superuserreadonly">Superuser (read-only)</option>
                                <option value="3">Device administrator</option>
								<option value="4">Device administrator (read-only)</option>
							</select>
						</div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group" id="drop4" style="display:none">
						<label for="field-6" class="col-md-3 control-label">Profile</label>
						
						<div class="col-md-9">
							<select id="erolename" name="erolename" onchange="eadminChange()" class="selectboxit" data-first-option="false">
								<option>None</option>
								
								<%
									int k=0;
								%>
								<%
									List<Rolebean> bean=UserRole.getallroles();
								%>
								<%
									for(;k<bean.size();k++){
								%>
								<option value=<%=bean.get(k).getRole_name()%>><%=bean.get(k).getRole_name()%></option>
								<%
									}
								%>
								<option value="1">New Profile</option>
							</select>
						</div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-6" class="col-md-3 control-label">Password Profile</label>
						
						<div class="col-md-9">
							<select id = "passProfile" onchange = "pass1()" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="0">None</option>
								<option value="1">New Password Profile</option>
							</select>
						</div>
					</div></div>
                
			<div class="modal-footer">
                <button type="button" class="btn btn-info">Preview Changes</button>
                <button type="button" class="btn btn-info">Validate Changes</button>
                <button type="submit" class="btn btn-info">Commit</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
              
			</div>
			 </form>
			</div>
		</div>
	</div>
</div>
	

  <div class="modal fade custom-width" id="modal-3">
	<div class="modal-dialog" style="width: 50%;">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Administrators</strong></h4>
			</div>
			
			<div class="modal-body">
				<form role="form" action="Createuser.jsp" method="POST">
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-1" class="col-md-3 control-label">Name</label>
						
						<div class="col-md-9">
							<input type="text" name="name" class="form-control" id="field-1" data-validate="required" data-message-required="Required Field" placeholder="Enter Name">
						</div>
					</div></div>
                    
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-2" class="col-md-3 control-label">Auth Profile</label>
						
						<div class="col-md-9">
							<select id ="numbers" onchange = "myFunction()" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="0">None</option>
								<option value="1">New Profile</option>
							</select>
						</div>
					</div></div>
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-3" class="col-md-3 control-label">Password</label>
						<div class="col-md-9">
							<input type="password" class="form-control" name="Password1" id="Password1" required="required">
						</div>
                       <div class="row">
                           <label for="field-3" class="col-md-3 control-label"></label>
						<div class="col-md-9">
							<ul class="icheck-list" style="margin-left:5px">
						    <li>
						        <input tabindex="5" type="checkbox" class="icheck" id="check1">
						        <label for="minimal-checkbox-1">Use Public Key Authentication (SSH)</label>
						    </li>
                                </ul>
						</div>
                       </div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group" id="importhide" style="display:none">
						<label for="field-3" class="col-md-3 control-label"><a data-toggle="modal" href="#myModal4">Key Import</a></label>
						
						<div class="col-md-9">
							<textarea class="form-control" id="Textarea1" placeholder="Click Import Key to configure this field"></textarea>
						</div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-4" class="col-md-3 control-label">Confirm Password</label>
						
						<div class="col-md-9">
						<input type="hidden" name="passwardvalue" id="passwordmatch_id" /> 
							<input type="password" class="form-control" name="Password2" id="Password2" required="required" onchange="check()">
						</div>
	<script type="text/javascript">
    function check() {
    	var password1=document.getElementById('Password1');
    	var password2=document.getElementById('Password2');
    	//alert(password1.value);
        if (password1.value == password2.value) {
        	document.getElementById('passwordmatch_id').value='matched';
           // alert(document.getElementById('passwordmatch_id').value);
            
        } else {
            // input is valid -- reset the error message
            alert("Password not Matched");
           
        }
    }
</script>
					</div></div>
                   
                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-5" class="col-md-3 control-label">Administrator Type</label>
						
						<div class="col-md-9">
						
						        <input class="icheck-11" type="radio" id="Dynamic" name="minimal-radio-1" value="Dynamic" checked>
						        <label for="minimal-radio-1-11">Dynamic</label>
						   
						        <input class="icheck-11" type="radio" id="RoleBased" name="minimal-radio-1" value="Role" style="margin-left:10px">
						        <label for="minimal-radio-2-11">Role Based</label>
						    </div>
                        
					</div></div>
                   <div class="row" style="margin-top:5px">   <div class="form-group" id="drop1">
						<label for="field-7" class="col-md-3 control-label">Dynamic</label>
						
						<div class="col-md-9">
							<select name="dynamic" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="superuser">Super User</option>
								<option value="superuserreadonly">Superuser (read-only)</option>
                                <option value="3">Device administrator</option>
								<option value="4">Device administrator (read-only)</option>
							</select>
						</div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group" id="drop2" style="display:none">
						<label for="field-6" class="col-md-3 control-label">Profile</label>
						
						<div class="col-md-9">
							<select id="adminChnge" name="rolename" onchange="adminChange()" class="selectboxit" data-first-option="false">
								<option>None</option>
								
								<%
									int i=0;
								%>
								<%
									List<Rolebean> ubean=UserRole.getallroles();
								%>
								<%
									for(;i<ubean.size();i++){
								%>
								<option value=<%=ubean.get(i).getRole_name()%>><%=ubean.get(i).getRole_name()%></option>
								<%
									}
								%>
								<option value="1">New Profile</option>
							</select>
						</div>
					</div></div>

                   <div class="row" style="margin-top:5px">  <div class="form-group">
						<label for="field-6" class="col-md-3 control-label">Password Profile</label>
						
						<div class="col-md-9">
							<select id = "passProfile" onchange = "pass1()" class="selectboxit" data-first-option="false">
								<option>None</option>
								<option value="0">None</option>
								<option value="1">New Password Profile</option>
							</select>
						</div>
					</div></div>
                
			<div class="modal-footer">
                <button type="button" class="btn btn-info">Preview Changes</button>
                <button type="button" class="btn btn-info">Validate Changes</button>
                <button type="submit" class="btn btn-info">Commit</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				
              
			</div>
			 </form>
			</div>
		</div>
	</div>
</div>

    <link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="assets/js/select2/select2.css">
	<link rel="stylesheet" href="assets/js/selectboxit/jquery.selectBoxIt.css">
    <script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/select2/select2.min.js"></script>
	<script src="assets/js/bootstrap-tagsinput.min.js"></script>
	<script src="assets/js/typeahead.min.js"></script>
	<script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
	<script src="assets/js/bootstrap-datepicker.js"></script>
	<script src="assets/js/bootstrap-timepicker.min.js"></script>
	<script src="assets/js/bootstrap-colorpicker.min.js"></script>
	<script src="assets/js/daterangepicker/moment.min.js"></script>
	<script src="assets/js/daterangepicker/daterangepicker.js"></script>
	<script src="assets/js/jquery.multi-select.js"></script>
	<script src="assets/js/icheck/icheck.min.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>
