<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ page language="java" import="sources.*" %>
<%@ page language="java" import="java.util.*" %>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="java.io.*" %>
<%@ page language="java" import="bean.*" %>
<%@ page language="java" import="FlowPusher.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<% try{
	List<String>match = new ArrayList<String>();
	String priority = request.getParameter("flowpriority");
	String in_port = request.getParameter("in_port");
	String in_phy_port = request.getParameter("in_phy_port");
	String eth_dst = request.getParameter("eth_dst");
	String eth_src = request.getParameter("eth_src");
	String eth_type = request.getParameter("eth_type");
	String ip_proto = request.getParameter("ip_proto");
	String vlan_id = request.getParameter("vlan_id");
	String vlan_pcp = request.getParameter("vlan_pcp");
	String ip_dscp = request.getParameter("ip_dscp");
	String mpls_tc = request.getParameter("mpls_tc");
	String mpls_bos = request.getParameter("mpls_bos");
	String ipv4_src= request.getParameter("ipv4_src");
	String ipv4_dst= request.getParameter("ipv4_dst");
	String ipv6_src = request.getParameter("ipv6_src");
	String ipv6_dst = request.getParameter("ipv6_dst");
	String tcp_src = request.getParameter("tcp_src");
	String tcp_dst = request.getParameter("tcp_dst");
	String udp_src = request.getParameter("udp_src");
	
	
	String output ="{";
	output = output + "\"flow\":{";
	output= output+ "\"priority\":"+priority+",";
	output = output+ "\"match\": [";
	
	if(String.valueOf(in_port).equalsIgnoreCase("")||in_port==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"in_port\":"+ in_port +"},";
	}
	
	if(String.valueOf(in_phy_port).equalsIgnoreCase("")||in_phy_port==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"in_phy_port\":"+ in_phy_port +"},";
	}
	
	if(String.valueOf(eth_dst).equalsIgnoreCase("")||eth_dst==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"eth_dst\":"+"\""+eth_dst+"\"" +"},";
	}
	
	if(String.valueOf(eth_src).equalsIgnoreCase("")||eth_src==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"eth_src\":"+"\""+ eth_src+"\""  +"},";
	}
	
	if(String.valueOf(eth_type).equalsIgnoreCase("")||eth_type==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"eth_type\":"+"\""+ eth_type +"\""+"},";
	}
	
	if(String.valueOf(ip_proto).equalsIgnoreCase("")||ip_proto==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"ip_proto\":"+"\""+ ip_proto +"\""+"},";
	}
	
	if(String.valueOf(vlan_id).equalsIgnoreCase("")||vlan_id==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"vlan_id\":"+ vlan_id +"},";
	}
	
	if(String.valueOf(vlan_pcp).equalsIgnoreCase("")||vlan_pcp==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"vlan_pcp\":"+ vlan_pcp +"},";
	}
	
	if(String.valueOf(ip_dscp).equalsIgnoreCase("")||ip_dscp==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"ip_dscp\":"+ ip_dscp +"},";
	}
	
	if(String.valueOf(mpls_tc).equalsIgnoreCase("")||mpls_tc==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"mpls_tc\":"+ mpls_tc +"},";
	}
	
	if(String.valueOf(mpls_bos).equalsIgnoreCase("")||mpls_bos==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"mpls_bos\":"+ mpls_bos +"},";
	}
	
	if(String.valueOf(ipv4_src).equalsIgnoreCase("")||ipv4_src==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"ipv4_src\":"+"\""+ ipv4_src +"\""+"},";
	}
	
	if(String.valueOf(ipv4_dst).equalsIgnoreCase("")||ipv4_dst==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"ipv4_dst\":"+"\""+ ipv4_dst +"\""+"},";
	}
	
	if(String.valueOf(ipv6_src).equalsIgnoreCase("")||ipv6_src==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"ipv6_src\":"+"\""+ ipv6_src +"\""+"},";
	}
	
	if(String.valueOf(tcp_src).equalsIgnoreCase("")||tcp_src==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"tcp_src\":"+ tcp_src +"},";
	}
	
	if(String.valueOf(tcp_dst).equalsIgnoreCase("")||tcp_dst==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"tcp_dst\":"+ tcp_dst +"},";
	}
	
	if(String.valueOf(udp_src).equalsIgnoreCase("")||udp_src==null)
	{
	output = output;
	}
	else{
	output = output+"{ \"udp_src\":"+ udp_src +"},";
	}
	output=output.substring(0,output.length()-1);
	output=output+"],";

	output = output+ "\"actions\": [";

	String aoutput= request.getParameter("output");
	String copy_ttl_out= request.getParameter("copy_ttl_out");
	String copy_ttl_in= request.getParameter(" copy_ttl_in");
	String set_mpls_ttl= request.getParameter("set_mpls_ttl");
	String push_vlan= request.getParameter("push_vlan");
	String pop_vlan= request.getParameter("pop_vlan");
	String push_mpls= request.getParameter("push_mpls");
	String pop_mpls= request.getParameter("pop_mpls");

	if(String.valueOf(aoutput).equalsIgnoreCase("")||aoutput==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"output\":\""+ aoutput +"\"},";
		}

	if(String.valueOf(copy_ttl_out).equalsIgnoreCase("")||copy_ttl_out==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"copy_ttl_out\":"+"\""+ copy_ttl_out +"\""+"},";
		}

	if(String.valueOf(copy_ttl_in).equalsIgnoreCase("")||copy_ttl_in==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"copy_ttl_in\":"+"\""+ copy_ttl_in +"\""+"},";
		}

	if(String.valueOf(set_mpls_ttl).equalsIgnoreCase("")||set_mpls_ttl==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"set_mpls_ttl\":"+ set_mpls_ttl +"},";
		}

	if(String.valueOf(push_vlan).equalsIgnoreCase("")||push_vlan==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"push_vlan\":"+ push_vlan +"},";
		}

	if(String.valueOf(pop_vlan).equalsIgnoreCase("")||pop_vlan==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"pop_vlan\":"+"\""+ pop_vlan +"\""+"},";
		}

	if(String.valueOf(push_mpls).equalsIgnoreCase("")||push_mpls==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"push_mpls\":"+ push_mpls +"},";
		}

	if(String.valueOf(pop_mpls).equalsIgnoreCase("")||pop_mpls==null)
		{
		output = output;
		}
		else{
		output = output+"{ \"pop_mpls\":"+ pop_mpls +"},";
		}
	output=output.substring(0,output.length()-1);

	output=output+"]}}";
	System.out.println("out in sfp : "+output);
	String dpid= request.getParameter("switchnum");
	flowPusher.pushFlows(dpid, output);
	
	
}
catch(Exception e)
{
	e.printStackTrace();
}

%>

<%
    String redirectURL = "Viewer.jsp";
   response.sendRedirect(redirectURL);
%>
</body>
</html>