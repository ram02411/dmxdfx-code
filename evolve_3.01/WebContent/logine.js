var count = 2;
function validate() {
	var un = document.login.text1.value;
	var pw = document.login.password1.value;


	var check;
	$.ajax({ 
		url: 'enterajax.jsp',
		dataType: 'text',
		data: {name: un, pwd:pw },
		success: function(data) {
			check=data;
			if(parseInt(data) > 0){
				window.location = "setnow.jsp";
			}
			else{
				var t = " tries";
				if (count == 1) {
					t = " try"
				}

				if (count >= 1) {
					alert("Invalid username and/or password.  You have " + count + t
							+ " left.");
					document.login.text1.value = "";
					document.login.password1.value = "";
					setTimeout("document.myform.username.focus()", 25);
					setTimeout("document.myform.username.select()", 25);
					count--;
				}

				else {
					alert("Still incorrect! You have no more tries left!");
					document.login.text1.value = "No more tries allowed!";
					document.login.text1.disabled = true;
					document.login.password1.disabled = true;
					return false;
				}
	}
		}
	});

}
	
function clear() {
	document.login.text1.value = "";
	document.login.password1.value = "";
	document.login.ip1.value = "";
}