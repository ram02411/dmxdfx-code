<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@page import="dto.Filteralerts"%>
<%@ page language="java" import="java.util.*"%>
<%@page import="java.sql.*"%>
<%@ page language="java" import="service.*"%>
<%@ page language="java" import="bean.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ page language="java" import="dto.*"%>
<%
 try{

  String check=setsessionvar.getSessioname();
	if(check.length()>0||!(check.equals(""))){
		String tl=Lockdown.getLockdown();
		if(tl.equalsIgnoreCase("open")){
		}
		else{
			String redirectURL = "lock.jsp";
			 response.sendRedirect(redirectURL);
		}
}
else{
 String redirectURL = "invalid.jsp";
 response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Evolve Panel" />
    <meta name="author" content="" />

    <title>Evolve | Controller Alerts</title>

	<meta http-equiv="description" content="This is my page">
    <link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="assets/css/neon-core.css">
    <link rel="stylesheet" href="assets/css/neon-theme.css">
    <link rel="stylesheet" href="assets/css/neon-forms.css">
    <link rel="stylesheet" href="assets/css/custom.css">
    
    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <script src="assets/js/datetimepicker_css.js"></script>
    

 
    <style>
        .table-hover tbody tr:hover td, .table-hover tbody tr:hover th {
            background-color: #D4E0E0;
        }

        .table-striped > tbody > tr:nth-child(odd) > td,
        .table-striped > tbody > tr:nth-child(odd) > th {
            background-color: #E5EEEE;
        }
     
    </style>
</head>


<body class="page-body" data-url="http://ecodenetworks.com">

    <div class="page-container">
      
        <div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                   <a href="#" class="user-link" style="margin-top:-5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width:57px;height:53px;"/>
                        <span style="margin-left:8px">Welcome<strong>Admin</strong></span>
                       
                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>

            </header>

            <ul id="main-menu" class="">
               <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                <li class="opened">
                    <a href="Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="ControllerAlerts.jsp">
                                <span>Controller Alerts</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                <li class="opened">
                    <a href="Canvas.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Canvas.jsp">
                                <span>Canvas</span>
                            </a>
                        </li>
                        <li>
                            <a href="Viewer.jsp">
                                <span>Viewer</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="opened">
                    <a href="Simulate.jsp">
                        <i class="entypo-layout"></i>
                        <span>Control</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Rulebase.jsp">
                                <span>Rule Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="PolicyBase.jsp">
                                <span>Policy Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="Simulate.jsp">
                                <span>Simulate</span>
                            </a>
                        </li>
                    </ul>

                </li>


               
                <li>
                    <a href="Flowrector.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Flow Rector</span>
                    </a>


                </li>
            </ul>


        </div>

        <div class="main-content">

            <div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">
                    
                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">
                        <li>
                            <a><strong>Ecode Networks</strong> <i class="entypo-users"></i></a>
                        </li>
                        <li class="sep"></li>
                        <li>
                        
                               <a href="#" onclick="jQuery('#modal-2').modal('show');">
                            <strong>Filter Alerts</strong> <i class="entypo-cog"></i></a>
                        </li>
                        <li class="sep"></li>
                        

                        <li>
                            <a href="setout.jsp">
                                <strong>LogOut</strong>  <i class="entypo-logout right"></i></a>
                        </li>
                    </ul>
                </div>

            </div>

            <hr />

            <div class="row">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="panel-title"><strong> Controller Alerts</strong></div>

                        <div class="panel-options">
                           <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                            <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                            <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                        </div>
                    </div>

                    <table class="table table-bordered responsive table-hover table-striped datatable" id="table-1">
                        <thead>
                            <tr>
                                <th>SNo.</th>
                                <th data-priority="1">ID</th>
                                <th data-priority="2">Date</th>
                                <th data-priority="4">Time</th>
                                <th data-priority="4">Severity</th>
                                <th data-priority="4">Topic</th>
                                <th data-priority="4">Origin</th>
                                <th data-priority="4">Description</th>
                            </tr>
                        </thead>
                        <tbody>
                        <%
												int i=0;
											%>
											<% System.out.println("TESTBEAN 3 HAS BEEN ENTERED !! "+Filteralerts.getStartime()); %>
											<%
												List<String> sea= new ArrayList<String>();
											%>
											<%
												System.out.println("starttime"+Filteralerts.getStartime());
											%>
											<%
												sea.add(Filteralerts.getStartime());
											%>
											<%
												sea.add(Filteralerts.getEndime());
											%>
											<% sea.add(Filteralerts.getSeverity()); %>
											<% sea.add(Filteralerts.getTopic()); %>
											<% sea.add(Filteralerts.getOrigin()); %>
											<%  System.out.println("SORT TYPE IS AS FOLLOWS " +Filteralerts.getSortalerts()); %>
											<% sea.add(Filteralerts.getSortalerts()); %>
											<% List<Alertbean> bean=Alertsbase.callalerts(sea); %>
											<% for(;i<bean.size();i++){ %>
											<% String color=bean.get(i).getSeverity(); %>
											<tr >
												<td><%= i+1 %></td>
												<td><%= bean.get(i).getId() %></td>
												<td><%= bean.get(i).getDatestamp() %></td>
												<td><%= bean.get(i).getTimestamp() %></td>
												<td><%= bean.get(i).getSeverity() %></td>
												<td><%= bean.get(i).getTopic() %></td>
												<td><%= bean.get(i).getOrg() %></td>
												<td class="description"><%= bean.get(i).getDescr() %></td>
											</tr>
											<% } %>
                           
                        </tbody>
                    </table>
                   
                </div>

                
            </div>
            <!-- Footer -->
            <footer class="main">
                Copyright &copy; 2016 <strong>Ecode Networks,</strong>All Rights Reservered

	
            </footer>
        </div>


    </div>


    <div class="modal fade custom-width" id="modal-2">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			<div class="modal-header"  style="text-align:center">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title"><strong>Filter Alerts</strong></h3>
			</div>
			
			<div class="modal-body">
               
                <form role="form" name="calendar" method="POST" action="alertscall.jsp">
			    <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							
							
							<label for="field-1" class="control-label">Start Date</label>
								<input type="text" id="demo1" name="demo1" maxlength="25" size="25">
								 <img src="assets/images/cal.gif" onclick="javascript:NewCssCal ('demo1','DDMMMYYYY','arrow',true,'24',true)" style="cursor:pointer"/>
								
								
										
							
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
			
				<div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							
							
							<label for="field-1" class="control-label">End Date</label>
								<input type="text" id="demo2" name="demo2" maxlength="25" size="25">
								 <img src="assets/images/cal.gif" onclick="javascript:NewCssCal ('demo2','DDMMMYYYY','arrow',true,'24',true)" style="cursor:pointer"/>
								
								
										
							</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
			
                <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Severity</label>
							
							<select name='severity' id='severity' data-mini='true' data-inline='true'>
							<option name='selectsev' value='All'>All</option>
							<% int a=0; %><% List<String> gsev=Alertsbase.callsev(); %>
							<% for(;a<gsev.size();a++){ %>
							<option name='<%= gsev.get(a).toString() %>' value='<%= gsev.get(a).toString() %>'><%= gsev.get(a).toString() %></option><% } %>
							</select>
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>

                <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Topic</label>
							
							<select name='topic' id='topic' data-mini='true' data-inline='true'>
							<option name='selecttopic' value='All'>All</option>
							<% int b=0; %><% List<String> gtopic=Alertsbase.calltopic(); %>
							<% for(;b<gtopic.size();b++){ %>
							<option name='<%= gtopic.get(b).toString() %>' value='<%= gtopic.get(b).toString() %>'><%= gtopic.get(b).toString() %></option><% } %>
							</select>
							</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
                <div class="row">
                    <div class="col-md-3"></div>
					<div class="col-md-6">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Origin</label>
								<select name='origin' id='origin' data-mini='true' data-inline='true'>
								<option name='selectorigin' value='All'>All</option>
								<% int c=0; %><% List<String> gorg=Alertsbase.callorg(); %>
								<% for(;c<gorg.size();c++){ %>
								<option name='<%= gorg.get(c).toString() %>' value='<%= gorg.get(c).toString() %>'><%= gorg.get(c).toString() %></option><% } %>
								</select>
						</div>	
						
					</div>
					<div class="col-md-3"></div>
				</div>
                <div class="roform-group"  style="text-align:center">
               <button type="button" class="btn btn-danger btn-grey" data-dismiss="modal">Close</button>&nbsp &nbsp
               <button type="submit" class="btn btn-info btn-grey">Filter</button>&nbsp &nbsp
               <button type="reset" class="btn btn-info btn-grey">Reset</button>
           </div>
</form>
			</div>
			
                

                
		</div>
	</div>
</div>


    

    <link rel="stylesheet" href="assets/js/zurb-responsive-tables/responsive-tables.css">

    <!-- Bottom Scripts -->
    <script src="assets/js/gsap/main-gsap.js"></script>
    <script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/joinable.js"></script>
    <script src="assets/js/resizeable.js"></script>
    <script src="assets/js/neon-api.js"></script>
    <script src="assets/js/zurb-responsive-tables/responsive-tables.js"></script>
    <script src="assets/js/bootstrap-datepicker.js"></script>
	<script src="assets/js/bootstrap-timepicker.min.js"></script>
	<script src="assets/js/bootstrap-colorpicker.min.js"></script>
	<script src="assets/js/daterangepicker/daterangepicker.js"></script>
    <script src="assets/js/daterangepicker/moment.min.js"></script>
    <script src="assets/js/neon-custom.js"></script>
    <script src="assets/js/neon-demo.js"></script>

</body>
</html>
	
