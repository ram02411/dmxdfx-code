<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page language="java" import="java.util.*" %>
<%@ page language="java" import="stateTable.*"%>
<%@ page language="java" import="FlowPusher.*"%>
<%@ page language="java" import="dto.*"%>
<%  List<String> switches=new ArrayList<String>();
 try{

  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	String tl=Lockdown.getLockdown();
	if(tl.equalsIgnoreCase("open")){
		switches =stateTableHP.getTopo();
	}
	else{
		String redirectURL = "lock.jsp";
		 response.sendRedirect(redirectURL);
	}
}
else{
 String redirectURL = "invalid.jsp";
 response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>Ecode | Viewer</title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">
     <link href="assets/css/font-icons/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<script language="javascript" type="text/javascript" src="jstp/jit.js"></script>
	<script src="assets/js/jquery-1.11.0.min.js"></script>

	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
<script>
	var labelType, useGradients, nativeTextSupport, animate;

	(function() {
		var ua = navigator.userAgent, iStuff = ua.match(/iPhone/i)
				|| ua.match(/iPad/i), typeOfCanvas = typeof HTMLCanvasElement, nativeCanvasSupport = (typeOfCanvas == 'object' || typeOfCanvas == 'function'), textSupport = nativeCanvasSupport
				&& (typeof document.createElement('canvas').getContext('2d').fillText == 'function');
		//I'm setting this based on the fact that ExCanvas provides text support for IE
		//and that as of today iPhone/iPad current text support is lame
		labelType = (!nativeCanvasSupport || (textSupport && !iStuff)) ? 'Native'
				: 'HTML';
		nativeTextSupport = labelType == 'Native';
		useGradients = nativeCanvasSupport;
		animate = !(iStuff || !nativeCanvasSupport);
	})();

	var Log = {
		elem : false,
		write : function(text) {
			if (!this.elem)
				this.elem = document.getElementById('log');
			alert(text);
			this.elem.innerHTML = text;
			this.elem.style.left = (500 - this.elem.offsetWidth / 2) + 'px';
		}
	};

	function init() {
		// init data
		var json = <%= topologyConstructor.getTopology() %>
		// end
		// init ForceDirected
		var fd = new $jit.ForceDirected({
			//id of the visualization container
			injectInto : 'infovis',
			//Enable zooming and panning
			//by scrolling and DnD
			Navigation : {
				enable : true,
				//Enable panning events only if we're dragging the empty
				//canvas (and not a node).
				panning : 'avoid nodes',
				zooming : 10
			//zoom speed. higher is more sensible
			},
			// Change node and edge styles such as
			// color and width.
			// These properties are also set per node
			// with dollar prefixed data-properties in the
			// JSON structure.
			Node : {
				overridable : true
			},
			Edge : {
				overridable : true,
				color : '#23A4FF',
				lineWidth : 0.4
			},
			//Native canvas text styling
			Label : {
				type : labelType, //Native or HTML
				size : 10,
				style : 'bold'
			},
			//Add Tips
			Tips : {
				enable : true,
				onShow : function(tip, node) {
					var count = 0;
					node.eachAdjacency(function() {
						count++;
					});
					
					
					var out="<table><tr>"+"\n";
					$.ajax({
					    url : 'switchDetailsServer.jsp',
					    data : { dpid: node.name },
					    dataType: 'json',
					    success : function(json) {
					    	 	for(var i=0;i<json.length;i++)
					    		{
					    		
					    		for(var k=0;k<Object.keys(json[i]).length;k++){
					    		key=Object.keys(json[i])[k];
					    		out = out+"<th>"+key+"</th>";
					    		}
					    		out=out+"</tr>"+"\n";
					    		out = out+"<tr>";
					    		for(var k=0;k<Object.keys(json[i]).length;k++){
						    		 key=Object.keys(json[i])[k];
						    		  
						    		      
						    		out = out+"<td>"+ json[i][key]+"</td>";
						    		}
					    		out=out+"</tr>"+"\n";}
					    		out = out+"</table>";
					    		   tip.innerHTML = "<div class=\"tip-title\">" + node.name
									+ "</div>"
									+ "<div class=\"tip-text\"><b>connections:</b> "
									+ count + "</div>"
									+ "<div class=\"ports\"><b>port details:</b> "+out+"</div>";
									    		
					    	    }
					 
					    
					});
					//count connections
					
				
					//display node info in tooltip
					
				}
			},
			// Add node events
			Events : {
				enable : true,
				type : 'Native',
				//Change cursor style when hovering a node
				onMouseEnter : function() {
					fd.canvas.getElement().style.cursor = 'move';
				},
				onMouseLeave : function() {
					fd.canvas.getElement().style.cursor = '';
				},
				//Update node positions when dragged
				onDragMove : function(node, eventInfo, e) {
					var pos = eventInfo.getPos();
					node.pos.setc(pos.x, pos.y);
					fd.plot();
				},
				//Implement the same handler for touchscreens
				onTouchMove : function(node, eventInfo, e) {
					$jit.util.event.stop(e); //stop default touchmove event
					this.onDragMove(node, eventInfo, e);
				},
				//Add also a click handler to nodes
				onClick : function(node) {
					if (!node)
						return;
					// Build the right column relations list.
					// This is done by traversing the clicked node connections.
					var html = "<h4>" + node.name
							+ "</h4><b> connections:</b><ul><li>", list = [];
					node.eachAdjacency(function(adj) {
						list.push(adj.nodeTo.name);
					});
					//append connections information
					$jit.id('inner-details').innerHTML = html
							+ list.join("</li><li>") + "</li></ul>";
				},
				onRightClick : function(node) {
					if (!node)
						return;
					var out="<link type='text/css' href='cssnew/flowdetails.css' rel='stylesheet'><div style='width:50%; float:left; text-align:left;'><a style='margin-left:0%;' href='javascript:closepopup1();' onclick='closepopup1();'><b class='canvasbutton3'>&#88;</b></a></div><center><h4 align='center' style='text-align:center;'>Flow Details - "+node.name+"</h4></center><input type='hidden' name='switchname' val='"+node.name+"'><table class='flowdetls'><tr>"+"\n";
					$.ajax({
					    url : 'flowTableServer.jsp',
					    data : { dpid: node.name },
					    dataType: 'json',
					    success : function(json) {
					    	for(var i=0;i<json.length;i++)
					    		{
					    		
					    		for(var k=0;k<Object.keys(json[i]).length;k++){
					    		key=Object.keys(json[i])[k];
					    		out = out+"<th>"+key+"</th>";
					    		}
					    		out=out+"</tr>"+"\n";
					    		out = out+"<tr>";
					    		for(var k=0;k<Object.keys(json[i]).length;k++){
						    		 key=Object.keys(json[i])[k];
						    		  
						    		      
						    		out = out+"<td>"+ json[i][key]+"</td>";
						    		}
					    		out=out+"</tr>"+"\n";}
					    		out = out+"</table>";
					    		
					    		
					    		document.getElementById("popup1").innerHTML=out;
					    		loadpopup1();
					    	    }
					});
				}
			},
			//Number of iterations for the FD algorithm
			iterations : 200,
			//Edge length
			levelDistance : 130,
			// Add text to the labels. This method is only triggered
			// on label creation and only for DOM labels (not native canvas ones).
			onCreateLabel : function(domElement, node) {
				domElement.innerHTML = node.name;
				var style = domElement.style;
				style.fontSize = "0.8em";
				style.color = "#ddd";
			},
			// Change node styles when DOM labels are placed
			// or moved.
			onPlaceLabel : function(domElement, node) {
				var style = domElement.style;
				var left = parseInt(style.left);
				var top = parseInt(style.top);
				var w = domElement.offsetWidth;
				style.left = (left - w / 2) + 'px';
				style.top = (top + 10) + 'px';
				style.display = '';
			}
		});
		// load JSON data.
		fd.loadJSON(json);
		// compute positions incrementally and animate.
		fd.computeIncremental({
			iter : 40,
			property : 'end',
			onStep : function(perc) {
				Log.write(perc + '% loaded...');
			},
			onComplete : function() {
				Log.write('');
				fd.animate({
					modes : [ 'linear' ],
					transition : $jit.Trans.Elastic.easeOut,
					duration : 2500
				});
			}
		});
		// end
	}
</script>
	
	
</head>
<body class="page-body"  onload="init();">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width: 57px; height: 53px;" />

                        <span style="margin-left:8px"> Welcome <strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse" >
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                      <!--  <i class="entypo-menu"></i>-->
                    </a>
                </div>

            </header>

          
            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                <li class="opened">
				<a href="Dashboard.html">
					<i class="entypo-gauge"></i>
					<span>Dashboard</span>
				</a>
				<ul>
                    <li>
                   <a href="Dashboard.html">
					<span>Dashboard</span>
				</a>
                    </li>
					<li>
						<a href="ControllerAlerts.html">
							<span>Controller Alerts</span>
						</a>
					</li>
                    </ul>
                   </li>
                <li>
                <li class="opened">
                    <a href="canvas.html">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        <li>
                            <a href="canvas.html">
                                <span>Canvas</span>
                            </a>
                        </li>
                        <li>
                            <a href="Viewer.html">
                                <span>Viewer</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="opened">
                    <a href="simulate.html">
                        <i class="entypo-layout"></i>
                        <span>Control</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Rulebase.html">
                                <span>Rule Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="PolicyBase.html">
                                <span>Policy Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="simulate.html">
                                <span>Simulate</span>
                            </a>
                        </li>
                    </ul>

                </li>


                 <li>
                    <a href="Netlyzer.html">
                        <i class="entypo-doc-text"></i>
                        <span>Netlyzer</span>
                    </a>

                </li>

                <li>
                    <a href="flowrector.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Flow Rector</span>
                    </a>


                </li>
            </ul>



        </div>	
	<div class="main-content">
		
<div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">

                    <ul class="user-info pull-left pull-none-xsm">
                    </ul>
                    <ul class="user-info pull-left pull-right-xs pull-none-xsm">

                        <!-- Raw Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-attention"></i>
                                <span class="badge badge-info">6</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p class="small">
                                        <a href="#" class="pull-right">Mark all Read</a>
                                        You have <strong>3</strong> new notifications.
                                    </p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="unread notification-success">
                                            <a href="#">
                                                <i class="entypo-user-add pull-right"></i>

                                                <span class="line">
                                                    <strong>New user registered</strong>
                                                </span>

                                                <span class="line small">30 seconds ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="unread notification-secondary">
                                            <a href="#">
                                                <i class="entypo-heart pull-right"></i>

                                                <span class="line">
                                                    <strong>Someone special liked this</strong>
                                                </span>

                                                <span class="line small">2 minutes ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-primary">
                                            <a href="#">
                                                <i class="entypo-user pull-right"></i>

                                                <span class="line">
                                                    <strong>Privacy settings have been changed</strong>
                                                </span>

                                                <span class="line small">3 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-danger">
                                            <a href="#">
                                                <i class="entypo-cancel-circled pull-right"></i>

                                                <span class="line">John cancelled the event
                                                </span>

                                                <span class="line small">9 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-info">
                                            <a href="#">
                                                <i class="entypo-info pull-right"></i>

                                                <span class="line">The server is status is stable
                                                </span>

                                                <span class="line small">yesterday at 10:30am
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-warning">
                                            <a href="#">
                                                <i class="entypo-rss pull-right"></i>

                                                <span class="line">New comments waiting approval
                                                </span>

                                                <span class="line small">last week
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">View all notifications</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Message Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-mail"></i>
                                <span class="badge badge-secondary">10</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-1.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">
                                                    <strong>Luc Chartier</strong>
                                                    - yesterday
                                                </span>

                                                <span class="line desc small">This ain’t our first item, it is the best of the rest.
                                                </span>
                                            </a>
                                        </li>

                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-2.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">
                                                    <strong>Salma Nyberg</strong>
                                                    - 2 days ago
                                                </span>

                                                <span class="line desc small">Oh he decisively impression attachment friendship so if everything. 
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-3.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">Hayden Cartwright
					- a week ago
                                                </span>

                                                <span class="line desc small">Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-4.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">Sandra Eberhardt
					- 16 days ago
                                                </span>

                                                <span class="line desc small">On so attention necessary at by provision otherwise existence direction.
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="mailbox.html">All Messages</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Task Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-list"></i>
                                <span class="badge badge-warning">1</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p>You have 6 pending tasks</p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Procurement</span>
                                                    <span class="percent">27%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 27%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">27% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">App Development</span>
                                                    <span class="percent">83%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 83%;" class="progress-bar progress-bar-danger">
                                                        <span class="sr-only">83% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">HTML Slicing</span>
                                                    <span class="percent">91%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 91%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">91% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Database Repair</span>
                                                    <span class="percent">12%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 12%;" class="progress-bar progress-bar-warning">
                                                        <span class="sr-only">12% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Backup Create Progress</span>
                                                    <span class="percent">54%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 54%;" class="progress-bar progress-bar-info">
                                                        <span class="sr-only">54% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Upgrade Progress</span>
                                                    <span class="percent">17%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 17%;" class="progress-bar progress-bar-important">
                                                        <span class="sr-only">17% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">See all tasks</a>
                                </li>
                            </ul>

                        </li>

                    </ul>

                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">


                        <li>
                            <strong>Ecode Networks</strong> <i class="entypo-users"></i>

                        </li>

                        <li class="sep"></li>

                        <li>
                            <a href="Login.html">Log Out <i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>




                </div>
            </div>
<nav class="navbar navbar-inverse" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Viewer</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Add<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#" onclick="jQuery('#modal-1').modal('show', {backdrop: 'static'});">Add Flow</a>

                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Go To<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="simulate.html">PNT-SNT</a>
                                </li>
                                <li><a href="ViewerSnt.html">Viewer SNT</a>
                                </li>
                                <li><a href="Rulebase.html">PNT RuleBase</a>
                                </li>
                                <li><a href="PolicyBase.html">PNT PolicyBase</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
<div class="container-fluid">
                <div class="col-lg-12" style="background-color: aliceblue; width: auto; height: 600px;" id="infovis" >
               		 
                    <div id="log">
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
<footer class="main">
	
		
	&copy; 2014 <strong>Neon</strong> Admin Theme by <a href="http://laborator.co" target="_blank">Laborator</a>
	
</footer>	</div>

	</div>

<!-- Modal 3 (Custom Width)-->
<div class="modal fade" id="modal-1">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Add Flow</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Switch</label>
  <select class="form-control" id="Select1">
    <option>1</option>
    <option>2</option>
    <option>3</option>
    <option>4</option>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
                <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Priority</label>
                           <input type="text" class="form-control" id=" Priority" placeholder="Enter Priority">
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							 <label for="sel1">Match Parameter No.</label>
                           <input type="text" class="form-control" id="Text2" placeholder="Enter Priority">
						</div>
                    </div>
					<div class="col-md-6">
						
						<div class="form-group">
							 <label for="sel1">Action Parameter No.</label>
                           <input type="text" class="form-control" id="Text1" placeholder="Enter Priority">
						</div>	
						
					</div>
					
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info">Save changes</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal-4">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Add Switch</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1"> Switch Type </label>
  <select class="form-control" id="Select2">
    <option>Default Switch</option>
    <option>HPFB_12900</option>
    <option>HSR 6800</option>
    <option>FF5900</option>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
                <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Switch Name </label>
  <input type="text" class="form-control" id="switchName" placeholder="Enter Switch Name">
                            			</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info">Save changes</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal 4 (Confirm)-->
<div class="modal fade" id="modal-5">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h4 class="modal-title">Add Host</h4>
			</div>
			
			<div class="modal-body">
			<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Host Name </label>
  <input type="text" class="form-control" id="hostName" placeholder="Enter Host Name">
                            			</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-info" data-dismiss="modal">Continue</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal 5 (Long Modal)-->
<div class="modal fade" id="modal-7">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Policy</h4>
			</div>
			
			<div class="modal-body">
			<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Protocol</label>
  <select class="form-control" id="Select3">
    <option>---Select---</option>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
            <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Source</label>
  <select class="form-control" id="Select4">
    <option>---Select---</option>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
            <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Destination</label>
  <select class="form-control" id="Select5">
    <option>---Select---</option>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info">Save changes</button>
			</div>
		</div>
	</div>
</div>


<!-- Modal 6 (Long Modal)-->
<div class="modal fade" id="modal-6">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Links</h4>
			</div>
			
			<div class="modal-body">
			       <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							<label for="field-1" class="control-label">Source</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
					<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							<label for="field-1" class="control-label">Destination</label>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info">Save changes</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal 7 (Ajax Modal)-->
<div class="modal fade" id="modal-71">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Dynamic Content</h4>
			</div>
			
			<div class="modal-body">
			
				Content is loading...
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info">Save changes</button>
			</div>
		</div>
	</div>
</div>


	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/neon-chat.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>