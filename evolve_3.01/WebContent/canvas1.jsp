<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page language="java" import="java.util.*"%>

<%@ page language="java" import="service.*"%>
<%@ page language="java" import="dto.*"%>
<%
	
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<%List<List<String>> alert =new ArrayList<List<String>>();
List<String> switches=new ArrayList<String>();
 try{

  String check=setsessionvar.getSessioname();
if(check.length()>0||!(check.equals(""))){
	//alert = alertPicker.getAlerts(); 
	
}
else{
 String redirectURL = "invalid.jsp";
 //response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	
	<title>Ecode | Canvas</title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">
     <link href="assets/css/font-icons/font-awesome/css/font-awesome.css" rel="stylesheet" />

	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>
	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body class="page-body">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width: 57px; height: 53px;" />

                        <span style="margin-left:8px"> Welcome <strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse" >
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                      <!--  <i class="entypo-menu"></i>-->
                    </a>
                </div>

            </header>

          
            <ul id="main-menu" class="">
                
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                <li class="opened">
                    <a href="Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="ControllerAlerts.jsp">
                                <span>Controller Alerts</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                <li class="opened">
                    <a href="Canvas.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Canvas.jsp">
                                <span>Canvas</span>
                            </a>
                        </li>
                        <li>
                            <a href="Viewer.jsp">
                                <span>Viewer</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="opened">
                    <a href="Simulate.jsp">
                        <i class="entypo-layout"></i>
                        <span>Control</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Rulebase.jsp">
                                <span>Rule Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="PolicyBase.jsp">
                                <span>Policy Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="Simulate.jsp">
                                <span>Simulate</span>
                            </a>
                        </li>
                    </ul>

                </li>


               
                <li>
                    <a href="Flowrector.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Flow Rector</span>
                    </a>


                </li>
            </ul>


        </div>	
	<div class="main-content">
		
<div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">

                    <ul class="user-info pull-left pull-none-xsm">
                    </ul>
                    <ul class="user-info pull-left pull-right-xs pull-none-xsm">

                        <!-- Raw Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-attention"></i>
                                <span class="badge badge-info">6</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p class="small">
                                        <a href="#" class="pull-right">Mark all Read</a>
                                        You have <strong>3</strong> new notifications.
                                    </p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="unread notification-success">
                                            <a href="#">
                                                <i class="entypo-user-add pull-right"></i>

                                                <span class="line">
                                                    <strong>New user registered</strong>
                                                </span>

                                                <span class="line small">30 seconds ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="unread notification-secondary">
                                            <a href="#">
                                                <i class="entypo-heart pull-right"></i>

                                                <span class="line">
                                                    <strong>Someone special liked this</strong>
                                                </span>

                                                <span class="line small">2 minutes ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-primary">
                                            <a href="#">
                                                <i class="entypo-user pull-right"></i>

                                                <span class="line">
                                                    <strong>Privacy settings have been changed</strong>
                                                </span>

                                                <span class="line small">3 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-danger">
                                            <a href="#">
                                                <i class="entypo-cancel-circled pull-right"></i>

                                                <span class="line">John cancelled the event
                                                </span>

                                                <span class="line small">9 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-info">
                                            <a href="#">
                                                <i class="entypo-info pull-right"></i>

                                                <span class="line">The server is status is stable
                                                </span>

                                                <span class="line small">yesterday at 10:30am
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-warning">
                                            <a href="#">
                                                <i class="entypo-rss pull-right"></i>

                                                <span class="line">New comments waiting approval
                                                </span>

                                                <span class="line small">last week
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">View all notifications</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Message Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-mail"></i>
                                <span class="badge badge-secondary">10</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-1.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">
                                                    <strong>Luc Chartier</strong>
                                                    - yesterday
                                                </span>

                                                <span class="line desc small">This ain’t our first item, it is the best of the rest.
                                                </span>
                                            </a>
                                        </li>

                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-2.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">
                                                    <strong>Salma Nyberg</strong>
                                                    - 2 days ago
                                                </span>

                                                <span class="line desc small">Oh he decisively impression attachment friendship so if everything. 
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-3.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">Hayden Cartwright
					- a week ago
                                                </span>

                                                <span class="line desc small">Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-4.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">Sandra Eberhardt
					- 16 days ago
                                                </span>

                                                <span class="line desc small">On so attention necessary at by provision otherwise existence direction.
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="mailbox.html">All Messages</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Task Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-list"></i>
                                <span class="badge badge-warning">1</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p>You have 6 pending tasks</p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Procurement</span>
                                                    <span class="percent">27%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 27%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">27% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">App Development</span>
                                                    <span class="percent">83%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 83%;" class="progress-bar progress-bar-danger">
                                                        <span class="sr-only">83% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">HTML Slicing</span>
                                                    <span class="percent">91%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 91%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">91% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Database Repair</span>
                                                    <span class="percent">12%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 12%;" class="progress-bar progress-bar-warning">
                                                        <span class="sr-only">12% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Backup Create Progress</span>
                                                    <span class="percent">54%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 54%;" class="progress-bar progress-bar-info">
                                                        <span class="sr-only">54% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Upgrade Progress</span>
                                                    <span class="percent">17%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 17%;" class="progress-bar progress-bar-important">
                                                        <span class="sr-only">17% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">See all tasks</a>
                                </li>
                            </ul>

                        </li>

                    </ul>

                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">


                        <li>
                            <strong>Ecode Networks</strong> <i class="entypo-users"></i>

                        </li>

                        <li class="sep"></li>

                        <li>
                            <a href="setout.jsp">Log Out <i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>




                </div>
            </div>
<nav class="navbar navbar-inverse" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Canvas</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">File<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Canvas.jsp" ><strong>New Canvas</strong></a></li>
                                <li><a href="#modal-1" onclick="savecanvas1();"><strong>Save Canvas</strong></a>
                                </li>
                                <li><a href="#modal-2" onclick="loadcanvas1();"><strong>Load Canvas</strong></a>
                                </li>
                                <li><a href="#modal-3" onclick="deletecanvas1();"><strong>Delete Canvas</strong></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Actions<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#modal-4" onClick="addswitch1();"><strong>Add Switch</strong></a></li>
                                <li><a href="#modal-5" onclick="addhost1();"><strong>Add Host</strong></a>
                                </li>
                                <li><a href="#modal-6" onclick="addlink1(),populate();;"><strong>Add Links</strong></a>
                                </li>
                                <li><a href="#modal-7" onclick="addpolicy1(),policypopulate();"><strong>Add Policy</strong></a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Create<b class="caret"></b> </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:createNetwork();" onclick="createNetwork();"><strong>Create SNT</strong></a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <script>
			function windowcl()
			{
			setTimeout(function(){window.location="Viewer.jsp"; },89000);
			}
				function savecanvas1() {
					jQuery('#modal-1').modal('show');

				}
				function loadcanvas1() {
					jQuery('#modal-2').modal('show');

				}
				function deletecanvas1() {
					jQuery('#modal-3').modal('show');
				}

				function addswitch1() {
					
					
					jQuery('#modal-4').modal('show');

				}
				function addhost1() {
					jQuery('#modal-5').modal('show');

				}
				function addlink1() {
					jQuery('#modal-6').modal('show');

				}
				function addpolicy1() {
					jQuery('#modal-7').modal('show');

				}
			</script>
 
       <script src="canvas/kinetic-v5.0.2.min.js"></script>
       
					<script src="canvas/jstorage.js"></script>     
<div class="container-fluid">
                <div class="col-lg-12" style="background-color: aliceblue; width: auto; height: 600px;">
                    <div id="container">
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
            
            <form name="scripter" id="src" method="POST" action="scriptPusher.jsp">
<input type="hidden" value="" name="script" id="here">
<input type="hidden" value="" name="policy" id="polc">
</form>
<footer class="main">
	
		
	Copyrights &copy; 2016 <strong>Ecode Networks</strong>
	
</footer>	</div>

	</div>

<!-- Modal 1 (Basic)-->
<form name="savemultipletp" id="formsavetp" action="savetp.jsp" method="POST">
<div class="modal fade" id="modal-1">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Save Canvas</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							<label for="field-1" class="control-label">Name</label>
							
							<input type="text" class="form-control" id="Text2" name="tpname" placeholder="Enter Canvas Name">
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-info" onclick="tojson();">Save</button>
                
			</div>
		</div>
	</div>
	<input type="hidden" id="switchdet" name="switchdet" /> <input
						type="hidden" id="hostdet" name="hostdet" /> <input type="hidden"
						id="linkdet" name="linkdet" />
</div>
</form>
<script>
				function tojson() {
					//alert("Saving canvas");
					document.getElementById("switchdet").value = switchdb;
					document.getElementById("hostdet").value = hostdb;
					document.getElementById("linkdet").value = linkdetails;
				}
			</script>
<!-- Modal 2 (Custom Width)-->
<form name="fetchcanvasdb" id="fetchcanvasdb"
				action="loadingcanvas.jsp" method="POST">
<div class="modal fade" id="modal-2">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Load Canvas</h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Name</label>
  <select class="form-control" name="canvname" id="Select1">
    <%
								int a=0;
							%>
							<%
								List<String> cnamelist=fetchtopo.fetchcanvasname();
							%>
							<%
								for(;a<cnamelist.size();a++){
							%>
							<option value="<%=cnamelist.get(a).toString()%>"><%=cnamelist.get(a).toString()%>
							</option>
							<%
								}
							%>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-info">Load</button>
               
			</div>
		</div>
	</div>
</div>
</form>
<!-- Modal 3 (Custom Width)-->
<form name="delcanvasdb" id="delcanvasdb" action="deletecanvas.jsp"
						method="POST">
<div class="modal fade" id="modal-3">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><strong>Delete Canvas </strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						
						<div class="form-group">
							 <label for="sel1">Name</label>
  <select class="form-control" name="canvnamedel" id="canvnamedel">
    <%
								int b=0;
							%>
							<%
								List<String> cnamelist2=fetchtopo.fetchcanvasname();
							%>
							<%
								for(;b<cnamelist2.size();b++){
							%>
							<option value="<%=cnamelist2.get(b).toString()%>"><%=cnamelist2.get(b).toString()%>
							</option>
							<%
								}
							%>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-info">Delete</button>
                
			</div>
		</div>
	</div>
</div>
</form>
<!-- Modal 4 -->
<div class="modal fade" id="modal-4">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h4 class="modal-title"><strong>Add Switch</strong></h4>
			</div>
			
			<div class="modal-body">
				<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1"> Switch Type </label>
  <select class="form-control" name="switchtypnm" id="switchtypid">
  <option  value="0">---select------</option>
    <option value="100">Default Switch</option>
    <option value="12900">HPFB_12900</option>
    <option value="6800">HSR 6800</option>
    <option value="5900">FF5900</option>
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
                <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Switch Name </label>
  <input type="text" class="form-control" id="switchname" placeholder="Enter Switch Name">
                            			</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			<div id="swtname"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" onclick="actor_name(),callswtpopup();">Add More</button>
                <button type="button" class="btn btn-info" onclick="actor_name(),closeswitch();">Add & Close</button>
			</div>
			
			<script>
						
						function callswtpopup() {
							document.getElementById("switchtypid").value = "0";
							document.getElementById("switchname").value = "";
							document.getElementById("swtname").innerHTML = "<input name='setswtname' id='setswtid' data-mini='true' required='true' type='hidden' />";
							jQuery('#modal-4').modal('show');
						}
						function closeswitch() {
							jQuery('#modal-4').modal('hide');
							document.getElementById("switchtypid").value = "0";
							document.getElementById("switchname").value = "";
						}
						
						function close() {
							$("#openModal").hide();
						}
					</script>
			
		</div>
	</div>
</div>

<!-- Modal 4 (Confirm)-->
<div class="modal fade" id="modal-5">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<h4 class="modal-title">Add Host</h4>
			</div>
			
			<div class="modal-body">
			<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Host Name </label>
  <input type="text" class="form-control" id="hostname" placeholder="Enter Host Name">
                            			</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" onclick="draw_uc();resethost()">Add More</button>
                <button type="button" class="btn btn-info" onclick="draw_uc();closepopup1() ">Add & Close</button>
			</div>
		</div>
		<script>
						var increment = 1;
						function getname() {
							var host = document.getElementById("hostname").value;
							if (host != "") {
								var labelID = "lblhost" + increment;
								//alert(labelID);
								showhost(labelID);
								$("#" + labelID).text(host);
								increment++;
							}

							// $('#Text1').val("");
						}

						function resethost() {
							document.getElementById("hostname").value = "";
							jQuery('#modal-5').modal('show');

						}

						function showhost(labelID) {
							$("#dynamicInput")
									.append(
											"<div class='draggable'><img src='assets/images/Host.png'/><br/><label id='"+labelID+"'></label></div>");
							$('.draggable').draggable();
						}
						function closepopup1() {
							jQuery('#modal-5').modal('hide');
							document.getElementById("hostname").value = "";
						}
					</script>
			
	</div>
</div>


<!-- Modal 5 (Long Modal)-->
<div class="modal fade" id="modal-7">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add Policy</h4>
			</div>
			
			<div class="modal-body">
			<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Protocol</label>
  <select class="form-control" id="traffic">
    
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
            <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Source</label>
  <select class="form-control" id="host1">
    
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
            <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							 <label for="sel1">Destination</label>
  <select class="form-control" id="host2">
    
  </select>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" onClick="addpolicy(),closepolicy();">Add</button>
                
			</div>
		</div>
	</div>
</div>


<!-- Modal 6 (Long Modal)-->
<div class="modal fade" id="modal-6">
	<div class="modal-dialog">
		<div class="modal-content">
			
			<div class="modal-header">
				
				<h4 class="modal-title"><strong>Add Links</strong></h4>
			</div>
			
			<div class="modal-body">
			       <div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							<label for="field-1" class="control-label">Source</label>
							<select class="form-control" id="srcList" onChange="portcall1();">
							<option value="0">---select------</option>
							</select>
							<div id="srcport" style="display: none"></div>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
					<div class="row">
                    <div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="form-group">
							<label for="field-1" class="control-label">Destination</label>
							<select class="form-control" id="dstList" onChange="portcall2();">
							<option  value="0">---select------</option>
							</select>
						<div id="destport" style="display: none"></div>
						</div>	
						
					</div>
					<div class="col-md-2"></div>
				</div>
				<script>
				var tosavedb = "";
				var getswtname1 = "";
				var getswtname2 = "";
				function savenow() {
					tosavedb = tosavedb
							+ " "
							+ document.getElementById("srcList").value
							+ "-"
							+ document.getElementById("" + getswtname1 + "").value
							+ ":"
							+ document.getElementById("dstList").value
							+ "-"
							+ document.getElementById("" + getswtname2 + "").value;
					//alert(tosavedb);
				}
			</script>
			<script type="text/javascript">
				function portcall1() {
					//alert("welcome");
					getswtname1 = document.getElementById("srcList").value;
					var portnum = $.jStorage.get(getswtname1 + "_ports");

					var portlist = "<label>Source Port: </label><select   id="+getswtname1+" name="+getswtname1+"><option value='0'>-choose-</option>";
					for (var a = 1; a < (portnum + 1); a++) {
						portlist = portlist + "<option val="+a+">" + a
								+ "</option>";
					}
					portlist = portlist + "</select>";
					document.getElementById("srcport").innerHTML = portlist;
					//alert(portlist);
				}

				function portcall2() {
					getswtname2 = document.getElementById("dstList").value;
					var portnum = $.jStorage.get(getswtname2 + "_ports");
					var portlist = "<label>Destination Port: </label><select  id="+getswtname2+" name="+getswtname2+"><option value='0'>-choose-</option>";
					for (var b = 1; b < (portnum + 1); b++) {
						portlist = portlist + "<option val="+b+">" + b
								+ "</option>";
					}
					portlist = portlist + "</select>";
					document.getElementById("destport").innerHTML = portlist;

				}

				function closelinks() {
					//alert("close links");
					jQuery('#modal-6').modal('hide');
				}
				function savenow(){
					tosavedb=tosavedb+" "+document.getElementById("srcList").value+"-"+document.getElementById(""+getswtname1+"").value+":"+document.getElementById("dstList").value+"-"+document.getElementById(""+getswtname2+"").value;
					//alert(tosavedb);
				}
			</script>
			
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info" onclick="addLink(),savenow();">Add</button>
                <button type="button" class="btn btn-info" onclick="addLink(),closelinks(),savenow();">Add & Close</button>
			</div>
		</div>
	</div>
</div>
<script>
				var inc = 1;
				var linename = new Array();
				var srcarr = new Array();
				var dstarr = new Array();
				var alp = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
				var script = "";
				var newscript = "";
				var layer_lines = new Kinetic.Layer();
				var layer = new Kinetic.Layer();
				var stage = new Kinetic.Stage({
					container : 'container',
					width : 1070,
					height : 530
				});

				var i = 0;
				var j = 0;
				var k = 1;
				var h = 0;
				var o = 0;
				var a1x;
				var a1y;
				var a2x;
				var a2y;
				var re = true;
				var acidno = 0;
				var sflag = false;
				var src;
				var dest;
				var switchdb = "";
				var hostdb = "";
				var linksdb = "";
				var linkdetails = "";
				var z = 0;
				var y = 0;
				var switchimglink = "";
				var swtnameinput = "";
				var hostnameinput = "";
				var namearr = [];
				var scount = 0;
				var hcount = 0;
				function actor_name() {
					swtnameinput = document.getElementById("switchname").value;
					//switchimglink = $(".form-control option:selected").val();
					switchimglink=document.getElementById("switchtypid").value;
					//alert(switchimglink);
				

					var name = swtnameinput;

					switchdb = switchdb + " " + name;
					srcarr[j] = name;

					script = script + "  " + "Switch" + alp.charAt(j) + "=" + k
							+ "\n";
					newscript = newscript + "  " + "Switch" + alp.charAt(j)
							+ "=" + "self.addSwitch( 's" + k + "' )" + "\n";
					$.jStorage.set(name + "_py", "Switch" + alp.charAt(j));
					script = script + "  " + "self.add_node(" + "Switch"
							+ alp.charAt(j) + ",Node(is_switch=True))" + "\n";
					scount = scount + 1;
					$.jStorage.set(name + "_num", scount);
					k = k + 1;
					j = j + 1;

					var imageObj = new Image();
					imageObj.onload = function() {
						var actor;
						if (switchimglink == 100) {
							actor = new Kinetic.Image({

								width : 30,
								height : 30,
								image : imageObj
							});
						}
						if (switchimglink == 12900) {
							actor = new Kinetic.Image({

								width : 60,
								height : 115,
								image : imageObj
							});
						}
						if (switchimglink == 6800) {
							actor = new Kinetic.Image({

								width : 115,
								height : 88,
								image : imageObj
							});
						}
						if (switchimglink == 5900) {
							actor = new Kinetic.Image({

								width : 80,
								height : 34,
								image : imageObj
							});
						}

						actor.on('mouseover', function() {
							document.body.style.cursor = 'pointer';
						});

						actor.on('mouseout', function() {
							document.body.style.cursor = 'default';
						});

						var label_text = new Kinetic.Text({
							x : actor.getX(),
							y : actor.getY() + actor.getHeight(),
							text : name,
							fontSize : 15,
							fontFamily : 'Calibri',
							fill : '#ooo'
						});

						label_text.setOffset({
							x : -(actor.getWidth() - label_text.getWidth()) / 2
						});
						if (switchimglink == 100) {
							var group = new Kinetic.Group({
								id : '_ac' + acidno,
								x : 100,
								y : 100,
								draggable : true
							});
						}
						if (switchimglink == 12900) {
							var group = new Kinetic.Group({
								id : '_ac' + acidno,
								x : 100,
								y : 100,
								draggable : true
							});
						}
						if (switchimglink == 6800) {
							var group = new Kinetic.Group({
								id : '_ac' + acidno,
								x : 300,
								y : 100,
								draggable : true
							});
						}
						if (switchimglink == 5900) {
							var group = new Kinetic.Group({
								id : '_ac' + acidno,
								x : 500,
								y : 100,
								draggable : true
							});
						}

						group.add(actor);
						group.add(label_text);

						var pos_s = group.getPosition();
						$.jStorage.set(name + "_x", pos_s.x + actor.getWidth()
								/ 2);
						$.jStorage.set(name + "_y", pos_s.y + actor.getHeight()
								/ 2);

						// add the shape to the layer
						layer.add(group);

						// add the layer to the stage
						stage.add(layer);

						group.on('click tap', function() {

							var xx = $.jStorage.get(name + "_x");
							var yy = $.jStorage.get(name + "_y");

							var tooltip = new Kinetic.Label({
								x : xx,
								y : yy,
								opacity : 0.75,
								draggable : true
							});
							// alert(namearr);
							var fruit = null;
							for ( var prop in namearr) {

								if (namearr[prop].switch_name == name) {

									fruit = prop;
									break;
								}
							}
							var ind = fruit;
							tooltip.add(new Kinetic.Tag({
								fill : 'black',
								pointerDirection : 'down',
								pointerWidth : 10,
								pointerHeight : 10,
								lineJoin : 'round',
								shadowColor : 'black',
								shadowBlur : 10,
								shadowOffset : {
									x : 10,
									y : 20
								},
								shadowOpacity : 0.5
							}));

							tooltip.add(new Kinetic.Text({
								text : "source host:" + namearr[ind].src_host
										+ "\n" + "Destination host:"
										+ namearr[ind].dst_host + "\n"
										+ "action:" + namearr[ind].action,
								fontFamily : 'Calibri',
								fontSize : 18,
								padding : 5,
								fill : 'white'
							}));

							layer.add(tooltip);
							layer.draw();
						});
						group.on('dragend', function() {

							var pos_k = group.getPosition();
							$.jStorage.set(name + "_x", pos_k.x
									+ actor.getWidth() / 2);
							$.jStorage.set(name + "_y", pos_k.y
									+ actor.getHeight() / 2);
							switchdb = switchdb + " " + name + "-" + "x"
									+ pos_k.x + ":y" + pos_k.y;

						});
						group.on('dragstart dragmove', function() {

							var pos_k = group.getPosition();
							$.jStorage.set(name + "_x", pos_k.x
									+ actor.getWidth() / 2);
							$.jStorage.set(name + "_y", pos_k.y
									+ actor.getHeight() / 2);

							var indices = $.jStorage.index();
							var indice = String(indices);
							var matche = indice
									.match(/(\w+)(\s+)?(\w+)_linkname(\w+)/g);
							var matched = new Array();
							matched = String(matche).split(",");
							// alert(matched);
							var sources = new Array();
							var dests = new Array();
							var f = -2;
							for (var h = 0; h < matched.length / 2; h++) {
								sources[h] = matched[f + 2];
								dests[h] = matched[f + 3];
								f = f + 2;
							}
							var indexfindsrc = "";
							var indexfinddst = "";
							var indices;
							var srcs = new Array();
							var dsts = new Array();
							for (var g = 0; g < sources.length; g++) {
								srcs[g] = sources[g].substring(0,
										sources[g].length - 1);

								if (srcs[g] == name + "_linkname") {
									indexfindsrc = indexfindsrc + g + " ";
								}

							}

							for (var w = 0; w < dests.length; w++) {
								dsts[w] = dests[w].substring(0,
										dests[w].length - 1);

								if (dsts[w] == name + "_linkname") {
									indexfinddst = indexfinddst + w + " ";
								}
								//console.log("switches:"+ dests.length);
							}

							var lines = layer.get('Line');

							var indexfind = indexfindsrc + indexfinddst;
							var kill = new Array();
							kill = indexfind.split(" ");

							// var kill2 = new Array();
							if (srcs.indexOf(name + "_linkname") >= 0
									|| dsts.indexOf(name + "_linkname") >= 0) {

								for (var t = 0; t < kill.length; t++) {

									var sds = $.jStorage.get($.jStorage
											.get(sources[kill[t]]));
									var sd = sds.split(",");
									//alert($.jStorage.get(sd[1]+"_x"),$.jStorage.get(sd[1]+"_y"));
									var ind;
									if (name == sd[0]) {
										ind = 1;
									} else {
										ind = 0;
									}
									lines[kill[t]].setPoints([pos_k.x+actor.getWidth()/2,pos_k.y+actor.getHeight()/2,$.jStorage.get(sd[ind]+"_x"),$.jStorage.get(sd[ind]+"_y")]);
									layer.draw();
								}
							}

						});

					};

						
					if (switchimglink == 100) {
						imageObj.src = 'switchimg/switch1.png';
						switchdb = switchdb + "(100)";
					}
					if (switchimglink == 12900) {
						imageObj.src = 'switchimg/HPFB_12900.png';
						switchdb = switchdb + "(12900)";
					}
					if (switchimglink == 6800) {
						imageObj.src = 'switchimg/HSR6800.png';
						switchdb = switchdb + "(6800)";
					}
					if (switchimglink == 5900) {
						imageObj.src = 'switchimg/FF5900.png';
						switchdb = switchdb + "(5900)";
					}
					acidno = acidno + 1;

				}


				var ucx = 100;

				function draw_uc() {
					hostnameinput = document.getElementById("hostname").value;
					var uc = hostnameinput;
					hostdb = hostdb + " " + uc;

					dstarr[o] = uc;
					o = o + 1;
					script = script + "  " + "Host" + alp.charAt(h) + "=" + k
							+ "\n";
					newscript = newscript + "  " + "Host" + alp.charAt(h) + "="
							+ "self.addHost( 'h" + k + "' )" + "\n";

					$.jStorage.set(uc + "_py", "Host" + alp.charAt(h));
					hcount = hcount + 1;
					$.jStorage.set(uc + "_num", hcount);
					script = script + "  " + "self.add_node(" + "Host"
							+ alp.charAt(h) + ",Node(is_switch=False))" + "\n";
					k = k + 1;
					h = h + 1;
					var imageObj = new Image();
					imageObj.onload = function() {
						var host = new Kinetic.Image({

							width : 30,
							height : 32,
							image : imageObj
						});

						host.on('mouseover', function() {
							document.body.style.cursor = 'pointer';
						});

						host.on('mouseout', function() {
							document.body.style.cursor = 'default';
						});

						var label_text = new Kinetic.Text({
							x : host.getX(),
							y : host.getY() + host.getHeight(),
							text : uc,
							fontSize : 15,
							fontFamily : 'Calibri',
							fill : '#ooo'
						});

						label_text.setOffset({
							x : -(host.getWidth() - label_text.getWidth()) / 2
						});
						if (ucx == 1000) {
							ucx = 100;
						}
						var group_uc = new Kinetic.Group({
							id : '_ac' + acidno,
							x : ucx,
							y : 300,
							draggable : true
						});
						ucx = ucx + 100;
						group_uc.on('click tap', function(e) {
							if (e.which == 3) {
								document.body.style.cursor = 'pointer';
								var corda1 = this.getPosition();
								a1x = (corda1.x) + host.getWidth() / 2;
								a1y = (corda1.y) + host.getHeight() / 2;
								re = true;

							}
						});

						group_uc.add(host);
						group_uc.add(label_text);
						
						var pos_s = group_uc.getPosition();
						$.jStorage
								.set(uc + "_x", pos_s.x + host.getWidth() / 2);
						$.jStorage.set(uc + "_y", pos_s.y + host.getHeight()
								/ 2);

						// add the shape to the layer
						layer.add(group_uc);

						// add the layer to the stage
						stage.add(layer);
						group_uc.on('dragend', function() {

							var pos_k = group_uc.getPosition();
							$.jStorage.set(uc + "_x", pos_k.x + host.getWidth()
									/ 2);
							$.jStorage.set(uc + "_y", pos_k.y
									+ host.getHeight() / 2);

							hostdb = hostdb + " " + uc + "-" + "x" + pos_k.x
									+ ":y" + pos_k.y;
						});
						group_uc.on('dragstart dragmove', function() {

							var pos_k = group_uc.getPosition();
							$.jStorage.set(uc + "_x", pos_k.x + host.getWidth()
									/ 2);
							$.jStorage.set(uc + "_y", pos_k.y
									+ host.getHeight() / 2);

							var indices = $.jStorage.index();
							var indice = String(indices);
							var matche = indice
									.match(/(\w+)(\s+)?(\w+)_linkname(\w+)/g);
							var matched = new Array();
							
							matched = String(matche).split(",");
							//alert(matched);
							var sources = new Array();
							var dests = new Array();
							var f = -2;
							for (var h = 0; h < matched.length / 2; h++) {
								sources[h] = matched[f + 2];
								dests[h] = matched[f + 3];
								f = f + 2;
							}
							var indexfindsrc = "";
							var indexfinddst = "";
							var indices;
							var srcs = new Array();
							var dsts = new Array();
							for (var g = 0; g < sources.length; g++) {
								srcs[g] = sources[g].substring(0,
										sources[g].length - 1);

								if (srcs[g] == uc + "_linkname") {
									indexfindsrc = indexfindsrc + g + " ";
								}

							}

							for (var w = 0; w < dests.length; w++) {
								dsts[w] = dests[w].substring(0,
										dests[w].length - 1);

								if (dsts[w] == uc + "_linkname") {
									indexfinddst = indexfinddst + w + " ";
								}
								//console.log("switches:"+ dests.length);
							}

							var lines = layer.get('Line');

							var indexfind = indexfindsrc + indexfinddst;
							var kill = new Array();
							kill = indexfind.split(" ");

							// var kill2 = new Array();
							if (srcs.indexOf(uc + "_linkname") >= 0
									|| dsts.indexOf(uc + "_linkname") >= 0) {

								for (var t = 0; t < kill.length; t++) {

									var sds = $.jStorage.get($.jStorage
											.get(sources[kill[t]]));
									var sd = sds.split(",");
									//alert($.jStorage.get(sd[1]+"_x"),$.jStorage.get(sd[1]+"_y"));
									var ind;
									if (name == sd[0]) {
										ind = 1;
									} else {
										ind = 0;
									}
									lines[kill[t]].setPoints([
											pos_k.x + host.getWidth() / 2,
											pos_k.y + host.getHeight() / 2,
											$.jStorage.get(sd[ind] + "_x"),
											$.jStorage.get(sd[ind] + "_y") ]);
									layer.draw();
								}
							}
						});

					};
					imageObj.src = 'img/Host.png';
					acidno = acidno + 1;

				}

				function SwitchName() {
					var Switch1 = document.getElementById("switchname").value;
					if (Switch1 != "") {
						var SwitchID = "lblSwitch" + inc;
						//alert(SwitchID);
						//alert(Switch1);
						$("#" + SwitchID).text(Switch1);
						inc++;
					}

					// $('#Text1').val("");
				}

				function reset() {
					document.getElementById("switchname").value = "";
				}
				var counter = 1;

				function addInput(divName) {
					if (document.getElementById('drop').selectedIndex == 1) {

						var newdiv = document.createElement('div');
						newdiv.innerHTML = "<div class='draggable' style='text-align:center'><img src='assets/images/switch1.png'  /><br/><label id='" +SwitchID + "'></label></div>";
						document.getElementById(divName).appendChild(newdiv);
						counter++;
						$('.draggable').draggable();
					}

					if (document.getElementById('drop').selectedIndex == 2) {

						var newdiv = document.createElement('div');
						newdiv.innerHTML = "<div class='draggable' style='text-align:center'><img src='assets/images/HPFB_12900.png' style='height:80px;'/><br/><label id='"+SwitchID+"'></label></div>";
						document.getElementById(divName).appendChild(newdiv);
						$('.draggable').draggable();
						counter++;
					}
					if (document.getElementById('drop').selectedIndex == 3) {

						var newdiv = document.createElement('div');
						newdiv.innerHTML = " <div class='draggable' style='text-align:center'><img src='assets/images/HSR6800.png'  style='height:80px;'/><br/><label id='" + SwitchID + "></label></div>";
						document.getElementById(divName).appendChild(newdiv);
						$('.draggable').draggable();
						counter++;
					}
					if (document.getElementById('drop').selectedIndex == 4) {
						var newdiv = document.createElement('div');
						newdiv.innerHTML = " <div class='draggable' style='text-align:center'><img src='assets/images/FF5900.png' style='height:80px;width:60px;'/><br/><label id='" + SwitchID + "></label></div>";
						document.getElementById(divName).appendChild(newdiv);
						$('.draggable').draggable();
						counter++;

					}
				}
				function populate() {
					document.getElementById("srcList").style.visibility = "visible";
					document.getElementById("dstList").style.visibility = "visible";

					var sel1 = document.getElementById('srcList');
					for (var m = 0; m < srcarr.length; m++) {
						var opt = document.createElement('option');
						opt.innerHTML = srcarr[m];
						opt.value = srcarr[m];
						sel1.appendChild(opt);
					}

					var sel2 = document.getElementById('dstList');
					for (var n = 0; n < dstarr.length; n++) {
						var opt = document.createElement('option');
						opt.innerHTML = dstarr[n];
						opt.value = dstarr[n];
						sel2.appendChild(opt);
					}

					for (var l = 0; l < srcarr.length; l++) {
						var opt = document.createElement('option');
						opt.innerHTML = srcarr[l];
						opt.value = srcarr[l];
						sel2.appendChild(opt);
					}
				}
				function addLink() {
					// alert("hello");
					var srcselected = document.getElementById("srcList").options[srcList.selectedIndex].text;
					;
					var dstselected = document.getElementById("dstList").options[dstList.selectedIndex].text;
					;
					a1x = $.jStorage.get(srcselected + "_x");
					a1y = $.jStorage.get(srcselected + "_y");
					a2x = $.jStorage.get(dstselected + "_x");
					a2y = $.jStorage.get(dstselected + "_y");
					linkdetails = linkdetails + " " + "src=" + srcselected
							+ "dst=" + dstselected + "";
					var src = $.jStorage.get(srcselected + "_py");
					var dst = $.jStorage.get(dstselected + "_py");

					script = script + "  " + "self.add_edge(" + src + "," + dst
							+ ")" + "\n";
					newscript = newscript + "  " + "self.addLink(" + src + ","
							+ dst + ")" + "\n";
					if (dstarr.indexOf(dstselected) >= 0) {

						linename[i] = new Kinetic.Line({
							points : [ a1x, a1y, a2x, a2y ],
							name : '_ac' + i,
							stroke : 'black',
							strokeWidth : 2,
							lineCap : 'round',
							lineJoin : 'bevel'
						});
						layer.add(linename[i]);
					} else {

						linename[i] = new Kinetic.Line({
							points : [ a1x, a1y, a2x, a2y ],
							name : '_ac' + i,
							stroke : 'grey',
							strokeWidth : 2,
							lineCap : 'round',
							lineJoin : 'bevel'
						});
						layer.add(linename[i]);
					}

					stage.add(layer);

					$.jStorage.set("linename[" + i + "]", srcselected + ","
							+ dstselected);

					$.jStorage.set(srcselected + "_link" + i, linename[i]);
					$.jStorage.set(srcselected + "_linkname" + i, "linename["
							+ i + "]");

					if ($.jStorage.get(srcselected + "_size") != null) {

						$.jStorage.set(srcselected + "_size", $.jStorage
								.get(srcselected + "_size") + 1);

					} else {

						$.jStorage.set(srcselected + "_size", 1);
					}

					//alert($.jStorage.get(srcselected+"_links"));

					$.jStorage.set(dstselected + "_link" + i, linename[i]);

					$.jStorage.set(dstselected + "_linkname" + i, "linename["
							+ i + "]");

					if ($.jStorage.get(dstselected + "_size") != null) {

						$.jStorage.set(dstselected + "_size", $.jStorage
								.get(dstselected + "_size") + 1);

					} else {

						$.jStorage.set(dstselected + "_size", 1);
					}

					i = i + 1;

				}


				function addpolicy() {
					// alert("hello");
					var srcselected = document.getElementById("host1").options[host1.selectedIndex].text;
					var dstselected = document.getElementById("host2").options[host2.selectedIndex].text;
					var protocol = document.getElementById("traffic").options[host2.selectedIndex].text;
					a1x = $.jStorage.get(srcselected + "_x");
					a1y = $.jStorage.get(srcselected + "_y");
					a2x = $.jStorage.get(dstselected + "_x");
					a2y = $.jStorage.get(dstselected + "_y");

					//alert(srcselected+" "+a1x);
					a3x = (a1x + a2x) / 2 + 40;
					a3y = (a1y + a2y) / 2 + 40;
					linename[i] = new Kinetic.Line({
						points : [ a1x, a1y, a3x, a3y, a2x, a2y ],
						name : '_ac' + i,
						stroke : 'red',
						strokeWidth : 3,
						lineCap : 'round',
						lineJoin : 'round',
						dash : [ 29, 20, 0.001, 20 ]
					});
					layer.add(linename[i]);

					stage.add(layer);
					$.jStorage.set("linename[" + i + "]", srcselected + ","
							+ dstselected);

					$.jStorage.set(srcselected + "_link" + i, linename[i]);
					$.jStorage.set(srcselected + "_linkname" + i, "linename["
							+ i + "]");

					if ($.jStorage.get(srcselected + "_size") != null) {
						$.jStorage.set(srcselected + "_size", $.jStorage
								.get(srcselected + "_size") + 1);
					} else {
						$.jStorage.set(srcselected + "_size", 1);
					}
					//alert($.jStorage.get(srcselected+"_links"));

					$.jStorage.set(dstselected + "_link" + i, linename[i]);

					$.jStorage.set(dstselected + "_linkname" + i, "linename["
							+ i + "]");

					if ($.jStorage.get(dstselected + "_size") != null) {

						$.jStorage.set(dstselected + "_size", $.jStorage
								.get(dstselected + "_size") + 1);

					} else {

						$.jStorage.set(dstselected + "_size", 1);
					}
					var indices = $.jStorage.index();
					var str = String(indices);
					var a = srcselected;
					var filter = new RegExp(a + "_linkname" + "(\\w+)")
					var res = str.match(filter);
					var des = $.jStorage.get(res[0]);

					var jkl = ($.jStorage.get(des));
					var o = new Array();
					o = jkl.split(",");
					// alert(des+" "+jkl);
					var ccount = $.jStorage.get(o[0] + "_num");
					var hcount1 = $.jStorage.get(srcselected + "_num");
					var hcount2 = $.jStorage.get(dstselected + "_num");
					//alert("{switch_name:"+o[0]+",src_host:"+ srcselected+", dst_host:"+ dstselected+", action:"+ "\'deny\' }");
					(namearr).push({
						switch_name : o[0],
						switch_number : ccount,
						src_host : srcselected,
						src_count : hcount1,
						dst_host : dstselected,
						dst_count : hcount2,
						protocol : protocol,
						action : 'deny'
					});

					i = i + 1;

				}

				function policypopulate() {
					document.getElementById("host1").style.visibility = "visible";
					document.getElementById("host2").style.visibility = "visible";
					document.getElementById("traffic").style.visibility = "visible";

					var sel1 = document.getElementById('host1');
					for (var m = 0; m < dstarr.length; m++) {
						var opt = document.createElement('option');
						opt.innerHTML = dstarr[m];
						opt.value = dstarr[m];
						sel1.appendChild(opt);
					}

					var sel2 = document.getElementById('host2');
					for (var n = 0; n < dstarr.length; n++) {
						var opt = document.createElement('option');
						opt.innerHTML = dstarr[n];
						opt.value = dstarr[n];
						sel2.appendChild(opt);
					}
					var traarr = [ "tcp", "icmp", "udp" ];
					var sel3 = document.getElementById('traffic');
					for (var n = 0; n < traarr.length; n++) {
						var opt = document.createElement('option');
						opt.innerHTML = traarr[n];
						opt.value = traarr[n];
						sel3.appendChild(opt);
					}

				}

				function addpolicy() {
					// alert("hello");
					var srcselected = document.getElementById("host1").options[host1.selectedIndex].text;
					var dstselected = document.getElementById("host2").options[host2.selectedIndex].text;
					var protocol = document.getElementById("traffic").options[host2.selectedIndex].text;
					a1x = $.jStorage.get(srcselected + "_x");
					a1y = $.jStorage.get(srcselected + "_y");
					a2x = $.jStorage.get(dstselected + "_x");
					a2y = $.jStorage.get(dstselected + "_y");

					//alert(srcselected+" "+a1x);
					a3x = (a1x + a2x) / 2 + 40;
					a3y = (a1y + a2y) / 2 + 40;
					linename[i] = new Kinetic.Line({
						points : [ a1x, a1y, a3x, a3y, a2x, a2y ],
						name : '_ac' + i,
						stroke : 'red',
						strokeWidth : 3,
						lineCap : 'round',
						lineJoin : 'round',
						dash : [ 29, 20, 0.001, 20 ]
					});
					layer.add(linename[i]);

					stage.add(layer);
					$.jStorage.set("linename[" + i + "]", srcselected + ","
							+ dstselected);

					$.jStorage.set(srcselected + "_link" + i, linename[i]);
					$.jStorage.set(srcselected + "_linkname" + i, "linename["
							+ i + "]");

					if ($.jStorage.get(srcselected + "_size") != null) {
						$.jStorage.set(srcselected + "_size", $.jStorage
								.get(srcselected + "_size") + 1);
					} else {
						$.jStorage.set(srcselected + "_size", 1);
					}
					//alert($.jStorage.get(srcselected+"_links"));

					$.jStorage.set(dstselected + "_link" + i, linename[i]);

					$.jStorage.set(dstselected + "_linkname" + i, "linename["
							+ i + "]");

					if ($.jStorage.get(dstselected + "_size") != null) {

						$.jStorage.set(dstselected + "_size", $.jStorage
								.get(dstselected + "_size") + 1);

					} else {

						$.jStorage.set(dstselected + "_size", 1);
					}
					var indices = $.jStorage.index();
					var str = String(indices);
					var a = srcselected;
					var filter = new RegExp(a + "_linkname" + "(\\w+)")
					var res = str.match(filter);
					var des = $.jStorage.get(res[0]);

					var jkl = ($.jStorage.get(des));
					var o = new Array();
					o = jkl.split(",");
					// alert(des+" "+jkl);
					var ccount = $.jStorage.get(o[0] + "_num");
					var hcount1 = $.jStorage.get(srcselected + "_num");
					var hcount2 = $.jStorage.get(dstselected + "_num");
					//alert("{switch_name:"+o[0]+",src_host:"+ srcselected+", dst_host:"+ dstselected+", action:"+ "\'deny\' }");
					(namearr).push({
						switch_name : o[0],
						switch_number : ccount,
						src_host : srcselected,
						src_count : hcount1,
						dst_host : dstselected,
						dst_count : hcount2,
						protocol : protocol,
						action : 'deny'
					});

					i = i + 1;

				}
				function closepolicy() {
					jQuery('#modal-7').modal('hide');
				}
				function createNetwork()
				{
				//alert(script);
				document.getElementById("here").value=newscript;
				document.getElementById("polc").value=JSON.stringify(namearr);
				//alert(document.getElementById("polc").value);
				document.getElementById("src").submit();
				}
				
			</script>
			

	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="assets/js/jquery.sparkline.min.js"></script>
	<script src="assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="assets/js/neon-chat.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>

</body>
</html>