<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="dto.Canvasdto"%>

        <%@ page language="java" import="java.util.*"%>
    <%@ page language="java" import="service.*"%>
<%@page import="java.sql.*"%>
<%@ page language="java" import="bean.*" %>
<%@ page language="java" import="dto.*" %>
<%@ page import="sources.*"%>
<%
 try{

  String check=setsessionvar.getSessioname();
	if(check.length()>0||!(check.equals(""))){
}
else{
 String redirectURL = "invalid.jsp";
 response.sendRedirect(redirectURL);
}
}
catch(Exception e)
{
 e.printStackTrace();
}
%>	
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
 
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Neon Admin Panel" />
	<meta name="author" content="" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Ecode | Fetch Canvas</title>
	

	<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="assets/css/bootstrap.css">
	<link rel="stylesheet" href="assets/css/neon-core.css">
	<link rel="stylesheet" href="assets/css/neon-theme.css">
	<link rel="stylesheet" href="assets/css/neon-forms.css">
	<link rel="stylesheet" href="assets/css/custom.css">
     <link href="assets/css/font-icons/font-awesome/css/font-awesome.css" rel="stylesheet" />

	<script src="assets/js/jquery-1.11.0.min.js"></script>
	<script src="canvas/jstorage.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js"></script>
	<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
	
	
</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->	
	
	<div class="sidebar-menu">


            <header class="logo-env">

                <!-- logo -->
                <div class="sui-normal logo ">
                    <a href="#" class="user-link" style="margin-top: -5px;">
                        <img src="assets/images/Admin.png" alt="" class="img-circle" style="width: 57px; height: 53px;" />

                        <span style="margin-left:8px"> Welcome <strong>Admin</strong></span>

                    </a>
                </div>

                <!-- logo collapse icon -->

                <div class="sidebar-collapse" >
                    <a href="#" class="sidebar-collapse-icon with-animation">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>



                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation -->
                      <!--  <i class="entypo-menu"></i>-->
                    </a>
                </div>

            </header>

          
            <ul id="main-menu" class="">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <!-- Search Bar -->
                <li id="search">
                    <form method="get" action="">
                        <input type="text" name="q" class="search-input" placeholder="Search something..." />
                        <button type="submit">
                            <i class="entypo-search"></i>
                        </button>
                    </form>
                </li>
                 <li class="opened">
                    <a href="Dashboard.jsp">
                        <i class="entypo-gauge"></i>
                        <span>Dashboard</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Dashboard.jsp">
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="ControllerAlerts.jsp">
                                <span>Controller Alerts</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                <li class="opened">
                    <a href="Canvas.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Design</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Canvas.jsp">
                                <span>Canvas</span>
                            </a>
                        </li>
                        <li>
                            <a href="Viewer.jsp">
                                <span>Viewer</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="opened">
                    <a href="Simulate.jsp">
                        <i class="entypo-layout"></i>
                        <span>Control</span>
                    </a>
                    <ul>
                        <li>
                            <a href="Rulebase.jsp">
                                <span>Rule Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="PolicyBase.jsp">
                                <span>Policy Base</span>
                            </a>
                        </li>
                        <li>
                            <a href="Simulate.jsp">
                                <span>Simulate</span>
                            </a>
                        </li>
                    </ul>

                </li>


               
                <li>
                    <a href="Flowrector.jsp">
                        <i class="entypo-flow-tree"></i>
                        <span>Flow Rector</span>
                    </a>


                </li>
            </ul>
                


        </div>	
	<div class="main-content">
		
<div class="row">

                <!-- Profile Info and Notifications -->
                <div class="col-md-6 col-sm-8 clearfix">

                    <ul class="user-info pull-left pull-none-xsm">
                    </ul>
                    <ul class="user-info pull-left pull-right-xs pull-none-xsm">

                        <!-- Raw Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-attention"></i>
                                <span class="badge badge-info">6</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p class="small">
                                        <a href="#" class="pull-right">Mark all Read</a>
                                        You have <strong>3</strong> new notifications.
                                    </p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="unread notification-success">
                                            <a href="#">
                                                <i class="entypo-user-add pull-right"></i>

                                                <span class="line">
                                                    <strong>New user registered</strong>
                                                </span>

                                                <span class="line small">30 seconds ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="unread notification-secondary">
                                            <a href="#">
                                                <i class="entypo-heart pull-right"></i>

                                                <span class="line">
                                                    <strong>Someone special liked this</strong>
                                                </span>

                                                <span class="line small">2 minutes ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-primary">
                                            <a href="#">
                                                <i class="entypo-user pull-right"></i>

                                                <span class="line">
                                                    <strong>Privacy settings have been changed</strong>
                                                </span>

                                                <span class="line small">3 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-danger">
                                            <a href="#">
                                                <i class="entypo-cancel-circled pull-right"></i>

                                                <span class="line">John cancelled the event
                                                </span>

                                                <span class="line small">9 hours ago
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-info">
                                            <a href="#">
                                                <i class="entypo-info pull-right"></i>

                                                <span class="line">The server is status is stable
                                                </span>

                                                <span class="line small">yesterday at 10:30am
                                                </span>
                                            </a>
                                        </li>

                                        <li class="notification-warning">
                                            <a href="#">
                                                <i class="entypo-rss pull-right"></i>

                                                <span class="line">New comments waiting approval
                                                </span>

                                                <span class="line small">last week
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">View all notifications</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Message Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-mail"></i>
                                <span class="badge badge-secondary">10</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-1.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">
                                                    <strong>Luc Chartier</strong>
                                                    - yesterday
                                                </span>

                                                <span class="line desc small">This ain’t our first item, it is the best of the rest.
                                                </span>
                                            </a>
                                        </li>

                                        <li class="active">
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-2.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">
                                                    <strong>Salma Nyberg</strong>
                                                    - 2 days ago
                                                </span>

                                                <span class="line desc small">Oh he decisively impression attachment friendship so if everything. 
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-3.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">Hayden Cartwright
					- a week ago
                                                </span>

                                                <span class="line desc small">Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="#">
                                                <span class="image pull-right">
                                                    <img src="assets/images/thumb-4.png" alt="" class="img-circle" />
                                                </span>

                                                <span class="line">Sandra Eberhardt
					- 16 days ago
                                                </span>

                                                <span class="line desc small">On so attention necessary at by provision otherwise existence direction.
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="mailbox.html">All Messages</a>
                                </li>
                            </ul>

                        </li>

                        <!-- Task Notifications -->
                        <li class="notifications dropdown">

                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="entypo-list"></i>
                                <span class="badge badge-warning">1</span>
                            </a>

                            <ul class="dropdown-menu">
                                <li class="top">
                                    <p>You have 6 pending tasks</p>
                                </li>

                                <li>
                                    <ul class="dropdown-menu-list scroller">
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Procurement</span>
                                                    <span class="percent">27%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 27%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">27% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">App Development</span>
                                                    <span class="percent">83%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 83%;" class="progress-bar progress-bar-danger">
                                                        <span class="sr-only">83% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">HTML Slicing</span>
                                                    <span class="percent">91%</span>
                                                </span>

                                                <span class="progress">
                                                    <span style="width: 91%;" class="progress-bar progress-bar-success">
                                                        <span class="sr-only">91% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Database Repair</span>
                                                    <span class="percent">12%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 12%;" class="progress-bar progress-bar-warning">
                                                        <span class="sr-only">12% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Backup Create Progress</span>
                                                    <span class="percent">54%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 54%;" class="progress-bar progress-bar-info">
                                                        <span class="sr-only">54% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#">
                                                <span class="task">
                                                    <span class="desc">Upgrade Progress</span>
                                                    <span class="percent">17%</span>
                                                </span>

                                                <span class="progress progress-striped">
                                                    <span style="width: 17%;" class="progress-bar progress-bar-important">
                                                        <span class="sr-only">17% Complete</span>
                                                    </span>
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="external">
                                    <a href="#">See all tasks</a>
                                </li>
                            </ul>

                        </li>

                    </ul>

                </div>


                <!-- Raw Links -->
                <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                    <ul class="list-inline links-list pull-right">


                        <li>
                            <strong>Ecode Networks</strong> <i class="entypo-users"></i>

                        </li>

                        <li class="sep"></li>

                        <li>
                            <a href="setout.jsp">Log Out <i class="entypo-logout right"></i>
                            </a>
                        </li>
                    </ul>




                </div>
            </div>
<nav class="navbar navbar-inverse" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="Canvas.jsp">Canvas</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">File<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="Canvas.jsp" ><strong>New Canvas</strong></a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Create<b class="caret"></b> </a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:createNetwork();" onclick="createNetwork();"><strong>Create PNT</strong></a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
 <div class="container-fluid">
				<div class="col-lg-12"
					style="background-color: aliceblue; width: auto; height: 600px;">
					<div id="container"></div>
				</div>
				<div class="col-lg-1"></div>
			</div>
			<script src="canvas/kinetic-v5.0.2.min.js"></script>
			<script src="canvas/jstorage.js"></script>
			<script defer="defer">
	$.jStorage.flush();
      var stage = new Kinetic.Stage({
        container: 'container',
        width: 1000,
        height: 530
      });
	  
  var layer_lines = new Kinetic.Layer();
  var layer = new Kinetic.Layer();
  var but = document.getElementById('imginject');
  function background() {
	 
	 var upload = document.getElementById('input');
	  var file = upload.files[0],
	    reader = new FileReader();
	  reader.onload = function (event) {
		 
		  var imageObj = new Image();
	      imageObj.onload = function() {
	        var darthVaderImg = new Kinetic.Image({
		
			  width: 500,
			  height: 500,
			  name: 'image',
	          image: imageObj
	          }); 
	 
	  var darthVaderGroup = new Kinetic.Group({
	      x: 270,
	      y: 100,
	      draggable: true
	    });

	    darthVaderGroup.add(darthVaderImg);
	    addAnchor(darthVaderGroup, 0, 0, 'topLeft');
	    addAnchor(darthVaderGroup, 500, 0, 'topRight');
	    addAnchor(darthVaderGroup, 500, 500, 'bottomRight');
	    addAnchor(darthVaderGroup, 0, 500, 'bottomLeft');
	    layer_lines.add(darthVaderGroup);
	 stage.add(layer_lines);
	    darthVaderGroup.on('dragstart', function() {
	      this.moveToTop();
	    });
	 
	  
	  function update(activeAnchor) {
	      var group = activeAnchor.getParent();
	     
	      var topLeft = group.find('.topLeft')[0];
	      var topRight = group.find('.topRight')[0];
	      var bottomRight = group.find('.bottomRight')[0];
	      var bottomLeft = group.find('.bottomLeft')[0];
	      var image = group.find('.image')[0];

	      var anchorX = activeAnchor.x();
	      var anchorY = activeAnchor.y();
	      
	      // update anchor positions
	      switch (activeAnchor.name()) {
	        case 'topLeft':
	          topRight.y(anchorY);
	          bottomLeft.x(anchorX);
	          break;
	        case 'topRight':
	          topLeft.y(anchorY);
	          bottomRight.x(anchorX);
	          break;
	        case 'bottomRight':
	          bottomLeft.y(anchorY);
	          topRight.x(anchorX); 
	          break;
	        case 'bottomLeft':
	          bottomRight.y(anchorY);
	          topLeft.x(anchorX); 
	          break;
	      }

	      image.setPosition(topLeft.getPosition());

	      var width = topRight.x() - topLeft.x();
	      var height = bottomLeft.y() - topLeft.y();
	      if(width && height) {
	        image.setSize({width:width, height: height});
	      }
	    }
	    function addAnchor(group, x, y, name) {
	    	
	     

	      var anchor = new Kinetic.Circle({
	        x: x,
	        y: y,
	        stroke: '#666',
	        fill: '#ddd',
	        strokeWidth: 2,
	        radius: 8,
	        name: name,
	        draggable: true,
	        dragOnTop: false
	      });

	      anchor.on('dragmove', function() {
	        update(this);
	        layer.draw();
	      });
	      anchor.on('mousedown touchstart', function() {
	        group.setDraggable(false);
	        this.moveToTop();
	      });
	      anchor.on('dragend', function() {
	        group.setDraggable(true);
	        layer.draw();
	      });
	      // add hover styling
	      anchor.on('mouseover', function() {
	        var layer = this.getLayer();
	        document.body.style.cursor = 'pointer';
	        this.setStrokeWidth(4);
	        layer.draw();
	      });
	      anchor.on('mouseout', function() {
	        var layer = this.getLayer();
	        document.body.style.cursor = 'default';
	        this.strokeWidth(2);
	        layer.draw();
	      });

	      group.add(anchor);
	    }
	   
	      
	    };
	    
	  imageObj.src = event.target.result;
	  
	  };
	  reader.readAsDataURL(file);
closepopup5();
	  return false;
	  }

	  
   var linename = new Array();
      var srcarr = new Array();
	     var dstarr = new Array();
		 var alp ="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		 var script="";
		 var newscript="";
		 
   var i=0;
   var j=0;
   var k=1;
   var h=0;
   var o=0;
  var a1x;
  var a1y;
  var a2x;
  var a2y;
  var re=true;
  var acidno=0;
  var sflag=false;
  var src;
  var dest;
  var switchdb="";
  var hostdb="";
  var linksdb="";
  var linkdetails="";
  var z=0;
  var y=0;
  </script>

			<% int i=0; %>
			<% System.out.println("checkpointswitch1"); %>
			<% String canvasname=Canvasdto.getFetchcanvasname(); %>
			<% List<fetchtopobean> ftbean= fetchtopo.switchhost(canvasname); %>
			<% System.out.println("checkpointswitch2"); %>
			<% for(;i<ftbean.size();i++){ %>
			<% String switchdet=ftbean.get(i).getSwitchorhost(); %>
			<% if(switchdet.equalsIgnoreCase("switch")){ %>
			<% System.out.println("switchame is "+ftbean.get(i).getName()); %>
			<% System.out.println("cordinates are "+ftbean.get(i).getXcordinate()+":"+ftbean.get(i).getYcordinate()); %>
			<% String switchtype=ftbean.get(i).getSwitchtype(); %>

			<script>
  actor_name();
function actor_name(){
var name = "<%= ftbean.get(i).getName() %>";

srcarr[j] = name;

script = script+"  "+"Switch"+alp.charAt(j)+"="+k+"\n";
newscript = newscript+"  "+"Switch"+alp.charAt(j)+"="+"self.addSwitch( 's"+k+"' )"+"\n";

$.jStorage.set(name+"_py", "Switch"+alp.charAt(j) );
script = script+"  "+"self.add_node("+"Switch"+alp.charAt(j)+",Node(is_switch=True))"+"\n";
k =k+1;
j = j+1;

      var imageObj = new Image();
      imageObj.onload = function() {
    	  
    	  <% if(switchtype.equalsIgnoreCase("100")){ %>
    	  var actor = new Kinetic.Image({
				
			  width: 30	,
			  height: 30,
	          image: imageObj
	          }); 
      <% } %>
    	  <% if(switchtype.equalsIgnoreCase("12900")){ %>
    	  var actor = new Kinetic.Image({
				
			  width: 60	,
			  height: 115,
	          image: imageObj
	          }); 
      <% } %>
      <% if(switchtype.equalsIgnoreCase("6800")){ %>
      var actor = new Kinetic.Image({
			
		  width: 115	,
		  height: 88,
          image: imageObj
          }); 
          <% } %>
          <% if(switchtype.equalsIgnoreCase("5900")){ %>
          var actor = new Kinetic.Image({
  			
    		  width: 80	,
    		  height: 34,
              image: imageObj
              }); 
          <% } %>
       
     
actor.on('mouseover', function() {
        document.body.style.cursor = 'pointer';
      });

        actor.on('mouseout', function() {
        document.body.style.cursor = 'default';
      });		 


var label_text = new Kinetic.Text({
        x: actor.getX(),
        y: actor.getY()+actor.getHeight(),
        text: name,
        fontSize: 15,
        fontFamily: 'Calibri',
        fill: '#ooo'
      });
	  
label_text.setOffset({
        x:-(actor.getWidth() - label_text.getWidth())/2
      });

	var group = new Kinetic.Group({
	     id: '_ac'+acidno,
	     x: <%= ftbean.get(i).getXcordinate() %>-actor.getWidth()/2,
	     y: <%= ftbean.get(i).getYcordinate() %>-actor.getHeight()/2,
        draggable: true
    });
	
	group.on('click tap', function(e) {
	      if(e.which == 3)
		  {
          document.body.style.cursor = 'pointer';
		  var corda1 = this.getPosition();
		  a1x=(corda1.x)+actor.getWidth()/2;
		  a1y=(corda1.y)+actor.getHeight()/2;
		  re=true;
		 console.log(corda1.x+" "+corda1.y);
		  }
		  });
	
	 group.add(actor);
    group.add(label_text);
	
	var pos_s = group.getPosition();
	$.jStorage.set(name+"_x", pos_s.x+actor.getWidth()/2);
		$.jStorage.set(name+"_y",pos_s.y+actor.getHeight()/2);

	
	
	
	
         // add the shape to the layer
        layer.add(group);
	

        // add the layer to the stage
        stage.add(layer);
		
		
  group.on('dragend', function(){
	  	    
			var pos_k = this.getPosition();
			 $.jStorage.set(name+"_x", pos_k.x+actor.getWidth()/2);
			  $.jStorage.set(name+"_y",pos_k.y+actor.getHeight()/2);
				//switchdb=switchdb+" "+name+"-"+"x"+pos_k.x+":y"+pos_k.y;
  
       });
	     group.on('dragstart dragmove', function(){
	    	
	  	    
			var pos_k = group.getPosition();
			 $.jStorage.set(name+"_x", pos_k.x+actor.getWidth()/2);
			  $.jStorage.set(name+"_y",pos_k.y+actor.getHeight()/2);
			  
			 
			 var indices = $.jStorage.index();
			 var indice = String(indices);
			 console.log(indice);
			  var matche = indice.match(/(\w+)?_linkname(\w+)/g);
			  console.log(matche);
			 var matched = new Array();
			 matched = String(matche).split(",");
			 
			 var sources = new Array();
			 var dests = new Array();
			var f=-2;
for(var h=0;h<matched.length/2;h++)
			 {
		 sources[h] = matched[f+2];
			 dests[h] = matched[f+3];
f = f+2;
			 }
var indexfindsrc="";
var indexfinddst="";
var indices;
var srcs = new Array();
var dsts = new Array();
for(var g=0;g<sources.length;g++){
srcs[g] = sources[g].substring(0, sources[g].length-1);

if(srcs[g]== name+"_linkname"){indexfindsrc = indexfindsrc+g+" ";}

} 


for(var w=0;w<dests.length;w++){
dsts[w] = dests[w].substring(0, dests[w].length-1);

if(dsts[w]==name+"_linkname"){indexfinddst = indexfinddst+w+" ";}
//console.log("switches:"+ dests.length);
} 	

var lines = layer.get('Line');
	
	var indexfind = indexfindsrc +indexfinddst;
	console.log(indexfind);
			 var kill = new Array();
			  kill= indexfind.split(" ");


			  // var kill2 = new Array();
			 if(srcs.indexOf(name+"_linkname")>=0 || dsts.indexOf(name+"_linkname")>=0)
			 {
	
			 for(var t=0;t<kill.length;t++)
{			  

  var sds = $.jStorage.get( $.jStorage.get(sources[kill[t]])); 
  var sd = sds.split(",");
  //alert($.jStorage.get(sd[1]+"_x"),$.jStorage.get(sd[1]+"_y"));
  var ind;
  if(name==sd[0]){ind=1;}else{ind=0;}
  console.log(pos_k);
lines[kill[t]].setPoints([pos_k.x+actor.getWidth()/2,pos_k.y+actor.getHeight()/2,$.jStorage.get(sd[ind]+"_x"),$.jStorage.get(sd[ind]+"_y")]);
			layer.draw();
 } 
			 }
     

       }); 
       
      };
      <% if(switchtype.equalsIgnoreCase("100")){ %>
	  <% System.out.println("switchtypedefault"); %>
      imageObj.src = 'switchimg/switch1.png';
  <% } %>
      <% if(switchtype.equalsIgnoreCase("12900")){ %>
	  <% System.out.println("switchtype1"); %>
      imageObj.src = 'switchimg/HPFB_12900.png';
  <% } %>
  <% if(switchtype.equalsIgnoreCase("6800")){ %>
  <% System.out.println("switchtype2"); %>
      imageObj.src = 'switchimg/HSR6800.png';
      <% } %>
      <% if(switchtype.equalsIgnoreCase("5900")){ %>
      <% System.out.println("switchtype3"); %>
      imageObj.src = 'switchimg/FF5900.png';
      <% } %>
      
	  acidno = acidno+1;
	  
	
} 
</script>
			<% } %>
			<% } %>


			<% int j=0; %>
			<% System.out.println("checkpointhost1"); %>
			<% String canvasname2=Canvasdto.getFetchcanvasname(); %>
			<% List<fetchtopobean> ftbean2= fetchtopo.switchhost(canvasname2); %>
			<% System.out.println("checkpointhost2"); %>
			<% for(;j<ftbean2.size();j++){ %>
			<% String hostdet=ftbean2.get(j).getSwitchorhost(); %>
			<% if(hostdet.equalsIgnoreCase("host")){ %>
			<% System.out.println("hostname is "+ftbean2.get(j).getName()); %>
			<% System.out.println("cordinates are "+ftbean2.get(j).getXcordinate()+":"+ftbean2.get(j).getYcordinate()); %>

			<script>
draw_uc();
function draw_uc(){

	var uc = "<%= ftbean2.get(j).getName() %>";

dstarr[o] = uc;
o = o+1;
script = script+"  "+"Host"+alp.charAt(h)+"="+k+"\n";

$.jStorage.set(uc+"_py", "Host"+alp.charAt(h) );
newscript = newscript+"  "+"Host"+alp.charAt(h)+"="+"self.addHost( 'h"+k+"' )"+"\n";

script = script+"  "+"self.add_node("+"Host"+alp.charAt(h)+",Node(is_switch=False))"+"\n";
k =k+1;
h= h+1;
      var imageObj = new Image();
      imageObj.onload = function() {
        var host = new Kinetic.Image({
	
		  width: 30,
		  height: 32,
          image: imageObj
          }); 
     
host.on('mouseover', function() {
        document.body.style.cursor = 'pointer';
      });

        host.on('mouseout', function() {
        document.body.style.cursor = 'default';
      });		 


var label_text = new Kinetic.Text({
        x: host.getX(),
        y: host.getY()+host.getHeight(),
        text: uc,
        fontSize: 15,
        fontFamily: 'Calibri',
        fill: '#ooo'
      });
	  
label_text.setOffset({
        x:-(host.getWidth() - label_text.getWidth())/2
      });

	var group_uc = new Kinetic.Group({
	     id: '_ac'+acidno,
	     x: <%= ftbean2.get(j).getXcordinate() %>,
	     y: <%= ftbean2.get(j).getYcordinate() %>,
        draggable: true
    });
	
	group_uc.on('click tap', function(e) {
	      if(e.which == 3)
		  {
          document.body.style.cursor = 'pointer';
		  var corda1 = this.getPosition();
		  a1x=(corda1.x)+host.getWidth()/2;
		  a1y=(corda1.y)+host.getHeight()/2;
		  re=true;
		 
		  }
		  });
	
	 group_uc.add(host);
    group_uc.add(label_text);
	
	var pos_s = group_uc.getPosition();
	$.jStorage.set(uc+"_x", pos_s.x+host.getWidth()/2);
		$.jStorage.set(uc+"_y",pos_s.y+host.getHeight()/2);

	
	
	
	
         // add the shape to the layer
        layer.add(group_uc);
	

        // add the layer to the stage
        stage.add(layer);
  group_uc.on('dragend', function(){
	  	    
			var pos_k = group_uc.getPosition();
			 $.jStorage.set(uc+"_x", pos_k.x+host.getWidth()/2);
		$.jStorage.set(uc+"_y",pos_k.y+host.getHeight()/2);

		hostdb=hostdb+" "+uc+"-"+"x"+pos_k.x+":y"+pos_k.y;
       });
	     group_uc.on('dragstart dragmove', function(){
	  	    
			var pos_k = group_uc.getPosition();
			 $.jStorage.set(uc+"_x", pos_k.x+host.getWidth()/2);
		$.jStorage.set(uc+"_y",pos_k.y+host.getHeight()/2);
			  
			
			 
			 var indices = $.jStorage.index();
			 var indice = String(indices);
			  var matche = indice.match(/(\w+)(\s+)?(\w+)_linkname(\w+)/g);
			 var matched = new Array();
			 matched = String(matche).split(",");
			// alert(matched);
			 var sources = new Array();
			 var dests = new Array();
			var f=-2;
for(var h=0;h<matched.length/2;h++)
			 {
		 sources[h] = matched[f+2];
			 dests[h] = matched[f+3];
f = f+2;
			 }
var indexfindsrc="";
var indexfinddst="";
var indices;
var srcs = new Array();
var dsts = new Array();
for(var g=0;g<sources.length;g++){
srcs[g] = sources[g].substring(0, sources[g].length-1);

if(srcs[g]== uc+"_linkname"){indexfindsrc = indexfindsrc+g+" ";}

} 


for(var w=0;w<dests.length;w++){
dsts[w] = dests[w].substring(0, dests[w].length-1);

if(dsts[w]==uc+"_linkname"){indexfinddst = indexfinddst+w+" ";}
//console.log("switches:"+ dests.length);
} 	

 
 var lines = layer.get('Line');
	var indexfind = indexfindsrc +indexfinddst;
			 var kill = new Array();
			  kill= indexfind.split(" ");


			  // var kill2 = new Array();
			 if(srcs.indexOf(uc+"_linkname")>=0 || dsts.indexOf(uc+"_linkname")>=0)
			 {
	
			 for(var t=0;t<kill.length;t++)
{			  

  var sds = $.jStorage.get( $.jStorage.get(sources[kill[t]])); 
  var sd = sds.split(",");
  //alert($.jStorage.get(sd[1]+"_x"),$.jStorage.get(sd[1]+"_y"));
  var ind;
  if(name==sd[0]){ind=1;}else{ind=0;}
lines[kill[t]].setPoints([pos_k.x+host.getWidth()/2,pos_k.y+host.getHeight()/2,$.jStorage.get(sd[ind]+"_x"),$.jStorage.get(sd[ind]+"_y")]);
			layer.draw();
 } 
			 }
			 
			 
			 
			 
			
             

 

			   

       }); 
       
      };
      imageObj.src = 'img/Host.png';
	  acidno = acidno+1;
	  
      }
      </script>
			<% } %>
			<% } %>


			<% int k=0; %>
			<% System.out.println("checkpointlinks1"); %>
			<% String canvasname3=Canvasdto.getFetchcanvasname(); %>
			<% List<fetchtopobeanlink> ftlinkb = fetchtopo.links(canvasname3); %>
			<% System.out.println("checkpointlinks2"); %>
			<% for(;k<ftlinkb.size();k++){ %>


			<script>
	addLink();
	  function addLink()
	  {
		  
	 // alert("hello");
	 	var srcselected="<%= ftlinkb.get(k).getLinksource() %>";;
	var dstselected="<%= ftlinkb.get(k).getLinkdestination() %>";;
    a1x = "<%= ftlinkb.get(k).getLinksrcx() %>";;
	a1y = "<%= ftlinkb.get(k).getLinksrcy() %>";;
	a2x = "<%= ftlinkb.get(k).getLinkdestx() %>";;
	a2y = "<%= ftlinkb.get(k).getLinkdesty() %>";;
	<% System.out.println("Source and destination "+ftlinkb.get(k).getLinksource()+"-"+ftlinkb.get(k).getLinkdestination()+" cordinates "+ftlinkb.get(k).getLinksrcx()+":"+ftlinkb.get(k).getLinksrcy()+"-"+ftlinkb.get(k).getLinkdestx()+":"+ftlinkb.get(k).getLinkdesty()); %>
	linkdetails=linkdetails+" "+"src="+srcselected+"dst="+dstselected+"";
	var src = $.jStorage.get(srcselected+"_py");
	var dst = $.jStorage.get(dstselected+"_py");
	
	script = script+"  "+"self.add_edge("+src+","+dst+")"+"\n";
	newscript = newscript+"  "+"self.addLink("+src+","+dst+")"+"\n";

	if(dstarr.indexOf(dstselected)>=0){
	  
	 linename[i] = new Kinetic.Line({
        points: [a1x,a1y,a2x,a2y],
		name:'_ac'+i,
        stroke: 'black',
        strokeWidth: 3,
        lineCap: 'round',
        lineJoin: 'bevel'
      });
 layer.add(linename[i]);
}
else
{
	  
	 linename[i] = new Kinetic.Line({
        points: [a1x,a1y,a2x,a2y],
		name:'_ac'+i,
        stroke: 'grey',
        strokeWidth: 3,
        lineCap: 'round',
        lineJoin: 'bevel'
      });
 layer.add(linename[i]);
}


stage.add(layer);



$.jStorage.set("linename["+i+"]",srcselected+","+dstselected);

$.jStorage.set(srcselected+"_link"+i,linename[i]);
$.jStorage.set(srcselected+"_linkname"+i, "linename["+i+"]");

if($.jStorage.get(srcselected+"_size")!=null)
{

$.jStorage.set(srcselected+"_size", $.jStorage.get(srcselected+"_size")+1);

}
else{

$.jStorage.set(srcselected+"_size",1);
}



//alert($.jStorage.get(srcselected+"_links"));


$.jStorage.set(dstselected+"_link"+i,linename[i]);

$.jStorage.set(dstselected+"_linkname"+i, "linename["+i+"]");

if($.jStorage.get(dstselected+"_size")!=null)
{

$.jStorage.set(dstselected+"_size", $.jStorage.get(dstselected+"_size")+1);

}
else{

$.jStorage.set(dstselected+"_size",1);
}


	  
	  i = i+1;
	  
	  }
	  </script>
			<% } %>


			<script>
	  function populate(){
	document.getElementById("srcList").style.visibility = "visible";
	document.getElementById("dstList").style.visibility = "visible";
	
	var sel1 = document.getElementById('srcList');
    for (var m = 0; m < srcarr.length; m++) {
        var opt = document.createElement('option');
        opt.innerHTML = srcarr[m];
        opt.value = srcarr[m];
        sel1.appendChild(opt);
        }
		
		var sel2 = document.getElementById('dstList');
    for (var n = 0; n < dstarr.length; n++) {
        var opt = document.createElement('option');
        opt.innerHTML = dstarr[n];
        opt.value = dstarr[n];
        sel2.appendChild(opt);
        }
		
    for (var l = 0; l < srcarr.length; l++) {
        var opt = document.createElement('option');
        opt.innerHTML = srcarr[l];
        opt.value = srcarr[l];
        sel2.appendChild(opt);
        }
		
	
	
	
	}
	
	function createNetwork()
	{
	//alert(script);
	document.getElementById("here").value=newscript;
	
	document.getElementById("src").submit();
	
	}
	
	   
    
	
	
	
        
    </script>
   		
       <form name="scripter" id="src" method="POST" action="scriptPusher.jsp">
<input type="hidden" value="" name="script" id="here">
</form>    
            
<footer class="main">
	
		
	Copyrights &copy; 2016 <strong>Ecode Networks</strong>
	
</footer>	</div>

	</div>

	<!-- Bottom Scripts -->
	<script src="assets/js/gsap/main-gsap.js"></script>
	<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/neon-api.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<script src="assets/js/jquery.sparkline.min.js"></script>
	<script src="assets/js/rickshaw/vendor/d3.v3.js"></script>
	<script src="assets/js/rickshaw/rickshaw.min.js"></script>
	<script src="assets/js/neon-chat.js"></script>
	<script src="assets/js/neon-custom.js"></script>
	<script src="assets/js/neon-demo.js"></script>
</body>
</html>