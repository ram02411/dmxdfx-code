var da="null";

var t = new NetChart({
        container: document.getElementById("snt"),
        height: 350,
       // data:{
         //   url: "http://localhost:8080/EvolveOns/evolve/erest/data.json"
        //},
        data:{
             preloadNodeLinks:true
        },
        navigation:{
            mode:"manual",
            initialNodes: ["1c:4f:08:9e:01:53:78:46","00:00:00:00:00:00:00:01"]
        },
        
         events:{
        	 onClick: function(event){ webSocketSwitch.send(event.clickNode.id);},
             onRightClick: function(event){ webSocketPort.send(event.clickNode.id);},
             onDoubleClick: function(event){ webSocketFlow.send(event.clickNode.id);}
  },
        style:{
            nodeRules:{"rule1":nodeStyle},
            linkRules:{"rule1":linkStyle},
            makeImagesCircular: true
        }
    });
    function nodeStyle(node){
         var image = null;
        if (node.data.type == "switch"){
            image = "pics/Switch.png";
            node.label = node.data.name;
        }
        else{
            image = "pics/hostnew.png";
            node.label = node.data.name;
        }
          
        node.image = image;
    
      
    }
    function linkStyle(link){
        link.fillColor = "limegreen";
    }


            var webSocket = 
            new WebSocket('wss://192.168.0.115:8443/evolve3.01/websocketPNT');

        webSocket.onerror = function(event) {
            onError(event)
        };

        webSocket.onopen = function(event) {
            onOpen(event)
        };

        webSocket.onmessage = function(event) {
            onMessage(event)
        };

        function onMessage(event) {
            da = event.data;
           // alert(da);
            if(event.data=="same")
            {

            }
            else{
                    t.replaceData(event.data);}
        }

        function onOpen(event) {
webSocket.send(da);
            return false;          
        }

        function onError(event) {
            alert("error" + event.data);
        }

        function start() {
            webSocket.send(da);
            return false;
        }

        setInterval(function() {
start()

}, 15000); 


        var webSocketSwitch = 
            new WebSocket('wss://192.168.0.115:8443/evolve3.01/switchdetailsPNT');

        webSocketSwitch.onerror = function(eventSwitch) {
            onErrorSwitch(eventSwitch)
        };

        webSocketSwitch.onopen = function(eventSwitch) {
         //   onOpenSwitch(eventSwitch)
        };

        webSocketSwitch.onmessage = function(eventSwitch) {
            onMessageSwitch(eventSwitch)
        };

        function onMessageSwitch(eventSwitch) {
        var tab= "<table id='excelDataTable1' border='1' ></table>";        	
        	document.getElementById('details').innerHTML = tab; 
        	switchDetails(eventSwitch.data);

        }

        function onErrorSwitch(eventSwitch) {
            alert(event.data);
        }

       
        var webSocketFlow = 
            new WebSocket('wss://192.168.0.115:8443/evolve3.01/flowdetailsPNT');

        webSocketFlow.onerror = function(eventFlow) {
            onErrorPort(eventFlow)
        };

        webSocketFlow.onopen = function(eventFlow) {
         //   onOpenSwitch(eventSwitch)
        };

        webSocketFlow.onmessage = function(eventFlow) {
            onMessageFlow(eventFlow)
        };

        function onMessageFlow(eventFlow) {
        	//var tab= "<table id='excelDataTable' border='1'></table>";        	
        	//document.getElementById('details').innerHTML = tab; 
        	flowDetails(eventFlow.data);

        }

        function onErrorSwitch(eventFlow) {
            alert(eventFlow.data);
        }
        
        
        
        function switchDetails(json)
        {
        	var myList = JSON.parse(json);
        	
        	var count = Object.keys(myList).length
var head = "<thead id='firstrow' style='background:rgba(69, 74, 84, 0.7);'><th>manufacturer</th><th>hardware</th><th>software</th><th>serial</th><th>description</th><th>Device IP</th><th>Version</th><th>dpid</th></thead>";

                     var row$ = $('<tr/>');

                         var md = myList["mfr"];
                         var hd = myList["hw"];
                         var sd = myList["sw"];
                         var sn = myList["serial"];
                         var dd = myList["desc"];
                         var dp = myList["deviceIp"];
                         var pc = myList["negotiatedVersion"];
                         var bc = myList["numTables"];
                         var fc = myList["dpid"];

                         row$.append($('<td/>').html(md));
                         row$.append($('<td/>').html(hd));
                         row$.append($('<td/>').html(sd));
                         row$.append($('<td/>').html(sn));
                         row$.append($('<td/>').html(dd));
                         row$.append($('<td/>').html(dp));
                         row$.append($('<td/>').html(pc));
                         row$.append($('<td/>').html(fc));

                     
                         $("#excelDataTable1").append(head);

                     $("#excelDataTable1").append(row$);


        }
        
        
        function flowDetails(jsons)
        {
        	var json = JSON.stringify(eval("(" + jsons + ")"));
        	 json = JSON.parse(json);
        	var out= "<table id='excelDataTable' border='1'>"; 
   
        	var count = Object.keys(json).length

        	
out = out +"<tr>"
        		var kset = Object.keys(json[0]);
        		for(var j=0;j<kset.length;j++)
        		{
        			
        			
        			out=out+"<th>"+kset[j]+"</th>";
        			
        		}
        		
        		
        	
        	out = out +"</tr>"
        	
        	for(var i=0;i<count;i++)
        	{

        		var kset = Object.keys(json[i]);
        		
        		out = out +"<tr>"
        		
        		for(var j=0;j<kset.length;j++)
        		{
                             var ob = json[i][kset[j]];

                             out = out +"<td>"+ob+"</td>";
                             
                             

        		}


        		out = out +"</tr>";
        		
        	}
        	
        	
    		out = out+"</table>";
    		
    		
    		document.getElementById("details").innerHTML=out;
        		}
        	
           
        
