package FlowPusher;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import canvas.util.JSONArray;
import canvas.util.JSONObject;
import dto.IPSdto;
import logger.logger;

public class flowFetcherOnos {

static String IP = IPSdto.getControllerIP();
	
	public static String switchid;
	static logger logme = logger.getInstance() ;

	public static List<HashMap<String,String>> getFlowTable(String switchdpid) 
	{
		switchid = switchdpid;
		 
     String dpid =switchid;
     
List<HashMap<String,String>> flowtable = new ArrayList<HashMap<String,String>>();

String jsonresponse="";

try{
	URL Url=new URL("http://"+IP+":8181/onos/v1/flow/of:"+dpid);
	HttpURLConnection con = (HttpURLConnection) Url.openConnection();
   con.setRequestProperty("Accept", "application/json");
   con.setRequestMethod("GET");
     
   InputStream is = con.getInputStream();
   BufferedReader br = new BufferedReader(new InputStreamReader(is));
   String line = null;
   while ((line = br.readLine()) != null) {
	   jsonresponse = jsonresponse.concat(line);
   }
   br.close();
	con.disconnect();
	
	JSONObject obj3 = new JSONObject(jsonresponse);
    JSONArray geodata1 = obj3.getJSONArray("flows");
     int n = geodata1.length();
     int packetc=0;
     int byte_c=0;
     int priorityc=0;
     String dpid1="";
     for (int g = 0; g < n; g++) {
    	 HashMap<String,String> flows = new HashMap<String,String>();
    	    JSONObject person1 = geodata1.getJSONObject(g);
    	    priorityc=(Integer)person1.getInt("priority");
    	    byte_c=(Integer)person1.getInt("bytes");
    	    packetc=(Integer)person1.getInt("packets");
    	  
    	    flows.put("priority", String.valueOf(priorityc));
    	    flows.put("packet_count", String.valueOf(packetc));
    		flows.put("byte_count", String.valueOf(byte_c));
    		flowtable.add(flows);
    	   }
  }
catch(Exception e)
{
	e.printStackTrace();
}
	
	        
	    
return flowtable;
	   
	}
}
