package FlowPusher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import logger.logger;

import canvas.util.JSONArray;
import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;

public class portDetails {
 static String IP=IPSdto.getControllerIP();
	public static String switchid;
	
	public static List<HashMap<String,String>> getPorts(String dpid) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
		switchid =dpid;
		String token = getAuth();
	
		 TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
             public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
             }
             public void checkClientTrusted(X509Certificate[] certs, String authType) {
             }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
             }
         }
     };

    
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
     HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

    
     HostnameVerifier allHostsValid = new HostnameVerifier() {
         public boolean verify(String hostname, SSLSession session) {
             return true;
        }
     };

   
     HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

     

    
     String dpid1 =switchid;
     
     String sorted="";
     for(int i=0;i<21;i++)
     {
     	sorted = sorted + (char)dpid1.charAt(i)+(char)dpid1.charAt(i+1)+"%3A";
     	i= i+2;
     }
     
     sorted = sorted+(char)dpid1.charAt(21)+(char)dpid1.charAt(22);
     
     
     URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/datapaths/"+sorted+"/ports");
     HttpURLConnection con = (HttpURLConnection) url.openConnection();
  con.setRequestMethod("GET");
con.setRequestProperty ("X-Auth-Token", token );

    InputStreamReader reader = new InputStreamReader(con.getInputStream());
  String res="";
  while (true) {
         int ch = reader.read();
      if(ch!=-1){
        res=res+(char)ch;}
      else{break;}
     }
  
  
List<HashMap<String,String>> porttable = new ArrayList<HashMap<String,String>>();

	    JSONObject obj = new JSONObject(res);
	     JSONArray geodata1 = obj.getJSONArray("ports");
	      int m = geodata1.length();
	    String arr[] = new String[20];
	   for (int j = 0; j < m; j++) {
	    	HashMap<String,String> ports = new HashMap<String,String>();
	     JSONObject person1 = geodata1.getJSONObject(j);
	  
	  // System.out.println(person1.get("name"));
	 ports.put("port_name", (String) person1.get("name"));
	  // System.out.println(person1.get("mac"));
	   ports.put("mac_address", (String) person1.get("mac"));
	  JSONArray arr1 = person1.getJSONArray("state");
	  String arry = "";
	  for(int i=0;i<arr1.length();i++){
	   arry = arr1.get(i)+" ";
	  
	  }
	//  System.out.println(arry);
	  ports.put("state", arry);
	  
	  JSONArray arr2 = person1.getJSONArray("config");
	  String arry3 = "";
	  for(int i=0;i<arr2.length();i++){
	   arry3 = arr2.get(i)+" ";
	  
	  }
	  ports.put("config", arry3);
	        System.out.println();
	        porttable.add(ports);
	    }
	return porttable; 

	   
	}

	private static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException {
		String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
		String jsonresponse = "";
		

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty("Content-Type", "application/json");

	con.setDoOutput(true);
 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		
 wr.write(tokenInput);
	wr.flush();
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}
	wr.close();
	rd.close();
	
	 JSONObject obj1 = new JSONObject(jsonresponse);
     JSONObject geodata = obj1.getJSONObject("record");
     String token = geodata.getString("token");
 
	return token;
				
	}


	}


