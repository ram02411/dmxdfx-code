package FlowPusher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;
import logger.logger;

public class flowPusherSNT {
	static String IP="192.168.0.21";
	static logger logme = logger.getInstance() ;
		@SuppressWarnings("static-access")
		public static String pushFlows(String dpid,String json) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
			String token = getAuth();
		
			 TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	             public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                return null;
	             }
	             public void checkClientTrusted(X509Certificate[] certs, String authType) {
	             }
	            public void checkServerTrusted(X509Certificate[] certs, String authType) {
	             }
	         }
	     };

	    
	    SSLContext sc = SSLContext.getInstance("SSL");
	    sc.init(null, trustAllCerts, new java.security.SecureRandom());
	     HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

	    
	     HostnameVerifier allHostsValid = new HostnameVerifier() {
	         public boolean verify(String hostname, SSLSession session) {
	             return true;
	        }
	     };

	   
	     HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

	     

	    
	    
	     
	     String sorted="";
	     for(int i=0;i<21;i++)
	     {
	     	sorted = sorted + (char)dpid.charAt(i)+(char)dpid.charAt(i+1)+"%3A";
	     	i= i+2;
	     }
	     
	     sorted = sorted+(char)dpid.charAt(21)+(char)dpid.charAt(22);
	     
	     
	     URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/datapaths/"+sorted+"/flows");
	     HttpURLConnection con = (HttpURLConnection) url.openConnection();
	  con.setRequestMethod("POST");
	con.setRequestProperty ("X-Auth-Token", token );
	con.setRequestProperty("Content-Type", "application/json");
	con.setDoOutput(true);
	//System.out.println("https://"+IP+":8443/sdn/v2.0/of/datapaths/"+sorted+"/flows");
	//System.out.println(dpid);
	//System.out.println(json);
	OutputStreamWriter wr = null;
	try{
	 wr = new OutputStreamWriter(con.getOutputStream());
	wr.write(json);
	wr.flush();}
	catch(Exception e)
	{
		 logme.log.severe( e.getMessage());
	}
	logme.log.info( "flow pushed on "+dpid);

	wr.close();
			
			
			return String.valueOf(( con.getResponseCode()));
				
			
		}

		private static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException {
			String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
			String jsonresponse = "";
			

			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	               return null;
	            }
	            public void checkClientTrusted(X509Certificate[] certs, String authType) {
	            }
	           public void checkServerTrusted(X509Certificate[] certs, String authType) {
	            }
	        }
	    };

	   
	   SSLContext sc = SSLContext.getInstance("SSL");
	   sc.init(null, trustAllCerts, new java.security.SecureRandom());
	    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

	   
	    HostnameVerifier allHostsValid = new HostnameVerifier() {
	        public boolean verify(String hostname, SSLSession session) {
	            return true;
	       }
	    };
	    
		     
	  
	    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

	    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
	    HttpURLConnection con = (HttpURLConnection) url.openConnection();
	 con.setRequestMethod("POST");
	con.setRequestProperty("Content-Type", "application/json");

		con.setDoOutput(true);
	 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
			
	 wr.write(tokenInput);
		wr.flush();
			
		BufferedReader rd = new BufferedReader(new InputStreamReader(
				con.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			jsonresponse = jsonresponse.concat(line);
		}
		wr.close();
		rd.close();
		
		 JSONObject obj1 = new JSONObject(jsonresponse);
	     JSONObject geodata = obj1.getJSONObject("record");
	     String token = geodata.getString("token");
	 
		return token;
					
		}


}
