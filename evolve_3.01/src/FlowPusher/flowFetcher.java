package FlowPusher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import logger.logger;

import canvas.util.JSONArray;
import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;

public class flowFetcher {

	 static String IP = IPSdto.getControllerIP();
	
	public static String switchid;
	static logger logme = logger.getInstance() ;

	@SuppressWarnings({ "static-access", "unchecked" })
	public static List<HashMap<String,String>> getFlowTable(String switchdpid) throws KeyManagementException, ProtocolException, NoSuchAlgorithmException {
		switchid = switchdpid;
		String token = null;
		try {
			token = getAuth();
		} catch (KeyManagementException | NoSuchAlgorithmException
				| IOException | JSONException e) {
			// TODO Auto-generated catch block
	          logme.log.severe( e.getMessage());
	          logme.log.warning( "unable to fetch the access token from controller");
		}
	
		 TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
             public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
             }
             public void checkClientTrusted(X509Certificate[] certs, String authType) {
             }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
             }
         }
     };

    
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
     HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

    
     HostnameVerifier allHostsValid = new HostnameVerifier() {
         public boolean verify(String hostname, SSLSession session) {
             return true;
        }
     };

   
     HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

     

    
     String dpid =switchid;
     
     String sorted="";
     for(int i=0;i<21;i++)
     {
     	sorted = sorted + (char)dpid.charAt(i)+(char)dpid.charAt(i+1)+"%3A";
     	i= i+2;
     }
     
     sorted = sorted+(char)dpid.charAt(21)+(char)dpid.charAt(22);
     
     
     URL url = null;
	try {
		url = new URL("https://"+IP+":8443/sdn/v2.0/of/datapaths/"+sorted+"/flows");
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
     HttpURLConnection con = null;
	try {
		con = (HttpURLConnection) url.openConnection();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logme.log.severe( e.getMessage());
        logme.log.warning( "unable to fetch the flows from switches");

	}
  con.setRequestMethod("GET");
con.setRequestProperty ("X-Auth-Token", token );

    InputStreamReader reader = null;
	try {
		reader = new InputStreamReader(con.getInputStream());
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logme.log.severe( e.getMessage());
        logme.log.warning( "unable to fetch the flows from switches");

	}
  String res="";
  while (true) {
         int ch = 0;
		try {
			ch = reader.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logme.log.severe( e.getMessage());
	        logme.log.warning( "unable to fetch the flows from switches");

		}
      if(ch!=-1){
        res=res+(char)ch;}
      else{break;}
     }
  
  
List<HashMap<String,String>> flowtable = new ArrayList<HashMap<String,String>>();

	    JSONObject obj = null;
		try {
			obj = new JSONObject(res);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logme.log.severe( e.getMessage());
		}
	     JSONArray geodata1 = null;
		try {
			geodata1 = obj.getJSONArray("flows");
		} catch (JSONException e) {
			logme.log.severe( e.getMessage());
		}
	      int m = geodata1.length();
	      String arr[] = new String[20];
	    for (int j = 0; j < m; j++) {
	    	HashMap<String,String> flows = new HashMap<String,String>();
	     JSONObject person1 = null;
		try {
			person1 = geodata1.getJSONObject(j);
		} catch (JSONException e) {
			logme.log.severe( e.getMessage());
		}
	  
	    int a = 0;
		try {
			a = (Integer)person1.getInt("packet_count");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logme.log.severe( e.getMessage());
		}
	    int b = 0;
		try {
			b = (Integer)person1.getInt("byte_count");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			logme.log.severe( e.getMessage());
		}
	  //  System.out.println("packet_count"+"  "+a);
	  //  System.out.println("byte_count"+"  "+b);
	    JSONObject person2 = null;
		try {
			person2 = geodata1.getJSONObject(j);
		} catch (JSONException e) {
			logme.log.severe( e.getMessage());
		}
	   JSONArray obj2 = null;
	try {
		obj2 = person2.getJSONArray("match");
	} catch (JSONException e) {
		logme.log.severe( e.getMessage());
	}
	String out="";
	
 for(int z=0;z<obj2.length();z++){
	 @SuppressWarnings("unchecked")
	Iterator<String> keys = null;
	try {
		keys = obj2.getJSONObject(z).keys();
	} catch (JSONException e1) {
		// TODO Auto-generated catch block
		logme.log.severe( e1.getMessage());
	}
	    while(keys.hasNext()){
	        String key = keys.next();
	     //   System.out.print(key+" ");
	        
	    if(key.equals("in_port"))
	    {
	    	try {
				out = obj2.getJSONObject(z).getInt(key)+"";
			} catch (JSONException e) {
				logme.log.severe( e.getMessage());
			}
	    	flows.put(key, out);
	    }
	    else{
  try {
	out = obj2.getJSONObject(z).getString(key);
} catch (JSONException e) {
	// TODO Auto-generated catch block
	logme.log.severe( e.getMessage());
}
  flows.put(key, out);
	    }
	    
	 //   System.out.println(out);
	    
	    }
 }

 JSONArray obj3 = null;
try {
	obj3 = person2.getJSONArray("actions");
} catch (JSONException e) {
	logme.log.severe( e.getMessage());
}
	String out1="";
	
 for(int z=0;z<obj3.length();z++){
	 @SuppressWarnings("unchecked")
	Iterator<String> keys = null;
	try {
		keys = obj3.getJSONObject(z).keys();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		logme.log.severe( e.getMessage());
	}
	    while(keys.hasNext()){
	        String key = keys.next();
	     //   System.out.print(key+" ");
	        
	    if(key.equals("output"))
	    {
	    	try {
				out1 = obj3.getJSONObject(z).getInt(key)+"";
			} catch (JSONException e) {
				logme.log.severe( e.getMessage());
			}
	    	flows.put(key, out1);
	    }
	    else{
  try {
	out1 = obj3.getJSONObject(z).getString(key);
} catch (JSONException e) {
	// TODO Auto-generated catch block
	logme.log.severe( e.getMessage());
}
  flows.put(key, out1);
	    }
	    
	 //   System.out.println(out);
	    
	    }
 }

 //arr[j]=out;
	flows.put("packet_count", String.valueOf(a));
	flows.put("byte_count", String.valueOf(b));
	flowtable.add(flows);
	        
	    }
return flowtable;
	   
	}

	private static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException {
		String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
		String jsonresponse = "";
		

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty("Content-Type", "application/json");

	con.setDoOutput(true);
 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		
 wr.write(tokenInput);
	wr.flush();
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}
	wr.close();
	rd.close();
	
	 JSONObject obj1 = new JSONObject(jsonresponse);
     JSONObject geodata = obj1.getJSONObject("record");
     String token = geodata.getString("token");
 
	return token;
				
	}

}
