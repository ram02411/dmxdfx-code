package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import sources.excelWriter;

import bean.Policybean;
import bean.Rulebean;
import bean.Policyschedulebean;

public class RuleBaseSNT {
	static ResultSet rs = null;

	/**
	 * @param args
	 * @param user
	 * @param pass
	 * @return
	 */

public static List<Rulebean> printresult() {
	List<Rulebean> rb=new ArrayList<Rulebean>();

	try {
		System.out.println("Process RULEon1");
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		
		//values to be used for testing
		/*String user = "root";
		String pass = "3volve123!";
		String ruleid = "914";
		String switchnum = "00:00:00:00:00:11";
		String inter = "035";
		String sourceip = "192.168.17.1";
		String destip = "192.168.17.2";
		String protocol = "icmp";
		String port = "3449";
		String action = "ALLOW";
		String validation = "NOT VALIDATED";
		String datetime = "2012-12-12 08:09:10";
		// String polid="900";*/

		Statement st = conn.createStatement();

		System.out.println("Process RULEon2");
		// String SQL1 =
		// String.format("INSERT INTO FIREWALL_RULE_BASE VALUES('"+policyid+"', '"+switchnum+"', '"+inter+"', '"+sourceip+"', '"+destip+"', '"+protocol+"', '"+port+"','"+action+"', '"+datetime+"')");
		// st.executeUpdate(SQL1);
	
		System.out.println("SELECT * FROM SNTFIREWALL_RULE_BASE");

		Statement stmt = conn.createStatement();
		String query = "SELECT * FROM SNTFIREWALL_RULE_BASE;";
		rs = stmt.executeQuery(query);

		while (rs.next()) {
			Rulebean rbean=new Rulebean();
			String RULEID = rs.getObject(1).toString();
			rbean.setRuleid(RULEID);
			String SWITCHID = rs.getObject(2).toString();
			rbean.setRswitchnum(SWITCHID);
			try{
				String INTERFACE="";
				if(String.valueOf(rs.getObject(3).toString()).equalsIgnoreCase("")){
				INTERFACE = "N/A";
				}
				else{
					INTERFACE=rs.getObject(3).toString();
				}
				rbean.setRinter(INTERFACE);
			}
			catch(Exception e){
				//e.printStackTrace();
			}
			try{
				String SOURCEIP="";
				if(String.valueOf(rs.getObject(4).toString()).equalsIgnoreCase("")){
				SOURCEIP = "N/A";
				}
				else{
					SOURCEIP=rs.getObject(4).toString();
				}
				rbean.setRsourceip(SOURCEIP);
			}
			catch(Exception e){
				//e.printStackTrace();
			}
			try{
				String DESTINATIONIP="";
				if(String.valueOf(rs.getObject(5).toString()).equalsIgnoreCase("")){
				DESTINATIONIP = "N/A";
				}
				else{
					DESTINATIONIP=rs.getObject(5).toString();
				}
				rbean.setRdestip(DESTINATIONIP);
			}
			catch(Exception e){
				//e.printStackTrace();
			}
			try{
				String PROTOCOL="";
				if(String.valueOf(rs.getObject(6).toString()).equalsIgnoreCase("")){
				PROTOCOL = "N/A";
				}
				else{
					PROTOCOL=rs.getObject(6).toString();
				}
				rbean.setRprotocol(PROTOCOL);
			}
			catch(Exception e){
				//e.printStackTrace();
			}
			try{
				String PORT="";
				if(String.valueOf(rs.getObject(7).toString()).equalsIgnoreCase("")){
				PORT = "N/A";
				}
				else{
					PORT=rs.getObject(7).toString();
				}
				rbean.setRport(PORT);
			}
			catch(Exception e){
				//e.printStackTrace();
			}
			String PRIORITY=rs.getObject(8).toString();
			rbean.setPriority(PRIORITY);
			String ACTION = rs.getObject(9).toString();
			rbean.setRaction(ACTION);
			String DATETIME = rs.getObject(10).toString();
			rbean.setRdatetime(DATETIME);
			// System.out.println("VALUES ARE:" + "\n" +POLICYID+ "\n"
			// +SWITCHID+ "\n" +INTERFACE+ "\n" +SOURCEIP+ "\n"
			// +DESTINATIONIP+ "\n" +PROTOCOL+ "\n" +PORT+ "\n" +ACTION+
			// "\n" +DATETIME);
			// ls.add(POLICYID +SWITCHID +INTERFACE +SOURCEIP+DESTINATIONIP
			// +PROTOCOL +PORT +ACTION +DATETIME);
			rb.add(rbean);
		}
		conn.close();

	} catch (Exception e) {
		System.err.println("Got an exception! ");
		e.printStackTrace();
	}
	return rb;
}

public static void clearrb() {
	try {
		System.out.println("Process RULEdel1");
		Class.forName("com.mysql.jdbc.Driver");
		Connection conn = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

		Statement stmt=conn.createStatement();
		String query="truncate table SNTFIREWALL_RULE_BASE;";
		System.out.println(query);
		stmt.executeUpdate(query);
		System.out.println("deleted");
		
		conn.close();

	} catch (Exception e) {
		System.err.println("Got an exception! ");
		e.printStackTrace();
	}
}


}