package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;


import dto.Canvasdto;

public class Canvassave {
	public static void savecanas(){
		try {
			System.out.println("Process enter");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			
			
			//adding in namelist
		String canvasname=Canvasdto.getCanvasname();
		Statement stmt1 = conn.createStatement();
		String SQL1=String.format("INSERT INTO canvasnamelist VALUES(default, '"+canvasname+"')");
		stmt1.executeUpdate(SQL1);
		
		//creating elements table
		Statement stmt2 = conn.createStatement();
		String SQL2=String.format("CREATE TABLE "+canvasname+"(switchorhost varchar(50), name varchar(500), switchtype varchar(100), xcordinate varchar(100), ycordinate varchar(100))");
		stmt2.execute(SQL2);
		
		//creating link table
		String linktablename=canvasname+"link";
		Statement stmt3 = conn.createStatement();
		String SQL3=String.format("CREATE TABLE "+linktablename+"(source varchar(500), srcxcordinate varchar(500), srxycordinate varchar(500), destination varchar(500), destxcordinate varchar(500), destycordinate varchar(500))");
		stmt3.execute(SQL3);
		
		String switchdetinput=Canvasdto.getSwitchdetails();
		String hostdetinput=Canvasdto.getHostdetails();
		String linkdetinput=Canvasdto.getLinkdetails();
		
		System.out.println("start");

		//for entering switch details
		List<String> switchdetails = new ArrayList<String>();
		
		String switchmed=switchdetinput;
		Integer sl=switchmed.length();
		switchmed=switchmed.substring(1, sl);
		System.out.println(switchmed+"\n");
		String output;
		while(!switchmed.isEmpty()){
			if(switchmed.contains(" ")){
			 Integer space=switchmed.indexOf(" ");
			 Integer length=switchmed.length();
			 output=switchmed.substring(0, space);
			 System.out.println("output is "+output);
			 switchmed=switchmed.substring(space+1, length);
		}
			else{
				System.out.println("-"+switchmed+"-");
				output=switchmed;
				switchmed="";
			}
			 String sname;
			 if (output.contains("-")){
				 System.out.println("contains dash");
				 Integer dash = output.indexOf("-");
				 sname = output.substring(0, dash);
			 }
			 else{
				 sname = output;
			 }
			 Integer listsize=switchdetails.size();
			 if(listsize==0){
				 System.out.println("list is empty");
				 switchdetails.add(output);
			 }
			 else{
				 System.out.println("list is not empty");
				 Integer check=0;
				 Integer catchindex=0;
				 for(int a=0;a<listsize;a++){
					 System.out.println(sname+" checked with "+switchdetails.get(a).toString());
					 if(switchdetails.get(a).toString().contains(sname)){
						 check=check+1;
						 catchindex=a;
						 System.out.println("value matched in loop "+check+","+catchindex);
					 }
					 else{
						 System.out.println("value not matched in loop");
					 }
				 }
				 if(check==1){
					 System.out.println("value had been found");
					 System.out.println("index caught is "+catchindex);
					 String nametyp=switchdetails.get(catchindex).toString();
					 if(nametyp.contains("-")){
						 Integer ntdash=nametyp.indexOf("-");
						 nametyp=nametyp.substring(0, ntdash);
						 System.out.println("erasing previous cordinates");
					 }
					 else{
						 nametyp=nametyp;
					 }
					 Integer odash=output.indexOf("-");
					 Integer olength=output.length();
					 output=output.substring(odash+1, olength);
					 System.out.println(output);
					 output=nametyp+"-"+output;	
					 System.out.println(output+ " value at that index "+switchdetails.get(catchindex).toString());
					 System.out.println("removing previous values");
					 //switchdetails.remove(catchindex);
					 System.out.println("adding new value "+output);
					 switchdetails.set(catchindex, output);
					 System.out.println("new value is "+switchdetails.get(catchindex).toString()+"\n");
				 }
				 else{
					 System.out.println("not present in list so adding");
					 switchdetails.add(output);
				 }
			 }
		}
		
		System.out.println("size of list "+switchdetails.size());
		for(int b=0;b<switchdetails.size();b++){
			System.out.println("\n"+"output is "+switchdetails.get(b).toString());
			String switchinput=switchdetails.get(b).toString();
			if(switchinput.contains("-")){
				Integer sdash=switchinput.indexOf("-");
				Integer switlength=switchinput.length();
				String switchnametyp=switchinput.substring(0, sdash);
				Integer openbr=switchnametyp.indexOf("(");
				Integer closedbr=switchnametyp.indexOf(")");
				String switchname=switchnametyp.substring(0, openbr);
				String switchtype=switchnametyp.substring(openbr+1, closedbr);
				String xandy=switchinput.substring(sdash, switlength);
				Integer switx=xandy.indexOf("x");
				Integer scol=xandy.indexOf(":");
				Integer swity=xandy.indexOf("y");
				Integer xylength=xandy.length();
				String switchxc=xandy.substring(switx+1, scol);
				String switchyc=xandy.substring(swity+1, xylength);
			System.out.println("values to be entered are :"+switchname+",xy"+switchxc+":"+switchyc);
			//adding switch values
			Statement stmt4 = conn.createStatement();
			String SQL4=String.format("INSERT INTO "+canvasname+" VALUES('switch', '"+switchname+"', '"+switchtype +"', '"+switchxc+"', '"+switchyc+"')");
			stmt4.executeUpdate(SQL4);
			}
			else{
				String switchnametyp=switchinput;
				Integer openbr=switchnametyp.indexOf("(");
				Integer closedbr=switchnametyp.indexOf(")");
				String switchname=switchnametyp.substring(0, openbr);
				String switchtype=switchnametyp.substring(openbr+1, closedbr);
				//adding switch values
				Statement stmt4b = conn.createStatement();
				String SQL4b=String.format("INSERT INTO "+canvasname+" VALUES('switch', '"+switchname+"', '"+switchtype +"', '100', '100')");
				stmt4b.executeUpdate(SQL4b);
			}
		}
		
		
		//for entering host details
		//for entering switch details
		try{
				List<String> hostdetails = new ArrayList<String>();
				
				String hostmed=hostdetinput;
				Integer hl=hostmed.length();
				if(!(hostmed.equalsIgnoreCase(""))){
				hostmed=hostmed.substring(1, hl);
				System.out.println(hostmed+"\n");
				String output2;
				while(!hostmed.isEmpty()){
					if(hostmed.contains(" ")){
					 Integer space=hostmed.indexOf(" ");
					 Integer length=hostmed.length();
					 output2=hostmed.substring(0, space);
					 System.out.println("output is "+output2);
					 hostmed=hostmed.substring(space+1, length);
				}
					else{
						System.out.println("-"+hostmed+"-");
						output2=hostmed;
						hostmed="";
					}
					 String hname;
					 if (output2.contains("-")){
						 System.out.println("contains dash");
						 Integer dash = output2.indexOf("-");
						 hname = output2.substring(0, dash);
					 }
					 else{
						 hname = output2;
					 }
					 Integer listsize=hostdetails.size();
					 if(listsize==0){
						 System.out.println("list is empty");
						 hostdetails.add(output2);
					 }
					 else{
						 System.out.println("list is not empty");
						 Integer check=0;
						 Integer catchindex=0;
						 for(int c=0;c<listsize;c++){
							 System.out.println(hname+" checked with "+hostdetails.get(c).toString());
							 if(hostdetails.get(c).toString().contains(hname)){
								 check=check+1;
								 catchindex=c;
								 System.out.println("value matched in loop "+check+","+catchindex);
							 }
							 else{
								 System.out.println("value not matched in loop");
							 }
						 }
						 if(check==1){
							 System.out.println("value had been found");
							 System.out.println("index caught is "+catchindex);
							 System.out.println(output2+ " value at that index "+hostdetails.get(catchindex).toString());
							 System.out.println("removing previous values");
							 //hostdetails.remove(catchindex);
							 System.out.println("adding new value "+output2);
							 hostdetails.set(catchindex, output2);
							 System.out.println("new value is "+hostdetails.get(catchindex).toString()+"\n");
						 }
						 else{
							 System.out.println("not present in list so adding");
							 hostdetails.add(output2);
						 }
					 }
				}
				
				System.out.println("size of list "+hostdetails.size());
				for(int d=0;d<hostdetails.size();d++){
					System.out.println("\n"+"output is "+hostdetails.get(d).toString());
					String hostinput=hostdetails.get(d).toString();
					if(hostinput.contains("-")){
					Integer hdash=hostinput.indexOf("-");
					Integer hostlength=hostinput.length();
					String hostname=hostinput.substring(0, hdash);
					String xandy=hostinput.substring(hdash, hostlength);
					Integer hostx=xandy.indexOf("x");
					Integer hcol=xandy.indexOf(":");
					Integer hosty=xandy.indexOf("y");
					Integer xylength=xandy.length();
					String hostxc=xandy.substring(hostx+1, hcol);
					String hostyc=xandy.substring(hosty+1, xylength);
					System.out.println("values to be entered are :"+hostname+",xy"+hostxc+":"+hostyc);
					//adding host values
					Statement stmt5 = conn.createStatement();
					String SQL5=String.format("INSERT INTO "+canvasname+" VALUES('host', '"+hostname+"','-', '"+hostxc+"', '"+hostyc+"')");
					stmt5.executeUpdate(SQL5);
					}
					else{
						String hostname=hostinput;
						//adding switch values
						Statement stmt5b = conn.createStatement();
						String SQL5b=String.format("INSERT INTO "+canvasname+" VALUES('host', '"+hostname+"','-', '100', '100')");
						stmt5b.executeUpdate(SQL5b);
					}
				}
		}}
		catch(Exception e){
			e.printStackTrace();
		}
				
				//getting link details
				
				String linkmed=linkdetinput;
				Integer ll=linkmed.length();
				linkmed=linkmed.substring(1, ll);
				while(!linkmed.isEmpty()){
					ResultSet rs=null;
					String sourcename, destinationame, srcx="", srcy="", dstx="", dsty="";
					if(linkmed.contains(" ")){
						 Integer space=linkmed.indexOf(" ");
						 Integer length=linkmed.length();
						 output=linkmed.substring(0, space);
						 System.out.println("output is "+output);
						 linkmed=linkmed.substring(space+1, length);
						 
						 //seperate src and dst
						 Integer srcn=output.indexOf("src=");
						 Integer dstn=output.indexOf("dst=");
						 Integer linlength=output.length();
						 sourcename=output.substring(srcn+4, dstn);
						 destinationame=output.substring(dstn+4, linlength);
						 System.out.println("-"+sourcename+"-"+destinationame+"-");
						 //checking for x and y cordinates
						 Statement stmt6 = conn.createStatement();
						 String SQL6 = "SELECT * FROM "+canvasname+";";
						 rs = stmt6.executeQuery(SQL6);
						 //for source and destination element
						 while(rs.next()){
							 if(rs.getObject(2).toString().equalsIgnoreCase(sourcename)){
								 srcx=rs.getObject(4).toString();
								 srcy=rs.getObject(5).toString();
							 }
							 if(rs.getObject(2).toString().equalsIgnoreCase(destinationame)){
								 dstx=rs.getObject(4).toString();
								 dsty=rs.getObject(5).toString();
							 }
						 }
						 }
						else{
							System.out.println("-"+linkmed+"-");
							output=linkmed;
							linkmed="";
							
							//seperate src and dst
							 Integer srcn=output.indexOf("src=");
							 Integer dstn=output.indexOf("dst=");
							 Integer linlength=output.length();
							 sourcename=output.substring(srcn+4, dstn);
							 destinationame=output.substring(dstn+4, linlength);
							 System.out.println("-"+sourcename+"-"+destinationame+"-");
							 
							 //checking for x and y cordinates
							 Statement stmt7 = conn.createStatement();
							 String SQL7 = "SELECT * FROM "+canvasname+";";
							 rs = stmt7.executeQuery(SQL7);
							 //for source and destination element
							 while(rs.next()){
								 if(rs.getObject(2).toString().equalsIgnoreCase(sourcename)){
									 srcx=rs.getObject(4).toString();
									 srcy=rs.getObject(5).toString();
								 }
								 if(rs.getObject(2).toString().equalsIgnoreCase(destinationame)){
									 dstx=rs.getObject(4).toString();
									 dsty=rs.getObject(5).toString();
								 }
							 }
						}
					//adding host values
					Statement stmt8 = conn.createStatement();
					System.out.println("INSERT INTO "+linktablename+" VALUES('"+sourcename+"', '"+srcx+"', '"+srcy+"', '"+destinationame+"', '"+dstx+"', '"+dsty+"')");
					String SQL8=String.format("INSERT INTO "+linktablename+" VALUES('"+sourcename+"', '"+srcx+"', '"+srcy+"', '"+destinationame+"', '"+dstx+"', '"+dsty+"')");
					stmt8.executeUpdate(SQL8);
				}
				
				conn.close();
				
				
	} catch (Exception e) {
		System.err.println("Got an exception! ");
		e.printStackTrace();
	}
		
}
}