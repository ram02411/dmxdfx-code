package service;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
 
public class test {
 
  public static void main(String[] argv) {
 
	System.out.println("-------- MySQL JDBC Connection Testing ------------");
 
	try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		System.out.println("Where is your MySQL JDBC Driver?");
		e.printStackTrace();
		return;
	}
 
	System.out.println("MySQL JDBC Driver Registered!");
	System.out.println("Process on");
	Connection connection = null;
 
	try {
		connection = DriverManager
		.getConnection("jdbc:mysql://localhost:3306/evolve","root", "3volve123!");
		//Statement st = connection.createStatement(); 
		System.out.println("Process on2");
		
        //String SQL = String.format("INSERT INTO FIREWALL_POLICY_BASE VALUES('902', '00:00:00:00:00:11', '034', '192.168.17.14', '192.168.17.02', 'tcp', '5343','ALLOW', '2012-12-12 08:09:10')");
        //st.executeUpdate(SQL);
 
	} catch (SQLException e) {
		System.out.println("Connection Failed! Check output console");
		e.printStackTrace();
		return;
	}
 
	if (connection != null) {
		  

		System.out.println("You made it, take control your database now!");
	
	} else {
		System.out.println("Failed to make connection!");
	}
  }
}