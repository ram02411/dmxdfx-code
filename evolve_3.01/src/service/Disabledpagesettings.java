package service;

import bean.Disabledpagebean;

public class Disabledpagesettings {
	
	public static void disall(){
		Disabledpagebean.setAdministratorli(0);
		Disabledpagebean.setAdministrator(0);
		Disabledpagebean.setAdminrole(0);
		Disabledpagebean.setDesign(0);
		Disabledpagebean.setCanvas(0);
		Disabledpagebean.setViewer(0);
		Disabledpagebean.setViewersnt(0);
		Disabledpagebean.setTags(0);
		Disabledpagebean.setControl(0);
		Disabledpagebean.setPolicybase(0);
		Disabledpagebean.setRulebase(0);
		Disabledpagebean.setPolicybasesnt(0);
		Disabledpagebean.setRulebasesnt(0);
		Disabledpagebean.setSimulate(0);
		Disabledpagebean.setFlowrector(0);
	}

	public static void disbledpages(String username){
		
		String page=UserRole.getdisabledpages(username);
		if(page.equals(null)){
			Disabledpagebean.setAdministratorli(1);
			Disabledpagebean.setAdministrator(1);
			Disabledpagebean.setAdminrole(1);
			Disabledpagebean.setDesign(1);
			Disabledpagebean.setCanvas(1);
			Disabledpagebean.setViewer(1);
			Disabledpagebean.setViewersnt(1);
			Disabledpagebean.setTags(1);
			Disabledpagebean.setControl(1);
			Disabledpagebean.setPolicybase(1);
			Disabledpagebean.setRulebase(1);
			Disabledpagebean.setPolicybasesnt(1);
			Disabledpagebean.setRulebasesnt(1);
			Disabledpagebean.setSimulate(1);
			Disabledpagebean.setSecurityrulebase(1);
			Disabledpagebean.setFlowrector(1);
			
			
			
		}
		if(page.contains(",")){
		String[] pages=page.split(",");
		for(int i=0;i<pages.length;i++){
			if(pages[i].equalsIgnoreCase("administratorli"))
				Disabledpagebean.setAdministratorli(2);
			
						
			if(pages[i].equalsIgnoreCase("administrator"))
				Disabledpagebean.setAdministrator(2);
			
			if(pages[i].equalsIgnoreCase("adminrole"))
				Disabledpagebean.setAdminrole(2);
			
			if(pages[i].equalsIgnoreCase("design"))
				Disabledpagebean.setDesign(2);
			
			if(pages[i].equalsIgnoreCase("canvas"))
				Disabledpagebean.setCanvas(2);
			
			if(pages[i].equalsIgnoreCase("viewer"))
				Disabledpagebean.setViewer(2);
			
			if(pages[i].equalsIgnoreCase("viewersnt"))
				Disabledpagebean.setViewersnt(2);
			
			if(pages[i].equalsIgnoreCase("tags"))
				Disabledpagebean.setTags(2);
			
			if(pages[i].equalsIgnoreCase("control"))
				Disabledpagebean.setControl(2);
			
			if(pages[i].equalsIgnoreCase("policybase"))
				Disabledpagebean.setPolicybase(2);
			
			if(pages[i].equalsIgnoreCase("rulebase"))
				Disabledpagebean.setRulebase(2);
			
			if(pages[i].equalsIgnoreCase("policybasesnt"))
				Disabledpagebean.setPolicybasesnt(2);
			if(pages[i].equalsIgnoreCase("rulebasesnt"))
				Disabledpagebean.setRulebasesnt(2);
			
			if(pages[i].equalsIgnoreCase("simulate"))
				Disabledpagebean.setSimulate(2);
			if(pages[i].equalsIgnoreCase("securityrulebase"))
			Disabledpagebean.setSecurityrulebase(2);
			
			if(pages[i].equalsIgnoreCase("flowrector"))
				Disabledpagebean.setFlowrector(2);
			
		}
		}
		if(!page.contains(",")){
			if(page.equalsIgnoreCase("administratorli"))
				Disabledpagebean.setAdministratorli(2);
			else
				Disabledpagebean.setAdministratorli(1);
			if(page.equalsIgnoreCase("administrator"))
				Disabledpagebean.setAdministrator(2);
			else
				Disabledpagebean.setAdministrator(1);
			if(page.equalsIgnoreCase("adminrole"))
				Disabledpagebean.setAdminrole(2);
			else
				Disabledpagebean.setAdminrole(1);
			if(page.equalsIgnoreCase("design"))
				Disabledpagebean.setDesign(2);
			else
				Disabledpagebean.setDesign(1);
			if(page.equalsIgnoreCase("canvas"))
				Disabledpagebean.setCanvas(2);
			else
				Disabledpagebean.setCanvas(1);
			if(page.equalsIgnoreCase("viewer"))
				Disabledpagebean.setViewer(2);
			else
				Disabledpagebean.setViewer(1);
			if(page.equalsIgnoreCase("viewersnt"))
				Disabledpagebean.setViewersnt(2);
			else
				Disabledpagebean.setViewersnt(1);
			if(page.equalsIgnoreCase("tags"))
				Disabledpagebean.setTags(2);
			else
				Disabledpagebean.setTags(1);
			if(page.equalsIgnoreCase("control"))
				Disabledpagebean.setControl(2);
			else
				Disabledpagebean.setControl(1);
			if(page.equalsIgnoreCase("policybase"))
				Disabledpagebean.setPolicybase(2);
			else
				Disabledpagebean.setPolicybase(1);
			if(page.equalsIgnoreCase("rulebase"))
				Disabledpagebean.setRulebase(2);
			else
				Disabledpagebean.setRulebase(1);
			if(page.equalsIgnoreCase("policybasesnt"))
				Disabledpagebean.setPolicybasesnt(2);
			else
				Disabledpagebean.setPolicybasesnt(1);
			if(page.equalsIgnoreCase("rulebasesnt"))
				Disabledpagebean.setRulebasesnt(2);
			else
				Disabledpagebean.setRulebasesnt(1);
			if(page.equalsIgnoreCase("simulate"))
				Disabledpagebean.setSimulate(2);
			else
				Disabledpagebean.setSimulate(1);
			if(page.equalsIgnoreCase("securityrulebase"))
				Disabledpagebean.setSecurityrulebase(2);
			else
				Disabledpagebean.setSecurityrulebase(1);
			if(page.equalsIgnoreCase("flowrector"))
				Disabledpagebean.setFlowrector(2);
			else
				Disabledpagebean.setFlowrector(1);
		}
	}
	
	
}
