package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bean.Rolebean;
import bean.Securityrule;

public class Securityrulebase {
	
	public static List<Securityrule> getallrules(){
		List<Securityrule> rules=new ArrayList<Securityrule>();
		ResultSet rs=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM SECURITYRULES;"));

			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM SECURITYRULES;";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				Securityrule sr=new Securityrule();
				String rulename=rs.getObject(1).toString();
				String ruletype=rs.getObject(2).toString();
				String ruledesc=rs.getObject(3).toString();
				String ruletags=rs.getObject(4).toString();
				
				
				sr.setName(rulename);
				sr.setType(ruletype);
				sr.setDesc(ruledesc);
				sr.setTags(ruletags);
				rules.add(sr);
				}
				
				
				
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return rules;
		
	}
	
	
	
public static void updaterule(String name,String type,String desc,String tags,String rulename){
		
		name="'"+name+"'";
		desc="'"+desc+"'";
		type="'"+type+"'";
		tags="'"+tags+"'";
		rulename="'"+rulename+"'";
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2");
			String query1="UPDATE SECURITYRULES SET Rulename="+name+",Description="+desc+",RUletype="+type+",Tags="+tags+" WHERE Rulename="+rulename+";";
			System.out.println(query1);
		st.executeUpdate(query1);
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void deleterule(String name){
		ResultSet rs=null;
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2"+name);
			String query1="DELETE  FROM SECURITYRULES WHERE Rulename='"+name+"';";
			System.out.println(st.executeUpdate(query1));
		st.executeUpdate(query1);
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
	
	public static Securityrule getrule(String name){
		Securityrule rule=new Securityrule();
		ResultSet rs=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM SECURITYRULES;"));

			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM SECURITYRULES WHERE Rulename='"+name+"';";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				
				String rulename=rs.getObject(1).toString();
				String ruletype=rs.getObject(2).toString();
				String ruledesc=rs.getObject(3).toString();
				String ruletags=rs.getObject(4).toString();
				
				rule.setName(rulename);
				rule.setType(ruletype);
				rule.setDesc(ruledesc);
				rule.setTags(ruletags);
				
			}
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return rule;
		
	}
	
	public static void cloneRule(String name){
		List<String> rule=new ArrayList<String>();
		ResultSet rs=null;
		ResultSet rs1=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM SECURITYRULES;"));

			
			Statement stmt = conn.createStatement();
			String query1 = "SELECT * FROM SECURITYRULES;";
			rs=stmt.executeQuery(query1);
			
			while(rs.next()){
				String rulename=rs.getObject(1).toString();
				if(rulename.contains(name)){
					rule.add(rulename);
				}
			}
			String query2="SELECT * FROM SECURITYRULES WHERE Rulename='"+name+"';";
			rs1=stmt.executeQuery(query2);
			String name1=name+"-"+Integer.toString(rule.size());
			String desc1=null;
			String type1=null;
			String tags1=null;
			while(rs1.next()){
			 desc1=rs1.getObject(3).toString();
			 type1=rs1.getObject(2).toString();
			 tags1=rs1.getObject(4).toString();
			
			System.out.println(name1+"-"+desc1+"-"+type1+"-"+tags1);
			}
			addrule(name1, type1, desc1, tags1);
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
 		
		
 		
	}
	
	
	public static void addrule(String name,String type,String desc,String tags){
		ResultSet rs=null;
		name="'"+name+"'";
		desc="'"+desc+"'";
		type="'"+type+"'";
		tags="'"+tags+"'";
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2");
			
			String query = "INSERT INTO SECURITYRULES VALUES("+name+","+type+","+desc+","+tags+");";
			st.executeUpdate(query);
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

}
