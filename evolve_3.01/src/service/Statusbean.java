package service;



public class Statusbean {
	
	private static int administrator;
	private static int adminrole;
	private static int dashboard;
	private static int controlleralerts;
	private static int canvas;
	private static int fetchcanvas;
	private static int viewer;
	private static int viewersnt;
	private static int tags;
	private static int policybase;
	private static int rulebase;
	private static int policybasesnt;
	private static int rulebasesnt;
	private static int simulate;
	private static int securityrulebase;	
	private static int flowrector;
	
	public static int getAdministrator() {
		return administrator;
	}
	public static void setAdministrator(int administrator) {
		Statusbean.administrator = administrator;
	}
	public static int getAdminrole() {
		return adminrole;
	}
	public static void setAdminrole(int adminrole) {
		Statusbean.adminrole = adminrole;
	}
	public static int getDashboard() {
		return dashboard;
	}
	public static void setDashboard(int dashboard) {
		Statusbean.dashboard = dashboard;
	}
	public static int getControlleralerts() {
		return controlleralerts;
	}
	public static void setControlleralerts(int controlleralerts) {
		Statusbean.controlleralerts = controlleralerts;
	}
	public static int getCanvas() {
		return canvas;
	}
	public static void setCanvas(int canvas) {
		Statusbean.canvas = canvas;
	}
	public static int getFetchcanvas() {
		return fetchcanvas;
	}
	public static void setFetchcanvas(int fetchcanvas) {
		Statusbean.fetchcanvas = fetchcanvas;
	}
	public static int getViewer() {
		return viewer;
	}
	public static void setViewer(int viewer) {
		Statusbean.viewer = viewer;
	}
	public static int getViewersnt() {
		return viewersnt;
	}
	public static void setViewersnt(int viewersnt) {
		Statusbean.viewersnt = viewersnt;
	}
	
	
	public static int getTags() {
		return tags;
	}
	public static void setTags(int tags) {
		Statusbean.tags = tags;
	}
	public static int getPolicybase() {
		return policybase;
	}
	public static void setPolicybase(int policybase) {
		Statusbean.policybase = policybase;
	}
	public static int getRulebase() {
		return rulebase;
	}
	public static void setRulebase(int rulebase) {
		Statusbean.rulebase = rulebase;
	}
	public static int getPolicybasesnt() {
		return policybasesnt;
	}
	public static void setPolicybasesnt(int policybasesnt) {
		Statusbean.policybasesnt = policybasesnt;
	}
	public static int getRulebasesnt() {
		return rulebasesnt;
	}
	public static void setRulebasesnt(int rulebasesnt) {
		Statusbean.rulebasesnt = rulebasesnt;
	}
	public static int getSimulate() {
		return simulate;
	}
	public static void setSimulate(int simulate) {
		Statusbean.simulate = simulate;
	}
	
	
	public static int getSecurityrulebase() {
		return securityrulebase;
	}
	public static void setSecurityrulebase(int securityrulebase) {
		Statusbean.securityrulebase = securityrulebase;
	}
	public static int getFlowrector() {
		return flowrector;
	}
	public static void setFlowrector(int flowrector) {
		Statusbean.flowrector = flowrector;
	}
	
	


}
