package service;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bean.Rolebean;



public class UserRole {
	public static String getreadpage(String username){
		ResultSet rs=null;
		ResultSet rs1=null;
		username="'"+username+"'";
		String rolename = null;
		String pages=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
		

			Statement st = conn.createStatement();

			System.out.println("Process on2");
			 
				
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM CREDENTIALS WHERE USERNAME="+username+";";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				rolename=rs.getObject(3).toString();
							
			}
			if(rolename!=null){
				
				String query1="SELECT READONLY FROM ROLES WHERE NAME='"+rolename+"';";
				rs1=stmt.executeQuery(query1);
				while(rs1.next()){
					pages=rs1.getObject(1).toString();
				}
				
				
			}
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return pages;
	}

	public static void updaterole(String name,String desc,String readonly,String disabled,String rolename){
		
		name="'"+name+"'";
		desc="'"+desc+"'";
		readonly="'"+readonly+"'";
		disabled="'"+disabled+"'";
		rolename="'"+rolename+"'";
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2");
			String query1="UPDATE ROLES SET NAME="+name+",DESCRIPTION="+desc+",READONLY="+readonly+",DISABLED="+disabled+" WHERE NAME="+rolename+";";
			System.out.println(query1);
		st.executeUpdate(query1);
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static List<Rolebean> getallroles(){
		List<Rolebean> roles=new ArrayList<Rolebean>();
		ResultSet rs=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM ROLES;"));

			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ROLES;";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				Rolebean rb=new Rolebean();
				String roleid=rs.getObject(1).toString();
				String rolename=rs.getObject(2).toString();
				String roledesc=rs.getObject(3).toString();
				rb.setRole_id(roleid);
				rb.setRole_name(rolename);
				rb.setDesc(roledesc);
				if(!rolename.equalsIgnoreCase("superuser")){ 
						if(!rolename.equalsIgnoreCase("superuserreadonly"))
				roles.add(rb);
				}
				
				
				
			}
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return roles;
		
	}
	
	public static Rolebean getrole(String name){
		Rolebean role=new Rolebean();
		ResultSet rs=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM ROLES;"));

			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ROLES WHERE NAME='"+name+"';";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				
				String roleid=rs.getObject(1).toString();
				String rolename=rs.getObject(2).toString();
				String roledesc=rs.getObject(3).toString();
				String readonly=rs.getObject(4).toString();
				String disabled=rs.getObject(5).toString();
				role.setRole_id(roleid);
				role.setRole_name(rolename);
				role.setDesc(roledesc);
				role.setReadonly(readonly);
				role.setDisabled(disabled);			
				
			}
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return role;
		
	}
	
	public static void deleterole(String name){
		ResultSet rs=null;
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2"+name);
			String query1="DELETE  FROM ROLES WHERE NAME='"+name+"';";
			System.out.println(st.executeUpdate(query1));
		st.executeUpdate(query1);
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void cloneRole(String name){
		List<String> role=new ArrayList<String>();
		ResultSet rs=null;
		ResultSet rs1=null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM ROLES;"));

			
			Statement stmt = conn.createStatement();
			String query1 = "SELECT * FROM ROLES;";
			rs=stmt.executeQuery(query1);
			
			while(rs.next()){
				String rolename=rs.getObject(2).toString();
				if(rolename.contains(name)){
					role.add(rolename);
				}
			}
			String query2="SELECT * FROM ROLES WHERE NAME='"+name+"';";
			rs1=stmt.executeQuery(query2);
			String name1=name+"-"+Integer.toString(role.size());
			String desc1=null;
			String readonly1=null;
			String disabled=null;
			while(rs1.next()){
			 desc1=rs1.getObject(3).toString();
			 readonly1=rs1.getObject(4).toString();
			disabled=rs1.getObject(5).toString();
			System.out.println(name1+"-"+desc1+"-"+readonly1+"-"+disabled);
			}
			addrole(name1,desc1, readonly1, disabled);
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
 		
		
 		
	}
	
	
	
	public static String getdisabledpages(String username){
		ResultSet rs=null;
		ResultSet rs1=null;
		String disabledpages = null;
		try{
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();
			System.out.println("Process on2");			 
			System.out.println(st
					.executeQuery("SELECT * FROM ROLES;"));

			
			Statement stmt = conn.createStatement();
			String query1 = "SELECT ROLENAME FROM CREDENTIALS where USERNAME='"+username+"';";
			rs1=stmt.executeQuery(query1);
			String rolename=null;
			while(rs1.next()){
				rolename=rs1.getObject(1).toString();
			}
			if(rolename!=null){
			String query = "SELECT DISABLED FROM ROLES where NAME='"+rolename+"';";
			rs = stmt.executeQuery(query);
			while(rs.next()){
			disabledpages=rs.getObject(1).toString();	
				
			}
			}
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		return disabledpages;
	}
	
	public static void addrole(String name,String desc,String readonly,String disabled){
		ResultSet rs=null;
		name="'"+name+"'";
		desc="'"+desc+"'";
		readonly="'"+readonly+"'";
		disabled="'"+disabled+"'";
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2");
			String query1="SELECT * FROM ROLES;";
			rs=st.executeQuery(query1);
			String id = null;
			while(rs.next()){
				id=rs.getObject(1).toString();	
			}
			int roleid=Integer.parseInt(id)+1;
			String query = "INSERT INTO ROLES VALUES("+roleid+","+name+","+desc+","+readonly+","+disabled+");";
			st.executeUpdate(query);
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

}
