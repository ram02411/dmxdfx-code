package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import sources.excelWriter;

import FlowPusher.flowPusher;
import bean.Policybean;
import bean.Rulebean;
import bean.Policyschedulebean;

public class PolicyBase {
	static ResultSet rs = null;

	/**
	 * @param args
	 * @param user
	 * @param pass
	 * @return
	 */

	public static void printresultexcel() {
		HashMap<String, String> params = excelWriter.readDict();
		try {

			System.out.println("Process enter excel");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			Statement st = conn.createStatement();

			System.out.println("Process enter2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICY_BASE;";
			rs = stmt.executeQuery(query);

			String POLICYID = null;
			if(!(rs.next())){
				POLICYID = "1001";
			}
			else{
				while(rs.next()){
				POLICYID = rs.getObject(1).toString();
				}
			}
			/*while (rs.next()) {
				
			}*/
			Integer out = Integer.parseInt(POLICYID) + 1;
			Integer policyid = out;
			String switchnum = params.get("SWITCH DPID");
			String inter = params.get("IN PORT");
			String sourceip = params.get("Source IP");
			String destip = params.get("Destination IP");
			String protocol = params.get("Protocol");
			String port = params.get("tp_destination");
			String action = params.get("ACTION");

			Date datetime1 = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss ");
			System.out.println(ft.format(datetime1));
			String datetime = ft.format(datetime1);

			String SQL1 = String
					.format("INSERT INTO FIREWALL_POLICY_BASE VALUES('"
							+ policyid + "', '" + switchnum + "', '" + inter
							+ "', '" + sourceip + "', '" + destip + "', '"
							+ protocol + "', '" + port + "','" + action
							+ "', '" + datetime + "'" + ", 'NOT VALIDATED'"
							+ ")");
			System.out.println("entered");
			System.out.println(protocol);
			st.executeUpdate(SQL1);
			conn.close();
		} catch (Exception e) {
			Loggerdb.loggingdb("INFO", "Database", "Insert Database Error", "Unable to insert policy from the external file.");
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}

	public static List<Policybean> printresult() {
		List<String> ls = new ArrayList<String>();
		List<Policybean> bn = new ArrayList<Policybean>();

		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			/*String user = "root";
			String pass = "3volve123!";
			String policyid = "914";
			String switchnum = "00:00:00:00:00:11";
			String inter = "035";
			String sourceip = "192.168.17.1";
			String destip = "192.168.17.2";
			String protocol = "icmp";
			String port = "3449";
			String action = "ALLOW";
			String validation = "NOT VALIDATED";
			String datetime = "2012-12-12 08:09:10";*/
			// String polid="900";

			Statement st = conn.createStatement();

			System.out.println("Process on2");
			// String SQL1 =
			// String.format("INSERT INTO FIREWALL_POLICY_BASE VALUES('"+policyid+"', '"+switchnum+"', '"+inter+"', '"+sourceip+"', '"+destip+"', '"+protocol+"', '"+port+"','"+action+"', '"+datetime+"')");
			// st.executeUpdate(SQL1);
			// String SQL2 =
			// String.format("DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="+polid);
			// st.executeUpdate(SQL2);
			System.out.println(st
					.executeQuery("SELECT * FROM FIREWALL_POLICY_BASE"));

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICY_BASE;";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				Policybean pbean = new Policybean();
				String POLICYID = rs.getObject(1).toString();
				pbean.setPolicyid(POLICYID);
				String SWITCHID = rs.getObject(2).toString();
				pbean.setSwitchnum(SWITCHID);
				try{
					String INTERFACE="";
					if(String.valueOf(rs.getObject(3).toString()).equalsIgnoreCase("")){
					INTERFACE = "N/A";
					}
					else{
						INTERFACE=rs.getObject(3).toString();
					}
					pbean.setInter(INTERFACE);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String SOURCEIP="";
					if(String.valueOf(rs.getObject(4).toString()).equalsIgnoreCase("")){
					SOURCEIP = "N/A";
					}
					else{
						SOURCEIP=rs.getObject(4).toString();
					}
					pbean.setSourceip(SOURCEIP);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String DESTINATIONIP="";
					if(String.valueOf(rs.getObject(5).toString()).equalsIgnoreCase("")){
					DESTINATIONIP = "N/A";
					}
					else{
						DESTINATIONIP=rs.getObject(5).toString();
					}
					pbean.setDestip(DESTINATIONIP);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String PROTOCOL="";
					if(String.valueOf(rs.getObject(6).toString()).equalsIgnoreCase("")){
					PROTOCOL = "N/A";
					}
					else{
						PROTOCOL=rs.getObject(6).toString();
					}
					pbean.setProtocol(PROTOCOL);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String PORT="";
					if(String.valueOf(rs.getObject(7).toString()).equalsIgnoreCase("")){
					PORT = "N/A";
					}
					else{
						PORT=rs.getObject(7).toString();
					}
					pbean.setPort(PORT);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String PRIORITY="";
					if(String.valueOf(rs.getObject(8).toString()).equalsIgnoreCase("")){
					PRIORITY = "N/A";
					}
					else{
						PRIORITY=rs.getObject(8).toString();
					}
					pbean.setPriority(PRIORITY);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				String ACTION = rs.getObject(9).toString();
				pbean.setAction(ACTION);
				String DATETIME = rs.getObject(10).toString();
				pbean.setDatetime(DATETIME);
				String VALIDATION = rs.getObject(11).toString();
				pbean.setValidation(VALIDATION);
				// System.out.println("VALUES ARE:" + "\n" +POLICYID+ "\n"
				// +SWITCHID+ "\n" +INTERFACE+ "\n" +SOURCEIP+ "\n"
				// +DESTINATIONIP+ "\n" +PROTOCOL+ "\n" +PORT+ "\n" +ACTION+
				// "\n" +DATETIME);
				// ls.add(POLICYID +SWITCHID +INTERFACE +SOURCEIP+DESTINATIONIP
				// +PROTOCOL +PORT +ACTION +DATETIME);
				bn.add(pbean);
			}
			System.out.println(ls.size());
			conn.close();

		} catch (Exception e) {
			Loggerdb.loggingdb("INFO", "Database", "Fetch Database Error", "Unable to fetch the list of policies from databse.");
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return bn;
	}

	public static void addvalue(Policybean pbean) {
		List<String> ls = new ArrayList<String>();
		List<Policybean> bn = new ArrayList<Policybean>();
		try {
			System.out.println("Process enter");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			Statement st = conn.createStatement();

			System.out.println("Process enter2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICY_BASE;";
			rs = stmt.executeQuery(query);
			
			Integer out=0;
			
			 String POLICYID = null;
		       while (rs.next()) {
		         POLICYID = rs.getObject(1).toString();
		         System.out.println("PID is :"+POLICYID);
		       }
		       if(POLICYID==null){
		    	   POLICYID="1000";
		    	   }
		      out = Integer.valueOf(Integer.parseInt(POLICYID) + 1);
			//out = Integer.parseInt(POLICYID) + 1;
			Integer policyid = out;
			String switchnum = pbean.getSwitchnum();
			String inter = pbean.getInter();
			String sourceip = pbean.getSourceip();
			String destip = pbean.getDestip();
			String protocol = pbean.getProtocol();
			String port = pbean.getPort();
			String action = pbean.getAction();
			String priority=pbean.getPriority();
					

			Date datetime1 = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss ");
			System.out.println(ft.format(datetime1));
			String datetime = ft.format(datetime1);

			String SQL1 = String
					.format("INSERT INTO FIREWALL_POLICY_BASE VALUES('"
							+ policyid + "', '" + switchnum + "', '" + inter
							+ "', '" + sourceip + "', '" + destip + "', '"
							+ protocol + "', '" + port + "', '" + priority + "','" + action
							+ "', '" + datetime + "'" + ", 'TO BE VALIDATED'"
							+ ")");
			System.out.println("entered");
			System.out.println(protocol);
			st.executeUpdate(SQL1);
			conn.close();
		} catch (Exception e) {
			Loggerdb.loggingdb("SEVERE", "Database", "Insert Error", "Unable to create polocy from entered values.");
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}

	public static void deleterow(Policybean pbean) {
		List<Policybean> bn = new ArrayList<Policybean>();
		try {
			System.out.println("Process del1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			Statement st = conn.createStatement();

			System.out.println("Process del2");
			String pid = pbean.getPolicyid();

			System.out
					.println("DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
							+ pid);
			Statement stmt1 = conn.createStatement();
			String query = "DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
					+ pid;
			stmt1.executeUpdate(query);
			System.out.println("policy with " + pid + " has been deleted");
			conn.close();
		} catch (Exception e) {
			Loggerdb.loggingdb("INFO", "Database", "Execute Databse Error", "Unable to delete policy from database.");
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}

	public static void addtorulebase(Policybean pbean) {
		ResultSet rb = null;
		Rulebean ruleb = new Rulebean();
		try {
			System.out.println("Process pnow1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			System.out.println("Process pnow2");
			String pid = pbean.getPolicyid();

			System.out
					.println("SELECT * FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
							+ pid);
			Statement stmt1 = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
					+ pid;
			rb = stmt1.executeQuery(query);
			if (rb.next()) {
				String Switch = rb.getObject(2).toString();
				ruleb.setRswitchnum(Switch);
				System.out.println(Switch);
				String inter = rb.getObject(3).toString();
				ruleb.setRinter(inter);
				String sourceip = rb.getObject(4).toString();
				ruleb.setRsourceip(sourceip);
				String destinip = rb.getObject(5).toString();
				ruleb.setRdestip(destinip);
				String proto = rb.getObject(6).toString();
				ruleb.setRprotocol(proto);
				String port = rb.getObject(7).toString();
				ruleb.setRport(port);
				String priority = rb.getObject(8).toString();
				System.out.println("priority :- "+priority);
				ruleb.setPriority(priority);
				String action = rb.getObject(9).toString();
				ruleb.setRaction(action);
			}
			// String POLICYID = rb.getObject(2).toString();
//pushing to HP
			
			
			String priority = ruleb.getPriority();
			String dpid =  ruleb.getRswitchnum();
			String src_ip = ruleb.getRsourceip();
			String dst_ip =  ruleb.getRdestip();
			String in_port =  ruleb.getRinter();
			String protocol = ruleb.getRprotocol();
			String dst_port = ruleb.getRport();
			String action = ruleb.getRaction();

			String output ="{";
					output = output + "\"flow\":{";
			output= output+ "\"priority\":"+priority+",";
			output = output+ "\"match\": [";
			if(String.valueOf(in_port).equalsIgnoreCase("")||in_port==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"in_port\":"+ in_port +"},";
			}
			
			if(String.valueOf(src_ip).equalsIgnoreCase("")||src_ip==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"ipv4_src\":"+"\""+src_ip+"\"" +"},";
			}
			
			if(String.valueOf(dst_ip).equalsIgnoreCase("")||dst_ip==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"ipv4_dst\":"+"\""+ dst_ip+"\"" +"},";
			}
			
			if(String.valueOf(protocol).equalsIgnoreCase("")||protocol==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"ip_proto\":"+"\""+ protocol+"\"" +"},";
			}
			//if(protocol!=null)
			//{
			// output = output+"{ \"tcp_dst\":"+ dst_port+"}";
			//}
            output = output+"{ \"eth_type\":"+"\"ipv4\"" +"}";
			output=output+"],";

			output = output+ "\"actions\": [";

			if(action.equals("DENY"))
			{
			
			}
			else
			{
			 output = output +"{ \"output\":\""+action+"\"}";
			}
			output=output+"]}}";
			System.out.println(output);
			String res = flowPusher.pushFlows(dpid, output);
			System.out.println(res);
			
			
			
			
			//push to HP COMPLETE
			Statement stmt2 = conn.createStatement();
			Statement st3 = conn.createStatement();
			ResultSet rb2 = null, rmax=null;
			String query2 = "SELECT * FROM FIREWALL_RULE_BASE ORDER BY RULE_ID DESC;";
			rb2 = stmt2.executeQuery(query2);

			String rid = null;
			if(!(rb2.next())) {
				System.out.println("default rule id 11001");
				rid="11001";
			}
			else{
				//String selectmax="SELECT * FROM FIREWALL_RULE_BASE ORDER BY RULE_ID DESC;";
				//rmax= st3.executeQuery(selectmax);
				while(rb2.next()){
				rid = rb2.getObject(1).toString();
				}
				System.out.println("max rule id is "+rid);
			}
			Integer out = Integer.parseInt(rid) + 1;

			Integer ruleid = out;
			String ruleswitch = ruleb.getRswitchnum();
			String ruleinter = ruleb.getRinter();
			String rulesrcip = ruleb.getRsourceip();
			String rdestip = ruleb.getRdestip();
			String rproto = ruleb.getRprotocol();
			String rport = ruleb.getRport();
			String raction = ruleb.getRaction();
			String rpriority = ruleb.getPriority();

			Date datetime1 = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd  hh:mm:ss ");
			System.out.println(ft.format(datetime1));
			String rdatetime = ft.format(datetime1);

			Statement stpush = conn.createStatement();

			String SQLpush = String
					.format("INSERT INTO FIREWALL_RULE_BASE VALUES('" + ruleid
							+ "', '" + ruleswitch + "', '" + ruleinter + "', '"
							+ rulesrcip + "', '" + rdestip + "', '" + rproto
							+ "', '" + rport + "', '"+ rpriority + "','" + raction + "', '"
							+ rdatetime + "'" + ")");
			stpush.executeUpdate(SQLpush);
			System.out.println("entered");
			System.out.println(raction);
			System.out.println(ruleid);
			System.out.println(ruleswitch);
			System.out.println(rproto);
			// st.executeUpdate(SQL1);

			// DELETING FROM ORIGINAL POLICYBASE TABLE
			String delpid = pbean.getPolicyid();
			System.out
					.println("DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
							+ delpid);
			Statement stmt3 = conn.createStatement();
			String query3 = "DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
					+ delpid;
			stmt3.executeUpdate(query3);
			System.out.println("policy with " + delpid + " has been deleted");
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}

	public static void validate(Policybean pbean) {
		ResultSet rb = null;
		ResultSet rb2 = null;
		//Rulebean ruleb = new Rulebean();
		String Switch = null;
		String inter = null;
		String sourceip = null;
		String destinip = null;
		String proto = null;
		String port = null;
		String priority=null;
		String action=null;
		//boolean vali = false;
		Integer i = 0,j=0;
		try {
			System.out.println("Process val1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			System.out.println("Process val2");
			String pid = pbean.getPolicyid();

			System.out
					.println("SELECT * FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
							+ pid);
			Statement stmt1 = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
					+ pid;
			rb = stmt1.executeQuery(query);
			if (rb.next()) {
				Switch = rb.getObject(2).toString();
				inter = rb.getObject(3).toString();
				sourceip = rb.getObject(4).toString();
				destinip = rb.getObject(5).toString();
				proto = rb.getObject(6).toString();
				port = rb.getObject(7).toString();
				priority=rb.getObject(8).toString();
				action=rb.getObject(9).toString();
			}
			// String POLICYID = rb.getObject(2).toString();

			Statement stmtval = conn.createStatement();
			String query2 = "SELECT * FROM FIREWALL_RULE_BASE;";
			rb2 = stmtval.executeQuery(query2);
			
			j=0;
			while (rb2.next()) {
				i=0;
				System.out.println("original switch is"+Switch);
				System.out.println("rule switch is="+rb.getObject(2).toString());
				if (Switch.compareToIgnoreCase(rb2.getObject(2).toString()) == 0){
					i = i + 1;
					System.out.println("I="+i);
				}
				System.out.println("I="+i);
				if (inter.compareToIgnoreCase(rb2.getObject(3).toString()) == 0) {
					i = i + 1;
					System.out.println("I="+i);
				}
				if (sourceip.compareToIgnoreCase(rb2.getObject(4).toString()) == 0) {
					i = i + 1;
					System.out.println("I="+i);
				}
				if (destinip.compareToIgnoreCase(rb2.getObject(5).toString()) == 0) {
					i = i + 1;
					System.out.println("I="+i);
				}
				if (proto.compareToIgnoreCase(rb2.getObject(6).toString()) == 0) {
					i = i + 1;
					System.out.println("I="+i);
				}
				if (port.compareToIgnoreCase(rb2.getObject(7).toString()) == 0) {
					i = i + 1;
					System.out.println("I="+i);
				}
				if(priority.compareToIgnoreCase(rb2.getObject(8).toString()) == 0){
					i = i + 1;
					System.out.println("I="+i);
				}
				if(action.compareToIgnoreCase(rb2.getObject(9).toString()) == 0){
					i = i + 1;
					System.out.println("I="+i);
				}
				if(i==8){
					j=j+1;
					System.out.println("ruleID hwere it matches is ="+rb2.getObject(1).toString());
				}
			}
			System.out.println("j="+j);
			
			if(j>=1){
				Statement stmtnotvalidated = conn.createStatement();
				String queryval1 = "UPDATE FIREWALL_POLICY_BASE SET VALIDATIONS='NOT VALIDATED' WHERE POLICY_ID="+pid;
				stmtnotvalidated.executeUpdate(queryval1);
			}
			else {
				Statement stmtvalidated = conn.createStatement();
				String queryval2 = "UPDATE FIREWALL_POLICY_BASE SET VALIDATIONS='TO BE IMPLEMENTED' WHERE POLICY_ID="+pid;
				stmtvalidated.executeUpdate(queryval2);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}
	
	
	public static void policyschedule(Policybean pbean) {
		ResultSet rb = null;
		Policyschedulebean pschbean=new Policyschedulebean();
		try {
			System.out.println("Process sch1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			System.out.println("Process sch2");
			String pid = pbean.getPolicyid();

			System.out
					.println("SELECT * FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
							+ pid);
			Statement stmt1 = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
					+ pid;
			rb = stmt1.executeQuery(query);
			String Switch="", inter="", sourceip="", destinip="", proto="", port="", ipriority="",iaction="";
			if (rb.next()) {
				Switch = rb.getObject(2).toString();
				pschbean.setSwitchsch(Switch);
				inter = rb.getObject(3).toString();
				pschbean.setIntersch(inter);
				sourceip = rb.getObject(4).toString();
				pschbean.setSourcipsch(sourceip);
				destinip = rb.getObject(5).toString();
				pschbean.setDestinipsch(destinip);
				proto = rb.getObject(6).toString();
				pschbean.setProtosch(proto);
				port = rb.getObject(7).toString();
				pschbean.setPortsch(port);
				ipriority=rb.getObject(8).toString();
				pschbean.setPriority(ipriority);
				iaction = rb.getObject(9).toString();
				pschbean.setActionsch(iaction);
			}
			// String POLICYID = rb.getObject(2).toString();

			//hp schedule
			
			
			String priority = ipriority;
			String dpid =  Switch;
			String src_ip = sourceip;
			String dst_ip =  destinip;
			String in_port =  inter;
			String protocol = proto;
			String dst_port = port;
			String action = iaction;

			String output ="{";
					output = output + "\"flow\":{";
			output= output+ "\"priority\":"+priority+",";
			output = output+ "\"match\": [";
			if(String.valueOf(in_port).equalsIgnoreCase("")||in_port==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"in_port\":"+ in_port +"},";
			}
			
			if(String.valueOf(src_ip).equalsIgnoreCase("")||src_ip==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"ipv4_src\":"+"\""+src_ip+"\"" +"},";
			}
			
			if(String.valueOf(dst_ip).equalsIgnoreCase("")||dst_ip==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"ipv4_dst\":"+"\""+ dst_ip+"\"" +"},";
			}
			
			if(String.valueOf(protocol).equalsIgnoreCase("")||protocol==null)
			{
			 output = output;
			}
			else{
				output = output+"{ \"ip_proto\":"+"\""+ protocol+"\"" +"},";
			}
			//if(protocol!=null)
			//{
			// output = output+"{ \"tcp_dst\":"+ dst_port+"}";
			//}
            output = output+"{ \"eth_type\":"+"\"ipv4\"" +"}";
			output=output+"],";

			output = output+ "\"actions\": [";

			if(action.equals("DENY"))
			{
			
			}
			else
			{
			 output = output +"{ \"output\":"+action+"}";
			}
			output=output+"]}}";
			 
			String inputdate=Policyschedulebean.getDatetimeschhp();
			hpSchedule hps = new hpSchedule(dpid,output, inputdate);
			
			
			//hpschedule-end
			
			Statement stmt2 = conn.createStatement();
			ResultSet rb2 = null;
			String query2 = "SELECT * FROM FIREWALL_POLICYSCH_BASE;";
			rb2 = stmt2.executeQuery(query2);

			String pschid = null;
			if(!(rb2.next())) {
				pschid="9011";
			}
			else{
				while(rb2.next()){
				pschid = rb2.getObject(1).toString();
				}
			}
			Integer out = Integer.parseInt(pschid) + 1;

			Integer policyschid = out;
			String schswitch = Switch;
			String schinter = inter;
			String schsrcip = sourceip;
			String schdestip = destinip;
			String schproto = proto;
			String schport = port;
			String schaction = action;
			String schpriority=priority;
			String schdatetime = pbean.getDatetime();

			Statement stsch = conn.createStatement();

			String SQLpush = String
					.format("INSERT INTO FIREWALL_POLICYSCH_BASE VALUES('" + policyschid
							+ "', '" + schswitch + "', '" + schinter + "', '"
							+ schsrcip + "', '" + schdestip + "', '" + schproto
							+ "', '" + schport + "', '"+ schpriority + "','" + schaction + "', '"
							+ schdatetime + "'" + ")");
			stsch.executeUpdate(SQLpush);
			System.out.println("entered");
			System.out.println(policyschid);
			System.out.println(schsrcip);
			System.out.println(schproto);
			System.out.println(schaction);
			// st.executeUpdate(SQL1);

			// DELETING FROM ORIGINAL POLICYBASE TABLE
			String delpid = pbean.getPolicyid();
			System.out
					.println("DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
							+ delpid);
			Statement stmt3 = conn.createStatement();
			String query3 = "DELETE FROM FIREWALL_POLICY_BASE WHERE POLICY_ID="
					+ delpid;
			stmt3.executeUpdate(query3);
			System.out.println("policy with " + delpid + " has been deleted");
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}
	
	public static List<Policyschedulebean> printschpolicy() {
		List<String> ls = new ArrayList<String>();
		List<Policyschedulebean> scbn = new ArrayList<Policyschedulebean>();
		
		try {
			System.out.println("Process on1sch");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		

			Statement st = conn.createStatement() ;

			System.out.println("Process on2sch");
			
			System.out.println(st
					.executeQuery("SELECT * FROM FIREWALL_POLICYSCH_BASE"));

			
			
				
			Statement st1=conn.createStatement();
			String query1= "DELETE FROM FIREWALL_POLICYSCH_BASE WHERE SCHEDULETIME < CURRENT_DATE();";
			System.out.println(query1);
			st1.executeUpdate(query1);
			st1.close();
			
			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_POLICYSCH_BASE;";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				Policyschedulebean psbean=new Policyschedulebean();
				String pschid=rs.getObject(1).toString();
				psbean.setPolicyschid(pschid);
				String schswitch=rs.getObject(2).toString();
				psbean.setSwitchsch(schswitch);
				try{
					String schinter="";
					if(String.valueOf(rs.getObject(3).toString()).equalsIgnoreCase("")){
					schinter = "N/A";
					}
					else{
						schinter=rs.getObject(3).toString();
					}
					psbean.setIntersch(schinter);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String schsrcip="";
					if(String.valueOf(rs.getObject(4).toString()).equalsIgnoreCase("")){
					schsrcip = "N/A";
					}
					else{
						schsrcip=rs.getObject(4).toString();
					}
					psbean.setSourcipsch(schsrcip);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String schdstip="";
					if(String.valueOf(rs.getObject(5).toString()).equalsIgnoreCase("")){
					schdstip = "N/A";
					}
					else{
						schdstip=rs.getObject(5).toString();
					}
					psbean.setDestinipsch(schdstip);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String schprotocol="";
					if(String.valueOf(rs.getObject(6).toString()).equalsIgnoreCase("")){
					schprotocol = "N/A";
					}
					else{
						schprotocol=rs.getObject(6).toString();
					}
					psbean.setProtosch(schprotocol);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				try{
					String schport="";
					if(String.valueOf(rs.getObject(7).toString()).equalsIgnoreCase("")){
					schport = "N/A";
					}
					else{
						schport=rs.getObject(7).toString();
					}
					psbean.setPortsch(schport);
				}
				catch(Exception e){
					//e.printStackTrace();
				}
				String schpriority=rs.getObject(8).toString();
				psbean.setPriority(schpriority);
				String schaction=rs.getObject(9).toString();
				psbean.setActionsch(schaction);
				String schdt=rs.getObject(10).toString();
				psbean.setRuledatesch(schdt);
				System.out.println("time is "+schdt);
				scbn.add(psbean);
			}
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return scbn;
	}
	

	private static String url = "jdbc:mysql://localhost:3306/evolve";
}
