package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import stateTable.stateTableHP;

import conversions.Datetime1;
import dto.Filteralerts;


import bean.Alertbean;

public class Alertsbase {
	static final Logger logger = Logger.getLogger( Alertsbase.class.getName() );
	static FileHandler fh; 
	static ResultSet rs=null;
	
	/*public static void enterlog(){
		 try {  

		        // This block configure the logger with handler and formatter  
		        fh = new FileHandler("/home/logs/evolve_log");  
		        logger.addHandler(fh);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  }
		 catch(Exception e)
		 {
			 
		 }
	}*/
	
	public static List<Alertbean> print() throws Exception{
		try {  

	        // This block configure the logger with handler and formatter  
	        fh = new FileHandler("/home/logs/evolve_log1");  
	        logger.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  }
	 catch(Exception e)
	 {
		 
	 }
		 List<Alertbean> tbn = new ArrayList<Alertbean>();
		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		
			
			Statement st1=conn.createStatement();
			String query1= "DELETE FROM ALERTS WHERE TIMESTAMP < DATE_SUB(CURRENT_DATE(), INTERVAL 10 DAY);";
			System.out.println(query1);
			st1.executeUpdate(query1);
			st1.close();
//			System.out.println("deleted previous alerts");
			
			
			Statement st = conn.createStatement();

			System.out.println("Process on2");
			 
			System.out.println(st
					.executeQuery("SELECT * FROM ALERTS"));

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ALERTS order by TIMESTAMP DESC;";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				Alertbean tb=new Alertbean();
				String ID = rs.getObject(1).toString();
				tb.setId(ID);
				String tv = rs.getObject(2).toString();
				Integer position=tv.indexOf(" ");
				Integer end=tv.length();
				String jt=tv.substring(position+1, end);
				tv=tv.substring(0, position);
				tb.setDatestamp(tv);
				tb.setTimestamp(jt);
				String sev=rs.getObject(3).toString();
				tb.setSeverity(sev);
				String topic=rs.getObject(4).toString();
				tb.setTopic(topic);
				String origin=rs.getObject(5).toString();
				tb.setOrg(origin);
				String descr=rs.getObject(6).toString();
				tb.setDescr(descr);
				tbn.add(tb);
			}
			
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
			//System.err.println("Got an exception! ");
			Loggerdb.loggingdb("SEVERE", "Database", "Fetch Database Error", "Unable to fetch alerts from Database");
			logger.log(Level.SEVERE, "Unable to fetch alerts.");
		}
		return tbn;
	}
	
	public static List<Alertbean> callalerts(List<String> se) {
		List<Alertbean> tbn = new ArrayList<Alertbean>();
		ResultSet ralerts=null;
		try {
			System.out.println("Process on1");
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

					
			Statement st1=conn.createStatement();
			String query1= "DELETE FROM ALERTS WHERE TIMESTAMP < DATE_SUB(CURRENT_DATE(), INTERVAL 10 DAY);";
			System.out.println(query1);
			st1.executeUpdate(query1);
			st1.close();
			//System.out.println("deleted previous alerts");
			
			
			System.out.println("Severity is "+Filteralerts.getSeverity());
			System.out.println("Topic is "+Filteralerts.getTopic());
			System.out.println("Origin is "+Filteralerts.getOrigin());
			System.out.println("previous query was "+Filteralerts.getQuery());
			System.out.println("start date  "+Filteralerts.getStartime());
			System.out.println("end time is "+Filteralerts.getEndime());
			System.out.println("query type is " + Filteralerts.getQuerytype());
			System.out.println(Filteralerts.getSortoutput()+" sortouput type ");
			
			String sortoutput=Filteralerts.getSortoutput();
			if(String.valueOf(sortoutput).equalsIgnoreCase("null")||String.valueOf(sortoutput).equalsIgnoreCase("")||sortoutput.isEmpty()||String.valueOf(sortoutput).equalsIgnoreCase(null)){
			System.out.println("ENTERED THE BIG BLOCK");
			String start=se.get(0);
			System.out.println("hey2");
			System.out.println(start);
			System.out.println("start is -"+ start + "- value");
			if(String.valueOf(start).equalsIgnoreCase("null")||String.valueOf(start).equalsIgnoreCase("")||start.isEmpty()||String.valueOf(start).equalsIgnoreCase(null)){
				System.out.println("start empty");
				start="All";				
				Filteralerts.setStartime("All");
			}
			else{
				start=start;
			}
			
			String end=se.get(1);
			if(String.valueOf(end).equals("null")||String.valueOf(end).equals("")||end.isEmpty()||String.valueOf(end).equals(null)){
				System.out.println("end empty");
				end="All";
				Filteralerts.setEndime("All");
			}
			else{
				end=end;
			}
			
			System.out.println("start is "+ start + " end is "+end);
			
			String severity=se.get(2);
			if(String.valueOf(severity).equalsIgnoreCase("null")||String.valueOf(severity).equalsIgnoreCase("")||severity.isEmpty()||String.valueOf(severity).equalsIgnoreCase(null)){
				System.out.println("severity empty");
				severity="All";
				Filteralerts.setSeverity("All");
			}
			String topic=se.get(3);
			if(String.valueOf(topic).equalsIgnoreCase("null")||String.valueOf(topic).equalsIgnoreCase("")||topic.isEmpty()||String.valueOf(topic).equalsIgnoreCase(null)){
				System.out.println("topic empty");
				topic="All";
				Filteralerts.setTopic("All");
			}
			String origin=se.get(4);
			if(String.valueOf(origin).equalsIgnoreCase("null")||String.valueOf(origin).equalsIgnoreCase("")||origin.isEmpty()||String.valueOf(origin).equalsIgnoreCase(null)){
				System.out.println("origin empty");
				origin="All";
				Filteralerts.setOrigin("All");
			}
			
			Filteralerts.setQuerytype("None");
			String sorttype=se.get(5);
			System.out.println("SORT TYPE IS AS FOLLOWS "+se.get(5));
			if(String.valueOf(sorttype).equalsIgnoreCase("None")||String.valueOf(sorttype).equalsIgnoreCase("null")||String.valueOf(sorttype).equalsIgnoreCase("")||sorttype.isEmpty()||String.valueOf(sorttype).equalsIgnoreCase(null)){
				System.out.println("sort empty");
				sorttype="All";
				Filteralerts.setSortalerts("None");
			}
			System.out.println("THE QUERY FROM BEFORE IS "+Filteralerts.getQuery());

			System.out.println(start+end+severity+topic+origin);
			Statement stmt = conn.createStatement();
			String query = null;
			if(start.equalsIgnoreCase("All") && end.equalsIgnoreCase("All") && severity.equalsIgnoreCase("All") && topic.equalsIgnoreCase("All") && origin.equalsIgnoreCase("All") && (sorttype.equalsIgnoreCase("All")||sorttype.equalsIgnoreCase("None"))){
				System.out.print("general");
				query = "SELECT * FROM ALERTS;";
				//String query = "SELECT * FROM ALERTS where TIMESTAMP>='"+start+"' and TIMESTAMP<='"+end+"';";
			}
			else if(sorttype.equalsIgnoreCase("dtasc")){
				query = "select * from ALERTS order by TIMESTAMP;";
				System.out.println(query);
				Filteralerts.setQuerytype("sort");
				Filteralerts.setQuery(query);
			}
			else if(sorttype.equalsIgnoreCase("dtdesc")){
				query = "select * from ALERTS order by TIMESTAMP desc;";
				System.out.println(query);
				Filteralerts.setQuerytype("sort");
				Filteralerts.setQuery(query);
			}
			else if(sorttype.equalsIgnoreCase("sevtol")){
				query = "select * from ALERTS order by FIELD(SEVERITY, 'CRITICAL', 'INFO');";
				System.out.println(query);
				Filteralerts.setQuerytype("sort");
				Filteralerts.setQuery(query);
			}
			else if(sorttype.equalsIgnoreCase("sevlot")){
				query = "select * from ALERTS order by FIELD(SEVERITY, 'INFO', 'CRITICAL');";
				System.out.println(query);
				Filteralerts.setQuerytype("sort");
				Filteralerts.setQuery(query);
			}
			else if(!(start=="All")&&!(end=="All")){
				System.out.println("loop1");
				String startdt=Datetime1.convert(start);
				System.out.println("INTESTBASE STARTTIME"+startdt);
				String enddt=Datetime1.convert(end);
				System.out.println("INTESTBASE ENDTIME"+enddt);
						if(!(severity.equalsIgnoreCase("All"))){
							if(!(topic.equalsIgnoreCase("All"))){
								if(!(origin.equalsIgnoreCase("All"))){
									query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and SEVERITY='"+severity+"' and TOPIC='"+topic+"' and ORIGIN='"+origin+"';";
									System.out.print("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+end+"' and SEVERITY='"+severity+"' and TOPIC='"+topic+"' and ORIGIN='"+origin+"';");
								}
								else{
									query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and SEVERITY='"+severity+"' and TOPIC='"+topic+"';";
								System.out.println("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+end+"' and SEVERITY='"+severity+"' and TOPIC='"+topic+"';");
								}
							}
							else{
							query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and SEVERITY='"+severity+"';";
							System.out.println("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and SEVERITY='"+severity+"';");
							}
						}
						else if(!(topic.equalsIgnoreCase("All"))){
							if(!(origin.equalsIgnoreCase("All"))){
								query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and TOPIC='"+topic+"' and ORIGIN='"+origin+"';";
								System.out.print("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+end+"' and TOPIC='"+topic+"' and ORIGIN='"+origin+"';");
							}
							else{
								query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and TOPIC='"+topic+"';";
							System.out.println("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+end+"' and TOPIC='"+topic+"';");
							}
						}
						else if(!(origin.equalsIgnoreCase("All"))){
							query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"' and ORIGIN='"+origin+"';";
							System.out.print("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+end+"' and ORIGIN='"+origin+"';");
						}
						else{
						query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"';";
						System.out.println("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"';");
						}
						Filteralerts.setQuerytype("filter");
						Filteralerts.setQuery(query);
			}
			else if(!(severity.equalsIgnoreCase("All"))){
				Filteralerts.setQuerytype("filter");
				System.out.println(severity);
				System.out.println("loop2");
				if(!(topic.equalsIgnoreCase("All"))){
					if(!(origin.equalsIgnoreCase("All"))){
						query = "SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"' and TOPIC='"+topic+"' and ORIGIN='"+origin+"';";
						System.out.println("SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"' and TOPIC='"+topic+"' and ORIGIN='"+origin+"';");
					}
					else{
					query = "SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"' and TOPIC='"+topic+"';";
					System.out.println("SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"' and TOPIC='"+topic+"';");
					}
				}
				else if(!(origin.equalsIgnoreCase("All"))){
					query = "SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"' and ORIGIN='"+origin+"';";
					System.out.println("SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"' and ORIGIN='"+origin+"';");
				}
				else{
				query = "SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"';";
				System.out.println("SELECT * FROM ALERTS WHERE SEVERITY='"+severity+"';");
				}
				Filteralerts.setQuerytype("filter");
				Filteralerts.setQuery(query);
			}
			else if(!(topic.equalsIgnoreCase("All"))){
				Filteralerts.setQuerytype("filter");
				System.out.println("loop3");
				if(!(origin.equalsIgnoreCase("All"))){
					query = "SELECT * FROM ALERTS WHERE TOPIC='"+topic+"' and ORIGIN='"+origin+"';";
					System.out.println("SELECT * FROM ALERTS WHERE TOPIC='"+topic+"' and ORIGIN='"+origin+"';");
				}
				else{
				query = "SELECT * FROM ALERTS WHERE TOPIC='"+topic+"';";
				System.out.println("SELECT * FROM ALERTS WHERE TOPIC='"+topic+"';");
				}
				Filteralerts.setQuery(query);
			}
			else if(!(origin.equalsIgnoreCase("All"))){
				System.out.println("loop4");
				query = "SELECT * FROM ALERTS WHERE ORIGIN='"+origin+"';";
				System.out.println("SELECT * FROM ALERTS WHERE ORIGIN='"+origin+"';");
				Filteralerts.setQuerytype("filter");
				Filteralerts.setQuery(query);
			}
			System.out.println(query);
			String checkquery;
			String getquery=Filteralerts.getQuery();
			if(String.valueOf(query).equalsIgnoreCase("null")||String.valueOf(query).equalsIgnoreCase("")||query.isEmpty()||String.valueOf(query).equalsIgnoreCase(null)||String.valueOf(query).equalsIgnoreCase("None")||Filteralerts.getQuerytype().equalsIgnoreCase("sort")||Filteralerts.getQuerytype().equalsIgnoreCase("None")){
							checkquery=Filteralerts.getQuery();
			}
			else{
				checkquery=query;
			}
			if(String.valueOf(checkquery).equalsIgnoreCase("null")||String.valueOf(checkquery).equalsIgnoreCase("")||checkquery.isEmpty()||String.valueOf(checkquery).equalsIgnoreCase(null)||String.valueOf(checkquery).equalsIgnoreCase("None")){
				checkquery="SELECT * FROM ALERTS;";
			}
			System.out.println("query entering for check is "+checkquery);
			ResultSet check = stmt.executeQuery(checkquery);
			if(!check.next()){
				System.out.print("NO ROWS PRESbENT");
				Alertbean tb=new Alertbean();
				tb.setId("Null");
				tb.setDatestamp("No Rows Present for filter");
				tb.setTimestamp("No Time Present");
				tb.setSeverity("No Severity Present");
				tb.setTopic("No Topic Present");
				tb.setOrg("No Origin Present");
				tb.setDescr("No Description Present");
				tbn.add(tb);
			}
			String beforequery=Filteralerts.getQuery();
			System.out.println("query from before "+beforequery);
			if(String.valueOf(beforequery).equalsIgnoreCase("None")||String.valueOf(beforequery).equalsIgnoreCase("null")||String.valueOf(beforequery).equalsIgnoreCase("")||beforequery.isEmpty()||String.valueOf(beforequery).equalsIgnoreCase(null)){
				System.out.println("query entering from if loop is going in final is "+query);
				ralerts = stmt.executeQuery(query);
				Filteralerts.setQuery(query);
			}
			else{
				System.out.println("query entering from else loop going in final is "+beforequery);
				ralerts = stmt.executeQuery(beforequery);
			}
			//Filteralerts.setQuery(query);
			while (ralerts.next()) {
				Alertbean tb=new Alertbean();
				String ID = ralerts.getObject(1).toString();
				System.out.println(ID);
				tb.setId(ID);
				String tv = ralerts.getObject(2).toString();
				Integer position=tv.indexOf(" ");
				Integer endindex=tv.length();
				String jt=tv.substring(position+1, endindex);
				tv=tv.substring(0, position);
				System.out.println(tv);
				tb.setDatestamp(tv);
				System.out.println(jt);
				tb.setTimestamp(jt);
				String sev=ralerts.getObject(3).toString();
				System.out.println(sev);
				tb.setSeverity(sev);
				String top=ralerts.getObject(4).toString();
				System.out.println(top);
				tb.setTopic(top);
				String orig=ralerts.getObject(5).toString();
				System.out.println(orig);
				tb.setOrg(orig);
				String descr=ralerts.getObject(6).toString();
				System.out.println(descr);
				tb.setDescr(descr);
				tbn.add(tb);
			}
			Filteralerts.setSortalerts(null);
			conn.close();
			}
			else if(sortoutput.equalsIgnoreCase("dtasc")||sortoutput.equalsIgnoreCase("dtdesc")||sortoutput.equalsIgnoreCase("sevtol")||sortoutput.equalsIgnoreCase("sevlot")){
				System.out.println("it worked???????");
				Statement stmt = conn.createStatement();
				
				String query=Filteralerts.getQuery();
				String sorttype=Filteralerts.getSortoutput();
				System.out.println(query);
				System.out.println(query);
				System.out.println(query);
				System.out.println(query);
				System.out.println(query);
				System.out.println(query);
				System.out.println(sorttype);
				System.out.println(sorttype);
				if(String.valueOf(sorttype).equalsIgnoreCase("null")||String.valueOf(sorttype).equalsIgnoreCase("")||sorttype.isEmpty()||String.valueOf(sorttype).equalsIgnoreCase(null)){
					query="Select * from ALERTS;";
					Integer z=query.length();
					query=query.substring(0, z-1);
					System.out.println("GOING IN NULL VALUE");
				}
				if(!(String.valueOf(query).equalsIgnoreCase("null")||String.valueOf(query).equalsIgnoreCase("")||query.isEmpty()||String.valueOf(query).equalsIgnoreCase(null))){
					if(query.contains("order")){
						System.out.println("contains pre defined sorting");
						Integer y=query.length();
						Integer prev=query.indexOf("order");
						query=query.substring(0, prev-1);
					}
					else{
						System.out.println("does not contains pre defined sorting");
					Integer x=query.length();
					query=query.substring(0, x-1);
					System.out.println(Filteralerts.getQuerytype());
					System.out.println(Filteralerts.getQuerytype());
					System.out.println(Filteralerts.getQuerytype());
					System.out.println(Filteralerts.getQuerytype());
					System.out.println(Filteralerts.getQuerytype());
					}
				}
				
				if(String.valueOf(query).equalsIgnoreCase("null")||String.valueOf(query).equalsIgnoreCase("")||query.isEmpty()||String.valueOf(query).equalsIgnoreCase(null)||String.valueOf(query).equalsIgnoreCase("None")){
					query="Select * from ALERTS;";
					Integer y=query.length();
					query=query.substring(0, y-1);
					System.out.println("GOING IN SORT QUERY VALUE");
				}
				
				if(sorttype.equalsIgnoreCase("dtasc")){
					query =  query+ " order by TIMESTAMP;";
					System.out.println(query);
				}
				else if(sorttype.equalsIgnoreCase("dtdesc")){
					query = query+ " order by TIMESTAMP DESC;";
					System.out.println(query);
				}
				else if(sorttype.equalsIgnoreCase("sevtol")){
					query = query+ " order by FIELD(SEVERITY, 'CRITICAL', 'INFO');";
					System.out.println(query);
				}
				else if(sorttype.equalsIgnoreCase("sevlot")){
					query = query+ " order by FIELD(SEVERITY, 'INFO', 'CRITICAL');";
					System.out.println(query);
				}
				Filteralerts.setQuery(query);
				ralerts = stmt.executeQuery(query);
				while (ralerts.next()) {
					Alertbean tb=new Alertbean();
					String ID = ralerts.getObject(1).toString();
					System.out.println(ID);
					tb.setId(ID);
					String tv = ralerts.getObject(2).toString();
					Integer position=tv.indexOf(" ");
					Integer endindex=tv.length();
					String jt=tv.substring(position+1, endindex);
					tv=tv.substring(0, position);
					System.out.println(tv);
					tb.setDatestamp(tv);
					System.out.println(jt);
					tb.setTimestamp(jt);
					String sev=ralerts.getObject(3).toString();
					System.out.println(sev);
					tb.setSeverity(sev);
					String top=ralerts.getObject(4).toString();
					System.out.println(top);
					tb.setTopic(top);
					String orig=ralerts.getObject(5).toString();
					System.out.println(orig);
					tb.setOrg(orig);
					String descr=ralerts.getObject(6).toString();
					System.out.println(descr);
					tb.setDescr(descr);
					tbn.add(tb);
				}
				Filteralerts.setSortoutput(null);
			}
			/*if(startlength==0){
				System.out.println("start empty");SORT TYPE IS AS FOLLOWS dtasc
				start="all";
			}
			Integer endlength=end.length();
			if(endlength==0){
				System.out.println("end empty");
				end="all";
   }
   Integer sevlength=severity.length();
   if(sevlength==0){
    System.out.println("severity empty");
    severity="all";
   }
   Integer tplength=topic.length();
   if(tplength==0){
    System.out.println("topic empty");
    topic="all";
   }
   Integer orglength=origin.length();
   if(orglength==0){
    System.out.println("end empty");
				origin="all";
			}*/
			//System.out.println(start+end+severity+topic+origin);

			//String startdate=request.getParameter("demo2");
			//Integer x=startdate.length();
			//if(x==0){
				//System.out.print("startdate is empty");
			//	startdate="Jan 01";
			//}
			//Startendalerts.setStartime(startdate);
			//System.out.println(Startendalerts.getStartime());
			//Startendalerts.setEndime(request.getParameter("demo2"));
			//System.out.print("hey severity is "+request.getParameter("demo2"));
			//System.out.print("12334");
			//tb.setSev(request.getParameter("severity"));
			//String severity=tb.getSev();
			/*Integer s=severity.length();
			System.out.print("\n"+s+"\n");
			System.out.println("Value is " + severity + "check");
			if(severity.equalsIgnoreCase("null")){
				System.out.print("12334 hey severity is empty");
				severity="All";
				System.out.print("1 severity is "+severity);
			
			String startdt=Datetime1.convert(start);
			System.out.println("INTESTBASE STARTTIME"+startdt);
			String enddt=Datetime1.convert(end);
			System.out.println("INTESTBASE ENDTIME"+enddt);
			
			System.out.println("Process on2");
			
			System.out.println(st
					.executeQuery("SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"'"));

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ALERTS where TIMESTAMP>='"+startdt+"' and TIMESTAMP<='"+enddt+"';";
			ralerts = stmt.executeQuery(query);

			while (ralerts.next()) {
				Alertbean addtb=new Alertbean();
				String ID = rs.getObject(1).toString();
				addtb.setId(ID);
				System.out.println(ID);
				String tv = rs.getObject(2).toString();
				addtb.setTwo_val(tv);
				System.out.println(tv);
				String jt= rs.getObject(3).toString();
				addtb.setJt(jt);
				System.out.println(jt);
				tbn.add(addtb);
			}
			conn.close();*/
			conn.close();
		} catch (Exception e) {
			Loggerdb.loggingdb("INFO", "Database", "Fetch Database Error", "Unable to filter/sort alerts.");
			logger.log(Level.INFO, "Unable to filter/sort alerts.");
			//System.err.println("Got an exception! ");
		}
		return tbn;
	}
	
	/*public List<Alertbean> returnalerts(){
		int i=0;
		List<Alertbean> tb=new ArrayList<Alertbean>();
		tb=Testbase.callalerts(se)
		
	}*/
	
	public static List<String> callsev() {
		List<String> sev = new ArrayList<String>();
		ResultSet ra=null;
		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			 try {  

		        // This block configure the logger with handler and formatter  
		        fh = new FileHandler("/home/logs/evolve_log");  
		        logger.addHandler(fh);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  }
		 catch(Exception e)
		 {
			 
		 }
			System.out.println("Process on2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ALERTS;";
			ra = stmt.executeQuery(query);
			while (ra.next()) {
				String SV = ra.getObject(3).toString();
				if (sev.size() == 0) {
					//System.out.println("null");
					sev.add(SV);
					//System.out.println(SV);
				} else {
					//System.out.println("full");
					Integer j=0;
					for (int i = 0; i < sev.size(); i++) {
						if(SV.equalsIgnoreCase(sev.get(i).toString())) {
							//System.out.println("match");
							j=j+1;
						} else {
							//System.out.println("nomatch");
							//System.out.println(SV);
						}
					}
					if(j==0){
						sev.add(SV);
					}
				}				
			}
			System.out.println("SIZE OF LIST IS "+sev.size());
			for (int i = 0; i < sev.size(); i++) {
				System.out.println(sev.get(i).toString());
			}
			conn.close();
		}
			catch (Exception e) {
				Loggerdb.loggingdb("INFO", "Database", "Fetch Database Error", "Unable to fetch list-severity for alerts.");
				logger.log(Level.INFO, "Unable to fetch list-severity for alerts.");
				//System.err.println("Got an exception! ");
				//e.printStackTrace();
			}
			return sev;
	}
	
	
	public static List<String> calltopic() {
		List<String> tp = new ArrayList<String>();
		ResultSet ra=null;
		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			 try {  

		        // This block configure the logger with handler and formatter  
		        fh = new FileHandler("/home/logs/evolve_log");  
		        logger.addHandler(fh);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  }
		 catch(Exception e)
		 {
			 
		 }
			System.out.println("Process on2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ALERTS;";
			ra = stmt.executeQuery(query);
			while (ra.next()) {
				String TOPIC = ra.getObject(4).toString();
				if (tp.size() == 0) {
					//System.out.println("null");
					tp.add(TOPIC);
					//System.out.println(TOPIC);
				} else {
					//System.out.println("full");
					Integer j=0;
					for (int i = 0; i < tp.size(); i++) {
						if (TOPIC.equalsIgnoreCase(tp.get(i).toString())) {
							//System.out.println("match");
							j=j+1;
						} else {
							//System.out.println("nomatch");
							//System.out.println(TOPIC);
						}
					}
					if(j==0){
						tp.add(TOPIC);
					}
				}				
			}
			System.out.println("SIZE OF LIST IS "+tp.size());
			for (int i = 0; i < tp.size(); i++) {
				System.out.println(tp.get(i).toString());
			}
			conn.close();
		}
			catch (Exception e) {
				Loggerdb.loggingdb("INFO", "Database", "Fetch Database Error", "Unable to fetch list-topicfor alerts.");
				logger.log(Level.INFO, "Unable to fetch topic-list for alerts.");
				//System.err.println("Got an exception! ");
				//e.printStackTrace();
			}
			return tp;
	}
	

	public static List<String> callorg() {
		List<String> org = new ArrayList<String>();
		ResultSet ra=null;
		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			 try {  

		        // This block configure the logger with handler and formatter  
		        fh = new FileHandler("/home/logs/evolve_log");  
		        logger.addHandler(fh);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  }
		 catch(Exception e)
		 {
			 
		 }
			System.out.println("Process on2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ALERTS;";
			ra = stmt.executeQuery(query);
			while (ra.next()) {
				String ORG = ra.getObject(5).toString();
				if (org.size() == 0) {
					//System.out.println("null");
					org.add(ORG);
					//System.out.println(ORG);
				} else {
					//System.out.println("full");
					Integer j=0;
					for (int i = 0; i < org.size(); i++) {
						if (ORG.equalsIgnoreCase(org.get(i).toString())) {
							//System.out.println("match");
							j=j+1;
						} else {
							//System.out.println("nomatch");
							//System.out.println(ORG);
						}
					}
					if(j==0){
						org.add(ORG);
					}
				}				
			}
			System.out.println("SIZE OF LIST IS "+org.size());
			for (int i = 0; i < org.size(); i++) {
				System.out.println(org.get(i));
			}
			conn.close();
			
		}
			catch (Exception e) {
				Loggerdb.loggingdb("INFO", "Database", "Fetch Database Error", "Unable to fetch list-origin for alerts.");
				logger.log(Level.INFO, "Unable to fetch origin-list for alerts.");
				//System.err.println("Got an exception! ");
				//e.printStackTrace();
			}
			return org;
	}
	
	public static List<Alertbean> sortoutput(String sortvalue){
		List<Alertbean> tbn = new ArrayList<Alertbean>();
		ResultSet ralerts=null;
		try {
			System.out.println("Process on1");
			
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			
			 try {  

		        // This block configure the logger with handler and formatter  
		        fh = new FileHandler("/home/logs/evolve_log");  
		        logger.addHandler(fh);
		        SimpleFormatter formatter = new SimpleFormatter();  
		        fh.setFormatter(formatter);  }
		 catch(Exception e)
		 {
			 
		 }
			Statement stmt = conn.createStatement();
			
			String query=Filteralerts.getQuery();
			String sorttype=sortvalue;
			
			if(sorttype.equalsIgnoreCase("dtasc")){
				query =  query+ " order by TIMESTAMP;";
				System.out.println(query);
			}
			else if(sorttype.equalsIgnoreCase("dtdesc")){
				query = query+ " order by TIMESTAMP desc;";
				System.out.println(query);
			}
			else if(sorttype.equalsIgnoreCase("sevtol")){
				query = query+ " order by FIELD(SEVERITY, 'CRITICAL', 'INO');";
				System.out.println(query);
			}
			else if(sorttype.equalsIgnoreCase("sevlot")){
				query = query+ " order by FIELD(SEVERITY, 'INFO', 'CRITICIAL');";
				System.out.println(query);
			}
			
			ralerts = stmt.executeQuery(query);
			while (ralerts.next()) {
				Alertbean tb=new Alertbean();
				String ID = ralerts.getObject(1).toString();
				System.out.println(ID);
				tb.setId(ID);
				String tv = ralerts.getObject(2).toString();
				Integer position=tv.indexOf(" ");
				Integer endindex=tv.length();
				String jt=tv.substring(position+1, endindex);
				tv=tv.substring(0, position);
				System.out.println(tv);
				tb.setDatestamp(tv);
				System.out.println(jt);
				tb.setTimestamp(jt);
				String sev=ralerts.getObject(4).toString();
				System.out.println(sev);
				tb.setSeverity(sev);
				String top=ralerts.getObject(5).toString();
				System.out.println(top);
				tb.setTopic(top);
				String orig=ralerts.getObject(6).toString();
				System.out.println(orig);
				tb.setOrg(orig);
				String descr=ralerts.getObject(7).toString();
				System.out.println(descr);
				tb.setDescr(descr);
				tbn.add(tb);
			}
			
			conn.close();
			
		}
		catch (Exception e) {
			Loggerdb.loggingdb("INFO", "Database", "Fetch Database Error", "Unable to sort alerts.");
			logger.log(Level.INFO, "Unable to sort alerts.");
			//System.err.println("Got an exception! ");
			//e.printStackTrace();
		}
		return tbn;
}
	}
		
	
	

