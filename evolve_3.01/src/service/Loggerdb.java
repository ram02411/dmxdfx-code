package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import sources.excelWriter;

public class Loggerdb {
	static ResultSet rs=null;
	/**
	 * @param args
	 */
	public static void loggingdb(String sev, String topc, String org, String descr) {
		try {

			System.out.println("Process enter db");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			Statement st = conn.createStatement();

			System.out.println("Process enter db2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM EVOLVELOGS;";
			rs = stmt.executeQuery(query);

			String LOGID = null;
			
			if(!(rs.next())){
				LOGID="101";
			}
			else{
				LOGID = "default";
			}		
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int month=Calendar.getInstance().get(Calendar.MONTH);
			int date = Calendar.getInstance().get(Calendar.DATE);
			int hour = Calendar.getInstance().get(Calendar.HOUR);
			int minute = Calendar.getInstance().get(Calendar.MINUTE);
			int second = Calendar.getInstance().get(Calendar.SECOND);
			String datetime=year+"-"+month+"-"+date+" "+hour+"-"+minute+"-"+second;
			
			String severity=sev;
			String topic=topc;
			String origin=org;
			String description=descr;

			String SQL1 = String
					.format("INSERT INTO EVOLVELOGS VALUES("
							+ LOGID + ", '" + datetime + "', '" + severity
							+ "', '" + topic + "', '" + origin + "', '"
							+ description + "')");
			System.out.println("entered in db");
			System.out.println(SQL1);
			st.executeUpdate(SQL1);
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}

}
