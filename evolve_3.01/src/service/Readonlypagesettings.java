package service;

public class Readonlypagesettings {
	public static void settingpagestatus(String username){
		if(username.equalsIgnoreCase("admin")){
			Statusbean.setAdministrator(1);
			Statusbean.setAdminrole(1);
			Statusbean.setDashboard(1);
			Statusbean.setControlleralerts(1);
			Statusbean.setCanvas(1);
			Statusbean.setViewer(1);
			Statusbean.setViewersnt(1);
			Statusbean.setTags(1);
			Statusbean.setFetchcanvas(1);
			Statusbean.setPolicybase(1);
			Statusbean.setPolicybasesnt(1);
			Statusbean.setRulebase(1);
			Statusbean.setRulebasesnt(1);
			Statusbean.setSimulate(1);
			Statusbean.setSecurityrulebase(1);
			Statusbean.setFlowrector(1);
		}
		else{
			String pread=UserRole.getreadpage(username);
			if(pread==null){
				Statusbean.setAdministrator(1);
				Statusbean.setAdminrole(1);
				Statusbean.setDashboard(1);
				Statusbean.setControlleralerts(1);
				Statusbean.setCanvas(1);
				Statusbean.setViewer(1);
				Statusbean.setViewersnt(1);
				Statusbean.setTags(1);
				Statusbean.setFetchcanvas(1);
				Statusbean.setPolicybase(1);
				Statusbean.setPolicybasesnt(1);
				Statusbean.setRulebase(1);
				Statusbean.setRulebasesnt(1);
				Statusbean.setSimulate(1);
				Statusbean.setSecurityrulebase(1);
				Statusbean.setFlowrector(1);
			}
			if(pread.equalsIgnoreCase("all")){
				Statusbean.setAdministrator(2);
				Statusbean.setAdminrole(2);
				Statusbean.setDashboard(2);
				Statusbean.setControlleralerts(2);
				Statusbean.setCanvas(2);
				Statusbean.setViewer(2);
				Statusbean.setViewersnt(2);
				Statusbean.setTags(2);
				Statusbean.setFetchcanvas(2);
				Statusbean.setPolicybase(2);
				Statusbean.setPolicybasesnt(2);
				Statusbean.setRulebase(2);
				Statusbean.setRulebasesnt(2);
				Statusbean.setSimulate(2);
				Statusbean.setSecurityrulebase(2);
				Statusbean.setFlowrector(2);
			}
			if(!pread.contains(",")){
				if(pread.equalsIgnoreCase("administrator"))
				Statusbean.setAdministrator(2);
				if(pread.equalsIgnoreCase("adminrole"))
				Statusbean.setAdminrole(2);
				if(pread.equalsIgnoreCase("dashboard"))
					Statusbean.setDashboard(2);	
				if(pread.equalsIgnoreCase("controlleralerts"))
					Statusbean.setControlleralerts(2);
				if(pread.equalsIgnoreCase("canvas"))
					Statusbean.setCanvas(2);
				Statusbean.setFetchcanvas(2);
				if(pread.equalsIgnoreCase("viewer"))
					Statusbean.setViewer(2);
				if(pread.equalsIgnoreCase("viewersnt"))
					Statusbean.setViewersnt(2);
				if(pread.equalsIgnoreCase("tags"))
					Statusbean.setTags(2);
				if(pread.equalsIgnoreCase("policybase"))	
					Statusbean.setPolicybase(2);
				if(pread.equalsIgnoreCase("policybasesnt"))
					Statusbean.setPolicybasesnt(2);
				if(pread.equalsIgnoreCase("rulebase"))
					Statusbean.setRulebase(2);
				if(pread.equalsIgnoreCase("rulebasesnt"))
					Statusbean.setRulebasesnt(2);
				if(pread.equalsIgnoreCase("simulate"))
					Statusbean.setSimulate(2);
				if(pread.equalsIgnoreCase("securityrulebase"))
				Statusbean.setSecurityrulebase(2);
				if(pread.equalsIgnoreCase("flowrector"))
					Statusbean.setFlowrector(2);
					
			}
			else{
			String[] pages=pread.split(",");
			for(int i=0;i<pages.length;i++){
				if(pages[i]!=null){
				if(pread.equalsIgnoreCase("administrator"))
					Statusbean.setAdministrator(2);
				if(pread.equalsIgnoreCase("adminrole"))
					Statusbean.setAdminrole(2);	
				if(pages[i].equalsIgnoreCase("dashboard"))
					Statusbean.setDashboard(2);	
				if(pages[i].equalsIgnoreCase("controlleralerts"))
					Statusbean.setControlleralerts(2);
				if(pages[i].equalsIgnoreCase("canvas"))
					Statusbean.setCanvas(2);
				Statusbean.setFetchcanvas(2);
				if(pages[i].equalsIgnoreCase("viewer"))
					Statusbean.setViewer(2);
				if(pages[i].equalsIgnoreCase("viewersnt"))
					Statusbean.setViewersnt(2);
				if(pages[i].equalsIgnoreCase("tags"))
					Statusbean.setTags(2);
				if(pages[i].equalsIgnoreCase("policybase"))	
					Statusbean.setPolicybase(2);
				if(pages[i].equalsIgnoreCase("policybasesnt"))
					Statusbean.setPolicybasesnt(2);
				if(pages[i].equalsIgnoreCase("rulebase"))
					Statusbean.setRulebase(2);
				if(pages[i].equalsIgnoreCase("rulebasesnt"))
					Statusbean.setRulebasesnt(2);
				if(pages[i].equalsIgnoreCase("simulate"))
					Statusbean.setSimulate(2);
				if(pages[i].equalsIgnoreCase("securityrulebase"))
				Statusbean.setSecurityrulebase(2);
				if(pages[i].equalsIgnoreCase("flowrector"))
					Statusbean.setFlowrector(2);
					
				
				}
				
			}
			
			}
			
			
		}
	}


}
