package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import conversions.Datetime1;


import bean.Alertbean;

public class Alertbase {
	static ResultSet rs=null;
	/*public static List<Testbean> print() {
		List<Testbean> tbn = new ArrayList<Testbean>();

		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "root123");
		

			Statement st = conn.createStatement();

			System.out.println("Process on2");
			
			System.out.println(st
					.executeQuery("SELECT * FROM test"));

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM test;";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				Testbean tb=new Testbean();
				String ID = rs.getObject(1).toString();
				tb.setId(ID);
				String tv = rs.getObject(2).toString();
				tb.setTwo_val(tv);
				String jt= rs.getObject(3).toString();
				tb.setJt(jt);				
				tbn.add(tb);
			}
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return tbn;
	}*/
	
	public static List<Alertbean> callalerts(List<String> se) {
		List<Alertbean> ab = new ArrayList<Alertbean>();

		try {
			System.out.println("Process on1");
			System.out.println(se.get(0));
			System.out.println(se.get(1));
			System.out.println("Process on12");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "root123");
		
			System.out.println(se.get(0));
			System.out.println(se.get(1));

			String start=se.get(0);
			String end=se.get(1);
			
			String startdt=Datetime1.convert(start);
			System.out.println("INTESTBASE STARTTIME"+startdt);
			String enddt=Datetime1.convert(end);
			System.out.println("INTESTBASE ENDTIME"+enddt);
			
			Statement st = conn.createStatement();
			System.out.println("Process on2");
			
			System.out.println(st
					.executeQuery("SELECT * FROM ALERTS where TIME_STAMP>='"+startdt+"' and TIME_STAMP<='"+enddt+"'"));

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM ALERTS where TIME_STAMP>='"+startdt+"' and TIME_STAMP<='"+enddt+"';";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				Alertbean abn=new Alertbean();
				String TS = rs.getObject(1).toString();
				abn.setTimestamp(TS);
				String topic = rs.getObject(2).toString();
				abn.setTopic(topic);
				String sev= rs.getObject(3).toString();
				abn.setSeverity(sev);
				String org= rs.getObject(4).toString();
				abn.setOrg(org);
				String description= rs.getObject(5).toString();
				abn.setDescr(description);
				
				ab.add(abn);
			}
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return ab;
	}
	
	/*public List<Testbean> returnalerts(){
		int i=0;
		List<Testbean> tb=new ArrayList<Testbean>();
		tb=Testbase.callalerts(se)
		
	}*/
}
