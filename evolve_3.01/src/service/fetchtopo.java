package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bean.fetchtopobean;
import bean.fetchtopobeanlink;

public class fetchtopo {
	static ResultSet rs = null;

	
	public static List<String> fetchcanvasname() {
		List<String> cname = new ArrayList<String>();
		ResultSet ra=null;
		try {
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			System.out.println("Process on2");

			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM canvasnamelist;";
			ra = stmt.executeQuery(query);
			while (ra.next()) {
				String canvasname = ra.getObject(2).toString();
				cname.add(canvasname);
			}			
			conn.close();
		}
			catch (Exception e) {
				System.err.println("Got an exception! ");
				e.printStackTrace();
			}
			return cname;
	}
	
	
	
	public static List<fetchtopobean> switchhost(String canvasname) {
		System.out.println("going in try block1");
		List<fetchtopobean> returnft = new ArrayList<fetchtopobean>();
		System.out.println("going in try block2");
		try {
			System.out.println("Process on nodes");
			System.out.println("going in the method");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		
			Statement stmt1 = conn.createStatement();
			String query = "SELECT * FROM "+canvasname+";";
			rs = stmt1.executeQuery(query);
			while(rs.next()){
				fetchtopobean ft=new fetchtopobean();
				String sorh=rs.getObject(1).toString();
				ft.setSwitchorhost(sorh);
				//System.out.println(sorh);
				String name=rs.getObject(2).toString();
				ft.setName(name);
				//System.out.println(name);
				String swtype=rs.getObject(3).toString();
				ft.setSwitchtype(swtype);
				//System.out.println(swtype);
				String xc=rs.getObject(4).toString();
				ft.setXcordinate(xc);
				//System.out.println(xc);
				String yc=rs.getObject(5).toString();
				ft.setYcordinate(yc);
				//System.out.println(yc);
				returnft.add(ft);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return returnft;

	}
	
	
	public static List<fetchtopobeanlink> links(String canvasname) {
		List<fetchtopobeanlink> returnftlinks = new ArrayList<fetchtopobeanlink>();
		try {
			System.out.println("Process on links");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			
			String linktable=canvasname+"link";
			
			Statement stmtlink = conn.createStatement();
			String query = "SELECT * FROM "+linktable+";";
			rs = stmtlink.executeQuery(query);
			while(rs.next()){
				fetchtopobeanlink ftlinks=new fetchtopobeanlink();
				String src=rs.getObject(1).toString();
				ftlinks.setLinksource(src);
				String srcx=rs.getObject(2).toString();
				ftlinks.setLinksrcx(srcx);
				String srcy=rs.getObject(3).toString();
				ftlinks.setLinksrcy(srcy);
				String dst=rs.getObject(4).toString();
				ftlinks.setLinkdestination(dst);
				String dtx=rs.getObject(5).toString();
				ftlinks.setLinkdestx(dtx);
				String dsty=rs.getObject(6).toString();
				ftlinks.setLinkdesty(dsty);
				returnftlinks.add(ftlinks);
			}
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return returnftlinks;

	}
	
}
