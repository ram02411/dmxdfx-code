package service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import dto.Lockdown;
import dto.setsessionvar;
import evolve.hpc.alertpickScheduler;
import bean.Rolebean;
import bean.Userbean;
import bean.setsessionbean;

public class Credential {

	
	public static void checklocldown(){
		String value = null;
		ResultSet rs=null;
		ResultSet r1=null;
		try {
			
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			//setting the time-lock
			System.out.println("DIVEIN1");
			Statement st1=conn.createStatement();
			String q1="select * from locktime;";
			r1=st1.executeQuery(q1);
		
				System.out.println("DIVEIN4");
				while(r1.next()){
				int year = Calendar.getInstance().get(Calendar.YEAR);
				int month=Calendar.getInstance().get(Calendar.MONTH);
				month=month+1;
				int date = Calendar.getInstance().get(Calendar.DATE);
				/*int hour = Calendar.getInstance().get(Calendar.HOUR);
				int minute = Calendar.getInstance().get(Calendar.MINUTE);
				int second = Calendar.getInstance().get(Calendar.SECOND);*/
				String sysdt=year+"-"+month+"-"+date;
				String lockdt=r1.getObject(1).toString();
				System.out.println(sysdt);
				
				int cuttopos=lockdt.indexOf(" ");
				String lockdate=lockdt.substring(0, cuttopos);
				System.out.println(lockdate);
				
				SimpleDateFormat tolock=new SimpleDateFormat("yyyy-MM-dd");
				Date d1=tolock.parse(sysdt);
				Date d2=tolock.parse(lockdate);
				
				
				if(d1.compareTo(d2)>0){
					System.out.println("stop evolve");
					Lockdown.setLockdown("lock");
				}
				else{
					System.out.println("run evolve");
					Lockdown.setLockdown("open");
				}
			
		}
			
			
			System.out.println("DIVEIN7");
			//finishing the set-lock
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}
	
	
	public static void deleteuser(String name){
		ResultSet rs=null;
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("Process enter2");
			
			
			System.out.println(st.executeUpdate("DELETE  FROM CREDENTIALS WHERE USERNAME='"+name+"';"));
			String query1="DELETE  FROM CREDENTIALS WHERE USERNAME='"+name+"';";
		st.executeUpdate(query1);
			
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		
	}
	
	
	
	public static void updateuser(String username,String password,String rolename,String editname){
		username="'"+username+"'";
		password="'"+password+"'";
		rolename="'"+rolename+"'";
		editname="'"+editname+"'";
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			//System.out.println("INSERT INTO CREDENTIALS VALUES("+username+","+password+","+rolename+");");

			
			String query = "UPDATE CREDENTIALS SET USERNAME="+username+",PASSWORD="+password+",ROLENAME="+rolename+" WHERE USERNAME="+editname+";";
			st.executeUpdate(query);
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	public static void createuser(String username,String password,String rolename){
		
		username="'"+username+"'";
		password="'"+password+"'";
		rolename="'"+rolename+"'";
		
		
		try{
			System.out.println("Process enter1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
			Statement st = conn.createStatement();

			System.out.println("INSERT INTO CREDENTIALS VALUES("+username+","+password+","+rolename+");");

			
			String query = "INSERT INTO CREDENTIALS VALUES("+username+","+password+","+rolename+");";
			st.executeUpdate(query);
			conn.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static List<Userbean> getallusers(){
		List<Userbean> users=new ArrayList<Userbean>();
		ResultSet rs=null;
		try {
			
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
		

			Statement st = conn.createStatement();

			System.out.println("Process on2");
			 
			System.out.println(st
					.executeQuery("SELECT * FROM CREDENTIALS"));

			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM CREDENTIALS;";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				Userbean ub=new Userbean();
				String username=rs.getObject(1).toString();
				String rolename=rs.getObject(3).toString();
				ub.setUsername(username);
				ub.setRolename(rolename);
				if(!username.equalsIgnoreCase("admin")){
				users.add(ub);
				}
			}
		}catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		
		return users;
		
	}
	
	public static Userbean getusers(String name){
		Userbean user=new Userbean();
		ResultSet rs=null;
		try {
			
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
		

			Statement st = conn.createStatement();

			System.out.println("Process on2");
			 
			System.out.println(st
					.executeQuery("SELECT * FROM CREDENTIALS"));

			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM CREDENTIALS WHERE USERNAME='"+name+"';";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				
				String username=rs.getObject(1).toString();
				String password=rs.getObject(2).toString();
				String rolename=rs.getObject(3).toString();
				user.setUsername(username);
				user.setRolename(rolename);
				user.setPassword(password);
				
			}
		}catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		
		return user;
		
	}
	
	
	public static String print(String name, String pwd) {
		String value = null;
		ResultSet rs=null;
		ResultSet r1=null;
		try {
			lockdownScheduler.checklock();
			alertpickScheduler.callalert();
			System.out.println("Process on1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", Applicationconstant.username, Applicationconstant.password);
		

			Statement st = conn.createStatement();

			System.out.println("Process on2");
			 
			System.out.println(st
					.executeQuery("SELECT * FROM CREDENTIALS"));

			int check=0;
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM CREDENTIALS;";
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				String gotname=rs.getObject(1).toString();
				String gotpwd=rs.getObject(2).toString();
				if(name.equals(gotname)){
					check++;
				}
				if(pwd.equals(gotpwd)){
					check++;
				}
				
				if(check==2){
					value="match";
									
					setsessionvar.setSessioname(gotname);
					CreateLeftmenu.createmenu(gotname);
					Readonlypagesettings.settingpagestatus(gotname);
					break;
				}
				else{
					value="no match";
				}
			}
			
			
			//setting the time-lock
			System.out.println("DIVEIN1");
			Statement st1=conn.createStatement();
			String q1="select * from locktime;";
			r1=st1.executeQuery(q1);
			
			int setnow=0;
			System.out.println("DIVEIN2");
			if(!(r1.next())){
				setnow=1;
				System.out.println("DIVEIN3");
				/*String checkforset=r1.getObject(1).toString();
				if(String.valueOf(checkforset).equalsIgnoreCase("")||checkforset.equalsIgnoreCase("")||checkforset==null){
					System.out.println("DIVEIN4");
					setnow=1;
				}
				else{
					System.out.println("DIVEIN5");
				}*/
				Lockdown.setLockdown("open");
			}
			else{
				System.out.println("DIVEIN4");
				
				int year = Calendar.getInstance().get(Calendar.YEAR);
				int month=Calendar.getInstance().get(Calendar.MONTH);
				month=month+1;
				int date = Calendar.getInstance().get(Calendar.DATE);
				int hour = Calendar.getInstance().get(Calendar.HOUR);
				int minute = Calendar.getInstance().get(Calendar.MINUTE);
				int second = Calendar.getInstance().get(Calendar.SECOND);
				String sysdt=year+"-"+month+"-"+date;
				String lockdt=r1.getObject(1).toString();
				System.out.println(sysdt);
				
				int cuttopos=lockdt.indexOf(" ");
				String lockdate=lockdt.substring(0, cuttopos);
				System.out.println(lockdate);
				
				SimpleDateFormat tolock=new SimpleDateFormat("yyyy-MM-dd");
				Date d1=tolock.parse(sysdt);
				Date d2=tolock.parse(lockdate);
				
				
				if(d1.compareTo(d2)>0){
					System.out.println("stop evolve");
					Lockdown.setLockdown("lock");
				}
				else{
					System.out.println("run evolve");
					Lockdown.setLockdown("open");
				}
			}
			
			
			if(setnow==1){
				System.out.println("DIVEIN6");
				int year = Calendar.getInstance().get(Calendar.YEAR);
				int month=Calendar.getInstance().get(Calendar.MONTH);
				month=month+1;
				int date = Calendar.getInstance().get(Calendar.DATE);
				int hour = Calendar.getInstance().get(Calendar.HOUR);
				int minute = Calendar.getInstance().get(Calendar.MINUTE);
				int second = Calendar.getInstance().get(Calendar.SECOND);
				String checkdatetime=year+"-"+month+"-"+date+" "+hour+"-"+minute+"-"+second;
				Statement st2=conn.createStatement();
				String q2= "INSERT INTO locktime VALUES('"+checkdatetime+"');";
				System.out.println(q2);
				st1.executeUpdate(q2);
				
				Statement st3=conn.createStatement();
				String q3= "UPDATE locktime SET start_time = DATE_ADD(start_time, INTERVAL 60 DAY);";
				st3.executeUpdate(q3);
				
			}
			System.out.println("DIVEIN7");
			//finishing the set-lock
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		return value;
	}
}
