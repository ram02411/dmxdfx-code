package dto;

public class Zonegendto {
	private String zonename;
	private String zonenumber;
	private String switchnamez;
	private String interfacez;
	public String getZonename() {
		return zonename;
	}
	public void setZonename(String zonename) {
		this.zonename = zonename;
	}
	public String getZonenumber() {
		return zonenumber;
	}
	public void setZonenumber(String zonenumber) {
		this.zonenumber = zonenumber;
	}
	public String getSwitchnamez() {
		return switchnamez;
	}
	public void setSwitchnamez(String switchnamez) {
		this.switchnamez = switchnamez;
	}
	public String getInterfacez() {
		return interfacez;
	}
	public void setInterfacez(String interfacez) {
		this.interfacez = interfacez;
	}
	
	

}
