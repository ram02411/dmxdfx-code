package dto;

public class Startendalerts {
	private static String starttime;
	private static String endtime;
	public static String getStarttime() {
		return starttime;
	}
	public static void setStarttime(String stime) {
		starttime = stime;
	}
	public static String getEndtime() {
		return endtime;
	}
	public static void setEndtime(String etime) {
		endtime = etime;
	}
	
}
