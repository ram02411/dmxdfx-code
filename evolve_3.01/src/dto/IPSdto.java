package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * @author java
 *  This class is used for setting and Getting the values of  
 *   Controller ip,Controller type And evolve ip using sql database 
 *
 */
public class IPSdto {

public static String controllerIP= "192.168.0.10";
public static String evolveIP="192.168.0.100";
/*public static String getEvolveIP(){

	return evolveIP;
}*/

/*public static String getControllerIP(){

	return controllerIP;
}*/


		public static void setControllerIP(String ip){
			
				System.out.println("Process on1");
				try {
					Class.forName("com.mysql.jdbc.Driver");
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Connection conn = null;
				Statement stmt = null;
				try {
					conn = DriverManager.getConnection(
							"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
				 stmt = conn.createStatement();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {

				Statement st = conn.createStatement();

				System.out.println("Process on2");
				 

				int check=0;
				
				String query = "UPDATE IPADDR SET IPADDR='"+ ip +"' WHERE DESCR='CONTROLLERIP';";
				System.out.println(query);
				stmt.executeUpdate(query);
				
				System.out.println("controller ip has been updated");
				
				

			} catch (Exception e) {
				System.err.println("Got an exception! ");
				e.printStackTrace();
			}
			finally{
				try {
					conn.close();
					stmt.close();
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
	public static void setEvolveIP(String ip){
		
			System.out.println("Process on1");
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			try {
			Statement st = conn.createStatement();

			System.out.println("Process on2");
			 

			int check=0;
			Statement stmt = conn.createStatement();
			String query = "UPDATE IPADDR SET IPADDR='"+ ip +"' WHERE DESCR='EVOLVEIP';";
			stmt.executeUpdate(query);
			System.out.println("EVOLVE ip has been updated");
			
			

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
			
		}
	
	/**
	 * 
	 * 
	 * @param type 
	 * 
	 * 	 */
	public static void setControllerType(String type){
		
		System.out.println("Process on1");
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Connection conn = null;
		Statement stmt = null;
		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		 stmt = conn.createStatement();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {

		Statement st = conn.createStatement();

		System.out.println("Process on2");
		 

		int check=0;
		
		String query = "UPDATE IPADDR SET TYPE='"+ type +"' WHERE DESCR='CONTROLLERIP';";
		System.out.println(query);
		stmt.executeUpdate(query);
		
		System.out.println("controller type has been updated");
		
		

	} catch (Exception e) {
		System.err.println("Got an exception! ");
		e.printStackTrace();
	}
	finally{
		try {
			conn.close();
			stmt.close();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
	public static String getEvolveIP(){
		ResultSet rs=null;
		String evolveip=null;
		
			System.out.println("Process on1");
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		
			try {
			System.out.println("Process on2");
			 

			Statement stmt = conn.createStatement();
			String query = "SELECT IPADDR  FROM IPADDR WHERE DESCR='EVOLVEIP';";
			rs=stmt.executeQuery(query);
			while(rs.next()){
				evolveip=rs.getObject(1).toString();
			}
			System.out.println("getting evolveip "+evolveip);
			
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
		
			e.printStackTrace();
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		return evolveip;
	}
	public static String getControllerIP(){
	
		ResultSet rs=null;
		String controllerip=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			System.out.println("Process on1");
			
		

			System.out.println("Process on2");
			 

			Statement stmt = conn.createStatement();
			String query = "SELECT IPADDR  FROM IPADDR WHERE DESCR='CONTROLLERIP';";
			rs=stmt.executeQuery(query);
			while(rs.next()){
				controllerip=rs.getObject(1).toString();
			}
			System.out.println("getting controllerip "+controllerip);
			
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		return controllerip;	
		
		
	}
	
	/**
	 * 
	 * this function return the controller type
	 * @return 
	 */
	public static String getControllerType(){
		
		ResultSet rs=null;
		String controllertype=null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			System.out.println("Process on1");
			
		

			System.out.println("Process on2");
			 

			Statement stmt = conn.createStatement();
			String query = "SELECT TYPE  FROM IPADDR WHERE DESCR='CONTROLLERIP';";
			rs=stmt.executeQuery(query);
			while(rs.next()){
				controllertype=rs.getObject(1).toString();
			}
			System.out.println("getting controllertype "+controllertype);
			
			conn.close();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		return controllertype;	
		
		
	}
	

}
