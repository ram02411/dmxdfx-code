package dto;

public class FindTraffical {
	
	private static String valtoshow;
	private static String findval;
	private static String finddetails;
	private static String filteralertype;
	private static String filterstartime;
	private static String filterendtime;
	private static String filterclassi;
	private static String filterprior;
	private static String filterprotocol;
	public static String getValtoshow() {
		return valtoshow;
	}
	public static void setValtoshow(String valtoshow) {
		FindTraffical.valtoshow = valtoshow;
	}
	public static String getFindval() {
		return findval;
	}
	public static void setFindval(String findval) {
		FindTraffical.findval = findval;
	}
	public static String getFinddetails() {
		return finddetails;
	}
	public static void setFinddetails(String finddetails) {
		FindTraffical.finddetails = finddetails;
	}
	public static String getFilteralertype() {
		return filteralertype;
	}
	public static void setFilteralertype(String filteralertype) {
		FindTraffical.filteralertype = filteralertype;
	}
	public static String getFilterstartime() {
		return filterstartime;
	}
	public static void setFilterstartime(String filterstartime) {
		FindTraffical.filterstartime = filterstartime;
	}
	public static String getFilterendtime() {
		return filterendtime;
	}
	public static void setFilterendtime(String filterendtime) {
		FindTraffical.filterendtime = filterendtime;
	}
	public static String getFilterclassi() {
		return filterclassi;
	}
	public static void setFilterclassi(String filterclassi) {
		FindTraffical.filterclassi = filterclassi;
	}
	public static String getFilterprior() {
		return filterprior;
	}
	public static void setFilterprior(String filterprior) {
		FindTraffical.filterprior = filterprior;
	}
	public static String getFilterprotocol() {
		return filterprotocol;
	}
	public static void setFilterprotocol(String filterprotocol) {
		FindTraffical.filterprotocol = filterprotocol;
	}
	
	
}
