package dto;

public class Filteralerts {
	private static String startime;
	private static String endime;
	private static String severity;
	private static String topic;
	private static String origin;
	private static String sortalerts;
	private static String query;
	private static String sortoutput;
	private static String querytype;
	
	
	public static String getQuerytype() {
		return querytype;
	}
	public static void setQuerytype(String querytype) {
		Filteralerts.querytype = querytype;
	}
	public static String getSortoutput() {
		return sortoutput;
	}
	public static void setSortoutput(String sortoutput) {
		Filteralerts.sortoutput = sortoutput;
	}
	public static String getQuery() {
		return query;
	}
	public static void setQuery(String query) {
		Filteralerts.query = query;
	}
	public static String getSortalerts() {
		return sortalerts;
	}
	public static void setSortalerts(String sortalerts) {
		Filteralerts.sortalerts = sortalerts;
	}
	public static String getSeverity() {
		return severity;
	}
	public static void setSeverity(String severity) {
		Filteralerts.severity = severity;
	}
	public static String getTopic() {
		return topic;
	}
	public static void setTopic(String topic) {
		Filteralerts.topic = topic;
	}
	public static String getOrigin() {
		return origin;
	}
	public static void setOrigin(String origin) {
		Filteralerts.origin = origin;
	}
	public static String getStartime() {
		return startime;
	}
	public static void setStartime(String stime) {
		startime = stime;
	}
	public static String getEndime() {
		return endime;
	}
	public static void setEndime(String etime) {
		endime = etime;
	}
	
}
