package dto;

public class Canvasdto {
	private static String switchdetails;
	private static String hostdetails;
	private static String linkdetails;
	private static String canvasname;
	private static String fetchcanvasname;
	
		
	public static String getFetchcanvasname() {
		return fetchcanvasname;
	}
	public static void setFetchcanvasname(String fetchcanvasname) {
		Canvasdto.fetchcanvasname = fetchcanvasname;
	}
	public static String getCanvasname() {
		return canvasname;
	}
	public static void setCanvasname(String canvasname) {
		Canvasdto.canvasname = canvasname;
	}
	public static String getSwitchdetails() {
		return switchdetails;
	}
	public static void setSwitchdetails(String switchdetails) {
		Canvasdto.switchdetails = switchdetails;
	}
	public static String getHostdetails() {
		return hostdetails;
	}
	public static void setHostdetails(String hostdetails) {
		Canvasdto.hostdetails = hostdetails;
	}
	public static String getLinkdetails() {
		return linkdetails;
	}
	public static void setLinkdetails(String linkdetails) {
		Canvasdto.linkdetails = linkdetails;
	}
	
	
}
