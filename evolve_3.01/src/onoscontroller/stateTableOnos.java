package onoscontroller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import canvas.util.JSONArray;
import canvas.util.JSONObject;
import dto.IPSdto;
import logger.logger;

/**
 * 
 * @author java
 *  This class is used to switches links and hosts from ONOS controller 
 *
 */
public class stateTableOnos {
	//static logger logme = logger.getInstance() ;
	
	 static String IP;
	 

	public static HashMap<String,String> host_switch;
	 public static List<String> hosts; 
	 public static List<String> src_dpid; 
	 public static List<String> src_port;
	 public static List<String> dst_port;
	 public static List<String> dst_dpid; 
	 public static HashMap<String,String> host_switchport;
	 
	@SuppressWarnings("static-access")
	
	/**
	 * 
	 * 
	 * @return 
	 */
	public static List<String> getTopo()   {
		
		IP   = IPSdto.getControllerIP();
		List<String> switches = null;
		
		host_switch = new HashMap<String,String>();
		hosts = new ArrayList<String>();
		src_dpid = new ArrayList<String>();
		dst_dpid = new ArrayList<String>();
		src_port = new ArrayList<String>();
		dst_port = new ArrayList<String>();
		
		host_switchport = new HashMap<String,String>();
		getLinks();
		switches=getSwitches();
		return switches;
	
	}
	
	/**
	 * 
	 * 
	 * @return 
	 */
	private static List<String> getSwitches()
	{
	
		String jsonresponse="";
		List<String> switches = new ArrayList<String>();
	
	try{
		URL Url=new URL("http://"+IP+":8181/onos/v1/devices");
		HttpURLConnection con = (HttpURLConnection) Url.openConnection();
	   con.setRequestProperty("Accept", "application/json");
	   con.setRequestMethod("GET");
	     
	   InputStream is = con.getInputStream();
	   BufferedReader br = new BufferedReader(new InputStreamReader(is));
	   String line = null;
	   while ((line = br.readLine()) != null) {
		   jsonresponse = jsonresponse.concat(line);
	   }
	   br.close();
		con.disconnect();
		
		JSONObject obj3 = new JSONObject(jsonresponse);
	    JSONArray geodata1 = obj3.getJSONArray("devices");
	     int n = geodata1.length();
	     String dpid1="";
	     for (int g = 0; g < n; g++) {
	    	    JSONObject person1 = geodata1.getJSONObject(g);
	    	    dpid1=person1.getString("id");
	    	    
	    	   switches.add(dpid1.substring(3, dpid1.length()));
	    	   
	    	   }
	  }
	catch(Exception e)
	{
		e.printStackTrace();
	}
	return switches;
	
}
	/**
	 * 
	 * 
	 *  
	 */
	private static void getLinks()
	{
	
		String jsonresponse="";
		
	
	try{
		URL Url=new URL("http://"+IP+":8181/onos/v1/links");
		HttpURLConnection con = (HttpURLConnection) Url.openConnection();
	   con.setRequestProperty("Accept", "application/json");
	   con.setRequestMethod("GET");
	     
	   InputStream is = con.getInputStream();
	   BufferedReader br = new BufferedReader(new InputStreamReader(is));
	   String line = null;
	   while ((line = br.readLine()) != null) {
		   jsonresponse = jsonresponse.concat(line);
	   }
	   br.close();
		con.disconnect();
		
		JSONObject obj3 = new JSONObject(jsonresponse);
	    JSONArray geodata1 = obj3.getJSONArray("links");
	     int n = geodata1.length();
	     String srcid1="";
	     String desid1="";
	     String sport="";
	     String dport="";
	     for (int g = 0; g < n; g++) {
	    	    JSONObject person1 = geodata1.getJSONObject(g);
	    	    JSONObject srcjson= person1.getJSONObject("src");
	    	    JSONObject desjson=  person1.getJSONObject("dst");
	    	    srcid1=srcjson.getString("device");
	    	    desid1=desjson.getString("device");
	    	   	    	   
	    	    src_dpid.add(srcid1.substring(3, srcid1.length()));
	    		   dst_dpid.add(desid1.substring(3, desid1.length()));
	    		   src_port.add((String) srcjson.get("port"));
	    		   dst_port.add((String) desjson.get("port"));
	    	   
	    	   
	    	   }
	  }
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
String jsonresponse1="";
		
	
	try{
		URL Url=new URL("http://"+IP+":8181/onos/v1/hosts/");
		HttpURLConnection con = (HttpURLConnection) Url.openConnection();
	   con.setRequestProperty("Accept", "application/json");
	   con.setRequestMethod("GET");
	     
	   InputStream is = con.getInputStream();
	   BufferedReader br1 = new BufferedReader(new InputStreamReader(is));
	   String line = null;
	   while ((line = br1.readLine()) != null) {
		   jsonresponse1 = jsonresponse1.concat(line);
	   }
	   br1.close();
		con.disconnect();
		
		JSONObject obj4 = new JSONObject(jsonresponse1);
	    JSONArray geodata2 = obj4.getJSONArray("hosts");
	     int n = geodata2.length();
	     String srcid1="";
	     String host1="";
	     String host2="";
	     for (int g = 0; g < n; g++) {
	    	    JSONObject person2 = geodata2.getJSONObject(g);
	    	    JSONObject location=person2.getJSONObject("location");
	    	    srcid1=location.getString("elementId");
	    	    host1=(String) person2.getJSONArray("ipAddresses").get(0);
	    	    //JSONObject host1=(JSONObject) ipaddress.get(0);
	    	  
	    	    hosts.add(host1);
			    host_switch.put(host1, srcid1.substring(3, srcid1.length()));
			    host_switchport.put(host1, String.valueOf(location.get("port")));
	    	   System.out.println(host1+"  "+srcid1.substring(3, srcid1.length()));
	    	   
	    	   }
	  }
	catch(Exception e)
	{
		e.printStackTrace();
	}
	
	
}
		

}
