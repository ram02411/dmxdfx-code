package data;
import java.util.List;

import bean.Rulebean;
import service.*;

public class Rulebasedata {
	static List<Rulebean> bean=RuleBase.printresult();
	public static int length=bean.size();
	public static String[] ruleid(){
		String[] Ruleid=new String[length];
		for(int i=0;i<length;i++){
			Ruleid[i]=bean.get(i).getRuleid().toString();
		}
		return Ruleid;
	}
	
	public static String[] switchid(){
		String[] Switchid=new String[length];
		for(int i=0;i<length;i++){
			Switchid[i]=bean.get(i).getRswitchnum().toString();
		}
		return Switchid;
	}
	public static String[] intfnum(){
		String[] Intfnum=new String[length];
		for(int i=0;i<length;i++){
			Intfnum[i]=bean.get(i).getRinter().toString();
		}
		return Intfnum;
	}
	
	public static String[] sourceip(){
		String[] Sourceip=new String[length];
		for(int i=0;i<length;i++){
			Sourceip[i]=bean.get(i).getRsourceip().toString();
		}
		return Sourceip;
	}
	public static String[] destnationip(){
		String[] Destip=new String[length];
		for(int i=0;i<length;i++){
			Destip[i]=bean.get(i).getRdestip();
		}
		return Destip;
	}
	
	public static String[] protocol(){
		String[] Protocol=new String[length];
		for(int i=0;i<length;i++){
			Protocol[i]=bean.get(i).getRprotocol().toString();
		}
		return Protocol;
	}
	
	public static String[] port(){
		String[] Port=new String[length];
		for(int i=0;i<length;i++){
			Port[i]=bean.get(i).getRport().toString();
		}
		return Port;
	}
	
	public static String[] priority(){
		String[] Priority=new String[length];
		for(int i=0;i<length;i++){
			Priority[i]=bean.get(i).getPriority().toString();
		}
		return Priority;
	}
	public static String[] action(){
		String[] Action=new String[length];
		for(int i=0;i<length;i++){
			Action[i]=bean.get(i).getRaction().toString();
		}
		return Action;
	}
	public static String[] rdate(){
		String[] Rdate=new String[length];
		for(int i=0;i<length;i++){
			Rdate[i]=bean.get(i).getRdatetime().toString();
		}
		return Rdate;
	}
}
