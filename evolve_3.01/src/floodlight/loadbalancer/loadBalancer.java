package floodlight.loadbalancer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class loadBalancer {
	private static final String RemoteHostName = "192.168.0.20";
	List<String> utils = new ArrayList<String>();
	
	
	private static Session connectSession(String userName, String password, String host) throws Exception {
        
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(userName, host, 22);
            session.setPassword(password);
            session.setTimeout(20000);
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.connect();                      
        }
        catch (JSchException ex) {
            System.out.println(ex);
        }
        return session;
    }
	
	public static void forVip(Session session, vip loadvip) throws JSchException, IOException{
		
		String command = "curl -X POST -d \'{\"id\":\""+loadvip.getVipName()+"\",\"name\":\""+ loadvip.getVipName()+"\",\"protocol\":\""+ loadvip.getVipProto()+"\",\"address\":\""+loadvip.getVipAddress()+"\",\"port\":\""+loadvip.getVipPort()+"\"}' http://"+RemoteHostName+":8080/quantum/v1.0/vips/";
		
		 Channel channel = null;
	        channel = session.openChannel("exec");
	        ((ChannelExec) channel).setCommand(command);
	        InputStream in = channel.getInputStream();
	        channel.connect();
	        System.out.println("channel.isConnected()  "+channel.isConnected());
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    	String msg1=null;
	    	  msg1=br.readLine();	
	    	  System.out.println(msg1);
			   channel.disconnect();
		        System.out.println("channel.isClosed()  "+channel.isClosed());
		
		
		
	}

public static void forPool(Session session, pool loadpool) throws JSchException, IOException{
		
	String command = "curl -X POST -d \'{\"id\":\""+ loadpool.getpoolid()  +"\",\"name\":\""+loadpool.getpoolname()   +"\",\"protocol\":\""+loadpool.getpoolproto()   +"\",\"vip_id\":\""+loadpool.getpool_vip_id()  +"\"}\' http://"+RemoteHostName+ ":8080/quantum/v1.0/pools/";
	System.out.println(command);
	 Channel channel = null;
       channel = session.openChannel("exec");
       ((ChannelExec) channel).setCommand(command);
       InputStream in = channel.getInputStream();
       channel.connect();
       System.out.println("channel.isConnected()  "+channel.isConnected());
       BufferedReader br = new BufferedReader(new InputStreamReader(in));
   	String msg1=null;
   	  msg1=br.readLine();	
   	  System.out.println(msg1);
		   channel.disconnect();
	        System.out.println("channel.isClosed()  "+channel.isClosed());
	
	}
public static void forMems(Session session, member loadmem) throws JSchException, IOException{
	
	String command = "curl -X POST -d \'{\"id\":\""+ loadmem.getmemid()   +"\",\"address\":\""+ loadmem.getmemaddress()   +"\",\"port\":\""+loadmem.getmemport()  +"\",\"pool_id\":\""+loadmem.getmem_pool_id()  +"\"}\' http://"+RemoteHostName + ":8080/quantum/v1.0/members/";
	
	 Channel channel = null;
       channel = session.openChannel("exec");
       ((ChannelExec) channel).setCommand(command);
       InputStream in = channel.getInputStream();
       channel.connect();
       System.out.println("channel.isConnected()  "+channel.isConnected());
       BufferedReader br = new BufferedReader(new InputStreamReader(in));
   	String msg1=null;
   	  msg1=br.readLine();	
   	  System.out.println(msg1);
		   channel.disconnect();
	        System.out.println("channel.isClosed()  "+channel.isClosed());
	
}
	
	
	
	 public loadBalancer(vip loadvip,pool loadpool,List<member> mems) {
	        System.out.println("starting");
	        
	        try {
	            Session session = connectSession("openflow", "openflow", RemoteHostName);
	            if(session!=null) {
	              forVip(session,loadvip); 
	              forPool(session,loadpool); 
	              for(int j=0;j<mems.size();j++)
	              {
	              forMems(session,mems.get(j)); 
	              }
	               

	                session.disconnect();
	            }
	        } catch (JSchException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (IOException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (Exception e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        System.out.println("Load balancing is done..");
	    }


}
