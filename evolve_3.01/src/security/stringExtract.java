package security;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import logger.logger;


public class stringExtract {
	static logger logme = logger.getInstance() ;

	/**
	 * @param conn 
	 * @param args
	 */
	public static HashMap<String,String> sorted(String alertme, Connection conn) {

		//String alert="04/16-05:04:15.741415  [**] [1:30524:1] SERVER-OTHER OpenSSL TLSv1.1 heartbeat read overrun attempt [**] [Classification: Attempted Information Leak] [Priority: 2] {TCP} 183.60.244.37:58553 -> 192.168.1.147:993 04/16-05:04:15.986131";
				String alert= alertme;
				int endp=alert.length();
			//	System.out.println("!!!!ALERT IS "+alert);
				int date_end=alert.indexOf("-");
				int time_end=date_end+9;
				int priority=alert.indexOf("[Priority:");
				
				HashMap<String,String> alet = new HashMap<String,String>();
				
				List<String> alertdb=new ArrayList<String>();
				
				String[][] alerts = new String [2][10];
				alerts[0][0] = "date";
				alerts[0][1] = "time";
				alerts[0][2] = "priority";
				alerts[0][3] = "classification";
				alerts[0][4] = "protocol";
				alerts[0][5] = "message";
				alerts[0][6] = "sourceip";
				alerts[0][7] = "source port ";
				alerts[0][8] = "destinationip";
				alerts[0][9] = "destination port";
				
				
			
				//get date
				String alertdate=alert.substring(0, date_end);
				//System.out.println("date is "+alertdate);
				int datebtw=alertdate.indexOf("/");
				String month1=alertdate.substring(0, datebtw);
				String date1=alertdate.substring(datebtw+1, alertdate.length());
				int year1 = Calendar.getInstance().get(Calendar.YEAR);
				String year=String.valueOf(year1);
				//String alert_date=date1+"-"+month1+"-"+year;
				String alert_date=year+"-"+month1+"-"+date1;
			//	System.out.println("alert date is "+alert_date);
				alet.put("date", alert_date);
				alerts[1][0] = alert_date;
				alertdb.add(alert_date);
				//got date
				
				//getting time
				String alert_time=alert.substring(date_end+1, time_end);
				System.out.println("time is "+alert_time);
				alet.put("time", alert_time);
				alerts[1][1] = alert_time;
				alertdb.add(alert_time);
				//got time
				
				//getting priority
				String sub1pr=alert.substring(priority+10, endp);
				int endpri=sub1pr.indexOf("]");
				String sub2pr=sub1pr.substring(0, endpri);
				System.out.println("priority is "+sub2pr);
				alet.put("priority", sub2pr);
				alerts[1][2]=sub2pr;
				alertdb.add(sub2pr);
				//got priority
				
				if(alert.contains("[Classification:")){
					int classification=alert.indexOf("[Classification:");
				//getting classification
				String sub1cl=alert.substring(classification+16, endp);
				int endcl=sub1cl.indexOf("]");
				String sub2cl=sub1cl.substring(0, endcl);
			//	System.out.println("classification is "+sub2cl);
				alet.put("classification", sub2cl);
				alerts[1][3]=sub2cl;
				alertdb.add(sub2cl);
				//got classification
				}
				else{
					alertdb.add(" ");
					alet.put("classification", " ");
				}
				
				//getting protocol
				String protocol=null;
				if(alert.contains("TCP")){
					protocol="TCP";
				}
				else if(alert.contains("UDP")){
					protocol="UDP";
				}
				else if(alert.contains("ICMP")){
					protocol="ICMP";
				}
				else{
					protocol="Not Recog.";
				}
				System.out.println("protocol is "+protocol);
				alet.put("protocol", protocol);
				alerts[1][4]= protocol;
				alertdb.add(protocol);
				//got protocol
				
				
				//getting alert message
			int firstbrack=alert.indexOf("]");
			//System.out.println("first bracket loc "+firstbrack);
			String sub1=alert.substring(firstbrack+1, endp);
			int secondbrack=sub1.indexOf("]");
			//System.out.println(sub1);
			//System.out.println("second bracket loc "+secondbrack);
			int endp2=sub1.length();
			String sub2=sub1.substring(secondbrack+2, endp2);
			//System.out.println(sub2);
			int endbrack_msg=sub2.indexOf("[");
			String alert_msg=sub2.substring(0, endbrack_msg);
			System.out.println("message is "+alert_msg);
			alet.put("message", alert_msg);
			alerts[1][5]= alert_msg;
			alertdb.add(alert_msg);
			//got alert message
			
			//getting source and destination
			int endofbrackets=alert.indexOf("}");
		//	System.out.println("IPS ARE !! "+alert.substring(endofbrackets+1, alert.length()));
			int btw=alert.indexOf("->");
			String source=alert.substring(endofbrackets+2, btw);
			String destbtw=alert.substring(btw+3, endp);
			int spacebtwdt=destbtw.indexOf(" ");
			//System.out.println("source is "+source);
		//	System.out.println("destbtw is -"+destbtw);
			String destination=null;
			if(destbtw.contains(" ")){
			destination=destbtw.substring(0, spacebtwdt);
			}
			else{
				destination=destbtw;
			}
		//	System.out.println("source is "+source);
		//	System.out.println("destination is "+destination);
			String srcip=null, srcport=null,destip=null, destport=null;
			if(source.contains(":")&&destination.contains(":")){
			int src_colm=source.indexOf(":");
			int dst_col=destination.indexOf(":");
			srcip=source.substring(0, src_colm);
			srcport=source.substring(src_colm+1, source.length());
			destip=destination.substring(0, dst_col);
			destport=destination.substring(dst_col+1, destination.length());
		//	System.out.println("sourceip is "+srcip);
			alet.put("srcip",srcip);
		//	System.out.println("destinationip is "+destip);
			alet.put("dstip", destip);
		//	System.out.println("source port is "+srcport);
			alet.put("srcport", srcport);
		//	System.out.println("destination port is "+destport);
			alet.put("dstport", destport);
			}
			else{
			//	System.out.println("going in else");
				srcip=source;
				destip=destination;
				srcport=" ";
				destport=" ";
			//	System.out.println("sourceip is "+srcip);
		//		System.out.println("destinationip is "+destip);
				alet.put("srcip",srcip);
				alet.put("dstip", destip);
				alet.put("srcport", srcport);
				alet.put("dstport", destport);
				
			}
			alerts[1][6]= srcip;
			alerts[1][7]=srcport;
			alerts[1][8]=destip;
			alerts[1][9]=destport;
			alertdb.add(srcip);
			alertdb.add(srcport);
			alertdb.add(destip);
			alertdb.add(destport);
			
			//calling method to enter alerts in db
		//	System.out.println("call enter alerts");
			if(source.length()<21 && destination.length()<21){
			enteralerts(alertdb,conn);
			}
			else{
			//	System.out.println("THE IPADDRESS IS IPV6 SO WE HAVE TO IGNORE IT !!!!!!!!!!!!!!!!!!!!!!");
			}
			//System.out.println("call finished enter alerts");
			return alet;
		
	}

	public static void enteralerts(List<String> alertlist, Connection conn) {
		try {
			//System.out.println("Process enter alerts");
			
			Statement st = conn.createStatement();

		//	System.out.println("Process enter2");

		//	System.out.println(alertlist.get(0));
		//	System.out.println(alertlist.get(2));
			String datetime=alertlist.get(0)+" "+alertlist.get(1);
			String altype="IDP_logs";
			String priority=alertlist.get(2);
			String classi=alertlist.get(3);
			String protocol=alertlist.get(4);
			String msg=alertlist.get(5);
			String sip=alertlist.get(6);
			String sport=alertlist.get(7);
			String dip=alertlist.get(8);
			String dport=alertlist.get(9);
			
			String toheck=priority.replace(" ", "");
			int checkpr=Integer.parseInt(toheck);
			if(checkpr==1||checkpr==2){
		/*	System.out.println("INSERT INTO TRAFFIC_ALERTS VALUES(DEFAULT, '"
					+ altype + "', '" + datetime + "', '" + priority
					+ "', '" + classi + "', '" + protocol + "', '"
					+ msg + "', '" + sip + "','" + sport
					+ "', '" + dip + "', '" +  dport + "' )");*/
			
			String SQL1 = String
					.format("INSERT INTO TRAFFIC_ALERTS VALUES(DEFAULT, '"
							+ altype + "', '" + datetime + "', '" + priority
							+ "', '" + classi + "', '" + protocol + "', '"
							+ msg + "', '" + sip + "','" + sport
							+ "', '" + dip + "', '" +  dport + "' )");
		//	System.out.println("entered");
			st.executeUpdate(SQL1);
			}
			else{
			//	System.out.println("VALUES NOT ENTERED BECAUSE PRIORITY IS "+priority);
			}
		} catch (Exception e) {
			logme.log.severe(e.getMessage());
		}
	}
	
}
