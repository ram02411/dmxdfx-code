
package security;
import java.io.IOException;
import java.util.Properties;

import logger.logger;
import networkCreator.Expect;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import dto.IPSdto;

public class snortStopex {

	private static final String RemoteHostName = IPSdto.getEvolveIP();
	
	static logger logme = logger.getInstance() ;

	
	@SuppressWarnings("static-access")
	public static void stopSnort() {
		try {
			JSch jsch=new JSch();
			 String remoteHostUserName = "demo";
			Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
			   String remoteHostpassword = "demo123!";
			session.setPassword(remoteHostpassword);
			   Properties config = new Properties();
			   config.put("StrictHostKeyChecking", "no");
			   session.setConfig(config);
			   session.connect();
		    Channel channel = session.openChannel("shell");
		    Expect expect = new Expect(channel.getInputStream(),
		            channel.getOutputStream());
		    
		    channel.connect();
		    expect.expect("demo@ubuntu:~$");
		    System.out.println(expect.before + expect.match);
		    
		    expect.send("ps -e|grep snort \n");
		  
		    expect.expect("demo@ubuntu:~$");
		    String msg1 = expect.before;
		 	String idd = msg1.substring(0, 5);
		    System.out.println(expect.before + expect.match);
		    expect.send("sudo kill -2 "+idd+"\n");
		   // expect.expect("demo@ubuntu:~$");
		  //  System.out.println(expect.before + expect.match);
		    
		    expect.close();
		    session.disconnect();
		} catch (JSchException e) {
	          logme.log.severe( e.getMessage());
		} catch (IOException e) {
	          logme.log.severe( e.getMessage());
		}
		   
          
		}

}
