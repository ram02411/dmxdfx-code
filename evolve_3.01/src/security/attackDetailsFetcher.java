package security;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import canvas.util.JSONException;
import service.*;
import bean.*;
import logger.logger;
import monitoring.*;
public class attackDetailsFetcher {
	static logger logme = logger.getInstance() ;

	/**
	 * @param args
	 */
	public static void createFirewall(HashMap<String,String> extracted) {

		//List<HashMap<String,String>> alerts = fileReader.getSortedAlerts();
		
		
		
		
		
		
			
					List<String> aldb=new ArrayList<String>();
					List<String> makerule=new ArrayList<String>();
					
					
					
					if(extracted.get("message").toLowerCase().contains("heartbleed")||extracted.get("classification").toLowerCase().contains("information leak")||extracted.get("classification").toLowerCase().contains("denial"))
					{
					String src_ip =extracted.get("srcip"); 
					String proto = extracted.get("protocol"); 
					String sdpid = switchNameFetcher.getSwitch();
				//	System.out.println("creating a firewall policy to block the"+" "+proto+" "+"traffic from"+" "+src_ip+" at switch"+" "+sdpid);
			          logme.log.info( "creating a firewall policy to block the"+" "+proto+" "+"traffic from"+" "+src_ip+" at switch"+" "+sdpid);

					aldb.add(extracted.get("date"));
					aldb.add(extracted.get("time"));
					aldb.add(extracted.get("priority"));
					aldb.add("Firewall Policy creation");
					aldb.add(extracted.get("protocol"));
					aldb.add("Security policy created to block information leak");
					aldb.add(extracted.get("srcip"));
					aldb.add(extracted.get("srcport"));
					aldb.add(extracted.get("dstip"));
					aldb.add(extracted.get("dstport"));
					enteralerts(aldb);
					
					makerule.add(proto);
					makerule.add(src_ip);
					makerule.add(sdpid);
					addtorulebase(makerule);
					try {
						String response = attackBlock.blockAttackIP(sdpid, src_ip,proto);
					//	System.out.println(response);
					} catch (KeyManagementException | NoSuchAlgorithmException
							| IOException | JSONException e) {
						// TODO Auto-generated catch block
				          logme.log.severe( e.getMessage());
					}
					
									
				
					}
		
				
		
	}
	
	public static void enteralerts(List<String> alertlist) {
		try {
			System.out.println("Process enter alerts");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
			Statement st = conn.createStatement();

			System.out.println("Process enter2");

			int year = Calendar.getInstance().get(Calendar.YEAR);
			int month=Calendar.getInstance().get(Calendar.MONTH);
			int date = Calendar.getInstance().get(Calendar.DATE);
			int hour = Calendar.getInstance().get(Calendar.HOUR);
			int minute = Calendar.getInstance().get(Calendar.MINUTE);
			int second = Calendar.getInstance().get(Calendar.SECOND);
			String datetime=year+"-"+month+"-"+date+" "+hour+"-"+minute+"-"+second;
			//String datetime=alertlist.get(0)+" "+alertlist.get(1);
			String altype="Firewall_logs";
			String priority=alertlist.get(2);
			String classi=alertlist.get(3);
			String protocol=alertlist.get(4);
			
			String msg=alertlist.get(5);
			String sip=alertlist.get(6);
			String sport=alertlist.get(7);
			String dip=alertlist.get(8);
			String dport=alertlist.get(9);
			
			String SQL1 = String
					.format("INSERT INTO TRAFFIC_ALERTS VALUES(DEFAULT, '"
							+ altype + "', '" + datetime + "', '" + priority
							+ "', '" + classi + "', '" + protocol + "', '"
							+ msg + "', '" + sip + "','" + sport
							+ "', '" + dip + "', '" +  dport + "' )");
			System.out.println("entered");
			st.executeUpdate(SQL1);
			conn.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}
	
	public static void addtorulebase(List<String> makerule) {
		Rulebean ruleb = new Rulebean();
		ResultSet rs=null;
		try {
			System.out.println("Process pnow1");
			Class.forName("com.mysql.jdbc.Driver");
			Connection conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");

			System.out.println("Process pnow2");
			//push to HP COMPLETE
			Statement stmt2 = conn.createStatement();
			ResultSet rb2 = null;
			String query2 = "SELECT * FROM FIREWALL_RULE_BASE;";
			rb2 = stmt2.executeQuery(query2);

			String rid = null;
			if(!(rb2.next())) {
				rid="11001";
			}
			else{
				rid = rb2.getObject(1).toString();
			}
			Integer out = Integer.parseInt(rid) + 1;

			Integer ruleid = out;
			int year = Calendar.getInstance().get(Calendar.YEAR);
			int month=Calendar.getInstance().get(Calendar.MONTH);
			int date = Calendar.getInstance().get(Calendar.DATE);
			int hour = Calendar.getInstance().get(Calendar.HOUR);
			int minute = Calendar.getInstance().get(Calendar.MINUTE);
			int second = Calendar.getInstance().get(Calendar.SECOND);
			String rdatetime=year+"-"+month+"-"+date+" "+hour+"-"+minute+"-"+second;
			String rproto=makerule.get(0);
			String rulesrcip=makerule.get(1);
			String ruleswitch=makerule.get(2);
			
			Statement stpush = conn.createStatement();
			
			int check=0;
			int insertindb=0;
			
			Statement stmt = conn.createStatement();
			String query = "SELECT * FROM FIREWALL_RULE_BASE;";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				String checkswitch=rs.getObject(2).toString();
				String checksrcip=rs.getObject(4).toString();
				String checkproto=rs.getObject(6).toString();
				String checkaction=rs.getObject(9).toString();
				
				if(ruleswitch.equalsIgnoreCase(checkswitch)){
					check=check+1;
				}
				if(rulesrcip.equalsIgnoreCase(checksrcip)){
					check=check+1;
				}
				if(rproto.equalsIgnoreCase(checkproto)){
					check=check+1;
				}
				if(checkaction.equalsIgnoreCase("DENY")){
					check=check+1;
				}
				
				if(check==4){
					insertindb=insertindb+1;
				}
				
			}
			
			if(insertindb==0){
			String SQLpush = String
					.format("INSERT INTO FIREWALL_RULE_BASE VALUES('" + ruleid
							+ "', '" + ruleswitch + "', '" + " " + "', '"
							+ rulesrcip + "', '" + " " + "', '" + rproto
							+ "', '" + " " + "', '"+ " " + "','" + "DENY" + "', '"
							+ rdatetime + "'" + ")");
			stpush.executeUpdate(SQLpush);
			System.out.println("entered");
			}
			else{
				System.out.println("THE RULE ALREADY EXISTS !!!!!!!!!!!!!!!!! IN THE DATABASE");
			}

			
			/*System.out.println(ruleswitch);
			System.out.println(rproto);
			st.executeUpdate(SQL1);*/
			conn.close();
			

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			e.printStackTrace();
		}
	}
	
	
}
