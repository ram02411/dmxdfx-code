package monitoring;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import logger.logger;

import canvas.util.JSONException;

import stateTable.stateTableHP;

public class TopoContructor {
	static logger logme = logger.getInstance() ;

	/**
	 * @param args
	 */
	public static String getTopo() throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException{
	
		
		List<String> switches = null;
		try {
			switches = stateTableHP.getTopo();
		} catch (KeyManagementException e) {
	          logme.log.severe( e.getMessage());

		} catch (NoSuchAlgorithmException e) {
	          logme.log.severe( e.getMessage());

		} catch (IOException e) {
	          logme.log.severe( e.getMessage());

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> hosts =  stateTableHP.hosts;
		List<String> src_dpid = stateTableHP.src_dpid;
		List<String> dst_dpid = stateTableHP.dst_dpid;
		HashMap<String,String> host_switch = stateTableHP.host_switch;
		HashMap<String,String> switch_color = topoMonitor.getSwitchColor();
				
		String out = null;
		if(switches!=null){
		
		out = "[" + "\n" + "{" + "\n" + " \"adjacencies\": [" ;
		System.out.println(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			out =  out +  "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + hosts.get(i) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ host_switch.get(hosts.get(i)) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#29220A\"" + "\n" + "}" + "\n" + "}";
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \""+ switch_color.get(host_switch.get(hosts.get(i))) +"\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + host_switch.get(hosts.get(i))+ "\"," + "\n" +
		        "\"name\": \"" + host_switch.get(hosts.get(i))+"\"" + "\n" +
		      "}" + "," + "{" + "\n" +  "\"adjacencies\": [" ;
			
		}
		
		
		for(int j=0;j<src_dpid.size();j++){
			out = out + "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + src_dpid.get(j) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ dst_dpid.get(j) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#557EAA\"" + "\n" + "}" + "\n" + "}";
			
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \""+ switch_color.get(dst_dpid.get(j)) +"\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + dst_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + dst_dpid.get(j)+"\"" + "\n" +
		      "}"+ "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		out = out + "]," + "\n" +
        "\"data\": {" + "\n" + 
            "\"$color\": \"\"," + "\n" +
            "\"$type\": \"\"," + "\n" +
            "\"$dim\": 0" + "\n" +
          "}," + "\n" +
          "\"id\": \"\"," + "\n" +
          "\"name\": \"\"" + "\n" +
				"}" + "," ; 
		
		for (int j = 0; j < hosts.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#416D9C\"," + "\n" +
		          "\"$type\": \"square\"," + "\n" +
		          "\"$dim\": 4" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + hosts.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + hosts.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		for (int j = 0; j < src_dpid.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + src_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + src_dpid.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		out = out + "{" + "\n" +
				"\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" + 
		            "\"$color\": \"\"," + "\n" +
		            "\"$type\": \"\"," + "\n" +
		            "\"$dim\": 0" + "\n" +
		          "}," + "\n" +
		          "\"id\": \"\"," + "\n" +
		          "\"name\": \"\"" + "\n" +
						"}" + "\n" + "]" ; 
		
		//System.out.println(out);
		
		
		
		}
		return out;
		
		
		
		
	}


	}


