package monitoring;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import logger.logger;

import FlowPusher.portDetails;

import canvas.util.JSONException;

import stateTable.stateTableHP;

public class topoMonitor {
	static logger logme = logger.getInstance() ;

	/**
	 * @param args
	 */
	@SuppressWarnings("static-access")
	public static  List<HashMap<String,String>> getSwitchColor(){
		List<String> switches =null;
		List<HashMap<String,String>> hm = new ArrayList<HashMap<String,String>>();
		HashMap<String,String> switchcolor=new HashMap<String,String>();
		HashMap<String,String> switchsize=new HashMap<String,String>();
		
			 switches = stateTableHP.getTopo();
		
		
		
		
		
		for(int i=0;i<switches.size();i++){
			//System.out.println("Switch dpid:"+switches.get(i));
			List<HashMap<String,String>> ports = null;
			
			try {
				ports = portDetails.getPorts(switches.get(i));
			} catch (KeyManagementException e) {
		          logme.log.severe( e.getMessage());

			} catch (NoSuchAlgorithmException e) {
		          logme.log.severe( e.getMessage());

			} catch (IOException e) {
		          logme.log.severe( e.getMessage());

			} catch (JSONException e) {
		          logme.log.severe( e.getMessage());

			} 
		
			List<String> configs = new ArrayList<String>();
	for(int j=0;j<ports.size();j++){
				
				//System.out.println(ports.get(j).get("state"));
		if(!(String.valueOf(ports.get(j).get("config")).equalsIgnoreCase("")||ports.get(j).get("config")==null)){
			//System.out.println("here "+ports.get(j).get("config"));	
			configs.add(ports.get(j).get("config"));}
				
	}
	
	 
	
	
	
	String switch_color="";
	String switch_size="";
	if(ports.size()==configs.size()){
		switch_color="#FF1205";
		switch_size="10";
		//System.out.println("red");
	}
	else if(configs.size()>1&&configs.size()<ports.size()){
		switch_color="#FFC200";
		switch_size="8";
	//	System.out.println("amber");
	}
	else{
		switch_color="#4AC556";
		switch_size="6";
		//System.out.println("green");
	}
	
	switchcolor.put(switches.get(i), switch_color);
	switchsize.put(switches.get(i), switch_size);
	
	
	
	//System.out.println("switch color is  "+switches.get(i) + switchcolor.get(switches.get(i)));
		}
		hm.add(switchcolor);
		hm.add(switchsize);
		return hm;
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
