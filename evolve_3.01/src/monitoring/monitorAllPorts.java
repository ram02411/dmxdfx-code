package monitoring;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import logger.logger;

import canvas.util.JSONArray;
import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;

public class monitorAllPorts {
static String IP=IPSdto.getControllerIP();
	public static String switchid;
	static logger logme = logger.getInstance() ;

	@SuppressWarnings("static-access")
	public static void monitorAll() throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException, InterruptedException {

		String token = getAuth();
	
		 TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
             public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
             }
             public void checkClientTrusted(X509Certificate[] certs, String authType) {
             }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
             }
         }
     };

    
    SSLContext sc = SSLContext.getInstance("SSL");
    sc.init(null, trustAllCerts, new java.security.SecureRandom());
     HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

    
     HostnameVerifier allHostsValid = new HostnameVerifier() {
         public boolean verify(String hostname, SSLSession session) {
             return true;
        }
     };

   
     HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

     

    
     
     URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/stats/ports");
     HttpURLConnection con = (HttpURLConnection) url.openConnection();
  con.setRequestMethod("GET");
con.setRequestProperty ("X-Auth-Token", token );

    InputStreamReader reader = new InputStreamReader(con.getInputStream());
  String res="";
  while (true) {
         int ch = reader.read();
      if(ch!=-1){
        res=res+(char)ch;}
      else{break;}
     }
  
  
HashMap<String,List<String>> porttable = new HashMap<String,List<String>>();

	    JSONObject obj = new JSONObject(res);
	     JSONArray geodata1 = obj.getJSONArray("stats");
	      int m = geodata1.length();
	    String arr[] = new String[20];
	   for (int j = 0; j < m; j++) {
	    	List<String>ports = new ArrayList<String>();
	     JSONObject person1 = geodata1.getJSONObject(j);
	  
	 //  System.out.println(person1.get("dpid"));
	 
	  
	   
	  JSONArray arr1 = person1.getJSONArray("port_stats");
	 
	  for(int i=0;i<arr1.length();i++){
	   JSONObject p2 = arr1.getJSONObject(i);
	   if(p2.getInt("rx_bytes")>5000||p2.getInt("rx_dropped")>50||p2.getInt("collisions")>50||p2.getInt("rx_errors")>50||p2.getInt("rx_frame_err")>10||p2.getInt("rx_over_err")>10)
	   {ports.add(String.valueOf(p2.getInt("port_id")));}
	 
	    }
	porttable.put(person1.getString("dpid"),ports);
	   
	if(ports.size()>0)
	{
		for(int k=0;k<ports.size();k++)
		{

		     String dpid1 =person1.getString("dpid");
		     
		     String sorted="";
		     for(int i=0;i<21;i++)
		     {
		     	sorted = sorted + (char)dpid1.charAt(i)+(char)dpid1.charAt(i+1)+"%3A";
		     	i= i+2;
		     }
		     
		     sorted = sorted+(char)dpid1.charAt(21)+(char)dpid1.charAt(22);
		     
		String otp = disablePort(sorted,ports.get(k),token);     
		   //  System.out.println(otp);
		     if(otp.equals("202")){
		          logme.log.info( "switch:"+person1.getString("dpid")+" "+"port:"+ports.get(k)+" "+"is disabled");

		  //   System.out.println("switch:"+person1.getString("dpid")+" "+"port:"+ports.get(k)+" "+"is disabled");
		     }
		     Thread.sleep(7000);
		     String otp1 = enablePort(sorted,ports.get(k),token);
		   //  System.out.println(otp);
		     if(otp1.equals("202")){	
		          logme.log.info( "switch:"+person1.getString("dpid")+" "+"port:"+ports.get(k)+" "+"is enabled");

		    // System.out.println("switch:"+person1.getString("dpid")+" "+"port:"+ports.get(k)+" "+"is enabled");
		     }
		   //  System.out.println("auto healing completed");
		     logme.log.info("auto healing completed on"+person1.getString("dpid")+" "+"port:"+ports.get(k));
		}
	}
	}
	   
	   
	}

	private static String disablePort(String sorted, String portid, String token) throws NoSuchAlgorithmException, KeyManagementException, IOException {
		
		

		 TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };

  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    

   
    
    URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/datapaths/"+sorted+"/ports/"+portid+"/action");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty ("X-Auth-Token", token );
con.setDoOutput(true);

OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
wr.write("disable");
wr.flush();


wr.close();
		
		
		return String.valueOf(( con.getResponseCode()));
				
		
	}
	private static String enablePort(String sorted, String portid, String token) throws NoSuchAlgorithmException, KeyManagementException, IOException {
		
		

		 TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
           public java.security.cert.X509Certificate[] getAcceptedIssuers() {
              return null;
           }
           public void checkClientTrusted(X509Certificate[] certs, String authType) {
           }
          public void checkServerTrusted(X509Certificate[] certs, String authType) {
           }
       }
   };

  
  SSLContext sc = SSLContext.getInstance("SSL");
  sc.init(null, trustAllCerts, new java.security.SecureRandom());
   HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

  
   HostnameVerifier allHostsValid = new HostnameVerifier() {
       public boolean verify(String hostname, SSLSession session) {
           return true;
      }
   };

 
   HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

   

  
   
   URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/datapaths/"+sorted+"/ports/"+portid+"/action");
   HttpURLConnection con = (HttpURLConnection) url.openConnection();
con.setRequestMethod("POST");
con.setRequestProperty ("X-Auth-Token", token );
con.setDoOutput(true);

OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
wr.write("enable");
wr.flush();


wr.close();
		
		
		return String.valueOf(( con.getResponseCode()));
				
		
	}
	private static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException {
		String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
		String jsonresponse = "";
		

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty("Content-Type", "application/json");

	con.setDoOutput(true);
 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		
 wr.write(tokenInput);
	wr.flush();
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}
	wr.close();
	rd.close();
	
	 JSONObject obj1 = new JSONObject(jsonresponse);
     JSONObject geodata = obj1.getJSONObject("record");
     String token = geodata.getString("token");
 
	return token;
				
	}


	}


