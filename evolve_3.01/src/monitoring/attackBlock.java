package monitoring;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import stateTable.stateTableHP;

import logger.logger;

import FlowPusher.flowPusher;

import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;

public class attackBlock {
 static String IP=IPSdto.getControllerIP();
	static logger logme = logger.getInstance() ;

	@SuppressWarnings("unused")
	public static String blockAttackIP(String dpid,String srcip,String protocol) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
		
		Random r = new Random();
		int i1 = r.nextInt(80 - 65) + 65;
		
		String priority=""+i1;
		
String output ="{\"flow\":{";
output= output+ "\"priority\":"+priority+",";
output = output+ "\"match\": [";

	output = output+"{ \"ipv4_src\":"+"\""+srcip+"\"" +"},";
	output = output+"{ \"eth_type\":"+"\""+"ipv4"+"\"" +"},";
	output = output+"{ \"ip_proto\":"+"\""+ protocol+"\"" +"}";





output=output+"],";

output = output+ "\"actions\": [";

output=output+"]}}";
try{
	List<String> switches = stateTableHP.getTopo();
	for(int i=0;i<switches.size();i++){
String res = flowPusher.pushFlows(switches.get(i), output);} }
catch(Exception e)
{
    logme.log.severe( e.getMessage());

}
logme.log.warning( "Attack has been blocked from"+" "+srcip+"check the logs for other details");


	return "Attack has been blocked from"+" "+srcip+"check the logs for other details";

	}
				
		
	@SuppressWarnings({ "static-access", "unused" })
	public static String blockAttackMAC(String dpid,String srcmac) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
		Random r = new Random();
		int i1 = r.nextInt(80 - 65) + 65;
		
		String priority=""+i1;		
String output ="{\"flow\":{";
output= output+ "\"priority\":"+priority+",";
output = output+ "\"match\": [";

	output = output+"{ \"eth_src\":"+"\""+srcmac+"\"" +"},";
	output = output+"{ \"eth_type\":"+"\""+"ipv4"+"\"" +"},";



output=output+"],";

output = output+ "\"actions\": [";

output=output+"]}}";
try{
String res = flowPusher.pushFlows(dpid, output); }

catch(Exception e)
{
    logme.log.severe( e.getMessage());

}
logme.log.info( "Attack has been blocked from"+" "+srcmac+"check the logs for other details");

	return "Attack has been blocked from"+" "+srcmac+"check the logs for other details";
	}
				
		
		
		
		

	
	

}