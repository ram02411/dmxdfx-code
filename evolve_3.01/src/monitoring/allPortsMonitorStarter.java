package monitoring;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.TimerTask;

import logger.logger;

import canvas.util.JSONException;

public class allPortsMonitorStarter extends TimerTask {
	static logger logme = logger.getInstance() ;

	@SuppressWarnings("static-access")
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			monitorAllPorts.monitorAll();
	          logme.log.info( "monitoring on all ports started");

		} catch (KeyManagementException | NoSuchAlgorithmException
				| IOException | JSONException | InterruptedException e) {
			// TODO Auto-generated catch block
	          logme.log.severe( e.getMessage());
		}


	}

}
