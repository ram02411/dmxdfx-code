package monitoring;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.TimerTask;

import logger.logger;

import canvas.util.JSONException;

public class switchMonitorStarter extends TimerTask {
	static logger logme = logger.getInstance() ;

	@Override
	public void run() {

		
		String dpid = allPortsTimer.dpid;
		try {
			switchMonitor.healSwitch(dpid);
		} catch (KeyManagementException | NoSuchAlgorithmException
				| IOException | JSONException | InterruptedException e) {
			// TODO Auto-generated catch block
	          logme.log.severe( e.getMessage());
		}
	}

}
