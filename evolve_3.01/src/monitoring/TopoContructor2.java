package monitoring;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import logger.logger;

import org.apache.poi.ss.formula.functions.T;

import canvas.util.JSONException;

import stateTable.stateTableHP;

public class TopoContructor2 {

	public static Integer topocheck=0; 
	/**
	 * @param args
	 */
	/**
	 * @param argv
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 * @throws JSONException
	 */
	
	static logger logme = logger.getInstance() ;

	@SuppressWarnings("static-access")
	public static String getTopo() throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException{
	
		
		List<String> switches = null;
		List<String> switches2 = stateTableHP.getTopo();
		
			switches = stateTableHP.getTopo();
		
		List<String> hosts =  stateTableHP.hosts;
		List<String> src_dpid = stateTableHP.src_dpid;
		List<String> dst_dpid = stateTableHP.dst_dpid;
		HashMap<String,String> host_switch = stateTableHP.host_switch;
		List<HashMap<String,String>> hm = topoMonitor.getSwitchColor();
		HashMap<String,String> switch_color = hm.get(0);
		HashMap<String,String> switch_size = hm.get(1);
		//HashMap<String,String> switch_color = topoMonitor.getSwitchColor();
				
		String out = null;
		//TopoCheck.setChecktopoforapi(false);
		if(switches!=null){
		//System.out.println("VALUE FROM ORIGINAL BEAN IS "+TopoCheck.isChecktopoforapi());
			if(TopoCheck.isChecktopoforapi()==false){
				topocheck=topocheck+1;
			int check=0;
			out="[";
			for(int a=0;a<switches2.size();a++){
				check=0;
				//System.out.println("switches list item is "+switches2.get(a));
				out = out + "{" + "\n" +
						"\"id\": " + "\"" + switches2.get(a) + "\"," + "\n" +
						"\"name\": " + "\"" + switches2.get(a) + "\"," + "\n" +
						"\"data\": {" + "\n" +
						"\"$dim\": " + switch_size.get(switches2.get(a)) + "," + "\n" +
						"\"$color\": " + "'"+ switch_color.get(switches2.get(a)) + "'," + "\n" +
						"}," + "\n" +
						"\"adjacencies\":" + "[" ;
				for(int b=0;b<src_dpid.size();b++){
					//System.out.println("scrdpid item is "+src_dpid.get(b));
					if((switches2.get(a)).equalsIgnoreCase(src_dpid.get(b))){
						check=check+1;
						out = out + "{" + "\n" +
								"\"nodeTo\": " + "\"" + dst_dpid.get(b) + "\"," + "\n" +
								"\"data\": {" + "\n" +
											"\"weight\": 1" + "\n" +
										"}" + "\n" +
											"}, " + "\n" ;
					}
				}
				//System.out.println("value of check is "+check);
				if(check==0){
				//	System.out.println("remove free node");
					int reml=out.length();
					out=out.substring(0, reml-120);
				}
				//System.out.println(" BEFORE " +out + "\n");
				int cut = out.length();
				//System.out.println("LENGTH "+cut);
				out=out.substring(0, cut-3);
				//System.out.println("AFTER "+out);
				out = out + "] " + "\n" +
 						"}, " ;
			}
			int cut2=out.length();
			out=out.substring(0, cut2-2);
			out = out + "]" ;
			TopoCheck.setChecktopoforapi(true);
			TopoCheck.setTopojson(out);
			}
			
			else if(TopoCheck.isChecktopoforapi()==true){
			//	System.out.println("NOW JUST CHANGE THE COLORS OF ");
				String jsonval=TopoCheck.getTopojson();
				for(int b=0;b<switches2.size();b++){
				String checkswitchpos="\"name\": " + "\"" + switches2.get(b) + "\"," ;
				if(jsonval.contains(checkswitchpos)){
				//	System.out.println("POSITION OF EACH PLACE IS "+jsonval.indexOf(checkswitchpos));
					Integer pos1=jsonval.indexOf(checkswitchpos);
					String extrac=jsonval.substring(pos1+35, pos1+79);
					String extract = new String(extrac);
				//	System.out.println("EXTRACTED STRING IS "+extract);
				//	System.out.println("EXTRACTED STRING IS "+extract.substring(18, 19));
				//	System.out.println("EXTRACTED STRING IS "+extract.substring(32, 39));
				//	System.out.println("REPLACABLE  STRING IS "+switch_size.get(switches2.get(b)));
				//	System.out.println("REPLACABLE STRING IS "+switch_color.get(switches2.get(b)));
					StringBuffer rnow=new StringBuffer(extract);
					rnow.replace(18, 19, switch_size.get(switches2.get(b)));
					rnow.replace(32, 39, switch_color.get(switches2.get(b)));
					StringBuffer rnowmain=new StringBuffer(jsonval);
					String repl=rnow.toString();
				//	System.out.println("TO BE REPLACED STRING "+repl);
					rnowmain.replace(pos1+35, pos1+79, repl);
					//extract.replace((extract.substring(18, 19)), switch_size.get(switches2.get(b)));
					//extract.replace((extract.substring(32, 39)), switch_color.get(switches2.get(b)));
					//extract.replaceAll((extract.substring(18, 19)), switch_size.get(switches2.get(b)));
					//extract.replaceAll((extract.substring(32, 39)), switch_color.get(switches2.get(b)));
					//String replc=jsonval.substring(pos1+35, pos1+79);
					//jsonval.replaceAll(replc, extract);
					//jsonval.replace(replc, extract);
					String chng=rnowmain.toString();
				//	System.out.println("CHANGED VALUE IS !!!!!!!!!!!!!!!!!!! "+chng);
					jsonval=chng;
					
				}
				}	
			//	System.out.println("NEW JSON OBJECT IS "+jsonval);
				TopoCheck.setChecktopoforapi(true);
				TopoCheck.setTopojson(jsonval);
				out=jsonval;
			}
			
		/*out = "[" + "\n" + "{" + "\n" + " \"adjacencies\": [" ;
		System.out.println(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			out =  out +  "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + hosts.get(i) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ host_switch.get(hosts.get(i)) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#29220A\"" + "\n" + "}" + "\n" + "}";
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \""+ switch_color.get(host_switch.get(hosts.get(i))) +"\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + host_switch.get(hosts.get(i))+ "\"," + "\n" +
		        "\"name\": \"" + host_switch.get(hosts.get(i))+"\"" + "\n" +
		      "}" + "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		
		for(int j=0;j<src_dpid.size();j++){
			out = out + "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + src_dpid.get(j) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ dst_dpid.get(j) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#557EAA\"" + "\n" + "}" + "\n" + "}";
			
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \""+ switch_color.get(dst_dpid.get(j)) +"\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + dst_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + dst_dpid.get(j)+"\"" + "\n" +
		      "}"+ "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		out = out + "]," + "\n" +
        "\"data\": {" + "\n" + 
            "\"$color\": \"\"," + "\n" +
            "\"$type\": \"\"," + "\n" +
            "\"$dim\": 0" + "\n" +
          "}," + "\n" +
          "\"id\": \"\"," + "\n" +
          "\"name\": \"\"" + "\n" +
				"}" + "," ; 
		
		for (int j = 0; j < hosts.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#416D9C\"," + "\n" +
		          "\"$type\": \"square\"," + "\n" +
		          "\"$dim\": 4" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + hosts.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + hosts.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		for (int j = 0; j < src_dpid.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + src_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + src_dpid.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		out = out + "{" + "\n" +
				"\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" + 
		            "\"$color\": \"\"," + "\n" +
		            "\"$type\": \"\"," + "\n" +
		            "\"$dim\": 0" + "\n" +
		          "}," + "\n" +
		          "\"id\": \"\"," + "\n" +
		          "\"name\": \"\"" + "\n" +
						"}" + "\n" + "]" ; 
		
		System.out.println(out);
		
		
		
		}*/
	//	System.out.println("Final output is  "+ out);
		
		
		
		
	}
		return out;

	}
}

