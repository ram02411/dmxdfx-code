package monitoring;

import java.util.List;
import java.util.Timer;

import logger.logger;



public class allPortsTimer {

	
	public static String stopper="";
	public static String dpid;
	public static String switchstopper="";
	static logger logme = logger.getInstance() ;

	public static void setStopper(String stoped)
	{
		stopper = stoped;
	}
	public static void setSwitchStopper(String stoped)
	{
		switchstopper = stoped;
	}
	
 public allPortsTimer()
 
 {
	 
	 scheduleRule(); 
	 
 }

 public allPortsTimer(String dpidi)
 
 {
	 dpid = dpidi;
	 scheduleRuleSwitch(); 
	 
 }


private void scheduleRuleSwitch() {
	//System.out.println("Scheduling for traffic engineering starting from now!");
    logme.log.info( "Scheduling for traffic engineering starting now!");

	Timer timer = new Timer();
	timer.scheduleAtFixedRate(new switchMonitorStarter(),5000,30000);
	if(stopper.equals("stop"))
	{
		timer.cancel();
	}
	
}

public static void scheduleRule() {
	//System.out.println("Scheduling for traffic engineering starting from now!");
    logme.log.info( "Scheduling for traffic engineering starting now!");

	Timer timer = new Timer();
	timer.scheduleAtFixedRate(new allPortsMonitorStarter(),5000,30000);
	if(switchstopper.equals("stop"))
	{
		timer.cancel();
	}
	
}
	
	

}
