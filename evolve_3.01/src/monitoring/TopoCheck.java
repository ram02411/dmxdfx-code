package monitoring;

public class TopoCheck {
public static boolean checktopoforapi=false;
public static String topojson;


public static String getTopojson() {
	return topojson;
}

public static void setTopojson(String topojson) {
	TopoCheck.topojson = topojson;
}

public static boolean isChecktopoforapi() {
	return checktopoforapi;
}

public static void setChecktopoforapi(boolean checktopoforapi) {
	TopoCheck.checktopoforapi = checktopoforapi;
}

}
