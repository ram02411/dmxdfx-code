package connections;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import logger.logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import dto.IPSdto;

public class circularGauge {
	private static final String RemoteHostName = "192.168.0.20";
	List<String> utils = new ArrayList<String>();
	static Float cpuutilization;
	static int hardutilization;
	static int memutilization;
	static logger logme = logger.getInstance() ;
	
		@SuppressWarnings("static-access")
		private static Session connectSession(String userName, String password, String host) throws Exception {
	        
	        JSch jsch = new JSch();
	        Session session = null;
	        try {
	            session = jsch.getSession(userName, host, 22);
	            session.setPassword(password);
	            session.setTimeout(10000);
	            java.util.Properties config = new java.util.Properties();
	            config.put("StrictHostKeyChecking", "no");
	            session.setConfig(config);
	            session.connect();                      
	        }
	        catch (JSchException ex) {
	          logme.log.severe( ex.getMessage());
	        }
	        return session;
	    }

	    
	    private static void cpuutil(Session session, String command) throws JSchException, IOException

	    {
	    	//System.out.println(command);
	        Channel channel = null;
	        channel = session.openChannel("exec");
	        ( (ChannelExec) channel).setCommand(command);
	        InputStream in = channel.getInputStream();
	        channel.connect();
	      //  System.out.println("channel.isConnected()  "+channel.isConnected());
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	    	String msg1=null;
			   msg1=br.readLine();	
			   channel.disconnect();
		      //  System.out.println("channel.isClosed()  "+channel.isClosed());
		        
		        cpuutilization = Float.parseFloat(msg1);	        
	    }
	    private static void harddisk(Session session, String command) throws JSchException, IOException

	    {
	    	System.out.println(command);
	        Channel channel = null;
	        channel = session.openChannel("exec");
	        ((ChannelExec) channel).setCommand(command);
	        InputStream in = channel.getInputStream();
	        channel.connect();
	        System.out.println("channel.isConnected()  "+channel.isConnected());
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        String msg=null;
			   msg=br.readLine();
			   msg=br.readLine();
			    int i = msg.indexOf("%");
			    String out= msg.substring(i-2, i);
	      	   channel.disconnect();
			     			   
			    hardutilization = Integer.parseInt(out);
		        System.out.println("channel.isClosed()  "+channel.isClosed());
		        
		        	        
	    }
	    private static void memutil(Session session, String command) throws JSchException, IOException

	    {
	    	System.out.println(command);
	        Channel channel = null;
	        channel = session.openChannel("exec");
	        ((ChannelExec) channel).setCommand(command);
	        InputStream in = channel.getInputStream();
	        channel.connect();
	      //  System.out.println("channel.isConnected()  "+channel.isConnected());
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        String msg1 = br.readLine();
			  String output = msg1.substring(26, 29);
	      //  System.out.println(output);
			   channel.disconnect();
			     		   
			   memutilization = Integer.parseInt(output); 
			   memutilization = memutilization/10;
	    }
	    
	    
	    public static Float getcpuUtil()
	    {
	    	return cpuutilization;
	    }
	    public static int gethardUtil()
	    {
	    	return hardutilization;
	    }
	    public static int getmemUtil()
	    {
	    	return memutilization;
	    }
	    
	    
	    
	    public circularGauge() {
	      //  System.out.println("starting");
	        // TODO Auto-generated method stub
	        try {
	            Session session = connectSession("sdn", "skyline", RemoteHostName);
	            if(session!=null) {
	               cpuutil(session, "top -b -n1 | grep 'Cpu(s)' | awk '{print $2 + $4}'");
	                harddisk(session, "df -m");
	                memutil(session, "free -m | grep /+");

	                session.disconnect();
	            }
	        } catch (JSchException e) {
	            // TODO Auto-generated catch block
	        	logme.log.severe( e.getMessage());
	        } catch (IOException e) {
	        	logme.log.severe( e.getMessage());
	        } catch (Exception e) {
	        	logme.log.severe( e.getMessage());
	        }
	        logme.log.severe( "controller machine status updated");;
	    }

}
