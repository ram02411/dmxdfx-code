package connections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class memUtil {

	private static final String RemoteHostName = "192.168.0.10";
	public static int getMemUtil() throws IOException, JSchException {
		
		JSch jsch=new JSch();
		 String remoteHostUserName = "odp";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "passwd";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();

		   ChannelExec channel=(ChannelExec) session.openChannel("exec");
		   BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
		    
		    
		   channel.setCommand("free -m | grep /+");
		   channel.connect();

		   
		   String msg1 = in.readLine();
		  String output = msg1.substring(26, 29);
        System.out.println(output);
		   channel.disconnect();
		     		   
		   session.disconnect();
		
		   int mutil = Integer.parseInt(output);
		   return mutil;

	}

}
