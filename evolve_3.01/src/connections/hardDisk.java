package connections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class hardDisk {
	private static final String RemoteHostName = "192.168.0.10";
	
	public static int getHardDisk() throws JSchException, IOException {
		JSch jsch=new JSch();
		 String remoteHostUserName = "odp";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "passwd";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();

		   ChannelExec channel=(ChannelExec) session.openChannel("exec");
		   BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
		   channel.setCommand("df -m");
		   channel.connect();

		   String msg=null;
		   msg=in.readLine();
		   msg=in.readLine();
		    int i = msg.indexOf("%");
		    String out= msg.substring(i-2, i);
      	   channel.disconnect();
		     		   
		   session.disconnect();

		   int hdisk = Integer.parseInt(out);
		   return hdisk;
		   
	}

}
