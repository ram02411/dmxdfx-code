










package connections;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import sources.sntTest;
import sources.stateTableSNT;

public class Canvaspush {

	private static String mini1;
	private static String mini2;
	private static final String RemoteHostName = "192.168.0.20";
	
	public static void createScript(String script) throws Exception {
		
		
		
		
		
		BufferedReader br = new BufferedReader(new FileReader("/home/ecode/evolve/mini1.txt"));
		String sCurrentLine;
		mini1 = br.readLine();
		while ((sCurrentLine = br.readLine()) != null) {
						
				mini1 = mini1+ "\n"+sCurrentLine;
			
		}
			
	br.close();
	BufferedReader brr = new BufferedReader(new FileReader("/home/ecode/evolve/mini2.txt"));
	String ssCurrentLine;
	mini2 = brr.readLine();
	while ((ssCurrentLine = brr.readLine()) != null) {
					
			mini2 = mini2+ "\n"+ssCurrentLine;
		
	}
	brr.close();
	String output = mini1+"\n"+script+"\n"+mini2;
		
		String response = createPython(output);
		System.out.println(response);
		String network_creation = createNewNetwork();
		System.out.println(network_creation);

	}
	
	private static String createNewNetwork() throws Exception {
		
		JSch jsch=new JSch();
		 String remoteHostUserName = "openflow";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "openflow";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();
ChannelExec channel=(ChannelExec) session.openChannel("exec");
		 //  BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
		   session.sendKeepAliveMsg();
		   channel.setCommand("sudo mn -c");
          
          channel.connect();
         Thread.sleep(6000);
         channel.disconnect();
         channel.setCommand("sudo mn --custom ~/mininet/custom/topo-2sw-2host.py --topo mytopo");
          channel.connect();
          Thread.sleep(10000);
		  
		  
		
		   return "mininet created";
		
		
	}

	public static String createPython(String output) throws JSchException, SftpException, IOException
	{
		JSch jsch=new JSch();
		 String remoteHostUserName = "openflow";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "openflow";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();
	
		   Channel channel = session.openChannel("sftp");
	        channel.connect();
	 
	        ChannelSftp sftp = (ChannelSftp) channel;
	        sftp.cd("mininet/custom");
	        @SuppressWarnings("unchecked")
			Vector<ChannelSftp.LsEntry> files = sftp.ls("*");
	      
	        ChannelSftp.LsEntry file = files.elementAt(0);
	        ChannelSftp.LsEntry file1 = files.elementAt(1);
	        ChannelSftp.LsEntry file2 = files.elementAt(2);
	        
	        
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sftp.put(file.getFilename())));
	        
	           	      char[] buf_f = output.toCharArray();
		             bw.write(buf_f,0,buf_f.length);
		             
	         bw.close();
		          	           	           
	        BufferedReader bis = new BufferedReader(new InputStreamReader(sftp.get(file.getFilename())));
          String line = null;
          BufferedWriter bos = new BufferedWriter(new OutputStreamWriter(sftp.put(file1.getFilename())));
          while ((line = bis.readLine()) != null) {
          	      char[] buf = line.toCharArray();
	             bos.write(buf,0,buf.length);
	             bos.newLine();
                  
          } 
          bos.close(); 
                bis.close();
	          	      channel.disconnect();
	        session.disconnect();
	        
	        String response = "Successfully created python script";
	        
	        return response;		
			}
	

}
