package connections;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class cpuUtil {
	private static final String RemoteHostName = "192.168.0.10";
	public static Float getCpuUtil() throws JSchException, IOException {
	
		JSch jsch=new JSch();
		 String remoteHostUserName = "odp";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "passwd";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();
ChannelExec channel=(ChannelExec) session.openChannel("exec");
		   BufferedReader in=new BufferedReader(new InputStreamReader(channel.getInputStream()));
		    
		    
 channel.setCommand("top -b -n1 | grep 'Cpu(s)' | awk '{print $2 + $4}'");
		   channel.connect();

		   String msg1=null;
		   msg1=in.readLine();		  
        
		   channel.disconnect();
		     		   
		   session.disconnect();
		   
		   Float cutil = Float.parseFloat(msg1);
		
		return cutil;
	}

}
