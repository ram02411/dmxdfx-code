package bean;

public class Rulebean {
	
	private String ruleid;
	private String rswitchnum;
	private String rinter;
	private String rsourceip;
	private String rdestip;
	private String rprotocol;
	private String rport;
	private String raction;
	private String rdatetime;
	private String zonesrc;
	private String zonedest;
	private String priority;
	private String zoptions;
	
	public String getZonesrc() {
		return zonesrc;
	}
	public void setZonesrc(String zonesrc) {
		this.zonesrc = zonesrc;
	}
	public String getZonedest() {
		return zonedest;
	}
	public void setZonedest(String zonedest) {
		this.zonedest = zonedest;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getZoptions() {
		return zoptions;
	}
	public void setZoptions(String zoptions) {
		this.zoptions = zoptions;
	}
	public String getRuleid() {
		return ruleid;
	}
	public void setRuleid(String ruleid) {
		this.ruleid = ruleid;
	}
	public String getRswitchnum() {
		return rswitchnum;
	}
	public void setRswitchnum(String rswitchnum) {
		this.rswitchnum = rswitchnum;
	}
	public String getRinter() {
		return rinter;
	}
	public void setRinter(String rinter) {
		this.rinter = rinter;
	}
	public String getRsourceip() {
		return rsourceip;
	}
	public void setRsourceip(String rsourceip) {
		this.rsourceip = rsourceip;
	}
	public String getRdestip() {
		return rdestip;
	}
	public void setRdestip(String rdestip) {
		this.rdestip = rdestip;
	}
	public String getRprotocol() {
		return rprotocol;
	}
	public void setRprotocol(String rprotocol) {
		this.rprotocol = rprotocol;
	}
	public String getRport() {
		return rport;
	}
	public void setRport(String rport) {
		this.rport = rport;
	}
	public String getRaction() {
		return raction;
	}
	public void setRaction(String raction) {
		this.raction = raction;
	}
	public String getRdatetime() {
		return rdatetime;
	}
	public void setRdatetime(String rdatetime) {
		this.rdatetime = rdatetime;
	}
	
}

