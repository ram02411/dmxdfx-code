package bean;

public class Policyschedulebean {
	
	String policyschid;
	String switchsch;
	String intersch;
	String sourcipsch;
	String destinipsch;
	String protosch;
	String portsch;
	String actionsch;
	String ruledatesch;
	String priority;
	public static String datetimeschhp;
	
		
	
	public static String getDatetimeschhp() {
		return datetimeschhp;
	}
	public static void setDatetimeschhp(String datetimeschhp) {
		Policyschedulebean.datetimeschhp = datetimeschhp;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getPolicyschid() {
		return policyschid;
	}
	public void setPolicyschid(String policyschid) {
		this.policyschid = policyschid;
	}
	public String getSwitchsch() {
		return switchsch;
	}
	public void setSwitchsch(String switchsch) {
		this.switchsch = switchsch;
	}
	public String getIntersch() {
		return intersch;
	}
	public void setIntersch(String intersch) {
		this.intersch = intersch;
	}
	public String getSourcipsch() {
		return sourcipsch;
	}
	public void setSourcipsch(String sourcipsch) {
		this.sourcipsch = sourcipsch;
	}
	public String getDestinipsch() {
		return destinipsch;
	}
	public void setDestinipsch(String destinipsch) {
		this.destinipsch = destinipsch;
	}
	public String getProtosch() {
		return protosch;
	}
	public void setProtosch(String protosch) {
		this.protosch = protosch;
	}
	public String getPortsch() {
		return portsch;
	}
	public void setPortsch(String portsch) {
		this.portsch = portsch;
	}
	public String getActionsch() {
		return actionsch;
	}
	public void setActionsch(String actionsch) {
		this.actionsch = actionsch;
	}
	public String getRuledatesch() {
		return ruledatesch;
	}
	public void setRuledatesch(String ruledatesch) {
		this.ruledatesch = ruledatesch;
	}
	
	
}
