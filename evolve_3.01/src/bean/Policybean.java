package bean;

public class Policybean implements java.io.Serializable{
	
	private String policyid;
	private String switchnum;
	private String inter;
	private String sourceip;
	private String destip;
	private String protocol;
	private String port;
	private String action;
	private String validation;
	private String datetime;
	private String priority;
	
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getValidation() {
		return validation;
	}
	public void setValidation(String validation) {
		this.validation = validation;
	}
	
	
	public String getPolicyid() {
		return policyid;
	}
	public void setPolicyid(String policyid) {
		this.policyid = policyid;
	}
	public String getSwitchnum() {
		return switchnum;
	}
	public void setSwitchnum(String switchnum) {
		this.switchnum = switchnum;
	}
	public String getInter() {
		return inter;
	}
	public void setInter(String inter) {
		this.inter = inter;
	}
	public String getSourceip() {
		return sourceip;
	}
	public void setSourceip(String sourceip) {
		this.sourceip = sourceip;
	}
	public String getDestip() {
		return destip;
	}
	public void setDestip(String destip) {
		this.destip = destip;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
}
