package bean;

public class fetchtopobean {
	private String switchorhost;
	private String name;
	private String xcordinate;
	private String ycordinate;
	private String switchtype;

	
	public String getSwitchtype() {
		return switchtype;
	}
	public void setSwitchtype(String switchtype) {
		this.switchtype = switchtype;
	}
	public String getSwitchorhost() {
		return switchorhost;
	}
	public void setSwitchorhost(String switchorhost) {
		this.switchorhost = switchorhost;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getXcordinate() {
		return xcordinate;
	}
	public void setXcordinate(String xcordinate) {
		this.xcordinate = xcordinate;
	}
	public String getYcordinate() {
		return ycordinate;
	}
	public void setYcordinate(String ycordinate) {
		this.ycordinate = ycordinate;
	}
	
}
