package bean;

public class Zonepolicybean {
	
	private String zoneid;
	private String zswitch;
	private String zone_src;
	private String zone_dest;
	private String zsrcip;
	private String zdestip;
	private String zpriority;
	private String zprotocol;
	private String zport;
	private String zaction;
	private String zruledate;
	public String getZoneid() {
		return zoneid;
	}
	public void setZoneid(String zoneid) {
		this.zoneid = zoneid;
	}
	public String getZswitch() {
		return zswitch;
	}
	public void setZswitch(String zswitch) {
		this.zswitch = zswitch;
	}
	public String getZone_src() {
		return zone_src;
	}
	public void setZone_src(String zone_src) {
		this.zone_src = zone_src;
	}
	public String getZone_dest() {
		return zone_dest;
	}
	public void setZone_dest(String zone_dest) {
		this.zone_dest = zone_dest;
	}
	public String getZsrcip() {
		return zsrcip;
	}
	public void setZsrcip(String zsrcip) {
		this.zsrcip = zsrcip;
	}
	public String getZdestip() {
		return zdestip;
	}
	public void setZdestip(String zdestip) {
		this.zdestip = zdestip;
	}
	public String getZpriority() {
		return zpriority;
	}
	public void setZpriority(String zpriority) {
		this.zpriority = zpriority;
	}
	public String getZprotocol() {
		return zprotocol;
	}
	public void setZprotocol(String zprotocol) {
		this.zprotocol = zprotocol;
	}
	public String getZport() {
		return zport;
	}
	public void setZport(String zport) {
		this.zport = zport;
	}
	public String getZaction() {
		return zaction;
	}
	public void setZaction(String zaction) {
		this.zaction = zaction;
	}
	public String getZruledate() {
		return zruledate;
	}
	public void setZruledate(String zruledate) {
		this.zruledate = zruledate;
	}
	
	

}
