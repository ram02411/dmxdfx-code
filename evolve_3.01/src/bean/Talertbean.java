package bean;

public class Talertbean {
	
	private String alertid;
	private String alertype;
	private String alertdatetime;
	private String alertdate;
	private String alertime;
	private String alertprio;
	private String alertclassi;
	private String alertproto;
	private String alertmsg;
	private String alertsrcip;
	private String alertsrcport;
	private String alertdestip;
	private String alertdestport;
	public String getAlertid() {
		return alertid;
	}
	public void setAlertid(String alertid) {
		this.alertid = alertid;
	}
	public String getAlertype() {
		return alertype;
	}
	public void setAlertype(String alertype) {
		this.alertype = alertype;
	}
	public String getAlertdatetime() {
		return alertdatetime;
	}
	public void setAlertdatetime(String alertdatetime) {
		this.alertdatetime = alertdatetime;
	}
	public String getAlertdate() {
		return alertdate;
	}
	public void setAlertdate(String alertdate) {
		this.alertdate = alertdate;
	}
	public String getAlertime() {
		return alertime;
	}
	public void setAlertime(String alertime) {
		this.alertime = alertime;
	}
	public String getAlertprio() {
		return alertprio;
	}
	public void setAlertprio(String alertprio) {
		this.alertprio = alertprio;
	}
	public String getAlertclassi() {
		return alertclassi;
	}
	public void setAlertclassi(String alertclassi) {
		this.alertclassi = alertclassi;
	}
	public String getAlertproto() {
		return alertproto;
	}
	public void setAlertproto(String alertproto) {
		this.alertproto = alertproto;
	}
	public String getAlertmsg() {
		return alertmsg;
	}
	public void setAlertmsg(String alertmsg) {
		this.alertmsg = alertmsg;
	}
	public String getAlertsrcip() {
		return alertsrcip;
	}
	public void setAlertsrcip(String alertsrcip) {
		this.alertsrcip = alertsrcip;
	}
	public String getAlertsrcport() {
		return alertsrcport;
	}
	public void setAlertsrcport(String alertsrcport) {
		this.alertsrcport = alertsrcport;
	}
	public String getAlertdestip() {
		return alertdestip;
	}
	public void setAlertdestip(String alertdestip) {
		this.alertdestip = alertdestip;
	}
	public String getAlertdestport() {
		return alertdestport;
	}
	public void setAlertdestport(String alertdestport) {
		this.alertdestport = alertdestport;
	}
	
	
}
