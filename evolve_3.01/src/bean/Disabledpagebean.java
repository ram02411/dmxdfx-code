package bean;

public class Disabledpagebean {

	private static int administratorli;
	private static int administrator;
	private static int adminrole;
	private static int dashboardli;
	private static int dashboard;
	private static int controlleralerts;
	private static int design;
	private static int canvas;
	private static int fetchcanvas;
	private static int viewer;
	private static int viewersnt;
	private static int tags;
	private static int control;
	private static int policybase;
	private static int rulebase;
	private static int policybasesnt;
	private static int rulebasesnt;
	private static int simulate;
	private static int securityrulebase;
	private static int flowrector;
	public static int getAdministratorli() {
		return administratorli;
	}
	public static void setAdministratorli(int administratorli) {
		Disabledpagebean.administratorli = administratorli;
	}
	public static int getAdministrator() {
		return administrator;
	}
	public static void setAdministrator(int administrator) {
		Disabledpagebean.administrator = administrator;
	}
	public static int getAdminrole() {
		return adminrole;
	}
	public static void setAdminrole(int adminrole) {
		Disabledpagebean.adminrole = adminrole;
	}
	public static int getDashboardli() {
		return dashboardli;
	}
	public static void setDashboardli(int dashboardli) {
		Disabledpagebean.dashboardli = dashboardli;
	}
	public static int getDashboard() {
		return dashboard;
	}
	public static void setDashboard(int dashboard) {
		Disabledpagebean.dashboard = dashboard;
	}
	public static int getControlleralerts() {
		return controlleralerts;
	}
	public static void setControlleralerts(int controlleralerts) {
		Disabledpagebean.controlleralerts = controlleralerts;
	}
	public static int getDesign() {
		return design;
	}
	public static void setDesign(int design) {
		Disabledpagebean.design = design;
	}
	public static int getCanvas() {
		return canvas;
	}
	public static void setCanvas(int canvas) {
		Disabledpagebean.canvas = canvas;
	}
	public static int getFetchcanvas() {
		return fetchcanvas;
	}
	public static void setFetchcanvas(int fetchcanvas) {
		Disabledpagebean.fetchcanvas = fetchcanvas;
	}
	public static int getViewer() {
		return viewer;
	}
	public static void setViewer(int viewer) {
		Disabledpagebean.viewer = viewer;
	}
	public static int getViewersnt() {
		return viewersnt;
	}
	public static void setViewersnt(int viewersnt) {
		Disabledpagebean.viewersnt = viewersnt;
	}
	
	
	public static int getTags() {
		return tags;
	}
	public static void setTags(int tags) {
		Disabledpagebean.tags = tags;
	}
	public static int getControl() {
		return control;
	}
	public static void setControl(int control) {
		Disabledpagebean.control = control;
	}
	public static int getPolicybase() {
		return policybase;
	}
	public static void setPolicybase(int policybase) {
		Disabledpagebean.policybase = policybase;
	}
	public static int getRulebase() {
		return rulebase;
	}
	public static void setRulebase(int rulebase) {
		Disabledpagebean.rulebase = rulebase;
	}
	public static int getPolicybasesnt() {
		return policybasesnt;
	}
	public static void setPolicybasesnt(int policybasesnt) {
		Disabledpagebean.policybasesnt = policybasesnt;
	}
	public static int getRulebasesnt() {
		return rulebasesnt;
	}
	public static void setRulebasesnt(int rulebasesnt) {
		Disabledpagebean.rulebasesnt = rulebasesnt;
	}
	public static int getSimulate() {
		return simulate;
	}
	
	
	public static int getSecurityrulebase() {
		return securityrulebase;
	}
	public static void setSecurityrulebase(int securityrulebase) {
		Disabledpagebean.securityrulebase = securityrulebase;
	}
	public static void setSimulate(int simulate) {
		Disabledpagebean.simulate = simulate;
	}
	public static int getFlowrector() {
		return flowrector;
	}
	public static void setFlowrector(int flowrector) {
		Disabledpagebean.flowrector = flowrector;
	}
	
	
	
	
	
}
