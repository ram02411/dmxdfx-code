package bean;

public class Securityrule {
	private String name;
	private String type;
	private String desc;
	private String tags;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	@Override
	public String toString() {
		return "Securityrule [name=" + name + ", type=" + type + ", desc=" + desc + ", tags=" + tags + "]";
	}
	

}
