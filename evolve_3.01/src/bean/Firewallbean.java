package bean;

public class Firewallbean {
	private String zruleid;
	 private String zrswitchnum;
	 private String zrinter;
	 private String zrsourceip;
	 private String zrdestip;
	 private String zrprotocol;
	 private String zrport;
	 private String zraction;
	 private String rdatetime;
	 private String zonesrc;
	 private String zonedest;
	 private String zpriority;
	 private String zoptions;
	public String getZruleid() {
		return zruleid;
	}
	public void setZruleid(String zruleid) {
		this.zruleid = zruleid;
	}
	public String getZrswitchnum() {
		return zrswitchnum;
	}
	public void setZrswitchnum(String zrswitchnum) {
		this.zrswitchnum = zrswitchnum;
	}
	public String getZrinter() {
		return zrinter;
	}
	public void setZrinter(String zrinter) {
		this.zrinter = zrinter;
	}
	public String getZrsourceip() {
		return zrsourceip;
	}
	public void setZrsourceip(String zrsourceip) {
		this.zrsourceip = zrsourceip;
	}
	public String getZrdestip() {
		return zrdestip;
	}
	public void setZrdestip(String zrdestip) {
		this.zrdestip = zrdestip;
	}
	public String getZrprotocol() {
		return zrprotocol;
	}
	public void setZrprotocol(String zrprotocol) {
		this.zrprotocol = zrprotocol;
	}
	public String getZrport() {
		return zrport;
	}
	public void setZrport(String zrport) {
		this.zrport = zrport;
	}
	public String getZraction() {
		return zraction;
	}
	public void setZraction(String zraction) {
		this.zraction = zraction;
	}
	public String getRdatetime() {
		return rdatetime;
	}
	public void setRdatetime(String rdatetime) {
		this.rdatetime = rdatetime;
	}
	public String getZonesrc() {
		return zonesrc;
	}
	public void setZonesrc(String zonesrc) {
		this.zonesrc = zonesrc;
	}
	public String getZonedest() {
		return zonedest;
	}
	public void setZonedest(String zonedest) {
		this.zonedest = zonedest;
	}
	public String getZpriority() {
		return zpriority;
	}
	public void setZpriority(String zpriority) {
		this.zpriority = zpriority;
	}
	public String getZoptions() {
		return zoptions;
	}
	public void setZoptions(String zoptions) {
		this.zoptions = zoptions;
	}
	 
	 
}
