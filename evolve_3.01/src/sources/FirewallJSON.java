package sources;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class FirewallJSON {

	static String IP = "192.168.0.20";
	static JSONObject obj;
	static JSONArray json;
	static Future<Object> future;

	public static boolean isEnabled()
			throws JSONException, IOException {

		future = Deserializer.readJsonObjectFromURL("http://" + IP
				+ ":8080/wm/firewall/module/status/json");
		try {
			obj = (JSONObject) future.get(5, TimeUnit.SECONDS);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ExecutionException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (TimeoutException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (obj != null) {
			if(obj.getString("result").equals("firewall enabled"))
				return true;
		}
		return false;
	}
	
	public static String enable(boolean enable) throws JSONException{
		
		if(enable){
			future = Deserializer.readJsonObjectFromURL("http://" + IP
					+ ":8080/wm/firewall/module/enable/json");
			
			try {
				obj = (JSONObject) future.get(5, TimeUnit.SECONDS);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (TimeoutException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (obj != null) {
				if(obj.getString("status").equals("success"))
					return obj.getString("status");
				else
					return null;
			}
		}
		else{
			future = Deserializer.readJsonObjectFromURL("http://" + IP
					+ ":8080/wm/firewall/module/disable/json");
			
			try {
				obj = (JSONObject) future.get(5, TimeUnit.SECONDS);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (TimeoutException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (obj != null) {
				if(obj.getString("status").equals("success"))
					return obj.getString("status");
				else
					return null;
			}
		}
		return null;
	}
}
