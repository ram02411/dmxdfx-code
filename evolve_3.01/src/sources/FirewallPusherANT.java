package sources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;



public class FirewallPusherANT {
    
    static String IP = "192.168.0.20";
    static String jsonResponse;
    
    public static String push(Rule rule) throws IOException, JSONException{
        
        jsonResponse = "";
        URL url = new URL("http://" + IP
                + ":8080/wm/firewall/rules/json");
        
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
        wr.write(rule.serialize());
        wr.flush();

        // Get the response
        BufferedReader rd = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            jsonResponse = jsonResponse.concat(line);
        }
        wr.close();
        rd.close();

        // Wrap the response
        JSONObject json = new JSONObject(jsonResponse);
        
        // Make sure the firewall pusher throws no errors
        if (json.getString("status").equals("Rule added") ) {
           
                    return "Rule successfully pushed!";
            
        } else {
            return json.getString("status");
        }
    }
    
    public static String remove(Rule rule) throws IOException, JSONException{
        
        jsonResponse = "";

        URL url = new URL("http://" + IP
                + ":8080/wm/firewall/rules/json");
        HttpURLConnection connection = null;
        connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        // We have to override the post method so we can send data
        connection.setRequestProperty("X-HTTP-Method-Override", "DELETE");
        connection.setDoOutput(true);

        // Send request
        OutputStreamWriter wr = new OutputStreamWriter(
                connection.getOutputStream());
        wr.write(rule.deleteString());
        wr.flush();

        // Get Response
        BufferedReader rd = new BufferedReader(new InputStreamReader(
                connection.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            jsonResponse = jsonResponse.concat(line);
        }
        wr.close();
        rd.close();

        JSONObject json = new JSONObject(jsonResponse);
        // Return result string from key "status"
        return json.getString("status");
    }
    
    public static void removeAll(){
        List<Rule> rules = FloodlightProvider.getRules();
        
        for(Rule rule : rules){
            try {
                remove(rule);
            } catch (IOException  e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}

     
  
   
        
    
