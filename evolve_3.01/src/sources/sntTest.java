package sources;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Properties;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
public class sntTest {
	private static final String RemoteHostName = "192.168.56.21";
	
	public static String createPython(String output) throws JSchException, SftpException, IOException {
		JSch jsch=new JSch();
		 String remoteHostUserName = "openflow";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "openflow";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();
	
		   Channel channel = session.openChannel("sftp");
	        channel.connect();
	 
	        ChannelSftp sftp = (ChannelSftp) channel;
	        sftp.cd("mininet/custom");
	        @SuppressWarnings("unchecked")
			Vector<ChannelSftp.LsEntry> files = sftp.ls("*");
	       // System.out.printf("Found %d files in dir %s%n", files.size(), "mininet/custom");
	 
	     /*   for (ChannelSftp.LsEntry file : files) {
	            if (file.getAttrs().isDir()) {
	                continue;
	            }*/
	        ChannelSftp.LsEntry file = files.elementAt(0);
	        ChannelSftp.LsEntry file1 = files.elementAt(1);
	        ChannelSftp.LsEntry file2 = files.elementAt(2);
	        
	         //   System.out.printf("Reading file : %s%n", file.getFilename());
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sftp.put(file.getFilename())));
	        
	           	      char[] buf_f = output.toCharArray();
		             bw.write(buf_f,0,buf_f.length);
		             
	         bw.close();
		          
	           
	           
	        BufferedReader bis = new BufferedReader(new InputStreamReader(sftp.get(file.getFilename())));
           String line = null;
           BufferedWriter bos = new BufferedWriter(new OutputStreamWriter(sftp.put(file1.getFilename())));
           while ((line = bis.readLine()) != null) {
           	      char[] buf = line.toCharArray();
	             bos.write(buf,0,buf.length);
	             bos.newLine();
         
	          
           } 
           bos.close(); 
          
           bis.close();
	           /* BufferedWriter bos = new BufferedWriter(new OutputStreamWriter(sftp.put(file.getFilename())));
	            String s= "from mininet.topo import Topo";
	            char[] cbuf = s.toCharArray();
	            int len = cbuf.length;
	            
	            bos.write(cbuf, 0, len);
	            bos.close(); */
	 
	        channel.disconnect();
	        session.disconnect();
	        
	        String response = "Successfully created python script";
	        
	        return response;

	}

}
