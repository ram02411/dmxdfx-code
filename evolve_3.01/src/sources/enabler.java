package sources;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class enabler {

	public static void enableFirewall() throws IOException, JSONException {
		String jsonResponse = "";
		String IP = "192.168.0.20";
		
		URL url = new URL("http://" + IP
                + ":8080/wm/firewall/module/enable/json");
        
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        BufferedReader rd = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            jsonResponse = jsonResponse.concat(line);
        }
        rd.close();
        List<String> res = new ArrayList<String>();
        JSONObject json = new JSONObject(jsonResponse);
        res.add(json.getString("status"));
        res.add(json.getString("details"));
        
	}

}
