package sources;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


public class status {

	
	public static String getStatus() throws IOException, JSONException {
		String jsonResponse = "";
		String IP = "192.168.0.20";
		
		URL url = new URL("http://" + IP
                + ":8080/wm/firewall/module/status/json ");
        
        URLConnection conn = url.openConnection();
        conn.setDoOutput(true);
        BufferedReader rd = new BufferedReader(new InputStreamReader(
                conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            jsonResponse = jsonResponse.concat(line);
        }
        rd.close();
        JSONObject json = new JSONObject(jsonResponse);
        String status = json.getString("result");
       return status;


	}

}
