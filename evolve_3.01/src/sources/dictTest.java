package sources;
import java.io.*;
import java.util.*;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

public class dictTest {
private static String mini1;
private static String mini2;
private static String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
	
	public static void startMini() throws JSONException, IOException, JSchException, SftpException {
List<DeviceSummary> sum = null;
HashMap<String, String> sw = new HashMap<String, String>();
HashMap<String, String> nm = new HashMap<String, String>();

		
		DevicesJSON info = new DevicesJSON();
		
		sum = info.getDeviceSummaries();
		ArrayList<String> hosts = new ArrayList();
		ArrayList<String> swits = new ArrayList();
			
		System.out.println("MacAddress"+"  "+"Switchport"+"  "+"Attached switch");
		
for(int i=0;i<sum.size();i++){
	if(sum.get(i).getAttachedSwitch()!=null){
		System.out.println(sum.get(i).getMacAddress()+"  "+sum.get(i).getSwitchPort()+"  "+sum.get(i).getAttachedSwitch());
		hosts.add(sum.get(i).getMacAddress());
	swits.add(sum.get(i).getAttachedSwitch());	
	
		sw.put(sum.get(i).getMacAddress(), sum.get(i).getAttachedSwitch());
		
	
	}}
ArrayList<String> nonDupList = new ArrayList<String>();

Iterator<String> dupIter = swits.iterator();
while(dupIter.hasNext())
{
String dupWord = dupIter.next();
if(nonDupList.contains(dupWord))
{
    dupIter.remove();
}else
{
    nonDupList.add(dupWord);
}
}

for(int i=0;i<nonDupList.size();i++)
{
	nm.put(nonDupList.get(i), "Switch"+str.charAt(i));
}

System.out.println("size of nonDup NM: "+" "+nonDupList.size());
		
	List<SwitchLink> slink = null;
	SwitchLinkJSON switches = new SwitchLinkJSON();
	
	slink = switches.getSwitchLinks();
	
	ArrayList<String> switches_pys = new ArrayList();
	ArrayList<String> switches_pyd = new ArrayList();
		
	System.out.println("Source switch"+"           "+"sp"+"  "+"dp"+"    "+"Destination Switch");
	for(int j=0;j<slink.size();j++)
	{
		System.out.println(slink.get(j).getsrcswitch()+"  "+slink.get(j).getsrcport()+"  "+slink.get(j).getdstport()+"  "+slink.get(j).getdstswitch());
		switches_pys.add(slink.get(j).getsrcswitch());
		switches_pyd.add(slink.get(j).getdstswitch());
	}
	String created = createNW(sw,nm,hosts,swits,switches_pys,switches_pyd);
	BufferedReader br = new BufferedReader(new FileReader("/home/ecode/evolve/mini1.txt"));
	String sCurrentLine;
	mini1 = br.readLine();
	while ((sCurrentLine = br.readLine()) != null) {
					
			mini1 = mini1+ "\n"+sCurrentLine;
		
	}
		
br.close();
BufferedReader brr = new BufferedReader(new FileReader("/home/ecode/evolve/mini2.txt"));
String ssCurrentLine;
mini2 = brr.readLine();
while ((ssCurrentLine = brr.readLine()) != null) {
				
		mini2 = mini2+ "\n"+ssCurrentLine;
	
}
brr.close();
String output = mini1+"\n"+created+"\n"+mini2;

String response = sntTest.createPython(output);
System.out.println(response);
String network_creation = sources.createNetwork.createNewNetwork();
System.out.println(network_creation);
	
	
	}
	
	public static String createNW(HashMap<String, String> sw,HashMap<String, String> nm,ArrayList<String>hosts,ArrayList<String>swits,ArrayList<String>switches_pys,ArrayList<String>switches_pyd)
	{
		String out ="";
		int k=0;
		ArrayList<String> nonDupList = new ArrayList<String>();

		Iterator<String> dupIter = switches_pys.iterator();
		while(dupIter.hasNext())
		{
		String dupWord = dupIter.next();
		if(nonDupList.contains(dupWord))
		{
		    dupIter.remove();
		}else
		{
		    nonDupList.add(dupWord);
		}
		}
		
		ArrayList<String> nonDupswits = new ArrayList<String>();

		Iterator<String> dupIter1 = swits.iterator();
		while(dupIter1.hasNext())
		{
		String dupWord = dupIter1.next();
		if(nonDupswits.contains(dupWord))
		{
		    dupIter1.remove();
		}else
		{
		    nonDupswits.add(dupWord);
		}
		}

		ArrayList<String>diff = new ArrayList<String>(nonDupList);
		diff.removeAll(nonDupswits);
		
		int z = nm.size();
		for(int j=0;j<diff.size();j++)
		{
			nm.put(diff.get(j), "Switch"+str.charAt(z));
			z++;
		}
		
		for(int i=0;i<sw.size();i++)
		{
			out = out+"  "+"Host"+str.charAt(i)+" "+"="+i+"\n";
			k++;
			
		}
		
		List<String> nmk = new ArrayList<String>(); 
		
        Iterator itr =  nm.keySet().iterator();
        while(itr.hasNext())
        {
              String key = (String) itr.next();
              nmk.add(key);

         }
		for(int i=0;i<nmk.size();i++)
		{
			out = out+"  "+"Switch"+str.charAt(i)+"="+k+"\n";
			k++;
			
		}
		for(int i=0;i<sw.size();i++)
		{
		
			out = out+"  "+"self.add_node("+"Host"+str.charAt(i)+",Node(is_switch=False))"+"\n";
		
		}
		for(int i=0;i<nmk.size();i++)
		{
			
			out = out+"  "+"self.add_node("+"Switch"+str.charAt(i)+",Node(is_switch=True))"+"\n";
		}
		
		
		for(int j=0;j<sw.size();j++)
		{
			out = out+"  "+"self.add_edge("+"Host"+str.charAt(j)+","+ nm.get(sw.get(hosts.get(j)))+")"+"\n";
		}
		
		
		
		
		
	for(int j=0;j<switches_pys.size();j++)
		{
			out = out+"  "+"self.add_edge("+nm.get(switches_pys.get(j))+","+nm.get(switches_pyd.get(j))+")"+"\n";
		} 
		
		 
		return out;
	
		
		
	
		
	}
}
	