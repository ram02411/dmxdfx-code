package evolve.hpc;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SwitchLinkJSON {

	static String IP = "192.168.0.11";
	static JSONObject obj;

	public static List<SwitchLink> getSwitchLinks() throws JSONException {

		List<SwitchLink> switchlinks = new ArrayList<SwitchLink>();

		// Get the string IDs of all the switches and create switch summary
		// objects for each one
		try {
			Future<Object> switches = Deserializer.readJsonArrayFromURL("http://" + IP
					+ ":8080/wm/topology/links/json");
			JSONArray json = (JSONArray) switches.get(5, TimeUnit.SECONDS);
			for (int i = 0; i < json.length(); i++) {
				obj = json.getJSONObject(i);
				SwitchLink temp = new SwitchLink();
				temp.setsrcswitch(obj.getString("src-switch"));
				temp.setsrcport(Integer.toString(obj.getInt("src-port")));
				temp.setdstswitch(obj.getString("dst-switch"));
				temp.setdstport(Integer.toString(obj.getInt("dst-port")));
				temp.setdstportstate(Integer.toString(obj.getInt("dst-port-state")));
				temp.setsrcportstate(Integer.toString(obj.getInt("src-port-state")));
				temp.settype(obj.getString("type"));
				//if (!obj.getJSONArray("ipv4").isNull(0))
				switchlinks.add(temp);
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return switchlinks;
	}
}
