package evolve.hpc;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.TrustManager;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import canvas.util.JSONException;

import conversions.Datetimehp;

import connections.*;
import dto.IPSdto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import logger.logger;
public class alertPicker {

	static String IP = IPSdto.getControllerIP();
	public static List<String> userid;
	public static List<String> topic;
	public static List<String> severity;
	public static List<String> time;
	public static List<String> description;
	static logger logme = logger.getInstance() ;
	public static List<List<String>> getAlerts() throws NoSuchAlgorithmException, KeyManagementException, evolve.hpc.JSONException  {
		Connection conn=null;
		Statement st1=null, stmax=null, stcheck=null;
		ResultSet rmax=null;
		ResultSet rcheck=null;
		try{
			 System.out.println("Process enter excel");
	 			Class.forName("com.mysql.jdbc.Driver");
	 			conn = DriverManager.getConnection(
	 					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
	 			System.out.println("entered");
	 			st1=conn.createStatement();
	 			stmax=conn.createStatement();
	 			stcheck=conn.createStatement();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println("entering");
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
               public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                  return null;
               }
               public void checkClientTrusted(X509Certificate[] certs, String authType) {
               }
              public void checkServerTrusted(X509Certificate[] certs, String authType) {
               }
           }
       };

      
      SSLContext sc = SSLContext.getInstance("SSL");
      sc.init(null, trustAllCerts, new java.security.SecureRandom());
       HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      
       HostnameVerifier allHostsValid = new HostnameVerifier() {
           public boolean verify(String hostname, SSLSession session) {
               return true;
          }
       };
       
	     String token = null;
		try {
			token = hpAuth.getAuth();
		} catch (IOException | JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
     
       HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

       URL url = null;
	try {
		url = new URL("https://"+IP+":8443/sdn/v2.0/alerts");
	} catch (MalformedURLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
       HttpURLConnection con = null;
	try {
		System.out.println("opening connection");
		con = (HttpURLConnection) url.openConnection();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
    try {
		con.setRequestMethod("GET");
	} catch (ProtocolException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
 con.setRequestProperty ("X-Auth-Token", token);
 
      InputStreamReader reader = null;
	try {
		System.out.println("opening input stream");
		reader = new InputStreamReader(con.getInputStream());
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
    String res="";
    while (true) {
           int ch = 0;
		try {
			ch = reader.read();
			//System.out.print((char)ch);
			//System.out.println("got alert");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if(ch!=-1){
          res=res+(char)ch;}
        else{break;}
       }
    
   userid = new ArrayList<String>();
	  topic= new ArrayList<String>();
	  severity= new ArrayList<String>();
	 time= new ArrayList<String>();
	  description= new ArrayList<String>();

	    JSONObject obj1 = null;
		try {
			obj1 = new JSONObject(res);
		} catch (evolve.hpc.JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	     JSONArray geodata = null;
		try {
			geodata = obj1.getJSONArray("alerts");
		} catch (evolve.hpc.JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	      int n = geodata.length();
	      List<List<String>> name = new ArrayList<List<String>>();
	      
	    for (int r = 0; r < n; r++) {
	     JSONObject person = geodata.getJSONObject(r);
	  /* userid.add(person.getString("uid"));
	   topic.add(person.getString("topic"));
	  time.add(person.getString("ts"));
	  severity.add(person.getString("sev"));
	   description.add(person.getString("desc"));*/
	     
	     List<String> names = new ArrayList<String>();
	     names.add(person.getString("ts"));
	    names.add(person.getString("topic"));
    	 names.add(person.getString("sev"));
    	 names.add(person.getString("org"));
    	 names.add(person.getString("desc"));
    	 name.add(names);
	    	 
    	 System.out.println("before connecting to db");
    	 try{
    		// System.out.println("Process enter excel");
 			/*Class.forName("com.mysql.jdbc.Driver");
 			Connection conn = DriverManager.getConnection(
 					"jdbc:mysql://localhost:3306/evolve", "root", "3volve123!");
 			System.out.println("entered");
 			ResultSet rmax=null;
 			Statement st1=conn.createStatement();
 			Statement stmax=conn.createStatement();*/
    		 String checkq="SELECT * FROM ALERTS;";
    		 rcheck=stcheck.executeQuery(checkq);
 			
 			String startdt=null;
 			SimpleDateFormat dtformat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 			Date ds=null;
 			System.out.println("checking for if-else for emoty database");
 			if(!(rcheck.next())){
 
 				
 	 			
 				System.out.println("checking in if");
 	 			System.out.println("catching values");
 	     	 String dbts =person.getString("ts");
 	     	 String date=dbts.substring(0, 10);
 	     	 String time=dbts.substring(11, 18);
 	     	 String finalts=date+" "+time;
 	     	 String dbtopic =person.getString("topic");
 	     	 String dbsev=person.getString("sev");
 	     	 String dborg=person.getString("org");
 	     	 String dbdesc=person.getString("desc");
 	     	 
 	     	 
 	     	 
 	     	System.out.println("checking tecking the time difference");
 	     	String SQL1 = String
 					.format("INSERT INTO ALERTS VALUES(default, '"
 							+ finalts + "', '" + dbsev + "', '" + dbtopic
 							+ "', '" + dborg + "', '" + dbdesc + "' )");
 			System.out.println("entered"+SQL1);
 			st1.executeUpdate(SQL1);
 			
 			System.out.println("entering into database");
 	     	  				
 			}
 			else{
 				System.out.println("checking in else");
 				String SQLmax="Select MAX(TIMESTAMP) from ALERTS;";
 	 			rmax=stmax.executeQuery(SQLmax);
 	 			System.out.println("got max time");
 			while(rmax.next()){
 				System.out.println("checking in else-while");
 				startdt=rmax.getObject(1).toString();
 				System.out.println(startdt);
 				ds=dtformat.parse(startdt);
 				Date de=null;
 	 			
 	 			
 	 			
 	 			System.out.println("catching values");
 	     	 String dbts =person.getString("ts");
 	     	 System.out.println("date from hpc "+dbts);
 	     	 String date=dbts.substring(0, 10);
 	     	 String time=dbts.substring(11, 18);
 	     	 String finalts=date+" "+time;
 	     	 String dbtopic =person.getString("topic");
 	     	 String dbsev=person.getString("sev");
 	     	 String dborg=person.getString("org");
 	     	 String dbdesc=person.getString("desc");
 	     	 
 	     	 de=dtformat.parse(finalts);
 	     	 Long diff=de.getTime()-ds.getTime();
 	     	System.out.println("checking tecking the time difference");
 	     	 if(diff>0){
 	     	// System.out.println("INSERT INTO ALERTS VALUES(default, '"
 					//	+ finalts + "', '" + dbsev + "', '" + dbtopic
 						//+ "', '" + dborg + "', '" + dbdesc + "', )");
 	     	String SQL1 = String
 					.format("INSERT INTO ALERTS VALUES(default, '"
 							+ finalts + "', '" + dbsev + "', '" + dbtopic
 							+ "', '" + dborg + "', '" + dbdesc + "' )");
 			System.out.println("entered"+SQL1);
 			st1.executeUpdate(SQL1);
 			
 			System.out.println("entering into database");
 	     	 }
 	     	 else{
 	     		System.out.println("Alert already present in database");
 	     	 }
 			}
 			}	
 			
 			
 			
     	 
     	System.out.println("process completed");
	    }
    	 catch (Exception e) {
    		 e.printStackTrace();
    		 logme.log.severe( e.getMessage());
    	 }
		
	}
	    try{
	    	st1.close();
	    	conn.close();
	    }
	    catch(Exception e){
	    	e.printStackTrace();
	    }
	    System.out.println("exitings");
		return name;

	}
}
