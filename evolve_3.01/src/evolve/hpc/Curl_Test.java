package evolve.hpc;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.TrustManager;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

import connections.hpAuth;
import dto.IPSdto;







import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Curl_Test {
	public static StringBuilder sb;
	public static List<Integer> stats;
	static String IP = IPSdto.getControllerIP();
	
	public static int[] getStats() throws IOException, NoSuchAlgorithmException, KeyManagementException, JSONException, canvas.util.JSONException{
	
	       TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	    	   	                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	    	  	                    return null;
	    	   	                }
	    	   	                public void checkClientTrusted(X509Certificate[] certs, String authType) {
	    	   	                }
	    	  	                public void checkServerTrusted(X509Certificate[] certs, String authType) {
	    	   	                }
	    	   	            }
	    	   	        };
	    	   	 
	    	  	        
	    	  	        SSLContext sc = SSLContext.getInstance("SSL");
	    	  	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	    	   	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	    	  
	    	  	        
	    	   	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	    	   	            public boolean verify(String hostname, SSLSession session) {
	    	   	                return true;
	    	  	            }
	    	   	        };
	    	   	 
	    	  	       String token =hpAuth.getAuth();
	    	   	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	    	  	 
	    	   	        URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/stats");
	    	   	     System.out.println("url is:"+url);
	    	   	        HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    	   	     con.setRequestMethod("GET");
	    	   	  con.setRequestProperty ("X-Auth-Token", token);
	    	   	  
	    	  	        InputStreamReader reader = new InputStreamReader(con.getInputStream());
	    	  	      String res="";
	    	  	      while (true) {
	    	   	            int ch = reader.read();
	    	 	           if(ch!=-1){
	    	   	           res=res+(char)ch;}
	    	 	           else{break;}
	    	   	        }
	    	  	     
	    	  	    int [] arr = new int[2];
	    	  	  JSONObject obj = new JSONObject(res);
	    	  	   JSONObject result = obj.getJSONObject("controller_stats");
	    	  	 int pin = result.getInt("pkt_in");
	    	  	int pout = result.getInt("pkt_out");
   	  	    	  	 
	    	  
	    	  	arr[0]= pin;
	    	  	arr[1]=pout;
	    	  	     	  	 
	    	  	
	    	  		    
	    	  	      return arr;
    }

}
    
