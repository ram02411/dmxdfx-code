package evolve.hpc;
import java.io.*;
import java.util.*;

public class stateTable {



	public static String getTopo() throws NullPointerException,
			JSONException, IOException {

		List<DeviceSummary> sum = null;

		sum = DevicesJSON.getDeviceSummaries();
		
		List<List<String>> hosts = new ArrayList<List<String>>();

		System.out.println("MacAddress" + "  " + "Switchport" + "  "
				+ "Attached switch");

		for (int i = 0; i < sum.size(); i++) {
			if (sum.get(i).getAttachedSwitch() != null) {
				System.out.println(sum.get(i).getMacAddress() + "  "
						+ sum.get(i).getSwitchPort() + "  "
						+ sum.get(i).getAttachedSwitch());
				List<String> host = new ArrayList<String>();
				host.add(sum.get(i).getMacAddress());
				host.add(sum.get(i).getAttachedSwitch());
				hosts.add(host);
			}
		}

		List<SwitchLink> slink = null;
		slink = SwitchLinkJSON.getSwitchLinks();
		List<List<String>> switch1 = new ArrayList<List<String>>();
		ArrayList<String> switches_py[] = new ArrayList[sum.size()];
		for (int m = 0; m < sum.size(); m++) {
			switches_py[m] = new ArrayList<String>();
		}

		System.out.println("Source switch" + "           " + "sp" + "  " + "dp"
				+ "    " + "Destination Switch");
		for (int j = 0; j < slink.size(); j++) {
			System.out.println(slink.get(j).getsrcswitch() + "  "
					+ slink.get(j).getsrcport() + "  "
					+ slink.get(j).getdstport() + "  "
					+ slink.get(j).getdstswitch());
			switches_py[j].add(slink.get(j).getsrcswitch());
			switches_py[j].add(slink.get(j).getdstswitch());
			switch1.add(switches_py[j]);
		}

		System.out.println(switch1.size());
		
		String out = "[" + "\n" + "{" + "\n" + " \"adjacencies\": [" ;
		System.out.println(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			out =  out +  "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + hosts.get(i).get(0) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ hosts.get(i).get(1) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#29220A\"" + "\n" + "}" + "\n" + "}";
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + hosts.get(i).get(1)+ "\"," + "\n" +
		        "\"name\": \"" + hosts.get(i).get(1)+"\"" + "\n" +
		      "}" + "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		
		for(int j=0;j<switch1.size();j++){
			out = out + "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + switch1.get(j).get(0) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ switch1.get(j).get(1) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#557EAA\"" + "\n" + "}" + "\n" + "}";
			
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + switch1.get(j).get(1)+ "\"," + "\n" +
		        "\"name\": \"" + switch1.get(j).get(1)+"\"" + "\n" +
		      "}"+ "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		out = out + "]," + "\n" +
        "\"data\": {" + "\n" + 
            "\"$color\": \"\"," + "\n" +
            "\"$type\": \"\"," + "\n" +
            "\"$dim\": 0" + "\n" +
          "}," + "\n" +
          "\"id\": \"\"," + "\n" +
          "\"name\": \"\"" + "\n" +
				"}" + "," ; 
		
		for (int j = 0; j < hosts.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#416D9C\"," + "\n" +
		          "\"$type\": \"square\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + hosts.get(j).get(0)+ "\"," + "\n" +
		        "\"name\": \"" + hosts.get(j).get(0)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		for (int j = 0; j < switch1.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + switch1.get(j).get(0)+ "\"," + "\n" +
		        "\"name\": \"" + switch1.get(j).get(0)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		out = out + "{" + "\n" +
				"\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" + 
		            "\"$color\": \"\"," + "\n" +
		            "\"$type\": \"\"," + "\n" +
		            "\"$dim\": 0" + "\n" +
		          "}," + "\n" +
		          "\"id\": \"\"," + "\n" +
		          "\"name\": \"\"" + "\n" +
						"}" + "\n" + "]" ; 
		
		System.out.println(out);
		return out;
	}
}