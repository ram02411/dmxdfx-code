package graph;

import java.util.*;
import floodlight.loadbalancer.*;
import evolve.hpc.*;

public class createGraph {

	public static String make(List<vip> vips, List<pool> pools, List<member> mems)
	{
		HashMap<String,List<String>> vi = new HashMap<String,List<String>>();
		HashMap<String,List<String>> po = new HashMap<String,List<String>>();
		
		
		for(int i=0;i<vips.size();i++)
		{
			String vipid = vips.get(i).getVipId().substring(3);
			List<String>poolattach = new ArrayList<String>();
			
			for(int j=0;j<pools.size();j++)
			{
				if(vipid.equals(pools.get(j).getpool_vip_id()))
				{
					poolattach.add(pools.get(j).getpoolname());
				}
			}
			if(poolattach!=null){
			vi.put(vips.get(i).getVipId(), poolattach);}
						
		}
		for(int i=0;i<pools.size();i++)
		{
			String poolid = pools.get(i).getpoolid();
			List<String>memattach = new ArrayList<String>();
			
			for(int j=0;j<mems.size();j++)
			{
				if(poolid.equals(mems.get(j).getmem_pool_id()))
				{
					memattach.add(mems.get(j).getmemaddress());
				}
			}
			if(memattach!=null){
			po.put(pools.get(i).getpoolname(), memattach);}
						
		}
		
		String json = makeJson(vi,po,vips,pools);
		
		
		return json;
		
	}

	private static String makeJson(HashMap<String, List<String>> vi,
			HashMap<String, List<String>> po,List<vip> vips, List<pool> pools) {
		
		String out = "{\"name\":\"LOAD BALANCER MODULE\",";
		out = out + "\"children\":"+"[";
		
		for(int i=0;i<vips.size();i++)
		{
			out +=  "{\"name\": \""+ vips.get(i).getVipName()  +"\",";
			if(vi.get(vips.get(i).getVipId())!=null)
			{
				out+= "\"children\": [";
				for(int j =0;j<vi.get(vips.get(i).getVipId()).size();j++){
					
					
				
				out+= "{\"name\": \""+ vi.get(vips.get(i).getVipId()).get(j) +"\"";
				
				System.out.println(po.get(vi.get(vips.get(i).getVipId()).get(j)));
				
				if(!po.get(vi.get(vips.get(i).getVipId()).get(j)).isEmpty())
				{
					out+= ","+"\"children\": [";
					int m =0;
					for(int k=0;k<po.get(vi.get(vips.get(i).getVipId()).get(j)).size()-1;k++)
					
					{
						out+= "{\"name\": \""+po.get(vi.get(vips.get(i).getVipId()).get(j)).get(k)+"\""+"},";
						m++;
					}
					
					out+= "{\"name\": \""+po.get(vi.get(vips.get(i).getVipId()).get(j)).get(m)+"\""+"}";
					out+="]},";
				}
				//out=out+"]";
				
				
				}
				out = out.substring(0,out.lastIndexOf(","));
				out+="],";
			}
			out = out.substring(0,out.lastIndexOf(","));
			out+="},";
		}
		out = out.substring(0,out.lastIndexOf(","));
		out+="]}";
		
		
		return out;
	}
	
}
