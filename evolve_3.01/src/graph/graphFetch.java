package graph;

import java.io.*;
import floodlight.loadbalancer.*;
import evolve.hpc.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


public class graphFetch {

	
	@SuppressWarnings("resource")
	public static String start() throws IOException, JSONException {
	
		String vipjson = getVips();
		String pooljsonr = getPools();
		
		
		String [] as = pooljsonr.split(",\"pool\":");
		
		String finalone = "";

		int i=0;
		while (i<as.length)
		{ if(as[i].startsWith("\""))
		{
			as[i]= as[i].substring(3);
		}
			System.out.println(as[i]);
			
			if(as[i].startsWith("}")||as[i].startsWith("["))
			{
				finalone = finalone+as[i];
			}
			i++;
		}
		if(finalone.endsWith("\""))
		{
			finalone = finalone+"}]";
		}
		String pooljson = finalone;
		//System.out.println(finalone);
		
		//System.out.println(pooljson);
		
		
	    String memsjson = getMems();
		System.out.println(vipjson);
		System.out.println(pooljson);
		System.out.println(memsjson);
		
		List<vip> vips =getVipsFromJson(vipjson); 
		List<pool> pools =getPoolsFromJson(pooljson); 
		List<member> mems =getMemsFromJson(memsjson); 
		
		String json = createGraph.make(vips,pools,mems);
		System.out.println(json);
		FileWriter file2;
		 file2 = new FileWriter("/home/openflow/evolve/evolve/WebContent/json/flare.json");
		 file2.write(json);
		 file2.flush();
	return "completed";
	}
	
	private static List<vip> getVipsFromJson(String vipjson) throws JSONException {
		
		List<vip> vips = new ArrayList<vip>();
		JSONArray sportsArray = new JSONArray(vipjson);
		for(int i=0;i<sportsArray.length();i++){
		JSONObject firstSport = sportsArray.getJSONObject(i);
		vip vip = new vip();
		vip.setVipName(firstSport.getString("name"));
		vip.setVipId(firstSport.getString("id"));
		vip.setVipAddress(firstSport.getString("address"));
		vips.add(vip);
			
	}
		return vips;
	}

	private static List<pool> getPoolsFromJson(String pooljson) throws JSONException {
		
		List<pool> pools = new ArrayList<pool>();
		JSONArray sArray = new JSONArray(pooljson);
		for(int i=0;i<sArray.length();i++){
		JSONObject firstSport = sArray.getJSONObject(i);
		pool pool = new pool();
		pool.setpoolname((firstSport.getString("name")));
		pool.setpoolid(firstSport.getString("id"));
		pool.setpool_vip_id(firstSport.getString("vipId"));
		pools.add(pool);
			
	}
		return pools;
	}

	private static List<member> getMemsFromJson(String memjson) throws JSONException {
		List<member> mems = new ArrayList<member>();
		JSONArray sportsArray = new JSONArray(memjson);
		for(int i=0;i<sportsArray.length();i++){
		JSONObject firstSport = sportsArray.getJSONObject(i);
		member mem = new member();
		mem.setmemid(firstSport.getString("id"));
		mem.setmemaddress(firstSport.getString("address"));
		mem.setmem_pool_id(firstSport.getString("poolId"));
			mems.add(mem);
	}
		return mems;
	}

	public static String getVips() throws IOException{
		  URL url1 = null;
		try {
			
			url1 = new URL("http://192.168.0.20:8080/quantum/v1.0/vips/");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       HttpURLConnection con1 = (HttpURLConnection) url1.openConnection();
	    con1.setRequestMethod("GET");
	 ;
	 
	      InputStreamReader reader1 = new InputStreamReader(con1.getInputStream());
	    String res1="";
	    while (true) {
	           int ch = reader1.read();
	        if(ch!=-1){
	          res1=res1+(char)ch;}
	        else{break;}
	       }
		
		return res1;
		
	}
	public static String getPools() throws IOException{
		  URL url1 = null;
		try {
			
			url1 = new URL("http://192.168.0.20:8080/quantum/v1.0/pools/");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       HttpURLConnection con1 = (HttpURLConnection) url1.openConnection();
	    con1.setRequestMethod("GET");
	 ;
	 
	      InputStreamReader reader1 = new InputStreamReader(con1.getInputStream());
	    String res1="";
	    while (true) {
	           int ch = reader1.read();
	        if(ch!=-1){
	          res1=res1+(char)ch;}
	        else{break;}
	       }
		
		return res1;
		
	}
	public static String getMems() throws IOException{
		  URL url1 = null;
		try {
			
			url1 = new URL("http://192.168.0.20:8080/quantum/v1.0/members/");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	       HttpURLConnection con1 = (HttpURLConnection) url1.openConnection();
	    con1.setRequestMethod("GET");
	 ;
	 
	      InputStreamReader reader1 = new InputStreamReader(con1.getInputStream());
	    String res1="";
	    while (true) {
	           int ch = reader1.read();
	        if(ch!=-1){
	          res1=res1+(char)ch;}
	        else{break;}
	       }
		
		return res1;
		
	}

}
