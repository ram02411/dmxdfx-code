package stateTable;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import canvas.util.JSONException;

public class topologyConstructorSNT {

	
	
	
	public static String getTopology() throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
	
		List<String> switches = stateTableHPSNT.getTopo();
		List<String> hosts =  stateTableHPSNT.hosts;
		List<String> src_dpid = stateTableHPSNT.src_dpid;
		List<String> dst_dpid = stateTableHPSNT.dst_dpid;
		HashMap<String,String> host_switch = stateTableHPSNT.host_switch;
		
		
		
		String out = "[" + "\n" + "{" + "\n" + " \"adjacencies\": [" ;
	//	System.out.println(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			out =  out +  "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + hosts.get(i) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ host_switch.get(hosts.get(i)) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#000000\"" + "\n" + "}" + "\n" + "}";
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \"#0489B1\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + host_switch.get(hosts.get(i))+ "\"," + "\n" +
		        "\"name\": \"" + host_switch.get(hosts.get(i))+"\"" + "\n" +
		      "}" + "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		
		for(int j=0;j<src_dpid.size();j++){
			out = out + "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + src_dpid.get(j) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ dst_dpid.get(j) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#557EAA\"" + "\n" + "}" + "\n" + "}";
			
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \"#0489B1\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + dst_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + dst_dpid.get(j)+"\"" + "\n" +
		      "}"+ "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		out = out + "]," + "\n" +
        "\"data\": {" + "\n" + 
            "\"$color\": \"\"," + "\n" +
            "\"$type\": \"\"," + "\n" +
            "\"$dim\": 0" + "\n" +
          "}," + "\n" +
          "\"id\": \"\"," + "\n" +
          "\"name\": \"\"" + "\n" +
				"}" + "," ; 
		
		for (int j = 0; j < hosts.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#F7F8E0\"," + "\n" +
		          "\"$type\": \"square\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + hosts.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + hosts.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		for (int j = 0; j < src_dpid.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#0489B1\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + src_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + src_dpid.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		out = out + "{" + "\n" +
				"\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" + 
		            "\"$color\": \"\"," + "\n" +
		            "\"$type\": \"\"," + "\n" +
		            "\"$dim\": 0" + "\n" +
		          "}," + "\n" +
		          "\"id\": \"\"," + "\n" +
		          "\"name\": \"\"" + "\n" +
						"}" + "\n" + "]" ; 
		
	//	System.out.println(out);
		return out;
		
		
		
		
		
		
		
	}

}
