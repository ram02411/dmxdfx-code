package stateTable;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

import logger.logger;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

import networkCreator.miniCreatorSNT;

import canvas.util.JSONException;

public class PNTtoSNT {
	static logger logme = logger.getInstance() ;

	private static String str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
	@SuppressWarnings("static-access")
	public static void createNetwork() {
		
		List<String> switches = null;
		
			switches = stateTableHP.getTopo();
		
		List<String> hosts =  stateTableHP.hosts;
		List<String> src_dpid = stateTableHP.src_dpid;
		List<String> dst_dpid = stateTableHP.dst_dpid;
		HashMap<String,String> host_switch = stateTableHP.host_switch;
		 
		
		String created = createNW(switches,hosts,src_dpid,dst_dpid,host_switch);
		
		String mini1 = "from mininet.topo import Topo"+"\n"+"\n"+"class MyTopo(Topo):"+"\n"+" "+"def __init__(self):"+"\n"+"  "+"Topo.__init__(self)"+"\n";
		String mini2 ="\n"+"topos={'mytopo':(lambda:MyTopo())}";
		
		String script = mini1+created+"\n"+mini2;
		//System.out.println(script);
		
		try {
			 miniCreatorSNT.createSNTMini(script);
		} catch (JSchException | SftpException | IOException
				| InterruptedException e) {
			logme.log.severe(e.getMessage());

		}
	//	System.out.println(response);
	}

	private static String createNW(List<String> switches, List<String> hosts,
			List<String> src_dpid, List<String> dst_dpid,
			HashMap<String, String> host_switch) {
		String out ="";
		HashMap<String,String> switch_switch = new HashMap<String,String>();
		HashMap<String,String> host_host = new HashMap<String,String>();
		
		for(int j=0;j<switches.size();j++)
		{
			switch_switch.put(switches.get(j), "Switch"+str.charAt(j));
		}
		for(int j=0;j<hosts.size();j++)
		{
			host_host.put(hosts.get(j), "Host"+str.charAt(j));
		}
		int k=1;
		for(int i=0; i<hosts.size();i++){
		//out = out+"  "+"Host"+str.charAt(i)+" "+"="+(i+1)+"\n";
			int p =i+1;
		out = out+"  "+"Host"+str.charAt(i)+" "+"="+"self.addHost( 'h"+p+"' )"+"\n";
	//	out = out+"  "+"self.add_node("+"Host"+str.charAt(i)+",Node(is_switch=False))"+"\n";
		k++;
		}
		
		for(int i=0;i<switches.size();i++){
			//out = out+"  "+"Switch"+str.charAt(i)+"="+k+"\n";
			
			out = out+"  "+"Switch"+str.charAt(i)+" "+"="+"self.addSwitch( 's"+k+"' )"+"\n";
			
			//out = out+"  "+"self.add_node("+"Switch"+str.charAt(i)+",Node(is_switch=True))"+"\n";
			
			k++;
		
		}
		
		for(int i=0;i<src_dpid.size();i++)
		{
			if(!(out.contains("self.addLink("+switch_switch.get(dst_dpid.get(i))+","+switch_switch.get(src_dpid.get(i)) +")")))
			out = out+"  "+"self.addLink("+switch_switch.get(src_dpid.get(i))+","+switch_switch.get(dst_dpid.get(i)) +")"+"\n";
		}
		for(int i=0;i<host_switch.size();i++)
		{
			
			out = out+"  "+"self.addLink("+host_host.get(hosts.get(i))+","+switch_switch.get(host_switch.get(hosts.get(i)))+")"+"\n";
		}
		
		
		return out;
	}

}
