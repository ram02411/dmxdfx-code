package stateTable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import canvas.util.JSONArray;
import canvas.util.JSONException;
import canvas.util.JSONObject;
import trafficEngineering.pathFinder;

public class test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String json="[\"flows\"{\"priority\": 3000,\"isPermanent\": \"true\",\"timeout\": 0,\"deviceId\": \"of:0000000000000001\",\"tableId\": 0,\"treatment\":{\"instructions\": [{\"type\": \"OUTPUT\",\"port\": 10}],\"deferred\": []},\"selector\": {\"criteria\": [{\"type\": \"IPV4_SRC\",\"ip\": \"10.0.10.1\"},{\"type\": \"IPV4_DST\",\"ip\": \"10.0.10.2\"}]}}]";
		
		try{
			URL Url=new URL("http://192.168.0.17:8181/onos/v1/flows/");
			HttpURLConnection con = (HttpURLConnection) Url.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
		   
		   con.setDoOutput(true);
		   OutputStreamWriter wr = null;
		   try{
		    wr = new OutputStreamWriter(con.getOutputStream());
		   wr.write(json);
		   wr.flush();}
		   catch(Exception e)
		   {
		   	 e.printStackTrace();;
		   }
		   wr.close();
		   System.out.println(con.getResponseCode());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
