package stateTable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import logger.logger;

import canvas.util.*;






public class stateTableHPSNT {
	public static String IP = "192.168.0.21";
	static logger logme = logger.getInstance() ;

	public static HashMap<String,String> host_switch;
	 public static List<String>hosts; 
	 public static List<String>src_dpid; 
	 public static List<String>dst_dpid; 
	@SuppressWarnings("static-access")
	public static List<String> getTopo()  {

		host_switch = new HashMap<String,String>();
		hosts = new ArrayList<String>();
		src_dpid = new ArrayList<String>();
		dst_dpid = new ArrayList<String>();
		String token = null ;
		List<String> switches = null;
		try{
		token = getAuth();}
		catch(Exception e){	logme.log.severe(e.getMessage());
}
		 try{
	  getLinks(token);}
		 catch(Exception e){			logme.log.severe(e.getMessage());
}
		 try{
	   switches =getSwitches(token) ;}
	  catch(Exception e){			logme.log.severe(e.getMessage());
}
	return switches;
		
	
		
		

	}
	
	
	private static List<String> getSwitches(String token) throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException {
		
		String jsonresponse="";
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/of/datapaths");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("GET");
 con.setRequestProperty ("X-Auth-Token", token);
con.setRequestProperty("Content-Type", "application/json");
con.setDoOutput(true);

		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}

	rd.close();
	con.disconnect();
	List<String> switches = new ArrayList<String>();
	JSONObject obj3 = new JSONObject(jsonresponse);
    JSONArray geodata1 = obj3.getJSONArray("datapaths");
     int n = geodata1.length();
     
   for (int g = 0; g < n; g++) {
    JSONObject person1 = geodata1.getJSONObject(g);
 //System.out.println("switchlist");
   System.out.println(person1.getString("dpid"));
   switches.add(person1.getString("dpid"));
	
   }
		
		return switches;
		
	}


	private static void getLinks(String token) throws IOException, NoSuchAlgorithmException, KeyManagementException, JSONException {
		
		String jsonresponse="";
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/net/clusters");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("GET");
 con.setRequestProperty ("X-Auth-Token", token);
con.setRequestProperty("Content-Type", "application/json");
con.setDoOutput(true);

		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}

	rd.close();
	con.disconnect();
	
	//List<String> dst_port = new ArrayList<String>();
	     JSONObject obj1 = new JSONObject(jsonresponse);
	     JSONArray geodata = obj1.getJSONArray("clusters");
	      JSONObject person = geodata.getJSONObject(0);

	    JSONArray obj2 = person.getJSONArray("links");
			 for(int r=0;r<obj2.length();r++)    	    {
				 
				 JSONObject per = obj2.getJSONObject(r);
	 //  System.out.println(per.get("src_dpid")+"    "+per.get("dst_dpid"));
	   src_dpid.add((String) per.get("src_dpid"));
	   dst_dpid.add((String) per.get("dst_dpid"));
	

			 }
			 
			 
			 URL url1 = new URL("https://"+IP+":8443/sdn/v2.0/net/nodes");
			    HttpURLConnection conn = (HttpURLConnection) url1.openConnection();
			 conn.setRequestMethod("GET");
			 conn.setRequestProperty ("X-Auth-Token", token);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setDoOutput(true);

				String jsonresponse1 = "";	
				BufferedReader rd1 = new BufferedReader(new InputStreamReader(
						conn.getInputStream()));
				String line1;
				while ((line1 = rd1.readLine()) != null) {
					jsonresponse1 = jsonresponse1.concat(line1);
				}

				rd1.close();
				conn.disconnect();
			
			
				
				 JSONObject obj3 = new JSONObject(jsonresponse1);
				     JSONArray geodata1 = obj3.getJSONArray("nodes");
				      int n = geodata1.length();
				      
				    for (int g = 0; g < n; g++) {
				     JSONObject person1 = geodata1.getJSONObject(g);
				  
				    System.out.println(person1.getString("ip")+"    "+person1.getString("dpid"));
				    hosts.add(person1.getString("ip"));
				    host_switch.put(person1.getString("ip"), person1.getString("dpid"));
				    
				    }
				
				
				
				
	
		
	}


	public static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException
	{
		
		String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
		String jsonresponse = "";
		

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty("Content-Type", "application/json");

	con.setDoOutput(true);
 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		
 wr.write(tokenInput);
	wr.flush();
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}
	wr.close();
	rd.close();
	
	 JSONObject obj1 = new JSONObject(jsonresponse);
     JSONObject geodata = obj1.getJSONObject("record");
     String token = geodata.getString("token");
 
	return token;
				
	}

}
