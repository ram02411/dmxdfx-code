package stateTable;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import logger.logger;
import onoscontroller.stateTableOnos;
import canvas.util.JSONException;
import dto.IPSdto;

/**
 * 
 * @author java
 *  This class is used to construct topology in JSON fromat 
 *
 */

public class topologyConstructor {

	
	static logger logme = logger.getInstance() ;
	public static List<String> switches; 
	public static List<String> hosts; 
	
	
	/**
	 * 
	 * 
	 * @return 
	 */
	public static String getTopology() throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
	
		String out = null;
		switches = new ArrayList<String>();
		hosts =  new ArrayList<String>();
		String controllertype=IPSdto.getControllerType();
		if(controllertype!=null){
		System.out.println("controller type : "+controllertype);	
		if(controllertype.equals("HPVAN")){
		 switches = stateTableHP.getTopo();
		 hosts =  stateTableHP.hosts;
		List<String> src_dpid = stateTableHP.src_dpid;
		List<String> dst_dpid = stateTableHP.dst_dpid;
		HashMap<String,String> host_switch = stateTableHP.host_switch;
		
		if(switches!=null){
		
		out = "[" + "\n" + "{" + "\n" + " \"adjacencies\": [" ;
	//	System.out.println(hosts.size());
		for (int i = 0; i < hosts.size(); i++) {
			out =  out +  "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + hosts.get(i) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ host_switch.get(hosts.get(i)) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#29220A\"" + "\n" + "}" + "\n" + "}";
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + host_switch.get(hosts.get(i))+ "\"," + "\n" +
		        "\"name\": \"" + host_switch.get(hosts.get(i))+"\"" + "\n" +
		      "}" + "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		
		for(int j=0;j<src_dpid.size();j++){
			out = out + "\n" + "{" + "\n" + " \"nodeTo\": "
					+ "\"" + src_dpid.get(j) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
					+ dst_dpid.get(j) + "\""+"," + "\n" + "\"data\": {" + "\n"
					+ "\"$color\": \"#557EAA\"" + "\n" + "}" + "\n" + "}";
			
			out = out + "\n" + "]" ;
			out = out + "," + "\n" +
			        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
		        "\"id\": \"" + dst_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + dst_dpid.get(j)+"\"" + "\n" +
		      "}"+ "," + "{" + "\n" +  "\"adjacencies\": [" ;
		}
		
		out = out + "]," + "\n" +
        "\"data\": {" + "\n" + 
            "\"$color\": \"\"," + "\n" +
            "\"$type\": \"\"," + "\n" +
            "\"$dim\": 0" + "\n" +
          "}," + "\n" +
          "\"id\": \"\"," + "\n" +
          "\"name\": \"\"" + "\n" +
				"}" + "," ; 
		
		for (int j = 0; j < hosts.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#416D9C\"," + "\n" +
		          "\"$type\": \"square\"," + "\n" +
		          "\"$dim\": 6" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + hosts.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + hosts.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		for (int j = 0; j < src_dpid.size(); j++){
			out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" +
		          "\"$color\": \"#83548B\"," + "\n" +
		          "\"$type\": \"circle\"," + "\n" +
		          "\"$dim\": 10" + "\n" + 
		        "}," + "\n" +
	          "\"id\": \"" + src_dpid.get(j)+ "\"," + "\n" +
		        "\"name\": \"" + src_dpid.get(j)+"\"" + "\n" +
					"}" + "," ; 
			
		}
		
		out = out + "{" + "\n" +
				"\"adjacencies\": []," + "\n" +
		        "\"data\": {" + "\n" + 
		            "\"$color\": \"\"," + "\n" +
		            "\"$type\": \"\"," + "\n" +
		            "\"$dim\": 0" + "\n" +
		          "}," + "\n" +
		          "\"id\": \"\"," + "\n" +
		          "\"name\": \"\"" + "\n" +
						"}" + "\n" + "]" ; 
		
	//	System.out.println(out);
		
		
		
		}
		
		}
		if(controllertype.equals("ONOS")){
			out=null;
			 switches = stateTableOnos.getTopo();
			 hosts =  stateTableOnos.hosts;
			List<String> osrc_dpid = stateTableOnos.src_dpid;
			List<String> odst_dpid = stateTableOnos.dst_dpid;
			HashMap<String,String> ohost_switch = stateTableOnos.host_switch;
			
			if(switches!=null){
			
			out = "[" + "\n" + "{" + "\n" + " \"adjacencies\": [" ;
		//	System.out.println(hosts.size());
			for (int i = 0; i < hosts.size(); i++) {
				out =  out +  "\n" + "{" + "\n" + " \"nodeTo\": "
						+ "\"" + hosts.get(i) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
						+ ohost_switch.get(hosts.get(i)) + "\""+"," + "\n" + "\"data\": {" + "\n"
						+ "\"$color\": \"#29220A\"" + "\n" + "}" + "\n" + "}";
				out = out + "\n" + "]" ;
				out = out + "," + "\n" +
				        "\"data\": {" + "\n" +
			          "\"$color\": \"#83548B\"," + "\n" +
			          "\"$type\": \"circle\"," + "\n" +
			          "\"$dim\": 10" + "\n" + 
			        "}," + "\n" +
			        "\"id\": \"" + ohost_switch.get(hosts.get(i))+ "\"," + "\n" +
			        "\"name\": \"" + ohost_switch.get(hosts.get(i))+"\"" + "\n" +
			      "}" + "," + "{" + "\n" +  "\"adjacencies\": [" ;
			}
			
			
			for(int j=0;j<osrc_dpid.size();j++){
				out = out + "\n" + "{" + "\n" + " \"nodeTo\": "
						+ "\"" + osrc_dpid.get(j) + "\""+"," + "\n" + " \"nodeFrom\": " + "\""
						+ odst_dpid.get(j) + "\""+"," + "\n" + "\"data\": {" + "\n"
						+ "\"$color\": \"#557EAA\"" + "\n" + "}" + "\n" + "}";
				
				out = out + "\n" + "]" ;
				out = out + "," + "\n" +
				        "\"data\": {" + "\n" +
			          "\"$color\": \"#83548B\"," + "\n" +
			          "\"$type\": \"circle\"," + "\n" +
			          "\"$dim\": 10" + "\n" + 
			        "}," + "\n" +
			        "\"id\": \"" + odst_dpid.get(j)+ "\"," + "\n" +
			        "\"name\": \"" + odst_dpid.get(j)+"\"" + "\n" +
			      "}"+ "," + "{" + "\n" +  "\"adjacencies\": [" ;
			}
			
			out = out + "]," + "\n" +
	        "\"data\": {" + "\n" + 
	            "\"$color\": \"\"," + "\n" +
	            "\"$type\": \"\"," + "\n" +
	            "\"$dim\": 0" + "\n" +
	          "}," + "\n" +
	          "\"id\": \"\"," + "\n" +
	          "\"name\": \"\"" + "\n" +
					"}" + "," ; 
			
			for (int j = 0; j < hosts.size(); j++){
				out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
			        "\"data\": {" + "\n" +
			          "\"$color\": \"#416D9C\"," + "\n" +
			          "\"$type\": \"square\"," + "\n" +
			          "\"$dim\": 6" + "\n" + 
			        "}," + "\n" +
		          "\"id\": \"" + hosts.get(j)+ "\"," + "\n" +
			        "\"name\": \"" + hosts.get(j)+"\"" + "\n" +
						"}" + "," ; 
				
			}
			
			for (int j = 0; j < osrc_dpid.size(); j++){
				out = out + "{" + "\n" +  "\"adjacencies\": []," + "\n" +
			        "\"data\": {" + "\n" +
			          "\"$color\": \"#83548B\"," + "\n" +
			          "\"$type\": \"circle\"," + "\n" +
			          "\"$dim\": 10" + "\n" + 
			        "}," + "\n" +
		          "\"id\": \"" + osrc_dpid.get(j)+ "\"," + "\n" +
			        "\"name\": \"" + osrc_dpid.get(j)+"\"" + "\n" +
						"}" + "," ; 
				
			}
			
			out = out + "{" + "\n" +
					"\"adjacencies\": []," + "\n" +
			        "\"data\": {" + "\n" + 
			            "\"$color\": \"\"," + "\n" +
			            "\"$type\": \"\"," + "\n" +
			            "\"$dim\": 0" + "\n" +
			          "}," + "\n" +
			          "\"id\": \"\"," + "\n" +
			          "\"name\": \"\"" + "\n" +
							"}" + "\n" + "]" ; 
			
		//	System.out.println(out);
			
			
			
			}
			
			}
		}
		
		System.out.println(out);	
		return out;
		
		
	}

}
