
package networkCreator;import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Properties;
import java.util.Vector;

import logger.logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import dto.IPSdto;

public class miniCreatorSNT {

	private static  String RemoteHostName = IPSdto.getEvolveIP();
	 static  String evolveIP = IPSdto.getEvolveIP();
	 static  String controllerIP = IPSdto.getEvolveIP();
	 
	 static  String sntcontrollerIP = "192.168.0.21";
		static logger logme = logger.getInstance() ;

	public static String createMini(String script) throws JSchException, SftpException, IOException, InterruptedException {
		
		
		String res = createPython(script);
		//System.out.println(res);
		String done = createNewNetwork();
		return done;
		

	}
public static String createSNTMini(String script) throws JSchException, SftpException, IOException, InterruptedException {
		
		
		String res = createPythonsnt(script);
		//System.out.println(res);
		String done = createNewNetworkSNT();
		return done;
		

	}
	
	@SuppressWarnings("static-access")
	private static String createNewNetwork() throws JSchException, IOException, InterruptedException {
		try {
			JSch jsch=new JSch();
			 String remoteHostUserName = "demo";
			Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
			   String remoteHostpassword = "demo123!";
			session.setPassword(remoteHostpassword);
			   Properties config = new Properties();
			   config.put("StrictHostKeyChecking", "no");
			   session.setConfig(config);
			   session.connect();
		    Channel channel = session.openChannel("shell");
		    Expect expect = new Expect(channel.getInputStream(),
		            channel.getOutputStream());
		    
		    channel.connect();
		    expect.expect("demo@ubuntu:~$");
		  //  System.out.println(expect.before + expect.match);
		    
		    //expect.send("sudo mn -c \n");
		    //expect.expect("[sudo] password for demo:");
		   // expect.expect("demo@ubuntu:~$");
		 //   System.out.println(expect.before + expect.match);
		    //expect.send("demo123!\n");
		  //expect.expect("demo@ubuntu:~$");
		//  System.out.println(expect.before + expect.match);
		  Thread.sleep(3000);
		    expect.send("sudo mn --controller=remote,ip="+evolveIP+",port=6633 --custom ~/mininet/custom/test.py --topo mytopo \n");
		  expect.expect("[sudo] password for demo:");
			   // expect.expect("demo@ubuntu:~$");
			 //   System.out.println(expect.before + expect.match);
			   expect.send("demo123!\n");
		    expect.expect("mininet>");
		  
		   System.out.println(expect.before + expect.match);
		    expect.send("pingall -t\n");
		   // Thread.sleep(20000);
		    expect.expect("mininet>");  
		    System.out.println(expect.before+expect.match);
		    
		    expect.close();
		    session.disconnect();
		} catch (JSchException e) {
	          logme.log.severe( e.getMessage());
		} catch (IOException e) {
	          logme.log.severe( e.getMessage());
		}
		   
          
		Thread.sleep(10000);

		
	
	
		return "mininet created";
	}
	
	private static String createNewNetworkSNT() throws JSchException, IOException, InterruptedException {
		try {
			JSch jsch=new JSch();
			 String remoteHostUserName = "demo";
			Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
			   String remoteHostpassword = "demo123!";
			session.setPassword(remoteHostpassword);
			   Properties config = new Properties();
			   config.put("StrictHostKeyChecking", "no");
			   session.setConfig(config);
			   session.connect();
		    Channel channel = session.openChannel("shell");
		    Expect expect = new Expect(channel.getInputStream(),
		            channel.getOutputStream());
		    
		    channel.connect();
		    expect.expect("demo@ubuntu:~$");
		  //  System.out.println(expect.before + expect.match);
		    
		   // expect.send("sudo mn -c \n");
		    //expect.expect("[sudo] password for demo:");
		   // expect.expect("demo@ubuntu:~$");
		 //   System.out.println(expect.before + expect.match);
		    //expect.send("demo123!\n");
		  //expect.expect("demo@ubuntu:~$");
		//  System.out.println(expect.before + expect.match);
		  Thread.sleep(3000);
		    expect.send("sudo mn --controller=remote,ip="+sntcontrollerIP+",port=6633 --custom ~/mininet/custom/test.py --topo mytopo \n");
		  expect.expect("[sudo] password for demo:");
			   // expect.expect("demo@ubuntu:~$");
			 //   System.out.println(expect.before + expect.match);
			    expect.send("demo123!\n");
		    expect.expect("mininet>");
		  
		   System.out.println(expect.before + expect.match);
		    expect.send("pingall -t\n");
		   // Thread.sleep(20000);
		    expect.expect("mininet>");  
		    System.out.println(expect.before+expect.match);
		    
		    expect.close();
		    session.disconnect();
		} catch (JSchException e) {
	          logme.log.severe( e.getMessage());
		} catch (IOException e) {
	          logme.log.severe( e.getMessage());
		}
		   
          
		Thread.sleep(10000);

		
	
	
		return "mininet created";
	}
	private static String createPython(String script) throws JSchException, SftpException, IOException, InterruptedException {
		JSch jsch=new JSch();
		 String remoteHostUserName = "demo";
		Session session=jsch.getSession(remoteHostUserName, RemoteHostName, 22);
		   String remoteHostpassword = "demo123!";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();
	
		   Channel channel = session.openChannel("sftp");
	        channel.connect();
	 
	        ChannelSftp sftp = (ChannelSftp) channel;
	        sftp.cd("mininet/custom");
	        @SuppressWarnings("unchecked")
			Vector<ChannelSftp.LsEntry> files = sftp.ls("*");
	       // System.out.printf("Found %d files in dir %s%n", files.size(), "mininet/custom");
	 
	     /*   for (ChannelSftp.LsEntry file : files) {
	            if (file.getAttrs().isDir()) {
	                continue;
	            }*/
	        ChannelSftp.LsEntry file = files.elementAt(0);
	        ChannelSftp.LsEntry file1 = files.elementAt(1);
	      
	       
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sftp.put(file.getFilename())));
	        
	           	      char[] buf_f = script.toCharArray();
		             bw.write(buf_f,0,buf_f.length);
		             
	         bw.close();
		          
	           
	           
	        BufferedReader bis = new BufferedReader(new InputStreamReader(sftp.get(file.getFilename())));
          String line = null;
          BufferedWriter bos = new BufferedWriter(new OutputStreamWriter(sftp.put(file1.getFilename())));
          while ((line = bis.readLine()) != null) {
          	      char[] buf = line.toCharArray();
	             bos.write(buf,0,buf.length);
	             bos.newLine();
        
	          
          } 
          bos.close(); 
         
          bis.close();
	         	 
	        channel.disconnect();
	        session.disconnect();
	        
	        String response = "Successfully created python script";
	        
	          

	        return response;

		
	}

	private static String createPythonsnt(String script) throws JSchException, SftpException, IOException, InterruptedException {
		JSch jsch=new JSch();
		 String remoteHostUserName = "demo";
		 String hostname="192.168.0.30";
		Session session=jsch.getSession(remoteHostUserName, hostname, 22);
		   String remoteHostpassword = "demo123!";
		session.setPassword(remoteHostpassword);
		   Properties config = new Properties();
		   config.put("StrictHostKeyChecking", "no");
		   session.setConfig(config);
		   session.connect();
	
		   Channel channel = session.openChannel("sftp");
	        channel.connect();
	 
	        ChannelSftp sftp = (ChannelSftp) channel;
	        sftp.cd("mininet/custom");
	        @SuppressWarnings("unchecked")
			Vector<ChannelSftp.LsEntry> files = sftp.ls("*");
	       // System.out.printf("Found %d files in dir %s%n", files.size(), "mininet/custom");
	 
	     /*   for (ChannelSftp.LsEntry file : files) {
	            if (file.getAttrs().isDir()) {
	                continue;
	            }*/
	        ChannelSftp.LsEntry file = files.elementAt(0);
	        ChannelSftp.LsEntry file1 = files.elementAt(1);
	      
	       
	        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sftp.put(file.getFilename())));
	        
	           	      char[] buf_f = script.toCharArray();
		             bw.write(buf_f,0,buf_f.length);
		             
	         bw.close();
		          
	           
	           
	        BufferedReader bis = new BufferedReader(new InputStreamReader(sftp.get(file.getFilename())));
          String line = null;
          BufferedWriter bos = new BufferedWriter(new OutputStreamWriter(sftp.put(file1.getFilename())));
          while ((line = bis.readLine()) != null) {
          	      char[] buf = line.toCharArray();
	             bos.write(buf,0,buf.length);
	             bos.newLine();
        
	          
          } 
          bos.close(); 
         
          bis.close();
	         	 
	        channel.disconnect();
	        session.disconnect();
	        
	        String response = "Successfully created python script";
	        
	          

	        return response;

		
	}

}
