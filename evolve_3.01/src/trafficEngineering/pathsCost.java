package trafficEngineering;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import FlowPusher.flowFetcher;

import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;

public class pathsCost {
static String IP =IPSdto.getControllerIP();
	/**
	 * @param token2 
	 * @param args
	 * @return 
	 * @throws JSONException 
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * @throws KeyManagementException 
	 */
	public static HashMap<String, Integer> getAverageCost(List<String> list, String token2) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
		
		String token = token2;
		int tot_prio = 0;
		int tot_pkts =0;
		int tot_bytes=0;
				
		int tot_avg_prio = 0;
		int tot_avg_pkts =0;
		int tot_avg_bytes=0;
		HashMap<String,Integer> costs = new HashMap<String,Integer>();
		
		for(int i=0;i<list.size();i++)
		{
			HashMap<String, Integer> cost = getSwitchCost(list.get(i));
		    tot_prio = tot_prio + cost.get("avg_prio");
		    tot_pkts = tot_pkts + cost.get("avg_pkts");
		    tot_bytes = tot_bytes + cost.get("avg_bytes");
		    			
		}
		
		int path_size = list.size();
		
		if(path_size!=0)
		{
			tot_avg_prio = tot_prio/path_size;
			tot_avg_pkts = tot_pkts/path_size;
			tot_avg_bytes = tot_bytes/path_size;
		}
		
		int cost  = (tot_avg_prio/100) + (tot_avg_pkts/500)+(tot_avg_bytes/1000)+(path_size*100);

		costs.put("prio", tot_avg_prio);
		costs.put("pkts", tot_avg_pkts);
		costs.put("bytes", tot_avg_bytes);
		costs.put("cost", cost);
		costs.put("number", path_size);
		
		return costs;
	}

	

	private static HashMap<String, Integer> getSwitchCost(String dpid) throws JSONException {
		List<HashMap<String,String>> sflow =  new ArrayList<HashMap<String,String>>();
		
		try {
			 sflow = flowFetcher.getFlowTable(dpid);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int no_of_flows = sflow.size();
		int sumpackets =0;
		int avg_pkts = 0;
		int sumbytes =0;
		int avg_bytes=0;
		List<Integer> prios = new ArrayList<Integer>();
		for(int j=0;j<sflow.size();j++)
		{
			int prio=0;
			if(sflow.get(j).get("priority")!=null){
			prio = Integer.parseInt(sflow.get(j).get("priority"));}
			if(sflow.get(j).get("packet_count")!=null){
			sumpackets = sumpackets+Integer.parseInt(sflow.get(j).get("packet_count"));}
			if(sflow.get(j).get("byte_count")!=null){
			sumbytes = sumbytes+Integer.parseInt(sflow.get(j).get("byte_count"));}
			prios.add(prio);
		}
		if(no_of_flows!=0){
		avg_pkts = sumpackets/no_of_flows;}
		
		if(no_of_flows!=0){
			avg_bytes = sumbytes/no_of_flows;}
		
		//int least_prio = Collections.min(prios);
		int sum=0;
		int avg_prio = 0;
		
		for(int k =0;k<prios.size();k++)
		{
			sum = sum+prios.get(k);
		}
		if(prios.size()!=0)
		{
			avg_prio = (sum/prios.size());
		}
		
		HashMap<String,Integer> one_cost = new HashMap<String,Integer>();
		
		one_cost.put("avg_prio", avg_prio);
		one_cost.put("avg_pkts", avg_pkts);
		one_cost.put("avg_bytes", avg_bytes);

		
		
		return one_cost;
	}



	public static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException
	{

		String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
		String jsonresponse = "";
		

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty("Content-Type", "application/json");

	con.setDoOutput(true);
 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		
 wr.write(tokenInput);
	wr.flush();
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}
	wr.close();
	rd.close();
	
	 JSONObject obj1 = new JSONObject(jsonresponse);
     JSONObject geodata = obj1.getJSONObject("record");
     String token = geodata.getString("token");
 
	return token;

		
	}
	

	
	
}
