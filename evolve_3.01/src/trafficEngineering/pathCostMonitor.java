package trafficEngineering;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import FlowPusher.flowFetcher;
import canvas.util.JSONException;

public class pathCostMonitor {

	public static String prio;
	public static void costMonitor(List<String> path, List<List<String>> paths,String priority,String protocol, String srchost, String dsthost ) throws JSONException {
		
		
		int[] costs = new int[paths.size()];
		prio = priority;
		for(int i=0;i< paths.size();i++){
			
			costs[i]=(getCost(paths.get(i)));
					
		}
	
	     int smallest = costs[0];
         
        
         for(int i=1; i< costs.length; i++)
         {
                 
                  if (costs[i] < smallest)
                         smallest = costs[i];
                
         }
        
		
		
		if((smallest!= getCost(path)))
		{
			int idex = -1;
			for(int i=0;i< costs.length;i++){
				if(costs[i]==smallest)
				idex =i;		
			}
			try {
				deleteExistingFlow.deleteFlow(path, priority, protocol, srchost, dsthost);
				flowFromPath.createFlow(paths.get(idex), priority, protocol, srchost, dsthost);
			} catch (KeyManagementException | NoSuchAlgorithmException
					| IOException | JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}

	}

	private static Integer getCost(List<String> list) throws JSONException {

		
		int tot_prio = 0;
		int tot_pkts =0;
		int tot_bytes=0;
				
		int tot_avg_prio = 0;
		int tot_avg_pkts =0;
		int tot_avg_bytes=0;
		HashMap<String,Integer> costs = new HashMap<String,Integer>();
		
		for(int i=0;i<list.size();i++)
		{
			HashMap<String, Integer> cost = getSwitchCost(list.get(i));
		    tot_prio = tot_prio + cost.get("avg_prio");
		    tot_pkts = tot_pkts + cost.get("avg_pkts");
		    tot_bytes = tot_bytes + cost.get("avg_bytes");
		    			
		}
		
		int path_size = list.size();
		
		if(path_size!=0)
		{
			tot_avg_prio = tot_prio/path_size;
			tot_avg_pkts = tot_pkts/path_size;
			tot_avg_bytes = tot_bytes/path_size;
		}
		
		int cost  = (tot_avg_prio/100) + (tot_avg_pkts/500)+(tot_avg_bytes/1000)+(path_size*100);

		costs.put("prio", tot_avg_prio);
		costs.put("pkts", tot_avg_pkts);
		costs.put("bytes", tot_avg_bytes);
		costs.put("cost", cost);
		costs.put("number", path_size);
		
		return cost;

		
		
		
	}

	private static HashMap<String, Integer> getSwitchCost(String dpid) throws JSONException {

List<HashMap<String,String>> sflow =  new ArrayList<HashMap<String,String>>();
		
		try {
			 sflow = flowFetcher.getFlowTable(dpid);
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int no_of_flows = sflow.size();
		int sumpackets =0;
		int avg_pkts = 0;
		int sumbytes =0;
		int avg_bytes=0;
		List<Integer> prios = new ArrayList<Integer>();
		for(int j=0;j<sflow.size();j++)
			
		{ if((sflow.get(j).get("priority")!=null)){
			if(!(sflow.get(j).get("priority").equals(prio))){
			int prio = Integer.parseInt(sflow.get(j).get("priority"));
			sumpackets = sumpackets+Integer.parseInt(sflow.get(j).get("packet_count"));
			sumbytes = sumbytes+Integer.parseInt(sflow.get(j).get("byte_count"));
			prios.add(prio);}
		}}
		if(no_of_flows!=0){
		avg_pkts = sumpackets/no_of_flows;}
		
		if(no_of_flows!=0){
			avg_bytes = sumbytes/no_of_flows;}
		
	
		int sum=0;
		int avg_prio = 0;
		
		for(int k =0;k<prios.size();k++)
		{
			sum = sum+prios.get(k);
		}
		if(prios.size()!=0)
		{
			avg_prio = (sum/prios.size());
		}
		
		HashMap<String,Integer> one_cost = new HashMap<String,Integer>();
		
		one_cost.put("avg_prio", avg_prio);
		one_cost.put("avg_pkts", avg_pkts);
		one_cost.put("avg_bytes", avg_bytes);

		
		
		return one_cost;
		
		
	}

}
