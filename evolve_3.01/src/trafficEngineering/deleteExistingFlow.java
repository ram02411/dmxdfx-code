package trafficEngineering;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import logger.logger;

import stateTable.stateTableHP;
import FlowPusher.flowPusher;
import canvas.util.JSONException;

public class deleteExistingFlow {
	static logger logme = logger.getInstance() ;

	public static List<String> switches;
	public static List<String> hosts;
	public static List<String> src_dpid;
	public static List<String> src_port;
	public static List<String> dst_port;
	public static List<String> dst_dpid;
    public static HashMap<String,String>host_switch;
    public static HashMap<String,String>host_switchport;

	public static void deleteFlow(List<String> path, String priority, String protocol, String srchost, String dsthost) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {
		
		
		switches = new ArrayList<String>();
		hosts = new ArrayList<String>();
		src_dpid = new ArrayList<String>();
		src_port = new ArrayList<String>();
		dst_dpid = new ArrayList<String>();
		dst_port = new ArrayList<String>();
		host_switch = new HashMap<String,String>();
		host_switchport = new HashMap<String,String>();

		
		switches = stateTableHP.getTopo();
		
		src_dpid = stateTableHP.src_dpid;
		src_port = stateTableHP.src_port;
		dst_dpid = stateTableHP.dst_dpid;
		dst_port = stateTableHP.dst_port;
        host_switchport = stateTableHP.host_switchport;
		
		String token = stateTableHP.getAuth();
		
		for(int i=0;i<path.size()-1;i++){
		
		List<Integer> indices = new ArrayList<Integer>();
		int finalindex = -1;
		for(int j=0;j<src_dpid.size();j++){
			
			if(path.get(i).equals(src_dpid.get(j))&& path.get(i+1).equals(dst_dpid.get(j))){ finalindex = j;}
			
		}
		
		delFlow(path.get(i),priority,protocol,srchost,dsthost);
				
		}
		
		delFlow(path.get(path.size()-1),priority,protocol,srchost,dsthost);
		

		for(int i=path.size()-1;i>0;i--){
		
		List<Integer> indices = new ArrayList<Integer>();
		int finalindex = -1;
		for(int j=0;j<dst_dpid.size();j++){
			
			if(path.get(i).equals(dst_dpid.get(j))&& path.get(i-1).equals(src_dpid.get(j))){ finalindex = j;}
			
		}
		
		delFlow(path.get(i),priority,protocol,dsthost,srchost);
				
		}
		
		delFlow(path.get(0),priority,protocol,dsthost,srchost);
		
	}


	@SuppressWarnings("static-access")
	private static void delFlow(String dpid, String priority,
			String protocol, String srchost, String dsthost) {

		String output ="{\"version\": \"1.0.0\",";
		output = output + "\"flow\":{";
output= output+ "\"priority\":"+priority+",";
output = output+ "\"match\": [";

if(String.valueOf(srchost).equalsIgnoreCase("")||srchost==null)
{
 output = output;
}
else{
	output = output+"{ \"ipv4_src\":"+"\""+srchost+"\"" +"},";
}

if(String.valueOf(dsthost).equalsIgnoreCase("")||dsthost==null)
{
 output = output;
}
else{
	output = output+"{ \"ipv4_dst\":"+"\""+ dsthost+"\"" +"},";
}

if(String.valueOf(protocol).equalsIgnoreCase("")||protocol==null||String.valueOf(protocol).equalsIgnoreCase("ANY"))
{
 output = output;
}
else{
	output = output+"{ \"ip_proto\":"+"\""+ protocol+"\"" +"},";
}
//if(protocol!=null)
//{
// output = output+"{ \"tcp_dst\":"+ dst_port+"}";
//}
output = output+"{ \"eth_type\":"+"\"ipv4\"" +"}";
output=output+"]";


output=output+"}}";
//System.out.println(output);
String res = null;
try {
	res = flowPusher.pushFlows(dpid, output);
} catch (KeyManagementException | NoSuchAlgorithmException | IOException
		| JSONException e) {
	logme.log.severe(e.getMessage());

}
logme.log.info(res);


		
		
	}

		
		

		

	}


