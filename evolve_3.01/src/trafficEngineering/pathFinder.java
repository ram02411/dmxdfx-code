package trafficEngineering;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.*;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;



import canvas.util.JSONArray;
import canvas.util.JSONException;
import canvas.util.JSONObject;
import dto.IPSdto;
import onoscontroller.stateTableOnos;
import stateTable.stateTableHP;

public class pathFinder {
static String IP = IPSdto.getControllerIP();
	public static List<String> switches;
	public static List<String> hosts;
    public static HashMap<String,String>host_switch;
   public static List<HashMap<String,Integer>> cost_details;
   public static List<List<String>> paths;
    
	public static void findPath(String host1,String host2) throws KeyManagementException, NoSuchAlgorithmException, IOException, JSONException {

		String controllertype=IPSdto.getControllerType();
		if(controllertype.equalsIgnoreCase("ONOS")){
			switches =   new ArrayList<String>();
	        hosts=   new ArrayList<String>();
	        host_switch = new HashMap<String,String>();

	        
			
				switches = stateTableOnos.getTopo();
			
			hosts = stateTableOnos.hosts;
			host_switch = stateTableOnos.host_switch;
			
			String dpid1 = host_switch.get(host1);
			String dpid2 = host_switch.get(host2);
			
			paths = new ArrayList<List<String>>();
			
			OnosBFS a = new OnosBFS(dpid1,dpid2);
			 paths =  a.getPath();
			//System.out.println(paths.get(0));
			 cost_details = new ArrayList<HashMap<String,Integer>>();
			
			for(int i=0;i<paths.size();i++)
			{
				try {
					cost_details.add(OnospathsCost.getAverageCost(paths.get(i)));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
										
			}
		}
		if(controllertype.equalsIgnoreCase("HPVAN")){
		switches =   new ArrayList<String>();
        hosts=   new ArrayList<String>();
        host_switch = new HashMap<String,String>();

        String token = getAuth();
		
			switches = stateTableHP.getTopo();
		
		hosts = stateTableHP.hosts;
		host_switch = stateTableHP.host_switch;
		
		String dpid1 = host_switch.get(host1);
		String dpid2 = host_switch.get(host2);
		
		paths = new ArrayList<List<String>>();
		
		BFS a = new BFS(dpid1,dpid2);
		 paths =  a.getPath();
		//System.out.println(paths.get(0));
		 cost_details = new ArrayList<HashMap<String,Integer>>();
		
		for(int i=0;i<paths.size();i++)
		{
			try {
				cost_details.add(pathsCost.getAverageCost(paths.get(i),token));
			} catch (KeyManagementException | NoSuchAlgorithmException
					| IOException | JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
									
		}
		
		}
	}


		
	private static String getSorted(String dpid) {
		String sorted="";
		 for(int i=0;i<21;i++)
		    {
		    	sorted = sorted + (char)dpid.charAt(i)+(char)dpid.charAt(i+1)+"%3A";
		    	i= i+2;
		    }
		    
		    sorted = sorted+(char)dpid.charAt(21)+(char)dpid.charAt(22);
		    
		return sorted;
	}


	public static String getAuth() throws NoSuchAlgorithmException, KeyManagementException, IOException, JSONException
	{

		String tokenInput = "{\"login\":{\"user\":\"sdn\",\"password\":\"skyline\"}}";
		String jsonresponse = "";
		

		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
               return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
           public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
    };

   
   SSLContext sc = SSLContext.getInstance("SSL");
   sc.init(null, trustAllCerts, new java.security.SecureRandom());
    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

   
    HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
            return true;
       }
    };
    
	     
  
    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

    URL url = new URL("https://"+IP+":8443/sdn/v2.0/auth");
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
 con.setRequestMethod("POST");
con.setRequestProperty("Content-Type", "application/json");

	con.setDoOutput(true);
 OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
		
 wr.write(tokenInput);
	wr.flush();
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(
			con.getInputStream()));
	String line;
	while ((line = rd.readLine()) != null) {
		jsonresponse = jsonresponse.concat(line);
	}
	wr.close();
	rd.close();
	
	 JSONObject obj1 = new JSONObject(jsonresponse);
     JSONObject geodata = obj1.getJSONObject("record");
     String token = geodata.getString("token");
 
	return token;

		
	}
	
}
