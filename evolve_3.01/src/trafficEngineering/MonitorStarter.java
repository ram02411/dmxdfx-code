package trafficEngineering;

import java.util.List;
import java.util.TimerTask;

import logger.logger;

import canvas.util.JSONException;

public class MonitorStarter extends TimerTask {
	static logger logme = logger.getInstance() ;

	@Override
	public void run() {
		
		 List<List<String>> paths = monitorScheduler.paths;
		 List<String>path= monitorScheduler.path;
		 String priority= monitorScheduler.priority;
		 String protocol= monitorScheduler.protocol;
		String srchost= monitorScheduler.srchost;
		 String dsthost= monitorScheduler.dsthost;
		 
		 try {
			pathCostMonitor.costMonitor(path, paths, priority, protocol, srchost, dsthost);
		} catch (JSONException e) {
			logme.log.severe(e.getMessage());
		}

	}

}
