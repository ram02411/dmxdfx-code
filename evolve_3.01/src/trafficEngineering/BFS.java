package trafficEngineering;


import java.util.*;

import stateTable.stateTableHP;

public class BFS{

public static int count;
public static Hashtable hash; 
private static String START;
private static  String END;

//public static  List<LinkedList<String>> allpaths=ArrList<String>>(); 
public List<String> trypath = new ArrayList<String>();
public BFS() {
     hash=new Hashtable();
     count=0;
}

public BFS(String start,String end)
{
	//allpaths= new ArrayList<LinkedList<String>>();
	hash=new Hashtable();
    count=0;
	START = start;
	END = end;
}

public List<List<String>>  getPath() {  
List<List<String>> allpaths = new ArrayList<List<String>>();
List<String>src_dpid = stateTableHP.src_dpid;
	List<String>dst_dpid = stateTableHP.dst_dpid;

    GraphStructure graph = new GraphStructure();

    for(int i=0;i<src_dpid.size();i++){
    graph.addEdge(src_dpid.get(i), dst_dpid.get(i));
    graph.addEdge(dst_dpid.get(i), src_dpid.get(i));
    }


    

    LinkedList<String> visited = new LinkedList<String>();
   
    visited.add(START);
    

    new BFS().breadthFirst(graph, visited,allpaths);
    //System.out.println(allpaths.get(0));
    for(int i=0;i<allpaths.size();i++)
    {
    	System.out.println(allpaths.get(i));
    }
	return allpaths;
   }      
   public  void breadthFirst(GraphStructure graph, LinkedList<String> visited, List<List<String>> allpaths) {
    LinkedList<String> nodes = graph.adjacentNodes(visited.getLast());

    for (String node : nodes) {
        if (visited.contains(node)) {
            continue;
        }
        if (node.equals(END)) {
            count++;
            visited.add(node);
            printPath(count,visited);
            
          System.out.println(visited);
          System.out.println("SIZE IS !! "+visited.size());
          for(int a=0;a<visited.size();a++){
        	  System.out.println(visited.get(a));
        	  //printPath(count, visited.get(a));
        	  trypath.add((visited.get(a)));
        	  }
          allpaths.add(trypath);
          trypath = new ArrayList<String>();
          System.out.println("FINFISHED FOR LOOP");
            //allpaths.get(count -1)=( visited);
            visited.removeLast();
            break;
        }
    }
    // in breadth-first, recursion needs to come after visiting adjacent nodes
    for (String node : nodes) {
        if (visited.contains(node) || node.equals(END)) {
            continue;
        }
        visited.addLast(node);
        breadthFirst(graph, visited,allpaths);
        visited.removeLast();
    }
    }
    public static  void printPath(int count,LinkedList<String> visited) {   
    	//allpaths.add(visited);
    	//System.out.println(allpaths.get(0));
    	
    	
    	
    	//System.out.println(visited.size());
    String temp="";
    for (String node : visited) {
        temp=temp+node+",";            
       // System.out.print(node);
       // System.out.print(" ");
    }
    System.out.println();
    System.out.println("Available Path  "+count+": : : :"+ temp);     
    hash.put(count,temp);
    //System.out.println("exp = " + hash.toString());       
    }
    } 