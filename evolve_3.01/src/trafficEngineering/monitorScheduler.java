package trafficEngineering;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

import stateTable.stateTableHP;



public class monitorScheduler {

	public static List<List<String>> paths;
	public static List<String>path;
	public static String priority;
	public static String protocol;
	public static String srchost;
	public static String dsthost;
	
	
 public monitorScheduler(List<String>selected,List<List<String>> pathes, String prio,String proto,String src_host, String dst_host)
 
 {
	 
	 paths = pathes;
	 path = selected;
	 priority = prio;
	 protocol = proto;
	 srchost = src_host;
	 dsthost = dst_host;
	 scheduleRule(); 
	 
 }


public static void scheduleRule() {
	//System.out.println("Scheduling for traffic engineering starting from now!");
	Timer timer = new Timer();
	
	timer.scheduleAtFixedRate(new MonitorStarter(),5000,30000);
	if(stateTableHP.getTopo()==null){timer.cancel();}
}
	
	

}
