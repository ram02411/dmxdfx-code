package conversions;

import java.util.Calendar;

public class Datetimehp {
	
	public static String convertdatetime(String datetime){
		String date=convertdate(datetime);
		String time=converttime(datetime);
		String finaldatetime=date+" "+time;
		return finaldatetime;
	}
	
	
	public static String convertdate(String datetime) {
		String date=null;
		String month=null;
		int position1 = datetime.indexOf("@");
		System.out.println(position1);
		String variable=datetime.substring(0,position1);
		System.out.println(variable);
		int position2=variable.indexOf("/");
		System.out.println(position2);
		if(position2==1){
			System.out.println("date is "+variable);
			month=variable.substring(0, 1);
			month="0"+month;
			System.out.println(month);
		}
		else if(position2==2){
			System.out.println("date is "+variable);
			month=variable.substring(0, 2);
			System.out.println(month);
		}
		
		String variable2=variable.substring(position2+1, position1);
		System.out.println("date is "+variable2);
		Integer lengthdate=variable2.length();
		if(lengthdate==1){			
			date="0"+variable2;
		}
		else{
			date=variable2;
		}
		System.out.println("final date is "+date);
		System.out.println("final month is "+month);
		int year = Calendar.getInstance().get(Calendar.YEAR);
		System.out.println("final year is "+year);
		String finaldate = year + "-" + month + "-" + date;
		System.out.println(finaldate);
		return finaldate;
	}
	
	public static String converttime(String datetime) {
		String hour=null;
		String minute=null;
		int position1 = datetime.indexOf("@");
		System.out.println(position1);
		Integer end=datetime.length();
		String variable=datetime.substring(position1+1,end);
		System.out.println("time is "+variable);
		int position2=variable.indexOf(":");
		System.out.println(position2);
		if(position2==1){
			System.out.println("time is "+variable);
			hour=variable.substring(0, 1);
			hour="0"+hour;
			System.out.println(hour);
		}
		else if(position2==2){
			System.out.println("date is "+variable);
			hour=variable.substring(0, 2);
			System.out.println(hour);
		}
		
		Integer endindex=variable.length();
		String variable2=variable.substring(position2+1, endindex);
		System.out.println(variable2);
		if(variable2.contains("p")){
			System.out.println("afternoon");
			Integer position=variable2.indexOf("p");
			System.out.println(position);
			if(position==1){
				minute="0"+variable2.substring(0, 1);
				System.out.println(minute);
			}
			else if(position==2){
				minute=variable2.substring(0, 2);
				System.out.println(minute);
			}
			hour=String.valueOf(Integer.parseInt(hour)+12);
			System.out.println("updated hour "+hour);
			Integer limit=Integer.parseInt(hour);
			if(limit>24){
				limit=limit-24;
				hour=String.valueOf("0"+limit);
				System.out.println("updated twice hour "+hour);
			}
		}
		else if(variable2.contains("a")){
			System.out.println("morning");
			Integer position=variable2.indexOf("a");
			System.out.println(position);
			if(position==1){
				minute="0"+variable2.substring(0, 1);
				System.out.println(minute);
			}
			else if(position==2){
				minute=variable2.substring(0, 2);
				System.out.println(minute);
			}
			System.out.println("updated morning hour "+hour);
		}
		int sec = Calendar.getInstance().get(Calendar.SECOND);
		String second;
		if(sec<10){
			second=String.valueOf(sec);
			second="0"+sec;
		}
		else{
			second=String.valueOf(sec);
		}
		System.out.println("final second is "+second);
		String finaltime = hour + ":" + minute + ":" + second;
		System.out.println(finaltime);
		return finaltime;
	}
	
}
